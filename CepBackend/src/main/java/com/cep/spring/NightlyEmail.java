package com.cep.spring;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.model.testscore.RecordCount;
import com.cep.spring.utils.ConductSSLHandShake;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

@RestController
public class NightlyEmail {
	
	private Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("NightlyEmail");
	private static final String PROD_URL = "https://ceptsws-pki.dmdc.osd.mil/ceptsws";
	private static final String CEP_PROD_URL = "http://www.asvabprogram.com/";
	
	@Value("${application.url}")
	private String appUrl;
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ContactUsDAO contactUsDao;
	
	@Autowired
	CEPMailer mailer;
	
	@Autowired
	TestScoreDao dao;
	
	@Autowired
	ConductSSLHandShake conductSSLHandShake;
	
	@Scheduled(cron = "0 0 1 * * TUE,WED,THU,FRI,SAT")
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, value="/getNightlyEmail")
	public @ResponseBody void sendNightlyEmail() throws Exception{
		if(prodCheck()) {
//			logger.setLevel(Level.ALL);
			
			boolean isWebServiceOn = Boolean.valueOf(jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='turn_on_web_service_accesscode'", String.class));
	
			ArrayList<String> emails = contactUsDao.getEmail(12);
			
			if (!Boolean.valueOf(isWebServiceOn)) {
				logger.debug("Currently on webservice is turned off. Terminating daily email.");
				for (int i = 0; i < emails.size(); i++) {
					mailer.sendEmail(emails.get(i), "The web service is turned off today.","Nightly test score report", null);
				}
				return;
			}
	
			RestTemplate restTemplate = conductSSLHandShake.doHandShake();
			
			LocalDateTime date = LocalDateTime.now().minusDays(1);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			String sourceFileName = date.format(formatter);
			logger.debug("STARTING: " + sourceFileName);
			

			logger.debug("Sending Nightly Email for "+ sourceFileName);
			
			int icatCount = dao.getRecordCount(sourceFileName, "ICAT");
			String dmdcSTPCount = "0";
			String dmdcIcatCount = null;
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				String icatTypeCount = getICATCountFromDMDC(restTemplate, sourceFileName);
				dmdcIcatCount = parse(icatTypeCount, sourceFileName, objectMapper).get(sourceFileName).toString();
			}catch (Exception e) {
				dmdcIcatCount = "No test score counts returned for iCAT CEP.";
			}
			 
			int stpCount = dao.getRecordCount(sourceFileName, "STP");

			try {
				String stpTypeCount = getSTPCountFromDMDC(restTemplate, sourceFileName);
				dmdcSTPCount = parse(stpTypeCount, sourceFileName, objectMapper).get(sourceFileName).toString();

			}catch(Exception e) {
				dmdcSTPCount ="No test score counts returned for MEPCOM PNP.";
			}
			
			for (int i = 0; i < emails.size(); i++) {
				mailer.sendEmail(emails.get(i), "For "+sourceFileName+":\n iCat CEP scores transferred to CEP: "+icatCount+"\n iCat CEP scores at DMDC: "+dmdcIcatCount+"\n MEPCOM PNP scores transferred to CEP: "+stpCount+"\n MEPCOM PNP scores at DMDC: "+dmdcSTPCount ,"Nightly test score report", null);
			}
		}
	}
	
	private HashMap<String, Integer> parse(String jsonString, String sourceFileName, ObjectMapper objectMapper) {
		HashMap<String, Integer> map = new HashMap<>();
		try {
			List<RecordCount> pnpRecordCount = objectMapper.readValue("["+jsonString+"]", new TypeReference<List<RecordCount>>(){});
			for(RecordCount fileName: pnpRecordCount) {
				if(map.containsKey(fileName.getRecordDate()))
					map.put(fileName.getRecordDate(), map.get(fileName.getRecordDate()) + Integer.valueOf(fileName.getCount()));
				else
					map.put(fileName.getRecordDate(), Integer.valueOf(fileName.getCount()));
			}
		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return map;
	}
	
	private boolean prodCheck() {
		return CEP_PROD_URL.contains(appUrl) ? true : false;
	}
	
	private String getICATCountFromDMDC(RestTemplate restTemplate, String recordDate) {
		return restTemplate.getForObject(PROD_URL+"/rest/countIcatByDate/"+recordDate+"/"+recordDate, String.class);
	}
	
	private String getSTPCountFromDMDC(RestTemplate restTemplate, String recordDate) {
		return restTemplate.getForObject(PROD_URL+"/rest/getPnpByDate/"+recordDate+"/"+recordDate, String.class);
	}
	
}
