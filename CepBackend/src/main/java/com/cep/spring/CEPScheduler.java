package com.cep.spring;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CEPScheduler {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	
//  @Scheduled(cron = "0 1 1 * * ?")
//  Here you are some example pattern found on spring forum:
//
//  * "0 0 * * * *" = the top of every hour of every day.
//  * "*/10 * * * * *" = every ten seconds.
//  * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
//  * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
//  * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
//  * "0 0 0 25 12 ?" = every Christmas Day at midnight
//  Cron expression is represented by six fields:
//
//  second, minute, hour, day of month, month, day(s) of week
//  (*) means match any
//
//  */X means "every X"    
	
	// Every June 30 at Midnight
// 	@Scheduled(cron="0 0 0 30 6 ?")
//    // Every Day 10 minutes 
//    @Scheduled(cron="0 */10 * * * ?")
    // Every Day at 0600  
	@Scheduled(cron="0 0 6 * * ?")
	public void doSomething() {
	    logger.info("doing something ... ie cron timer kicks off this message morning at 0600.");
	}
}
