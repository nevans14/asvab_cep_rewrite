package com.cep.spring;


import java.util.Date;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpSessionChecker implements HttpSessionListener {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		System.out.println("Test");
		logger.debug(String.format("Session ID %s created at %s%n", se.getSession().getId(), new Date()));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		// TODO Auto-generated method stub
		logger.debug(String.format("Session ID %s destroyed at %s%n", se.getSession().getId(), new Date()));
	}

	
}