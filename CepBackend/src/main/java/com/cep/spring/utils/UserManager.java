package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
public class UserManager {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

    @Autowired
    private HttpServletRequest request;

    public void setUserId(Integer userId) {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                session.setAttribute("ASVAB_USER_ID", String.valueOf(userId));
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
            }
        }
    }

    /***
     * Returns the User ID of the logged in user.
     * @return returns user id in session
     */
    public Integer getUserId() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("ASVAB_USER_ID");
                if (attribute != null) {
                    return Integer.valueOf(attribute.toString());
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    /***
     * Returns the Access Code of the logged in user.
     * @return returns user access code in session
     */
    public String getUserAccessCode() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("USER_ACCESS_CODE");
                if (attribute != null) {
                    return attribute.toString();
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    /***
     * Returns the User Role of the logged in user.
     * @return returns the user role that's in session
     */
    public String getUserRole() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("USER_ROLE");
                if (attribute != null) {
                    return attribute.toString();
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    public boolean isRegistered() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("IS_REGISTERED");
                if (attribute != null) {
                    return Boolean.parseBoolean(attribute.toString());
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
            }
        }
        return false;
    }
}
