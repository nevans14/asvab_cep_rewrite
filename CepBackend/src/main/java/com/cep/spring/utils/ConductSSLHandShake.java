package com.cep.spring.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConductSSLHandShake {

	public RestTemplate doHandShake() {

		InputStream keystoreInputStream = null;
		InputStream truststoreInputStream = null;

		try {

			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			ClassPathResource keystoreFile = new ClassPathResource("cepKeyStore.jks");
			keystoreInputStream = keystoreFile.getInputStream();
			keystore.load(keystoreInputStream, "Shannon1!@".toCharArray());

			keystoreInputStream.close();

			KeyStore trustore = KeyStore.getInstance(KeyStore.getDefaultType());
			ClassPathResource truststoreFile = new ClassPathResource("cepTrustStore.jks");
			truststoreInputStream = truststoreFile.getInputStream();
			trustore.load(truststoreInputStream, "dmdc01".toCharArray());

			SSLContext sslcontext = SSLContexts.custom().useProtocol("TLS").loadKeyMaterial(keystore, "Shannon1!@Shannon1!@".toCharArray()).loadTrustMaterial(trustore, null).build();

			HostnameVerifier hostnameverifier = null;

			SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslcontext, null, null, hostnameverifier);

			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSocketFactory).build();

			HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

			requestFactory.setHttpClient(httpClient);
			requestFactory.setBufferRequestBody(false);
			
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
			return restTemplate;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (keystoreInputStream != null) {
				try {
					keystoreInputStream.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			if (truststoreInputStream != null) {
				try {
					truststoreInputStream.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
}
