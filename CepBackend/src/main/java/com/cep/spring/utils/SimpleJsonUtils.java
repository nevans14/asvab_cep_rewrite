package com.cep.spring.utils;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

//import mil.osd.dmdc.psa.aces.processengine.common.model.Phase;

/**
 * SimpleJsonUtils -  Simple Shared Utilities that transfer common objects to JSON and JSON strings to objects.
 *   
 *   Note:
 *   These methods are synchronized such that the objectMapper object does not get modified during execution.
 *   
 * 
 * @author Dave Springer
 *
 */
public class SimpleJsonUtils {

	private static ObjectMapper objectMapper = new ObjectMapper();
	

	/**
	 * parseToJson converts a Phase object to a json string representing the object.
	 * @param obj phase is the object to be converted to json.
	 * @return a json string representing the phase.
	 */
	public static synchronized String parseToJson(Object obj) {
		String message=""; 
		try {
			message = objectMapper.writeValueAsString(obj) ;
			// Debug statement 
			// System.out.println( message);
		} catch (Exception e){
			RuntimeException re = new RuntimeException("parseToJson:Cannot convert Phase object to json string!");
			re.setStackTrace(e.getStackTrace());
			throw(re);
		}		
		return message;
	}
	
	/**
	 * pretty print json string 
	 * 	 * @param phase is the object to be converted to json.
	 * @return a json string representing the phase.
	 */
//	public static synchronized String prettyPrintJson(String jsonString) {
//		String indented="";
//		try {
//			Object json = objectMapper.readValue(jsonString, Object.class);
//			indented = objectMapper.defaultPrettyPrintingWriter().writeValueAsString(json);			
//			// Debug statement 
//			// System.out.println( message);
//		} catch (Exception e){
//			RuntimeException re = new RuntimeException("parseToJson:Cannot convert Phase object to json string!");
//			re.setStackTrace(e.getStackTrace());
//			throw(re);
//		}		
//		return indented;
//	}	
	

}
