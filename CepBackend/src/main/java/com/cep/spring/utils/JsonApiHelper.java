package com.cep.spring.utils;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

/***
 * <strong>JsonApiHelper</strong>
 * How to use:<br>
 * 1. Know the API REST URL, this will be the <code>baseUrl</code><br>
 * 2. Create a <code>HashMap<String,String></code> object to store parameters<br>
 * 3. Call the <code>addParameters</code> method, returns a formatted URL<br>
 * 4. Call the <code>sendRequest</code> method, returns a JSON Response String<br>
 * @author jcidras
 *
 */
public class JsonApiHelper {
	
	/***
	 * Builds a REST URL with the given base URL and parameters
	 * @param hasQuestionMark starts the query string with a question mark
	 * @param baseUrl
	 * @param params
	 * @return Formatted URL (String)
	 */
	public static String addParameters(boolean hasQuestionMark, String baseUrl, Map<String, String> params) {
		StringBuilder sb = new StringBuilder();
		sb.append(baseUrl);
		int index = 0;
		for (Entry<String, String> entry : params.entrySet()) {
			if (index == 0) {
				sb.append((hasQuestionMark ? "?" : "") + entry.getKey() + "=" + entry.getValue());
			} else {
				sb.append("&" + entry.getKey() + "=" + entry.getValue());
			}
			index++;
		}
		return sb.toString();
	}
	
	/***
	 * Sends a request with the given URL.
	 * @param connection the https connection to requested target
	 * @return Response (JSON String)
	 * @throws Exception
	 */
	public static String sendRequest(HttpURLConnection connection) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
		return readAll(br);
	}
	
	/***
	 * Reads the JSON response into a String
	 * @param rd
	 * @return String
	 * @throws Exception
	 */
	public static String readAll(Reader rd) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
		
		return sb.toString();
	}
}