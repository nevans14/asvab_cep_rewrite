package com.cep.spring.mvc.controller;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.MediaCenterDAOImpl;
import com.cep.spring.model.media.MediaCenter;

@Controller
@RequestMapping("/media-center")
public class MediaCenterController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	MediaCenterDAOImpl dao;

	@RequestMapping(value = "/getMediaCenterList", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<MediaCenter>> getMediaCenterList() {
		logger.info("getMediaCenterList");
		ArrayList<MediaCenter> results;
		try {
			results = dao.getMediaCenterList();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<MediaCenter>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<MediaCenter>>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMediaCenterByCategoryId/{categoryId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<MediaCenter>> getMediaCenterByCategoryId(@PathVariable Integer categoryId) {
		if (categoryId == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		
		ArrayList<MediaCenter> results;
		
		try {
			results = dao.getMediaCenterByCategoryId(categoryId);
		} catch (Exception e) {
			logger.error("Error", e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(results);
	}
	
	@RequestMapping(value = "/getMediaCenterArticleById/{mediaId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MediaCenter> getMediaCenterArticleById(@PathVariable int mediaId) {
		logger.info("getMediaCenterArticleById" + mediaId);
		MediaCenter results;
		try {
			results = dao.getArticleById(mediaId);
		} catch (Exception e) {
			logger.error("Error", e);			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		return ResponseEntity.status(HttpStatus.OK).body(results);		
	}
}
