package com.cep.spring.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cep.spring.dao.mybatis.ResourceDAO;
import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

@RestController
@RequestMapping("/resources")
public class ResourceController {
	
	@Autowired
	ResourceDAO dao;
	
	@RequestMapping(value = "/get-resources", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResourceTopic>> getResources() throws Exception {
		List<ResourceTopic> results = null;
		
		results = dao.getResources();
		
		return new ResponseEntity<List<ResourceTopic>>(results, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/get-quick-links", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResourceQuickLinkTopic>> getResourceTopics() throws Exception {
		List<ResourceQuickLinkTopic> results = null;
		
		results = dao.getQuickLinks();
		
		return new ResponseEntity<List<ResourceQuickLinkTopic>>(results, HttpStatus.OK);
	}
}