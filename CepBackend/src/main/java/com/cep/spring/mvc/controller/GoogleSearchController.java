package com.cep.spring.mvc.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.cep.spring.utils.JsonApiHelper;

@RestController
@RequestMapping("/google-search")
public class GoogleSearchController {
	
	private final String GOOGLE_SEARCH_KEY = "AIzaSyBEiT9QZsrHfmprjtmIkfiHmGPaN7QDqP8";
	private final String GOOGLE_SEARCH_CX = "016306542328905981446:wkbq-87up34";
	private final String GOOGLE_SEARCH_URL = "https://www.googleapis.com/customsearch/v1";
	
	@RequestMapping(value = "/get-search-results/{searchString}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getSearchResults(@PathVariable String searchString) {
		String results = null;
		try {
			
			// get parameters
			HashMap<String, String> params = getDefaultParameters();
			// add parameter for text
			params.put("q", URLEncoder.encode(searchString, "UTF-8"));
			// build request url
			String requestUrl = JsonApiHelper.addParameters(true,GOOGLE_SEARCH_URL, params);
			// build connection and request
			InputStream is = new URL(requestUrl).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			// write to string
			results = JsonApiHelper.readAll(rd);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(results, HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/get-search-results/{searchString}/{startIndex}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getSearchResults(@PathVariable String searchString, @PathVariable Integer startIndex) {
		String results = null;
		try {
			// get parameters
			HashMap<String, String> params = getDefaultParameters();
			// add parameter for text and start index
			params.put("q", URLEncoder.encode(searchString, "UTF-8"));
			params.put("start", String.valueOf(startIndex));
			// build request url
			String requestUrl = JsonApiHelper.addParameters(true, GOOGLE_SEARCH_URL, params);
			// build connection and request
			InputStream is = new URL(requestUrl).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			// write to string
			results = JsonApiHelper.readAll(rd);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(results, HttpStatus.OK);
	}	
	
	private HashMap<String, String> getDefaultParameters() {
		return new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
		{
			put("key", GOOGLE_SEARCH_KEY);
			put("cx", GOOGLE_SEARCH_CX);
		}};
	}
}