package com.cep.spring.mvc.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.dao.mybatis.impl.TestScoreDAOImpl;
import com.cep.spring.dao.mybatis.service.TestScoreService;
import com.cep.spring.model.testscore.CSVFileCreator;
import com.cep.spring.model.testscore.FilterScoreObj;
import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;
import com.cep.spring.model.AccessCodes;

@Controller
@RequestMapping("/testScore")
public class TestScoreController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	TestScoreDao dao;
	@Autowired
	RegistrationDAO registrationDao;
	@Autowired
	UserManager userManager;
	@Autowired
	CSVFileCreator csvFileCreator;
//	Allow Spring MVC to inject on the fly.  Doing it this way increase the configuration 
//    need for unit testing. See filter Test scores ... filterTestScores(...)
//	@Autowired
//	HttpServletResponse response;
	
	@Autowired
	TestScoreService testScoreService;

	@RequestMapping(value = "/InsertTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertTestScore(
			@RequestBody List<StudentTestingProgam> testScoreObjects) {
		logger.info("insertTestScore");
		int rowsInserted = 0;
		try {
			rowsInserted = dao.insertTestScoreimpl(testScoreObjects);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted == -1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowsInserted, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-test-score-access-code/{accessCode}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScoreWithAccessCode(@PathVariable String accessCode) {
	    logger.info("getTestScoreWithAccessCode");
	    StudentTestingProgamSelects studentTestingProgramObject = null;
		try {
			studentTestingProgramObject = dao.getTestScoreWithAccessCode(accessCode);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<StudentTestingProgamSelects>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StudentTestingProgamSelects>(studentTestingProgramObject, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-test-score", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScore() {
		logger.info("get-test-scores");
		// get user info from session
		Integer userId = userManager.getUserId();
		String accessCode = userManager.getUserAccessCode();
		String role = userManager.getUserRole();
		StudentTestingProgamSelects results = null;
		try {
			// if the user is a role of "T"eacher, then get a mock ASVAB Score
			if (role.equals("T")) {
				String mockAccessCode = accessCode.contains("CEP123") ? "SHANNON-A" : "KATE-A";
				// get mock test scores
				results = dao.getTestScoreWithAccessCode(mockAccessCode);
			} else {
				results = dao.getTestScoreWithUserId(userId);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<StudentTestingProgamSelects>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StudentTestingProgamSelects>(results, HttpStatus.OK);
	}

	@Deprecated
	@RequestMapping(value = "/get-test-score-user-id/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScoreWithUserId(@PathVariable Integer userId) {
	    logger.info("getTestScoreWithUserId");
	    // get user role via access code
	    AccessCodes accessCode = registrationDao.getAccessCodes(null, userId);
	    StudentTestingProgamSelects studentTestingProgramObject = null;
		try {
			// if the user is a role of "T"eacher, then get a mock ASVAB Score
			if (accessCode.getRole().equals("T")) {
				studentTestingProgramObject = dao.getTestScoreWithAccessCode("SHANNON-A");
			} else {
				studentTestingProgramObject = dao.getTestScoreWithUserId(userId);
			}			
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<StudentTestingProgamSelects>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StudentTestingProgamSelects>(studentTestingProgramObject, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filter-results/{filterScoreObj}/", method = RequestMethod.POST)
	public void filterTestScores(@PathVariable FilterScoreObj filterScoreObj, HttpServletResponse response) {
	    logger.info("getTestScoreWithAccessCode");
	    List<StudentTestingProgamSelects> studentTestingProgramObjects = null;
		StringBuffer sb = new StringBuffer();
		try {
			studentTestingProgramObjects = dao.filterTestScore(filterScoreObj);
			for(StudentTestingProgamSelects stp: studentTestingProgramObjects) {
				if(sb.length() == 0)
					sb.append(csvFileCreator.getHeader()+"\n");
				sb.append(csvFileCreator.writeSingleRecord(stp)+"\n");
			}
			System.out.println(sb.toString());
			testScoreService.downloadFile(response, sb.toString(), filterScoreObj.getFileName());
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
	
	@RequestMapping(value = "/insertManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("insertManualTestScore");
		int rowsInserted = 0;
		manualTestScoreObj.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowsInserted, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getManualTestScore/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ManualTestScore> getManualTestScore() {
	    logger.info("getManaulTestScore");
	    ManualTestScore manualTestScoreObj = null;
	    Integer userId = userManager.getUserId();
		try {
			manualTestScoreObj = dao.getManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ManualTestScore>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ManualTestScore>(manualTestScoreObj, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> updateManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("updateManualTestScore");
		int rowUpdated = 0;
		try {
			rowUpdated = dao.updateManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowUpdated != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowUpdated, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteManualTestScore/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> deleteManualTestScore() {
	    logger.info("getManaulTestScore");
	    Integer userId = userManager.getUserId();
	    int rowdeleted = 0;
		try {
			rowdeleted = dao.deleteManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Integer>(rowdeleted, HttpStatus.OK);
	}
	
	public StudentTestingProgam wsClient(){
		
		RestTemplate restTemplate = new RestTemplate();
		StudentTestingProgam testScoreObjects = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/ap", StudentTestingProgam.class);
		return testScoreObjects;
	}
}
