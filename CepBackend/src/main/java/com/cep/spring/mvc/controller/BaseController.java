package com.cep.spring.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cep.spring.dao.mybatis.impl.FindYourInterestDAOImpl;

@Controller
public class BaseController {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
	private static int counter = 0;
	private static final String VIEW_INDEX = "index";

	
	@Autowired
	FindYourInterestDAOImpl fyiDao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {
	    logger.info("welcome");

		String testId = fyiDao.getTestId();
		
		model.addAttribute("message", "Welcome your test id = " + testId);
		model.addAttribute("counter", ++counter);
		logger.debug("[welcome] counter : {}", counter);

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return VIEW_INDEX;

	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String name, ModelMap model) {
		
		String testId = fyiDao.getTestId();
		

		model.addAttribute("message", "Welcome " + name + testId);
		model.addAttribute("counter", ++counter);
		logger.debug("[welcomeName] counter : {}", counter);
		return VIEW_INDEX;
	}

}