package com.cep.spring.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.HomeDAO;
import com.cep.spring.model.HomepageImage;

@Controller
@RequestMapping("/home")
public class HomeController {
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	HomeDAO dao;
	
	@RequestMapping(value="/get-current-homepage-img", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<HomepageImage> getCurrentHomepageImg() {
		logger.info("fetching current homepage img");
		
		HomepageImage results = dao.getCurrentHomepageImg();
		
		return new ResponseEntity<HomepageImage>(results, HttpStatus.OK);
	}	
}