package com.cep.spring.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cep.spring.dao.mybatis.StaticPageDAO;
import com.cep.spring.model.StaticPage;

@RestController
@RequestMapping("/pages")
public class StaticPageController {
	
	@Autowired
	StaticPageDAO dao;
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@RequestMapping(value = "/get-by-page-id/{pageId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StaticPage> getPageById(@PathVariable int pageId) {
		StaticPage results = null;
		
		try {
			results = dao.getPageById(pageId);
		} catch (Exception e) {
			return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-by-page-name/{pageName}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StaticPage> getPageByPageName(@PathVariable String pageName) {
		StaticPage results = null;
		
		try {
			results = dao.getPageByPageNameOld(pageName);
		} catch (Exception e) {
			return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
	}
	
    @RequestMapping(value = "/get-by-page-name-system/{pagename}/{website}/", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<StaticPage> getPageByPageName(@PathVariable("pagename") String pageName, @PathVariable("website") String website) {
        StaticPage results = null;
        
        try {
            results = dao.getPageByPageName(pageName, website);
        } catch (Exception e) {
            logger.error("Error getting pageByName", e);
            return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
    }
    
}