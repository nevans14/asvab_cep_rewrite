package com.cep.spring.mvc.controller; 

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import javax.net.ssl.SSLContext;

/**
 * Controller that allows for application login and user registration.
 * 
 * This controller has the following endpoints
 * loginAccessCode
 * user
 * loginEmail
 * register
 * passwordResetRequest
 * passwordReset
 * 
 * @author Dave Springer
 *
 */
@Controller
@RequestMapping("/CareerOneStop")
    public class CareerOneStopController { 
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	private final static String LIMIT_RECORD = "100";
	private final static String CAREER_ONE_STOP_CUSTOMER_ID = "wIAYt7EaoNXai43";
	private final static String CAREER_ONE_STOP_API_TOKEN = "Bearer 9ewTt7exOMGnQs/jOPm6P6wG4UPGrSRA7fZUdzFPfCfOLmSP5ZuyfvKq5zTFPnwFy/dn1jEooRuwXPc3UAkfKA==";
	private final static String CAREER_ONE_STOP_CERTIFICATION_URL = "https://api.careeronestop.org/v1/certificationfinder/#CUSTOMER_ID#/#ONET_SOC#/0/0/0/0/0/0/0/0/0/#LIMIT_RECORD#";
	private final static String CAREER_ONE_STOP_CERTIFICATION_DETAIL_URL  = "https://api.careeronestop.org/v1/certificationfinder/#CUSTOMER_ID#/#CERT_ID#";
	private final static String CAREER_ONE_STOP_LICENSES = "https://api.careeronestop.org/v1/license/#CUSTOMER_ID#/#ONET_SOC#/#STATE#/0/0/0/#LIMIT_RECORD#";
	private final static String CAREER_ONE_STOP_LICENSE_DETAILS = "https://api.careeronestop.org/v1/license/#CUSTOMER_ID#/#ONET_SOC#/#LICENSE_ID#";
	private final static String CAREER_ONE_STOP_TRAINING_BY_OCCUPATION = "https://api.careeronestop.org/v1/Training/#CUSTOMER_ID#/#STATE#/#ONET_SOC#/0/0/0/0/0/0/#LIMIT_RECORD#";
	private final static String CAREER_ONE_STOP_TRAINING_BY_KNOWLEDGE_AREA = "https://api.careeronestop.org/v1/Training/#CUSTOMER_ID#/#ONET_SKILL_ID#/#STATE#/#RADIUS_IN_MILES#/0/0/0/0/0/0/0/0/#LIMIT_RECORD#";
	private final static String CAREER_ONE_STOP_APPRENTICESHIP = "https://api.careeronestop.org/v1/apprenticeshipfinder/#CUSTOMER_ID#/#ONET_SOC#/#STATE#/20/0/0/0/0/0/#LIMIT_RECORD#";

	private static final Integer CERT = 1;
	private static final Integer CERT_DETAIL = 2;
	private static final Integer LICENSE = 3;
	private static final Integer LICENSE_DETAIL = 4;
	private static final Integer TRAINING = 5;
	private static final Integer TRAINING_BY_KNOWLEDGE = 6;
	private static final Integer APPRENTICESHIP = 7;
	
	@RequestMapping(value = "/cert/{onet_soc}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				certificate(@PathVariable String onet_soc) {
	    logger.info("certificate");
		CareerOneStopPayload cosp = new CareerOneStopPayload();
		
		if ( onet_soc.contains("socId") ) {
		      logger.error("onet soc id is not valid ... :" + onet_soc );
		      String jsonPrettyPrintString = "" ;
		      JSONObject xmlJSONObj;
            try {
                xmlJSONObj = XML.toJSONObject(
                          "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                  "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                  "   <ErrorNumber>-4</ErrorNumber> " +
                                  "   <CertificationDetails i:nil=\"true\"/> " +
                                  "   <CertificationList i:nil=\"true\"/> " +
                                  "   <ORGList i:nil=\"true\"/> " +
                                  "   <OnetTitle i:nil=\"true\"/> " +
                                  "</CertificationInfo>"		          
                          );
          
              jsonPrettyPrintString = xmlJSONObj.toString(2);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		      cosp.setType("CERT");
		      cosp.setTypeNumber(CERT);
		      cosp.setPayload(jsonPrettyPrintString); 
		      return cosp;
		}
		
		String url = CAREER_ONE_STOP_CERTIFICATION_URL.
					replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
					replace("#ONET_SOC#", onet_soc).
					replace("#LIMIT_RECORD#", LIMIT_RECORD);
		logger.info(url);

		cosp.setType("CERT");
		cosp.setTypeNumber(CERT);
		cosp.setPayload(callCareerOneStop(url));
		return cosp ;		
		
	}
	
	
	
	@RequestMapping(value = "/certDetail/{cert_id}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				certificateDetail(@PathVariable String cert_id) {
		logger.info("certificateDetail");
		CareerOneStopPayload cosp = new CareerOneStopPayload();

		String url = CAREER_ONE_STOP_CERTIFICATION_DETAIL_URL.
							replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
							replace("#CERT_ID#", cert_id).
							replace("#LIMIT_RECORD#", LIMIT_RECORD);
		logger.info(url);

		cosp.setType("CERT_DETAIL");
		cosp.setTypeNumber(CERT_DETAIL);
		cosp.setPayload(callCareerOneStop(url));
		return cosp ;		
	}

	
	@RequestMapping(value = "/license/{onet_soc}/{state}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
						license(@PathVariable("onet_soc") String onet_soc,
								@PathVariable("state") String state) {
	    logger.info("license");
		CareerOneStopPayload cosp = new CareerOneStopPayload();

		String url = CAREER_ONE_STOP_LICENSES.replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
												replace("#ONET_SOC#", onet_soc).
												replace("#STATE#", state).
												replace("#LIMIT_RECORD#", LIMIT_RECORD);
        if ( onet_soc.contains("socId") ) {
            logger.error("onet soc id is not valid ... :" + onet_soc );
            String jsonPrettyPrintString = "" ;
            JSONObject xmlJSONObj;
          try {
              xmlJSONObj = XML.toJSONObject(
                        "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                "   <ErrorNumber>-4</ErrorNumber> " +
                                "   <CertificationDetails i:nil=\"true\"/> " +
                                "   <CertificationList i:nil=\"true\"/> " +
                                "   <ORGList i:nil=\"true\"/> " +
                                "   <OnetTitle i:nil=\"true\"/> " +
                                "</CertificationInfo>"                  
                        );
        
            jsonPrettyPrintString = xmlJSONObj.toString(2);
          } catch (JSONException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
            cosp.setType("CERT");
            cosp.setTypeNumber(CERT);
            cosp.setPayload(jsonPrettyPrintString);
            return cosp;
      }

		cosp.setType("LICENSE");
		cosp.setTypeNumber(LICENSE);
		cosp.setPayload(callCareerOneStop(url));
		return cosp ;		
	}
		
	@RequestMapping(value = "/licenseDetail/{onet_soc}/{license_id}/{state}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				licenseDetail(@PathVariable("onet_soc") String onet_soc,
						@PathVariable("license_id") String license_id) {
		logger.info("licenseDetail");
		CareerOneStopPayload cosp = new CareerOneStopPayload();

		String url = CAREER_ONE_STOP_LICENSE_DETAILS.replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
										replace("#ONET_SOC#", onet_soc).
										replace("#LICENSE_ID#",license_id).
										replace("#LIMIT_RECORD#", LIMIT_RECORD);
		
        if ( onet_soc.contains("socId") ) {
            logger.error("onet soc id is not valid ... :" + onet_soc );
            String jsonPrettyPrintString = "" ;
            JSONObject xmlJSONObj;
          try {
              xmlJSONObj = XML.toJSONObject(
                        "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                "   <ErrorNumber>-4</ErrorNumber> " +
                                "   <CertificationDetails i:nil=\"true\"/> " +
                                "   <CertificationList i:nil=\"true\"/> " +
                                "   <ORGList i:nil=\"true\"/> " +
                                "   <OnetTitle i:nil=\"true\"/> " +
                                "</CertificationInfo>"                  
                        );
        
            jsonPrettyPrintString = xmlJSONObj.toString(2);
          } catch (JSONException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
            cosp.setType("CERT");
            cosp.setTypeNumber(CERT);
            cosp.setPayload( jsonPrettyPrintString
                    );
              
            return cosp;
      }

		cosp.setType("LICENSE_DETAIL");
		cosp.setTypeNumber(LICENSE_DETAIL);
		cosp.setPayload(callCareerOneStop(url));
		
		return cosp ;		
	}
	
	@RequestMapping(value = "/training/{onet_soc}/{zip_code}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				training(@PathVariable("onet_soc") String onet_soc,
						 @PathVariable("state") String state) {
		logger.info("training");
		CareerOneStopPayload cosp = new CareerOneStopPayload();
		
		String url = CAREER_ONE_STOP_TRAINING_BY_OCCUPATION.
					replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
					replace("#STATE#", state). 
					replace("#ONET_SOC#", onet_soc). 
					replace("#LIMIT_RECORD#", LIMIT_RECORD);
		logger.info(url);

        if ( onet_soc.contains("socId") ) {
            logger.error("onet soc id is not valid ... :" + onet_soc );
            String jsonPrettyPrintString = "" ;
            JSONObject xmlJSONObj;
          try {
              xmlJSONObj = XML.toJSONObject(
                        "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                "   <ErrorNumber>-4</ErrorNumber> " +
                                "   <CertificationDetails i:nil=\"true\"/> " +
                                "   <CertificationList i:nil=\"true\"/> " +
                                "   <ORGList i:nil=\"true\"/> " +
                                "   <OnetTitle i:nil=\"true\"/> " +
                                "</CertificationInfo>"                  
                        );
        
            jsonPrettyPrintString = xmlJSONObj.toString(2);
          } catch (JSONException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
            cosp.setType("CERT");
            cosp.setTypeNumber(CERT);
            cosp.setPayload( jsonPrettyPrintString
                    );
              
            return cosp;
      }

		cosp.setType("TRAINING");
		cosp.setTypeNumber(TRAINING);
		cosp.setPayload(callCareerOneStop(url));

		return cosp ;		
	}
	
	@RequestMapping(value = "/training/{onet_soc}/{onet_skill_id}/{zip_code}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				trainingByKnowledgeArea(@PathVariable("onet_soc") String onet_soc,
						@PathVariable("onet_skill_id") String onet_skill_id,
						 @PathVariable("state") String state) {
		logger.info("trainingByKnowledgeArea");
		CareerOneStopPayload cosp = new CareerOneStopPayload();

		
		
		String url = CAREER_ONE_STOP_TRAINING_BY_KNOWLEDGE_AREA.
					replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
					replace("#ONET_SKILL_ID#", onet_skill_id). 
					replace("#STATE#", state). 
					replace("#RADIUS_IN_MILES#", "50"). 
					replace("#LIMIT_RECORD#", LIMIT_RECORD);
		logger.info(url);

        if ( onet_soc.contains("socId") ) {
            logger.error("onet soc id is not valid ... :" + onet_soc );
            String jsonPrettyPrintString = "" ;
            JSONObject xmlJSONObj;
          try {
              xmlJSONObj = XML.toJSONObject(
                        "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                "   <ErrorNumber>-4</ErrorNumber> " +
                                "   <CertificationDetails i:nil=\"true\"/> " +
                                "   <CertificationList i:nil=\"true\"/> " +
                                "   <ORGList i:nil=\"true\"/> " +
                                "   <OnetTitle i:nil=\"true\"/> " +
                                "</CertificationInfo>"                  
                        );
        
            jsonPrettyPrintString = xmlJSONObj.toString(2);
          } catch (JSONException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
            cosp.setType("CERT");
            cosp.setTypeNumber(CERT);
            cosp.setPayload( jsonPrettyPrintString
                    );
              
            return cosp;
      }

		cosp.setType("TRAINING_BY_KNOWLEDGE");
		cosp.setTypeNumber(TRAINING_BY_KNOWLEDGE);
		cosp.setPayload(callCareerOneStop(url));

		return cosp ;		
	}
	
	@RequestMapping(value = "/apprenticeship/{onet_soc}/{state}", method = RequestMethod.GET)
	public @ResponseBody CareerOneStopPayload  
				trainingByKnowledgeArea(@PathVariable("onet_soc") String onet_soc,
						 @PathVariable("state") String state) {
		logger.info("trainingByKnowledgeArea");
		CareerOneStopPayload cosp = new CareerOneStopPayload();

		
		String url = CAREER_ONE_STOP_APPRENTICESHIP.
					replace("#CUSTOMER_ID#", CAREER_ONE_STOP_CUSTOMER_ID).
					replace("#ONET_SOC#", onet_soc). 
					replace("#STATE#",state).
					replace("#LIMIT_RECORD#", LIMIT_RECORD);
		logger.info(url);
        if ( onet_soc.contains("socId") ) {
            logger.error("onet soc id is not valid ... :" + onet_soc );
            String jsonPrettyPrintString = "" ;
            JSONObject xmlJSONObj;
          try {
              xmlJSONObj = XML.toJSONObject(
                        "<CertificationInfo xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"> " +
                                "   <ErrorMessage>Invalid OnetCode</ErrorMessage> " +
                                "   <ErrorNumber>-4</ErrorNumber> " +
                                "   <CertificationDetails i:nil=\"true\"/> " +
                                "   <CertificationList i:nil=\"true\"/> " +
                                "   <ORGList i:nil=\"true\"/> " +
                                "   <OnetTitle i:nil=\"true\"/> " +
                                "</CertificationInfo>"                  
                        );
        
            jsonPrettyPrintString = xmlJSONObj.toString(2);
          } catch (JSONException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
            cosp.setType("CERT");
            cosp.setTypeNumber(CERT);
            cosp.setPayload( jsonPrettyPrintString
                    );
              
            return cosp;
      }

		cosp.setType("APPRENTICESHIP");
		cosp.setTypeNumber(APPRENTICESHIP);
		cosp.setPayload(callCareerOneStop(url));

		return cosp ;		
	}
	
	/**
	 * Inner class for return payload.
	 * 
	 * 
	 * @author Dave Springer
	 *
	 */
	public class CareerOneStopPayload {
		
		private String 	type = "";
		private Integer typeNumber = 0;
		private String 	payload = "";
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Integer getTypeNumber() {
			return typeNumber;
		}
		public void setTypeNumber(Integer typeNumber) {
			this.typeNumber = typeNumber;
		}
		public String getPayload() {
			return payload;
		}
		public void setPayload(String payload) {
			this.payload = payload;
		}
		
	}

	private String callCareerOneStop(String url) {
		String returnJson = "";
		SSLContext context = null;
		HttpHeaders headers = null;
		RestTemplate restTemplate = null;
		CloseableHttpClient client = null;
		HttpEntity<String> request = null;
		ResponseEntity<String> response = null;
		HttpComponentsClientHttpRequestFactory factory = null;
		try {
			context = SSLContext.getInstance("TLSv1.2");
			context.init(null, null, null);
			
			client = HttpClientBuilder.create().setSSLContext(context).build();
			factory = new HttpComponentsClientHttpRequestFactory(client);
			
			restTemplate = new RestTemplate(factory);

			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", CAREER_ONE_STOP_API_TOKEN);
			request = new HttpEntity<String>(headers);
			
			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
			returnJson = response.getBody();
		}catch (Exception e) {
			returnJson = "No data found";
		}

		return returnJson ;		
	}
}