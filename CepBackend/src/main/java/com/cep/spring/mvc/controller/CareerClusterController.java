package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.CareerClusterDAOImpl;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Controller
@RequestMapping("/careerCluster")
public class CareerClusterController {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
//	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(CareerClusterController.class);

	@Autowired
	CareerClusterDAOImpl careerClusterDao;

	@RequestMapping(value = "/careerClusterList", method = RequestMethod.GET)
	public @ResponseBody List<CareerCluster> getCareerCluster() {
	    logger.info("getCareerCluster");
		List<CareerCluster> results = careerClusterDao.getCareerCluster();

		return results;
	}

	@RequestMapping(value = "/careerClusterOccupationList/{ccId}", method = RequestMethod.GET)
	public @ResponseBody List<CareerClusterOccupation> getCareerClusterOccupation(@PathVariable int ccId) {
	    logger.info("getCareerClusterOccupation");
		List<CareerClusterOccupation> results = careerClusterDao.getCareerClusterOccupation(ccId);

		return results;
	}

	@RequestMapping(value = "/occupationSkillImportance/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody SkillImportance getSkillImportance(@PathVariable String onetSoc) {
	    logger.info("getSkillImportance");
		SkillImportance results = careerClusterDao.getSkillImportance(onetSoc);

		return results;
	}

	@RequestMapping(value = "/careerClusterOccupationTaskList/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<Task> getTasks(@PathVariable String onetSoc) {
        logger.info("");
		List<Task> results = careerClusterDao.getTasks(onetSoc);

		return results;
	}

	@RequestMapping(value = "/occupationInterestCodes/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody OccupationInterestCode getOccupationInterestCodes(@PathVariable String onetSoc) {
	    logger.info("getOccupationInterestCodes");
		OccupationInterestCode results = careerClusterDao.getOccupationInterestCodes(onetSoc);

		return results;
	}

	@RequestMapping(value = "/occupationEducationLevel/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<OccupationEducationLevel> getEducationLevel(@PathVariable String onetSoc) {
	    logger.info("getEducationLevel");
		List<OccupationEducationLevel> results = careerClusterDao.getEducationLevel(onetSoc);

		return results;
	}

	@RequestMapping(value = "/relatedOccupation/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<RelatedOccupation> getRelatedOccupations(@PathVariable String onetSoc) {
	    logger.info("getRelatedOccupations");
		List<RelatedOccupation> results = careerClusterDao.getRelatedOccupations(onetSoc);

		return results;
	}

	@RequestMapping(value = "/CareerClusterOccupations/{ccId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getCareerClusterOccupations(@PathVariable Integer ccId) {

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = careerClusterDao.getCareerClusterOccupations(ccId);
		} catch (Exception e) {
			logger.error("Mybatis Error: ", e);
			return new ResponseEntity<List<OccufindSearch>>(HttpStatus.BAD_REQUEST);
		}

		if (results.size() == 0) {
			return new ResponseEntity<List<OccufindSearch>>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<List<OccufindSearch>>(results, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/CareerClusterById/{ccId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<CareerCluster> getCareerClusterById(@PathVariable Integer ccId) {

		CareerCluster results = null;
		try {
			results = careerClusterDao.getCareerClusterById(ccId);
		} catch (Exception e) {
			logger.error("Mybatis Error: ", e);
			return new ResponseEntity<CareerCluster>(HttpStatus.BAD_REQUEST);
		}

		if (results == null) {
			return new ResponseEntity<CareerCluster>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<CareerCluster>(results, HttpStatus.OK);
		}
	}

}
