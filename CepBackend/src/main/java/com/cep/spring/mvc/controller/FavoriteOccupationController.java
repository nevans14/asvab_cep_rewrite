package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.FavorateOccupationDAO;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Controller
@RequestMapping("/favorite")
public class FavoriteOccupationController {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	UserManager userManager;
	@Autowired
	FavorateOccupationDAO dao;

	@RequestMapping(value = "/getFavoriteOccupation", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteOccupation>> getFavoriteOccupation() {
	    logger.info("getFavoriteOccupation");
	    Integer userId = userManager.getUserId();
		ArrayList<FavoriteOccupation> results;
		try {
			results = dao.getFavoriteOccupation(userId);
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
			return new ResponseEntity<ArrayList<FavoriteOccupation>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<FavoriteOccupation>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteOccupation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteOccupation> insertFavoriteOccupation(
			@RequestBody FavoriteOccupation favoriteObject) {
	    logger.info("insertFavoriteOccupation");
		int rowsInserted = 0;
		favoriteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFavoriteOccupation(favoriteObject);
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
			return new ResponseEntity<FavoriteOccupation>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected result ..");
			return new ResponseEntity<FavoriteOccupation>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteOccupation>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteOccupation/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteOccupation(@PathVariable Integer id) {
		int result = 0;
		logger.info("deleteFavoriteOccupation");
		try {
			result = dao.deleteFavoriteOccupation(id);
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getFavoriteCareerCluster", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteCareerCluster>> getFavoriteCareerCluster() {
		Integer userId = userManager.getUserId();
		ArrayList<FavoriteCareerCluster> results;
		try {
			results = dao.getFavoriteCareerCluster(userId);
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
			return new ResponseEntity<ArrayList<FavoriteCareerCluster>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<FavoriteCareerCluster>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteCareerCluster", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteCareerCluster> insertFavoriteCareerCluster(
			@RequestBody FavoriteCareerCluster favoriteObject) {
		int rowsInserted = 0;
		favoriteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFavoriteCareerCluster(favoriteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FavoriteCareerCluster>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<FavoriteCareerCluster>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteCareerCluster>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteCareerCluster/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteCareerCluster(@PathVariable Integer id) {
		int result = 0;

		try {
			result = dao.deleteFavoriteCareerCluster(id);
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
	
	
	
	@RequestMapping(value = "/getFavoriteSchool", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteSchool>> getFavoriteSchool() {
	    logger.info("getFavoriteSchool");
		Integer userId = userManager.getUserId();
		ArrayList<FavoriteSchool> results;
		try {
			results = dao.getFavoriteSchool(userId);
		} catch (Exception e) {
			logger.error("FavoriteSchoolController Error: ", e);
			return new ResponseEntity<ArrayList<FavoriteSchool>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<FavoriteSchool>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteSchool", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteSchool> insertFavoriteSchool(
			@RequestBody FavoriteSchool favoriteObject) {
	    logger.info("insertFavoriteSchool");
	    // set the user id from session
	    favoriteObject.setUserId(userManager.getUserId());
		int rowsInserted = 0;
		try {
			rowsInserted = dao.insertFavoriteSchool(favoriteObject);
		} catch (Exception e) {
			logger.error("FavoriteSchoolController Error: ", e);
			return new ResponseEntity<FavoriteSchool>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected result ..");
			return new ResponseEntity<FavoriteSchool>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteSchool>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteSchool/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteSchool(@PathVariable Integer id) {
		int result = 0;
		logger.info("deleteFavoriteSchool");
		try {
			result = dao.deleteFavoriteSchool(id);
		} catch (Exception e) {
			logger.error("FavoriteSchoolController Error: ", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/merge-favorates/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void mergeFavorates(@PathVariable int oldUserId, @PathVariable int newUserId) {
		try {
			
			List<FavoriteCareerCluster> oldFavoratesCareerClusterList = dao.getFavoriteCareerCluster(oldUserId);
			List<FavoriteOccupation> oldFavoratesOccupationList  = dao.getFavoriteOccupation(oldUserId);
			List<FavoriteSchool> oldFavoriteSchoolList  = dao.getFavoriteSchool(oldUserId);
			List<FavoriteCitmOccupation> oldFavoratesCitmOccupationList  = dao.getFavoriteCitmOccupation(oldUserId);
			List<FavoriteMilitaryService> oldFavoriteMilitaryServiceList  = dao.getFavoriteService(oldUserId);
			
			if (oldFavoratesCareerClusterList.size()>0) {
				List<FavoriteCareerCluster> careerClusterList = new ArrayList<>();
				Set<String> uniqueCareerClusters = new HashSet<>();
				
				oldFavoratesCareerClusterList.forEach(a-> a.setUserId(newUserId));
				List<FavoriteCareerCluster> newFavoratesCareerClusterList = dao.getFavoriteCareerCluster(newUserId);
				newFavoratesCareerClusterList.forEach(a-> uniqueCareerClusters.add(a.getTitle()));
				

				oldFavoratesCareerClusterList.forEach(a-> { if(!uniqueCareerClusters.contains(a.getTitle())) 
															careerClusterList.add(a);});
				
				if (careerClusterList.size()>0)
					careerClusterList.forEach(a-> {
						try {
							dao.insertFavoriteCareerCluster(a);
							} catch (Exception e) {
								logger.error("FavoriteOccupationController Error: ", e);
							}
					});
			}
			if(oldFavoratesOccupationList.size()>0) {
				List<FavoriteOccupation> occupationList = new ArrayList<>();
				Set<String> uniqueOccupations = new HashSet<>();
				
				oldFavoratesOccupationList.forEach(a-> a.setUserId(newUserId));
				List<FavoriteOccupation> newFavoratesOccupationList  = dao.getFavoriteOccupation(newUserId);
				newFavoratesOccupationList.forEach(a-> uniqueOccupations.add(a.getOnetSocCd()));

				oldFavoratesOccupationList.forEach(a->{ if(!uniqueOccupations.contains(a.getOnetSocCd())) 
														occupationList.add(a);});

				if (occupationList.size()>0)
					occupationList.forEach(a-> {
						try {
							dao.insertFavoriteOccupation(a);
						} catch (Exception e) {
							logger.error("FavoriteOccupationController Error: ", e);
						}
					});
			}
			if(oldFavoriteSchoolList.size()>0) {
				List<FavoriteSchool> schoolList = new ArrayList<>();
				Set<Integer> uniqueSchools = new HashSet<>();

				oldFavoriteSchoolList.forEach(a-> a.setUserId(newUserId));
				List<FavoriteSchool> newFavoratesSchoolList  = dao.getFavoriteSchool(newUserId);
				newFavoratesSchoolList.forEach(a-> uniqueSchools.add(a.getUnitId()));
				

				oldFavoriteSchoolList.forEach(a->{ if(!uniqueSchools.contains(a.getUnitId())) 
														schoolList.add(a);});
				
				if (schoolList.size()>0)
					schoolList.forEach(a-> {
						try {
							dao.insertFavoriteSchool(a);
						} catch (Exception e) {
							System.out.println(e);
							logger.error("FavoriteOccupationController Error: ", e);
						}
					});
			}
			if(oldFavoratesCitmOccupationList.size()>0) {
				List<FavoriteCitmOccupation> FavoriteCitmOccupationList = new ArrayList<>();
				Set<String> uniqueCitmOccupation = new HashSet<>();
				
				oldFavoratesCitmOccupationList.forEach(a-> a.setUserId(newUserId));
				List<FavoriteCitmOccupation> newFavoratesCitmOccupationList  = dao.getFavoriteCitmOccupation(newUserId);
				newFavoratesCitmOccupationList.forEach(a-> uniqueCitmOccupation.add(a.getMcId()));
				

				oldFavoratesCitmOccupationList.forEach(a->{ if(!uniqueCitmOccupation.contains(a.getMcId())) 
														FavoriteCitmOccupationList.add(a);
													});
			
				if (FavoriteCitmOccupationList.size()>0)
					FavoriteCitmOccupationList.forEach(a-> {
						try {
							dao.insertFavoriteCitmOccupation(a);
						} catch (Exception e) {
							logger.error("FavoriteOccupationController Error: ", e);
						}
					});
			}
			if(oldFavoriteMilitaryServiceList.size()>0) {
				List<FavoriteMilitaryService> FavoriteMilitaryServiceList = new ArrayList<>();
				Set<String> uniqueMilitaryService = new HashSet<>();
				
				oldFavoriteMilitaryServiceList.forEach(a-> a.setUserId(newUserId));
				List<FavoriteMilitaryService> newFavoriteMilitaryServiceList  = dao.getFavoriteService(newUserId);
				newFavoriteMilitaryServiceList.forEach(a-> uniqueMilitaryService.add(a.getSvcId()));
				

				oldFavoriteMilitaryServiceList.forEach(a->{ if(!uniqueMilitaryService.contains(a.getSvcId())) 
													FavoriteMilitaryServiceList.add(a);
													});
			
				if (FavoriteMilitaryServiceList.size()>0)
					FavoriteMilitaryServiceList.forEach(a-> {
						try {
							dao.insertFavoriteService(a);
						} catch (Exception e) {
							logger.error("FavoriteOccupationController Error: ", e);
						}
					});
			}
		} catch (Exception e) {
			logger.error("FavoriteOccupationController Error: ", e);
		}
	}
}
