package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.PortfolioDAO;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Controller
@RequestMapping("/portfolio")
public class PortfolioController {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	UserManager userManager;
	@Autowired
	PortfolioDAO dao;

	@RequestMapping(value = "/PortfolioStarted", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> isPortfolioStarted() {
	    logger.info("isPortfolioStarted");
		Integer results;
		try {
			results = dao.isPortfolioStarted(userManager.getUserId()).size();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/IsRegistered", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Boolean> isRegistered() {
		Boolean results = userManager.isRegistered();

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/WorkExperience", method = RequestMethod.GET)
	public @ResponseBody ArrayList<WorkExperience> getWorkExperience() {
	    logger.info("getWorkExperience");
		return dao.getWorkExperience(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertWorkExperience", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WorkExperience> insertWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
	    logger.info("insertWorkExperience");
		int rowsInserted;
		workExperienceObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertWorkExperience(workExperienceObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected Response");
		    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateWorkExperience", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<WorkExperience> updateWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
		logger.info("updateWorkExperience");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateWorkExperience(workExperienceObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteWorkExperience/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteWorkExperience(@PathVariable Integer id) {
	    logger.info("deleteWorkExperience");
		int result;
		try {
			result = dao.deleteWorkExperience(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
		    logger.error("Unexpected Response");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Education", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Education> getEducation() {
	    logger.info("getEducation");
		return dao.getEducation(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertEducation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Education> insertEducation(@RequestBody Education educationObject) {
	    logger.info("insertEducation");
		int rowsInserted;
		educationObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertEducation(educationObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateEducation", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Education> updateEducation(@RequestBody Education educationObject) {
		logger.info("updateEducation");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateEducation(educationObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteEducation/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteEducation(@PathVariable Integer id) {
	    logger.info("deleteEducation");
		int result;
		try {
			result = dao.deleteEducation(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Achievement", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Achievement> getAchievements() {
	    logger.info("getAchievements");
		return dao.getAchievements(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertAchievement", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Achievement> insertAchievement(@RequestBody Achievement achievementObject) {
	    logger.info("insertAchievement");
		int rowsInserted;
		achievementObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertAchievement(achievementObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateAchievement", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Achievement> updateAchievement(@RequestBody Achievement achievementObject) {
		logger.info("updateAchievement");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateAchievement(achievementObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteAchievement/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAchievement(@PathVariable Integer id) {
	    logger.info("deleteAchievement");
		int result;
		try {
			result = dao.deleteAchievement(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Interest", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Interest> getInterests() {
	    logger.info("getInterests");
		return dao.getInterests(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertInterest", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Interest> insertInterest(@RequestBody Interest interestObject) {
	    logger.info("insertInterest");
		int rowsInserted;
		interestObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertInterest(interestObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(interestObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateInterest", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Interest> updateInterest(@RequestBody Interest interestObject) {
		logger.info("updateInterest");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateInterest(interestObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(interestObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteInterest/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteInterest(@PathVariable Integer id) {
	    logger.info("deleteInterest");
		int result;
		try {
			result = dao.deleteInterest(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Skill", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Skill> getSkills() {
	    logger.info("getSkills");
		return dao.getSkills(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertSkill", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Skill> insertSkill(@RequestBody Skill skillObject) {
	    logger.info("insertSkill");
		int rowsInserted;
		skillObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertSkill(skillObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(skillObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateSkill", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Skill> updateSkill(@RequestBody Skill skillObject) {
		logger.info("updateSkill");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateSkill(skillObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(skillObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteSkill/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSkill(@PathVariable Integer id) {
	    logger.info("deleteSkill");
		int result;
		try {
			result = dao.deleteSkill(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/WorkValue", method = RequestMethod.GET)
	public @ResponseBody ArrayList<WorkValue> getWorkValues() {
	    logger.info("getWorkValues");
		return dao.getWorkValues(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertWorkValue", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WorkValue> insertWorkValue(@RequestBody WorkValue workValueObject) {
	    logger.info("insertWorkValue");
		int rowsInserted;
		workValueObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertWorkValue(workValueObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workValueObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateWorkValue", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<WorkValue> updateWorkValue(@RequestBody WorkValue workValueObject) {
		logger.info("updateWorkValue");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateWorkValue(workValueObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workValueObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteWorkValue/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteWorkValue(@PathVariable Integer id) {
	    logger.info("deleteWorkValue");
		int result;
		try {
			result = dao.deleteWorkValue(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Volunteer", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Volunteer> getVolunteers() {
	    logger.info("getVolunteers");
		return dao.getVolunteers(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertVolunteer", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Volunteer> insertVolunteer(@RequestBody Volunteer volunteerObject) {
	    logger.info("insertVolunteer");
		int rowsInserted;
		volunteerObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertVolunteer(volunteerObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(volunteerObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateVolunteer", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Volunteer> updateVolunteer(@RequestBody Volunteer volunteerObject) {
		logger.info("updateVolunteer");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateVolunteer(volunteerObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(volunteerObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteVolunteer/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteVolunteer(@PathVariable Integer id) {
	    logger.info("deleteVolunteer");
		int result;
		try {
			result = dao.deleteVolunteer(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ASVAB", method = RequestMethod.GET)
	public @ResponseBody ArrayList<AsvabScore> getAsvabScore() {
	    logger.info("getAsvabScore");
		return dao.getAsvabScore(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertASVAB", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<AsvabScore> insertAsvabScore(@RequestBody AsvabScore asvabObject) {
	    logger.info("insertAsvabScore");
		int rowsInserted;
		asvabObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertAsvabScore(asvabObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateASVAB", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<AsvabScore> updateAsvabScore(@RequestBody AsvabScore asvabObject) {
		logger.info("updateAsvabScore");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateAsvabScore(asvabObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteASVAB/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAsvabScore(@PathVariable Integer id) {
	    logger.info("deleteAsvabScore");
		int result;
		try {
			result = dao.deleteAsvabScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ACT", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ActScore> getActScore() {
	    logger.info("getActScore");
		return dao.getActScore(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertACT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<ActScore> insertActScore(@RequestBody ActScore actObject) {
	    logger.info("insertActScore");
		int rowsInserted;
		actObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertActScore(actObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateACT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<ActScore> updateActScore(@RequestBody ActScore actObject) {
		logger.info("updateActScore");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateActScore(actObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteACT/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteActScore(@PathVariable Integer id) {
	    logger.info("deleteActScore");
		int result;
		try {
			result = dao.deleteActScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/SAT", method = RequestMethod.GET)
	public @ResponseBody ArrayList<SatScore> getSatScore() {
	    logger.info("getSatScore");
		return dao.getSatScore(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertSAT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<SatScore> insertSatScore(@RequestBody SatScore satObject) {
	    logger.info("insertSatScore");
		int rowsInserted;
		satObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertSatScore(satObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateSAT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<SatScore> updateSatScore(@RequestBody SatScore satObject) {
		logger.info("updateSatScore");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateSatScore(satObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteSAT/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSatScore(@PathVariable Integer id) {
	    logger.info("deleteSatScore");
		int result;
		try {
			result = dao.deleteSatScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/FYI", method = RequestMethod.GET)
	public @ResponseBody ArrayList<FyiScore> getFyiScore() {
	    logger.info("getFyiScore");
		return dao.getFyiScore(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertFYI", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FyiScore> insertFyiScore(@RequestBody FyiScore fyiObject) {
	    logger.info("insertFyiScore");
		int rowsInserted;
		fyiObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFyiScore(fyiObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateFYI", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<FyiScore> updateFyiScore(@RequestBody FyiScore fyiObject) {
		logger.info("updateFyiScore");
		int rowsUpdate;
		try {
			rowsUpdate = dao.updateFyiScore(fyiObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteFYI/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFyiScore(@PathVariable Integer id) {
	    logger.info("deleteFyiScore");
		int result;
		try {
			result = dao.deleteFyiScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Other", method = RequestMethod.GET)
	public @ResponseBody ArrayList<OtherScore> getOtherScore() {
	    logger.info("getOtherScore");
		return dao.getOtherScore(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertOther", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<OtherScore> insertOtherScore(@RequestBody OtherScore otherObject) {
	    logger.info("insertOtherScore");
		int rowsInserted;
		otherObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertOtherScore(otherObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(otherObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateOther", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<OtherScore> updateOtherScore(@RequestBody OtherScore otherObject) {
		logger.info("updateOtherScore");
		int rowsUpdate;
		try {
			rowsUpdate = dao.updateOtherScore(otherObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(otherObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteOther/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteOtherScore(@PathVariable Integer id) {
	    logger.info("deleteOtherScore");
		int result;
		try {
			result = dao.deleteOtherScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/Plan", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Plan> getPlan() {
	    logger.info("getPlan");
		return dao.getPlan(userManager.getUserId());
	}

	@RequestMapping(value = "/InsertPlan", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Plan> insertPlan(@RequestBody Plan planObject) {
	    logger.info("insertPlan");
		int rowsInserted;
		planObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertPlan(planObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(planObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdatePlan", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Plan> updatePlan(@RequestBody Plan planObject) {
		logger.info("updatePlan");
		int rowsUpdated;
		try {
			rowsUpdated = dao.updatePlan(planObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(planObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeletePlan/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deletePlan(@PathVariable Integer id) {
	    logger.info("deletePlan");
		int result;
		try {
			result = dao.deletePlan(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/SelectNameGrade", method = RequestMethod.GET)
	public @ResponseBody ArrayList<UserInfo> selectNameGrade() {
		logger.info("selectNameGrade");
		ArrayList<UserInfo> results = new ArrayList<>();
		try {
			results = dao.selectNameGrade(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/UpdateInsertUserInfo", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<UserInfo> updateInsertNameGrade(@RequestBody UserInfo userInfo) {
	    logger.info("updateInsertNameGrade");
		int rowsUpdated;
		userInfo.setUserId(userManager.getUserId());
		try {
			rowsUpdated = dao.updateInsertNameGrade(userInfo);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(userInfo, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-citm-favorite-occupation/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<com.cep.spring.model.portfolio.FavoriteOccupation>> getFavoriteOccupation() {
	    logger.info("getFavoriteOccupation");
		ArrayList<com.cep.spring.model.portfolio.FavoriteOccupation> results;
		try {
			results = dao.getFavoriteOccupation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-access-code/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getAccessCode() {
		logger.info("getAccessCode");
		String r = userManager.getUserAccessCode();
		return Collections.singletonMap("r", r);
	}
	
	/**
	 * Stores information on whether user has completed teacher demo.
	 */
	@RequestMapping(value = "/insert-teacher-demo-taken", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertTeacherDemoTaken() {
	    logger.info("insertTeacherDemoTaken");
		int rowsInserted;
		try {
			rowsInserted = dao.insertTeacherDemoTaken(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(rowsInserted, HttpStatus.OK);
		}
	}
	
	/**
	 * Returns information on whether user has completed teacher demo.
	 * @return returns if the user has taken the demo
	 */
	@RequestMapping(value = "/get-teacher-demo-taken", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getTeacherDemoTaken() {
		logger.info("getTeacherDemoTaken");
		Integer r = null;
		try {
			r = dao.getTeacherDemoTaken(userManager.getUserId());
		} catch (Exception e) {
			logger.error(e);
		}

		return Collections.singletonMap("r", r);
	}
	
	@RequestMapping(value = "/merge-portfolio/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void mergePortfolio(@PathVariable Integer oldUserId, @PathVariable Integer newUserId) {
		try {
			List<String> oldPortfolioList = dao.isPortfolioStarted(oldUserId);

			for (String portfolio: oldPortfolioList)
 			switch (portfolio) {
			case "PORTFOLIO_ACHIEVEMENT":
				List<Achievement> oldAchievements = dao.getAchievements(oldUserId);
				List<Achievement> newAchievements = dao.getAchievements(newUserId);
				oldAchievements.forEach(a-> a.setUserId(newUserId));
				for(Achievement achievementObject: oldAchievements) {
					if(!newAchievements.contains(achievementObject))
						dao.insertAchievement(achievementObject);
				}
				break;
			case "PORTFOLIO_EDUCATION":
				List<Education> oldEducations = dao.getEducation(oldUserId);
				List<Education> newEducations = dao.getEducation(newUserId);
				oldEducations.forEach(a-> a.setUserId(newUserId));
				for(Education educationObject: oldEducations) {
					if(!newEducations.contains(educationObject))
						dao.insertEducation(educationObject);
				}
				break;
			case "PORTFOLIO_INTEREST":
				List<Interest> oldInterests = dao.getInterests(oldUserId);
				List<Interest> newInterests = dao.getInterests(newUserId);
				oldInterests.forEach(a-> a.setUserId(newUserId));
				for(Interest interestObject: oldInterests) {
					if(!newInterests.contains(interestObject))
						dao.insertInterest(interestObject);
				}
				break;
			case "PORTFOLIO_SKILL":
				List<Skill> oldSkills = dao.getSkills(oldUserId);
				List<Skill> newSkills = dao.getSkills(newUserId);
				oldSkills.forEach(a-> a.setUserId(newUserId));
				for(Skill skillObject: oldSkills) {
					if(!newSkills.contains(skillObject))
					dao.insertSkill(skillObject);
				}
				break;
			case "PORTFOLIO_TEST_ACT":
				List<ActScore> oldActScores = dao.getActScore(oldUserId);
				List<ActScore> newActScores = dao.getActScore(newUserId);
				oldActScores.forEach(a-> a.setUserId(newUserId));
				for(ActScore actScoreObject: oldActScores) {
					if(!newActScores.contains(actScoreObject))
						dao.insertActScore(actScoreObject);
				}
				break;
			case "PORTFOLIO_TEST_ASVAB":
				List<AsvabScore> oldAsvabScores = dao.getAsvabScore(oldUserId);
				List<AsvabScore> newAsvabScores = dao.getAsvabScore(newUserId);
				oldAsvabScores.forEach(a-> a.setUserId(newUserId));
				for(AsvabScore asvabScoreObject: oldAsvabScores) {
					if(!newAsvabScores.contains(asvabScoreObject))
						dao.insertAsvabScore(asvabScoreObject);
				}
				break;
			case "PORTFOLIO_TEST_FYI":
				List<FyiScore> oldFyiScores = dao.getFyiScore(oldUserId);
				List<FyiScore> newFyiScores = dao.getFyiScore(newUserId);
				oldFyiScores.forEach(a-> a.setUserId(newUserId));
				for(FyiScore fyiScoreObject: oldFyiScores) {
					if(!newFyiScores.contains(fyiScoreObject))
						dao.insertFyiScore(fyiScoreObject);
				}
				break;
			case "PORTFOLIO_TEST_OTHER":
				List<OtherScore> oldOtherScores = dao.getOtherScore(oldUserId);
				List<OtherScore> newOtherScores = dao.getOtherScore(newUserId);
				oldOtherScores.forEach(a-> a.setUserId(newUserId));
				for(OtherScore otherScoreObject: oldOtherScores) {
					if(!newOtherScores.contains(otherScoreObject))
						dao.insertOtherScore(otherScoreObject);
				}
				break;
			case "PORTFOLIO_TEST_SAT":
				List<SatScore> oldSatScores = dao.getSatScore(oldUserId);
				List<SatScore> newSatScores = dao.getSatScore(newUserId);
				oldSatScores.forEach(a-> a.setUserId(newUserId));
				for(SatScore satScoreObject: oldSatScores) {
					if(!newSatScores.contains(satScoreObject))
						dao.insertSatScore(satScoreObject);
				}
				break;
			case "PORTFOLIO_VOLUNTEER":
				List<Volunteer> oldVolunteers = dao.getVolunteers(oldUserId);
				List<Volunteer> newVolunteers = dao.getVolunteers(newUserId);
				oldVolunteers.forEach(a-> a.setUserId(newUserId));
				for(Volunteer volunteerObject: oldVolunteers) {
					if(!newVolunteers.contains(volunteerObject))
						dao.insertVolunteer(volunteerObject);
				}
				break;		
			case "PORTFOLIO_WORK_EXPERIENCE":
				List<WorkExperience> oldWorkExperiences = dao.getWorkExperience(oldUserId);
				List<WorkExperience> newWorkExperiences = dao.getWorkExperience(newUserId);
				oldWorkExperiences.forEach(a-> a.setUserId(newUserId));
				for(WorkExperience workExperienceObject: oldWorkExperiences) {
					if(!newWorkExperiences.contains(workExperienceObject))
						dao.insertWorkExperience(workExperienceObject);
				}
				break;
			case "PORTFOLIO_WORK_VALUE":
				List<WorkValue> oldWorkValues = dao.getWorkValues(oldUserId);
				List<WorkValue> newWorkValues = dao.getWorkValues(newUserId);
				oldWorkValues.forEach(a-> a.setUserId(newUserId));
				for(WorkValue workValueObject: oldWorkValues) {
					if(!newWorkValues.contains(workValueObject))
						dao.insertWorkValue(workValueObject);
				}
				break;					
			case "PORTFOLIO_PLANS":
				Set<String> uniquePlans = new HashSet<>();
				List<Plan> oldPlans = dao.getPlan(oldUserId);
				List<Plan> newPlans = dao.getPlan(newUserId);
				oldPlans.forEach(a->a.setUserId(newUserId));
				newPlans.forEach(a->uniquePlans.add(a.getPlan()));
				for(int i = 0; i < oldPlans.size(); i++) {
					if(uniquePlans.contains(oldPlans.get(i).getPlan())) {
						for(Plan PlanObject: newPlans) {
							if(oldPlans.get(i).getPlan().contains(PlanObject.getPlan())){
								PlanObject.setDescription(PlanObject.getDescription()+", " + oldPlans.get(i).getDescription());
								dao.updatePlan(PlanObject);
							}
						}
					} else {
						dao.insertPlan(oldPlans.get(i));
					}
				}
				break;	

			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@RequestMapping(value = "/copy-portfolio/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void copyPortfolio(@PathVariable Integer oldUserId, @PathVariable Integer newUserId) {
		try {
			List<String> oldPortfolioList = dao.isPortfolioStarted(oldUserId);

			StringBuffer sb = new StringBuffer();
			for (String portfolio: oldPortfolioList)
			switch (portfolio) {
			case "PORTFOLIO_ACHIEVEMENT":
				List<Achievement> oldAchievements = dao.getAchievements(oldUserId);
				oldAchievements.forEach(a-> a.setUserId(newUserId));
				for(Achievement achievementObject: oldAchievements)
					dao.updateAchievement(achievementObject);
				break;
			case "PORTFOLIO_EDUCATION":
				List<Education> oldEducations = dao.getEducation(oldUserId);
				oldEducations.forEach(a-> a.setUserId(newUserId));
				for(Education educationObject: oldEducations) 
					dao.updateEducation(educationObject);
				break;
			case "PORTFOLIO_INTEREST":
				List<Interest> oldInterests = dao.getInterests(oldUserId);
				oldInterests.forEach(a-> a.setUserId(newUserId));
				for(Interest interestObject: oldInterests) 
					dao.updateInterest(interestObject);
				break;
			case "PORTFOLIO_SKILL":
				List<Skill> oldSkills = dao.getSkills(oldUserId);
				oldSkills.forEach(a-> a.setUserId(newUserId));
				for(Skill skillObject: oldSkills) 
					dao.updateSkill(skillObject);
				break;
			case "PORTFOLIO_TEST_ACT":
				List<ActScore> oldActScores = dao.getActScore(oldUserId);
				oldActScores.forEach(a-> a.setUserId(newUserId));
				for(ActScore actScoreObject: oldActScores) 
					dao.updateActScore(actScoreObject);
				break;
			case "PORTFOLIO_TEST_ASVAB":
				sb.setLength(0);
				List<AsvabScore> oldAsvabScores = dao.getAsvabScore(oldUserId);
				oldAsvabScores.forEach(a-> a.setUserId(newUserId));
				for(AsvabScore asvabScoreObject: oldAsvabScores) 
					dao.updateAsvabScore(asvabScoreObject);
				break;
			case "PORTFOLIO_TEST_FYI":
				List<FyiScore> oldFyiScores = dao.getFyiScore(oldUserId);
				oldFyiScores.forEach(a-> a.setUserId(newUserId));
				for(FyiScore fyiScoreObject: oldFyiScores)
					dao.updateFyiScore(fyiScoreObject);
				break;
			case "PORTFOLIO_TEST_OTHER":
				List<OtherScore> oldOtherScores = dao.getOtherScore(oldUserId);
				oldOtherScores.forEach(a-> a.setUserId(newUserId));
				for(OtherScore otherScoreObject: oldOtherScores)
					dao.updateOtherScore(otherScoreObject);
				break;
			case "PORTFOLIO_TEST_SAT":
				List<SatScore> oldSatScores = dao.getSatScore(oldUserId);
				oldSatScores.forEach(a-> a.setUserId(newUserId));
				for(SatScore satScoreObject: oldSatScores) 
					dao.updateSatScore(satScoreObject);
				break;
			case "PORTFOLIO_VOLUNTEER":
				List<Volunteer> oldVolunteers = dao.getVolunteers(oldUserId);
				oldVolunteers.forEach(a-> a.setUserId(newUserId));
				for(Volunteer volunteerObject: oldVolunteers) 
					dao.updateVolunteer(volunteerObject);
				break;		
			case "PORTFOLIO_WORK_EXPERIENCE":
				List<WorkExperience> oldWorkExperiences = dao.getWorkExperience(oldUserId);
				oldWorkExperiences.forEach(a-> a.setUserId(newUserId));
				for(WorkExperience workExperienceObject: oldWorkExperiences)
					dao.updateWorkExperience(workExperienceObject);
				break;
			case "PORTFOLIO_WORK_VALUE":
				List<WorkValue> oldWorkValues = dao.getWorkValues(oldUserId);
				oldWorkValues.forEach(a-> a.setUserId(newUserId));
				for(WorkValue workValueObject: oldWorkValues) 
					dao.updateWorkValue(workValueObject);
				break;					
			case "PORTFOLIO_PLANS":
				List<Plan> oldPlans = dao.getPlan(oldUserId);
				oldPlans.forEach(a-> a.setUserId(newUserId));
				for(Plan planObject: oldPlans)
					dao.updatePlan(planObject);
				break;			
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
