package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.service.DbInfoService;
import com.cep.spring.model.DbInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.cep.spring.dao.mybatis.FlickrApiDAO;
import com.cep.spring.model.optionready.FlickrPhotoInfo;

@RestController
@RequestMapping("/flickr")
public class FlickrServiceController {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	private FlickrApiDAO dao;

	@Autowired
	private DbInfoService dbInfoService;
	
	@RequestMapping(value = "/get-public-photos", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<FlickrPhotoInfo>> getFlickrGalleries() {
		// if debugging turned on?
		DbInfo dbInfoIsDebugging = dbInfoService.getDbInfo("is_debugging_on");
		boolean isDebugging = false;
		if (dbInfoIsDebugging != null) {
			isDebugging = Boolean.valueOf(dbInfoIsDebugging.getValue());
		}

		List<FlickrPhotoInfo> results = null;
		try {
			results = dao.getPhotoCollection(isDebugging);
		} catch (Exception e) {
			// log error
			logger.error("error: " + e.getMessage());
			return new ResponseEntity<List<FlickrPhotoInfo>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<FlickrPhotoInfo>>(results, HttpStatus.OK);
	}	
}