package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.NotesDAOImpl;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;
import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;
import com.cep.spring.model.notes.SchoolNotes;

@Controller
@RequestMapping("/notes")
public class NotesController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	NotesDAOImpl dao;
	@Autowired
	UserManager userManager;

	@RequestMapping(value = "/getNotes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Notes>> getNotes() {
	    logger.info("getNotes");
		ArrayList<Notes> results;
		Integer userId = userManager.getUserId();
		try {
			results = dao.getNotes(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<Notes>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<Notes>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertNote", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Notes> insertNote(@RequestBody Notes noteObject) {
	    logger.info("insertNote");
		int rowsInserted = 0;
		noteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Notes>(noteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateNote", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Notes> updateNote(@RequestBody Notes noteObject) {
	    logger.info("updateNote");
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Notes>(noteObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteNote/{noteId}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteNote(@PathVariable Integer noteId) {
		int rowsAffected = 0;
		Integer userId = userManager.getUserId();
		try {
			rowsAffected = dao.deleteNote(userId, noteId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		if (rowsAffected > 0) {
			return new ResponseEntity<Integer>(rowsAffected, HttpStatus.OK);
		}
		return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/getOccupationNotes/{socId}/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getOccupationNotes(@PathVariable String socId) {
	    logger.info("getOccupationNotes");
		String note = null;
		Integer userId = userManager.getUserId();
		try {
			note = dao.getOccupationNotes(userId, socId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		
		return Collections.singletonMap("note", note);
	}
	
	@RequestMapping(value = "/getCareerClusterNotes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<CareerClusterNotes>> getCareerClusterNotes() {
		ArrayList<CareerClusterNotes> results;
		Integer userId = userManager.getUserId();
		try {
			results = dao.getCareerClusterNotes(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<CareerClusterNotes>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<CareerClusterNotes>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertCareerClusterNote", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<CareerClusterNotes> insertCareerClusterNote(@RequestBody CareerClusterNotes noteObject) {
		int rowsInserted = 0;
		noteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertCareerClusterNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<CareerClusterNotes>(noteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateCareerClusterNote", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<CareerClusterNotes> updateCareerClusterNote(@RequestBody CareerClusterNotes noteObject) {
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateCareerClusterNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<CareerClusterNotes>(noteObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteCareerClusterNote/{ccNoteId}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteCareerClusterNote(@PathVariable Integer ccNoteId) {
		int rowsAffected = 0;
		Integer userId = userManager.getUserId();
		try {
			rowsAffected = dao.deleteCareerClusterNote(userId, ccNoteId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		if (rowsAffected > 0) {
			return new ResponseEntity<Integer>(rowsAffected, HttpStatus.OK);
		}
		return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/getCareerClusterNote/{ccId}/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getCareerClusterNote(@PathVariable Integer ccId) {
		String note = null;
		Integer userId = userManager.getUserId();
		try {
			note = dao.getCareerClusterNote(userId, ccId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		
		return Collections.singletonMap("note", note);
	}
	
	
	
	
	@RequestMapping(value = "/getSchoolNotes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<SchoolNotes>> getSchoolNotes() {
	    logger.info("getSchoolNotes");
		ArrayList<SchoolNotes> results;
		Integer userId = userManager.getUserId();
		try {
			results = dao.getSchoolNotes(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<SchoolNotes>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<SchoolNotes>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertSchoolNote", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<SchoolNotes> insertSchoolNote(@RequestBody SchoolNotes noteObject) {
	    logger.info("insertSchoolNote");
		int rowsInserted = 0;
		noteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertSchoolNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<SchoolNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<SchoolNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<SchoolNotes>(noteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateSchoolNote", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<SchoolNotes> updateSchoolNote(@RequestBody SchoolNotes noteObject) {
	    logger.info("updateSchoolNote");
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateSchoolNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<SchoolNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<SchoolNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<SchoolNotes>(noteObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteSchoolNote/{schoolNoteId}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSchoolNote(@PathVariable Integer schoolNoteId) {
		int rowsAffected = 0;
		Integer userId = userManager.getUserId();
		try {
			rowsAffected = dao.deleteSchoolNote(userId, schoolNoteId);
		} catch (Exception e) {
			logger.error("Error",e);
		}
		if (rowsAffected > 0) {
			return new ResponseEntity<Integer>(rowsAffected, HttpStatus.OK);
		}
		return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/getSchoolNote/{unitId}/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getSchoolNote(@PathVariable Integer unitId) {
	    logger.info("getSchoolNote");
		String note = null;
		Integer userId = userManager.getUserId();
		try {
			note = dao.getSchoolNote(userId, unitId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		
		return Collections.singletonMap("note", note);
	}
	
	@RequestMapping(value = "/merge-notes/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void mergeNotes(@PathVariable int oldUserId, @PathVariable int newUserId) {
		try {
			
			List<Notes> oldCareerNotesList = dao.getNotes(oldUserId);
			List<SchoolNotes> oldSchoolNotesList  = dao.getSchoolNotes(oldUserId);
			
			if (oldCareerNotesList.size()>0) {
				List<Notes> careerNotesList = new ArrayList<>();
				List<Notes> updateCareerNotesList = new ArrayList<>();
				Set<String> uniqueCareerNotes = new HashSet<>();
				
				
				oldCareerNotesList.forEach(a-> a.setUserId(newUserId));
				List<Notes> newFavoratesCareerClusterList = dao.getNotes(newUserId);
				newFavoratesCareerClusterList.forEach(a-> uniqueCareerNotes.add(a.getSocId()));
				

				oldCareerNotesList.forEach(a-> { 
					if(!uniqueCareerNotes.contains(a.getSocId())) 
						careerNotesList.add(a);
					else {
						newFavoratesCareerClusterList.forEach(b->{
							if(b.getSocId().equals(a.getSocId())) {
								b.setNotes(b.getNotes() +", "+ a.getNotes());
								updateCareerNotesList.add(b);
							}
						});
					}
				});
				
				if (careerNotesList.size()>0)
					careerNotesList.forEach(a-> {
						try {
							dao.insertNote(a);
							} catch (Exception e) {
								logger.error("NotesController Error: ", e);
							}
					});
				if(updateCareerNotesList.size() >0)
					updateCareerNotesList.forEach(a-> {
						try {
							dao.updateNote(a);
							} catch (Exception e) {
								logger.error("NotesController Error: ", e);
							}
					});
			}
			if(oldSchoolNotesList.size()>0) {
				List<SchoolNotes> schoolNotesList = new ArrayList<>();
				List<SchoolNotes> updateSchoolNotesList = new ArrayList<>();
				Set<String> uniqueSchoolNotes = new HashSet<>();
				
				oldSchoolNotesList.forEach(a-> a.setUserId(newUserId));
				List<SchoolNotes> newFavoratesOccupationList  = dao.getSchoolNotes(newUserId);
				newFavoratesOccupationList.forEach(a-> uniqueSchoolNotes.add(a.getSchoolName()));

				oldSchoolNotesList.forEach(a->{ 
					if(!uniqueSchoolNotes.contains(a.getSchoolName())) 
						schoolNotesList.add(a);
					else {
						newFavoratesOccupationList.forEach(b->{
							if(b.getSchoolName().equals(a.getSchoolName())) {
								b.setNotes(b.getNotes() +", "+ a.getNotes());
								updateSchoolNotesList.add(b);
							}		
						});
					}
				});

				if (schoolNotesList.size()>0)
					schoolNotesList.forEach(a-> {
						try {
							dao.insertSchoolNote(a);
						} catch (Exception e) {
							logger.error("NotesController Error: ", e);
						}
					});
				if(updateSchoolNotesList.size() >0)
					updateSchoolNotesList.forEach(a-> {
						try {
							dao.updateSchoolNote(a);
						} catch (Exception e) {
							logger.error("NotesController Error: ", e);
						}
					});
			}

		} catch (Exception e) {
			logger.error("NotesController Error: ", e);
		}
	}

}
