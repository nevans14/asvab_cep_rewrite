package com.cep.spring.mvc.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cep.spring.dao.jdbctemplate.AccessCodesDao;
import com.cep.spring.dao.jdbctemplate.GenericRowMapper;
import com.cep.spring.model.AccessCodes;
import com.cep.spring.utils.SimpleUtils;



@Controller
@RequestMapping("/CEP/reports")
public class ReportController {
		private Logger logger = LogManager.getLogger(this.getClass().getName());
		
		@Autowired 
		JdbcTemplate jdbcTemplate;
		@Autowired
		AccessCodesDao acd;
		
		@RequestMapping(value = "/SurveyResponse", method = RequestMethod.GET)
		public @ResponseBody
		List<Map<String, String>> surveyResponse() {
		    logger.info("surveyResponse");

			String sql = "SELECT Response_ID, " +
							"userid, " +
							"role,  " +
							"roleOther, " +
							"valuable, " +
							"feature, " +
							"badFeature, " +
							"navigation, " +
							"perception, " +
							"computer, " +
							"location, " +
							"CompletedOn, " +
							"AccessCode, " +
							"browser, " +
							"OS " +
							"FROM SurveyResponse " +
							"ORDER BY Response_ID ASC";
			logger.debug(sql);
			List<Map<String, String>> rowMapList =  jdbcTemplate.query(sql, new GenericRowMapper());

			
			return rowMapList;

		}
		
		@RequestMapping(value = "/SurveyPerceptionReport", method = RequestMethod.GET)
		public @ResponseBody
		List<Map<String, String>> surveyPerceptionReport() {
		    logger.info("surveyPerceptionReport");
			String sql = "SELECT " +
							" userid,  " +
							" role, " +
							" perception  " +
							" FROM surveyResponse  " +
							" WHERE perception NOT LIKE 'Enter your feedback here%'";
            logger.debug(sql);
			List<Map<String, String>> rowMapList =  jdbcTemplate.query(sql, new GenericRowMapper());

			return rowMapList;

		}
		
		@RequestMapping(value = "/InterestCodeRecordCounts", method = RequestMethod.GET)
		public @ResponseBody
		List<Map<String, String>> interestCodeRecordCounts() {
		    logger.info("interestCodeRecordCounts");
			final String [] CODE_LETTERS = {"R", "I", "A", "S", "E","C"};
			String primarySql = "SELECT count(*) FROM onet_detail WHERE interest_code1 LIKE ?";
			String secondarySql = "SELECT count(*) FROM onet_detail WHERE interest_code2 LIKE ?";
			
			List<Map<String, String>> returnTable = new ArrayList<>();
			for (String interestCode : CODE_LETTERS ) {
				Map<String, String> rowMap = new HashMap<>();
				rowMap.put("interestCode", interestCode.toString());
				Long primaryCount = (Long) jdbcTemplate.queryForObject(primarySql,  Long.class, interestCode );
				rowMap.put("primary", primaryCount.toString());
				Long secondaryCount = (Long) jdbcTemplate.queryForObject(secondarySql,  Long.class, interestCode );
				rowMap.put("secondary", secondaryCount.toString());
				returnTable.add(rowMap);
			}
			return returnTable;
		}
				
		

				
		@RequestMapping(value = "/FYIResults/{startDate}/{endDate}", method = RequestMethod.GET)
		public @ResponseBody
		List<Map<String, String>> fyiResults(@PathVariable("startDate") String startDateFromAngular,
											 @PathVariable("endDate") String endDateFromAngular) {
		    logger.info("fyiResults");
		// yyyy-MM-dd 
		String startDate = "### 00:00:00.0".replace("###", startDateFromAngular); 
		String endDate	 = "### 00:00:00.0".replace("###", endDateFromAngular); ;
			
		String primarySql = 
	      "SELECT TOP 5000 " + 
	      " fyi_user_tests.DATESTAMP, " +
	      " fyi_user_tests.TEST_ID, " +
	      " fyi_user_tests.USER_ID, " +
	      " fyi_user_results.COMBINED_SCORE, " +
	      " fyi_user_results.GENDER_SCORE, " +
	      " fyi_user_results.INTEREST_CODE, " + 
	      " fyi_user_results.RAW_SCORE, " +
	      " fyi_user_results.TEST_ID, " +
	      " acc.access_code, " +
	      " acc.expire_date, " +
	      " pro.gender " +
	      " FROM fyi_user_tests " +
	      " LEFT JOIN  fyi_user_results ON fyi_user_tests.TEST_ID = fyi_user_results.TEST_ID    " +
	      " LEFT JOIN  access_codes acc ON acc.USER_ID = fyi_user_tests.USER_ID    " +
	      " LEFT JOIN  fyi_user_profile pro ON pro.USER_ID = fyi_user_tests.USER_ID    " +
	      " WHERE  " +
	      " fyi_user_tests.DATESTAMP BETWEEN ? AND ? " +
	      " ORDER BY  fyi_user_tests.TEST_ID ASC"; 

		  logger.debug(primarySql);
		    
			List<Map<String, String>> rowMapList =  
			jdbcTemplate.query(primarySql, 
					new Object[] { startDate, endDate }, 
					new GenericRowMapper());
		 
			return rowMapList;
		}
				
		/* url -  localhost:8080/CEP/rest/CEP/reports/upload   */ 
		@RequestMapping(value="/upload", method=RequestMethod.POST)
	    public @ResponseBody returnCode handleFileUpload( 
	            @RequestParam("fileToUpload") MultipartFile file, 
	            @RequestParam("role") String role,
	            @RequestParam("expireDate") String  expireDate){
		    
		    logger.info("handleFileUpload");
		    logger.info("role:" + role);
		    logger.info("expireDate" + "expireDate 00:00:00.000");
			List<AccessCodes> lc = new ArrayList<>();
	         String name = "test11";
	        Date dayExpires = SimpleUtils.getDateFromStringInStandardFormat(expireDate + " 00:00:00.000");
	        if (!file.isEmpty()) {
	            try {
	         
	                byte[] bytes = file.getBytes();
	                
	                InputStream myInputStream = new ByteArrayInputStream(bytes); 

	                BufferedReader br=new BufferedReader(new InputStreamReader(myInputStream));
	                
	                String thisLine = "";
	                Integer numberOfAccessCodes = 0;
	                while ((thisLine = br.readLine()) != null) {
	                	numberOfAccessCodes++;
	                    System.out.println(thisLine);
	                    AccessCodes ac = new AccessCodes();
	                    ac.setAccessCode(thisLine.trim());
	                    ac.setExpireDate(dayExpires );
	                    ac.setRole(role);
	                    lc.add(ac);
	                 }   	                
	                
	                
	           //     acd.insertBatch(lc);
	                
	                System.out.println("Total Access Codes Read:" + numberOfAccessCodes );
	                System.out.println("Size of access Code list:" + lc.size()  );
	                
	                acd.insertBatch(lc);
	                
// write file to os ...	                
//	                BufferedOutputStream stream = 
//	                        new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
//	                stream.write(bytes);
//	                stream.close();
	                return new returnCode("[returnCode:You successfully uploaded " + name + " into " + name + "-uploaded ]");
	            } catch (Exception e) {
	                return new returnCode("[returnCode:You failed to upload " + name + " => " + e.getMessage() + "]");
	            }
	        } else {
	            return new returnCode("[returnCode:You failed to upload " + name + " because the file was empty.]");
	        }
	    }
		
		public class returnCode {
			private String retCode = "";

			
			returnCode(String code){
				this.retCode = code;
			}
			
			
			public String getRetCode() {
				return retCode;
			}

			public void setRetCode(String retCode) {
				this.retCode = retCode;
			}
			
		}
	
}
