package com.cep.spring.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.EssTrainingDAOImpl;
import com.cep.spring.model.EssTraining.EssTraining;
import com.cep.spring.model.EssTraining.EssTrainingRegistration;
import com.cep.spring.model.EssTraining.EssTrainingRegistrationResults;
import com.cep.spring.CEPMailer;

@Controller
@RequestMapping("/ess-training")
public class EssTrainingController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	EssTrainingDAOImpl dao;
	@Autowired
	CEPMailer mailer;
	
	@RequestMapping(value = "/get-ess-training-by-id/{essTrainingId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<EssTraining> getEssTrainingById(@PathVariable int essTrainingId) {
		logger.info("fetching ESS Training info...");
		
		EssTraining results = new EssTraining();
		
		try {
			results = dao.getEssTrainingById(essTrainingId);
		} catch (Exception e) {
			return new ResponseEntity<EssTraining>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<EssTraining>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/add-ess-training-registration", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<EssTrainingRegistrationResults> addEssTrainingRegistration(@RequestBody EssTrainingRegistration essTrainingRegistration) {
		logger.info("adding new ESS Training Registration.");
		EssTrainingRegistrationResults results = new EssTrainingRegistrationResults();
		// check if the user has already registered for this training
		if (dao.isUserRegistered(essTrainingRegistration.getRegistrationEmailAddress(), essTrainingRegistration.getEssTrainingId()) > 0) {
			results.setResultCode(409); // Conflict
			results.setResultMessage("The email address " + essTrainingRegistration.getRegistrationEmailAddress() + " has already been registered for this training course.");
			return new ResponseEntity<EssTrainingRegistrationResults>(results, HttpStatus.CONFLICT);
					
		}
		
		int rowsAffected = 0;
		
		try {
			rowsAffected = dao.addEssTrainingRegistration(essTrainingRegistration);
			
			if (rowsAffected > 0) {
				results.setResultCode(200);
				results.setResultMessage("Email has been registered!");	
				mailer.sendEssTrainingRegistrationConfirmationEmail(essTrainingRegistration.getRegistrationEmailAddress(), essTrainingRegistration.getEssTrainingId());
			} else {
				results.setResultCode(500);
				results.setResultMessage("Registration was not successful. Please try again later.");
			}
		} catch (Exception e) {
			logger.error("error while adding ESS Training Registration: " + e.getMessage());
			results.setResultCode(500);
			results.setResultMessage("An error occurred while submitting your registration. Please try again later.");
			return new ResponseEntity<EssTrainingRegistrationResults>(results, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<EssTrainingRegistrationResults>(results, HttpStatus.OK);
	}
}