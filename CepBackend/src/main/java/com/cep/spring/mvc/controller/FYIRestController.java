package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

@Controller
@RequestMapping("/fyi")
public class FYIRestController {
	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	FindYourInterestDAO fyiDao;

	@Autowired
	UserManager userManager;

	@RequestMapping(value = "{name}", method = RequestMethod.GET)
	public @ResponseBody List<FYIQuestion> getFYIQuesitonInJSON(@PathVariable String name) {
		logger.info("getFYIQuesitonInJSON");
		List<FYIQuestion> fyiQuestions = fyiDao.getFYIQuestions();

		return fyiQuestions;
	}

	@RequestMapping(value = "/saveAnswerJson", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> saveAnswerJson(@RequestBody FindYourInterestProfile fyiObject) {
		logger.info("saveAnswerJson");
		fyiObject.setUserId(userManager.getUserId());
		boolean allInserted = fyiDao.insertProfileAndScores(fyiObject);

		if (!allInserted) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getGenderScore/{gender}/", method = RequestMethod.POST)
	public @ResponseBody List<GenderScore> getGenderScore(@RequestBody FYIRawScore rawScores,
			@PathVariable char gender) {
		logger.info("getGenderScore");
		logger.info("gender : " + gender);
		List<GenderScore> genderScores = fyiDao.getGenderScore(rawScores, gender);

		return genderScores;
	}

	@RequestMapping(value = "/getCombinedScore", method = RequestMethod.POST)
	public @ResponseBody List<CombinedScore> getCombinedScore(@RequestBody FYIRawScore rawScores) {
		logger.info("getCombinedScore");
		List<CombinedScore> results = fyiDao.getCombinedScore(rawScores);

		return results;
	}

	@RequestMapping(value = "/NumberTestTaken", method = RequestMethod.GET)
	public @ResponseBody Integer getNumberTestTaken() {
		logger.info("getNumberTestTaken");
		Integer userId = userManager.getUserId();
		Integer results = fyiDao.getNumberTestTaken(fyiDao.getOldUserId(userId), userId);
		return results;
	}

	@RequestMapping(value = "/CombinedAndGenderScores", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Scores> getCombinedAndGenderScores() {
		logger.info("getCombinedAndGenderScores");
		Integer userId = userManager.getUserId();
		ArrayList<Scores> results = fyiDao.getCombinedAndGenderScore(userId);

		return results;
	}

	@RequestMapping(value = "/PostScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> postScore(@RequestBody ScoreChoice scoreChoice) {
		logger.info("postScore");
		scoreChoice.setUserId(userManager.getUserId());
		int rowUpdated = fyiDao.updateProfile(scoreChoice);
		if (rowUpdated != 1) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UserInterestCodes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<ScoreChoice>> getProfile() {
		logger.info("getProfile");
		Integer userId = userManager.getUserId();
		ArrayList<ScoreChoice> userInterestCodes = fyiDao.getProfile(userId);
		int size = userInterestCodes.size();
		if (size > 1) {
			return new ResponseEntity<ArrayList<ScoreChoice>>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<ArrayList<ScoreChoice>>(userInterestCodes, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/InsertProfile", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> insertProfile(@RequestBody FindYourInterestProfile fyiObject) {
		logger.info("insertProfile");
		int numRowsInserted = 0;
		fyiObject.setUserId(userManager.getUserId());
		try {
			numRowsInserted = fyiDao.insertProfile(fyiObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		if (numRowsInserted < 1) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/CheckProfileExists", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getProfileExist() {
		logger.info("getProfileExist");
		Integer userId = userManager.getUserId();
		int numRowsSelected = 0;
		try {
			numRowsSelected = fyiDao.getProfileExist(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Integer>(numRowsSelected, HttpStatus.OK);
	}

	@RequestMapping(value = "/ScoreChoice", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getScoreChoice() {
		logger.info("getScoreChoice");
		String scoreChoice = null;
		Integer userId = userManager.getUserId();
		try {
			scoreChoice = fyiDao.getScoreChoice(userId);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return Collections.singletonMap("scoreChoice", scoreChoice);
	}

	@RequestMapping(value = "/get-tied-selection", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<TiedSelection> getTiedSelection() {
		logger.info("getTiedSelection");
		TiedSelection result = new TiedSelection();
		Integer userId = userManager.getUserId();
		try {
			result = fyiDao.getTiedSelection(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<TiedSelection>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<TiedSelection>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/insert-tied-selection", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> insertTiedSelection(@RequestBody TiedSelection tiedObject) {
		logger.info("insertTiedSelection");
		int numRowsInserted = 0;
		tiedObject.setUserId(userManager.getUserId());
		try {
			numRowsInserted = fyiDao.insertTiedSelection(tiedObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		if (numRowsInserted < 1) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/merge-account/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public @ResponseBody void mergeAccount(@PathVariable Integer oldUserId, @PathVariable Integer newUserId, FYIMerge fyiObject) {

			if(fyiObject != null && fyiObject.getUserId().intValue() != newUserId.intValue()) {

				fyiDao.mergeProfile(oldUserId, newUserId);
				fyiObject.setUserId(newUserId);
				fyiDao.mergeTests(fyiObject);
				
				fyiDao.mergeResults(oldUserId, newUserId);
				fyiDao.mergeAnswers(oldUserId, newUserId);

			}
	}
}