package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.OccufindDAOImpl;
import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.occufind.cip;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;

@Controller
@RequestMapping("/occufind")
public class OccufindRestController {
	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	OccufindDAOImpl occufindDao;

	@RequestMapping(value = "/getOccufindSearch", method = RequestMethod.POST)
	public @ResponseBody List<OccufindSearch> getOccufindSearch(@RequestBody OccufindSearchArguments userSearch) {
		logger.info("getOccufindSearch");
		// append wild card character to search string
		String userSearchString = userSearch.getUserSearchString();
		if (!StringUtils.isEmpty(userSearchString)) {
			userSearch.setUserSearchString("%" + userSearchString + "%");
		}

		List<OccufindSearch> results = occufindDao.getOccufindSearch(userSearch);

		return results;
	}

	@RequestMapping(value = "/certificateExplanation/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody CertificateExplanation getCertificateExplanation(@PathVariable String onetSoc) {
		logger.info("getCertificateExplanation");
		CertificateExplanation certificateExplanation = occufindDao.getCertificateExplanation(onetSoc);

		return certificateExplanation;
	}
	
	@RequestMapping(value = "/alternateTitles/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<AlternateTitles> getAlternateTitles(@PathVariable String onetSoc) {
		logger.info("getCertificateExplanation");
		List<AlternateTitles> alternateTitles = occufindDao.getAlternateTitles(onetSoc);

		return alternateTitles;
	}
	
	@RequestMapping(value = "/occupationTitleDescription/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody TitleDescription getOccufindTitleDescription(@PathVariable String onetSoc) {
		logger.info("getOccufindTitleDescription");
		TitleDescription occufindResults = occufindDao.getOccupationTitleDescription(onetSoc);
		// String response = "{\"description\":\"" + occufindResults + "\"}";

		return occufindResults;
	}

	@RequestMapping(value = "/occupationASK/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody Occupation getOccufindASK(@PathVariable String onetSoc) {
		logger.info("getOccufindASK");
		List<Occupation> occufindResults = occufindDao.getOccupationASK(onetSoc);
		Occupation occupationASKDetails = occufindResults.get(0);

		return occupationASKDetails;
	}

	@RequestMapping(value = "/militaryCareerResource/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<MilitaryCareerResource> getMilitaryCareerResources(@PathVariable String onetSoc) {
		logger.info("getMilitaryCareerResources");
		List<MilitaryCareerResource> occufindResults = occufindDao.getMilitaryCareerResources(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/OOHResource/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<OOHResource> getOOHResources(@PathVariable String onetSoc) {
		logger.info("getOOHResources");
		List<OOHResource> occufindResults = occufindDao.getOOHResources(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/StateSalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody List<StateSalary> getAvgSalaryByState(@PathVariable String onetSocTrimmed) {
		logger.info("getAvgSalaryByState");
		onetSocTrimmed = onetSocTrimmed + "%";
		List<StateSalary> occufindResults = occufindDao.getAvgSalaryByState(onetSocTrimmed);

		return occufindResults;
	}
	
	@RequestMapping(value = "/StateEntrySalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody List<StateSalaryEntry> getEntrySalaryByState(@PathVariable String onetSocTrimmed) {
		logger.info("getEntrySalaryByState");
		onetSocTrimmed += "%";
		
		List<StateSalaryEntry> occufindResults = occufindDao.getEntrySalaryByState(onetSocTrimmed);
		
		return occufindResults;
	}

	@RequestMapping(value = "/NationalSalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody Integer getNationalSalary(@PathVariable String onetSocTrimmed) {
		logger.info("getNationalSalary");
		Integer occufindResults = occufindDao.getNationalSalary(onetSocTrimmed);

		return occufindResults;
	}
	
	@RequestMapping(value = "/NationalEntrySalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody Integer getNationalEntrySalary(@PathVariable String onetSocTrimmed) {
		logger.info("getNationalEntrySalary");
		Integer occufindResults = occufindDao.getNationalEntrySalary(onetSocTrimmed);

		return occufindResults;
	}

	@RequestMapping(value = "/BLSTitle/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getBLSTitle(@PathVariable String onetSocTrimmed) {
		logger.info("getBLSTitle");
		String blsTitle = null;
		try {
			blsTitle = occufindDao.getBLSTitle(onetSocTrimmed);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error BLSTitle", e);
		}

		return Collections.singletonMap("blsTitle", blsTitle);
	}

	@RequestMapping(value = "/OccupationInterestCodes/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<OccupationInterestCode> getOccupationInterestCodes(@PathVariable String onetSoc) {
		logger.info("getOccupationInterestCodes");
		List<OccupationInterestCode> occufindResults = occufindDao.getOccupationInterestCodes(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/HotGreenStemFlags/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody HotGreenStem getHotGreenStemFlags(@PathVariable String onetSoc) {
		logger.info("getHotGreenStemFlags");
		HotGreenStem occufindResults = occufindDao.getHotGreenStemFlags(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/ServiceOfferingCareer/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<ServiceOfferingCareer> getServiceOfferingCareer(@PathVariable String onetSoc) {
		logger.info("getServiceOfferingCareer");
		List<ServiceOfferingCareer> occufindResults = occufindDao.getServiceOfferingCareer(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/RelatedCareerCluster/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<RelatedCareerCluster> getRelatedCareerCluster(@PathVariable String onetSoc) {
		logger.info("getRelatedCareerCluster");
		List<RelatedCareerCluster> occufindResults = occufindDao.getRelatedCareerCluster(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/SchoolMoreDetails/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<SchoolMoreDetails> getSchoolMoreDetails(@PathVariable String onetSoc) {
		logger.info("getSchoolMoreDetails");
		List<SchoolMoreDetails> occufindResults = occufindDao.getSchoolMoreDetails(onetSoc);
		return occufindResults;
	}
	
	@RequestMapping(value = "/SchoolProfile/{unitId}/", method = RequestMethod.GET)
	public @ResponseBody SchoolProfile getSchoolProfile(@PathVariable int unitId) {
		logger.info("getSchoolProfile");
		SchoolProfile occufindResults = occufindDao.getSchoolProfile(unitId);
		Map<String, List<cip>> majorSet = new HashMap<>();

		for(HashMap<String, Object> major: occufindResults.getMajors()) {
			String key = (String)major.get("cipFamily");
			cip newcip = new cip();
			newcip.setCipTitle((String)major.get("cipTitle"));
			newcip.setCipDescrpition((String)major.get("cipDescription"));
			
			if(majorSet.containsKey(key)) {
				majorSet.get(key).add(newcip);
			} else {
				List<cip> cipList = new ArrayList<>();
				cipList.add(newcip);
				majorSet.put(key, cipList);
			}
		}
		occufindResults.setMajors(new ArrayList<HashMap<String, Object>>());
		occufindResults.setCollegeMajors(majorSet);
		return occufindResults;
	}
	
	@RequestMapping(value = "/SchoolMajors/{onetCode}/", method = RequestMethod.GET)
	public @ResponseBody List<String> getCareerMajors(@PathVariable String onetCode) {
		logger.info("getCareerMajors");
		return occufindDao.getCareerMajors(onetCode);
	}

	@RequestMapping(value = "/StateSalaryYear", method = RequestMethod.GET)
	public @ResponseBody Integer getStateSalaryYear() {
		logger.info("getStateSalaryYear");
		Integer year = null;

		try {
			year = occufindDao.getStateSalaryYear();
		} catch (Exception e) {
			logger.error("Mybatis Error", e);
		}

		return year;
	}

	@RequestMapping(value = "/EmploymentMoreDetails/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody List<EmploymentMoreDetails> getEmploymentMoreDetails(@PathVariable String onetSocTrimmed) {
		logger.info("getEmploymentMoreDetails");
		List<EmploymentMoreDetails> occufindResults = occufindDao.getEmploymentMoreDetails(onetSocTrimmed);

		return occufindResults;
	}

	@RequestMapping(value = "/MilitaryMoreDetails/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<MilitaryMoreDetails> getMilitaryMoreDetails(@PathVariable String onetSoc) {
		logger.info("getMilitaryMoreDetails");
		List<MilitaryMoreDetails> occufindResults = occufindDao.getMilitaryMoreDetails(onetSoc);

		return occufindResults;
	}

	@RequestMapping(value = "/MilitaryHotJobs/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<MilitaryHotJobs> getMilitaryHotJobs(@PathVariable String onetSoc) {
		logger.info("getMilitaryHotJobs");
		List<MilitaryHotJobs> results = occufindDao.getMilitaryHotJobs(onetSoc);

		return results;
	}

	/**
	 * Returns a list of more resources for a particular occupation.
	 * 
	 * @param onetSoc
	 * @return
	 */
	@RequestMapping(value = "/OccufindMoreResources/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindMoreResources> getOccufindMoreResources(@PathVariable String onetSoc) {
		logger.info("getOccufindMoreResources");

		List<OccufindMoreResources> results = new ArrayList<OccufindMoreResources>();
		try {
			results = occufindDao.getOccufindMoreResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}

	/**
	 * Returns bright outlook list.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/DashboardBrightOutlook/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> getDashboardBrightOutlook(@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo, @PathVariable String interestCdThree) {
		logger.info("getDashboardBrightOutlook");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}

	/**
	 * Returns STEM career list.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/DashboardStemCareers/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> getDashboardStemCareers(@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo, @PathVariable String interestCdThree) {
		logger.info("getDashboardBrightOutlook");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}

	/**
	 * Returns Green career list.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/DashboardGreenCareers/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> getDashboardGreenCareers(@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo, @PathVariable String interestCdThree) {
		logger.info("getDashboardGreenOutlook");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}

	/**
	 * Returns all career list.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ViewAllOccupations/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> viewAllOccupations() {
		logger.info("viewAllOccupations");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.viewAllOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}
	
	/**
	 * Returns all career list.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ViewMilitaryOccupations/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> viewMilitaryOccupations() {
		logger.info("viewMilitaryOccupations");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.viewMilitaryOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}
	
	/**
	 * Returns hot military careers.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ViewHotOccupations/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindSearch> viewHotOccupations() {
		logger.info("viewMilitaryOccupations");

		List<OccufindSearch> results = new ArrayList<OccufindSearch>();
		try {
			results = occufindDao.viewHotOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return results;
	}

	@RequestMapping(value = "/OccufindAlternativeView/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody List<OccufindAlternative> onetHideDetails(@PathVariable String onetSoc) {
		logger.info("onetHideDetails");
		List<OccufindAlternative> results = null;
		try {
			results = occufindDao.onetHideDetails(onetSoc);
		} catch (Exception e) {
			logger.error("OccufindAlternativeView", e);
		}

		return results;
	}

	@RequestMapping(value = "/ImageAltText/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getImageAltText(@PathVariable String onetSoc) {
		logger.info("getImageAltText");
		String results = null;
		try {
			results = occufindDao.getImageAltText(onetSoc);
		} catch (Exception e) {
			logger.error("getImageAltText", e);
		}

		return Collections.singletonMap("imageAltText", results);
	}
	
	@RequestMapping(value = "/job-zone/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody JobZone getJobZone(@PathVariable String onetSoc) {
		logger.info("getJobZone");
		JobZone results = null;
		try {
			results = occufindDao.getJobZone(onetSoc);
		} catch (Exception e) {
			logger.error("getJobZone", e);
		}

		return results;
	}

}