package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.dao.mybatis.impl.CEPAnalyticsDAOImpl;
import com.cep.spring.model.ShareToCounselorForm;
import com.cep.spring.model.BringAsvabToSchoolForm;
import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;
import com.cep.spring.utils.VerifyRecaptcha;

@Controller
@RequestMapping("/contactUs")
public class ContactUs {

	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ContactUs.class);
	private static String ASVAB_SHARE_PDF = "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\CEP\\static\\CEP_PDF_Contents\\ASVAB_CEP_Fact_Sheet.pdf";

	@Autowired
	CEPMailer cepMailer;

	@Autowired
	CEPAnalyticsDAOImpl analyticsDao;

	@Autowired
	ContactUsDAO contactUsDao;

	/**
	 * Sends contact us information to help desk email and confirmation email to
	 * user.
	 * 
	 * @param emailObject
	 * @return
	 */
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> email(@RequestBody EmailContactUs emailObject) {

		try {

			// Verify recaptcha with Google.
			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());

			// If verify then proceed with sending inquire email.
			if (isVerified) {

				String message = emailObject.getUserType() + "\n" + emailObject.getFirstName() + "\n" + emailObject.getLastName() + "\n"
						+ emailObject.getEmail() + "\n" + emailObject.getTopics() + "\n" + emailObject.getMessage()
						+ "\n";
				String subject = "Someone is trying to contact us";

				// Get administrator emails.
				try {
					ArrayList<String> emails = contactUsDao.getEmail(1);

					// Send inquire to administrators.
					for (int i = 0; i < emails.size(); i++) {
						cepMailer.sendEmail(emails.get(i), emailObject.getEmail(), message, subject, null);
					}

				} catch (Exception e) {
					logger.error("Error getting administrator emails." + e);
				}

				// Store inquiry in database.
				try {
					int i = contactUsDao.insertGeneralContactUs(emailObject);

					// If zero records were inserted then throw exception.
					if (i == 0) {
						throw new RuntimeException("Error: Inserted zero records.");
					}
				} catch (Exception e) {
					logger.error("Error storing inquire to database.", e);
				}

			} else {

				// If Google recaptcha is not valid, then return error message.
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}

		// Send confirmation receipt to user.
		String userEmail = emailObject.getEmail();
		String message = "Hello,\n\nThank you for your inquiry. We have received your message and we will get back to you ASAP.\n\nDuring the months of October, November, and December, response time can be up to 3 weeks.\n\nThank you,\n The ASVAB CEP Team";
		String subject = "ASVAB CEP Received Your Inquiry";
		try {
			cepMailer.sendEmail(userEmail, message, subject, null);
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
		}

		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}

	/**
	 * Sends email to help desk with information from bring ASVAB CEP to your
	 * school form.
	 * 
	 * @param emailObject
	 * @return
	 */
	@RequestMapping(value = "/scheduleEmail", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> scheduleEmail(
			@RequestBody BringAsvabToSchoolForm emailObject) {

		try {

			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptcha());

			if (isVerified) {
				String message = "Title: " + emailObject.getTitle() + 
						"\nPosition: " + emailObject.getPosition()
						+ "\nFirst Name: " + emailObject.getFirstName() 
						+ "\nLast Name: " + emailObject.getLastName() 
						+ "\nEmail: " + emailObject.getEmail() 
						+ "\nPhone: " + emailObject.getPhone() 							
						+ "\nHow they heard about us: " + (emailObject.getHeardUs() != null && !emailObject.getHeardUs().isEmpty() ? emailObject.getHeardUs() : "") + (emailObject.getHeardUsOther() != null && !emailObject.getHeardUsOther().isEmpty() ? " : " + emailObject.getHeardUsOther() : "")
						+ "\nSchool Name: " + emailObject.getSchoolName()
						+ "\nSchool Address: " + emailObject.getSchoolAddress() 
						+ "\nSchool City: " + emailObject.getSchoolCity() 
						+ "\nSchool County: " + emailObject.getSchoolCounty()
						+ "\nSchool State: " + emailObject.getSchoolState() 
						+ "\nSchool Zip: " + emailObject.getSchoolZip();
				String subject = "Someone wants to schedule CEP test at their school";

				// Get administrator emails.
				try {
					ArrayList<String> emails = contactUsDao.getEmail(3);

					// Send inquire to administrators.
					for (int i = 0; i < emails.size(); i++) {
						cepMailer.sendEmail(emails.get(i), emailObject.getEmail(), message, subject, null);
					}

				} catch (Exception e) {
					logger.error("Error getting administrator emails." + e);
				}

			} else {
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}

			try {
				analyticsDao.insertCEPShare(emailObject.getEmail(), "Counselor");
			} catch (Exception e) {
				logger.error("Failed to insert CEP Share", e);
			}
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}

		try {
			String attachement = ASVAB_SHARE_PDF;
			cepMailer.sendEmail(emailObject.getEmail(),
					"Hello " + emailObject.getFirstName() + " " + emailObject.getLastName()
							+ ",\n\nThank you for your interest in the ASVAB Career Exploration Program. That's the first step in giving your students a resource to learn about themselves, explore careers, and plan for their futures.\n\nWe've attached a fact sheet so you can learn more about the key components of this free career planning platform. \n\nContact an Education Services Specialist directly at 1-800-323-0513.\n\nThank you,\nThe ASVAB CEP Team",
					"Bring ASVAB CEP to your school ", attachement);
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
		}

		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}

	/**
	 * Sends email to counselor with information about the ASVAB Career
	 * Exploration Program.
	 * 
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/shareWithCounselor", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> shareWithCounselor(@RequestBody ShareToCounselorForm emailObject) {

		try {

			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptcha());
			boolean isNotified = false;

			if (isVerified) {
				String attachement = ASVAB_SHARE_PDF;
				// Send request to school administrator/counselor
				try {
					cepMailer.sendEmail(emailObject.getEmail(),
							"Hello,\n\nWe received a request from someone who wants to bring the ASVAB Career Exploration Program to your school. Attached you'll find our Fact Sheet. Check it out!\n\nTo learn more visit www.asvabprogram.com or contact an Education Services Specialist at 1-800-323-0513.\n\nThank you,\nThe ASVAB CEP Team",
							"Bring ASVAB CEP to your school", attachement);
					isNotified = true;
				} catch (Exception e) {
					logger.error("Failed to email " + emailObject.getEmail());
				}
				
				// Send inquire to administrators.
				String emailMessage = "School Counselor " + emailObject.getEmail() + " was notified of our CEP website. The student or parent heard about the CEP via " + emailObject.getStudentHeardUs() + (emailObject.getStudentHeardUsOther() != null && !emailObject.getStudentHeardUsOther().isEmpty() ? " : " + emailObject.getStudentHeardUsOther() : "") + ".";				
				try {
					if (isNotified) {
						cepMailer.sendEmail("asvabcep@gmail.com",emailMessage,"Bring ASVAB CEP to your school",null);	
					}
				} catch (Exception e) {
					logger.error("Failed to email to asvabcep@gmail.com");
				}				
			} else {
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}

			try {
				analyticsDao.insertCEPShare(emailObject.getEmail(), "Student or Parent");
			} catch (Exception e) {
				logger.error("Failed to insert CEP Share", e);
			}

		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}

	/**
	 * Sends email to help desk with user's score request information.
	 * 
	 * @param emailObject
	 * @return
	 */
	@RequestMapping(value = "/scoreRequest", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> scoreRequest(@RequestBody ScoreRequest emailObject) {

		try {

			// Verify recaptcha with Google.
			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());

			if (isVerified) {
				String message = "UserType: " + emailObject.getUserType() 
						+ "\nFirst Name: " + emailObject.getFirstName() 
						+ "\nLast Name: " + emailObject.getLastName() 
						+ "\nEmail: " + emailObject.getEmail() 
						+ "\nSchool Name: " + emailObject.getSchoolName()
						+ "\nCity: " + emailObject.getCity()
						+ "\nCounty: " + emailObject.getCounty()
						+ "\nState: " + emailObject.getState() 
						+ "\nCountry:" + emailObject.getCountry()
						+ "\nTest Date: " + emailObject.getDt() 
						+ "\nMessage: " + emailObject.getMessage();
				String subject = "--- SCORE REQUEST ---";

				// Get administrator emails.
				try {
					ArrayList<String> emails = contactUsDao.getEmail(2);

					// Send inquire to administrators.
					for (int i = 0; i < emails.size(); i++) {
						cepMailer.sendEmail(emails.get(i), emailObject.getEmail() , message, subject, null);
					}
				} catch (Exception e) {
					logger.error("Error getting administrator emails." + e);
				}

				// Store inquiry in database.
				try {
					int i = contactUsDao.insertScoreRquest(emailObject);

					// If zero records were inserted then throw exception.
					if (i == 0) {
						throw new RuntimeException("Error: Inserted zero records.");
					}
				} catch (Exception e) {
					logger.error("Error storing inquire to database.", e);
				}

			} else {
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			System.err.println(e.getMessage());
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}

}
