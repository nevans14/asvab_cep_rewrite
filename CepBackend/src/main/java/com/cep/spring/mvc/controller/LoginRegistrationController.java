package com.cep.spring.mvc.controller; 

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.model.*;
import com.cep.spring.utils.UserManager;
import com.cep.spring.utils.VerifyRecaptcha;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.utils.GenerateAccessCodes;
import com.cep.spring.utils.SimpleUtils;
import java.security.Principal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static java.lang.Math.toIntExact;


/***
 * Status Enum to associate status codes and names
 * @author jcidras
 *
 */
enum Status
{
	SUCCESS(0, "Success"),
	ACCESS_CODE_NOT_FOUND(1001, "Access Code is not found."),
	EMAILADDRESS_NOT_REGISTERED(1002, "The provided Email Address is not registered."),	
	USERNAME_ALREADY_TAKEN(1003, "This username is already taken"),
	RESET_REQUEST_NOT_MADE(1005, "A reset request has not been made."),
	INVALID_RESET_KEY(1006, "An invalid reset request key was detected. Please check your email for a more recent reset invitation."),
	RESET_KEY_EXPIRED(1007, "Reset request has expired."),
	INVALID_CREDENTIALS(1008, "Invalid Username or Password."),
	NO_ROLE_ASSIGNED(1009, "A role was not assigned to this user."),
	ACCOUNT_IS_LOCKED(1010, "Account has been locked."),
	INVALID_LOGIN_METHOD(1011, "This Access Code was previously registered. Use email & password to login."),
	ACCESS_CODE_EXPIRED(1021, "Access Code has expired."),
	INVALID_ACCESS_CODE(1024, "Access Code is invalid."),
	REGISTRATION_INCOMPLETE(1023, "Registration incomplete, please register this email again."),
	REGISTRATION_COMPLETED(1027, "Registration is already completed. Use credentials to login."),
	REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD(1008, "Registration was completed with a different password. Try resetting it."),
	REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME(1029, "Registration was completed with a different email address."),
	INVALID_RECAPTCHA(1030, "Invalid recaptcha response.");
	// fields
	private final int statusCode;
	private final String statusName;
	// constructor
	Status(int statusCode, String statusName) {
		this.statusCode = statusCode;
		this.statusName = statusName;
	}
	// accessors
	public int getStatusCode() { return this.statusCode; }	
	public String getStatusName() { return this.statusName;	}
}

/***
 * Security Role for Spring
 * @author jcidras
 *
 */
enum SecurityRole {
	USER("ROLE_USER"),
	ADMIN("ROLE_ADMIN");
	// fields
	private final String roleName;
	// constructor
	SecurityRole(String roleName) {
		this.roleName = roleName;
	}
	// accessors
	public String getRoleName() { return this.roleName; }
}

/***
 * Roles Enum to associate role name to code
 * @author jcidras
 *
 */
enum Role {
	STUDENT("S"),
	TEACHER("T"),
	ADMIN("A"),
	PTI("P");
	// fields
	private final String roleName;
	// constructor
	Role(String roleName) {
		this.roleName = roleName;
	}
	// accessors
	public String getRoleName() { return this.roleName; }
}

/**
 * Controller that allows for application login and user registration.
 *
 * This controller has the following end points
 * <ul>
 *   <li>rest/applicationAccess/loginAccessCode</li>
 *   <li>rest/applicationAccess/user</li>
 *   <li>rest/applicationAccess/loginEmail</li>
 *   <li>rest/applicationAccess/register</li>
 *   <li>rest/applicationAccess/passwordResetRequest</li>
 *   <li>rest/applicationAccess/passwordReset</li>
 * </ul>
 *
 * @author Dave Springer
 *
 */
@Controller
@RequestMapping("/applicationAccess")
@Transactional(propagation=Propagation.REQUIRED) 
public class LoginRegistrationController 
    implements HandlerMethodArgumentResolver 
    { 
	private Logger logger = LogManager.getLogger( this.getClass().getName() );
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	@Autowired
	CEPMailer cepMailer;
	@Autowired
	static SecurityContextHolder securityContextHolder;
	@Autowired
	BCryptPasswordEncoder encoder;
	@Autowired
	RegistrationDAO registrationDao;
	@Autowired
	FYIRestController fyi;
	@Autowired
	FindYourInterestDAO fyiDao;
	@Autowired
	NotesController notes;
	@Autowired
	PortfolioController portfolio;
	@Autowired
	FavoriteOccupationController favorate;
	@Autowired
	UserManager userManager;
	@Autowired
	DbInfoDAO dbInfoDao;

	@RequestMapping(value = "/loginAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus loginAccessCode(@RequestBody Registration registration, HttpServletRequest request) {

		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		/*if (!verifyRecaptcha(registration, rls)) {
			return rls;
		}*/
        List<GrantedAuthority> authorities;
	    // Determine if this Access Code was generated by CITM 
        String aCode = registration.getAccessCode().trim();
        if ( aCode.length() == 10 && aCode.endsWith("CITM")) {
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }
		// get access code
		AccessCodes accessCode = registrationDao.getAccessCodes(aCode, null);
		// determine if the access code exists
		if (accessCode != null) { // access code exists
			// check if the access code has expired
			if (accessCode.getExpireDate() == null) {
				// update access to have 21 more days
				accessCode.setExpireDate(SimpleUtils.datePlusDays(new Date(), 21));
                registrationDao.updateAccessCode(accessCode);
                // continue login
			} else if (0 < SimpleUtils.dateCompare(new Date(), accessCode.getExpireDate())) {
				rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
				rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName());
				return rls;	
			}
			// get user by id
			Users user = registrationDao.getUser(accessCode.getUserId(), null);
			if (user != null) {
				// unable to continue, must login via username/password
				rls.setStatusNumber(Status.INVALID_LOGIN_METHOD.getStatusCode());
				rls.setStatus(Status.INVALID_LOGIN_METHOD.getStatusName());
				return rls;
			}
			// set meps info
			Meps mepsInfo = registrationDao.getUsersMeps(registration.getAccessCode());
			setMepsInfo(rls, mepsInfo);
			// get security role based on role
			SecurityRole role = getSecurityRole(accessCode.getRole());
			// check role if admin, need to add a new role for USER
			authorities = getAuthoritiesByRole(accessCode.getRole());
			// set spring security context
            setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, role.getRoleName(), accessCode.getRole(), accessCode.getUserId(), false, request);
            // log login
            registrationDao.insertAsvabLog(toIntExact(accessCode.getUserId()));
            // set status
            rls.setRole(accessCode.getRole());
            rls.setUser_id(accessCode.getUserId().toString());
            rls.setStatusNumber(Status.SUCCESS.getStatusCode());
            rls.setStatus(Status.SUCCESS.getStatusName());
            rls.setAccessCode(registration.getAccessCode());
		} else if (isThisAValidStudentId(registration.getAccessCode())) { // access code does not exist, but is valid
			// get user id
			long userId = createStudentAccessCode(registration.getAccessCode());
			// set meps info
			Meps mepsInfo = registrationDao.getUsersMeps(registration.getAccessCode());
			setMepsInfo(rls, mepsInfo);
			// get authorities, default role
			authorities = getAuthorities(SecurityRole.USER.getRoleName());
			// set spring security context
			setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), Role.STUDENT.getRoleName(), userId, false, request);
			// insert login record
			registrationDao.insertAsvabLog(toIntExact(userId));
			// set status
			rls.setRole(Role.STUDENT.getRoleName());
			rls.setUser_id(((Long)userId).toString());
			rls.setStatusNumber(Status.SUCCESS.getStatusCode());
            rls.setStatus(Status.SUCCESS.getStatusName());
            rls.setAccessCode(registration.getAccessCode());
		} else if(isValidPtiAccessCode(registration.getAccessCode())) { 
			// get user id
			long userId = createPtiAccessCode(registration.getAccessCode());
			// set meps info
			Meps mepsInfo = registrationDao.getUsersMeps(registration.getAccessCode());
			setMepsInfo(rls, mepsInfo);
			// get authorities, default role
			authorities = getAuthorities(SecurityRole.USER.getRoleName());
			// set spring security context
			setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), Role.PTI.getRoleName(), userId, false, request);
			// insert login record
			registrationDao.insertAsvabLog(toIntExact(userId));
			// set status
			rls.setRole(Role.PTI.getRoleName());
			rls.setUser_id(((Long)userId).toString());
			rls.setStatusNumber(Status.SUCCESS.getStatusCode());
			rls.setStatus(Status.SUCCESS.getStatusName());
			rls.setAccessCode(registration.getAccessCode());
		} else { // access code does not exist and is not valid
			rls.setStatusNumber(Status.ACCESS_CODE_NOT_FOUND.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_NOT_FOUND.getStatusName());
		}
		// return status
		return rls;
	}

	@RequestMapping("/user")
	public @ResponseBody Principal user(Principal user) {
		logger.info("User url was hit!");
		return user;
	}
	
	@RequestMapping(value = "/loginEmail", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus loginEmail(@RequestBody Registration registration, HttpServletRequest request) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		/*if (!verifyRecaptcha(registration, rls)) {
			return rls;
		}*/
        //   REST debug statements data from client ...
        logger.debug("loginEmail->" );
        logger.debug("Access Code:" + registration.getAccessCode());
        logger.debug("Email      :" + registration.getEmail());
        logger.debug("Password   :" + registration.getPassword());
        logger.debug("ResetKey   :" + registration.getResetKey());

        // get user info
		Users user = registrationDao.getByUsername(registration.getEmail());
		// invalid username OR if passwords do not match, return invalid credentials
		if (user == null || 
		   (user != null && (!encoder.matches(registration.getPassword(), user.getPassword())))) {
			rls.setStatusNumber(Status.INVALID_CREDENTIALS.getStatusCode());
			rls.setStatus(Status.INVALID_CREDENTIALS.getStatusName());
			return rls;
		}
//        logger.debug("user info:" + user);
		// check if the user id is 0 (incomplete registration)
		if (user.getUserId().equals(0)) {
			rls.setStatusNumber(Status.REGISTRATION_INCOMPLETE.getStatusCode());
			rls.setStatus(Status.REGISTRATION_INCOMPLETE.getStatusName());
			return rls;
		}		
        // Determine if this Access Code was generated by CITM 
        String aCode = user.getAccessCodes().getAccessCode().trim();
        if (aCode.length() == 10 && aCode.endsWith("CITM")){
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }		
		// test to see if the user is locked out
		if (user.getEnabled().equals((short)0)) {
			rls.setStatusNumber(Status.ACCOUNT_IS_LOCKED.getStatusCode());
			rls.setStatus(Status.ACCOUNT_IS_LOCKED.getStatusName()); 		
			return rls;
		}
		// has the access code expired
		if (0 < SimpleUtils.dateCompare(new Date(), user.getAccessCodes().getExpireDate())) {
			rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName());
			return rls;
		}
		// Get Roles		
		if (user.getUserRoles().isEmpty()) {
			rls.setStatusNumber(Status.NO_ROLE_ASSIGNED.getStatusCode());
			rls.setStatus(Status.NO_ROLE_ASSIGNED.getStatusName()); 	
			return rls;
		}
		// create authority list
		List<GrantedAuthority> authorities = getAuthorities(user.getUserRoles());
		// get security role
		SecurityRole role = getSecurityRole(user.getAccessCodes().getRole());
		// set security session info
		setSessionInfo(user.getUsername(), user.getPassword(), user.getAccessCodes().getAccessCode(), authorities, role.getRoleName(), user.getAccessCodes().getRole(), (long)user.getUserId(), true, request);
		logger.info("User logged in:" + user.getUsername() + " Role:" + role.getRoleName());
		// get user meps
		Meps mepsInfo = registrationDao.getUsersMeps(registration.getAccessCode());
		setMepsInfo(rls, mepsInfo);
		// set status info
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());		
		rls.setRole(user.getAccessCodes().getRole());
		rls.setUser_id(user.getUserId().toString());
		rls.setHash(user.getPassword());
		rls.setAccessCode(user.getAccessCodes().getAccessCode());
		// save login info
		registrationDao.insertAsvabLog(user.getUserId());
		// return status
		return rls ;		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus register(@RequestBody Registration registration, HttpServletRequest request) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		if (!verifyRecaptcha(registration, rls)) {
			return rls;
		}
		List<GrantedAuthority> authorities;
		String rawPwd = registration.getPassword();
		registration.setPassword(encoder.encode(registration.getPassword()));		
		// Determine if this Access Code was generated by CITM 
        String aCode = registration.getAccessCode().trim();
        if ( aCode.length() == 10 && aCode.endsWith("CITM") ){
            // This access code is for CITM use only.            
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }		
		// User Id
		Long userId = 0L;
		// get access code
		AccessCodes accessCode = registrationDao.getAccessCodes(aCode, null);
		// get meps info         
		Meps mepsInfo = registrationDao.getUsersMeps(aCode);
        // check if the access code exists
		if (accessCode != null ) {
			userId = accessCode.getUserId();
            rls.setUser_id( userId.toString().trim());
			rls.setRole(accessCode.getRole());
			// check dates
            if (accessCode.getExpireDate() == null) { // null, push expire date to 21 days ahead
                accessCode.setExpireDate(SimpleUtils.datePlusDays(new Date(), 21));
                registrationDao.updateAccessCode(accessCode);
                // continue with registration
            } else if (0 < SimpleUtils.dateCompare(new Date(), accessCode.getExpireDate())) { // expire, return error message                
        	    rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
                rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName()); 
                return rls;            	                           
            }	
            // check if the access code has been registered to the inbound email
            Users user = registrationDao.getUser(userId, null);
            if (user != null) { 
        		if (user.getUsername().equalsIgnoreCase(registration.getEmail())) {                    	
            		// return message to indicate the user should use their username/password to login
                	Status status = (encoder.matches(rawPwd, user.getPassword()) 
                			? Status.REGISTRATION_COMPLETED
        					: Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD);
                	rls.setStatusNumber(status.getStatusCode());
                	rls.setStatus(status.getStatusName());
        		} else {
        			// access code was registered with a different email address
        			rls.setStatusNumber(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME.getStatusCode());
        			rls.setStatus(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME.getStatusName());
        		}
        		// return status
            	return rls;
            }
            // continue with registration, assuming user ran into an issue in the registration process     
		} else if (isThisAValidStudentId(aCode)) { 
			// create student access code and get the user ID
			userId = createStudentAccessCode(aCode);
			// set status
            rls.setStatusNumber(Status.SUCCESS.getStatusCode()); 
            rls.setStatus(Status.SUCCESS.getStatusName()); 
            rls.setUser_id(String.valueOf(userId));
            rls.setRole(Role.STUDENT.getRoleName());
			setMepsInfo(rls, mepsInfo);		    		
		} else if (isValidPtiAccessCode(aCode)) {
			// create PTI access code and get the user ID
			userId = createPtiAccessCode(aCode);
			// set status
			rls.setStatusNumber(Status.SUCCESS.getStatusCode());
			rls.setStatus(Status.SUCCESS.getStatusName());
			rls.setUser_id(String.valueOf(userId));
			rls.setRole(Role.PTI.getRoleName());
			setMepsInfo(rls, mepsInfo);
		} else {
			rls.setStatusNumber(Status.ACCESS_CODE_NOT_FOUND.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_NOT_FOUND.getStatusName());
			return rls;
		}
		// get access code
		accessCode = registrationDao.getAccessCodes(null, toIntExact(userId));
		boolean updateUserRecord = false;
		// Query for user based email 
		Users existingUser = registrationDao.getUser(null, registration.getEmail());
		// check if user already exists in system (e.g., from a previous administration)
		if (existingUser != null) {
			Boolean doesntMatches = jdbcTemplate.query("SELECT 1 FROM [dbo].[access_codes_merged] where [user_id] = ? and [new_access_code]  = ?", new ResultSetExtractor<Boolean>() {
		        public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {return rs.next() ? false: true;}
		    }, existingUser.getUserId(), aCode);
			// get the old access code that the account was bound to
			String oldAccessCode = registrationDao.getAccessCodes(null, existingUser.getUserId()).getAccessCode();
			// if the access code and user ID haven't been merged and if the old/new access codes don't match, check if the passwords match
		    if (doesntMatches && !aCode.equalsIgnoreCase(oldAccessCode))  {
		    	// if passwords don't match from registration, display error
				if (!encoder.matches(rawPwd, existingUser.getPassword())) {
					rls.setStatusNumber(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD.getStatusCode());
					rls.setStatus(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD.getStatusName());
					return rls;
				}
				// Assume we have registration issue from before and update
				updateUserRecord = true;
		    } else {		    
    			// Error already have an account ...
    			rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
    			rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
    			return rls;
		    }
		} 		
		// determine if we need to update the record
		if ( updateUserRecord ) {
			// get authorities
			authorities = getAuthoritiesByRole(accessCode.getRole());
			// set spring security context
            setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), accessCode.getRole(), userId, true, request);
			// set meps info
			setMepsInfo(rls, mepsInfo);			
		    rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
            rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
            rls.setAccessCode(registration.getAccessCode());
            return rls;
		} else {	
    		// prepare new user and insert to database
    		Users userToInsert = new Users(toIntExact(userId), registration.getEmail(), registration.getPassword(), (short)1, "CEP-CITM", registration.getStudentId());
    		registrationDao.insertUser(userToInsert);  
    		// Insert new user role 
			registrationDao.insertUserRole(registration.getEmail(), SecurityRole.USER.getRoleName());	
			// check if the user wants to opts out of communications
			if (!registration.isOptedInCommunications()) {
				registrationDao.insertSubscription(registration.getEmail());
			}
			// get authorities
			authorities = getAuthoritiesByRole(accessCode.getRole());
			// set spring security context ....
            setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), accessCode.getRole(), userId, true, request);
            // record login 
            registrationDao.insertAsvabLog(toIntExact(userId));
            // so far success
    		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
    		rls.setStatus(Status.SUCCESS.getStatusName());
    		rls.setAccessCode(registration.getAccessCode());
    		// continue to send registration email to user
        }			
		// Send registration email	      
        String userEmail = registration.getEmail();
        // get website url 
        String websiteUrl = jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='website_url'", String.class);
        // Compose Registration email
        String htmlEmailMessage = jdbcTemplate.queryForObject("SELECT value2 FROM dbInfo WHERE name='registration_email'", String.class);
		htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
		htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");		
		try {
		    cepMailer.sendResetRegisterHtmlEmail(userEmail, "" , htmlEmailMessage, "Welcome to ASVAB CEP", null );
		} catch (Exception e) {
		    // Log and move on ....  This always fails locally 
		    e.printStackTrace();
		}
		return rls;
	}
	
	@Transactional
	@RequestMapping(value = "/passwordResetRequest", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  passwordResetRequest(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		/*if (!verifyRecaptcha(registration, rls)) {
			return rls;
		}*/
		// get user by username
		Users user = registrationDao.getUser(null, registration.getEmail());
		// test if the user doesn't exist
		if (user == null) {
			rls.setStatusNumber(Status.EMAILADDRESS_NOT_REGISTERED.getStatusCode());
			rls.setStatus(Status.EMAILADDRESS_NOT_REGISTERED.getStatusName());
			return rls;
		}
		// create reset flag
		UUID resetFlag = UUID.randomUUID();
		// get username (email)
        String username = user.getUsername();		
		// calculate two days from now.		
		Date expireDate = SimpleUtils.datePlusDays(new Date(), 2);
		// update user model
		user.setEnabled((short)0);
		user.setResetKey(resetFlag.toString());
		user.setResetExpireDate(expireDate);
		// update user
		registrationDao.updateUser(user);
		// http://localhost:8080/CEP/#/passwordReset?resetId=263246f4-470a-4a2b-9952-2f293c4d0834%20username%3Dtaylor@google.com
		// Compose reset URL
		DbInfo dbInfo = dbInfoDao.getDbInfo("password_reset_url");
		String resetUrl, htmlEmailMessage;
		resetUrl = dbInfo.getValue();
		// set reset URL
		resetUrl = resetUrl + resetFlag + "+username=" + username;
	    // compose email
		dbInfo = dbInfoDao.getDbInfo("password_reset_email_message");
        htmlEmailMessage = dbInfo.getValue2();
        htmlEmailMessage = htmlEmailMessage.replace("#resetUrl#", resetUrl);
        htmlEmailMessage = htmlEmailMessage.replace("#userEmail#", username);      
        htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");
        // try to send email to user
        try {
            cepMailer.sendResetRegisterHtmlEmail(username, "" , htmlEmailMessage, "Password Reset ...", null );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // return status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus("Email sent to complete password reset.");
		return rls;
	}

	@RequestMapping(value = "/passwordReset", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus passwordReset(@RequestBody Registration registration) {		
		RegistrationLoginStatus rls =new RegistrationLoginStatus();
		// Look up ResetKey
		Users user = registrationDao.getUser(null, registration.getEmail());
		// test if the user doesn't exist
		if (user == null) {
			rls.setStatusNumber(Status.EMAILADDRESS_NOT_REGISTERED.getStatusCode());
			rls.setStatus(Status.EMAILADDRESS_NOT_REGISTERED.getStatusName());
			return rls;
		}
		// If the reset-key is null
		if (user.getResetKey() == null) {
			rls.setStatusNumber(Status.RESET_REQUEST_NOT_MADE.getStatusCode());
			rls.setState(Status.RESET_REQUEST_NOT_MADE.getStatusName());
			return rls;
		}
		// If the keys do not match
		if (!user.getResetKey().equals(registration.getResetKey())) {
			rls.setStatusNumber(Status.INVALID_RESET_KEY.getStatusCode());
			rls.setStatus(Status.INVALID_RESET_KEY.getStatusName());
			return rls;		
		}		
		// If now is after resetExpireDate error 
		if ( 0L <= SimpleUtils.dateCompare(new Date(), user.getResetExpireDate())) {
			rls.setStatusNumber(Status.RESET_KEY_EXPIRED.getStatusCode());
			rls.setStatus(Status.RESET_KEY_EXPIRED.getStatusName());
			return rls;		
		}
		// update user model
		user.setPassword(registration.getPassword());
		user.setResetKey(null);
		user.setResetExpireDate(null);
		user.setEnabled((short)1);
		// save changes
		registrationDao.updateUser(user);
		// set and return status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		return rls;		
	}
	
	/****************************************************************
	 * ACCESS CODE API 											    *
	 ****************************************************************/
	@RequestMapping(value = "/getAccessCode", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<AccessCodes> GetAccessCodeByUserId() {
		AccessCodes model = new AccessCodes();
		Integer userId = userManager.getUserId();
		try {
			model = registrationDao.getAccessCodes(null, userId);
			if (model == null)
				return new ResponseEntity<AccessCodes>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<AccessCodes>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<AccessCodes>(model, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/upateAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
			RegistrationExists(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =new RegistrationLoginStatus();

		logger.info("testTeacherAccessCode->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );
		
		if (registration.isUpdateFYI()) {
			String promoAccessCodes = jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='teacher_access_codes'", String.class);
		}
		
		if (registration.isUpdateFavorates()) {
			
		}
		
		if (registration.isUpdateProfile()) {
			
		}
		return rls;
		
	}
	
	/*********************************************************************************/
	/*       Teacher Access Code Feature                                             */
	/*********************************************************************************/
	
	@RequestMapping(value = "/testTeacherAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
			isThisAValidTeacherPromoAccessCode(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =new RegistrationLoginStatus();

		logger.info("testTeacherAccessCode->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Subject      :" + registration.getSubject() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );
		DbInfo dbInfo = dbInfoDao.getDbInfo("teacher_access_codes");
		String promoAccessCodes = dbInfo.getValue();
		boolean isValidTeacherAccessCode = promoAccessCodes.contains(registration.getAccessCode() );
		if (isValidTeacherAccessCode) {
			rls.setStatusNumber(0);
			rls.setStatus("Success");
		} else {
			rls.setStatusNumber(805);
			rls.setStatus("Not A valid teacher access code.");			
		}
		return rls;
	}
	
	@Transactional
	@RequestMapping(value = "/teacherRegistration", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus teacherRegistrationForPromo(@RequestBody Registration registration,  HttpServletRequest request) {
		RegistrationLoginStatus rls =new RegistrationLoginStatus();
		if (!verifyRecaptcha(registration, rls)) {
			return rls;
		}
		List<GrantedAuthority> authorities;
		AccessCodes accessCodes;
		logger.info("teacherRegistration->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Subject      :" + registration.getSubject() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );
		// gets promo code from form
		String promoCode = registration.getAccessCode().trim().toUpperCase();
		// check to see if promo code is valid
		if (!isAValidTeacherCode(promoCode)) { 
			rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
			rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());
			return rls;
		}		
		// check if the user has already registered
		Users user = registrationDao.getUser(null, registration.getEmail());
		if (user != null) {
			rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
			rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
			return rls;
		}
		// create a new teacher access code
		long userId = createTeacherAccessCode(promoCode);
		// get new access code
		accessCodes = registrationDao.getAccessCodes(null, toIntExact(userId));
		// insert teacher 
		Users teacher = new Users((int)userId, registration.getEmail(), registration.getPassword(), (short)1, "CEP-CITM", registration.getStudentId());
		registrationDao.insertUser(teacher);
		// check if the user wants to opts out of communications
		if (!registration.isOptedInCommunications()) {
			registrationDao.insertSubscription(registration.getEmail());
		}
		// insert role for teacher
		registrationDao.insertUserRole(registration.getEmail(), SecurityRole.USER.getRoleName());
		// insert teacher profession
		if (registration.getSubject() != null && !registration.getSubject().isEmpty()) {
			registrationDao.insertTeacherProfession((int)userId, registration.getSubject().trim());
		}
		// get authorities
		authorities = getAuthoritiesByRole(Role.TEACHER.getRoleName());
        // set spring security context ....
        setSessionInfo(registration.getEmail(), registration.getPassword(), accessCodes.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), Role.TEACHER.getRoleName(), userId, true, request);
        // get/set meps info
        Meps mepsInfo = registrationDao.getUsersMeps(registration.getAccessCode());
        setMepsInfo(rls, mepsInfo);
        // insert asvab log
		registrationDao.insertAsvabLog((int)userId);
		// set status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		rls.setRole(Role.TEACHER.getRoleName());
		rls.setUser_id(String.valueOf(userId));
		rls.setAccessCode(registration.getAccessCode());
		/** PREPARE TO SEND EMAIL **/
        // get website url 
        String websiteUrl = jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='website_url'", String.class);
        // Compose Registration email
        String htmlEmailMessage = jdbcTemplate.queryForObject("SELECT value2 FROM dbInfo WHERE name='registration_email-teacher'", String.class);
        htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
        htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");    
        try {
        	cepMailer.sendResetRegisterHtmlEmail(registration.getEmail(), "" , htmlEmailMessage, "Welcome to ASVAB CEP", null );
        } catch (Exception e) {
        	// Log and move on ....  This always fails locally 
        	e.printStackTrace();
        }		
		return rls;
	}

	@RequestMapping(value = "/merge/{emailAddress}/{newAccessCode}/{profileFlagString}/", method = RequestMethod.GET)
	public @ResponseBody RegistrationLoginStatus  MergeAccounts(@PathVariable String emailAddress,@PathVariable String newAccessCode,@PathVariable String profileFlagString) {
        RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		boolean profileFlag = false;
		try {
			profileFlag = Boolean.parseBoolean(profileFlagString);
		} catch(Exception e) {
			rls.setStatusNumber(1011);
			rls.setStatus("fyi and/or profile flag is not a boolean");
            logger.error("fyi and/or profile flag is not a boolean");		
		}
		Integer oldUserId = registrationDao.getUserId(emailAddress).getUserId();
		AccessCodes oldAccessCodeObj = registrationDao.getAccessCodes(null,oldUserId);
		AccessCodes newAccessCodeObj = registrationDao.getAccessCodes(newAccessCode, null);
		Integer newUserId = newAccessCodeObj.getUserId().intValue();
		
		/*
		Boolean doesntMatches = jdbcTemplate.query("SELECT 1 FROM [dbo].[access_codes_merged] where [user_id] = ? and [new_access_code]  = ?", new ResultSetExtractor<Boolean>() {
	        public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {return rs.next() ? false: true;}
	    }, newAccessCodeObj.getUserId(), newAccessCodeObj.getAccessCode().trim());
		
        if(oldAccessCodeObj.getAccessCode().equalsIgnoreCase(newAccessCodeObj.getAccessCode()) || doesntMatches){
        	rls.setStatusNumber(0);
            rls.setStatus("Success");
            return rls;   
        }
		*/
		
		if(oldUserId != 0) {
			FYIMerge fyiObject = fyiDao.getLastTestObject(oldUserId, newUserId);
			
			AccessCodesMerge registrationObject = new AccessCodesMerge();
			registrationObject.setUserId(oldUserId.longValue());
			registrationObject.setOldAccessCode(oldAccessCodeObj.getAccessCode());
			registrationObject.setNewAccessCode(newAccessCode);
			if(fyiObject != null &&  fyiObject.getUserId().intValue() != newUserId.intValue())
				registrationObject.setFyiMovedToNewAccessCode("Y");
			else
				registrationObject.setFyiMovedToNewAccessCode("N");
			
			try {
				registrationDao.mergeAccounts(registrationObject);

				if(newAccessCodeObj != null) {
					fyi.mergeAccount(oldUserId, newUserId, fyiObject);
					notes.mergeNotes(oldUserId, newUserId);
					if(profileFlag) {
						portfolio.mergePortfolio(oldUserId, newUserId);
						favorate.mergeFavorates(oldUserId, newUserId);
					}
					registrationDao.changeAccessCodePointer(newUserId, emailAddress);
					registrationDao.disableAccessCode(registrationObject.getOldAccessCode());
					logger.error("Successfully Merged");
					

				}

			} catch (DuplicateKeyException e) {
				rls.setStatusNumber(1012);
				rls.setStatus("Access Code already registered " + e);
	            logger.error("Access Code already registered", e);			
	            return rls;
			}
		}
		Meps mepsInfo = registrationDao.getUsersMeps(newAccessCode);
		// insert login record
		registrationDao.insertAsvabLog(toIntExact(newAccessCodeObj.getUserId()));
        rls.setUser_id(String.valueOf(newAccessCodeObj.getUserId()));
        rls.setRole(newAccessCodeObj.getRole());
		setMepsInfo(rls, mepsInfo);
		rls.setStatusNumber(0);
		rls.setStatus("Success");
		return rls;		
	}
	
	/**
	 * Inner class for return payload.
	 * 
	 * 
	 * @author Dave Springer
	 *
	 */
	public class RegistrationLoginStatus {

		private String status, color, role, user_id, hash, accessCode, schoolCode, schoolName, city, state, zipCode, mepsId, mepsName;
		private Integer statusNumber = 0;
		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Integer getStatusNumber() {
			return statusNumber;
		}
		public void setStatusNumber(Integer statusNumber) {
			this.statusNumber = statusNumber;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getUser_id() {
			return user_id;
		}
		public void setUser_id(String user_id) {
			this.user_id = user_id;
		}
		public String getHash() {
			return hash;
		}
		public void setHash(String hash) {
			this.hash = hash;
		}
		public String getAccessCode() { return accessCode; }
		public void setAccessCode(String accessCode) { this.accessCode = accessCode; }
		/**
		 * @return the schoolCode
		 */
		public String getSchoolCode() {
			return schoolCode;
		}
		/**
		 * @param schoolCode the schoolCode to set
		 */
		public void setSchoolCode(String schoolCode) {
			this.schoolCode = schoolCode;
		}
		/**
		 * @return the schoolName
		 */
		public String getSchoolName() {
			return schoolName;
		}
		/**
		 * @param schoolName the schoolName to set
		 */
		public void setSchoolName(String schoolName) {
			this.schoolName = schoolName;
		}
		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}
		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			this.city = city;
		}
		/**
		 * @return the state
		 */
		public String getState() {
			return state;
		}
		/**
		 * @param state the state to set
		 */
		public void setState(String state) {
			this.state = state;
		}
		/**
		 * @return the zipCode
		 */
		public String getZipCode() {
			return zipCode;
		}
		/**
		 * @param zipCode the zipCode to set
		 */
		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}
		/**
		 * @return the mepsId
		 */
		public String getMepsId() {
			return mepsId;
		}
		/**
		 * @param mepsId the mepsId to set
		 */
		public void setMepsId(String mepsId) {
			this.mepsId = mepsId;
		}
		/**
		 * @return the mepsName
		 */
		public String getMepsName() {
			return mepsName;
		}
		/**
		 * @param mepsName the mepsName to set
		 */
		public void setMepsName(String mepsName) {
			this.mepsName = mepsName;
		}
	}
	
	/***
	 * Create a new student access code record
	 * @param accessCode
	 * @return user id
	 */
	private long createStudentAccessCode(String accessCode) {
		// get access code suffix
		String suffix = accessCode.substring(accessCode.length()-1);
		// get/set school year plus two years
		Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(suffix) + 2;
		// set expire date
	    String expireDate = schoolYear.toString().trim() + "-06-30 00:00:00.000";
	    // instantiate new access code object
	    AccessCodes newAccessCode = new AccessCodes(accessCode, SimpleUtils.getDateFromStringInStandardFormat(expireDate), "S");
	    // add new access code to db
	    registrationDao.insertAccessCode(newAccessCode);
	    // return user id
	    return registrationDao.getAccessCodes(accessCode, null).getUserId();
	}
	
	/***
	 * Creates a new access code for a teacher
	 * @param accessCode
	 * @return user id
	 */
	private long createTeacherAccessCode(String accessCode) {
		// get last promo code used based on the given access code
		AccessCodes lastPromoAccessCodeUsed = registrationDao.getLastPromoAccessCodeUsed(accessCode);
		
        Long accessCodeIdPrefix = 0L;
        if (lastPromoAccessCodeUsed == null) {
        	accessCodeIdPrefix = -1L;
        } else {
        	accessCodeIdPrefix = Long.parseLong(lastPromoAccessCodeUsed.getAccessCode().substring(0, 6));
        }
        // format new access code
        String formattedAccessCode = String.format("%06d%s", (++accessCodeIdPrefix), accessCode);
        // set school year
        Integer schoolYear = GenerateAccessCodes.determineSchoolYear();
        String expireDate = schoolYear.toString().trim() + "-06-30 00:00:00.000";
        // instantiate new access code
        AccessCodes teacherAccessCode = new AccessCodes(formattedAccessCode, SimpleUtils.getDateFromStringInStandardFormat(expireDate), Role.TEACHER.getRoleName());
        // insert new access code
        registrationDao.insertAccessCode(teacherAccessCode);
        // return the user id
        return registrationDao.getAccessCodes(formattedAccessCode, null).getUserId();
	}
	
	private long createPtiAccessCode(String accessCode) {
		// get current year
		int year = LocalDate.now().getYear();
		// set expire date in 3 years
		String expireDate = ((Integer)(year + 3)).toString() + "-06-30 00:00:00.000";
		// create new object (unlimited fyi for PTI users)
		AccessCodes newAccessCode = new AccessCodes(accessCode, SimpleUtils.getDateFromStringInStandardFormat(expireDate), Role.PTI.getRoleName(), (short)1);
		// insert into db
		registrationDao.insertAccessCode(newAccessCode);
		// return user id
		return registrationDao.getAccessCodes(accessCode, null).getUserId();
	}
	
	/**
	 * Sets user session with authorities
	 * @param username
	 * @param password
	 * @param authorities
	 * @param securityRole
	 * @param userRole
	 * @param userId
	 * @param request
	 */
	private void setSessionInfo(String username, String password, String accessCode, List<GrantedAuthority> authorities, String securityRole, String userRole, Long userId, boolean isRegistered, HttpServletRequest request) {
		// create authenticationToken
        UsernamePasswordAuthenticationToken authenticationToken = 
                new UsernamePasswordAuthenticationToken(username, password, authorities);
        // authenticationToken.isAuthenticated();
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);   
        logger.debug("User authenticated");
		// Create a new session and add the security context.
		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT_KEY", SecurityContextHolder.getContext());
		// user role (S, T, A, R, etc...)
		session.setAttribute("USER_ROLE", userRole);
		// access code
		session.setAttribute("USER_ACCESS_CODE", accessCode);
		// spring security
		session.setAttribute("ASVAB_ROLE", securityRole);
		session.setAttribute("ASVAB_USER_ID", userId.toString());
		session.setAttribute("IS_REGISTERED", isRegistered);
	}
	
	/***
	 * Returns a list of authorities
	 * @param userRoles
	 * @return
	 */
	private List<GrantedAuthority> getAuthorities(List<UserRoles> userRoles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (UserRoles role : userRoles) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		return authorities;
	}
	
	/***
	 * Returns a list of authorities
	 * @param roles
	 * @return
	 */
	private List<GrantedAuthority> getAuthorities(String... roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	/***
	 * Returns a list of authorities based on the Access Code role
	 * @param role
	 * @return
	 */
	private List<GrantedAuthority> getAuthoritiesByRole(String role) {
		if (role.equals(SecurityRole.ADMIN.getRoleName())) {
			return getAuthorities(SecurityRole.ADMIN.getRoleName(), SecurityRole.USER.getRoleName());
		} else {
			return getAuthorities(SecurityRole.USER.getRoleName());
		}
	}
	
	/***
	 * Returns the security role based on the User's role
	 * @param role
	 * @return
	 */
	private SecurityRole getSecurityRole(String role) {
		switch (role) {
			case "A": 	// Admin
				return SecurityRole.ADMIN;
			default: 	// Everyone else 
				return SecurityRole.USER;
		}
	}
	
	/**
	 * Sets the user MEPS information for the return status
	 * @param rls
	 * @param mepsInfo
	 */
	private void setMepsInfo(RegistrationLoginStatus rls, Meps mepsInfo) {
		if (mepsInfo != null) {
			rls.setMepsId(mepsInfo.getMepsId());
			rls.setMepsName(mepsInfo.getMepsName());
			rls.setSchoolCode(mepsInfo.getSchoolCode());
			rls.setSchoolName(mepsInfo.getSchoolName());
			rls.setCity(mepsInfo.getCity());
			rls.setState(mepsInfo.getState());
			rls.setZipCode(mepsInfo.getZipCode());
		}
	}
	
	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// Implementation not needed for this use case.
		return false;
	}

	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		// Implementation not needed for this use case.
		return null;
	}

	private boolean isValidPtiAccessCode(String accessCode) {
		boolean isValid = false;
		if (accessCode.isEmpty()) return isValid;
		if (accessCode.length() != 9) return isValid;
		String prefix = accessCode.substring(0, 3).toLowerCase();
		String suffix = accessCode.substring(3, accessCode.length());
		if (!prefix.equals("pti")) return isValid;
		try {
			Integer.parseInt(suffix);
			isValid = true;
		} catch (Exception e) {
			return isValid;
		}
		return isValid;
	}
	
	private boolean isThisAValidStudentId(String accessCode) {
		try {
		    Integer stringLength = accessCode.length();
		    String suffix = accessCode.substring( accessCode.length() - 1) ;
		    if ( stringLength != 10) return false;
		    
	        Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(suffix);
		    if ( schoolYear != null ) return true;
		    
		    return false;
		} catch (StringIndexOutOfBoundsException e) {
			return false;
		}
	}
	
	/***
	 * Determines if the given promo code is valid
	 * @param accessCode
	 * @return boolean
	 */
	private boolean isAValidTeacherCode(String accessCode) {
		DbInfo dbInfo = dbInfoDao.getDbInfo("teacher_access_codes");
		if (dbInfo != null) {
			String value = dbInfo.getValue();
			if (value != null && !value.isEmpty()) {
				return value.contains(accessCode);
			}
		}
		return false;
	}

	private boolean verifyRecaptcha(Registration model, RegistrationLoginStatus rls) {
		boolean isVerified = false;
		// verify recaptcha
		try {
			if (!VerifyRecaptcha.verify(model.getRecaptchaResponse())) {
				rls.setStatusNumber(Status.INVALID_RECAPTCHA.getStatusCode());
				rls.setStatus(Status.INVALID_RECAPTCHA.getStatusName());
			} else {
				isVerified = true;
			}
		} catch (Exception ex) {
			rls.setStatusNumber(Status.INVALID_RECAPTCHA.getStatusCode());
			rls.setStatus(Status.INVALID_RECAPTCHA.getStatusName());
		}
		return isVerified;
	}
}