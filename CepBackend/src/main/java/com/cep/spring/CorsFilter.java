package com.cep.spring;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import org.springframework.stereotype.Component;

@Component
public class CorsFilter implements Filter {

    private Logger logger = LogManager.getLogger(this.getClass().getName());
    // Need to put list of all allowed calling domains here;
    // When we have a different local value, add it here;
    // When we have a new prod or dev value, add it to the list
    private String[] allowedDomains = 
          {"http://localhost:4000", 
           "http://localhost:4200", 
           "https://localhost:4200", 
           "http://localhost:8080", 
           "http://localhost", 
           "http://sandbox.careersinthemilitary.com", 
           "http://sandbox.careersinthemilitary.com:8080", 
           "http://dev-www.careersinthemilitary.com",
           "http://dev-www.asvabprogram.com",
           "http://www.careersinthemilitary.com",
           "https://localhost", 
           "https://sandbox.careersinthemilitary.com", 
           "https://sandbox.careersinthemilitary.com:8080", 
           "https://dev-www.careersinthemilitary.com",
           "https://dev-www.asvabprogram.com",
           "https://www.careersinthemilitary.com"

           };
    
    // private String corsFilter="*";

    public CorsFilter() {
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        final HttpServletResponse response = (HttpServletResponse) res;
        final HttpServletRequest request = (HttpServletRequest) req;

        Set<String> allowedOrigins = new HashSet<String>(Arrays.asList(allowedDomains));

        String originHeader = request.getHeader("Origin");

        if (allowedOrigins.contains(originHeader)) {
            if (this.setData(request, response, chain, originHeader)) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                chain.doFilter(req, res);
            }
        } else {
            String url = request.getPathInfo();
            String query = request.getQueryString();
            if ((url.contains("sso/confirm_login") && query.contains("login_callback/test")) || url.contains("login_callback/test")) {
                if (this.setData(request, response, chain, "https://www.careersinthemilitary.com")) {
                    response.setStatus(HttpServletResponse.SC_OK);
                } else {
                    chain.doFilter(req, res);
                }

            } else {
                logger.error("CORS Error - Cross-Origin Resource Sharing is not allowed for " + originHeader);
                throw new ServletException(
                    "CORS Error - Cross-Origin Resource Sharing is not allowed for " + originHeader);
            }
        }
    }

    @Override
    public void init(final FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    private boolean setData(HttpServletRequest request, HttpServletResponse response, FilterChain chain, String originHeader) {
        response.setHeader("Access-Control-Allow-Origin", originHeader);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods",
                "POST, GET, PUT, OPTIONS, PATCH , DELETE, HEAD");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", " Content-Type, Authorization");

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            return true;
        } else {
            return false;
        }
    } 

}