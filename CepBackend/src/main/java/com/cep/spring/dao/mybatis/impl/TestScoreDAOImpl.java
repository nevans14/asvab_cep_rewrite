package com.cep.spring.dao.mybatis.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.dao.mybatis.service.TestScoreService;
import com.cep.spring.model.testscore.FilterScoreObj;
import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;

@Primary
@Repository
public class TestScoreDAOImpl implements TestScoreDao{

	@Autowired
	private TestScoreService service;
	
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    
	/**
	 * Insert student's test score record.
	 * @param testScoreObjects List<StudentTestingProgam>
	 * @return
	 */
	public int insertTestScoreimpl(List<StudentTestingProgam> testScoreObjects) {
		return service.insertTestScore(testScoreObjects);

	}

	/**
	 * Retrieve student's test score record with access code.
	 * 
	 * @param accessCode
	 * @return
	 * @throws Exception
	 */
	public StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode) {
		StudentTestingProgamSelects results = new StudentTestingProgamSelects();

		try {
			results = service.getTestScoreWithAccessCode(accessCode);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}
	
	/**
	 * Retrieve student's test score record with user id.
	 * 
	 * @param accessCode
	 * @return
	 * @throws Exception
	 */
	public StudentTestingProgamSelects getTestScoreWithUserId(Integer userId) {
		StudentTestingProgamSelects results = new StudentTestingProgamSelects();

		try {
			results = service.getTestScoreWithUserId(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}
	
	@Override
	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return service.insertManualTestScore(manualTestScoreObj);
	}

	@Override
	public ManualTestScore getManualTestScore(int userId) {
		ManualTestScore results = new ManualTestScore();

		try {
			results = service.getManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	@Override
	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return  service.updateManualTestScore(manualTestScoreObj);
	}

	@Override
	public int deleteManualTestScore(int userId) {
		return service.deleteManualTestScore(userId);
	}

	@Override
	public List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj) {
		return service.filterTestScore(filterScoreObj);
	}

	@Override
	public int deleteIcatDuplicates(String sourceFileName, String type) {
		return service.deleteIcatDuplicates(sourceFileName, type);
	}

	@Override
	public int getRecordCount(String sourceFileName, String platform) {
		return service.getRecordCount(sourceFileName, platform);
	}
}
