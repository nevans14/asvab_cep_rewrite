package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.MediaCenterService;
import com.cep.spring.model.media.MediaCenter;

@Repository
public class MediaCenterDAOImpl {

	@Autowired
	MediaCenterService service;

	public ArrayList<MediaCenter> getMediaCenterList() throws Exception {
		return service.getMediaCenterList();
	}
	
	public ArrayList<MediaCenter> getMediaCenterByCategoryId(Integer categoryId) throws Exception {
		return service.getMediaCenterByCategoryId(categoryId);
	}

	public MediaCenter getArticleById(int mediaId) throws Exception {
		return service.getArticleById(mediaId);
	}
}
