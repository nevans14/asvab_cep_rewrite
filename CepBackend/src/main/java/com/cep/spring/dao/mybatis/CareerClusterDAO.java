package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;

public interface CareerClusterDAO {

	ArrayList<CareerCluster> getCareerCluster();

	ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId);
}
