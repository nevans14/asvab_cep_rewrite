package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.EssTraining.EssTraining;
import com.cep.spring.model.EssTraining.EssTrainingLocation;
import com.cep.spring.model.EssTraining.EssTrainingRegistration;
import com.cep.spring.model.EssTraining.EssTrainingEmail;

import org.apache.ibatis.annotations.Param;

public interface EssTrainingMapper {
	
	EssTraining getEssTrainingById(@Param("essTrainingId") int essTrainingId);
	
	EssTrainingEmail getEssTrainingEmailById(@Param("essTrainingId") int essTrainingId);
	
	int addEssTraining(@Param("essTraining") EssTraining essTraining);
	
	int removeEssTraining(@Param("essTrainingId") int essTrainingId);
	
	int updateEssTraining(@Param("essTraining") EssTraining essTraining);
	
	int isUserRegistered(@Param("emailAddress") String emailAddress, @Param("essTrainingId") int essTrainingId);
	
	int addEssTrainingRegistration(@Param("essTrainingRegistration") EssTrainingRegistration essTrainingRegistration);
	
}