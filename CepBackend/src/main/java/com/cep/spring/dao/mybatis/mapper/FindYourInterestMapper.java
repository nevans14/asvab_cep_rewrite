package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

public interface FindYourInterestMapper {

	int getProfileExist(@Param("userId") Integer userId);

	int insertProfile(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertTest(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertTestScores(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertAnswers(@Param("fyiObject") FindYourInterestProfile fyiObject);

	ArrayList<Scores> getCombinedAndGenderScore(@Param("userId") int userId);

	String getTestId();

	ArrayList<FYIQuestion> getFYIQuestions();

	ArrayList<GenderScore> getGenderScore(@Param("gender") char gender, @Param("rRawScore") int rRawScore,
			@Param("iRawScore") int iRawScore, @Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore,
			@Param("eRawScore") int eRawScore, @Param("cRawScore") int cRawScore);

	ArrayList<CombinedScore> getCombinedScore(@Param("rRawScore") int rRawScore, @Param("iRawScore") int iRawScore,
			@Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore, @Param("eRawScore") int eRawScore,
			@Param("cRawScore") int cRawScore);

	Integer getNumberTestTaken(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	Integer getOldUserId(@Param("newUserId") int newUserId);

	int updateProfile(@Param("scoreObject") ScoreChoice scoreObject);

	ArrayList<ScoreChoice> getProfile(@Param("userId") int userId);

	String getScoreChoice(@Param("userId") int userId);

	TiedSelection getTiedSelection(@Param("userId") int userId);

	int insertTiedSelection(@Param("tiedObject") TiedSelection tiedObject);
	
	int mergeProfile(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	int mergeTests(@Param("fyiObject") FYIMerge fyiObject);
	
	int mergeResults(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	int mergeAnswers(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);

	FYIMerge getLastTestObject(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	int deleteProfile(@Param("userId") int userId);

}
