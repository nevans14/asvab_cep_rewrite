package com.cep.spring.dao.mybatis;

import com.cep.spring.model.HomepageImage;

public interface HomeDAO {
	
	HomepageImage getCurrentHomepageImg();
	
}