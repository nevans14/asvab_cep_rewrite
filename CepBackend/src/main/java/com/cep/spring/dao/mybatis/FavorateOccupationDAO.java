package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

public interface FavorateOccupationDAO {

	ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception;

	int insertFavoriteOccupation(FavoriteOccupation favoriteObject) throws Exception;

	int deleteFavoriteOccupation(Integer id) throws Exception;
	
	int deleteFavoriteOccupationByUserId(Integer UserId) throws Exception;

	ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(Integer userId) throws Exception;

	int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject) throws Exception;

	int deleteFavoriteCareerCluster(Integer id) throws Exception;
	
	int deleteFavoriteCareerClusterByUserId(Integer userId) throws Exception;

	ArrayList<FavoriteSchool> getFavoriteSchool(Integer userId) throws Exception;

	int insertFavoriteSchool(FavoriteSchool favoriteObject) throws Exception;

	int deleteFavoriteSchool(Integer userId) throws Exception;
	
	int deleteFavoriteSchoolByUserId(Integer userId) throws Exception;
	
	int insertFavoriteCitmOccupation(FavoriteCitmOccupation favoriteObject) throws Exception;

	int deleteFavoriteCitmOccupationByUserId(Integer userId) throws Exception;

	ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(Integer userId) throws Exception;
	
	int insertFavoriteService(FavoriteMilitaryService favoriteObject) throws Exception;

	int deleteFavoriteServiceByUserId(Integer userId) throws Exception;
	
	ArrayList<FavoriteMilitaryService> getFavoriteService(Integer userId) throws Exception;
}