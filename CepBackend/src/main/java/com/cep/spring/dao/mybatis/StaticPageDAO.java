package com.cep.spring.dao.mybatis;

import com.cep.spring.model.StaticPage;

public interface StaticPageDAO {
	StaticPage getPageById(int pageId);

    StaticPage getPageByPageNameOld(String pageName);

    StaticPage getPageByPageName(String pageName, String website);
}