package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.cep.spring.dao.mybatis.mapper.FindYourInterestMapper;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

@Service
public class FindYourInterestService {
	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	public FindYourInterestMapper findYourInterestMapper;

	// TODO: Investigate why transactional not working and fix it
	@Transactional(propagation = Propagation.REQUIRED)
	public Boolean insertProfileAndScores(FindYourInterestProfile fyiObject) {
		logger.info("inside the insertProfile method. is wrapped in transaction: "
				+ TransactionSynchronizationManager.isActualTransactionActive());
		// check to see if user has a find your interest profile
		int numRowSelectedProfile = findYourInterestMapper.getProfileExist(fyiObject.getUserId());

		int numRowInsertedProfile = 0;

		// if fyi user profile doesn't exist then create one
		if (numRowSelectedProfile == 0) {
			numRowInsertedProfile = findYourInterestMapper.insertProfile(fyiObject);
		}

		// test id is created here
		int numRowInsertedTest = findYourInterestMapper.insertTest(fyiObject);
		if (numRowInsertedTest > 0) {

		}
		// uses test id in the previous call as a foreign key here
		int numRowInsertedScore = findYourInterestMapper.insertTestScores(fyiObject);

		// saves user's test answers if they took the test
		int numRowInsertedAnswers = 0;
		if (fyiObject.getAnswers() != null) {
			numRowInsertedAnswers = findYourInterestMapper.insertAnswers(fyiObject);
		}

		// check to see if all necessary data were inserted
		boolean allInserted = false;
		if ((numRowInsertedProfile == 1 || numRowSelectedProfile == 1)
				&& (fyiObject.getAnswers() == null || numRowInsertedAnswers == 1) && numRowInsertedTest == 1
				&& numRowInsertedScore == 6) {
			allInserted = true;
		}

		return allInserted;
	}

	public ArrayList<Scores> getCombinedAndGenderScore(Integer userId) {
		return findYourInterestMapper.getCombinedAndGenderScore(userId);
	}

	public String getTestId() {
		return findYourInterestMapper.getTestId();
	}

	public ArrayList<FYIQuestion> getFYIQuestions() {
		return findYourInterestMapper.getFYIQuestions();
	}

	public ArrayList<GenderScore> getGenderScore(char gender, int rRawScore, int iRawScore, int aRawScore,
			int sRawScore, int eRawScore, int cRawScore) {
		return findYourInterestMapper.getGenderScore(gender, rRawScore, iRawScore, aRawScore, sRawScore, eRawScore,
				cRawScore);
	}

	public ArrayList<CombinedScore> getCombinedScore(int rRawScore, int iRawScore, int aRawScore, int sRawScore,
			int eRawScore, int cRawScore) {
		return findYourInterestMapper.getCombinedScore(rRawScore, iRawScore, aRawScore, sRawScore, eRawScore,
				cRawScore);
	}

	public Integer getNumberTestTaken(int oldUserId, int newUserId) {
		return findYourInterestMapper.getNumberTestTaken(oldUserId, newUserId);
	}

	@Transactional
	public int updateProfile(ScoreChoice scoreChoice) {
		return findYourInterestMapper.updateProfile(scoreChoice);
	}

	public ArrayList<ScoreChoice> getProfile(int userId) {
		return findYourInterestMapper.getProfile(userId);
	}
	
	@Transactional
	public int insertProfile(FindYourInterestProfile fyiObject) {
		return findYourInterestMapper.insertProfile(fyiObject);
	}

	public int getProfileExist(Integer userId) {
		return findYourInterestMapper.getProfileExist(userId);
	}

	public String getScoreChoice(int userId) {
		return findYourInterestMapper.getScoreChoice(userId);
	}

	public TiedSelection getTiedSelection(int userId) {
		return findYourInterestMapper.getTiedSelection(userId);
	}

	@Transactional
	public int insertTiedSelection(TiedSelection tiedObject) {
		return findYourInterestMapper.insertTiedSelection(tiedObject);
	}
	
	@Transactional
	public int mergeProfile(int oldUserId, int newUserId) {
		return findYourInterestMapper.mergeProfile(oldUserId, newUserId);
	}

	@Transactional
	public int mergeTests(FYIMerge fyiObject) {
		return findYourInterestMapper.mergeTests(fyiObject);
	}
	
	@Transactional
	public int mergeResults(int oldUserId, int newUserId) {
		return findYourInterestMapper.mergeResults(oldUserId, newUserId);
	}
	
	@Transactional
	public int mergeAnswers(int oldUserId, int newUserId) {
		return findYourInterestMapper.mergeAnswers(oldUserId, newUserId);
	}

	public FYIMerge getLastTestObject(int oldUserId, int newUserId) {
		return findYourInterestMapper.getLastTestObject(oldUserId, newUserId);
	}

	public int deleteProfile(int userId) {
		return findYourInterestMapper.deleteProfile(userId);
	}

	public Integer getOldUserId(Integer newUserId) {
		return findYourInterestMapper.getOldUserId(newUserId);
	}
}
