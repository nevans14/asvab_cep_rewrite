package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.CareerClusterDAO;
import com.cep.spring.dao.mybatis.service.CareerClusterService;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Repository
public class CareerClusterDAOImpl implements CareerClusterDAO {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	private CareerClusterService service;

	/**
	 * Returns career cluster list.
	 */
	public ArrayList<CareerCluster> getCareerCluster() {
		ArrayList<CareerCluster> careerClusters = new ArrayList<CareerCluster>();

		try {
			careerClusters = service.getCareerCluster();
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}

		return careerClusters;
	}

	/**
	 * Returns career occupations of the selected career cluster.
	 */
	public ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId) {
		ArrayList<CareerClusterOccupation> careerClustersOccupations = new ArrayList<CareerClusterOccupation>();

		try {
			careerClustersOccupations = service.getCareerClusterOccupation(ccId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}

		return careerClustersOccupations;
	}

	/**
	 * Returns skill importance in decimal values for math, verbal and
	 * science/technical.
	 * 
	 * @param onetSoc
     * @return
	 */
	public SkillImportance getSkillImportance(String onetSoc) {
		SkillImportance skillImportance = null;

		try {
			skillImportance = service.getSkillImportance(onetSoc);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return skillImportance;
	}

	/**
	 * Returns the occupation tasks with a limit of 5 records.
	 * @param onetSoc
     * @return
	 */
	public ArrayList<Task> getTasks(String onetSoc) {
		ArrayList<Task> occupationTasks = new ArrayList<Task>();

		try {
			occupationTasks = service.getTasks(onetSoc);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return occupationTasks;
	}

	/**
	 * Returns interest codes for an occupation.
     * @param onetSoc
     * @return
	 */
	public OccupationInterestCode getOccupationInterestCodes(String onetSoc) {
		OccupationInterestCode interestCodes = new OccupationInterestCode();

		try {
			interestCodes = service.getOccupationInterestCodes(onetSoc);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return interestCodes;
	}

	/**
	 * Returns required education level data for an occupation to create pie
	 * chart.
     * @param onetSoc
     * @return
	 */
	public ArrayList<OccupationEducationLevel> getEducationLevel(String onetSoc) {
		ArrayList<OccupationEducationLevel> educationLevel = new ArrayList<OccupationEducationLevel>();

		try {
			educationLevel = service.getEducationLevel(onetSoc);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}

		return educationLevel;
	}

	/**
	 * Returns a list of related careers.
     * @param onetSoc
     * @return
     *  
	 */
	public ArrayList<RelatedOccupation> getRelatedOccupations(String onetSoc) {
		ArrayList<RelatedOccupation> relatedOccupation = new ArrayList<RelatedOccupation>();

		try {
			relatedOccupation = service.getRelatedOccupations(onetSoc);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}

		return relatedOccupation;
	}

	/**
	 * Returns list of Career Cluster occupations.
	 * 
	 * @param ccId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<OccufindSearch> getCareerClusterOccupations(Integer ccId) throws Exception {
		return service.getCareerClusterOccupations(ccId);
	}

	/**
	 * Returns career cluster title based off of career cluster identifier.
	 * 
	 * @param ccId
	 * @return
	 * @throws Exception
	 */
	public CareerCluster getCareerClusterById(Integer ccId) throws Exception {
		return service.getCareerClusterById(ccId);
	}

}
