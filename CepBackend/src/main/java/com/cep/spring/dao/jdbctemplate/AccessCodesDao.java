package com.cep.spring.dao.jdbctemplate;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.model.AccessCodes;
import com.cep.spring.utils.SimpleUtils;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * AccessCodesDao Implements the crud operations for the AccessCodesDao Object
 * to an from access_codes table.
 * <ul>
 * <li>Create -- batchInsertAccessCodess</li>
 * <li>Read -- readAccessCodess</li>
 * <li>Update -- updateAccessCodess</li>
 * <li>Delete -- deleteAccessCodess</li>
 * </ul>
 * 
 * @author Dave Springer
 *
 */
@Component
@Transactional
public class AccessCodesDao {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
//	@Autowired
//	DataSource ds;
	@Autowired
	JdbcTemplate jdbcTemplate;

	// Create
	/**
	 * Insert a singleAccessCodes
	 * 
	 * @param accesscodes
	 * 
	 */
	@Transactional
	public int insert(AccessCodes accesscodes) {
	    logger.info("insert");
		String sql = " INSERT INTO access_codes ( " + " access_code , expire_date , role " + " ) VALUES ( " + "'"
				+ accesscodes.getAccessCode() + "','"
				+ SimpleUtils.getStringInStandardFormatFromDate(accesscodes.getExpireDate()) + "','"
				+ accesscodes.getRole() + "') ;";

		System.out.println(sql);
		int rowsInserted = jdbcTemplate.update(sql);
		logger.debug("insert statement:" + sql);
		Long pk = jdbcTemplate.queryForObject("SELECT user_id FROM access_codes where access_code = ?",
				new Object[] { accesscodes.getAccessCode() }, Long.class);

		accesscodes.setUserId(pk);

		return rowsInserted;
	}

	// Read
	/**
	 * select a singleAccessCodes based on status key
	 * 
	 * @param primaryKey
	 * 
	 */
	@Transactional
	public AccessCodes select(Long primaryKey) {
	    logger.info("select");
	    logger.debug("for pk " + primaryKey );
		List<AccessCodes> recList = jdbcTemplate.query(" SELECT * " + " FROM access_codes" + " WHERE user_id = ? ",
				new AccessCodes_mapper(false), primaryKey);

		return recList.get(0);
	}

	// Update
	/**
	 * Insert a singleAccessCodes
	 * 
	 * @param accesscodes
	 * @return
	 * 
	 */
	@Transactional
	public int update(AccessCodes accesscodes) {
	    logger.info("update");
		String sql = " UPDATE access_codes SET " + " access_code = '" + accesscodes.getAccessCode() + "' , "
				+ " expire_date = '" + SimpleUtils.getStringInStandardFormatFromDate(accesscodes.getExpireDate())
				+ "' , " + " role = '" + accesscodes.getRole() + "'" + " WHERE user_id = " + accesscodes.getUserId();
		logger.debug("update sql:" + sql);
		System.out.println(sql);
		int rowsInserted = jdbcTemplate.update(sql);

		return rowsInserted;
	}

	// Delete
	/**
	 * delete a singleAccessCodes based on primary key
	 * 
	 * @param primaryKey
	 * 
	 */
	@Transactional
	public void delete(Long primaryKey) {
	    logger.info("delete");
	    logger.debug("pk:" + primaryKey);
		jdbcTemplate.execute(" DELETE " + " FROM access_codes" + " WHERE user_id = " + primaryKey + " ");

	}

	// ******** List Functions utilizing collections and jdbc batch update

	// *** Crude for lists ***
	// Create
	// Read
	/**
	 * readAccessCodess - Read from access_codes table via a paging query based
	 * on the following where clause where BatchId = ? AND BatchStatus Not x and
	 * SubjectStatus Not y and page = 1
	 * 
	 * @return
	 */
	public List<AccessCodes> readAccessCodess() {
	    logger.info("readAccessCodess: top 100");
	    
		List<AccessCodes> spnidList = jdbcTemplate.query(" SELECT * " + " FROM access_codes " + " WHERE rownum < 100",
				new AccessCodes_mapper(false));

		return spnidList;
	}

	/**
	 * readAccessCodess - Read from access_codes table via a paging query based
	 * on the following where clause where BatchId = ? AND BatchStatus Not x and
	 * SubjectStatus Not y and page = 1
	 * 
     * @param ac
	 * @return
	 */
	public List<AccessCodes> readAccessCodesWithObject(AccessCodes ac) {
	    logger.info("readAccessCodesWithObject");
	    StringBuffer whereClause = new StringBuffer();

		if (null != ac.getAccessCode() && !ac.getAccessCode().trim().equalsIgnoreCase("")) {
			whereClause.append("access_code like '%" + ac.getAccessCode().trim() + "%' AND  \n");
		}
		if (null != ac.getRole() && !ac.getRole().trim().equalsIgnoreCase("")) {
			whereClause.append("role = '" + ac.getRole() + "' AND  \n");
		}
		if (!(ac.getExpireDate() == null)) {
			// whereClause =
			System.out.println(SimpleUtils.getStringInStandardFormatFromDate(ac.getExpireDate()));
			// String where = SimpleUtils.removeLast(whereClause.toString(), "
			// AND ") ;
			whereClause.append(
					"expire_date = '" + SimpleUtils.getStringInStandardFormatFromDate(ac.getExpireDate()) + "' AND ");

		}
		if (!(ac.getUserId() == null)) {
			whereClause.append("user_id = " + Long.toString(ac.getUserId()) + " AND ");
		}
		String where = "" ;
		if ( whereClause.length() > 4 ) {
			where = " WHERE " + SimpleUtils.removeLast(whereClause.toString(), " AND ");
		}
		logger.debug("where clause" + where);
		List<AccessCodes> spnidList = jdbcTemplate.query(" SELECT TOP 200 * " + " FROM access_codes " + where,
				new AccessCodes_mapper(false));

		return spnidList;
	}

	/**
	 * readAccessCodess - Read from access_codes table via a paging query based
	 * on the following where clause where BatchId = ? AND BatchStatus Not x and
	 * SubjectStatus Not y and page = 1
	 * 
     * @param code
	 * @return
	 */
	public List<AccessCodes> readAccessCodess(String code) {
	    logger.info("readAccessCodess");
		List<AccessCodes> spnidList = jdbcTemplate.query(
				" SELECT  TOP 100  * " + " FROM access_codes " + " WHERE  access_code like ?  ",
				new AccessCodes_mapper(false), code + "%");

		return spnidList;
	}

	/**
	 * readAccessCodess - Read from access_codes table via a paging query based
	 * on the following where clause where BatchId = ? AND BatchStatus Not x and
	 * SubjectStatus Not y and page = 1
	 * 
     * @param role
	 * @return
	 */
	public List<AccessCodes> readAccessCodesByRole(String role) {
	    logger.info("readAccessCodesByRole");
		List<AccessCodes> acList = null;

		if (role.equalsIgnoreCase("0")) {
			acList = jdbcTemplate.query(" SELECT   * " + " FROM access_codes", new AccessCodes_mapper(false));
		} else {
			acList = jdbcTemplate.query(" SELECT   * " + " FROM access_codes WHERE  role like ?  ",
					new AccessCodes_mapper(false), role + "%");
		}

		return acList;
	}

	// Update
	/**
	 * updateAccessCodess
	 * 
	 * @param rList
	 * @return 
	 */
	@Transactional
	public int [] updateAccessCodess(final List<AccessCodes> rList) {
	    logger.info("updateAccessCodess");
		String sql = " Update access_codes set " + 
				" access_code = ? , " + // 1
				" expire_date = ? , " + // 2
				" role 		= ?  " + // 3
				" where user_id  = ? "; // 4

		int updates[] = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				AccessCodes r = rList.get(i);
				ps.setString(1, r.getAccessCode()); // 1
				ps.setDate(2, (java.sql.Date) r.getExpireDate()); // 2
				ps.setString(3, r.getRole()); // 3
				ps.setLong(4, r.getUserId()); // 4
			}

			@Override
			public int getBatchSize() {
				return rList.size();
			}

		});
		
		return updates;
	}

	// Delete
	/**
	 * deleteAccessCodess
	 * 
	 * @param rList
	 */
	@Transactional
	public void deleteAccessCodess(final List<AccessCodes> rList) {
	    logger.info("deleteAccessCodess");

		// Delete from ...
		String sql = "delete from access_codes " + " where user_id = ? "; // 1

		int updates[] = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				AccessCodes r = rList.get(i);

				// Set Values TODO: change to use tables PrimaryKey
				ps.setLong(1, r.getUserId());
			}

			@Override
			public int getBatchSize() {
				return rList.size();
			}

		});
	}
	// insert batch example
	public int[] insertBatch(final List<AccessCodes> accesscodess) {
	    logger.info("insertBatch");
		String sql = "INSERT INTO access_codes " + "(access_code,expire_date,role) VALUES ( ? , ? , ? )";

		int[] returnStatus = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				AccessCodes accesscodes = accesscodess.get(i);
				ps.setString(1, accesscodes.getAccessCode());
				ps.setDate(2, new java.sql.Date(accesscodes.getExpireDate().getTime()));
				ps.setString(3, accesscodes.getRole());
			}

			@Override
			public int getBatchSize() {
				return accesscodess.size();
			}
		});

		return returnStatus;
	}
	/**
	 * Custom access code mapper 
	 * 
	 * @author Dave Springer
	 *
	 */
	class AccessCodes_mapper implements RowMapper<AccessCodes> {

		private Long userId;
		private String accessCode;
		private Date expireDate;
		private String role;
		private Boolean generateTestInserts = false;

		public AccessCodes_mapper() {
		}

		public AccessCodes_mapper(Boolean generateTestInserts) {
			this.generateTestInserts = generateTestInserts;
		}

		public AccessCodes mapRow(ResultSet rs, int i) throws SQLException {
			ResultSetMetaData rsmd = rs.getMetaData();
			Integer count = rsmd.getColumnCount();
			for (int j = 1; j <= count; j++) {
				String s = rsmd.getColumnName(j).trim().toUpperCase();
				try {
					switch (s) {
					case "USER_ID":
						userId = rs.getLong(rs.findColumn(s));
						break;
					case "ACCESS_CODE":
						accessCode = rs.getString(rs.findColumn(s));
						break;
					case "EXPIRE_DATE":
						expireDate = rs.getDate(rs.findColumn(s));
						break;
					case "ROLE":
						role = rs.getString(rs.findColumn(s));
						break;

					default:
						break;
					}

				} catch (SQLException se) {
					System.out.println("*** Warning ***********************************************************");
					System.out.println(
							s.toString() + " Column not found in configured query, this may cause and issue reporting");
				}

			}

			AccessCodes newAccessCodes = new AccessCodes();
			newAccessCodes.setUserId(userId);
			newAccessCodes.setAccessCode(accessCode);
			newAccessCodes.setExpireDate(expireDate);
			newAccessCodes.setRole(role);

			return newAccessCodes;

		}

	}

	@Transactional
	public List<AccessCodes> reserveAccessCodesByRole(String role, Integer quantity) {
	    logger.info("reserveAccessCodesByRole");

	    List<AccessCodes> acList = null;

		String sql = " SELECT  TOP  " + quantity + "  *  FROM access_codes WHERE  role = ? AND expire_date is NULL ";
		    
	    acList = jdbcTemplate.query(sql,
					new AccessCodes_mapper(false), role);
	    
	    
	    if ( acList.size() == 0 ) return null;
	    
	    for ( AccessCodes ac : acList) {
//	    	Date initDate = SimpleUtils.getDateFromStringInStandardFormat("1776-07-04 00:00:00.000");
	    	ac.setExpireDate(java.sql.Date.valueOf("1776-07-04"));
	    }

	    int [] retCodes = updateAccessCodess(acList);
	    for ( int i = 0 ; i < retCodes.length ; i++ ) {
	    	if ( retCodes[i] != 1 ) throw new RuntimeException("Error Updating Access Codes");
	    }
	    
	    
		return acList;
	}

}
