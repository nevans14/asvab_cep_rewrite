package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.dao.mybatis.service.FindYourInterestService;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

@Repository
public class FindYourInterestDAOImpl implements FindYourInterestDAO {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	FindYourInterestService service;

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#insertProfileAndScores(com.cep.spring.model.fyi.FindYourInterestProfile)
	 */
	@Override
	public Boolean insertProfileAndScores(FindYourInterestProfile fyiObject) {
		boolean allInserted = false;

		try {
			allInserted = service.insertProfileAndScores(fyiObject);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return allInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getCombinedAndGenderScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<Scores> getCombinedAndGenderScore(Integer userId) {
		ArrayList<Scores> scores = new ArrayList<Scores>();

		try {
			scores = service.getCombinedAndGenderScore(userId);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return scores;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getTestId()
	 */
	@Override
	public String getTestId() {
		String testId = null;
		System.out.println("here");
		try {
			testId = service.getTestId();
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return testId;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getFYIQuestions()
	 */
	@Override
	public ArrayList<FYIQuestion> getFYIQuestions() {
		ArrayList<FYIQuestion> fyiQuestions = new ArrayList<FYIQuestion>();
		try {
			fyiQuestions = service.getFYIQuestions();
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return fyiQuestions;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getGenderScore(com.cep.spring.model.FYIRawScore, char)
	 */
	@Override
	public ArrayList<GenderScore> getGenderScore(FYIRawScore rawScores, char gender) {
		int rRawScore = rawScores.getrRawScore();
		int iRawScore = rawScores.getiRawScore();
		int aRawScore = rawScores.getaRawScore();
		int sRawScore = rawScores.getsRawScore();
		int eRawScore = rawScores.geteRawScore();
		int cRawScore = rawScores.getcRawScore();

		ArrayList<GenderScore> genderScore = new ArrayList<GenderScore>();
		try {
			genderScore = service.getGenderScore(gender, rRawScore, iRawScore, aRawScore, sRawScore, eRawScore,
					cRawScore);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return genderScore;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getCombinedScore(com.cep.spring.model.FYIRawScore)
	 */
	@Override
	public ArrayList<CombinedScore> getCombinedScore(FYIRawScore rawScores) {
		int rRawScore = rawScores.getrRawScore();
		int iRawScore = rawScores.getiRawScore();
		int aRawScore = rawScores.getaRawScore();
		int sRawScore = rawScores.getsRawScore();
		int eRawScore = rawScores.geteRawScore();
		int cRawScore = rawScores.getcRawScore();

		ArrayList<CombinedScore> combinedScore = new ArrayList<CombinedScore>();
		try {
			combinedScore = service.getCombinedScore(rRawScore, iRawScore, aRawScore, sRawScore, eRawScore, cRawScore);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return combinedScore;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getNumberTestTaken(java.lang.Integer)
	 */
	@Override
	public Integer getNumberTestTaken(Integer oldUserId, Integer newUserId) {

		Integer numTestTaken = 0;

		try {
			if(oldUserId == null)
				oldUserId = 9090909;
			numTestTaken = service.getNumberTestTaken(oldUserId, newUserId);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}

		return numTestTaken;
	}
	

	@Override
	public Integer getOldUserId(Integer newUserId) {
		return service.getOldUserId(newUserId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#updateProfile(com.cep.spring.model.fyi.ScoreChoice)
	 */
	@Override
	public int updateProfile(ScoreChoice scoreChoice) {
		int rowUpdated = 0;

		try {
			rowUpdated = service.updateProfile(scoreChoice);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}
		return rowUpdated;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getProfile(java.lang.Integer)
	 */
	@Override
	public ArrayList<ScoreChoice> getProfile(Integer userId) {
		ArrayList<ScoreChoice> userInterestCodes = new ArrayList<ScoreChoice>();
		try {
			userInterestCodes = service.getProfile(userId);
		} catch (RuntimeException e) {
			logger.error("Mybatis Exception: ", e);
		}
		return userInterestCodes;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#insertProfile(com.cep.spring.model.fyi.FindYourInterestProfile)
	 */
	@Override
	public Integer insertProfile(FindYourInterestProfile fyiObject) throws Exception {
		Integer numberRowsInserted = null;

		numberRowsInserted = service.insertProfile(fyiObject);

		return numberRowsInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getProfileExist(com.cep.spring.model.fyi.FindYourInterestProfile)
	 */
	@Override
	public Integer getProfileExist(Integer userId) throws Exception {
		Integer numberRowSelected = null;

		numberRowSelected = service.getProfileExist(userId);

		return numberRowSelected;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getScoreChoice(java.lang.Integer)
	 */
	@Override
	public String getScoreChoice(Integer userId) throws Exception {
		return service.getScoreChoice(userId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getTiedSelection(java.lang.Integer)
	 */
	@Override
	public TiedSelection getTiedSelection(Integer userId) throws Exception {
		return service.getTiedSelection(userId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#insertTiedSelection(com.cep.spring.model.fyi.TiedSelection)
	 */
	@Override
	public int insertTiedSelection(TiedSelection tiedObject) {
		return service.insertTiedSelection(tiedObject);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#mergeProfile(int, int)
	 */
	@Override
	public int mergeProfile(int oldUserId, int newUserId) {
		Integer numberRowsInserted = null;

		numberRowsInserted = service.mergeProfile(oldUserId, newUserId);

		return numberRowsInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#mergeTests(int, int)
	 */
	@Override
	public int mergeTests(FYIMerge fyiObject) {
		Integer numberRowsInserted = null;

		numberRowsInserted = service.mergeTests(fyiObject);

		return numberRowsInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#mergeResults(int, int)
	 */
	@Override
	public int mergeResults(int oldUserId, int newUserId) {
		Integer numberRowsInserted = null;

		numberRowsInserted = service.mergeResults(oldUserId, newUserId);

		return numberRowsInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#mergeAnswers(int, int)
	 */
	@Override
	public int mergeAnswers(int oldUserId, int newUserId) {
		Integer numberRowsInserted = null;

		numberRowsInserted = service.mergeAnswers(oldUserId, newUserId);

		return numberRowsInserted;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#getLastTestObject(int, int)
	 */
	@Override
	public FYIMerge getLastTestObject(int oldUserId, int newUserId) {

		return service.getLastTestObject(oldUserId, newUserId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FindYourInteretDAO#deleteProfile(int)
	 */
	@Override
	public int deleteProfile(int userId) {
		return service.deleteProfile(userId);
	}

}
