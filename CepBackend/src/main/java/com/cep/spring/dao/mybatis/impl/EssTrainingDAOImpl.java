package com.cep.spring.dao.mybatis.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.EssTrainingDAO;
import com.cep.spring.dao.mybatis.service.EssTrainingService;
import com.cep.spring.model.EssTraining.*;

@Primary
@Repository
public class EssTrainingDAOImpl implements EssTrainingDAO {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	EssTrainingService service;
	
	public EssTraining getEssTrainingById(int essTrainingId) {
		logger.info("getting ESS Training");
		
		EssTraining results;
		
		results = service.getEssTrainingById(essTrainingId);
		
		return results;
	}
	
	public EssTrainingEmail getEssTrainingEmailById(int essTrainingId) {
		logger.info("getting ESS Training Confirmation Email");
		
		EssTrainingEmail results;
		
		results = service.getEssTrainingEmailById(essTrainingId);
		
		return results;
	}
	
	public int addEssTraining(EssTraining essTraining) {
		logger.info("adding ESS Training");
		
		int results;
		
		results = service.addEssTraining(essTraining);
		
		return results;
	}
	
	public int updateEssTraining(EssTraining essTraining) {
		logger.info("updating ESS Training");
		
		int results;
		
		results = service.updateEssTraining(essTraining);
		
		return results;
	}
	
	public int removeEssTraining(int essTrainingId) {
		logger.info("removing ESS Training");
		
		int results;
		
		results = service.removeEssTraining(essTrainingId);
		
		return results;
	}
	
	public int isUserRegistered(String emailAddress, int essTrainingId) {
		logger.info("checking if user is already registered with email address");
		
		int results;
		
		results = service.isUserRegistered(emailAddress, essTrainingId);
		
		return results;
	}
	
	public int addEssTrainingRegistration(EssTrainingRegistration essTrainingRegistration) {
		logger.info("adding ESS Training Registration");
		
		int results;
		
		results = service.addEssTrainingRegistration(essTrainingRegistration);
		
		return results;
	}
}