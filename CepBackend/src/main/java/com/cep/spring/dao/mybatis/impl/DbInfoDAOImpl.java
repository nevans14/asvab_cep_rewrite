package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.dao.mybatis.service.DbInfoService;
import com.cep.spring.model.DbInfo;

@Repository
public class DbInfoDAOImpl implements DbInfoDAO {
	
	@Autowired
	private DbInfoService service;
	
	public DbInfo getDbInfo(String name) {
		return service.getDbInfo(name);
	}
	
}