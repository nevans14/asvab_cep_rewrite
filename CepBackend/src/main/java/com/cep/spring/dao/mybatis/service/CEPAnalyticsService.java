package com.cep.spring.dao.mybatis.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.CEPAnalyticsMapper;

@Service
public class CEPAnalyticsService {

	@Autowired
	private CEPAnalyticsMapper mapper;

	public int insertCEPShare(String email, String category) {
		return mapper.insertCEPShare(email, category);
	}
}
