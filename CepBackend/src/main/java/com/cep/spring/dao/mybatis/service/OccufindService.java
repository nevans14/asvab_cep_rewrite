package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.OccufindMapper;
import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMajors;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;

@Service
public class OccufindService {

	@Autowired
	OccufindMapper occufindMapper;

	public ArrayList<OccufindSearch> getOccufindSearch(OccufindSearchArguments userSearchArguments) {
		return occufindMapper.getOccufindSearch(userSearchArguments);
	}

	public TitleDescription getOccupationTitleDescription(String onetSoc) {
		return occufindMapper.getOccupationTitleDescription(onetSoc);
	}

	public ArrayList<Occupation> getOccupationASK(String onetSoc) {
		return occufindMapper.getOccupationASK(onetSoc);
	}

	public ArrayList<MilitaryCareerResource> getMilitaryCareerResources(String onetSoc) {
		return occufindMapper.getMilitaryCareerResources(onetSoc);
	}

	public ArrayList<OOHResource> getOOHResources(String onetSoc) {
		return occufindMapper.getOOHResources(onetSoc);
	}

	public ArrayList<StateSalary> getAvgSalaryByState(String onetSocTrimmed) {
		return occufindMapper.getAvgSalaryByState(onetSocTrimmed);
	}
	
	public ArrayList<StateSalaryEntry> getEntrySalaryByState(String onetSocTrimmed) {
		return occufindMapper.getEntrySalaryByState(onetSocTrimmed);
	}

	public Integer getNationalSalary(String onetSocTrimmed) {
		return occufindMapper.getNationalSalary(onetSocTrimmed);
	}
	
	public Integer getNationalEntrySalary(String onetSocTrimmed) {
		return occufindMapper.getNationalEntrySalary(onetSocTrimmed);
	}

	public String getBLSTitle(String onetSocTrimmed) {
		return occufindMapper.getBLSTitle(onetSocTrimmed);
	}

	public ArrayList<OccupationInterestCode> getOccupationInterestCodes(String onetSoc) {
		return occufindMapper.getOccupationInterestCodes(onetSoc);
	}

	public HotGreenStem getHotGreenStemFlags(String onetSoc) {
		return occufindMapper.getHotGreenStemFlags(onetSoc);
	}

	public ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(String onetSoc) {
		return occufindMapper.getServiceOfferingCareer(onetSoc);
	}

	public ArrayList<RelatedCareerCluster> getRelatedCareerCluster(String onetSoc) {
		return occufindMapper.getRelatedCareerCluster(onetSoc);
	}

	public ArrayList<SchoolMoreDetails> getSchoolMoreDetails(String onetSoc) {
		return occufindMapper.getSchoolMoreDetails(onetSoc);
	}

	public Integer getStateSalaryYear() {
		return occufindMapper.getStateSalaryYear();
	}

	public ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(String trimmedOnetSoc) {
		return occufindMapper.getEmploymentMoreDetails(trimmedOnetSoc);
	}

	public ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(String onetSoc) {
		return occufindMapper.getMilitaryMoreDetails(onetSoc);
	}

	public ArrayList<MilitaryHotJobs> getMilitaryHotJobs(String onetSoc) {
		return occufindMapper.getMilitaryHotJobs(onetSoc);
	}

	public ArrayList<OccufindMoreResources> getOccufindMoreResources(String onetSoc) {
		return occufindMapper.getOccufindMoreResources(onetSoc);
	}

	public ArrayList<OccufindSearch> getDashboardBrightOutlook(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return occufindMapper.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> getDashboardStemCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return occufindMapper.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> getDashboardGreenCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return occufindMapper.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> viewAllOccupations() {
		return occufindMapper.viewAllOccupations();
	}
	
	public ArrayList<OccufindSearch> viewMilitaryOccupations() {
		return occufindMapper.viewMilitaryOccupations();
	}

	public ArrayList<OccufindSearch> viewHotOccupations() {
		return occufindMapper.viewHotOccupations();
	}
	
	public ArrayList<OccufindAlternative> onetHideDetails(String onetSoc) {
		return occufindMapper.onetHideDetails(onetSoc);
	}

	public String getImageAltText(String onetSoc) {
		return occufindMapper.getImageAltText(onetSoc);
	}
	
	public JobZone getJobZone(String onetSoc) {
		return occufindMapper.getJobZone(onetSoc);
	}

	public CertificateExplanation getCertificateExplanation(String onetSoc) {
		return occufindMapper.getCertificateExplanation(onetSoc);
	}

	public List<AlternateTitles> getAlternateTitles(String onetSoc) {
		return occufindMapper.getAlternateTitles(onetSoc);
	}

	public SchoolProfile getSchoolProfile(int unitId) {
		return occufindMapper.getSchoolProfile(unitId);
	}

	public List<String> getCareerMajors(String onetCode) {
		return occufindMapper.getCareerMajors(onetCode);
	}

}
