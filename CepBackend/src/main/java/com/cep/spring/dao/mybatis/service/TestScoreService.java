package com.cep.spring.dao.mybatis.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.TestScoreMapper;
import com.cep.spring.model.testscore.FilterScoreObj;
import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;

@Service
public class TestScoreService {
	
    private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	private static final String TXT_FILE_FORMAT = ".txt";
	private static final String ZIP_FILE_FORMAT = ".zip";
	
	@Autowired
	TestScoreMapper mapper;
		
	public int insertTestScore(List<StudentTestingProgam> testScoreObjects) {
		return mapper.insertTestScore(testScoreObjects);
	}
	
	public StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode) {
		StudentTestingProgamSelects testScore = mapper.getTestScoreWithAccessCode(accessCode);
		if (testScore == null)
			testScore = mapper.getMockTestScoreWithAccessCode(accessCode);
		System.out.println("accessCode: "+ accessCode);
		return testScore;
	}
	
	public StudentTestingProgamSelects getTestScoreWithUserId(Integer userId) {
		StudentTestingProgamSelects testScore = mapper.getTestScoreWithUserId(userId);
		if (testScore == null)
			testScore = mapper.getMockTestScoreWithUserId(userId);
		return testScore;
	}

	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.insertManualTestScore(manualTestScoreObj);
	}

	public ManualTestScore getManualTestScore(int userId) {
		return mapper.getManualTestScore(userId);
	}

	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.updateManualTestScore(manualTestScoreObj);
	}

	public int deleteManualTestScore(int userId) {
		return mapper.deleteManualTestScore(userId);
	}

	public List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj) {
		return mapper.filterTestScore(filterScoreObj.getAdministeredYear(), 
									  filterScoreObj.getAdministeredMonth(), 
									  filterScoreObj.getLastName(),
									  filterScoreObj.getGender(),
									  filterScoreObj.getEducationLevel(), 
									  filterScoreObj.getSchoolCode(), 
									  filterScoreObj.getSchoolAddress(), 
									  filterScoreObj.getSchoolCity(),
									  filterScoreObj.getSchoolState(), 
									  filterScoreObj.getSchoolZipCode(), 
									  filterScoreObj.getStudentCity(), 
									  filterScoreObj.getStudentState(), 
									  filterScoreObj.getStudentZipCode());
	}
	
	/**
	* This method allow the user to download a file
	* @author Avishek Roy
	*/

	@SuppressWarnings("unchecked")
	public void downloadFile(HttpServletResponse response, Object source, String name) {
		String disposition = null;
		InputStream inputStream = null;
		try {
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

			if (source instanceof String) {
				if (name == null)
					name = "testscores_" + formatter.format(now) + TXT_FILE_FORMAT;
				disposition = "attachment; fileName=" + name;

				inputStream = new ByteArrayInputStream(((String) source).getBytes());
				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", disposition);
				response.setHeader("content-Length", String.valueOf(stream(inputStream, response.getOutputStream())));
				inputStream.close();
			} else {
				if (source != null) {
					if (((List<File>) source).size() == 1) {

						File file = ((List<File>) source).get(0);
						String fileName = (file.getName());

						inputStream = new BufferedInputStream(new FileInputStream((File) file));
						disposition = "attachment; fileName=" + fileName;
						response.setContentType("text/csv");
						response.setHeader("Content-Disposition", disposition);
						response.setHeader("content-Length",
								String.valueOf(stream(inputStream, response.getOutputStream())));
						inputStream.close();

					} else {

						FileInputStream fis = null;
						ZipOutputStream zos = null;

						try {
							byte[] buffer = new byte[1024];
							response.setContentType("application/zip");
							response.setHeader("Content-Disposition",
									"attachment; filename=" + formatter.format(now) + ZIP_FILE_FORMAT);
							zos = new ZipOutputStream(response.getOutputStream());

							for (File file : ((List<File>) source)) {
								fis = new FileInputStream(file);
								zos.putNextEntry(new ZipEntry(file.getName()));

								int length;
								while ((length = fis.read(buffer)) > 0) {
									zos.write(buffer, 0, length);
								}
								zos.closeEntry();
							}

						} catch (IOException ioe) {
							logger.error("ScoreServiceImpl: Error creating zip file: ", ioe);
							throw new RuntimeException("ScoreServiceImpl: Error creating zip file: ", ioe);
						}finally {
							try {
								if(fis!= null)
									fis.close();
							}catch(Exception e){
								logger.error(e);		
							}
							try {
								if(zos!= null)
									zos.close();
							}catch(Exception e){
								logger.error(e);				
							}
						}
					}
				}
			}

		} catch (IOException e) {

			logger.error("ScoreServiceImpl: Error occurred while downloading file {}", e);

		}

	}

	private long stream(InputStream input, OutputStream output) throws IOException {

	ByteBuffer buffer = ByteBuffer.allocate(10240);

	long size = 0;
	ReadableByteChannel inputChannel = Channels.newChannel(input); 
	WritableByteChannel outputChannel = Channels.newChannel(output);
	
	while (inputChannel.read(buffer) != -1) {
		buffer.flip();
		size += outputChannel.write(buffer);
		buffer.clear();
	}
	inputChannel.close();
	outputChannel.close();
	return size;
	}

	public void downloadFile(HttpServletResponse response, Object source) {
	downloadFile(response, source, null);
	}

	public int deleteIcatDuplicates(String sourceFileName, String type) {
		return mapper.deleteIcatDuplicates(sourceFileName, type);
	}
	
	public int getRecordCount(String sourceFileName, String platform) {
		return mapper.getRecordCount(sourceFileName, platform);
	}
}
