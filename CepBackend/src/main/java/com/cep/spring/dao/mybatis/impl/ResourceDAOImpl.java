package com.cep.spring.dao.mybatis.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.ResourceDAO;
import com.cep.spring.dao.mybatis.service.ResourceService;
import com.cep.spring.model.resources.Resource;
import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

@Repository
public class ResourceDAOImpl implements ResourceDAO {
	
	@Autowired
	ResourceService service;
	
	/***
	 * Returns all resources
	 * @return
	 * @throws Exception
	 */
	public List<ResourceTopic> getResources() throws Exception {
		return service.getResources();
	}
	
	/***
	 * Returns all quick links
	 * @return
	 * @throws Exception
	 */
	public List<ResourceQuickLinkTopic> getQuickLinks() throws Exception {
		return service.getQuickLinks();
	}
	
}