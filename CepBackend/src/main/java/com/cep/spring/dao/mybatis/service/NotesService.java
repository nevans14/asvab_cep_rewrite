package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.NotesMapper;
import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;
import com.cep.spring.model.notes.SchoolNotes;

@Service
public class NotesService {

	@Autowired
	NotesMapper mapper;

	public ArrayList<Notes> getNotes(Integer userId) {
		return mapper.getNotes(userId);
	}

	public int insertNote(Notes noteObject) {
		return mapper.insertNote(noteObject);
	}

	public int updateNote(Notes noteObject) {
		return mapper.updateNote(noteObject);
	}
	
	public int deleteNote(Integer userId, Integer noteId) {
		return mapper.deleteNote(userId, noteId);
	}

	public String getOccupationNotes(Integer userId, String socId) {
		return mapper.getOccupationNotes(userId, socId);
	}
	
	public ArrayList<CareerClusterNotes> getCareerClusterNotes(Integer userId) {
		return mapper.getCareerClusterNotes(userId);
	}

	public int insertCareerClusterNote(CareerClusterNotes noteObject) {
		return mapper.insertCareerClusterNote(noteObject);
	}

	public int updateCareerClusterNote(CareerClusterNotes noteObject) {
		return mapper.updateCareerClusterNote(noteObject);
	}
	
	public int deleteCareerClusterNote(Integer userId, Integer ccNoteId) {
		return mapper.deleteCareerClusterNote(userId, ccNoteId);
	}

	public String getCareerClusterNote(Integer userId, Integer ccId) {
		return mapper.getCareerClusterNote(userId, ccId);
	}
	
	public ArrayList<SchoolNotes> getSchoolNotes(Integer userId) {
		return mapper.getSchoolNotes(userId);
	}

	public int insertSchoolNote(SchoolNotes noteObject) {
		return mapper.insertSchoolNote(noteObject);
	}

	public int updateSchoolNote(SchoolNotes noteObject) {
		return mapper.updateSchoolNote(noteObject);
	}
	
	public int deleteSchoolNote(Integer userId, Integer schoolNoteId) {
		return mapper.deleteSchoolNote(userId, schoolNoteId);
	}

	public String getSchoolNote(Integer userId, Integer unitId) {
		return mapper.getSchoolNote(userId, unitId);
	}

}
