package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

public interface FavoriteOccupationMapper {

	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);

	int insertFavoriteOccupation(@Param("favoriteObject") FavoriteOccupation favoriteObject);

	int deleteFavoriteOccupation(@Param("id") Integer id);
	
	ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(@Param("userId") Integer userId);

	int insertFavoriteCareerCluster(@Param("favoriteObject") FavoriteCareerCluster favoriteObject);

	int deleteFavoriteCareerCluster(@Param("id") Integer id);
	
	ArrayList<FavoriteSchool> getFavoriteSchool(@Param("userId") Integer userId);

	int insertFavoriteSchool(@Param("favoriteObject") FavoriteSchool favoriteObject);

	int deleteFavoriteSchool(@Param("id") Integer id);

	int deleteFavoriteCareerClusterByUserId(Integer userId);

	int deleteFavoriteOccupationByUserId(Integer userId);

	int deleteFavoriteSchoolByUserId(Integer userId);
	
	ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(@Param("userId") Integer userId);
	
	int insertFavoriteCitmOccupation(@Param("favoriteObject") FavoriteCitmOccupation favoriteObject);

	int deleteFavoriteCitmOccupationByUserId(@Param("userId") Integer userId);

	int insertFavoriteService(@Param("favoriteObject") FavoriteMilitaryService favoriteObject);
	
	int deleteFavoriteServiceByUserId(@Param("userId") Integer userId);

	ArrayList<FavoriteMilitaryService> getFavoriteService(@Param("userId") Integer userId);

}
