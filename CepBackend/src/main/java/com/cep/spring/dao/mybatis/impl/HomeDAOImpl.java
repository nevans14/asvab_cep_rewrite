package com.cep.spring.dao.mybatis.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.HomeDAO;
import com.cep.spring.dao.mybatis.service.HomeService;
import com.cep.spring.model.HomepageImage;

@Primary
@Repository
public class HomeDAOImpl implements HomeDAO {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	HomeService service;
	
	public HomepageImage getCurrentHomepageImg() {
		logger.info("In DAO: fetching current homepage image.");
		return service.getCurrentHomepageImg();
	}
}