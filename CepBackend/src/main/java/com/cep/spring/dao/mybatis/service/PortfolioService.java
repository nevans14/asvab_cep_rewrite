package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.dao.mybatis.mapper.PortfolioMapper;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Service
public class PortfolioService {

	@Autowired
	public PortfolioMapper mapper;

	public List<String> isPortfolioStarted(Integer userId) {
		return mapper.isPortfolioStarted(userId);
	}

	public ArrayList<WorkExperience> getWorkExperience(Integer userId) {
		return mapper.getWorkExperience(userId);
	}

	@Transactional
	public int insertWorkExperience(WorkExperience workExperienceObject) {
		return mapper.insertWorkExperience(workExperienceObject);
	}

	@Transactional
	public int deleteWorkExperience(Integer userId, Integer id) {
		return mapper.deleteWorkExperience(userId, id);
	}

	@Transactional
	public int updateWorkExperience(WorkExperience workExperienceObject) {
		return mapper.updateWorkExperience(workExperienceObject);
	}

	public ArrayList<Education> getEducation(Integer userId) {
		return mapper.getEducation(userId);
	}

	@Transactional
	public int insertEducation(Education educationObject) {
		return mapper.insertEducation(educationObject);
	}

	@Transactional
	public int deleteEducation(Integer userId, Integer id) {
		return mapper.deleteEducation(userId, id);
	}

	@Transactional
	public int updateEducation(Education educationObject) {
		return mapper.updateEducation(educationObject);
	}

	public ArrayList<Achievement> getAchievements(Integer userId) {
		return mapper.getAchievements(userId);
	}

	@Transactional
	public int insertAchievement(Achievement achievementObject) {
		return mapper.insertAchievement(achievementObject);
	}

	@Transactional
	public int deleteAchievement(Integer userId, Integer id) {
		return mapper.deleteAchievement(userId, id);
	}

	@Transactional
	public int updateAchievement(Achievement achievementObject) {
		return mapper.updateAchievement(achievementObject);
	}

	public ArrayList<Interest> getInterests(Integer userId) {
		return mapper.getInterests(userId);
	}

	@Transactional
	public int insertInterest(Interest interestObject) {
		return mapper.insertInterest(interestObject);
	}

	@Transactional
	public int deleteInterest(Integer userId, Integer id) {
		return mapper.deleteInterest(userId, id);
	}

	@Transactional
	public int updateInterest(Interest interestObject) {
		return mapper.updateInterest(interestObject);
	}

	public ArrayList<Skill> getSkills(Integer userId) {
		return mapper.getSkills(userId);
	}

	@Transactional
	public int insertSkill(Skill skillObject) {
		return mapper.insertSkill(skillObject);
	}

	@Transactional
	public int deleteSkill(Integer userId, Integer id) {
		return mapper.deleteSkill(userId, id);
	}

	@Transactional
	public int updateSkill(Skill skillObject) {
		return mapper.updateSkill(skillObject);
	}

	public ArrayList<WorkValue> getWorkValues(Integer userId) {
		return mapper.getWorkValues(userId);
	}

	@Transactional
	public int insertWorkValue(WorkValue workValueObject) {
		return mapper.insertWorkValue(workValueObject);
	}

	@Transactional
	public int deleteWorkValue(Integer userId, Integer id) {
		return mapper.deleteWorkValue(userId, id);
	}

	@Transactional
	public int updateWorkValue(WorkValue workValueObject) {
		return mapper.updateWorkValue(workValueObject);
	}

	public ArrayList<Volunteer> getVolunteers(Integer userId) {
		return mapper.getVolunteers(userId);
	}

	@Transactional
	public int insertVolunteer(Volunteer volunteerObject) {
		return mapper.insertVolunteer(volunteerObject);
	}

	@Transactional
	public int deleteVolunteer(Integer userId, Integer id) {
		return mapper.deleteVolunteer(userId, id);
	}

	@Transactional
	public int updateVolunteer(Volunteer volunteerObject) {
		return mapper.updateVolunteer(volunteerObject);
	}

	public ArrayList<AsvabScore> getAsvabScore(Integer userId) {
		return mapper.getAsvabScore(userId);
	}

	@Transactional
	public int insertAsvabScore(AsvabScore asvabObject) {
		return mapper.insertAsvabScore(asvabObject);
	}

	@Transactional
	public int deleteAsvabScore(Integer userId, Integer id) {
		return mapper.deleteAsvabScore(userId, id);
	}

	@Transactional
	public int updateAsvabScore(AsvabScore asvabObject) {
		return mapper.updateAsvabScore(asvabObject);
	}

	public ArrayList<ActScore> getActScore(Integer userId) {
		return mapper.getActScore(userId);
	}

	@Transactional
	public int insertActScore(ActScore actObject) {
		return mapper.insertActScore(actObject);
	}

	@Transactional
	public int deleteActScore(Integer userId, Integer id) {
		return mapper.deleteActScore(userId, id);
	}

	@Transactional
	public int updateActScore(ActScore actObject) {
		return mapper.updateActScore(actObject);
	}

	public ArrayList<SatScore> getSatScore(Integer userId) {
		return mapper.getSatScore(userId);
	}

	@Transactional
	public int insertSatScore(SatScore satObject) {
		return mapper.insertSatScore(satObject);
	}

	@Transactional
	public int deleteSatScore(Integer userId, Integer id) {
		return mapper.deleteSatScore(userId, id);
	}

	@Transactional
	public int updateSatScore(SatScore satObject) {
		return mapper.updateSatScore(satObject);
	}

	public ArrayList<FyiScore> getFyiScore(Integer userId) {
		return mapper.getFyiScore(userId);
	}

	@Transactional
	public int insertFyiScore(FyiScore fyiObject) {
		return mapper.insertFyiScore(fyiObject);
	}

	@Transactional
	public int deleteFyiScore(Integer userId, Integer id) {
		return mapper.deleteFyiScore(userId, id);
	}

	@Transactional
	public int updateFyiScore(FyiScore fyiObject) {
		return mapper.updateFyiScore(fyiObject);
	}

	public ArrayList<OtherScore> getOtherScore(Integer userId) {
		return mapper.getOtherScore(userId);
	}

	@Transactional
	public int insertOtherScore(OtherScore otherObject) {
		return mapper.insertOtherScore(otherObject);
	}

	@Transactional
	public int deleteOtherScore(Integer userId, Integer id) {
		return mapper.deleteOtherScore(userId, id);
	}

	@Transactional
	public int updateOtherScore(OtherScore otherObject) {
		return mapper.updateOtherScore(otherObject);
	}

	public ArrayList<Plan> getPlan(Integer userId) {
		return mapper.getPlan(userId);
	}

	@Transactional
	public int insertPlan(Plan planObject) {
		return mapper.insertPlan(planObject);
	}

	@Transactional
	public int deletePlan(Integer userId, Integer id) {
		return mapper.deletePlan(userId, id);
	}

	@Transactional
	public int updatePlan(Plan planObject) {
		return mapper.updatePlan(planObject);
	}

	public ArrayList<UserInfo> selectNameGrade(Integer userId) {
		return mapper.selectNameGrade(userId);
	}

	@Transactional
	public int updateInsertNameGrade(UserInfo userInfo) {
		return mapper.updateInsertNameGrade(userInfo);
	}
	
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return mapper.getFavoriteOccupation(userId);
	}
	
	public String getAccessCode(Integer userId) {
		return mapper.getAccessCode(userId);
	}
	
	@Transactional
	public int insertTeacherDemoTaken(Integer userId) {
		return mapper.insertTeacherDemoTaken(userId);
	}

	public int getTeacherDemoTaken(Integer userId) {
		return mapper.getTeacherDemoTaken(userId);
	}

}
