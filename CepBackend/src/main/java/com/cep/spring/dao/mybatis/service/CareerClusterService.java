package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.CareerClusterMapper;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Service
public class CareerClusterService {

	@Autowired
	private CareerClusterMapper careerClusterMapper;

	public ArrayList<CareerCluster> getCareerCluster() {
		return careerClusterMapper.getCareerCluster();
	}

	public ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId) {
		return careerClusterMapper.getCareerClusterOccupation(ccId);
	}

	public SkillImportance getSkillImportance(String onetSoc) {
		return careerClusterMapper.getSkillImportance(onetSoc);
	}

	public ArrayList<Task> getTasks(String onetSoc) {
		return careerClusterMapper.getTasks(onetSoc);
	}

	public OccupationInterestCode getOccupationInterestCodes(String onetSoc) {
		return careerClusterMapper.getOccupationInterestCodes(onetSoc);
	}

	public ArrayList<OccupationEducationLevel> getEducationLevel(String onetSoc) {
		return careerClusterMapper.getEducationLevel(onetSoc);
	}

	public ArrayList<RelatedOccupation> getRelatedOccupations(String onetSoc) {
		return careerClusterMapper.getRelatedOccupations(onetSoc);
	}

	public ArrayList<OccufindSearch> getCareerClusterOccupations(Integer ccId) {
		return careerClusterMapper.getCareerClusterOccupations(ccId);
	}

	public CareerCluster getCareerClusterById(Integer ccId) {
		return careerClusterMapper.getCareerClusterById(ccId);
	}
}
