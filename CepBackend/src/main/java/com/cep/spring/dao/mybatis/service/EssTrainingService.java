package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.model.EssTraining.*;
import com.cep.spring.dao.mybatis.mapper.EssTrainingMapper;

@Service
public class EssTrainingService {
	
	@Autowired
	EssTrainingMapper mapper;
	
	public EssTraining getEssTrainingById(int essTrainingId) {
		return mapper.getEssTrainingById(essTrainingId);
	}
	
	public EssTrainingEmail getEssTrainingEmailById(int essTrainingId) {
		return mapper.getEssTrainingEmailById(essTrainingId);
	}
	
	public int addEssTraining(EssTraining essTraining) {
		return mapper.addEssTraining(essTraining);
	}
	
	public int updateEssTraining(EssTraining essTraining) {
		return mapper.updateEssTraining(essTraining);
	}
	
	public int removeEssTraining(int essTrainingId) {
		return mapper.removeEssTraining(essTrainingId);
	}
	
	public int isUserRegistered(String emailAddress, int essTrainingId) {
		return mapper.isUserRegistered(emailAddress, essTrainingId);
	}
	
	public int addEssTrainingRegistration(EssTrainingRegistration essTrainingRegistration) {
		return mapper.addEssTrainingRegistration(essTrainingRegistration);
	}
	
}