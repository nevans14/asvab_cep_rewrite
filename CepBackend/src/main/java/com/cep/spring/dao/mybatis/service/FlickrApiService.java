package com.cep.spring.dao.mybatis.service;

import com.cep.spring.utils.HttpURLConnectionBuilder;
import com.cep.spring.utils.JsonApiHelper;
import com.google.gson.Gson;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.cep.spring.model.optionready.*;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

@Service
public class FlickrApiService {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	private final String KEY = "3355ada59e25646d6a1d6e78b45c43f1";
	private final String SECRET = "92a39ba6d9477f33";
	private final String USER_ID = "182826706@N05";
	private final String BASE_API_URL = "https://api.flickr.com/services/rest/";
	
	public ArrayList<FlickrPhotoInfo> getPhotoCollection(boolean isDebugging) {
		HashMap<String, String> params = null;
		ArrayList<FlickrPhotoInfo> photos = new ArrayList<FlickrPhotoInfo>();
		String jsonResults = null;
		FlickrPhotoCollectionWrapper wrapper = null;
		FlickrPhotoCollectionInfo collection = null;
		Gson gson = new Gson();
		
		try {
			// hash params
			params = new HashMap<String, String>();
			params.put("method", "flickr.people.getPublicPhotos");
			params.put("api_key", KEY);
			params.put("user_id", USER_ID);
			params.put("format", "json");	
			params.put("nojsoncallback", "1");
			// build request url
			String url = JsonApiHelper.addParameters(true, BASE_API_URL, params);
			// build connection
			URL requestUrl = new URL(url);
			HttpsURLConnection connection = (HttpsURLConnection)requestUrl.openConnection();
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null,null,null);
			// bypass cert
			connection.setSSLSocketFactory(sslContext.getSocketFactory());
			// send request and get results
			jsonResults = JsonApiHelper.sendRequest(connection);
			// print out results
			if (isDebugging) {
				System.out.println("flickr results: " + jsonResults);
			}
			// parse response
			wrapper = gson.fromJson(jsonResults, FlickrPhotoCollectionWrapper.class);
			// iterate over photos
			if (wrapper != null) {
				collection = wrapper.getPhotos();
				if (collection != null && collection.getPhoto().size() > 0) {
					for (int i = 0; i < collection.getPhoto().size(); i++) {
						FlickrPhotoCollectionPhoto photo = collection.getPhoto().get(i);
						photos.add(getPhotoInfo(photo.getId(), photo.getSecret(), isDebugging));
					}
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return photos;
	}
	
	public FlickrPhotoInfo getPhotoInfo(String photoId, String secret, boolean isDebugging) {
		HashMap<String, String> params = null;
		String jsonResults = null;
		FlickrPhotoInfoWrapper wrapper = null;
		Gson gson = new Gson();
		try {
			// hash params
			params = new HashMap<String, String>();
			params.put("method", "flickr.photos.getInfo");
			params.put("api_key", KEY);
			params.put("photo_id", photoId);
			params.put("secret", secret);
			params.put("format", "json");	
			params.put("nojsoncallback", "1");
			// build request url
			// build request url
			String url = JsonApiHelper.addParameters(true, BASE_API_URL, params);
			// build connection
			URL requestUrl = new URL(url);
			HttpsURLConnection connection = (HttpsURLConnection)requestUrl.openConnection();
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null,null,null);
			// bypass cert
			connection.setSSLSocketFactory(sslContext.getSocketFactory());
			// send request and get results
			jsonResults = JsonApiHelper.sendRequest(connection);
			// print results
			if (isDebugging) {
				System.out.println("flickr results: " + jsonResults);
			}
			// parse response
			wrapper = gson.fromJson(jsonResults, FlickrPhotoInfoWrapper.class);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return wrapper == null ? null : wrapper.getFlickrPhotoInfo();
	}
}