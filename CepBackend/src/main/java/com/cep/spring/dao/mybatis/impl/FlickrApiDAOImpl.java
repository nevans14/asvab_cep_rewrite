package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

import com.cep.spring.dao.mybatis.FlickrApiDAO;
import com.cep.spring.dao.mybatis.service.FlickrApiService;
import com.cep.spring.model.optionready.FlickrPhotoInfo;

@Repository
public class FlickrApiDAOImpl implements FlickrApiDAO {
	
	@Autowired
	private FlickrApiService service;
	
	public ArrayList<FlickrPhotoInfo> getPhotoCollection(boolean isDebugging) {
		return service.getPhotoCollection(isDebugging);
	}
	
}