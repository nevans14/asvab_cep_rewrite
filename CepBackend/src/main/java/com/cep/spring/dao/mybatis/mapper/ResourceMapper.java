package com.cep.spring.dao.mybatis.mapper;

import java.util.List;

import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

public interface ResourceMapper {
	
	List<ResourceTopic> getResources();
	
	List<ResourceQuickLinkTopic> getQuickLinks();
	
}