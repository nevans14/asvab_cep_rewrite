package com.cep.spring.dao.jdbctemplate;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
;

@Component
@Transactional
public class FlushAccessCodes {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private final static String DELETE_SQL_TEMPLATE = "DELETE FROM ### WHERE user_id = ?";
	private final static String SELECT_SQL_TEMPLATE = "SELECT * FROM ### WHERE user_id = ?";

	public void removeAccessCodes(String user_id){
		String sql = "";

		
		// Remove Save FYI first 
		  List<Map<String,String>> lhm = jdbcTemplate.query(
				"Select s.ID  " + 
					" FROM fyi_user_tests f " +
					" LEFT JOIN SaveFYI s ON f.test_id = s.ID " + 
					" WHERE f.user_id = ? ", 
					new GenericRowMapper(),  user_id);
			for(Map<String,String> row : lhm ) {
				sql = "DELETE FROM SaveFYI WHERE ID = ? ";
				jdbcTemplate.update(sql,  row.get("ID") );			   

			}
		  
		
		
		for (TableList tableName : TableList.values()) {
			  sql =  DELETE_SQL_TEMPLATE.replace("###", tableName.toString()) ;
			  System.out.println( sql );
			  jdbcTemplate.update(sql,  user_id);			   
		}
	}
	
	public void removeAccessCodes(List<String> userIdList ){	
		for(String userId: userIdList ) {
			removeAccessCodes(userId);
		}
	}

	public void removeAccessCodesBatch(List<String> userIdList){
		logger.info("removeAccessCodesBatch");


		// Remove Save FYI first 
        List<String> saveFYI_Id_list = new ArrayList();
        String saveFYIwhereClase = "";
		for ( String uId : userIdList) {
		    saveFYIwhereClase += uId + " , " ;
		}
		
		saveFYIwhereClase = saveFYIwhereClase.substring(0, saveFYIwhereClase.lastIndexOf(",") )  ; 
		System.out.println( saveFYIwhereClase  );
		
		
		  List<Map<String,String>> lhm = jdbcTemplate.query(
				"Select s.ID  " + 
					" FROM fyi_user_tests f " +
					" LEFT JOIN SaveFYI s ON f.test_id = s.ID " + 
					" WHERE f.user_id in ( " +  saveFYIwhereClase   + " )", 
					new GenericRowMapper());
			for(Map<String,String> row : lhm ) {
			    saveFYI_Id_list.add(row.get("ID") );
			}
			  
		
        deleteBatch(saveFYI_Id_list, "SaveFYI", "ID" );
		
		for (TableList tableName : TableList.values()) {
			deleteBatch(userIdList, tableName.toString(), "user_id" );
			System.out.print(".");
		}
	}
	
	public void selectAccessCodes( String uid ){
		String sql = "";
		 // "430847";
		
		for (TableList tableName : TableList.values()) {
			  sql =  SELECT_SQL_TEMPLATE.replace("###", tableName.toString()) ;
			   System.out.println( sql );
			  List<Map<String,String>> lhm = jdbcTemplate.query(sql, new GenericRowMapper(), uid);
			  System.out.println("Size:" + lhm.size() ) ; 
			   
		}
	}
	
	public void selectAccessCodes(List<String> userIdList ){	
		for(String userId: userIdList ) {
			selectAccessCodes(userId);
		}
	}
	
	
	
	// insert batch example
	public int[] deleteBatch(final List<String> userId, final String tableName, final String keyName) {
		String sql = "DELETE FROM " + tableName + "  WHERE " + keyName + " = ? ";

		int[] returnStatus = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setString(1, userId.get(i) );
			}

			@Override
			public int getBatchSize() {
				return userId.size();
			}
		});

		return returnStatus;
	}

	
	
	
	
	public enum TableList{
		asvab_log,
		DH_FavCareer,
		DH_FavCC,
		fyi_user_profile,
		fyi_user_tests,
		resume_achievement,
		resume_work_exp,
		users,
		access_codes
	}
	
}
