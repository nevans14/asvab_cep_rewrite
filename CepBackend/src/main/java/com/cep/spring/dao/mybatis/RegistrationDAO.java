package com.cep.spring.dao.mybatis;

import com.cep.spring.model.AccessCodes;
import com.cep.spring.model.AccessCodesMerge;
import com.cep.spring.model.Meps;
import com.cep.spring.model.Users;

public interface RegistrationDAO {

	public Users getUser(Long userId, String username);
	
	public Users getByUsername(String username);
	
	public int insertUser(Users model);
	
	public int updateUser(Users model);
	
	public int insertUserRole(String username, String role);
	
	public void mergeAccounts(AccessCodesMerge registrationObject);
	
	public AccessCodes getAccessCodes(String accessCode, Integer userId);
	
	public long insertAccessCode(AccessCodes newAccessCodeObj);
	
	public int updateAccessCode(AccessCodes model);
	
	public Users getUserId(String emailAddress);
	
	public void disableAccessCode(String accessCode);
	
	public void changeAccessCodePointer(Integer userId, String emailAddress);
	
	public Meps getUsersMeps(String accessCode);
	
	public int insertAsvabLog(Integer userId);
	
	public AccessCodes getLastPromoAccessCodeUsed(String accessCode);
	
	public int insertTeacherProfession(Integer userId, String subject);
	
	public int insertSubscription(String emailAddress);
}
