package com.cep.spring.dao.mybatis.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.dao.mybatis.service.RegistrationService;
import com.cep.spring.model.AccessCodes;
import com.cep.spring.model.AccessCodesMerge;
import com.cep.spring.model.Meps;
import com.cep.spring.model.Users;

@Primary
@Repository
public class RegistrationDAOImpl implements RegistrationDAO {

	private Logger logger = LogManager.getLogger(this.getClass().getName());
	@Autowired
	private RegistrationService service;
	
	public Users getUser(Long userId, String username) {
		Users results = null;
		try {
			results = service.getUser(userId, username);	
		} catch (Exception ex) {
			logger.error("ERROR: getUser (" + userId + "," + username + "); MESSAGE: " + ex.getMessage());
		}		
		return results;
	}
	
	public Users getByUsername(String username) {
		Users results = null;
		try {
			results = service.getByUsername(username); 
		} catch (Exception ex) {
			logger.error("ERROR: getByUsername(" + username + "); MESSAGE: " + ex.getMessage());
		}		
		return results;
	}
	
	public int insertUser(Users model) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.insertUser(model);
		} catch (Exception ex) {
			logger.error("ERROR: insertUser(" + model.toString() + "); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;		
	}
	
	public int updateUser(Users model) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.updateUser(model); 
		} catch (Exception ex) {
			logger.error("ERROR: updateUser(" + model.toString() + "); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;
	}
	
	public int insertUserRole(String username, String role) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.insertUserRole(username, role); 
		} catch (Exception ex) {
			logger.error("ERROR: insertUserRole(" + username + "," + role + "); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected; 
	}
	
	public long insertAccessCode(AccessCodes newAccessCodeObj) {
		long rowsAffected = 0;
		try {
			rowsAffected = service.insertAccessCode(newAccessCodeObj); 
		} catch (Exception ex) {
			logger.error("ERROR: insertAccessCode(" + newAccessCodeObj.toString() + "); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;
	}
	
	public int updateAccessCode(AccessCodes model) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.updateAccessCode(model); 
		} catch (Exception ex) {
			logger.error("ERROR: updateAccessCode(" + model.toString() + "; MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;
	}
	
	@Override
	public void mergeAccounts(AccessCodesMerge registrationObject) {
		try {
			service.MergeAccounts(registrationObject);	
		} catch (Exception ex) {
			logger.error("ERROR: mergeAccounts(); MESSAGE: " + ex.getMessage());
		}		
	}

	@Override
	public AccessCodes getAccessCodes(String accessCode, Integer userId) {
		AccessCodes results = null;
		try {
			results = service.getAccessCodes(accessCode, userId); 
		} catch (Exception ex) {
			logger.error("ERROR: getAccessCodes(" + accessCode + "," + userId + "); MESSAGE: " + ex.getMessage());
		}
		return results;
	}

	@Override
	public Users getUserId(String emailAddress) {
		Users results = null;
		try {
			results = service.getUserId(emailAddress); 
		} catch (Exception ex) {
			logger.error("ERROR: getUserId(" + emailAddress +"); MESSAGE: " + ex.getMessage());
		}
		return 	results;
	}

	@Override
	public void disableAccessCode(String accessCode) {
		service.disableAccessCode(accessCode);
	}

	@Override
	public void changeAccessCodePointer(Integer userId, String emailAddress) {
		service.changeAccessCodePointer(userId, emailAddress);
	}

	@Override
	public Meps getUsersMeps(String accessCode) {
		return service.getUsersMeps(accessCode);
	}
	
	public int insertAsvabLog(Integer userId) {
		return service.insertAsvabLog(userId);
	}

	public AccessCodes getLastPromoAccessCodeUsed(String accessCode) {
		AccessCodes results = null;
		try {
			results = service.getLastPromoAccessCodeUsed(accessCode);
		} catch (Exception ex) {
			logger.error("ERROR: getLastPromoAccessCodeUsed(" + accessCode +"); MESSAGE: " + ex.getMessage());
		}
		return results;
	}
	
	public int insertTeacherProfession(Integer userId, String subject) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.insertTeacherProfession(userId, subject);
		} catch (Exception ex) {
			logger.error("ERROR: insertTeacherProfession(" + userId + "," + subject +"); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;
	}
	
	public int insertSubscription(String emailAddress) {
		int rowsAffected = 0;
		try {
			rowsAffected = service.insertSubscription(emailAddress);
		} catch (Exception ex) {
			logger.error("ERROR: insertSubscription(" + emailAddress + "); MESSAGE: " + ex.getMessage());
		}
		return rowsAffected;
	}
}
