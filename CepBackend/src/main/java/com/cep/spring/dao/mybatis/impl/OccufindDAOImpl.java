package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.OccufindDAO;
import com.cep.spring.dao.mybatis.service.OccufindService;
import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;
import com.cep.spring.model.occufind.SchoolMajors;

@Repository
public class OccufindDAOImpl implements OccufindDAO {
	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	OccufindService service;

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccufindSearch(com.cep.spring.model.occufind.OccufindSearchArguments)
	 */
	@Override
	public ArrayList<OccufindSearch> getOccufindSearch(OccufindSearchArguments userSearchArguments) {
		ArrayList<OccufindSearch> occupationList = new ArrayList<OccufindSearch>();

		try {
			occupationList = service.getOccufindSearch(userSearchArguments);
		} catch (Exception e) {
			System.out.println(e);
			logger.error("Error", e);
		}
		return occupationList;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getCertificateExplanation(java.lang.String)
	 */
	@Override
	public CertificateExplanation getCertificateExplanation(String onetSoc) {
		CertificateExplanation results = null;

		try {
			results = service.getCertificateExplanation(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getAlternateTitles(java.lang.String)
	 */
	@Override
	public List<AlternateTitles> getAlternateTitles(String onetSoc) {
		List<AlternateTitles> results = null;

		try {
			results = service.getAlternateTitles(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationTitleDescription(java.lang.String)
	 */
	@Override
	public TitleDescription getOccupationTitleDescription(String onetSoc) {
		TitleDescription results = null;

		try {
			results = service.getOccupationTitleDescription(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationASK(java.lang.String)
	 */
	@Override
	public ArrayList<Occupation> getOccupationASK(String onetSoc) {
		ArrayList<Occupation> occupationList = new ArrayList<Occupation>();

		try {
			occupationList = service.getOccupationASK(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return occupationList;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryCareerResources(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryCareerResource> getMilitaryCareerResources(String onetSoc) {
		ArrayList<MilitaryCareerResource> occupationList = new ArrayList<MilitaryCareerResource>();

		try {
			occupationList = service.getMilitaryCareerResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return occupationList;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOOHResources(java.lang.String)
	 */
	@Override
	public ArrayList<OOHResource> getOOHResources(String onetSoc) {
		ArrayList<OOHResource> occupationList = new ArrayList<OOHResource>();

		try {
			occupationList = service.getOOHResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return occupationList;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getAvgSalaryByState(java.lang.String)
	 */
	@Override
	public ArrayList<StateSalary> getAvgSalaryByState(String onetSocTrimmed) {
		ArrayList<StateSalary> stateSalaryList = new ArrayList<StateSalary>();

		try {
			stateSalaryList = service.getAvgSalaryByState(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return stateSalaryList;
	}
	
	/**
	 * Return a list of entry level salary for the occupation by states 
	 */
	@Override
	public ArrayList<StateSalaryEntry> getEntrySalaryByState(String onetSocTrimmed) {
		ArrayList<StateSalaryEntry> stateSalaryEntryList = new ArrayList<StateSalaryEntry>();
		try {
			stateSalaryEntryList = service.getEntrySalaryByState(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return stateSalaryEntryList;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getNationalSalary(java.lang.String)
	 */
	@Override
	public Integer getNationalSalary(String onetSocTrimmed) {
		Integer nationalSalary = null;

		try {
			nationalSalary = service.getNationalSalary(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return nationalSalary;
	}
	
	/**
	 * Returns the national entry level salary for an occupation 
	 */
	@Override
	public Integer getNationalEntrySalary(String onetSocTrimmed) {
		Integer nationalSalary = null;

		try {
			nationalSalary = service.getNationalEntrySalary(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return nationalSalary;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getBLSTitle(java.lang.String)
	 */
	@Override
	public String getBLSTitle(String onetSocTrimmed) throws Exception {
		return service.getBLSTitle(onetSocTrimmed);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationInterestCodes(java.lang.String)
	 */
	@Override
	public ArrayList<OccupationInterestCode> getOccupationInterestCodes(String onetSoc) {
		ArrayList<OccupationInterestCode> interestCds = new ArrayList<OccupationInterestCode>();

		try {
			interestCds = service.getOccupationInterestCodes(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return interestCds;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getHotGreenStemFlags(java.lang.String)
	 */
	@Override
	public HotGreenStem getHotGreenStemFlags(String onetSoc) {
		HotGreenStem flags = new HotGreenStem();

		try {
			flags = service.getHotGreenStemFlags(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return flags;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getServiceOfferingCareer(java.lang.String)
	 */
	@Override
	public ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(String onetSoc) {
		ArrayList<ServiceOfferingCareer> serviceCodes = new ArrayList<ServiceOfferingCareer>();

		try {
			serviceCodes = service.getServiceOfferingCareer(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return serviceCodes;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getRelatedCareerCluster(java.lang.String)
	 */
	@Override
	public ArrayList<RelatedCareerCluster> getRelatedCareerCluster(String onetSoc) {
		ArrayList<RelatedCareerCluster> careerClusterName = new ArrayList<RelatedCareerCluster>();

		try {
			careerClusterName = service.getRelatedCareerCluster(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return careerClusterName;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getSchoolMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<SchoolMoreDetails> getSchoolMoreDetails(String onetSoc) {
		ArrayList<SchoolMoreDetails> schoolMoreDetails = new ArrayList<SchoolMoreDetails>();

		try {
			schoolMoreDetails = service.getSchoolMoreDetails(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return schoolMoreDetails;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getStateSalaryYear()
	 */
	@Override
	public Integer getStateSalaryYear() throws Exception {
		Integer year = null;

		year = service.getStateSalaryYear();

		return year;

	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getEmploymentMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(String trimmedOnetSoc) {
		ArrayList<EmploymentMoreDetails> employmentMoreDetails = new ArrayList<EmploymentMoreDetails>();

		try {
			employmentMoreDetails = service.getEmploymentMoreDetails(trimmedOnetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return employmentMoreDetails;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(String onetSoc) {
		ArrayList<MilitaryMoreDetails> militaryMoreDetails = new ArrayList<MilitaryMoreDetails>();

		try {
			militaryMoreDetails = service.getMilitaryMoreDetails(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return militaryMoreDetails;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryHotJobs(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryHotJobs> getMilitaryHotJobs(String onetSoc) {
		ArrayList<MilitaryHotJobs> results = new ArrayList<MilitaryHotJobs>();

		try {
			results = service.getMilitaryHotJobs(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccufindMoreResources(java.lang.String)
	 */
	@Override
	public ArrayList<OccufindMoreResources> getOccufindMoreResources(String onetSoc) throws Exception {
		return service.getOccufindMoreResources(onetSoc);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardBrightOutlook(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardBrightOutlook(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception {
		return service.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardStemCareers(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardStemCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception {
		return service.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardGreenCareers(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardGreenCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception {
		return service.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewAllOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewAllOccupations() throws Exception {
		return service.viewAllOccupations();
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewMilitaryOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewMilitaryOccupations() throws Exception {
		return service.viewMilitaryOccupations();
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewHotOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewHotOccupations() throws Exception {
		return service.viewHotOccupations();
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#onetHideDetails(java.lang.String)
	 */
	@Override
	public ArrayList<OccufindAlternative> onetHideDetails(String onetSoc) throws Exception {
		return service.onetHideDetails(onetSoc);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getImageAltText(java.lang.String)
	 */
	@Override
	public String getImageAltText(String onetSoc) throws Exception {
		return service.getImageAltText(onetSoc);
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getJobZone(java.lang.String)
	 */
	@Override
	public JobZone getJobZone(String onetSoc) throws Exception {
		return service.getJobZone(onetSoc);
	}

	@Override
	public SchoolProfile getSchoolProfile(int unitId) {
		return service.getSchoolProfile(unitId);
	}

	@Override
	public List<String> getCareerMajors(String onetCode) {
		return service.getCareerMajors(onetCode);
	}
}
