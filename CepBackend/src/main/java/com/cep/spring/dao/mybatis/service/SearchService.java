package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.SearchMapper;
import com.cep.spring.model.search.Search;

@Service
public class SearchService {

	@Autowired
	SearchMapper mapper;

	public ArrayList<Search> getFileName(String searchString) {
		return mapper.getFileName(searchString);
	}
}
