package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.dao.mybatis.mapper.RegistrationMapper;
import com.cep.spring.model.AccessCodes;
import com.cep.spring.model.AccessCodesMerge;
import com.cep.spring.model.Meps;
import com.cep.spring.model.Users;

@Service
public class RegistrationService {

	@Autowired
	public RegistrationMapper mapper;
	
	@Transactional
	public int MergeAccounts(AccessCodesMerge registrationObject) {
		return mapper.mergeAccounts(registrationObject);
	}
	
	public Users getUser(Long userId, String username) {
		return mapper.getUser(userId, username);
	}
	
	public Users getByUsername(String username) {
		return mapper.getByUsername(username);
	}
	
	public int insertUser(Users model) {
		return mapper.insertUser(model);
	}
	
	public int updateUser(Users model) {
		return mapper.updateUser(model);
	}
	
	public int insertUserRole(String username, String role) {
		return mapper.insertUserRole(username, role);
	}

	public AccessCodes getAccessCodes(String accessCode, Integer userId) {
		return mapper.getAccessCodes(accessCode, userId);		
	}
	
	public long insertAccessCode(AccessCodes newAccessCodeObj) {
		return mapper.insertAccessCode(newAccessCodeObj);
	}
	
	public int updateAccessCode(AccessCodes model) {
		return mapper.updateAccessCode(model);
	}

	public Users getUserId(String emailAddress) {
		return mapper.getUserId(emailAddress);
	}

	public void changeAccessCodePointer(Integer userId, String emailAddress) {
		mapper.changeAccessCodePointer(userId, emailAddress);
	}

	public void disableAccessCode(String accessCode) {
		mapper.disableAccessCode(accessCode);
	}

	public Meps getUsersMeps(String accessCode) {
		return mapper.getUsersMeps(accessCode);
	}
	
	public int insertAsvabLog(Integer userId) {
		return mapper.insertAsvabLog(userId);
	}
	
	public AccessCodes getLastPromoAccessCodeUsed(String accessCode) {
		return mapper.getLastPromoAccessCodeUsed(accessCode);
	}
	
	public int insertTeacherProfession(Integer userId, String subject) {
		return mapper.insertTeacherProfession(userId, subject);
	}
	
	public int insertSubscription(String emailAddress) {
		return mapper.insertSubscription(emailAddress);
	}
}
