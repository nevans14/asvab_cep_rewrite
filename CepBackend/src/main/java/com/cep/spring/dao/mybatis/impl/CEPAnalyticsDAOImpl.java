package com.cep.spring.dao.mybatis.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.CEPAnalyticsService;

@Repository
public class CEPAnalyticsDAOImpl {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	private CEPAnalyticsService service;

	/**
	 * Tracks usage of bring ASVAB CEP to your school sharing feature.
	 * 
	 * @param email
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public int insertCEPShare(String email, String category) throws Exception {
		return service.insertCEPShare(email, category);
	}
}
