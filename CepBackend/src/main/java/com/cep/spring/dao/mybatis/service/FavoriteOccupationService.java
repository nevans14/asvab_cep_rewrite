package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.FavoriteOccupationMapper;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Service
public class FavoriteOccupationService {

	@Autowired
	FavoriteOccupationMapper mapper;

	public ArrayList<FavoriteOccupation> getFavoriteOccupation(int userId) {
		return mapper.getFavoriteOccupation(userId);
	}

	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject) {
		return mapper.insertFavoriteOccupation(favoriteObject);
	}

	public int deleteFavoriteOccupation(int id) {
		return mapper.deleteFavoriteOccupation(id);
	}

	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(int userId) {
		return mapper.getFavoriteCareerCluster(userId);
	}

	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject) {
		return mapper.insertFavoriteCareerCluster(favoriteObject);
	}

	public int deleteFavoriteCareerCluster(int id) {
		return mapper.deleteFavoriteCareerCluster(id);
	}
	
	public ArrayList<FavoriteSchool> getFavoriteSchool(int userId) {
		return mapper.getFavoriteSchool(userId);
	}

	public int insertFavoriteSchool(FavoriteSchool favoriteObject) {
		return mapper.insertFavoriteSchool(favoriteObject);
	}

	public int deleteFavoriteSchool(int id) {
		return mapper.deleteFavoriteSchool(id);
	}

	public int deleteFavoriteCareerClusterByUserId(int userId) {
		return mapper.deleteFavoriteCareerClusterByUserId(userId);
	}

	public int deleteFavoriteOccupationByUserId(int userId) {
		return  mapper.deleteFavoriteOccupationByUserId(userId);
	}

	public int deleteFavoriteSchoolByUserId(int userId) {
		return mapper.deleteFavoriteSchoolByUserId(userId);
	}

	public int deleteFavoriteCitmOccupationByUserId(int userId) {
		return mapper.deleteFavoriteCitmOccupationByUserId(userId);
	}

	public int insertFavoriteCitmOccupation(FavoriteCitmOccupation favoriteObject) {
		return mapper.insertFavoriteCitmOccupation(favoriteObject);
	}

	public ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(int userId) {
		return mapper.getFavoriteCitmOccupation(userId);
	}

	public int insertFavoriteService(FavoriteMilitaryService favoriteObject) {
		return mapper.insertFavoriteService(favoriteObject);
	}

	public int deleteFavoriteServiceByUserId(Integer userId) {
		return mapper.deleteFavoriteServiceByUserId(userId);
	}

	public ArrayList<FavoriteMilitaryService> getFavoriteService(Integer userId) {
		return mapper.getFavoriteService(userId);
	}
}
