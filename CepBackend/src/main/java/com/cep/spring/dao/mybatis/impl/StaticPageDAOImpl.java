package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.StaticPageDAO;
import com.cep.spring.dao.mybatis.service.StaticPageService;
import com.cep.spring.model.StaticPage;

@Repository
public class StaticPageDAOImpl implements StaticPageDAO {
	@Autowired
	StaticPageService service;
	
	public StaticPage getPageById(int pageId) {
		return service.getPageById(pageId);
	}
	
	public StaticPage getPageByPageNameOld(String pageName) {
		return service.getPageByPageNameOld(pageName);
	}
	
    public StaticPage getPageByPageName(String pageName, String website) {
        return service.getPageByPageName(pageName, website);
    }
    
}