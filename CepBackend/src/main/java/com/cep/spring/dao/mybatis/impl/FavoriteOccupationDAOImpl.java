package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.FavorateOccupationDAO;
import com.cep.spring.dao.mybatis.service.FavoriteOccupationService;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Primary
@Repository
public class FavoriteOccupationDAOImpl implements FavorateOccupationDAO {
    private Logger logger = LogManager.getLogger(this.getClass().getName());


	@Autowired
	FavoriteOccupationService service;

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#getFavoriteOccupation(java.lang.Integer)
	 */
	@Override
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception {
		ArrayList<FavoriteOccupation> results = new ArrayList<FavoriteOccupation>();

		results = service.getFavoriteOccupation(userId);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#insertFavoriteOccupation(com.cep.spring.model.favorites.FavoriteOccupation)
	 */
	@Override
	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteOccupation(favoriteObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#deleteFavoriteOccupation(java.lang.Integer)
	 */
	@Override
	public int deleteFavoriteOccupation(Integer id) throws Exception {
		int results = 0;

		results = service.deleteFavoriteOccupation(id);

		return results;
	}
	
	@Override
	public int deleteFavoriteOccupationByUserId(Integer UserId) throws Exception {
		int results = 0;

		results = service.deleteFavoriteOccupationByUserId(UserId);

		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#getFavoriteCareerCluster(java.lang.Integer)
	 */
	@Override
	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(Integer userId) throws Exception {
		ArrayList<FavoriteCareerCluster> results = new ArrayList<FavoriteCareerCluster>();

		results = service.getFavoriteCareerCluster(userId);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#insertFavoriteCareerCluster(com.cep.spring.model.favorites.FavoriteCareerCluster)
	 */
	@Override
	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteCareerCluster(favoriteObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.FavorateOccupationDAO#deleteFavoriteCareerCluster(java.lang.Integer)
	 */
	@Override
	public int deleteFavoriteCareerCluster(Integer id) throws Exception {
		int results = 0;

		results = service.deleteFavoriteCareerCluster(id);

		return results;
	}
	@Override
	public int deleteFavoriteCareerClusterByUserId(Integer userId) throws Exception {
		int results = 0;

		results = service.deleteFavoriteCareerClusterByUserId(userId);

		return results;
	}
	
	public ArrayList<FavoriteSchool> getFavoriteSchool(Integer userId) throws Exception {
		ArrayList<FavoriteSchool> results = new ArrayList<FavoriteSchool>();

		results = service.getFavoriteSchool(userId);

		return results;
	}


	public int insertFavoriteSchool(FavoriteSchool favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteSchool(favoriteObject);

		return results;
	}


	public int deleteFavoriteSchool(Integer id) throws Exception {
		int results = 0;

		results = service.deleteFavoriteSchool(id);

		return results;
	}
	
	public int deleteFavoriteSchoolByUserId(Integer userId) throws Exception {
		int results = 0;

		results = service.deleteFavoriteSchoolByUserId(userId);

		return results;
	}

	@Override
	public int insertFavoriteCitmOccupation(FavoriteCitmOccupation favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteCitmOccupation(favoriteObject);

		return results;
	}

	@Override
	public int deleteFavoriteCitmOccupationByUserId(Integer userId) throws Exception {
		int results = 0;

		results = service.deleteFavoriteCitmOccupationByUserId(userId);
		
		return results;
	}

	@Override
	public ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(Integer userId) throws Exception {
		return service.getFavoriteCitmOccupation(userId);
	}

	@Override
	public int insertFavoriteService(FavoriteMilitaryService favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteService(favoriteObject);

		return results;
	}

	@Override
	public int deleteFavoriteServiceByUserId(Integer userId) throws Exception {
		int results = 0;

		results = service.deleteFavoriteServiceByUserId(userId);
		
		return results;
	}

	@Override
	public ArrayList<FavoriteMilitaryService> getFavoriteService(Integer userId) throws Exception {
		ArrayList<FavoriteMilitaryService> results = new ArrayList<FavoriteMilitaryService>();

		results = service.getFavoriteService(userId);

		return results;
	}

}
