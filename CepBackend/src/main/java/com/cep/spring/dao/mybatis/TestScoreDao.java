package com.cep.spring.dao.mybatis;

import java.util.List;

import com.cep.spring.model.testscore.FilterScoreObj;
import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;

public interface TestScoreDao {

	public int insertTestScoreimpl(List<StudentTestingProgam> testScore);
	
	public StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode);
	
	public StudentTestingProgamSelects getTestScoreWithUserId(Integer userId);
	
	public int insertManualTestScore(ManualTestScore manualTestScoreObj);
	
	public ManualTestScore getManualTestScore(int userId);
	
	public int updateManualTestScore(ManualTestScore manualTestScoreObj);
	
	public int deleteManualTestScore(int userId);
	
	public List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj);
	
	public int deleteIcatDuplicates(String sourceFileName, String type);
	
	public int getRecordCount(String sourceFileName, String platform);
}
