package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

public interface FindYourInterestDAO {

	/**
	 * Returns boolean value depending if all records are inserted or not.
	 * 
	 * @param fyiObject
	 * @return
	 */
	Boolean insertProfileAndScores(FindYourInterestProfile fyiObject);

	/**
	 * Returns calculated combined and gender scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Scores> getCombinedAndGenderScore(Integer userId);

	String getTestId();

	/**
	 * Returns test questions.
	 * 
	 * @return
	 */
	ArrayList<FYIQuestion> getFYIQuestions();

	/**
	 * Returns gender score for each interest code using raw score test results.
	 * 
	 * @param rawScores
	 * @return
	 */
	ArrayList<GenderScore> getGenderScore(FYIRawScore rawScores, char gender);

	/**
	 * Returns combined score for each interest code using raw score test
	 * results.
	 * 
	 * @param rawScores
	 * @return
	 */
	ArrayList<CombinedScore> getCombinedScore(FYIRawScore rawScores);

	/**
	 * Returns number of test taken depending on user's identification number.
	 * 
	 * @param userId
	 * @return
	 */
	Integer getNumberTestTaken(Integer oldUserId, Integer newUserId);
	
	Integer getOldUserId(Integer newUserId);

	/**
	 * Update user selected score choice.
	 * 
	 * @param scoreChoice
	 * @return
	 */
	int updateProfile(ScoreChoice scoreChoice);

	/**
	 * Returns user's interest codes.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<ScoreChoice> getProfile(Integer userId);

	/**
	 * Returns the number of rows inserted for Find-Your-Interest profile.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	Integer insertProfile(FindYourInterestProfile fyiObject) throws Exception;

	/**
	 * Returns number of rows in FYI profile.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	Integer getProfileExist(Integer userId) throws Exception;

	/**
	 * Returns user's score choice.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	String getScoreChoice(Integer userId) throws Exception;

	TiedSelection getTiedSelection(Integer userId) throws Exception;

	int insertTiedSelection(TiedSelection tiedObject);

	int mergeProfile(int oldUserId, int newUserId);

	int mergeTests(FYIMerge fyiObject);

	int mergeResults(int oldUserId, int newUserId);

	int mergeAnswers(int oldUserId, int newUserId);

	FYIMerge getLastTestObject(int oldUserId, int newUserId);

	int deleteProfile(int userId);

}