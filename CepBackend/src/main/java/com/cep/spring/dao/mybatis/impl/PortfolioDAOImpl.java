package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.PortfolioDAO;
import com.cep.spring.dao.mybatis.service.PortfolioService;
import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Primary
@Repository
public class PortfolioDAOImpl implements PortfolioDAO {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	private PortfolioService service;

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#isPortfolioStarted(java.lang.Integer)
	 */
	@Override
	public List<String> isPortfolioStarted(Integer userId) throws Exception {
		return service.isPortfolioStarted(userId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getWorkExperience(java.lang.Integer)
	 */
	@Override
	public ArrayList<WorkExperience> getWorkExperience(Integer userId) {
		ArrayList<WorkExperience> results = new ArrayList<WorkExperience>();

		try {
			results = service.getWorkExperience(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertWorkExperience(com.cep.spring.model.portfolio.WorkExperience)
	 */
	@Override
	public int insertWorkExperience(WorkExperience workExperienceObject) throws Exception {
		int results = 0;

		results = service.insertWorkExperience(workExperienceObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteWorkExperience(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteWorkExperience(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteWorkExperience(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateWorkExperience(com.cep.spring.model.portfolio.WorkExperience)
	 */
	@Override
	public int updateWorkExperience(WorkExperience workExperienceObject) throws Exception {
		int results = 0;

		results = service.updateWorkExperience(workExperienceObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getEducation(java.lang.Integer)
	 */
	@Override
	public ArrayList<Education> getEducation(Integer userId) {
		ArrayList<Education> results = new ArrayList<Education>();

		try {
			results = service.getEducation(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertEducation(com.cep.spring.model.portfolio.Education)
	 */
	@Override
	public int insertEducation(Education educationObject) throws Exception {
		int results = 0;

		results = service.insertEducation(educationObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteEducation(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteEducation(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteEducation(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateEducation(com.cep.spring.model.portfolio.Education)
	 */
	@Override
	public int updateEducation(Education educationObject) throws Exception {
		int results = 0;

		results = service.updateEducation(educationObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAchievements(java.lang.Integer)
	 */
	@Override
	public ArrayList<Achievement> getAchievements(Integer userId) {
		ArrayList<Achievement> results = new ArrayList<Achievement>();

		try {
			results = service.getAchievements(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertAchievement(com.cep.spring.model.portfolio.Achievement)
	 */
	@Override
	public int insertAchievement(Achievement achievementObject) throws Exception {
		int results = 0;

		results = service.insertAchievement(achievementObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteAchievement(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteAchievement(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteAchievement(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateAchievement(com.cep.spring.model.portfolio.Achievement)
	 */
	@Override
	public int updateAchievement(Achievement achievementObject) throws Exception {
		int results = 0;

		results = service.updateAchievement(achievementObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getInterests(java.lang.Integer)
	 */
	@Override
	public ArrayList<Interest> getInterests(Integer userId) {
		ArrayList<Interest> results = new ArrayList<Interest>();

		try {
			results = service.getInterests(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertInterest(com.cep.spring.model.portfolio.Interest)
	 */
	@Override
	public int insertInterest(Interest interestObject) throws Exception {
		int results = 0;

		results = service.insertInterest(interestObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteInterest(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteInterest(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteInterest(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateInterest(com.cep.spring.model.portfolio.Interest)
	 */
	@Override
	public int updateInterest(Interest interestObject) throws Exception {
		int results = 0;

		results = service.updateInterest(interestObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getSkills(java.lang.Integer)
	 */
	@Override
	public ArrayList<Skill> getSkills(Integer userId) {
		ArrayList<Skill> results = new ArrayList<Skill>();

		try {
			results = service.getSkills(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertSkill(com.cep.spring.model.portfolio.Skill)
	 */
	@Override
	public int insertSkill(Skill skillObject) throws Exception {
		int results = 0;

		results = service.insertSkill(skillObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteSkill(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteSkill(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteSkill(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateSkill(com.cep.spring.model.portfolio.Skill)
	 */
	@Override
	public int updateSkill(Skill skillObject) throws Exception {
		int results = 0;

		results = service.updateSkill(skillObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getWorkValues(java.lang.Integer)
	 */
	@Override
	public ArrayList<WorkValue> getWorkValues(Integer userId) {
		ArrayList<WorkValue> results = new ArrayList<WorkValue>();

		try {
			results = service.getWorkValues(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertWorkValue(com.cep.spring.model.portfolio.WorkValue)
	 */
	@Override
	public int insertWorkValue(WorkValue workValueObject) throws Exception {
		int results = 0;

		results = service.insertWorkValue(workValueObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteWorkValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteWorkValue(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteWorkValue(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateWorkValue(com.cep.spring.model.portfolio.WorkValue)
	 */
	@Override
	public int updateWorkValue(WorkValue workValueObject) throws Exception {
		int results = 0;

		results = service.updateWorkValue(workValueObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getVolunteers(java.lang.Integer)
	 */
	@Override
	public ArrayList<Volunteer> getVolunteers(Integer userId) {
		ArrayList<Volunteer> results = new ArrayList<Volunteer>();

		try {
			results = service.getVolunteers(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertVolunteer(com.cep.spring.model.portfolio.Volunteer)
	 */
	@Override
	public int insertVolunteer(Volunteer volunteerObject) throws Exception {
		int results = 0;

		results = service.insertVolunteer(volunteerObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteVolunteer(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteVolunteer(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteVolunteer(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateVolunteer(com.cep.spring.model.portfolio.Volunteer)
	 */
	@Override
	public int updateVolunteer(Volunteer volunteerObject) throws Exception {
		int results = 0;

		results = service.updateVolunteer(volunteerObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAsvabScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<AsvabScore> getAsvabScore(Integer userId) {
		ArrayList<AsvabScore> results = new ArrayList<AsvabScore>();

		try {
			results = service.getAsvabScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertAsvabScore(com.cep.spring.model.portfolio.AsvabScore)
	 */
	@Override
	public int insertAsvabScore(AsvabScore asvabObject) throws Exception {
		int results = 0;

		results = service.insertAsvabScore(asvabObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteAsvabScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteAsvabScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteAsvabScore(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateAsvabScore(com.cep.spring.model.portfolio.AsvabScore)
	 */
	@Override
	public int updateAsvabScore(AsvabScore asvabObject) throws Exception {
		int results = 0;

		results = service.updateAsvabScore(asvabObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getActScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<ActScore> getActScore(Integer userId) {
		ArrayList<ActScore> results = new ArrayList<ActScore>();

		try {
			results = service.getActScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertActScore(com.cep.spring.model.portfolio.ActScore)
	 */
	@Override
	public int insertActScore(ActScore actObject) throws Exception {
		int results = 0;

		results = service.insertActScore(actObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteActScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteActScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteActScore(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateActScore(com.cep.spring.model.portfolio.ActScore)
	 */
	@Override
	public int updateActScore(ActScore actObject) throws Exception {
		int results = 0;

		results = service.updateActScore(actObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getSatScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<SatScore> getSatScore(Integer userId) {
		ArrayList<SatScore> results = new ArrayList<SatScore>();

		try {
			results = service.getSatScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertSatScore(com.cep.spring.model.portfolio.SatScore)
	 */
	@Override
	public int insertSatScore(SatScore satObject) throws Exception {
		int results = 0;

		results = service.insertSatScore(satObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteSatScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteSatScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteSatScore(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateSatScore(com.cep.spring.model.portfolio.SatScore)
	 */
	@Override
	public int updateSatScore(SatScore satObject) throws Exception {
		int results = 0;

		results = service.updateSatScore(satObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getFyiScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<FyiScore> getFyiScore(Integer userId) {
		ArrayList<FyiScore> results = new ArrayList<FyiScore>();

		try {
			results = service.getFyiScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertFyiScore(com.cep.spring.model.portfolio.FyiScore)
	 */
	@Override
	public int insertFyiScore(FyiScore fyiObject) throws Exception {
		int results = 0;

		results = service.insertFyiScore(fyiObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteFyiScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteFyiScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteFyiScore(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateFyiScore(com.cep.spring.model.portfolio.FyiScore)
	 */
	@Override
	public int updateFyiScore(FyiScore fyiObject) throws Exception {
		int results = 0;

		results = service.updateFyiScore(fyiObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getOtherScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<OtherScore> getOtherScore(Integer userId) {
		ArrayList<OtherScore> results = new ArrayList<OtherScore>();

		try {
			results = service.getOtherScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertOtherScore(com.cep.spring.model.portfolio.OtherScore)
	 */
	@Override
	public int insertOtherScore(OtherScore otherObject) throws Exception {
		int results = 0;

		results = service.insertOtherScore(otherObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteOtherScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteOtherScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteOtherScore(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateOtherScore(com.cep.spring.model.portfolio.OtherScore)
	 */
	@Override
	public int updateOtherScore(OtherScore otherObject) throws Exception {
		int results = 0;

		results = service.updateOtherScore(otherObject);

		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getPlan(java.lang.Integer)
	 */
	@Override
	public ArrayList<Plan> getPlan(Integer userId) {
		ArrayList<Plan> results = new ArrayList<Plan>();

		try {
			results = service.getPlan(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertPlan(com.cep.spring.model.portfolio.Plan)
	 */
	@Override
	public int insertPlan(Plan planObject) throws Exception {
		int results = 0;

		results = service.insertPlan(planObject);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deletePlan(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deletePlan(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deletePlan(userId, id);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updatePlan(com.cep.spring.model.portfolio.Plan)
	 */
	@Override
	public int updatePlan(Plan planObject) throws Exception {
		int results = 0;

		results = service.updatePlan(planObject);

		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#selectNameGrade(java.lang.Integer)
	 */
	@Override
	public ArrayList<UserInfo> selectNameGrade(Integer userId) throws Exception {
		ArrayList<UserInfo> results = new ArrayList<UserInfo>();

		results = service.selectNameGrade(userId);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateInsertNameGrade(com.cep.spring.model.portfolio.UserInfo)
	 */
	@Override
	public int updateInsertNameGrade(UserInfo userInfo) throws Exception {
		int result = 0;

		result = service.updateInsertNameGrade(userInfo);

		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getFavoriteOccupation(java.lang.Integer)
	 */
	@Override
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception {
		ArrayList<FavoriteOccupation> results = new ArrayList<FavoriteOccupation>();

		results = service.getFavoriteOccupation(userId);

		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAccessCode(java.lang.Integer)
	 */
	@Override
	public String getAccessCode(Integer userId) throws Exception {
		return service.getAccessCode(userId);
	}
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertTeacherDemoTaken(java.lang.Object)
	 */
	@Override
	public int insertTeacherDemoTaken(Integer userId) throws Exception {
		return service.insertTeacherDemoTaken(userId);
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getTeacherDemoTaken(java.lang.Integer)
	 */
	@Override
	public int getTeacherDemoTaken(Integer userId) throws Exception {
		return service.getTeacherDemoTaken(userId);
	}

	
}
