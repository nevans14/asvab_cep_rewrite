package com.cep.spring;

import java.io.InputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.cep.spring.dao.mybatis.EssTrainingDAO;
import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.utils.SimpleFileUtils;

import com.cep.spring.model.EssTraining.EssTrainingEmail;

@Component
public class CEPMailer {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private SimpleMailMessage templateMessage;
    @Autowired
    private ContactUsDAO contactUsDAO;
    @Autowired
    private EssTrainingDAO essTrainingDAO;

    /**
     * sendEmailFromCep : Utilize spring to send e-mails.
     * 
     * @param emailAddr
     *            e-mail address to send the email.
     * @param emailMessage
     *            body of the e-mail sent to above address.
     * @param subject
     *            subject for the email.
     */
    public void sendEmailFromCep(String emailAddr, String emailMessage, String subject) {
    	if(isSubscribed(emailAddr))
    		return;
        logger.info("sendEmailFromCep");
        // Create a thread safe "copy" of the template message and customize it
        SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
        msg.setTo(emailAddr);
        msg.setText(emailMessage);
        msg.setSubject(subject);        
        try {
            logger.debug("email Address" + msg.getTo());
            this.mailSender.send(msg);
        } catch (MailException ex) {
            logger.error("Error sending email:" + ex);
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }
    
    /***
     * Sends an confirmation email to the registered user for ESS Training
     * @param emailAddress
     */
    public void sendEssTrainingRegistrationConfirmationEmail(String emailAddress, int essTrainingId) throws Exception {    	
    	// get ess training info from db
    	EssTrainingEmail model = essTrainingDAO.getEssTrainingEmailById(essTrainingId);
    	if (model == null) {
    		throw new Exception("Confirmation Email does not exist for ESS Training ID: " + essTrainingId);
    	}
    	// set up variables
    	MimeMessage mimeMessage = null;
    	MimeMessageHelper mimeMessageHelper = null;
    	FileSystemResource file = null;
    	// set up properties
    	mimeMessage = mailSender.createMimeMessage();
    	mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
    	mimeMessageHelper.setText(model.getContent(), true);
    	mimeMessageHelper.setTo(emailAddress);
    	mimeMessageHelper.setSubject(model.getTitle());
    	mimeMessageHelper.setFrom("postmaster@asvabprogram.com");
    	// add file attachment
    	if (model.getAttachmentLocation() != null && !model.getAttachmentLocation().isEmpty()) {
    		file = new FileSystemResource(model.getAttachmentLocation());
            mimeMessageHelper.addAttachment(file.getFilename(), file);
    	}        
    	// send message
    	this.mailSender.send(mimeMessage);
    }

    /**
     * 
     * Utilizes Mail Sender to send out emails.
     * 
     * 
     * @param emailAddr
     * @param emailMessage
     * @param subject
     * @param attachement
     * @throws Exception
     */
    public void sendEmail(String emailAddr, String emailMessage, String subject, String attachement) throws Exception {

        this.sendEmail(emailAddr, "", emailMessage, subject, attachement);

    }

    /**
     * Utilizes Mail Sender to send out emails.
     * 
     * @param toEmailAddr
     * @param fromEmailAddr
     * @param emailMessage
     * @param subject
     * @param attachement
     * @throws Exception
     */
    public void sendEmail(String toEmailAddr, String fromEmailAddr, String emailMessage, String subject, String attachement) throws Exception {    	
        logger.info("sendEmail");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(toEmailAddr);
        helper.setText(emailMessage);
        helper.setSubject(subject);
        helper.setFrom("postmaster@asvabprogram.com");

        if (!fromEmailAddr.contentEquals("")) {
            helper.setReplyTo(fromEmailAddr);
        } else {
            helper.setReplyTo("asvabcep@gmail.com");
        }
        // add file attachement
        if (attachement != null) {
            FileSystemResource file = new FileSystemResource(attachement);
            helper.addAttachment(file.getFilename(), file);
        }
        this.mailSender.send(message);

    }

    /**
     * Utilizes Mail Sender to send out emails.
     * 
     * @param toEmailAddr
     * @param fromEmailAddr
     * @param emailMessage
     * @param subject
     * @param attachement
     * @throws Exception
     */
    public void sendResetRegisterHtmlEmail(String toEmailAddr, String fromEmailAddr, String emailMessage, String subject, String attachement) throws Exception {    	
        logger.info("sendEmail");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(toEmailAddr);
        helper.setFrom("postmaster@asvabprogram.com");
        helper.setText(emailMessage);
        helper.setSubject(subject);
        if (!fromEmailAddr.contentEquals(""))
            helper.setFrom(fromEmailAddr);

        MimeMultipart multipart = new MimeMultipart("related");
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(emailMessage, "text/html");
        multipart.addBodyPart(messageBodyPart);
        try {
            messageBodyPart = new MimeBodyPart();
            InputStream imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/email-header.png");
            DataSource fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<emailHeader>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/twitter.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<twitter>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/youtube.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<youtube>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/facebook.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<facebook>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/linkedin.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<linkedin>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            this.mailSender.send(message);
            System.out.println("Done");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }
    
    private boolean isSubscribed(String email) {
    	return contactUsDAO.isSubscribed(email);
    }
}
