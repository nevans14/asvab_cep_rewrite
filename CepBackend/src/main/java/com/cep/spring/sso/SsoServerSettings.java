package com.cep.spring.sso;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration
@PropertySource("classpath:SsoProperties.properties")
public class SsoServerSettings {
	
	@Value("${citm.url}")
	private List<String> allowedUris;
	
	public List<String> getAllowedUris() {
		return allowedUris;
	}

	public void setAllowedUris(List<String> allowedUris) {
		this.allowedUris = allowedUris;
	}

	
	public boolean allow(String uri){
		for(String allowedUri: this.allowedUris){
			try {
				URL allowedUrl = new URL(allowedUri);
				URL url = new URL(uri);
				
				if (allowedUrl.getProtocol().equals(url.getProtocol()) &&
					allowedUrl.getHost().equals(url.getHost()) &&
					allowedUrl.getPort() == url.getPort()) {
					return true;
				}
				
			} catch (MalformedURLException e) {
				
			}
		}
		
		return false;
	}



}
