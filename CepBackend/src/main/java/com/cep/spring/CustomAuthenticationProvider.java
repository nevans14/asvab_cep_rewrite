package com.cep.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


/**
 * Used by spring filters to determine if user has been authenticated.  If this
 *  is not implemented the browser will pop up a user dialog for user name and 
 *  password.  
 *  
 *  Note:  The actual decision to validate the user is done in the 
 *  LoginRegistrationController, if user has supplied sufficient credentials then
 *  the Spring Security Context is supplied with the appropriate 
 *  authenticationToken.  This method is utilized by the security filter to 
 *  determine if the request is from an authenticated user, and does this user 
 *  have the appropriate role(s).
 *  
 *  
 *  
 * @author Dave Springer
 *
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	/**
	 * 
	 */
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	      logger.trace("spring custom authentication ");
		  String username = authentication.getName();
	      String password = (String) authentication.getCredentials();
	      logger.trace("username:" + authentication.getName());
	      if ( null == SecurityContextHolder.getContext().getAuthentication() ) return null;
	      
	      String user = SecurityContextHolder.getContext().getAuthentication().getName();
	      
	      System.out.println("username:" + username + "| password:" + password + "| userFromcontext:" + user);
	      		
		  UsernamePasswordAuthenticationToken authenticationToken = 
					 new UsernamePasswordAuthenticationToken(username, "password",     SecurityContextHolder.getContext().getAuthentication().getAuthorities() );
			 authenticationToken.isAuthenticated();
			 return authenticationToken;
	}

	public boolean supports(Class<?> arg0) {
		return true;
	}

}
