package com.cep.spring;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cep.spring.utils.SimpleUtils;

@Component
public class BackUpDatabase {
    private Logger logger = LogManager.getLogger(this.getClass().getName());    
    @Autowired
    JdbcTemplate jdbcTemplate; 
    
    public void backupDatabase() {
        
        logger.info("backupDatabase");
        
        String backupFileName = "CEP_";
        backupFileName += SimpleUtils.getStringInStandardFormatFromDate( new Date()  ).replace(" ","_") + ".bak'";
        backupFileName = backupFileName.replaceAll(":",".");
        // backupFileName = "'C:\\Software\\" + backupFileName;
        System.out.println(backupFileName);
        
        String sql = "BACKUP DATABASE asvab2015 TO DISK = " + backupFileName + "  WITH FORMAT" ;
        
        System.out.println(sql);
//      BACKUP DATABASE asvab2015 TO DISK = 'CEP_2016-06-01_16.15.22.350.bak'  WITH FORMAT        
//      BACKUP DATABASE [asvab2015] TO DISK = 'C:\Software\asvab2015_Tsql_backup.bak'  WITH FORMAT;  

        try {
            jdbcTemplate.execute(sql);
            
        } catch ( RuntimeException re ) {
            logger.error("Error Attempting to backup database:" + re.getMessage());
        }
        logger.debug("backupDatabase complete");
    }
}
