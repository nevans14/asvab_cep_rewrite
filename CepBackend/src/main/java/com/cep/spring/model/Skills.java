package com.cep.spring.model;

import java.io.Serializable;

public class Skills implements Serializable {

	private static final long serialVersionUID = -2521271890040521861L;

	String skillElementId;
	String skillDescription;

	public String getSkillElementId() {
		return skillElementId;
	}

	public void setSkillElementId(String skillElementId) {
		this.skillElementId = skillElementId;
	}

	public String getSkillDescription() {
		return skillDescription;
	}

	public void setSkillDescription(String skillDescription) {
		this.skillDescription = skillDescription;
	}
}
