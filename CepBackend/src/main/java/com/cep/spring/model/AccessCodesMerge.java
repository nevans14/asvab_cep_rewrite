package com.cep.spring.model;

import java.util.Date;

public class AccessCodesMerge {

	private Long userId;
	private String oldAccessCode;
	private String newAccessCode;
	private Date mergeDate;
	private String fyiMovedToNewAccessCode;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getOldAccessCode() {
		return oldAccessCode;
	}
	public void setOldAccessCode(String oldAccessCode) {
		this.oldAccessCode = oldAccessCode;
	}
	public String getNewAccessCode() {
		return newAccessCode;
	}
	public void setNewAccessCode(String newAccessCode) {
		this.newAccessCode = newAccessCode;
	}
	public Date getMergeDate() {
		return mergeDate;
	}
	public void setMergeDate(Date mergeDate) {
		this.mergeDate = mergeDate;
	}
	public String getFyiMovedToNewAccessCode() {
		return fyiMovedToNewAccessCode;
	}
	public void setFyiMovedToNewAccessCode(String fyiMovedToNewAccessCode) {
		this.fyiMovedToNewAccessCode = fyiMovedToNewAccessCode;
	}

}
