package com.cep.spring.model.optionready;

import java.util.List;

public class FlickrPhotoCollectionInfo {
	private Integer page, pages, perpage, total;
	private List<FlickrPhotoCollectionPhoto> photo;
	
	public void setPage(Integer page) {
		this.page = page;
	}
	
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	
	public void setPerPage(Integer perpage) {
		this.perpage = perpage;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public void setPhoto(List<FlickrPhotoCollectionPhoto> photo) {
		this.photo = photo;
	}
	
	public Integer getPage() {
		return page;
	}
	
	public Integer getPages() {
		return pages;
	}
	
	public Integer getPerPage() {
		return perpage;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public List<FlickrPhotoCollectionPhoto> getPhoto() {
		return photo;
	}
}