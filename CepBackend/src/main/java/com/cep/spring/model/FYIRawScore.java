package com.cep.spring.model;

import java.io.Serializable;

public class FYIRawScore implements Serializable {

	private static final long serialVersionUID = 5069430835419406442L;
	int rRawScore, iRawScore, aRawScore, sRawScore, eRawScore, cRawScore;

	public int getrRawScore() {
		return rRawScore;
	}

	public void setrRawScore(int rRawScore) {
		this.rRawScore = rRawScore;
	}

	public int getiRawScore() {
		return iRawScore;
	}

	public void setiRawScore(int iRawScore) {
		this.iRawScore = iRawScore;
	}

	public int getaRawScore() {
		return aRawScore;
	}

	public void setaRawScore(int aRawScore) {
		this.aRawScore = aRawScore;
	}

	public int getsRawScore() {
		return sRawScore;
	}

	public void setsRawScore(int sRawScore) {
		this.sRawScore = sRawScore;
	}

	public int geteRawScore() {
		return eRawScore;
	}

	public void seteRawScore(int eRawScore) {
		this.eRawScore = eRawScore;
	}

	public int getcRawScore() {
		return cRawScore;
	}

	public void setcRawScore(int cRawScore) {
		this.cRawScore = cRawScore;
	}

}
