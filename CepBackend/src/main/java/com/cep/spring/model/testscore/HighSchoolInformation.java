package com.cep.spring.model.testscore;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class HighSchoolInformation {

	private	int	schoolCode;
	private	int	schoolSessionNbr;
	private	int	schoolSpecialInstructions;
	private	int	releaseToServiceDate;
	private	String	schoolCouncellorCd;
//	private	String	serviceResponsibleForScheduling;
    private String  schoolSiteName;
    private String  schoolCityName;
    private String  schoolStateName;
    private String  schoolZipCode;

	/**
	 * @return the schoolCode
	 */
	public int getSchoolCode() {
		return schoolCode;
	}
	/**
	 * @param schoolCode the schoolCode to set
	 */
	public void setSchoolCode(int schoolCode) {
		this.schoolCode = schoolCode;
	}
	/**
	 * @return the schoolSessionNbr
	 */
	public int getSchoolSessionNbr() {
		return schoolSessionNbr;
	}
	/**
	 * @param schoolSessionNbr the schoolSessionNbr to set
	 */
	public void setSchoolSessionNbr(int schoolSessionNbr) {
		this.schoolSessionNbr = schoolSessionNbr;
	}
	/**
	 * @return the schoolSpecialInstructions
	 */
	public int getSchoolSpecialInstructions() {
		return schoolSpecialInstructions;
	}
	/**
	 * @param schoolSpecialInstructions the schoolSpecialInstructions to set
	 */
	public void setSchoolSpecialInstructions(int schoolSpecialInstructions) {
		this.schoolSpecialInstructions = schoolSpecialInstructions;
	}
	/**
	 * @return the releaseToServiceDate
	 */
	public int getReleaseToServiceDate() {
		return releaseToServiceDate;
	}
	/**
	 * @param releaseToServiceDate the releaseToServiceDate to set
	 */
	public void setReleaseToServiceDate(int releaseToServiceDate) {
		this.releaseToServiceDate = releaseToServiceDate;
	}
	/**
	 * @return the schoolCouncellorCd
	 */
	public String getSchoolCouncellorCd() {
		return schoolCouncellorCd;
	}
	/**
	 * @param schoolCouncellorCd the schoolCouncellorCd to set
	 */
	public void setSchoolCouncellorCd(String schoolCouncellorCd) {
		this.schoolCouncellorCd = schoolCouncellorCd;
	}
//	/**
//	 * @return the serviceResponsibleForScheduling
//	 */
//	public String getServiceResponsibleForScheduling() {
//		return serviceResponsibleForScheduling;
//	}
//	/**
//	 * @param serviceResponsibleForScheduling the serviceResponsibleForScheduling to set
//	 */
//	public void setServiceResponsibleForScheduling(String serviceResponsibleForScheduling) {
//		this.serviceResponsibleForScheduling = serviceResponsibleForScheduling;
//	}
	/**
	 * @return the schoolSiteName
	 */
	public String getSchoolSiteName() {
		return schoolSiteName;
	}
	/**
	 * @param schoolSiteName the schoolSiteName to set
	 */
	public void setSchoolSiteName(String schoolSiteName) {
		this.schoolSiteName = schoolSiteName;
	}
	/**
	 * @return the schoolCityName
	 */
	public String getSchoolCityName() {
		return schoolCityName;
	}
	/**
	 * @param schoolCityName the schoolCityName to set
	 */
	public void setSchoolCityName(String schoolCityName) {
		this.schoolCityName = schoolCityName;
	}
	/**
	 * @return the schoolStateName
	 */
	public String getSchoolStateName() {
		return schoolStateName;
	}
	/**
	 * @param schoolStateName the schoolStateName to set
	 */
	public void setSchoolStateName(String schoolStateName) {
		this.schoolStateName = schoolStateName;
	}
	/**
	 * @return the schoolZipCode
	 */
	public String getSchoolZipCode() {
		return schoolZipCode;
	}
	/**
	 * @param schoolZipCode the schoolZipCode to set
	 */
	public void setSchoolZipCode(String schoolZipCode) {
		this.schoolZipCode = schoolZipCode;
	}
}
