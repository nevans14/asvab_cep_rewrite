package com.cep.spring.model;

import java.io.Serializable;

public class Task implements Serializable {

	private static final long serialVersionUID = 2414014892631017585L;
	private String occupationTask;

	public String getOccupationTask() {
		return occupationTask;
	}

	public void setOccupationTask(String occupationTask) {
		this.occupationTask = occupationTask;
	}

}
