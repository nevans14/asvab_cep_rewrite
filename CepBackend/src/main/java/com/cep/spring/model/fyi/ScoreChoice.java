package com.cep.spring.model.fyi;

import java.io.Serializable;
import java.util.Date;

public class ScoreChoice implements Serializable {

	private static final long serialVersionUID = 2243622855231156763L;
	private char interestCodeOne;
	private char interestCodeTwo;
	private char interestCodeThree;
	private String scoreChoice;
	private Integer userId;
	private char gender;
	private Date profileDate;

	public char getInterestCodeOne() {
		return interestCodeOne;
	}

	public void setInterestCodeOne(char interestCodeOne) {
		this.interestCodeOne = interestCodeOne;
	}

	public char getInterestCodeTwo() {
		return interestCodeTwo;
	}

	public void setInterestCodeTwo(char interestCodeTwo) {
		this.interestCodeTwo = interestCodeTwo;
	}

	public char getInterestCodeThree() {
		return interestCodeThree;
	}

	public void setInterestCodeThree(char interestCodeThree) {
		this.interestCodeThree = interestCodeThree;
	}

	public String getScoreChoice() {
		return scoreChoice;
	}

	public void setScoreChoice(String scoreChoice) {
		this.scoreChoice = scoreChoice;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}
	
	  public Date getProfileDate() {
		return profileDate;
	}

	public void setProfileDate(Date profileDate) {
		this.profileDate = profileDate;
	}

}
