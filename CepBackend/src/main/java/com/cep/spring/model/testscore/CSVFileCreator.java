package com.cep.spring.model.testscore;

 import org.springframework.stereotype.Component;

 import com.cep.spring.model.testscore.StudentTestingProgam;
 import com.cep.spring.model.testscore.TestScore;

 @Component
 public class CSVFileCreator {
         
         private static final String DEFAULT_SAPERATOR = ",";
         
         public String getHeader(){
             return
//                 "position"                                              + DEFAULT_SAPERATOR +
                "Source File Name"                                      + DEFAULT_SAPERATOR +
                //Control Information
                "Access Code"                                           + DEFAULT_SAPERATOR +
                "Platform"                                              + DEFAULT_SAPERATOR +
                "Ans Seq Num"                                           + DEFAULT_SAPERATOR +
                "Meps Id"                                               + DEFAULT_SAPERATOR +
                "Met Site Id"                                           + DEFAULT_SAPERATOR +
                "Gender"                                                + DEFAULT_SAPERATOR +
                "Education Level"                                       + DEFAULT_SAPERATOR +
                "Certification"                                         + DEFAULT_SAPERATOR +
                "Test Form"                                             + DEFAULT_SAPERATOR +
                "Scoring Method"                                        + DEFAULT_SAPERATOR +
                "Administered Year"                              		+ DEFAULT_SAPERATOR +
                "Administered Month"                             		+ DEFAULT_SAPERATOR +
                "Administered Day"                               		+ DEFAULT_SAPERATOR +
                        
                //SS ASVAB Combine Control
                "Seq Num"                                               + DEFAULT_SAPERATOR +
                        
                //AFQT Raw SS Composites
                "Test Completion Cd"                              		+ DEFAULT_SAPERATOR +
                "General Science"                                       + DEFAULT_SAPERATOR +
                "Arithmetic Reasoning"                                  + DEFAULT_SAPERATOR +
                "Word Knowledge"                                        + DEFAULT_SAPERATOR +
                "Paragraph Comprehension"                               + DEFAULT_SAPERATOR +
//                "no"                                                  + DEFAULT_SAPERATOR +
//                "cs"                                                  + DEFAULT_SAPERATOR +
                "Automotive and Shop Information"                       + DEFAULT_SAPERATOR +
                "Mathematics Knowledge"                                 + DEFAULT_SAPERATOR +
                "Mechanical Comprehension"                              + DEFAULT_SAPERATOR +
                "Electronics Information"                               + DEFAULT_SAPERATOR +
                "Verbal Expression"                                     + DEFAULT_SAPERATOR +
                "Assembling Objects"                                    + DEFAULT_SAPERATOR +
                "AFQT"                                                  + DEFAULT_SAPERATOR +
                "Army General Technical"                                + DEFAULT_SAPERATOR +
                "Army Clerical"                                         + DEFAULT_SAPERATOR +
                "Army Combat Operations"                                + DEFAULT_SAPERATOR +
                "Army Electronics"                                      + DEFAULT_SAPERATOR +
                "Army Field Artillery"                                  + DEFAULT_SAPERATOR +
                "Army General Maintenance"                              + DEFAULT_SAPERATOR +
                "Army Mechanical Maintenance"                           + DEFAULT_SAPERATOR +
                "Army Operators and Food"                               + DEFAULT_SAPERATOR +
                "Army Surveillance and Communications"                  + DEFAULT_SAPERATOR +
                "Army Skilled Technical"                                + DEFAULT_SAPERATOR +
                "Navy General Technical"                                + DEFAULT_SAPERATOR +
                "Navy Electronics"                                      + DEFAULT_SAPERATOR +
                "Navy Basic Electricity and Electronics"                + DEFAULT_SAPERATOR +
                "Navy Engineering"                                      + DEFAULT_SAPERATOR +
                "Navy Mechanical Maintenance"                           + DEFAULT_SAPERATOR +
                "Navy Mechanical Maintenance 2"                         + DEFAULT_SAPERATOR +
                "Navy Nuclear Field"                                    + DEFAULT_SAPERATOR +
                "Navy Operations Specialist"                            + DEFAULT_SAPERATOR +
                "Navy Hospital Corpsman"                                + DEFAULT_SAPERATOR +
                "Navy Administrative"                                   + DEFAULT_SAPERATOR +
                "Air Force Mechanical"                                  + DEFAULT_SAPERATOR +
                "Air Force Administrative"                              + DEFAULT_SAPERATOR +
                "Air Force General"                                     + DEFAULT_SAPERATOR +
                "Air Force Electrical"                                  + DEFAULT_SAPERATOR +
                "Marine Corps Mechanical Maintenance"                   + DEFAULT_SAPERATOR +
                "Marine Corps General Technical"                        + DEFAULT_SAPERATOR + 
                "Marine Corps Electronics"                              + DEFAULT_SAPERATOR +
                "Marine Corps Clerical"                                 + DEFAULT_SAPERATOR +
                "Booklet Num"                                           + DEFAULT_SAPERATOR +
                        
                //High School Information
                "School Code"                                           + DEFAULT_SAPERATOR +
                "School Session Number"                              	+ DEFAULT_SAPERATOR +
                "School Special Instructions"             				+ DEFAULT_SAPERATOR +
                "Release To Service Date"                  				+ DEFAULT_SAPERATOR +
                "School Councellor Cd"                            		+ DEFAULT_SAPERATOR +
//                "schoolSiteName"                                      + DEFAULT_SAPERATOR +
//                "schoolCityName"                                      + DEFAULT_SAPERATOR +
//                "schoolStateName"                               		+ DEFAULT_SAPERATOR +
//                "schoolZipCode"                                       + DEFAULT_SAPERATOR +
                        
                //Student Information
               "Student Name"                                           + DEFAULT_SAPERATOR +
               "Last Name"                                              + DEFAULT_SAPERATOR +
               "First Name"                                             + DEFAULT_SAPERATOR +
               "Middle Initial"                                         + DEFAULT_SAPERATOR +
               "Student City"                                   		+ DEFAULT_SAPERATOR +
                "Student State"                                  		+ DEFAULT_SAPERATOR +
                "Student ZipCode"                                       + DEFAULT_SAPERATOR +
                "Student Intentions"                            		+ DEFAULT_SAPERATOR +
                        
                //Student Result SheetScores
                "Mil Career Score"                                      + DEFAULT_SAPERATOR +
                "Mil Career Score Category"                        		+ DEFAULT_SAPERATOR +
                        
                //Verbal Ability
                "VA_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "VA_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "VA_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "VA_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "VA_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "VA_SGS"                                                + DEFAULT_SAPERATOR +
                "VA_USL"                                                + DEFAULT_SAPERATOR +
                "VA_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Mathematical Ability
                "MA_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "MA_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MA_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "MA_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "MA_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MA_SGS"                                                + DEFAULT_SAPERATOR +
                "MA_USL"                                                + DEFAULT_SAPERATOR +
                "MA_LSL"                                               	+ DEFAULT_SAPERATOR +
                        
                //Science & Technical Ability
                "TEC_YP_Score"                                  		+ DEFAULT_SAPERATOR +
                "TEC_GS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "TEC_GOS_Percentage"                            		+ DEFAULT_SAPERATOR +
                "TEC_COMP_Percentage"                   				+ DEFAULT_SAPERATOR +
                "TEC_TP_Percentage"                             		+ DEFAULT_SAPERATOR +
                "TEC_SGS"                                               + DEFAULT_SAPERATOR +
                "TEC_USL"                                               + DEFAULT_SAPERATOR +
                "TEC_LSL"                                               + DEFAULT_SAPERATOR +
                        
                //General Science
                "GS_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "GS_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "GS_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "GS_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "GS_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "GS_SGS"                                                + DEFAULT_SAPERATOR +
                "GS_USL"                                                + DEFAULT_SAPERATOR +
                "GS_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Arithmetic Reasoning
                "AR_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "AR_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AR_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "AR_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "AR_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AR_SGS"                                                + DEFAULT_SAPERATOR +
                "AR_USL"                                                + DEFAULT_SAPERATOR +
                "AR_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Word Knowledge
                "WK_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "WK_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "WK_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "WK_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "WK_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "WK_SGS"                                                + DEFAULT_SAPERATOR +
                "WK_USL"                                                + DEFAULT_SAPERATOR +
                "WK_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Paragraph Comprehension
                "PC_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "PC_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "PC_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "PC_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "PC_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "PC_SGS"                                                + DEFAULT_SAPERATOR +
                "PC_USL"                                                + DEFAULT_SAPERATOR +
                "PC_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Mathematics Knowledge
                "MK_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "MK_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MK_GOS_Percentage"                            		 	+ DEFAULT_SAPERATOR +
                "MK_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "MK_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MK_SGS"                                                + DEFAULT_SAPERATOR +
                "MK_USL"                                                + DEFAULT_SAPERATOR +
                "MK_LSL"                                                + DEFAULT_SAPERATOR +
                        
                        
                //Electronics Information
                "EI_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "EI_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "EI_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "EI_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "EI_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "EI_SGS"                                               	+ DEFAULT_SAPERATOR +
                "EI_USL"                                                + DEFAULT_SAPERATOR +
                "EI_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Auto Shop Information
                "AS_YP_Score"                                  			+ DEFAULT_SAPERATOR +
                "AS_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AS_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "AS_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "AS_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AS_SGS"                                                + DEFAULT_SAPERATOR +
                "AS_USL"                                                + DEFAULT_SAPERATOR +
                "AS_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Mechanical Comprehension
                "MC_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "MC_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MC_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "MC_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "MC_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "MC_SGS"                                                + DEFAULT_SAPERATOR +
                "MC_USL"                                                + DEFAULT_SAPERATOR +
                "MC_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //AssemblingObjects
                "AO_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                "AO_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AO_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                "AO_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                "AO_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                "AO_SGS"                                                + DEFAULT_SAPERATOR +
                "AO_USL"                                                + DEFAULT_SAPERATOR +
                "AO_LSL"                                                + DEFAULT_SAPERATOR +
                        
                //Race Fields
                "americanIndian"                                        + DEFAULT_SAPERATOR +
                "asian"                                                 + DEFAULT_SAPERATOR +
                "africanAmerican"                               		+ DEFAULT_SAPERATOR +
                "nativeHawiian"                                         + DEFAULT_SAPERATOR +
                "white"                                                	+ DEFAULT_SAPERATOR +
                "reserved"                                              + DEFAULT_SAPERATOR +
                "hispanic"                                              + DEFAULT_SAPERATOR +
                "notHispanic"                                          	+ DEFAULT_SAPERATOR;
     }
         
         public String getShortNameHeader(){
                 return
 //                 "position"                                              + DEFAULT_SAPERATOR +
                    "Source File Name"                                      + DEFAULT_SAPERATOR +
                    //Control Information
                    "Access Code"                                           + DEFAULT_SAPERATOR +
                    "Platform"                                              + DEFAULT_SAPERATOR +
                    "Ans Seq Num"                                           + DEFAULT_SAPERATOR +
                    "Meps Id"                                               + DEFAULT_SAPERATOR +
                    "Met Site Id"                                           + DEFAULT_SAPERATOR +
                    "Gender"                                                + DEFAULT_SAPERATOR +
                    "Education Level"                                       + DEFAULT_SAPERATOR +
                    "Certification"                                         + DEFAULT_SAPERATOR +
                    "Test Form"                                             + DEFAULT_SAPERATOR +
                    "Scoring Method"                                        + DEFAULT_SAPERATOR +
                    "Administered Year"                              		+ DEFAULT_SAPERATOR +
                    "Administered Month"                             		+ DEFAULT_SAPERATOR +
                    "Administered Day"                               		+ DEFAULT_SAPERATOR +
                            
                    //SS ASVAB Combine Control
                    "Seq Num"                                               + DEFAULT_SAPERATOR +
                            
                    //AFQT Raw SS Composites
                    "TestCompletionCd"                              		+ DEFAULT_SAPERATOR +
                    "gs"                                       				+ DEFAULT_SAPERATOR +
                    "ar"                                  					+ DEFAULT_SAPERATOR +
                    "wk"                                        			+ DEFAULT_SAPERATOR +
                    "pc"                                                    + DEFAULT_SAPERATOR +
                    "no"                                                    + DEFAULT_SAPERATOR +
                    "cs"                                                    + DEFAULT_SAPERATOR +
                    "as"                                                    + DEFAULT_SAPERATOR +
                    "mk"                                                    + DEFAULT_SAPERATOR +
                    "mc"                                                    + DEFAULT_SAPERATOR +
                    "el"                                                    + DEFAULT_SAPERATOR +
                    "ve"                                                    + DEFAULT_SAPERATOR +
                    "ao"                                                    + DEFAULT_SAPERATOR +
                    "afqt"                                                  + DEFAULT_SAPERATOR +
                    "gt_army"                                               + DEFAULT_SAPERATOR +
                    "cl_army"                                               + DEFAULT_SAPERATOR +
                    "co_army"                                               + DEFAULT_SAPERATOR +
                    "el_army"                                               + DEFAULT_SAPERATOR +
                    "fa_army"                                               + DEFAULT_SAPERATOR +
                    "gm_army"                                               + DEFAULT_SAPERATOR +
                    "mm_army"                                               + DEFAULT_SAPERATOR +
                    "of_army"                                               + DEFAULT_SAPERATOR +
                    "sc_army"                                               + DEFAULT_SAPERATOR +
                    "st_army"                                               + DEFAULT_SAPERATOR +
                    "gt_navy"                                               + DEFAULT_SAPERATOR +
                    "el_navy"                                               + DEFAULT_SAPERATOR +
                    "bee_navy"                                              + DEFAULT_SAPERATOR +
                    "eng_navy"                                              + DEFAULT_SAPERATOR +
                    "mec_navy"                                              + DEFAULT_SAPERATOR +
                    "mec2_navy"                                             + DEFAULT_SAPERATOR +
                    "nuc_navy"                                              + DEFAULT_SAPERATOR +
                    "ops_navy"                                              + DEFAULT_SAPERATOR +
                    "hm_navy"                                               + DEFAULT_SAPERATOR +
                    "adm_navy"                                              + DEFAULT_SAPERATOR +
                    "m_af"                                                  + DEFAULT_SAPERATOR +
                    "a_af"                                                  + DEFAULT_SAPERATOR +
                    "g_af"                                                  + DEFAULT_SAPERATOR +
                    "e_af"                                                  + DEFAULT_SAPERATOR +
                    "mm_mc"                                                 + DEFAULT_SAPERATOR +
                    "gt_mc"                                                 + DEFAULT_SAPERATOR + 
                    "el_mc"                                                 + DEFAULT_SAPERATOR +
                    "cl_mc"                                                 + DEFAULT_SAPERATOR +
                    "bookletNum"                                            + DEFAULT_SAPERATOR +
                            
                    //High School Information
                    "School Code"                                           + DEFAULT_SAPERATOR +
                    "School Session Nbr"                              		+ DEFAULT_SAPERATOR +
                    "School Special Instructions"             				+ DEFAULT_SAPERATOR +
                    "release To Service Date"                  				+ DEFAULT_SAPERATOR +
                    "School Councellor Cd"                            		+ DEFAULT_SAPERATOR +
//                    "schoolSiteName"                                      + DEFAULT_SAPERATOR +
//                    "schoolCityName"                                      + DEFAULT_SAPERATOR +
//                    "schoolStateName"                               		+ DEFAULT_SAPERATOR +
//                    "schoolZipCode"                                       + DEFAULT_SAPERATOR +
                            
                    //Student Information
                    "Student Name"                                          + DEFAULT_SAPERATOR +
               		"Last Name"                                             + DEFAULT_SAPERATOR +
               		"First Name"                                            + DEFAULT_SAPERATOR +
               		"Middle Initial"                                        + DEFAULT_SAPERATOR + 
                    "Student City"                                   		+ DEFAULT_SAPERATOR +
                    "Student State"                                  		+ DEFAULT_SAPERATOR +
                    "Student ZipCode"                                       + DEFAULT_SAPERATOR +
                    "Student Intentions"                            		+ DEFAULT_SAPERATOR +
                            
                    //Student Result SheetScores
                    "Mil Career Score"                                      + DEFAULT_SAPERATOR +
                    "Mil Career Score Category"                        		+ DEFAULT_SAPERATOR +
                            
                    //Verbal Ability
                    "VA_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "VA_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "VA_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "VA_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "VA_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "VA_SGS"                                                + DEFAULT_SAPERATOR +
                    "VA_USL"                                                + DEFAULT_SAPERATOR +
                    "VA_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Mathematical Ability
                    "MA_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "MA_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MA_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "MA_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "MA_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MA_SGS"                                                + DEFAULT_SAPERATOR +
                    "MA_USL"                                                + DEFAULT_SAPERATOR +
                    "MA_LSL"                                               	+ DEFAULT_SAPERATOR +
                            
                    //Science & Technical Ability
                    "TEC_YP_Score"                                  		+ DEFAULT_SAPERATOR +
                    "TEC_GS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "TEC_GOS_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "TEC_COMP_Percentage"                   				+ DEFAULT_SAPERATOR +
                    "TEC_TP_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "TEC_SGS"                                               + DEFAULT_SAPERATOR +
                    "TEC_USL"                                               + DEFAULT_SAPERATOR +
                    "TEC_LSL"                                               + DEFAULT_SAPERATOR +
                            
                    //General Science
                    "GS_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "GS_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "GS_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "GS_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "GS_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "GS_SGS"                                                + DEFAULT_SAPERATOR +
                    "GS_USL"                                                + DEFAULT_SAPERATOR +
                    "GS_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Arithmetic Reasoning
                    "AR_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "AR_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AR_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "AR_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "AR_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AR_SGS"                                                + DEFAULT_SAPERATOR +
                    "AR_USL"                                                + DEFAULT_SAPERATOR +
                    "AR_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Word Knowledge
                    "WK_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "WK_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "WK_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "WK_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "WK_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "WK_SGS"                                                + DEFAULT_SAPERATOR +
                    "WK_USL"                                                + DEFAULT_SAPERATOR +
                    "WK_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Paragraph Comprehension
                    "PC_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "PC_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "PC_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "PC_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "PC_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "PC_SGS"                                                + DEFAULT_SAPERATOR +
                    "PC_USL"                                                + DEFAULT_SAPERATOR +
                    "PC_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Mathematics Knowledge
                    "MK_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "MK_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MK_GOS_Percentage"                            		 	+ DEFAULT_SAPERATOR +
                    "MK_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "MK_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MK_SGS"                                                + DEFAULT_SAPERATOR +
                    "MK_USL"                                                + DEFAULT_SAPERATOR +
                    "MK_LSL"                                                + DEFAULT_SAPERATOR +
                            
                            
                    //Electronics Information
                    "EI_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "EI_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "EI_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "EI_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "EI_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "EI_SGS"                                               	+ DEFAULT_SAPERATOR +
                    "EI_USL"                                                + DEFAULT_SAPERATOR +
                    "EI_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Auto Shop Information
                    "AS_YP_Score"                                  			+ DEFAULT_SAPERATOR +
                    "AS_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AS_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "AS_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "AS_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AS_SGS"                                                + DEFAULT_SAPERATOR +
                    "AS_USL"                                                + DEFAULT_SAPERATOR +
                    "AS_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Mechanical Comprehension
                    "MC_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "MC_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MC_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "MC_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "MC_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "MC_SGS"                                                + DEFAULT_SAPERATOR +
                    "MC_USL"                                                + DEFAULT_SAPERATOR +
                    "MC_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //AssemblingObjects
                    "AO_YP_Score"                                   		+ DEFAULT_SAPERATOR +
                    "AO_GS_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AO_GOS_Percentage"                             		+ DEFAULT_SAPERATOR +
                    "AO_COMP_Percentage"                            		+ DEFAULT_SAPERATOR +
                    "AO_TP_Percentage"                              		+ DEFAULT_SAPERATOR +
                    "AO_SGS"                                                + DEFAULT_SAPERATOR +
                    "AO_USL"                                                + DEFAULT_SAPERATOR +
                    "AO_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //Race Fields
                    "americanIndian"                                        + DEFAULT_SAPERATOR +
                    "asian"                                                 + DEFAULT_SAPERATOR +
                    "africanAmerican"                               		+ DEFAULT_SAPERATOR +
                    "nativeHawiian"                                         + DEFAULT_SAPERATOR +
                    "white"                                                	+ DEFAULT_SAPERATOR +
                    "reserved"                                              + DEFAULT_SAPERATOR +
                    "hispanic"                                              + DEFAULT_SAPERATOR +
                    "notHispanic"                                          	+ DEFAULT_SAPERATOR;
         }
         
         public String getCEPHeader(){
                 return
 //                 "position"                                              + DEFAULT_SAPERATOR +
                    "SOURCE_FILE_NAME"                              		+ DEFAULT_SAPERATOR +
                    //Control Information
                    "ACCESS_CODE "                                  		+ DEFAULT_SAPERATOR +
                    "PLATFORM"                                              + DEFAULT_SAPERATOR +
                    "ANS_SEQ_NUM"                                   		+ DEFAULT_SAPERATOR +
                    "MEPS_ID"                                               + DEFAULT_SAPERATOR +
                    "MET_SITE_ID"                                   		+ DEFAULT_SAPERATOR +
                    "GENDER"                                                + DEFAULT_SAPERATOR +
                    "EDUCATION_LEVEL"                               		+ DEFAULT_SAPERATOR +
                    "CERTIFICATION"                                         + DEFAULT_SAPERATOR +
                    "TEST_FORM"                                             + DEFAULT_SAPERATOR +
                    "SCORING_METHOD"                                        + DEFAULT_SAPERATOR +
                    "ADMINISTERED_YEAR"                             		+ DEFAULT_SAPERATOR +
                    "ADMINISTERED_MONTH"                            		+ DEFAULT_SAPERATOR +
                    "ADMINISTERED_DAY"                              		+ DEFAULT_SAPERATOR +
                            
                    //SS ASVAB Combine Control
                    "SEQ_NUM"                                               + DEFAULT_SAPERATOR +
                            
                    //AFQT Raw SS Composites
                    "TEST_COMPLETION_CD"                            		+ DEFAULT_SAPERATOR +
                    "GS"                                                    + DEFAULT_SAPERATOR +
                    "AR"                                                    + DEFAULT_SAPERATOR +
                    "WK"                                                    + DEFAULT_SAPERATOR +
                    "PC"                                                    + DEFAULT_SAPERATOR +
//                  "NO"                                                    + DEFAULT_SAPERATOR +
//                  "CS"                                                    + DEFAULT_SAPERATOR +
                    "AS"                                                    + DEFAULT_SAPERATOR +
                    "MK"                                                    + DEFAULT_SAPERATOR +
                    "MC"                                                    + DEFAULT_SAPERATOR +
                    "EL"                                                    + DEFAULT_SAPERATOR +
                    "VE"                                                    + DEFAULT_SAPERATOR +
                    "AO"                                                    + DEFAULT_SAPERATOR +
                    "AFQT"                                                  + DEFAULT_SAPERATOR +
                    "GT_ARMY"                                               + DEFAULT_SAPERATOR +
                    "CL_ARMY"                                               + DEFAULT_SAPERATOR +
                    "CO_ARMY"                                               + DEFAULT_SAPERATOR +
                    "EL_ARMY"                                               + DEFAULT_SAPERATOR +
                    "FA_ARMY"                                               + DEFAULT_SAPERATOR +
                    "GM_ARMY"                                               + DEFAULT_SAPERATOR +
                    "MM_ARMY"                                               + DEFAULT_SAPERATOR +
                    "OF_ARMY"                                               + DEFAULT_SAPERATOR +
                    "SC_ARMY"                                               + DEFAULT_SAPERATOR +
                    "ST_ARMY"                                               + DEFAULT_SAPERATOR +
                    "GT_NAVY"                                               + DEFAULT_SAPERATOR +
                    "EL_NAVY"                                               + DEFAULT_SAPERATOR +
                    "BEE_NAVY"                                              + DEFAULT_SAPERATOR +
                    "ENG_NAVY"                                              + DEFAULT_SAPERATOR +
                    "MEC_NAVY"                                              + DEFAULT_SAPERATOR +
                    "MEC2_NAVY"                                             + DEFAULT_SAPERATOR +
                    "NUC_NAVY"                                              + DEFAULT_SAPERATOR +
                    "OPS_NAVY"                                              + DEFAULT_SAPERATOR +
                    "HM_NAVY"                                               + DEFAULT_SAPERATOR +
                    "ADM_NAVY"                                              + DEFAULT_SAPERATOR +
                    "M_AF"                                                  + DEFAULT_SAPERATOR +
                    "A_AF"                                                  + DEFAULT_SAPERATOR +
                    "G_AF"                                                  + DEFAULT_SAPERATOR +
                    "E_AF"                                                  + DEFAULT_SAPERATOR +
                    "MM_MC"                                                 + DEFAULT_SAPERATOR +
                    "GT_MC"                                                 + DEFAULT_SAPERATOR + 
                    "EL_MC"                                                 + DEFAULT_SAPERATOR +
                    "CL_MC"                                                 + DEFAULT_SAPERATOR +
                    "BOOKLET_NUM"                                   		+ DEFAULT_SAPERATOR +
                            
                    //High School Information
                    "SCHOOL_CODE"                                   		+ DEFAULT_SAPERATOR +
                    "SCHOOL_SESSION_NBR"                            		+ DEFAULT_SAPERATOR +
                    "SCHOOL_SPECIAL_INSTRUCTIONS"   						+ DEFAULT_SAPERATOR +
                    "RELEASE_TO_SERVICE_DATE"               				+ DEFAULT_SAPERATOR +
                    "SCHOOL_COUNCELLOR_CD"                  				+ DEFAULT_SAPERATOR +
//                    "SCHOOL_SITE_NAME"                              		+ DEFAULT_SAPERATOR +
//                    "SCHOOL_CITY_NAME"                              		+ DEFAULT_SAPERATOR +
//                    "SCHOOL_STATE_NAME"                             		+ DEFAULT_SAPERATOR +
//                    "SCHOOL_ZIPCODE"                                        + DEFAULT_SAPERATOR +
                   
                    //Student Information
                    "STUDENT_NAME"                                          + DEFAULT_SAPERATOR +
               		"LAST_NAME"                                             + DEFAULT_SAPERATOR +
               		"FIRST_NAME"                                            + DEFAULT_SAPERATOR +
               		"MIDDLE_INITIAL"                                        + DEFAULT_SAPERATOR + 
                    "STUDENT_CITY"                                  		+ DEFAULT_SAPERATOR +
                    "STUDENT_STATE"                                         + DEFAULT_SAPERATOR +
                    "STUDENT_ZIPCODE"                               		+ DEFAULT_SAPERATOR +
                    "STUDENT_INTENTIONS"                            		+ DEFAULT_SAPERATOR +
                            
                    //Student Result SheetScores
                    "MIL_CAREER_SCORE"                              		+ DEFAULT_SAPERATOR +
                    "MIL_CAREER_SCORE_CATEGORY"             				+ DEFAULT_SAPERATOR +
                            
                    //VERBAL ABILITY
                    "VA_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "VA_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "VA_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "VA_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "VA_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "VA_SGS"                                                + DEFAULT_SAPERATOR +
                    "VA_USL"                                                + DEFAULT_SAPERATOR +
                    "VA_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //MATHEMATICAL ABILITY
                    "MA_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "MA_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MA_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "MA_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "MA_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MA_SGS"                                                + DEFAULT_SAPERATOR +
                    "MA_USL"                                                + DEFAULT_SAPERATOR +
                    "MA_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //SCIENCE & TECHNICAL ABILITY
                    "TEC_YP_SCORE"                                  		+ DEFAULT_SAPERATOR +
                    "TEC_GS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "TEC_GOS_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "TEC_COMP_PERCENTAGE"                   				+ DEFAULT_SAPERATOR +
                    "TEC_TP_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "TEC_SGS"                                               + DEFAULT_SAPERATOR +
                    "TEC_USL"                                               + DEFAULT_SAPERATOR +
                    "TEC_LSL"                                               + DEFAULT_SAPERATOR +
                            
                    //GENERAL SCIENCE
                    "GS_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "GS_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "GS_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "GS_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "GS_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "GS_SGS"                                                + DEFAULT_SAPERATOR +
                    "GS_USL"                                                + DEFAULT_SAPERATOR +
                    "GS_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //ARITHMETIC REASONING
                    "AR_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "AR_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AR_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "AR_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "AR_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AR_SGS"                                                + DEFAULT_SAPERATOR +
                    "AR_USL"                                                + DEFAULT_SAPERATOR +
                    "AR_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //WORD KNOWLEDGE
                    "WK_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "WK_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "WK_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "WK_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "WK_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "WK_SGS"                                                + DEFAULT_SAPERATOR +
                    "WK_USL"                                                + DEFAULT_SAPERATOR +
                    "WK_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //PARAGRAPH COMPREHENSION
                    "PC_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "PC_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "PC_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "PC_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "PC_TP_PERCENTAGE"                             			+ DEFAULT_SAPERATOR +
                    "PC_SGS"                                                + DEFAULT_SAPERATOR +
                    "PC_USL"                                                + DEFAULT_SAPERATOR +
                    "PC_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //MATHEMATICS KNOWLEDGE
                    "MK_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "MK_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MK_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "MK_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "MK_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MK_SGS"                                                + DEFAULT_SAPERATOR +
                    "MK_USL"                                                + DEFAULT_SAPERATOR +
                    "MK_LSL"                                                + DEFAULT_SAPERATOR +
                            
                            
                    //ELECTRONICS INFORMATION
                    "EI_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "EI_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "EI_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "EI_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "EI_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "EI_SGS"                                                + DEFAULT_SAPERATOR +
                    "EI_USL"                                                + DEFAULT_SAPERATOR +
                    "EI_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //AUTO SHOP INFORMATION
                    "AS_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "AS_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AS_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "AS_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "AS_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AS_SGS"                                                + DEFAULT_SAPERATOR +
                    "AS_USL"                                                + DEFAULT_SAPERATOR +
                    "AS_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //MECHANICAL COMPREHENSION
                    "MC_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "MC_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MC_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "MC_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "MC_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "MC_SGS"                                                + DEFAULT_SAPERATOR +
                    "MC_USL"                                                + DEFAULT_SAPERATOR +
                    "MC_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //ASSEMBLINGOBJECTS
                    "AO_YP_SCORE"                                   		+ DEFAULT_SAPERATOR +
                    "AO_GS_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AO_GOS_PERCENTAGE"                             		+ DEFAULT_SAPERATOR +
                    "AO_COMP_PERCENTAGE"                            		+ DEFAULT_SAPERATOR +
                    "AO_TP_PERCENTAGE"                              		+ DEFAULT_SAPERATOR +
                    "AO_SGS"                                                + DEFAULT_SAPERATOR +
                    "AO_USL"                                                + DEFAULT_SAPERATOR +
                    "AO_LSL"                                                + DEFAULT_SAPERATOR +
                            
                    //RACE FIELDS
                    "AMERICANINDIAN"                                        + DEFAULT_SAPERATOR +
                    "ASIAN"                                                 + DEFAULT_SAPERATOR +
                    "AFRICANAMERICAN"                               		+ DEFAULT_SAPERATOR +
                    "NATIVEHAWIIAN"                                         + DEFAULT_SAPERATOR +
                    "WHITE"                                                 + DEFAULT_SAPERATOR +
                    "RESERVED"                                              + DEFAULT_SAPERATOR +
                    "HISPANIC"                                              + DEFAULT_SAPERATOR +
                    "NOTHISPANIC"                                           + DEFAULT_SAPERATOR;
         }
          public String writeSingleRecord(StudentTestingProgamSelects stp) {
//                  TestScore ts = stp.getStudentTestProgram();
        	  		StudentTestingProgamSelects ts = stp;
                  return
 //                                          stp.getScorePosition()                                                                          + DEFAULT_SAPERATOR +
                                             stp.getResource()																		+ DEFAULT_SAPERATOR +
                                             
                                             //Control Information
                                             ts.getControlInformation().getAccessCode()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getPlatform()                                               + DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getAnsSeqNum()                                       		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getMepsId()                                          		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getMetSiteId()                                       		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getGender()                                          		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getEducationLevel()                          				+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getCertification()                           				+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getTestForm()                                               + DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getScoringMethod()                           				+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getAdministeredYear()                                		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getAdministeredMonth()                              		+ DEFAULT_SAPERATOR +
                                             ts.getControlInformation().getAdministeredDay()                                 		+ DEFAULT_SAPERATOR +
                                                     
                                             //SS ASVAB Combine Control
                                             ts.getsSASVABCombineControl().getSeqNum()                                       		 + DEFAULT_SAPERATOR +
                                                     
                                             //AFQT Raw SS Composites
                                             ts.getaFQTRawSSComposites().getTestCompletionCd()                              		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getGs()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getAr()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getWk()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getPc()                                                     + DEFAULT_SAPERATOR +
//                                           ts.getaFQTRawSSComposites().getNo()                                                     + DEFAULT_SAPERATOR +
//                                           ts.getaFQTRawSSComposites().getCs()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getAs()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMk()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMc()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getEl()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getVe()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getAo()                                                     + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getAfqt()                                           		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getGt_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getCl_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getCo_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getEl_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getFa_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getGm_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMm_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getOf_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getSc_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getSt_army()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getGt_navy()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getEl_navy()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getBee_navy()                                       		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getEng_navy()                                       		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMec_navy()                                       		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMec2_navy()                                      		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getNuc_navy()                                        		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getOps_navy()                                       		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getHm_navy()                                                + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getAdm_navy()                                       		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getM_af()                                           		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getA_af()                                           		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getG_af()                                           		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getE_af()                                           		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getMm_mc()                                          		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getGt_mc()                                          		 + DEFAULT_SAPERATOR + 
                                             ts.getaFQTRawSSComposites().getEl_mc()                                          		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getCl_mc()                                          		 + DEFAULT_SAPERATOR +
                                             ts.getaFQTRawSSComposites().getBookletNum()                                     		 + DEFAULT_SAPERATOR +
                                                     
                                             //High School Information
                                             ts.getHighSchoolInformation().getSchoolCode()                           				 + DEFAULT_SAPERATOR +
                                             ts.getHighSchoolInformation().getSchoolSessionNbr()                        			 + DEFAULT_SAPERATOR +
                                             ts.getHighSchoolInformation().getSchoolSpecialInstructions()   						 + DEFAULT_SAPERATOR +
                                             ts.getHighSchoolInformation().getReleaseToServiceDate()                				 + DEFAULT_SAPERATOR +
                                             ts.getHighSchoolInformation().getSchoolCouncellorCd()                  				 + DEFAULT_SAPERATOR +
//                                             ts.getHighSchoolInformation().getSchoolSiteName()                              		 + DEFAULT_SAPERATOR +
//                                             ts.getHighSchoolInformation().getSchoolCityName()                              		 + DEFAULT_SAPERATOR +
//                                             ts.getHighSchoolInformation().getSchoolStateName()                     				 + DEFAULT_SAPERATOR +
//                                             ts.getHighSchoolInformation().getSchoolZipCode()                               		 + DEFAULT_SAPERATOR +
                                                     
                                             //Student Information
                                             ts.getStudentInformation().getStudentName()                                     		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getFirstName()                                     		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getLastName()                                     		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getMiddleInitial()                                     		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getStudentCity()                                     		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getStudentState()                                    		 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getStudentZipCode()                          				 + DEFAULT_SAPERATOR +
                                             ts.getStudentInformation().getStudentIntentions()                              		 + DEFAULT_SAPERATOR +
                                                     
                                             //Student Result SheetScores
                                             ts.getStudentResultSheetScores().getMilCareerScore()                   				 + DEFAULT_SAPERATOR +
                                             ts.getStudentResultSheetScores().getMilCareerScoreCategory()   						 + DEFAULT_SAPERATOR +
                                                     
                                             //Verbal Ability
                                             ts.getVerbalAbility().getVA_YP_Score()                                          		+ DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_GS_Percentage()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_GOS_Percentage()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_COMP_Percentage()                           				+ DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_TP_Percentage()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_SGS()                                                      + DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_USL()                                                      + DEFAULT_SAPERATOR +
                                             ts.getVerbalAbility().getVA_LSL()                                                      + DEFAULT_SAPERATOR +
                                                     
                                             //Mathematical Ability
                                             ts.getMathematicalAbility().getMA_YP_Score()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_GS_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_GOS_Percentage()                     				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_COMP_Percentage()                    				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_TP_Percentage()                       				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_SGS()                                                + DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_USL()                                                + DEFAULT_SAPERATOR +
                                             ts.getMathematicalAbility().getMA_LSL()                                                + DEFAULT_SAPERATOR +
                                                     
                                            //Science & Technical Ability
                                             ts.getScienceTechnicalAbility().getTEC_YP_Score()                              		+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_GS_Percentage()         						+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_GOS_Percentage()                				+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_COMP_Percentage()               				+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_TP_Percentage()         						+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_SGS()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_USL()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getScienceTechnicalAbility().getTEC_LSL()                                    		+ DEFAULT_SAPERATOR +
                                                     
                                             //General Science
                                             ts.getGeneralScience().getGS_YP_Score()                                                + DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_GS_Percentage()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_GOS_Percentage()                           				+ DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_COMP_Percentage()                          				+ DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_TP_Percentage()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_SGS()                                                     + DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_USL()                                                     + DEFAULT_SAPERATOR +
                                             ts.getGeneralScience().getGS_LSL()                                                     + DEFAULT_SAPERATOR +
                                                     
                                             //Arithmetic Reasoning
                                             ts.getArithmeticReasoning().getAR_YP_Score()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_GS_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_GOS_Percentage()                     				+ DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_COMP_Percentage()                    				+ DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_TP_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_SGS()                                                + DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_USL()                                                + DEFAULT_SAPERATOR +
                                             ts.getArithmeticReasoning().getAR_LSL()                                                + DEFAULT_SAPERATOR +
                                                     
                                             //Word Knowledge
                                             ts.getWordKnowledge().getWK_YP_Score()                                          		+ DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_GS_Percentage()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_GOS_Percentage()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_COMP_Percentage()                           				+ DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_TP_Percentage()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_SGS()                                                      + DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_USL()                                                      + DEFAULT_SAPERATOR +
                                             ts.getWordKnowledge().getWK_LSL()                                                      + DEFAULT_SAPERATOR +
                                                     
                                                     //Paragraph Comprehension
                                             ts.getParagraphComprehension().getPC_YP_Score()                                 		+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_GS_Percentage()                   				+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_GOS_Percentage()                  				+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_COMP_Percentage()         						+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_TP_Percentage()                   				+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_SGS()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_USL()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getParagraphComprehension().getPC_LSL()                                      		+ DEFAULT_SAPERATOR +
                                                     
                                                     //Mathematics Knowledge
                                             ts.getMathematicsKnowledge().getMK_YP_Score()                           				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_GS_Percentage()                     				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_GOS_Percentage()                    				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_COMP_Percentage()                   				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_TP_Percentage()                     				+ DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_SGS()                                               + DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_USL()                                               + DEFAULT_SAPERATOR +
                                             ts.getMathematicsKnowledge().getMK_LSL()                                               + DEFAULT_SAPERATOR +
                                                     
                                                     
                                                     //Electronics Information
                                             ts.getElectronicsInformation().getEI_YP_Score()                                 		+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_GS_Percentage()                   				+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_GOS_Percentage()                  				+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_COMP_Percentage()         						+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_TP_Percentage()                   				+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_SGS()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_USL()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getElectronicsInformation().getEI_LSL()                                      		+ DEFAULT_SAPERATOR +
                                                     
                                             //Auto Shop Information
                                             ts.getAutoShopInformation().getAS_YP_Score()                                    		+ DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_GS_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_GOS_Percentage()                     				+ DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_COMP_Percentage()                    				+ DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_TP_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_SGS()                                                + DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_USL()                                                + DEFAULT_SAPERATOR +
                                             ts.getAutoShopInformation().getAS_LSL()                                                + DEFAULT_SAPERATOR +
                                                     
                                             //Mechanical Comprehension
                                             ts.getMechanicalComprehension().getMC_YP_Score()                               		+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_GS_Percentage()                  				+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_GOS_Percentage()         						+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_COMP_Percentage()                				+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_TP_Percentage()                  				+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_SGS()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_USL()                                     		+ DEFAULT_SAPERATOR +
                                             ts.getMechanicalComprehension().getMC_LSL()                                     		+ DEFAULT_SAPERATOR +
                                                     
                                             //AssemblingObjects
                                             ts.getAssemblingObjects().getAO_YP_Score()                                      		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_GS_Percentage()                                 		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_GOS_Percentage()                                		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_COMP_Percentage()                              		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_TP_Percentage()                                 		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_SGS()                                           		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_USL()                                           		+ DEFAULT_SAPERATOR +
                                             ts.getAssemblingObjects().getAO_LSL()                                           		+ DEFAULT_SAPERATOR +
                                                     
                                            //Race Fields
                                             ts.getRaceFields().getAmericanIndian()                                          		+ DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getAsian()                                                          + DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getAfricanAmerican()                                                + DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getNativeHawiian()                                           		+ DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getWhite()                                                          + DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getReserved()                                                       + DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getHispanic()                                                       + DEFAULT_SAPERATOR +
                                             ts.getRaceFields().getNotHispanic()                                                    + DEFAULT_SAPERATOR;
          }
 }
