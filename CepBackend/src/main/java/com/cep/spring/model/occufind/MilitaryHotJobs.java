package com.cep.spring.model.occufind;

import java.io.Serializable;

public class MilitaryHotJobs implements Serializable {

	private static final long serialVersionUID = -1187281789094553962L;

	private String title, url, service;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
