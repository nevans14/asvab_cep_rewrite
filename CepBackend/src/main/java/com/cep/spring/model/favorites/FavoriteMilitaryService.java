package com.cep.spring.model.favorites;

public class FavoriteMilitaryService {

	private Integer id;
	private Integer userId;
	private String svcId;

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the svcId
	 */
	public String getSvcId() {
		return svcId;
	}
	/**
	 * @param svcId the svcId to set
	 */
	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
