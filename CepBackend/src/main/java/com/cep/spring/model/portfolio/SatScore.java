package com.cep.spring.model.portfolio;

import java.io.Serializable;

public class SatScore implements Serializable {

	private static final long serialVersionUID = -4083765804190655745L;
	private Integer id;
	private Integer userId;
	private double readingScore;
	private double mathScore;
	private double writingScore;
	private String subject;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getReadingScore() {
		return readingScore;
	}

	public void setReadingScore(double readingScore) {
		this.readingScore = readingScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getWritingScore() {
		return writingScore;
	}

	public void setWritingScore(double writingScore) {
		this.writingScore = writingScore;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		long temp;
		temp = Double.doubleToLongBits(mathScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(readingScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		temp = Double.doubleToLongBits(writingScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SatScore)) {
			return false;
		}
		SatScore other = (SatScore) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (Double.doubleToLongBits(mathScore) != Double.doubleToLongBits(other.mathScore)) {
			return false;
		}
		if (Double.doubleToLongBits(readingScore) != Double.doubleToLongBits(other.readingScore)) {
			return false;
		}
		if (subject == null) {
			if (other.subject != null) {
				return false;
			}
		} else if (!subject.equals(other.subject)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		if (Double.doubleToLongBits(writingScore) != Double.doubleToLongBits(other.writingScore)) {
			return false;
		}
		return true;
	}
}
