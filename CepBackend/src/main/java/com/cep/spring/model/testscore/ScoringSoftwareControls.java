package com.cep.spring.model.testscore;

public class ScoringSoftwareControls {
	
	private	String	transformationTblVersion;
	private	String	itemParameterTblVersion;
	private	String	priorDistTblVer;
	/**
	 * @return the transformationTblVersion
	 */
	public String getTransformationTblVersion() {
		return transformationTblVersion;
	}
	/**
	 * @param transformationTblVersion the transformationTblVersion to set
	 */
	public void setTransformationTblVersion(String transformationTblVersion) {
		this.transformationTblVersion = transformationTblVersion;
	}
	/**
	 * @return the itemParameterTblVersion
	 */
	public String getItemParameterTblVersion() {
		return itemParameterTblVersion;
	}
	/**
	 * @param itemParameterTblVersion the itemParameterTblVersion to set
	 */
	public void setItemParameterTblVersion(String itemParameterTblVersion) {
		this.itemParameterTblVersion = itemParameterTblVersion;
	}
	/**
	 * @return the priorDistTblVer
	 */
	public String getPriorDistTblVer() {
		return priorDistTblVer;
	}
	/**
	 * @param priorDistTblVer the priorDistTblVer to set
	 */
	public void setPriorDistTblVer(String priorDistTblVer) {
		this.priorDistTblVer = priorDistTblVer;
	}


}
