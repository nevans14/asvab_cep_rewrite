package com.cep.spring.model;

import java.io.Serializable;

public class GenderScore implements Serializable {

	private static final long serialVersionUID = -4290780825101750146L;

	double gScore;
	char interestCd;

	public double getgScore() {
		return gScore;
	}

	public void setgScore(double gScore) {
		this.gScore = gScore;
	}

	public char getInterestCd() {
		return interestCd;
	}

	public void setInterestCd(char interestCd) {
		this.interestCd = interestCd;
	}

}
