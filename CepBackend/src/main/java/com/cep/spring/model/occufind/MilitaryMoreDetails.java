package com.cep.spring.model.occufind;

import java.io.Serializable;
import java.util.ArrayList;

public class MilitaryMoreDetails implements Serializable {

	private static final long serialVersionUID = -6271918381563656596L;

	private String title;
	private String description;
	private String whatTheyDoBase;
	private String trainingBase;
	private String mcId;
	private String officer;
	private String enlisted;
	private String activeDuty;
	private String reserve;     
	private String isNavy;
	private String isArmy;
	private String isAirForce;
	private String isMarines;
	private String isCoastGuard;
	private String isHotJob;
	private ArrayList<String> service;
	private ArrayList<String> whatTheyDo;
	private ArrayList<String> training;

	public String getIsHotJob() {
		return isHotJob;
	}

	public void setIsHotJob(String isHotJob) {
		this.isHotJob = isHotJob;
	}

	public String getIsNavy() {
		return isNavy;
	}

	public void setIsNavy(String isNavy) {
		this.isNavy = isNavy;
	}

	public String getIsArmy() {
		return isArmy;
	}

	public void setIsArmy(String isArmy) {
		this.isArmy = isArmy;
	}

	public String getIsAirForce() {
		return isAirForce;
	}

	public void setIsAirForce(String isAirForce) {
		this.isAirForce = isAirForce;
	}

	public String getIsMarines() {
		return isMarines;
	}

	public void setIsMarines(String isMarines) {
		this.isMarines = isMarines;
	}

	public String getIsCoastGuard() {
		return isCoastGuard;
	}

	public void setIsCoastGuard(String isCoastGuard) {
		this.isCoastGuard = isCoastGuard;
	}

	public String getOfficer() {
		return officer;
	}

	public void setOfficer(String officer) {
		this.officer = officer;
	}

	public String getEnlisted() {
		return enlisted;
	}

	public void setEnlisted(String enlisted) {
		this.enlisted = enlisted;
	}

	public String getActiveDuty() {
		return activeDuty;
	}

	public void setActiveDuty(String activeDuty) {
		this.activeDuty = activeDuty;
	}

	public String getReserve() {
		return reserve;
	}

	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getWhatTheyDoBase() {
		return whatTheyDoBase;
	}

	public void setWhatTheyDoBase(String whatTheyDoBase) {
		this.whatTheyDoBase = whatTheyDoBase;
	}

	public String getTrainingBase() {
		return trainingBase;
	}

	public void setTrainingBase(String trainingBase) {
		this.trainingBase = trainingBase;
	}

	public ArrayList<String> getWhatTheyDo() {
		return whatTheyDo;
	}

	public void setWhatTheyDo(ArrayList<String> whatTheyDo) {
		this.whatTheyDo = whatTheyDo;
	}

	public ArrayList<String> getTraining() {
		return training;
	}

	public void setTraining(ArrayList<String> training) {
		this.training = training;
	}

	public ArrayList<String> getService() {
		return service;
	}

	public void setService(ArrayList<String> service) {
		this.service = service;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
