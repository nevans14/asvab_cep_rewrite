package com.cep.spring.model.occufind;

import java.io.Serializable;

public class AlternateTitles implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4998265794213783727L;
	
	private String reportedJobTitle;
	private String shownInMyNextMove;

	public String getReportedJobTitle() {
		return reportedJobTitle;
	}
	public void setReportedJobTitle(String reportedJobTitle) {
		this.reportedJobTitle = reportedJobTitle;
	}
	public String getShownInMyNextMove() {
		return shownInMyNextMove;
	}
	public void setShownInMyNextMove(String shownInMyNextMove) {
		this.shownInMyNextMove = shownInMyNextMove;
	}

}
