package com.cep.spring.model.occufind;

public class cip {

	private String cipTitle;
	private String cipDescrpition;


	/**
	 * @return the cipTitle
	 */
	public String getCipTitle() {
		return cipTitle;
	}
	/**
	 * @param cipTitle the cipTitle to set
	 */
	public void setCipTitle(String cipTitle) {
		this.cipTitle = cipTitle;
	}
	public String getCipDescrpition() {
		return cipDescrpition;
	}
	public void setCipDescrpition(String cipDescrpition) {
		this.cipDescrpition = cipDescrpition;
	}
}
