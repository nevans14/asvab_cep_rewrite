package com.cep.spring.model.favorites;

import java.io.Serializable;

public class FavoriteOccupation implements Serializable {

	private static final long serialVersionUID = 8202604746765012670L;
	private Integer id;
	private Integer userId;
	private String onetSocCd;
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOnetSocCd() {
		return onetSocCd;
	}

	public void setOnetSocCd(String onetSocCd) {
		this.onetSocCd = onetSocCd;
	}

}
