package com.cep.spring.model.occufind;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SchoolProfile implements Serializable  {
	
	private static final long serialVersionUID = 8535592400722507090L;

	private int unitId;
	private String InstitutionName;
	private String city;
	private String state;
	private int inStateTutition;
	private int outOfStateTutition;
	private int totalApplicants;
	private int totalAdmitted;
	private int retentionRate;
	private int satCritica25Percent;
	private int satCritica75Percent;
	private int satMath25Percent;
	private int satMath75Percent;
	private int actCritical25Percent;
	private int actCritical75Percent;
	private int certificate;
	private int associates;
	private int bachelors;
	private int masters;
	private int doctoral;
	private int rotc;	
	private int rotcArmy;
	private int rotcNavy;
	private int rotcAirForce;
	private String url;
	@JsonIgnore
	private List<HashMap<String, Object>> majors;
	private Map<String, List<cip>> collegeMajors;
	
	public String getInstitutionName() {
		return InstitutionName;
	}
	public void setInstitutionName(String institutionName) {
		InstitutionName = institutionName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getInStateTutition() {
		return inStateTutition;
	}
	public void setInStateTutition(int inStateTutition) {
		this.inStateTutition = inStateTutition;
	}
	public int getOutOfStateTutition() {
		return outOfStateTutition;
	}
	public void setOutOfStateTutition(int outOfStateTutition) {
		this.outOfStateTutition = outOfStateTutition;
	}
	public int getTotalApplicants() {
		return totalApplicants;
	}
	public void setTotalApplicants(int totalApplicants) {
		this.totalApplicants = totalApplicants;
	}
	public int getTotalAdmitted() {
		return totalAdmitted;
	}
	public void setTotalAdmitted(int totalAdmitted) {
		this.totalAdmitted = totalAdmitted;
	}
	public int getRetentionRate() {
		return retentionRate;
	}
	public void setRetentionRate(int retentionRate) {
		this.retentionRate = retentionRate;
	}
	public int getSatCritica25Percent() {
		return satCritica25Percent;
	}
	public void setSatCritica25Percent(int satCritica25Percent) {
		this.satCritica25Percent = satCritica25Percent;
	}
	public int getSatCritica75Percent() {
		return satCritica75Percent;
	}
	public void setSatCritica75Percent(int satCritica75Percent) {
		this.satCritica75Percent = satCritica75Percent;
	}
	public int getSatMath25Percent() {
		return satMath25Percent;
	}
	public void setSatMath25Percent(int satMath25Percent) {
		this.satMath25Percent = satMath25Percent;
	}
	public int getSatMath75Percent() {
		return satMath75Percent;
	}
	public void setSatMath75Percent(int satMath75Percent) {
		this.satMath75Percent = satMath75Percent;
	}
	public int getActCritical25Percent() {
		return actCritical25Percent;
	}
	public void setActCritical25Percent(int actCritical25Percent) {
		this.actCritical25Percent = actCritical25Percent;
	}
	public int getActCritical75Percent() {
		return actCritical75Percent;
	}
	public void setActCritical75Percent(int actCritical75Percent) {
		this.actCritical75Percent = actCritical75Percent;
	}
	public int getCertificate() {
		return certificate;
	}
	public void setCertificate(int certificate) {
		this.certificate = certificate;
	}
	public int getAssociates() {
		return associates;
	}
	public void setAssociates(int associates) {
		this.associates = associates;
	}
	public int getBachelors() {
		return bachelors;
	}
	public void setBachelors(int bachelors) {
		this.bachelors = bachelors;
	}
	public int getMasters() {
		return masters;
	}
	public void setMasters(int masters) {
		this.masters = masters;
	}
	public int getDoctoral() {
		return doctoral;
	}
	public void setDoctoral(int doctoral) {
		this.doctoral = doctoral;
	}
	public int getRotc() {
		return rotc;
	}
	public void setRotc(int rotc) {
		this.rotc = rotc;
	}
	public int getRotcArmy() {
		return rotcArmy;
	}
	public void setRotcArmy(int rotcArmy) {
		this.rotcArmy = rotcArmy;
	}
	public int getRotcNavy() {
		return rotcNavy;
	}
	public void setRotcNavy(int rotcNavy) {
		this.rotcNavy = rotcNavy;
	}
	public int getRotcAirForce() {
		return rotcAirForce;
	}
	public void setRotcAirForce(int rotcAirForce) {
		this.rotcAirForce = rotcAirForce;
	}
	public int getUnitId() {
		return unitId;
	}
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the collegeMajors
	 */
	public Map<String, List<cip>> getCollegeMajors() {
		return collegeMajors;
	}
	/**
	 * @param collegeMajors the collegeMajors to set
	 */
	public void setCollegeMajors(Map<String, List<cip>> collegeMajors) {
		this.collegeMajors = collegeMajors;
	}
	public List<HashMap<String, Object>> getMajors() {
		return majors;
	}
	public void setMajors(List<HashMap<String, Object>> majors) {
		this.majors = majors;
	}
	
}
