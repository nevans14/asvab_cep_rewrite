package com.cep.spring.model.testscore;

public class WordKnowledge {

	private	int	WK_YP_Score;
	private	int	WK_GS_Percentage;
	private	int	WK_GOS_Percentage;
	private	int	WK_COMP_Percentage;
	private	int	WK_TP_Percentage;
	private	int	WK_SGS;
	private	int	WK_USL;
	private	int	WK_LSL;
	/**
	 * @return the wK_YP_Score
	 */
	public int getWK_YP_Score() {
		return WK_YP_Score;
	}
	/**
	 * @param wK_YP_Score the wK_YP_Score to set
	 */
	public void setWK_YP_Score(int wK_YP_Score) {
		WK_YP_Score = wK_YP_Score;
	}
	/**
	 * @return the wK_GS_Percentage
	 */
	public int getWK_GS_Percentage() {
		return WK_GS_Percentage;
	}
	/**
	 * @param wK_GS_Percentage the wK_GS_Percentage to set
	 */
	public void setWK_GS_Percentage(int wK_GS_Percentage) {
		WK_GS_Percentage = wK_GS_Percentage;
	}
	/**
	 * @return the wK_GOS_Percentage
	 */
	public int getWK_GOS_Percentage() {
		return WK_GOS_Percentage;
	}
	/**
	 * @param wK_GOS_Percentage the wK_GOS_Percentage to set
	 */
	public void setWK_GOS_Percentage(int wK_GOS_Percentage) {
		WK_GOS_Percentage = wK_GOS_Percentage;
	}
	/**
	 * @return the wK_COMP_Percentage
	 */
	public int getWK_COMP_Percentage() {
		return WK_COMP_Percentage;
	}
	/**
	 * @param wK_COMP_Percentage the wK_COMP_Percentage to set
	 */
	public void setWK_COMP_Percentage(int wK_COMP_Percentage) {
		WK_COMP_Percentage = wK_COMP_Percentage;
	}
	/**
	 * @return the wK_TP_Percentage
	 */
	public int getWK_TP_Percentage() {
		return WK_TP_Percentage;
	}
	/**
	 * @param wK_TP_Percentage the wK_TP_Percentage to set
	 */
	public void setWK_TP_Percentage(int wK_TP_Percentage) {
		WK_TP_Percentage = wK_TP_Percentage;
	}
	/**
	 * @return the wK_SGS
	 */
	public int getWK_SGS() {
		return WK_SGS;
	}
	/**
	 * @param wK_SGS the wK_SGS to set
	 */
	public void setWK_SGS(int wK_SGS) {
		WK_SGS = wK_SGS;
	}
	/**
	 * @return the wK_USL
	 */
	public int getWK_USL() {
		return WK_USL;
	}
	/**
	 * @param wK_USL the wK_USL to set
	 */
	public void setWK_USL(int wK_USL) {
		WK_USL = wK_USL;
	}
	/**
	 * @return the wK_LSL
	 */
	public int getWK_LSL() {
		return WK_LSL;
	}
	/**
	 * @param wK_LSL the wK_LSL to set
	 */
	public void setWK_LSL(int wK_LSL) {
		WK_LSL = wK_LSL;
	}

	
}
