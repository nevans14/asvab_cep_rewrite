package com.cep.spring.model;

public class Meps {

	private String schoolCode;
	private String schoolName;
	private String city;
	private String state;
	private String zipCode;
	private String mepsId;
	private String mepsName;
	/**
	 * @return the schoolCode
	 */
	public String getSchoolCode() {
		return schoolCode;
	}
	/**
	 * @param schoolCode the schoolCode to set
	 */
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	/**
	 * @return the schoolName
	 */
	public String getSchoolName() {
		return schoolName;
	}
	/**
	 * @param schoolName the schoolName to set
	 */
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the mepsId
	 */
	public String getMepsId() {
		return mepsId;
	}
	/**
	 * @param mepsId the mepsId to set
	 */
	public void setMepsId(String mepsId) {
		this.mepsId = mepsId;
	}
	/**
	 * @return the mepsName
	 */
	public String getMepsName() {
		return mepsName;
	}
	/**
	 * @param mepsName the mepsName to set
	 */
	public void setMepsName(String mepsName) {
		this.mepsName = mepsName;
	}
	
}
