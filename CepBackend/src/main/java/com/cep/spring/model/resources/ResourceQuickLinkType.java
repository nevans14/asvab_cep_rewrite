package com.cep.spring.model.resources;

import java.io.Serializable;

public class ResourceQuickLinkType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6415098167177097484L;
	private Short quickLinkTypeId;
	public String quickLinkTypeTitle;
	// accessors
	public Short getQuickLinkTypeId() { return quickLinkTypeId; }
	public String getQuickLinkTypeTitle() { return quickLinkTypeTitle; }
	// mutators
	public void setQuickLinkTypeId(Short quickLinkTypeId) { this.quickLinkTypeId = quickLinkTypeId; }
	public void setQuickLinkTypeTitle(String quickLinkTypeTitle) { this.quickLinkTypeTitle = quickLinkTypeTitle; }
}