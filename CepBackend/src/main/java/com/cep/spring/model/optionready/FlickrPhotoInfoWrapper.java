package com.cep.spring.model.optionready;

import com.cep.spring.model.optionready.FlickrPhotoInfo;

public class FlickrPhotoInfoWrapper {
	private FlickrPhotoInfo photo;
	
	public void setFlickrPhotoInfo(FlickrPhotoInfo photo) {
		this.photo = photo;
	}
	
	public FlickrPhotoInfo getFlickrPhotoInfo() {
		return photo;
	}
}