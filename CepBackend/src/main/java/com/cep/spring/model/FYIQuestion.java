package com.cep.spring.model;

import java.io.Serializable;

public class FYIQuestion implements Serializable{

	private static final long serialVersionUID = -4578610842139199004L;
	
	int qId;
	String qText;
	String interestCd;
	
	public int getqId() {
		return qId;
	}
	public void setqId(int qId) {
		this.qId = qId;
	}
	public String getqText() {
		return qText;
	}
	public void setqText(String qText) {
		this.qText = qText;
	}
	public String getInterestCd() {
		return interestCd;
	}
	public void setInterestCd(String interestCd) {
		this.interestCd = interestCd;
	}
}
