package com.cep.spring.model;

public class Registration {
	
	private String email = "";
	private String subject = "";
	private String accessCode = "";
	private String password = "";
	private String resetKey = "";
	private String studentId = "";
	private String recaptchaResponse;
	private boolean optedInCommunications = false;
	private boolean updateFYI = false;
	private boolean updateFavorates = false;
	private boolean updateProfile = false;
	
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getResetKey() {
		return resetKey;
	}
	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}	
	public boolean isOptedInCommunications() {
		return optedInCommunications;
	}	
	public void setOptedInCommunications(boolean optedInCommunications) {
		this.optedInCommunications = optedInCommunications;
	}	
	public boolean isUpdateFYI() {
		return updateFYI;
	}
	public void setUpdateFYI(boolean updateFYI) {
		this.updateFYI = updateFYI;
	}
	public boolean isUpdateFavorates() {
		return updateFavorates;
	}
	public void setUpdateFavorates(boolean updateFavorates) {
		this.updateFavorates = updateFavorates;
	}
	public boolean isUpdateProfile() {
		return updateProfile;
	}
	public void setUpdateProfile(boolean updateProfile) {
		this.updateProfile = updateProfile;
	}

	public String getRecaptchaResponse() {
		return recaptchaResponse;
	}

	public void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse;
	}
}
