package com.cep.spring.model.portfolio;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = 3586472287299344754L;
	private Integer userId;
	private String name, gpa;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGpa() {
		return gpa;
	}

	public void setGpa(String gpa) {
		this.gpa = gpa;
	}
}
