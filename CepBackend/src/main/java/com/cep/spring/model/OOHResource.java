package com.cep.spring.model;

import java.io.Serializable;

public class OOHResource implements Serializable {

	private static final long serialVersionUID = -2795095471619430621L;
	String onetSoc;
	String oohId;
	String oohTitle;
	String oohURL;

	public String getOnetSoc() {
		return onetSoc;
	}

	public void setOnetSoc(String onetSoc) {
		this.onetSoc = onetSoc;
	}

	public String getOohId() {
		return oohId;
	}

	public void setOohId(String oohId) {
		this.oohId = oohId;
	}

	public String getOohTitle() {
		return oohTitle;
	}

	public void setOohTitle(String oohTitle) {
		this.oohTitle = oohTitle;
	}

	public String getOohURL() {
		return oohURL;
	}

	public void setOohURL(String oohURL) {
		this.oohURL = oohURL;
	}

}
