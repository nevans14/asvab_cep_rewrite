package com.cep.spring.model.EssTraining;

public class EssTrainingEmail {
	private Integer essTrainingId;	
	private String title, content, attachmentLocation;
	
	public void setEssTrainingId (Integer essTrainingId) { this.essTrainingId = essTrainingId; }
	public Integer getEssTrainingId () { return this.essTrainingId; }
	
	public void setTitle(String title) { this.title = title; }
	public String getTitle() { return title; }
	
	public void setContent(String content) { this.content = content; }
	public String getContent() { return content; }
	
	public void setAttachmentLocation(String attachmentLocation) { this.attachmentLocation = attachmentLocation; }
	public String getAttachmentLocation() { return attachmentLocation; }
}