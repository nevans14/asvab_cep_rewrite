package com.cep.spring.model.fyi;

import java.io.Serializable;

public class FYIAnswers implements Serializable {

	private static final long serialVersionUID = 853715720341729643L;

	private int qId;
	private int answer;

	public int getqId() {
		return qId;
	}

	public void setqId(int qId) {
		this.qId = qId;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

}
