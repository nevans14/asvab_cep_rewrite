package com.cep.spring.model.portfolio;

import java.io.Serializable;

public class FyiScore implements Serializable {
	
	private static final long serialVersionUID = -6561273359655568273L;
	private Integer id;
	private Integer userId;
	private char interestCodeOne;
	private char interestCodeTwo;
	private char interestCodeThree;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public char getInterestCodeOne() {
		return interestCodeOne;
	}

	public void setInterestCodeOne(char interestCodeOne) {
		this.interestCodeOne = interestCodeOne;
	}

	public char getInterestCodeTwo() {
		return interestCodeTwo;
	}

	public void setInterestCodeTwo(char interestCodeTwo) {
		this.interestCodeTwo = interestCodeTwo;
	}

	public char getInterestCodeThree() {
		return interestCodeThree;
	}

	public void setInterestCodeThree(char interestCodeThree) {
		this.interestCodeThree = interestCodeThree;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + interestCodeOne;
		result = prime * result + interestCodeThree;
		result = prime * result + interestCodeTwo;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FyiScore)) {
			return false;
		}
		FyiScore other = (FyiScore) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (interestCodeOne != other.interestCodeOne) {
			return false;
		}
		if (interestCodeThree != other.interestCodeThree) {
			return false;
		}
		if (interestCodeTwo != other.interestCodeTwo) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}
}
