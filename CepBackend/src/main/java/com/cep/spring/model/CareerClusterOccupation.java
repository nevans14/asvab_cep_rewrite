package com.cep.spring.model;

import java.io.Serializable;

public class CareerClusterOccupation implements Serializable {

	private static final long serialVersionUID = -3225695734556301460L;
	private String onetSoc;
	private String title;
	private int ccId;

	public String getOnetSoc() {
		return onetSoc;
	}

	public void setOnetSoc(String onetSoc) {
		this.onetSoc = onetSoc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCcId() {
		return ccId;
	}

	public void setCcId(int ccId) {
		this.ccId = ccId;
	}
}
