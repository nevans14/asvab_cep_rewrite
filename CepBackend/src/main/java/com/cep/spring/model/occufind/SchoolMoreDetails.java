package com.cep.spring.model.occufind;

import java.io.Serializable;

public class SchoolMoreDetails implements Serializable {

	private static final long serialVersionUID = -8638260906505902338L;

	private Integer unitId;
	private String schoolName;
	private String city;
	private String state;
	private String link;
	private Integer inStateTuition;
	private Integer outStateTuition;
	private Boolean certificate;
	private Boolean associates;
	private Boolean bachelors;
	private Boolean masters;
	private Boolean doctoral;

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getInStateTuition() {
		return inStateTuition;
	}

	public void setInStateTuition(Integer inStateTuition) {
		this.inStateTuition = inStateTuition;
	}

	public Integer getOutStateTuition() {
		return outStateTuition;
	}

	public void setOutStateTuition(Integer outStateTuition) {
		this.outStateTuition = outStateTuition;
	}

	public Boolean getCertificate() {
		return certificate;
	}

	public void setCertificate(Boolean certificate) {
		this.certificate = certificate;
	}

	public Boolean getAssociates() {
		return associates;
	}

	public void setAssociates(Boolean associates) {
		this.associates = associates;
	}

	public Boolean getBachelors() {
		return bachelors;
	}

	public void setBachelors(Boolean bachelors) {
		this.bachelors = bachelors;
	}

	public Boolean getMasters() {
		return masters;
	}

	public void setMasters(Boolean masters) {
		this.masters = masters;
	}

	public Boolean getDoctoral() {
		return doctoral;
	}

	public void setDoctoral(Boolean doctoral) {
		this.doctoral = doctoral;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

}
