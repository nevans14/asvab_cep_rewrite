package com.cep.spring.model;

import java.io.Serializable;

public class ShareToCounselorForm implements Serializable {
	private static final long serialVersionUID = -1467977326576970352L;
	private String email, studentHeardUs, studentHeardUsOther, recaptcha;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getStudentHeardUs() {
		return studentHeardUs;
	}
	
	public void setStudentHeardUs(String studentHeardUs) {
		this.studentHeardUs = studentHeardUs;
	}
	
	public String getStudentHeardUsOther() {
		return studentHeardUsOther;
	}
	
	public void setStudentHeardUsOther(String studentHeardUsOther) {
		this.studentHeardUsOther = studentHeardUsOther;
	}
	
	public String getRecaptcha() {
		return recaptcha;
	}
	
	public void setRecaptcha (String recaptcha) {
		this.recaptcha = recaptcha;
	}
}