package com.cep.spring.model.resources;

import java.io.Serializable;
import java.util.ArrayList;

public class ResourceTopic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7023072458933644354L;
	private Short resourceTopicId;
	private String resourceTopicTitle;
	private ArrayList<Resource> resources;
	// accessors
	public Short getResourceTopicId() { return resourceTopicId; }
	public String getResourceTopicTitle() { return resourceTopicTitle; }
	public ArrayList<Resource> getResources() { return resources; }
	// mutators
	public void setResourceTopicId(Short resourceTopicId) { this.resourceTopicId = resourceTopicId; }
	public void setResourceTopicTitle(String resourceTopicTitle) { this.resourceTopicTitle = resourceTopicTitle; }
	public void setResources(ArrayList<Resource> resources) { this.resources = resources; }
}