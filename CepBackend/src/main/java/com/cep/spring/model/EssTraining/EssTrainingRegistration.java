package com.cep.spring.model.EssTraining;

import java.io.Serializable;

public class EssTrainingRegistration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3217374445291721960L;
	private Integer essTrainingId, essTrainingRegistrationId;	
	private String registrationFirstName, 
		registrationLastName, 
		registrationEmailAddress, 
		registrationOrganization, 
		registrationRole,
		registrationComment, 
		registrationDietaryRestrictions, 
		registrationMobilityRestrictions;
	
	public void setEssTrainingRegistrationId(Integer essTrainingRegistrationId) {
		this.essTrainingRegistrationId = essTrainingRegistrationId;
	}
	
	public Integer getEssTrainingRegistrationId() {
		return this.essTrainingRegistrationId;
	}
	
	public void setEssTrainingId(Integer essTrainingId) {
		this.essTrainingId = essTrainingId;
	}
	
	public Integer getEssTrainingId() {
		return this.essTrainingId;
	}

	public String getRegistrationFirstName() {
		return registrationFirstName;
	}

	public void setRegistrationFirstName(String registrationFirstName) {
		this.registrationFirstName = registrationFirstName;
	}

	public String getRegistrationLastName() {
		return registrationLastName;
	}

	public void setRegistrationLastName(String registrationLastName) {
		this.registrationLastName = registrationLastName;
	}

	public String getRegistrationEmailAddress() {
		return registrationEmailAddress;
	}

	public void setRegistrationEmailAddress(String registrationEmailAddress) {
		this.registrationEmailAddress = registrationEmailAddress;
	}

	public String getRegistrationOrganization() {
		return registrationOrganization;
	}

	public void setRegistrationOrganization(String registrationOrganization) {
		this.registrationOrganization = registrationOrganization;
	}
	
	public void setRegistrationRole(String registrationRole) {
		this.registrationRole = registrationRole;
	}
	
	public String getRegistrationRole() {
		return this.registrationRole;
	}

	public String getRegistrationComment() {
		return registrationComment;
	}

	public void setRegistrationComment(String registrationComment) {
		this.registrationComment = registrationComment;
	}

	public String getRegistrationDietaryRestrictions() {
		return registrationDietaryRestrictions;
	}

	public void setRegistrationDietaryRestrictions(String registrationDietaryRestrictions) {
		this.registrationDietaryRestrictions = registrationDietaryRestrictions;
	}

	public String getRegistrationMobilityRestrictions() {
		return registrationMobilityRestrictions;
	}

	public void setRegistrationMobilityRestrictions(String registrationMobilityRestrictions) {
		this.registrationMobilityRestrictions = registrationMobilityRestrictions;
	}
}