package com.cep.spring.model.testscore;

public class TestScoringIndicatorsAndControls {
	
	private	int	shotIndicator;
	private	int	specialStudies;
	private	int	page1Status;
	private	int	page2Status;
	private	int	page3Status;
	/**
	 * @return the shotIndicator
	 */
	public int getShotIndicator() {
		return shotIndicator;
	}
	/**
	 * @param shotIndicator the shotIndicator to set
	 */
	public void setShotIndicator(int shotIndicator) {
		this.shotIndicator = shotIndicator;
	}
	/**
	 * @return the specialStudies
	 */
	public int getSpecialStudies() {
		return specialStudies;
	}
	/**
	 * @param specialStudies the specialStudies to set
	 */
	public void setSpecialStudies(int specialStudies) {
		this.specialStudies = specialStudies;
	}
	/**
	 * @return the page1Status
	 */
	public int getPage1Status() {
		return page1Status;
	}
	/**
	 * @param page1Status the page1Status to set
	 */
	public void setPage1Status(int page1Status) {
		this.page1Status = page1Status;
	}
	/**
	 * @return the page2Status
	 */
	public int getPage2Status() {
		return page2Status;
	}
	/**
	 * @param page2Status the page2Status to set
	 */
	public void setPage2Status(int page2Status) {
		this.page2Status = page2Status;
	}
	/**
	 * @return the page3Status
	 */
	public int getPage3Status() {
		return page3Status;
	}
	/**
	 * @param page3Status the page3Status to set
	 */
	public void setPage3Status(int page3Status) {
		this.page3Status = page3Status;
	}

}
