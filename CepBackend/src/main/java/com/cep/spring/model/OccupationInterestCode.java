package com.cep.spring.model;

import java.io.Serializable;

public class OccupationInterestCode implements Serializable {

	private static final long serialVersionUID = 593946518640273279L;
	private String interestCd;

	public String getInterestCd() {
		return interestCd;
	}

	public void setInterestCd(String interestCd) {
		this.interestCd = interestCd;
	}

}
