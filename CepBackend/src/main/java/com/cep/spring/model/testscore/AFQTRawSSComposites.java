package com.cep.spring.model.testscore;

public class AFQTRawSSComposites {

	 private int  testCompletionCd;
//     private int  rawScore;
     private int  gs;
     private int  ar;
     private int  wk;
     private int  pc;
//     private int  no;
//     private int  cs;
     private int  as;
     private int  mk;
     private int  mc;
     private int  el;
     private int  ve;
     private int  ao;
     private int  afqt;
     private int  gt_army;
     private int  gl_army;
     private int  cl_army;
     private int  co_army;
     private int  el_army;
     private int  fa_army;
     private int  gm_army;
     private int  mm_army;
     private int  of_army;
     private int  sc_army;
     private int  st_army;
     private int  gt_navy;
     private int  el_navy;
     private int  bee_navy;
     private int  eng_navy;
     private int  mec_navy;
     private int  mec2_navy;
     private int  nuc_navy;
     private int  ops_navy;
     private int  hm_navy;
     private int  adm_navy;
     private int  m_af;
     private int  a_af;
     private int  g_af;
     private int  e_af;
     private int  mm_mc;
     private int  gt_mc; 
     private int  el_mc;
     private int  cl_mc;
     private String  bookletNum;
	/**
	 * @return the testCompletionCd
	 */
	public int getTestCompletionCd() {
		return testCompletionCd;
	}
	/**
	 * @param testCompletionCd the testCompletionCd to set
	 */
	public void setTestCompletionCd(int testCompletionCd) {
		this.testCompletionCd = testCompletionCd;
	}
//	/**
//	 * @return the rawScore
//	 */
//	public int getRawScore() {
//		return rawScore;
//	}
//	/**
//	 * @param rawScore the rawScore to set
//	 */
//	public void setRawScore(int rawScore) {
//		this.rawScore = rawScore;
//	}
	/**
	 * @return the gs
	 */
	public int getGs() {
		return gs;
	}
	/**
	 * @param gs the gs to set
	 */
	public void setGs(int gs) {
		this.gs = gs;
	}
	/**
	 * @return the ar
	 */
	public int getAr() {
		return ar;
	}
	/**
	 * @param ar the ar to set
	 */
	public void setAr(int ar) {
		this.ar = ar;
	}
	/**
	 * @return the wk
	 */
	public int getWk() {
		return wk;
	}
	/**
	 * @param wk the wk to set
	 */
	public void setWk(int wk) {
		this.wk = wk;
	}
	/**
	 * @return the pc
	 */
	public int getPc() {
		return pc;
	}
	/**
	 * @param pc the pc to set
	 */
	public void setPc(int pc) {
		this.pc = pc;
	}
//	/**
//	 * @return the no
//	 */
//	public int getNo() {
//		return no;
//	}
//	/**
//	 * @param no the no to set
//	 */
//	public void setNo(int no) {
//		this.no = no;
//	}
//	/**
//	 * @return the cs
//	 */
//	public int getCs() {
//		return cs;
//	}
//	/**
//	 * @param cs the cs to set
//	 */
//	public void setCs(int cs) {
//		this.cs = cs;
//	}
	/**
	 * @return the as
	 */
	public int getAs() {
		return as;
	}
	/**
	 * @param as the as to set
	 */
	public void setAs(int as) {
		this.as = as;
	}
	/**
	 * @return the mk
	 */
	public int getMk() {
		return mk;
	}
	/**
	 * @param mk the mk to set
	 */
	public void setMk(int mk) {
		this.mk = mk;
	}
	/**
	 * @return the mc
	 */
	public int getMc() {
		return mc;
	}
	/**
	 * @param mc the mc to set
	 */
	public void setMc(int mc) {
		this.mc = mc;
	}
	/**
	 * @return the el
	 */
	public int getEl() {
		return el;
	}
	/**
	 * @param el the el to set
	 */
	public void setEl(int el) {
		this.el = el;
	}
	/**
	 * @return the ve
	 */
	public int getVe() {
		return ve;
	}
	/**
	 * @param ve the ve to set
	 */
	public void setVe(int ve) {
		this.ve = ve;
	}
	/**
	 * @return the ao
	 */
	public int getAo() {
		return ao;
	}
	/**
	 * @param ao the ao to set
	 */
	public void setAo(int ao) {
		this.ao = ao;
	}
	/**
	 * @return the afqt
	 */
	public int getAfqt() {
		return afqt;
	}
	/**
	 * @param afqt the afqt to set
	 */
	public void setAfqt(int afqt) {
		this.afqt = afqt;
	}
	/**
	 * @return the gt_army
	 */
	public int getGt_army() {
		return gt_army;
	}
	/**
	 * @param gt_army the gt_army to set
	 */
	public void setGt_army(int gt_army) {
		this.gt_army = gt_army;
	}
	public int getGl_army() {
		return gl_army;
	}
	public void setGl_army(int gl_army) {
		this.gl_army = gl_army;
		this.gt_army  = gl_army;
	}
	/**
	 * @return the cl_army
	 */
	public int getCl_army() {
		return cl_army;
	}
	/**
	 * @param cl_army the cl_army to set
	 */
	public void setCl_army(int cl_army) {
		this.cl_army = cl_army;
	}
	/**
	 * @return the co_army
	 */
	public int getCo_army() {
		return co_army;
	}
	/**
	 * @param co_army the co_army to set
	 */
	public void setCo_army(int co_army) {
		this.co_army = co_army;
	}
	/**
	 * @return the el_army
	 */
	public int getEl_army() {
		return el_army;
	}
	/**
	 * @param el_army the el_army to set
	 */
	public void setEl_army(int el_army) {
		this.el_army = el_army;
	}
	/**
	 * @return the fa_army
	 */
	public int getFa_army() {
		return fa_army;
	}
	/**
	 * @param fa_army the fa_army to set
	 */
	public void setFa_army(int fa_army) {
		this.fa_army = fa_army;
	}
	/**
	 * @return the gm_army
	 */
	public int getGm_army() {
		return gm_army;
	}
	/**
	 * @param gm_army the gm_army to set
	 */
	public void setGm_army(int gm_army) {
		this.gm_army = gm_army;
	}
	/**
	 * @return the mm_army
	 */
	public int getMm_army() {
		return mm_army;
	}
	/**
	 * @param mm_army the mm_army to set
	 */
	public void setMm_army(int mm_army) {
		this.mm_army = mm_army;
	}
	/**
	 * @return the of_army
	 */
	public int getOf_army() {
		return of_army;
	}
	/**
	 * @param of_army the of_army to set
	 */
	public void setOf_army(int of_army) {
		this.of_army = of_army;
	}
	/**
	 * @return the sc_army
	 */
	public int getSc_army() {
		return sc_army;
	}
	/**
	 * @param sc_army the sc_army to set
	 */
	public void setSc_army(int sc_army) {
		this.sc_army = sc_army;
	}
	/**
	 * @return the st_army
	 */
	public int getSt_army() {
		return st_army;
	}
	/**
	 * @param st_army the st_army to set
	 */
	public void setSt_army(int st_army) {
		this.st_army = st_army;
	}
	/**
	 * @return the gt_navy
	 */
	public int getGt_navy() {
		return gt_navy;
	}
	/**
	 * @param gt_navy the gt_navy to set
	 */
	public void setGt_navy(int gt_navy) {
		this.gt_navy = gt_navy;
	}
	/**
	 * @return the el_navy
	 */
	public int getEl_navy() {
		return el_navy;
	}
	/**
	 * @param el_navy the el_navy to set
	 */
	public void setEl_navy(int el_navy) {
		this.el_navy = el_navy;
	}
	/**
	 * @return the bee_navy
	 */
	public int getBee_navy() {
		return bee_navy;
	}
	/**
	 * @param bee_navy the bee_navy to set
	 */
	public void setBee_navy(int bee_navy) {
		this.bee_navy = bee_navy;
	}
	/**
	 * @return the eng_navy
	 */
	public int getEng_navy() {
		return eng_navy;
	}
	/**
	 * @param eng_navy the eng_navy to set
	 */
	public void setEng_navy(int eng_navy) {
		this.eng_navy = eng_navy;
	}
	/**
	 * @return the mec_navy
	 */
	public int getMec_navy() {
		return mec_navy;
	}
	/**
	 * @param mec_navy the mec_navy to set
	 */
	public void setMec_navy(int mec_navy) {
		this.mec_navy = mec_navy;
	}
	/**
	 * @return the mec2_navy
	 */
	public int getMec2_navy() {
		return mec2_navy;
	}
	/**
	 * @param mec2_navy the mec2_navy to set
	 */
	public void setMec2_navy(int mec2_navy) {
		this.mec2_navy = mec2_navy;
	}
	/**
	 * @return the nuc_navy
	 */
	public int getNuc_navy() {
		return nuc_navy;
	}
	/**
	 * @param nuc_navy the nuc_navy to set
	 */
	public void setNuc_navy(int nuc_navy) {
		this.nuc_navy = nuc_navy;
	}
	/**
	 * @return the ops_navy
	 */
	public int getOps_navy() {
		return ops_navy;
	}
	/**
	 * @param ops_navy the ops_navy to set
	 */
	public void setOps_navy(int ops_navy) {
		this.ops_navy = ops_navy;
	}
	/**
	 * @return the hm_navy
	 */
	public int getHm_navy() {
		return hm_navy;
	}
	/**
	 * @param hm_navy the hm_navy to set
	 */
	public void setHm_navy(int hm_navy) {
		this.hm_navy = hm_navy;
	}
	/**
	 * @return the adm_navy
	 */
	public int getAdm_navy() {
		return adm_navy;
	}
	/**
	 * @param adm_navy the adm_navy to set
	 */
	public void setAdm_navy(int adm_navy) {
		this.adm_navy = adm_navy;
	}
	/**
	 * @return the m_af
	 */
	public int getM_af() {
		return m_af;
	}
	/**
	 * @param m_af the m_af to set
	 */
	public void setM_af(int m_af) {
		this.m_af = m_af;
	}
	/**
	 * @return the a_af
	 */
	public int getA_af() {
		return a_af;
	}
	/**
	 * @param a_af the a_af to set
	 */
	public void setA_af(int a_af) {
		this.a_af = a_af;
	}
	/**
	 * @return the g_af
	 */
	public int getG_af() {
		return g_af;
	}
	/**
	 * @param g_af the g_af to set
	 */
	public void setG_af(int g_af) {
		this.g_af = g_af;
	}
	/**
	 * @return the e_af
	 */
	public int getE_af() {
		return e_af;
	}
	/**
	 * @param e_af the e_af to set
	 */
	public void setE_af(int e_af) {
		this.e_af = e_af;
	}
	/**
	 * @return the mm_mc
	 */
	public int getMm_mc() {
		return mm_mc;
	}
	/**
	 * @param mm_mc the mm_mc to set
	 */
	public void setMm_mc(int mm_mc) {
		this.mm_mc = mm_mc;
	}
	/**
	 * @return the gt_mc
	 */
	public int getGt_mc() {
		return gt_mc;
	}
	/**
	 * @param gt_mc the gt_mc to set
	 */
	public void setGt_mc(int gt_mc) {
		this.gt_mc = gt_mc;
	}
	/**
	 * @return the el_mc
	 */
	public int getEl_mc() {
		return el_mc;
	}
	/**
	 * @param el_mc the el_mc to set
	 */
	public void setEl_mc(int el_mc) {
		this.el_mc = el_mc;
	}
	/**
	 * @return the cl_mc
	 */
	public int getCl_mc() {
		return cl_mc;
	}
	/**
	 * @param cl_mc the cl_mc to set
	 */
	public void setCl_mc(int cl_mc) {
		this.cl_mc = cl_mc;
	}
	/**
	 * @return the bookletNum
	 */
	public String getBookletNum() {
		return bookletNum;
	}
	/**
	 * @param bookletNum the bookletNum to set
	 */
	public void setBookletNum(String bookletNum) {
		this.bookletNum = bookletNum;
	}
}
