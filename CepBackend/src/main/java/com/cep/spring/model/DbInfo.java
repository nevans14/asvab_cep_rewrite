package com.cep.spring.model;

public class DbInfo {
	private Integer dbInfoId;
	private String name, value, value2;
	
	public DbInfo() {}
	
	public DbInfo(String name) {
		this.name = name;
	}
	
	public Integer getDbInfoId() { return dbInfoId; }
	public String getName() { return name; }
	public String getValue() { return value; }
	public String getValue2() { return value2; }
	
	public void setDbInfoId(Integer dbInfoId) { this.dbInfoId = dbInfoId; }
	public void setName(String name) { this.name = name; }
	public void setValue(String value) { this.value = value; }
	public void setValue2(String value2) { this.value2 = value2; }
}