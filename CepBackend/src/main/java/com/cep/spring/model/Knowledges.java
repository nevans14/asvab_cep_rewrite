package com.cep.spring.model;

import java.io.Serializable;
import java.util.List;

public class Knowledges implements Serializable {

	private static final long serialVersionUID = -7202779810527625251L;
	String knowledgeElementId;
	String knowledgeDescription;

	public String getKnowledgeElementId() {
		return knowledgeElementId;
	}

	public void setKnowledgeElementId(String knowledgeElementId) {
		this.knowledgeElementId = knowledgeElementId;
	}

	public String getKnowledgeDescription() {
		return knowledgeDescription;
	}

	public void setKnowledgeDescription(String knowledgeDescription) {
		this.knowledgeDescription = knowledgeDescription;
	}

}
