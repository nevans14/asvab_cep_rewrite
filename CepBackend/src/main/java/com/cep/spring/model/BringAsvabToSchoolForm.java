package com.cep.spring.model;

import java.io.Serializable;

public class BringAsvabToSchoolForm implements Serializable {

	private static final long serialVersionUID = -1467977326576970352L;
	String title, position, firstName, lastName, phone,  heardUs, heardUsOther, email, schoolName, schoolAddress, schoolCity, schoolCounty,
			schoolState, schoolZip, recaptcha;

	public String getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(String recaptcha) {
		this.recaptcha = recaptcha;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getHeardUs() {
		return heardUs;
	}
	
	public void setHeardUs(String heardUs) {
		this.heardUs = heardUs;
	}
	
	public String getHeardUsOther() {
		return heardUsOther;
	}
	
	public void setHeardUsOther(String heardUsOther) {
		this.heardUsOther = heardUsOther;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolCity() {
		return schoolCity;
	}

	public void setSchoolCity(String schoolCity) {
		this.schoolCity = schoolCity;
	}

	public String getSchoolCounty() {
		return schoolCounty;
	}

	public void setSchoolCounty(String schoolCounty) {
		this.schoolCounty = schoolCounty;
	}

	public String getSchoolState() {
		return schoolState;
	}

	public void setSchoolState(String schoolState) {
		this.schoolState = schoolState;
	}

	public String getSchoolZip() {
		return schoolZip;
	}

	public void setSchoolZip(String schoolZip) {
		this.schoolZip = schoolZip;
	}
}
