package com.cep.spring;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application.properties")
public class HttpsRedirectFilter implements Filter {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Value("${application.using.https}")
	private boolean isUsingHttps;
	private String uri, protocol, domain;
	@Value("${application.https.port}")
	private int port;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;	
		
		this.uri = req.getRequestURI();
		this.protocol = req.getScheme();
		this.domain = req.getServerName();
		/*
		if (this.protocol.toLowerCase().equals("http") && this.isUsingHttps) {
			logger.debug("user was redirected from http://%s%d:%s to https://%s%d:%s", domain, req.getServerPort(), uri, domain, port, uri);
			response.setContentType("text/html");
			String httpsPath = String.format("https://%s:%d%s", domain, port, uri);
			res.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
			res.setHeader("Location", httpsPath);			
		}*/
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	@Override
	public void destroy() {
		
	}	
}