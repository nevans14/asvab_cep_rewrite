package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

public interface PortfolioMapper {

	List<String> isPortfolioStarted(@Param("userId") Integer userId);

	ArrayList<WorkExperience> getWorkExperience(@Param("userId") Integer userId);

	int insertWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject);

	int deleteWorkExperience(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject);

	ArrayList<Education> getEducation(@Param("userId") Integer userId);

	int insertEducation(@Param("educationObject") Education educationObject);

	int deleteEducation(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateEducation(@Param("educationObject") Education educationObject);

	ArrayList<Achievement> getAchievements(@Param("userId") Integer userId);

	int insertAchievement(@Param("achievementObject") Achievement achievementObject);

	int deleteAchievement(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAchievement(@Param("achievementObject") Achievement achievementObject);

	ArrayList<Interest> getInterests(@Param("userId") Integer userId);

	int insertInterest(@Param("interestObject") Interest interestObject);

	int deleteInterest(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateInterest(@Param("interestObject") Interest interestObject);

	ArrayList<Skill> getSkills(@Param("userId") Integer userId);

	int insertSkill(@Param("skillObject") Skill skillObject);

	int deleteSkill(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateSkill(@Param("skillObject") Skill skillObject);

	ArrayList<WorkValue> getWorkValues(@Param("userId") Integer userId);

	int insertWorkValue(@Param("workValueObject") WorkValue workValueObject);

	int deleteWorkValue(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateWorkValue(@Param("workValueObject") WorkValue workValueObject);

	ArrayList<Volunteer> getVolunteers(@Param("userId") Integer userId);

	int insertVolunteer(@Param("volunteerObject") Volunteer volunteerObject);

	int deleteVolunteer(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateVolunteer(@Param("volunteerObject") Volunteer volunteerObject);

	ArrayList<AsvabScore> getAsvabScore(@Param("userId") Integer userId);

	int insertAsvabScore(@Param("asvabObject") AsvabScore asvabObject);

	int deleteAsvabScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAsvabScore(@Param("asvabObject") AsvabScore asvabObject);

	ArrayList<ActScore> getActScore(@Param("userId") Integer userId);

	int insertActScore(@Param("actObject") ActScore actObject);

	int deleteActScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateActScore(@Param("actObject") ActScore actObject);

	ArrayList<SatScore> getSatScore(@Param("userId") Integer userId);

	int insertSatScore(@Param("satObject") SatScore satObject);

	int deleteSatScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateSatScore(@Param("satObject") SatScore satObject);

	ArrayList<FyiScore> getFyiScore(@Param("userId") Integer userId);

	int insertFyiScore(@Param("fyiObject") FyiScore fyiObject);

	int deleteFyiScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateFyiScore(@Param("fyiObject") FyiScore fyiObject);

	ArrayList<OtherScore> getOtherScore(@Param("userId") Integer userId);

	int insertOtherScore(@Param("otherObject") OtherScore otherObject);

	int deleteOtherScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateOtherScore(@Param("otherObject") OtherScore otherObject);

	ArrayList<Plan> getPlan(@Param("userId") Integer userId);

	int insertPlan(@Param("planObject") Plan planObject);

	int deletePlan(@Param("userId") Integer userId, @Param("id") Integer id);

	int updatePlan(@Param("planObject") Plan planObject);
	
	ArrayList<UserInfo> selectNameGrade(@Param("userId") Integer userId);

	int updateInsertNameGrade(@Param("userInfo") UserInfo userInfo);
	
	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);
	
	String getAccessCode(@Param("userId") Integer userId);
	
	int insertTeacherDemoTaken(@Param("obj") Object obj);
	
	int getTeacherDemoTaken(@Param("userId") Integer userId);
}
