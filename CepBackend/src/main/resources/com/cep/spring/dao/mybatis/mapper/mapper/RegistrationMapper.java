import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.AccessCodesMerge;

public interface RegistrationMapper {

	int insertMergeAccounts(@Param("registrationObject") AccessCodesMerge registrationObject);
	
	String getAccessCodes(@Param("accessCode") String accessCode);
	
	String getAccessCodes(@Param("accessCode") String accessCode);
}
