package com.cep.spring.dao.mybatis.mapper;

import org.apache.ibatis.annotations.Param;

public interface CEPAnalyticsMapper {
	int insertCEPShare(@Param("email") String email, @Param("category") String category);
}
