package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;

public interface OccufindMapper {
	ArrayList<OccufindSearch> getOccufindSearch(@Param("searchArgument") OccufindSearchArguments userSearchArguments);

	CertificateExplanation getCertificateExplanation(@Param("onetSoc") String onetSoc);
	
	TitleDescription getOccupationTitleDescription(@Param("onetSoc") String onetSoc);

	ArrayList<Occupation> getOccupationASK(@Param("onetSoc") String onetSoc);

	ArrayList<MilitaryCareerResource> getMilitaryCareerResources(@Param("onetSoc") String onetSoc);

	ArrayList<OOHResource> getOOHResources(@Param("onetSoc") String onetSoc);

	ArrayList<StateSalary> getAvgSalaryByState(@Param("onetSocTrimmed") String onetSocTrimmed);

	Integer getNationalSalary(@Param("onetSocTrimmed") String onetSocTrimmed);

	String getBLSTitle(@Param("onetSocTrimmed") String onetSocTrimmed);

	ArrayList<OccupationInterestCode> getOccupationInterestCodes(@Param("onetSoc") String onetSoc);

	HotGreenStem getHotGreenStemFlags(@Param("onetSoc") String onetSoc);

	ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(@Param("onetSoc") String onetSoc);

	ArrayList<RelatedCareerCluster> getRelatedCareerCluster(@Param("onetSoc") String onetSoc);

	ArrayList<SchoolMoreDetails> getSchoolMoreDetails(@Param("onetSoc") String onetSoc);

	Integer getStateSalaryYear();

	ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(@Param("trimmedOnetSoc") String trimmedOnetSoc);

	ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(@Param("onetSoc") String onetSoc);

	ArrayList<MilitaryHotJobs> getMilitaryHotJobs(@Param("onetSoc") String onetSoc);

	ArrayList<OccufindMoreResources> getOccufindMoreResources(@Param("onetSoc") String onetSoc);

	ArrayList<OccufindSearch> getDashboardBrightOutlook(@Param("interestCodeOne") String interestCdOne,
			@Param("interestCodeTwo") String interestCdTwo, @Param("interestCodeThree") String interestCdThree);

	ArrayList<OccufindSearch> getDashboardStemCareers(@Param("interestCodeOne") String interestCdOne,
			@Param("interestCodeTwo") String interestCdTwo, @Param("interestCodeThree") String interestCdThree);

	ArrayList<OccufindSearch> getDashboardGreenCareers(@Param("interestCodeOne") String interestCdOne,
			@Param("interestCodeTwo") String interestCdTwo, @Param("interestCodeThree") String interestCdThree);

	ArrayList<OccufindSearch> viewAllOccupations();
	
	ArrayList<OccufindSearch> viewMilitaryOccupations();
	
	ArrayList<OccufindSearch> viewHotOccupations();

	ArrayList<OccufindAlternative> onetHideDetails(@Param("onetSoc") String onetSoc);

	String getImageAltText(@Param("onetSoc") String onetSoc);
	
	JobZone getJobZone(@Param("onetSoc") String onetSoc);

	List<AlternateTitles> getAlternateTitles(@Param("onetSoc") String onetSoc);

}
