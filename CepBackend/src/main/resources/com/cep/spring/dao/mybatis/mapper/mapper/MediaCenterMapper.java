package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.media.MediaCenter;

public interface MediaCenterMapper {

	ArrayList<MediaCenter> getMediaCenterList();

	MediaCenter getArticleById(@Param("mediaId") int mediaId);
}
