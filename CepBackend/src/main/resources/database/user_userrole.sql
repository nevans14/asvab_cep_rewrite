
use [asavb2015];


/* */
CREATE TABLE [dbo].[users](
	[user_id] [int]  NOT NULL,
	[username] varchar(45) NOT NULL,
	[password] varchar(60) NOT NULL,
	[enabled] TINYINT NOT NULL DEFAULT 1,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ( [username] )
 ) ;


CREATE TABLE [dbo].[user_roles] (
  [user_role_id] int NOT NULL IDENTITY(1,1),
  [username] varchar(45) NOT NULL,
  [role] varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  CONSTRAINT uni_username_role UNIQUE([username],[role]),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username));


  ALTER TABLE dbo.[users] ADD [reset_key] [varchar](36) NULL, [reset_expire_date] [datetime] NULL ;  
 








INSERT INTO [dbo].[users](user_id, username,password,enabled)
VALUES (1, 'dave','$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 1);

INSERT INTO [dbo].[user_roles] (username, role)
VALUES ('dave', 'ROLE_USER');
INSERT INTO [dbo].[user_roles] (username, role)
VALUES ('dave', 'ROLE_ADMIN');

/**/