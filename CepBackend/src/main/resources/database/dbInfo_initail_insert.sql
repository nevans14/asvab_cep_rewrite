
INSERT INTO dbInfo ([name],[value]) VALUES ('databaseVersion','2.00.001');
INSERT INTO dbInfo ([name],[value]) VALUES ('softwareVersion','1.00.004');
INSERT INTO dbInfo ([name],[value]) VALUES ('backup_file_for_cep_database','C:\Software\');
INSERT INTO dbInfo ([name],[value]) VALUES ('password_reset_url','C:\Software\');
INSERT INTO dbInfo ([name],[value]) VALUES ('password_reset_email_message','The Career Exploration Program website been requested to reset the password for your email, #userEmail# .\n Click on the following link to complete the reset process. \n #resetUrl# ');
INSERT INTO dbInfo ([name],[value]) VALUES ('registration_email', 'Welcome to the Career Exploration Program website. #NEW_LINE# #NEW_LINE# The online resourse to help you find a rewarding and fufilling career. #NEW_LINE# #NEW_LINE#Thank you for registering,#NEW_LINE# #NEW_LINE# Career Exploration Program #NEW_LINE# #WEBSITE_URL#');
INSERT INTO dbInfo ([name],[value]) VALUES ('website_url', 'http://cep-compta.humrro.org:8080/CEP');
