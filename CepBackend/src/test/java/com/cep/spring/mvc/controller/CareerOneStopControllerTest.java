package com.cep.spring.mvc.controller;

import org.junit.Assert;
import org.junit.Test;

public class CareerOneStopControllerTest {
    
    CareerOneStopController cosController = new CareerOneStopController(); 

    // @RequestMapping(value = "/cert/{onet_soc}", method = RequestMethod.GET)
    @Test
    public void test_certificate() {
        // public @ResponseBody CareerOneStopPayload  
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.certificate("39-5012.00");
        System.out.println( cosPayload.getPayload()  );//17-3021.00
        
        Assert.assertNotEquals("test_certificate", 0 , cosPayload.getPayload().length() );
        
    }

    // @RequestMapping(value = "/cert/{onet_soc}", method = RequestMethod.GET)
    @Test
    public void test_certificate_with_socId() {
        // public @ResponseBody CareerOneStopPayload  
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.certificate(":socId");
        System.out.println( cosPayload.getPayload()  );
        
        Assert.assertNotEquals("test_certificate", 0 , cosPayload.getPayload().length() );
        
    }

    //@RequestMapping(value = "/certDetail/{cert_id}", method = RequestMethod.GET)
    @Test
    public void test_certificateDetail() {
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.certificateDetail("333");
        
        System.out.println( cosPayload.getPayload()  );
        
        Assert.assertNotEquals("test_certificate", 0 , cosPayload.getPayload().length() );
    }
    
    // @RequestMapping(value = "/license/{onet_soc}/{state}", method = RequestMethod.GET)
    @Test
    public void test_license() {
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.license("39-5012.00","CA");
        
        System.out.println( cosPayload.getPayload()  );
        
        Assert.assertNotEquals("test_certificate", 0 , cosPayload.getPayload().length() );
    
    }
    
//     @RequestMapping(value = "/licenseDetail/{onet_soc}/{license_id}/{state}", method = RequestMethod.GET)
    @Test
    public void test_licenseDetails() {
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.licenseDetail("47204100", "ME");
        
        System.out.println( cosPayload.getPayload()  );
        
        Assert.assertNotEquals("test_certificate", 0 , cosPayload.getPayload().length() );

    }
    
    @Test
    public void test_apprenticeships() {
        CareerOneStopController.CareerOneStopPayload cosPayload = cosController.trainingByKnowledgeArea("39-5012.00","CA");
        
        System.out.println( cosPayload.getPayload());
        
        Assert.assertNotEquals("test_apprenticeships", 0 , cosPayload.getPayload().length() );

    }
 /* TODO: Should do more unit testing ...   
    
        

        
    @RequestMapping(value = "/training/{onet_soc}/{zip_code}", method = RequestMethod.GET)
    public @ResponseBody CareerOneStopPayload  
                training(@PathVariable("onet_soc") String onet_soc,
                         @PathVariable("zip_code") String zip_code) {
    

    @RequestMapping(value = "/training/{onet_soc}/{onet_skill_id}/{zip_code}", method = RequestMethod.GET)
    public @ResponseBody CareerOneStopPayload  
                trainingByKnowledgeArea(@PathVariable("onet_soc") String onet_soc,
                        @PathVariable("onet_skill_id") String onet_skill_id,
                         @PathVariable("zip_code") String zip_code) {
        logger.info("trainingByKnowledgeArea");

    @RequestMapping(value = "/apprenticeship/{onet_soc}/{state}", method = RequestMethod.GET)
    public @ResponseBody CareerOneStopPayload  
                trainingByKnowledgeArea(@PathVariable("onet_soc") String onet_soc,
                         @PathVariable("state") String state) {

        

    
    private String callCareerOneStop(String url) {
    
    
    
*/
    
}
