package com.cep.spring.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.model.testscore.FilterScoreObj;
import com.cep.spring.model.testscore.StudentTestingProgam;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class FilterScoreTest {
	
	@Autowired
	TestScoreController testScoreController;
		
	@Test
	public void filterTest(){
		
		FilterScoreObj filterScoreObj = new FilterScoreObj();
		filterScoreObj.setFileName("CEP_File");
		filterScoreObj.setAdministeredYear("2015");
		filterScoreObj.setAdministeredMonth("11");
//		filterScoreObj.setEducationLevel("11");
		filterScoreObj.setGender("M");
		filterScoreObj.setLastName("Brennan");
//		filterScoreObj.setSchoolCode(null);
//		filterScoreObj.setSchoolAddress(null);
//		filterScoreObj.setSchoolCity(null);
//		filterScoreObj.setSchoolState(null);
//		filterScoreObj.setSchoolZipCode(null);
//		filterScoreObj.setStudentCity("HOWELL");
//		filterScoreObj.setStudentState(null);
//		filterScoreObj.setStudentZipCode(null);
		
//		List<StudentTestingProgam> stps = testScoreController.filterTestScores(filterScoreObj);
//		System.out.println("Count: "+stps.size());
//		for(StudentTestingProgam stp: stps)
//		System.out.println(stp.getStudentTestProgram().getControlInformation().getLastName());
		MockHttpServletResponse response = new MockHttpServletResponse();
		testScoreController.filterTestScores(filterScoreObj, response);
		
	}

}
