package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.cep.spring.model.fyi.ScoreChoice;

public class TestCompare {

	public static void main(String[] args) {

		List<Date> dates = new ArrayList<>();
		dates.add(new Date(2018-1900, 01, 01));
		dates.add(new Date(2015-1900, 01, 01));
		dates.add(new Date(2016-1900, 01, 01));
		
		Collections.sort(dates, Collections.reverseOrder(new Comparator<Date>() {
			  public int compare(Date o1, Date o2) {
			      if (o1 == null || o2 == null)
			        return 0;
			      return o1.compareTo(o2);
			  }
			}));

		System.out.println(dates.get(0));
	}

}
