package com.cep.spring.mvc.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class MergeAccountsTest {

	@Autowired
	FYIRestController fyi;
	
	@Autowired
	PortfolioController portfolio;
	
	@Autowired
	FavoriteOccupationController favorate;
	
	@Autowired
	LoginRegistrationController login;
	
	@Test
	public void merge() {
			
//		int oldUserId = 2211762;
//		int newUserId = 3177271;
//		int oldUserId = 2070790;
//		int newUserId = 3177241;
//		int oldUserId = 2211291;
//		int oldUserId = 2069889;
		String newAccessCode = "Test196QV";
//		String oldAccessCode = "Test811BY ";
		String emailAddress = "Test@test.com";
		
//		fyi.mergeAccount(oldUserId, newUserId);
//		portfolio.mergePortfolio(oldUserId, newUserId);
//		favorate.mergeFavorates(oldUserId, newUserId);
		login.MergeAccounts(emailAddress, newAccessCode, "true");
	}
	
}
