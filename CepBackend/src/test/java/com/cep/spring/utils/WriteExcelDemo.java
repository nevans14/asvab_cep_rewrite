package com.cep.spring.utils;

//import statements
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class WriteExcelDemo 
{
  public static void main(String[] args) 
  {
	  ArrayList<String> ls = new ArrayList<>();
	  ls.add("NAME");
	  ls.add("Amit");
	  ls.add("Lokesh");
	  ls.add("John");
	  ls.add("Brian");
	  ls.add("Amit");
	  writeExcelDocFromList("howtodoinjava_demo.xlsx", ls);
 }
  
  public static void writeExcelDocFromList(String fileName, ArrayList<String> excelData) {
      //Blank workbook
      XSSFWorkbook workbook = new XSSFWorkbook(); 
       
      //Create a blank sheet
      XSSFSheet sheet = workbook.createSheet("Employee Data");
        
      //This data needs to be written (Object[])
      Map<String, Object[]> data = new TreeMap<String, Object[]>();
//      data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
//      data.put("2", new Object[] {1, "Amit", "Shukla"});
//      data.put("3", new Object[] {2, "Lokesh", "Gupta"});
//      data.put("4", new Object[] {3, "John", "Adwards"});
//      data.put("5", new Object[] {4, "Brian", "Schultz"});
      Integer rowNumber = 1;
	  data.put(rowNumber.toString(),new Object[] {"ACCESS_CODE"});
      rowNumber++;
      for ( String str : excelData) {
    	  data.put(rowNumber.toString(), new Object[] { str });
    	  rowNumber++;
      }
        
      //Iterate over data and write to sheet
//      Set<String> keyset = data.keySet();
  
      int rownum = 0;
//      for (String key : keyset)
    	  for ( Integer j = 1 ; j <= data.size(); j++)
      {
          Row row = sheet.createRow(rownum++);
          Object [] objArr = data.get(j.toString());
          int cellnum = 0;
          for (Object obj : objArr)
          {
             Cell cell = row.createCell(cellnum++);
             if(obj instanceof String)
                  cell.setCellValue((String)obj);
              else if(obj instanceof Integer)
                  cell.setCellValue((Integer)obj);
          }
      }
      try
      {
          //Write the workbook in file system
          FileOutputStream out = new FileOutputStream(new File(fileName));
          workbook.write(out);
          out.close();
          System.out.println(fileName + " written successfully on disk.");
      } 
      catch (Exception e) 
      {
          e.printStackTrace();
      }
 
  }
  
  
}