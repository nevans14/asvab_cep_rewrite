package com.cep.spring.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.mvc.controller.OccufindRestController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class SchoolProfileTest {

	@Autowired
	OccufindRestController controller;
	
	@Test
	public void getProfile() {
		SchoolProfile profile = controller.getSchoolProfile(218539);
		System.out.println(profile.getCollegeMajors());
		List<String> profiles = controller.getCareerMajors("13-2011.00");
		System.out.println(profiles);

	}

}
