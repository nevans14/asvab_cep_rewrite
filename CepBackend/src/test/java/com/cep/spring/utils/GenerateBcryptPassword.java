package com.cep.spring.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;




public class GenerateBcryptPassword {

	public static void main(String[] args) {
	    String clearTextPassword = null;
//	    if ( null != args[0] ) {
//	        System.out.println(args[0]);
//	        clearTextPassword = args[0];
//	    } else {
//            clearTextPassword = "#3Dprinter";
//	    }

	    clearTextPassword = "Avishek";
	    
	    ApplicationContext ctx = new ClassPathXmlApplicationContext("/controllerContext.xml");
	    BCryptPasswordEncoder bCryptPasswordEncoder = ctx.getBean(BCryptPasswordEncoder.class);

	    String encodedTextPassword= bCryptPasswordEncoder.encode(clearTextPassword);
        System.out.println(clearTextPassword);	    
	    System.out.println(encodedTextPassword);
	}	
}
