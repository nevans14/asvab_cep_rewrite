package com.cep.spring.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;




public class GeneratAccessCodeUploadFiles {

	public static void main(String[] args) {
		String strPath = "C:/Software/CEP/2017_Accesscodes/";
		
		String ShannonFile = "Shannon.AccessCodes.csv"; 
		String ThreeWeekFile = "ThreeWeek.AccessCodes"; 
		String CounselorFile = "Counselor.AccessCodes"; 
		String MarketingFile = "Marketing.AccessCodes"; 
		String ReservedFile = "Reserved.AccessCodes"; 
		ArrayList<String> lsAccess = new ArrayList<>();
		ArrayList<String> lsShannonAcces = new ArrayList<>();
//		Integer fileSize = 12;
//		Integer numberOfFiles = 2;
//		Integer shannonSize = 5;
		
		Integer fileSize = 2000;
		Integer numberOfFiles = 65;
		Integer shannonSize = 100;		
		
		Integer listSize = fileSize * numberOfFiles + shannonSize;

		System.out.println("**********  Red One ***************************");
		System.out.println("Requested Size ="+listSize);
		
		
		System.out.println("**********  ThreeWeekCodes ***************************");		
		Set<String> lsThreeWeekCodes = GenerateAccessCodes.generateThreeWeekCodes(listSize);
		System.out.println(lsThreeWeekCodes.size());
		// Append to Shannon File
		SimpleFileUtils.appendStringToFile(strPath + ShannonFile, "**** ThreeWeekCodes ****\n");
		lsShannonAcces.add("**** ThreeWeekCodes ****");		
		// Write out segments to files
		Integer counter = 0;
		Integer fileCounter = 1;
		for(String accessCode : lsThreeWeekCodes ){
			if ( counter < shannonSize) {
				SimpleFileUtils.appendStringToFile(strPath + ShannonFile,accessCode + "\n");
				lsShannonAcces.add(accessCode);
			}
			if( counter >= shannonSize &&
				(counter % fileSize) != 0  
					){
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + ThreeWeekFile + fileCounter + ".csv" ,accessCode + "\n");
				lsAccess.add(accessCode);

			} else if ( counter >= shannonSize &&
					(counter % fileSize) == 0 ){
				// Write Stuff to new file 
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + ThreeWeekFile + fileCounter + ".csv" ,accessCode + "\n");					
				lsAccess.add(accessCode);
				WriteExcelDemo.writeExcelDocFromList(strPath + ThreeWeekFile + fileCounter.toString() + ".xlsx", lsAccess);
				lsAccess.clear();
				fileCounter++;
			}
				
			counter++;
		}
		WriteExcelDemo.writeExcelDocFromList(strPath + ThreeWeekFile + fileCounter + ".xlsx", lsAccess);
		lsAccess.clear();
		lsAccess = null;
		lsAccess = new ArrayList<>();
		
		System.out.println("**********  CounselorCodes ***************************");	
		Set<String> lsCounselorCodes = GenerateAccessCodes.generateCounselorCodes(listSize);
		System.out.println(lsCounselorCodes.size());
		// Append to Shannon File
		SimpleFileUtils.appendStringToFile(strPath + ShannonFile, "**** CounselorCodes ****" + "\n");
		lsShannonAcces.add("**** CounselorCodes ****");
		// Write out segments to files
		counter = 0;
		fileCounter = 1;
		for(String accessCode : lsCounselorCodes ){
			if ( counter < shannonSize) {
				SimpleFileUtils.appendStringToFile(strPath + ShannonFile,accessCode + "\n");	
				lsShannonAcces.add(accessCode);
			}
			if( counter >= shannonSize &&
				(counter % fileSize) != 0  
					){
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + CounselorFile + fileCounter + ".csv" ,accessCode + "\n");
				lsAccess.add(accessCode);
			} else if ( counter >= shannonSize &&
					(counter % fileSize) == 0 ){
				// Write Stuff to new file 
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + CounselorFile + fileCounter + ".csv" ,accessCode + "\n");
				lsAccess.add( accessCode);
				WriteExcelDemo.writeExcelDocFromList(strPath + CounselorFile + fileCounter + ".xlsx", lsAccess);
				lsAccess.clear();
				fileCounter++;

			}
				
			counter++;
		}
		WriteExcelDemo.writeExcelDocFromList(strPath + CounselorFile + fileCounter + ".xlsx", lsAccess);
		lsAccess.clear();
		lsAccess = null;
		lsAccess = new ArrayList<>();


		System.out.println("**********  MarketingCodes ***************************");	
		Set<String> lsMarketingCodes = GenerateAccessCodes.generateMarketingCodes(listSize);
		System.out.println(lsMarketingCodes.size());
		// Append to Shannon File
		SimpleFileUtils.appendStringToFile(strPath + ShannonFile, "**** MarketingCodes ****" + "\n");
		lsShannonAcces.add("**** MarketingCodes ****");
		// Write out segments to files
		counter = 0;
		fileCounter = 1;
		for(String accessCode : lsMarketingCodes ){
			if ( counter < shannonSize) {
				SimpleFileUtils.appendStringToFile(strPath + ShannonFile,accessCode + "\n");
				lsShannonAcces.add(accessCode);
			}
			if( counter >= shannonSize &&
				(counter % fileSize) != 0  
					){
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + MarketingFile + fileCounter + ".csv" ,accessCode + "\n");		
				lsAccess.add(accessCode);

			} else if ( counter >= shannonSize &&
					(counter % fileSize) == 0 ){
				// Write Stuff to new file 
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + MarketingFile + fileCounter + ".csv" ,accessCode + "\n");
				lsAccess.add(accessCode);
				WriteExcelDemo.writeExcelDocFromList(strPath + MarketingFile + fileCounter + ".xlsx", lsAccess);
				lsAccess.clear();
				fileCounter++;
			}
				
			counter++;
		}
		WriteExcelDemo.writeExcelDocFromList(strPath + MarketingFile + fileCounter + ".xlsx", lsAccess);
		lsAccess.clear();
		lsAccess = null;
		lsAccess = new ArrayList<>();


		System.out.println("**********  ReservedCodes ***************************");	
		Set<String> lsReservedCodes = GenerateAccessCodes.generateReservedCodes(listSize);
		System.out.println(lsReservedCodes.size());

		// Append to Shannon File
		SimpleFileUtils.appendStringToFile(strPath + ShannonFile, "**** ReservedCodes ****" + "\n");
		lsShannonAcces.add("**** ReservedCodes ****");
		// Write out segments to files
		counter = 0;
		fileCounter = 1;
		for(String accessCode : lsReservedCodes ){
			if ( counter < shannonSize) {
				SimpleFileUtils.appendStringToFile(strPath + ShannonFile,accessCode + "\n");
				lsShannonAcces.add(accessCode);
			}
			if( counter >= shannonSize &&
				(counter % fileSize) != 0  
					){
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + ReservedFile + fileCounter + ".csv" ,accessCode + "\n");		
				lsAccess.add(accessCode);

			} else if ( counter >= shannonSize &&
					(counter % fileSize) == 0 ){
				// Write Stuff to new file 
				// Write Stuff to file
				SimpleFileUtils.appendStringToFile(strPath + ReservedFile + fileCounter + ".csv" ,accessCode + "\n");
				lsAccess.add(accessCode);
				WriteExcelDemo.writeExcelDocFromList(strPath + ReservedFile + fileCounter + ".xlsx", lsAccess);
				lsAccess.clear();
				fileCounter++;

			}
				
			counter++;
		}
		WriteExcelDemo.writeExcelDocFromList(strPath + ReservedFile + fileCounter + ".xlsx", lsAccess);
		lsAccess.clear();
		lsAccess = null;
		lsAccess = new ArrayList<>();

		WriteExcelDemo.writeExcelDocFromList(strPath + ShannonFile + ".xlsx", lsShannonAcces);
		
	}	
}
