package com.cep.spring.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;
import com.cep.spring.mvc.controller.TestScoreController;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class JsonConversionDemo {

	@Autowired
	TestScoreController controller;

	@Test
	public void testJson() {

//		Stream<Path> paths = null;
//		try {
//			paths = Files.list(Paths.get("C:\\Kheald-CEP\\resource\\"));
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//		Iterator<Path> it = paths.iterator();
//		List<StudentTestingProgam> stps = new ArrayList<>();
//		
//		while (it.hasNext()) {
//			Path obj = (Path) it.next();
//			ObjectMapper mapper = new ObjectMapper().setVisibility(FIELD, ANY);
//			Iterator<StudentTestingProgam> thingsIterator = null;
//			try {
//				thingsIterator = mapper.reader(StudentTestingProgam.class)
//						.readValues(new File(obj.toString()));
//			} catch (JsonProcessingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			while (thingsIterator.hasNext()) {
//				stps.add(thingsIterator.next());
////				controller.insertTestScore(thingsIterator.next());
//			}
//
//			controller.insertTestScore(stps);
//			System.out.println("End of File");
//			stps.clear();			
//		}

//		// select record
		 ResponseEntity<StudentTestingProgamSelects> rstp =
		 controller.getTestScoreWithUserId(2208284);
		 System.out.println(rstp.getBody());
		
//		ManualTestScore manualTestScoreObj = new ManualTestScore();
//		manualTestScoreObj.setAfqt("23");
//		manualTestScoreObj.setMath("12");
//		manualTestScoreObj.setScienceTechnology("34");
//		manualTestScoreObj.setVerbal("45");
//		manualTestScoreObj.setUserId(123);
//		
//		System.out.println(controller.insertManualTestScore(manualTestScoreObj));
		
//		ManualTestScore manualTestScoreObj = new ManualTestScore();
//		manualTestScoreObj.setAfqt("23");
//		manualTestScoreObj.setMath("12");
//		manualTestScoreObj.setScienceTechnology("44");
//		manualTestScoreObj.setVerbal("45");
//		manualTestScoreObj.setUserId(123);
//		
//		System.out.println(controller.updateManualTestScore(manualTestScoreObj));
		
//		System.out.println(controller.getManualTestScore(123));
		
//		System.out.println(controller.deleteManualTestScore(123));;
	}
}
