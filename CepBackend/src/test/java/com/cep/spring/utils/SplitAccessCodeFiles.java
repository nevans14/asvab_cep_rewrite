package com.cep.spring.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;




public class SplitAccessCodeFiles {

	public static void main(String[] args) {
		String strPath = "C:/Software/CEP/2017_Accesscodes-revised/";
		
		String ShannonFile = "Shannon.AccessCodes.csv"; 
		String ThreeWeekFile = "ThreeWeek.AccessCodes"; 
		String CounselorFile = "Counselor.AccessCodes"; 
		String MarketingFile = "Marketing.AccessCodes"; 
		String ReservedFile = "Reserved.AccessCodes"; 
		
		
		
		
		// Open Counselor File 
		String fin = strPath + ReservedFile ;
		String fout = strPath + "stations/" + ReservedFile + ".station.";
		FileInputStream fis;
		Integer fileCounter = 0;
		try {
			
			for ( Integer readFileCount = 1 ; readFileCount < 11 ; readFileCount ++ ) { 
					fis = new FileInputStream(fin + readFileCount + ".csv");
					
				 
				//Construct BufferedReader from InputStreamReader
				BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			 
				String line = null;

					Integer lineCounter = 0;
					ArrayList<String> ls = new ArrayList<String>();
					while ((line = br.readLine()) != null) {
						System.out.println(line);
						ls.add(line);
						lineCounter++;
						
						if ( lineCounter == 294){
							lineCounter = 0;
							fileCounter++;
							WriteExcelDemo.writeExcelDocFromList(fout + fileCounter + ".xlsx", ls);
							ls.clear();
						}
		
					}
			
					fileCounter++;
					WriteExcelDemo.writeExcelDocFromList(fout + fileCounter + ".xlsx", ls);	
					
					
			 System.out.println("zzzzzzzzzzzzzzzzz " + fileCounter);
				br.close();
				
		
		}
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
