package com.cep.spring.utils;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;




public class GenerateAccessCodesTest {

	@Test
	public void testGenerateThreeWeekCodes() {
		System.out.println("*** Three Week Codes ***");
		Set<String> ls = GenerateAccessCodes.generateThreeWeekCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
	@Test
	public void testCounselorCodes() {
		System.out.println("*** Counselor Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateCounselorCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}		
	}

	@Test
	public void testMarketingCodes() {
		System.out.println("*** Marketing Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateMarketingCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
	@Test
	public void testReservedCodes() {
		System.out.println("*** Reserved Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateReservedCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}

	
	   
    @Test
    public void testUATCodes() {
        System.out.println("*** UAT !!! Codes ***");       
        Integer setSize = 51 ;
        
        Set<String> set = new HashSet<String>();
        // Determine the suffix 
        String prefix = "UAT";
        
        while ( set.size() < setSize ){
                set.add( prefix + GenerateAccessCodes.getNextRandomNumber(1, 10000, "%05d") );
        }

        for ( String s : set) {
            System.out.println(s);
        }
        
        
        
        for ( String s : set) {
            System.out.println(
              " INSERT INTO access_codes ( " + " access_code , expire_date , role " + " ) VALUES ( " + "'"
              + s + "','"
              + SimpleUtils.getStringInStandardFormatFromDate( SimpleUtils.datePlusMonths( new Date() , 2 )  ) + "','"
              + "M" + "')"
                    );
        }


    }
    
    /** Given todays date determine what the Modulus year would be.
     * 
     * Modulus 0 SchoolYear2016 = "F,G,H,J,K,L"
     * Modulus 1 SchoolYear2017 = "M,N,P,Q,R,S"
     * Modulus 2 SchoolYear2018 = "T,U,V,W,X,Y"
     * 
     */
    @Test
    public void getSchoolYearSuffixesTest() {
        
        List<String> l2016 = GenerateAccessCodes.getSchoolYearSuffixes( 2016 );
        Assert.assertEquals("",true,l2016.contains("f"));
        Assert.assertEquals("",true,l2016.contains("F"));
        Assert.assertEquals("",true,l2016.contains("g"));
        Assert.assertEquals("",true,l2016.contains("G"));
        Assert.assertEquals("",true,l2016.contains("h"));
        Assert.assertEquals("",true,l2016.contains("H"));
        Assert.assertEquals("",true,l2016.contains("j"));
        Assert.assertEquals("",true,l2016.contains("J"));
        Assert.assertEquals("",true,l2016.contains("k"));
        Assert.assertEquals("",true,l2016.contains("K"));
        Assert.assertEquals("",true,l2016.contains("l"));
        Assert.assertEquals("",true,l2016.contains("L"));
        List<String> l2017 = GenerateAccessCodes.getSchoolYearSuffixes( 2017 ); 
        Assert.assertEquals("",true,l2017.contains("m"));
        Assert.assertEquals("",true,l2017.contains("M"));
        Assert.assertEquals("",true,l2017.contains("n"));
        Assert.assertEquals("",true,l2017.contains("N"));
        Assert.assertEquals("",true,l2017.contains("p"));
        Assert.assertEquals("",true,l2017.contains("P"));
        Assert.assertEquals("",true,l2017.contains("q"));
        Assert.assertEquals("",true,l2017.contains("Q"));
        Assert.assertEquals("",true,l2017.contains("r"));
        Assert.assertEquals("",true,l2017.contains("R"));
        Assert.assertEquals("",true,l2017.contains("s"));
        Assert.assertEquals("",true,l2017.contains("S"));
        List<String> l2018 = GenerateAccessCodes.getSchoolYearSuffixes( 2018 );  
        Assert.assertEquals("",true,l2018.contains("t"));
        Assert.assertEquals("",true,l2018.contains("T"));
        Assert.assertEquals("",true,l2018.contains("u"));
        Assert.assertEquals("",true,l2018.contains("U"));
        Assert.assertEquals("",true,l2018.contains("v"));
        Assert.assertEquals("",true,l2018.contains("V"));
        Assert.assertEquals("",true,l2018.contains("w"));
        Assert.assertEquals("",true,l2018.contains("W"));
        Assert.assertEquals("",true,l2018.contains("x"));
        Assert.assertEquals("",true,l2018.contains("X"));
        Assert.assertEquals("",true,l2018.contains("y"));
        Assert.assertEquals("",true,l2018.contains("Y"));
    }
        
    
    @Test
    public void isSuffixInCurrentYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("",true, GenerateAccessCodes.isSuffixInCurrentYear("m"));
    }
    @Test
    public void schoolYearOfSuffixTest(){
        // The current school year is 2017 
        // This test breaks every three year.        
        Assert.assertEquals("", new Integer(2017) ,GenerateAccessCodes.schoolYearOfSuffix("M"));
    }

    /**
     * Returns what the school year.
     * 
     */
    @Test
    public void  determineSchoolYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("", new Integer(2017),GenerateAccessCodes.determineSchoolYear());
    }

	
	
	
}
