$(document).ready(function() {
  
  $('body').on('click', '.recaptcha-no-top', function(){
      $('html').addClass('no-top');
  });

  redirectToHttps();
});

$(window).on("resize", function () {
  $(window).trigger('scroll');
});

/**
* Prevents CloudFlare's redirect to https
* @returns
*/
function redirectToHttps() {
if (location.href.indexOf('localhost') > -1) return;
if (location.href.indexOf('asvabprogram.com') > -1 && location.protocol === 'http:') {
    if (location.href.indexOf('www') === -1) {
        location.href = 'https://www.' + window.location.href.substring(window.location.protocol.length + 2);
    } else {
        location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }
}
}


