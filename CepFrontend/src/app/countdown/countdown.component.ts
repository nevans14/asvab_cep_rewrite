import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { OccufindSalaryDialogComponent } from 'app/core/dialogs/occufind-salary-dialog/occufind-salary-dialog.component';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { ConfigService } from 'app/services/config.service';
import { ContentManagementService } from 'app/services/content-management.service';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit {

  baseUrl: any;
  socId: string = "15-1199.02";
  socIdTrim: string = "15-1199";
  path;
  pageHtml = undefined;

  resource = this._configService.getImageUrl();

  documents = this._configService.getImageUrl() + this._configService.getDocumentFolder();


  constructor(private _configService: ConfigService,
    private _dialog: MatDialog,
    private _router: Router,
    private _contentManagementService: ContentManagementService,
  ) { }

  ngOnInit() {
    // this.baseUrl = this._configService.getBaseUrl();
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  /**
 * Show video modal
 */
  showVideoDialog(videoName) {
    this._dialog.open(PopupPlayerComponent, {
      data: { videoId: videoName },
      autoFocus: false
    });
  }


  showSalaryDialog(salaryId) {
    this._dialog.open(OccufindSalaryDialogComponent, {
      data: { id: salaryId }
    }).afterClosed().subscribe(response => {
      if (response) {
        if (response === 'employmentMoreDetails') {
          this._router.navigate(['/occufind-employment-details/' + this.socId]);
        }
      }
    });
  }
}
