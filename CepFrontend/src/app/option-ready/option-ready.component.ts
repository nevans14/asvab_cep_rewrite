import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { UtilityService } from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';
import { ContentManagementService } from 'app/services/content-management.service';
import { MediaCenterService } from 'app/services/media-center.service';
import { FlickrRestFactoryService } from 'app/services/flickr-rest-factory.service';

@Component({
  selector: 'app-option-ready',
  templateUrl: './option-ready.component.html',
  styleUrls: ['./option-ready.component.scss']
})
export class OptionReadyComponent implements OnInit {

  imageUrl;
  absUrl;
  title;
  description;
  resource;
  mediaUrl;
  path;
  pageHtml;
  sharablesList;
  photos = [];
  documents;
  resources;
  defaultLimit = 15;
	limit = 15;
  xsScreen = false;

  slideConfigResources = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false,
                dots: false
            }
        }, 
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
      ]
  };

  constructor(
    private _dialog: MatDialog,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _meta: Meta,
    private _titleTag: Title,
    private _router: Router,
    private _utility: UtilityService,
    private _config: ConfigService,
    private _contentManagementService: ContentManagementService,
    private _mediaCenterService: MediaCenterService,
    private _flickrService: FlickrRestFactoryService,
  ) {
    if (window.innerWidth < 768) {
      this.xsScreen = true;
    }
  }

  ngOnInit() {
    this.path = this._router.url;

    this.absUrl = this._utility.getCanonicalUrl();
    this.mediaUrl = this._config.getImageUrl();
    this.imageUrl = this.mediaUrl + 'CEP_PDF_Contents/sharables/Landing_Preview_2.jpg';
    this.documents = this.mediaUrl + 'CEP_PDF_Contents/';

    this.pageHtml = this._contentManagementService.getPageByName(this.path);

    this._mediaCenterService.getMediaCenterByCategoryId(9).then(
      data => {
        this.sharablesList = data;
      });

    this._flickrService.getFlickrPublicPhotos().subscribe(photos => this.photos = photos);

    this.updateMeta();

    this.resources = [
      {
        typeId: 1,
        resourceUrl: this.documents + 'ASVABCEP_Student_Flyer.pdf',
        resourceThumbnailUrl: this.documents + 'thumbnails/student_flyer_thumbnail.png',
        altText: 'Student Flyer',
        isPdf: true,
        title: 'Student Flyer'
      }, {
        typeId: 1,
        resourceUrl: this.documents + 'ASVABCEP_Parent_Flyer.pdf',
        resourceThumbnailUrl: this.documents + 'thumbnails/parent_flyer_thumbnail.png',
        altText: 'Parent Flyer',
        isPdf: true,
        title: 'Parent Flyer'
      }, {
        typeId: 1,
        resourceUrl: this.documents + 'ASVABCEP_Sign_Up_Poster.pdf',
        resourceThumbnailUrl: this.documents + 'thumbnails/Poster_thumbnail.jpg',
        altText: 'Sign Up Posters',
        isPdf: true,
        title: 'Sign Up Posters'
      }, {
        typeId: 2,
        resourceUrl: this.documents + 'ASVABCEP_Announcements.pdf',
        resourceThumbnailUrl: this.documents + 'thumbnails/announcement_thumbnail.png',
        altText: 'ASVAB CEP Announcements',
        isPdf: true,
        title: 'Announcements'
      }, {
        typeId: 2,
        resourceUrl: this.documents + 'ASVABCEP_Email_Template.docx',
        resourceThumbnailUrl: this.documents + 'thumbnails/email_template_thumbnail.jpg',
        altText: 'Email Template',
        isPdf: false,
        title: 'Email Template'
      }
    ];
  }

  updateMeta() {
    this._titleTag.setTitle(this.title);
    this._meta.updateTag({name: 'description', content: this.description});

    this._meta.updateTag({property: 'og:title', content: this.title});
    this._meta.updateTag({property: 'og:description', content: this.description});
    this._meta.updateTag({property: 'og:image', content: this.imageUrl});
    this._meta.updateTag({property: 'og:url', content: this.absUrl});

    this._meta.updateTag({ name: 'twitter:site', content: '@asvabcep'});
    this._meta.updateTag({ name: 'twitter:creator', content: '@asvabcep'});
    this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.updateTag({ name: 'twitter:title', content: this.title});
    this._meta.updateTag({ name: 'twitter:image', content: this.imageUrl});
    this._meta.updateTag({ name: 'twitter:description', content: this.description});
  }

  sendToResource = function(url) {
		window.open(url, '_blank');
	};

  viewLess = function () {
		var val = this.limit - 5;
		if (val > this.defaultLimit) {
			this.limit -= 5;
		} else {
			val = this.limit - this.defaultLimit;
			this.limit -= val;
		}
	};

	viewMore = function () {
		this.limit += 5;
	};

  viewAll = function () {
		// if one column has more than the other, then that will be the limit
		this.limit = this.photos.length;
	};

	showImage = function (index) {
		var selectedPhoto = this.photos[index];
		if (selectedPhoto === undefined) return;
		var src = 'https://farm' + selectedPhoto.farm + '.staticflickr.com/' + selectedPhoto.server + '/' + selectedPhoto.id + '_' + selectedPhoto.secret + '.' + selectedPhoto.originalFormat;
		var bodyText =
			'<p class="text-center"><img src="' + src + '" alt=""/></p>';
		var data = {
			title : '',
			message: bodyText
		};
		this._dialog.open(MessageDialogComponent, {
      data: data
    });
	};

  snapChatModal() {
    this._googleAnalyticsService.trackClick('#CONNECT_WITH_US_SNAPCHAT')

    var data = {
        title: 'Connect with us on SnapChat!',
        message: '<p class="text-center"><img style="width: 400px;" src="assets/images/option-ready-snap.png" alt=""/></p>'
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '500px'
    });
  }

  slickInit( e ) {
    console.debug( 'slick initialized' );
  }

  breakpoint( e ) {
      console.debug( 'breakpoint' );
  }

  afterChange( e ) {
      console.debug( 'afterChange' );
  }

  beforeChange( e ) {
      console.debug( 'beforeChange' );
  }
}
