import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionReadyComponent } from './option-ready.component';

describe('OptionReadyComponent', () => {
  let component: OptionReadyComponent;
  let fixture: ComponentFixture<OptionReadyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionReadyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionReadyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
