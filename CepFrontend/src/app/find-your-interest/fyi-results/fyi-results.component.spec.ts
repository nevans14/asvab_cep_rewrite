import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FyiResultsComponent } from './fyi-results.component';

describe('FyiResultsComponent', () => {
  let component: FyiResultsComponent;
  let fixture: ComponentFixture<FyiResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FyiResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FyiResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
