import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Title, Meta } from '@angular/platform-browser';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { LeftNavigationComponent } from 'app/core/page-partials/left-navigation/left-navigation.component';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { checkAndUpdateElementDynamic } from '@angular/core/src/view/element';
@Component({
  selector: 'app-fyi-results',
  templateUrl: './fyi-results.component.html',
  styleUrls: ['./fyi-results.component.scss']
})
export class FyiResultsComponent implements OnInit {
    
    @ViewChild('leftNavi')
    private leftNavi: LeftNavigationComponent;

    accessCode;
    scores;
    topCombinedInterestCodes;
    topGenderInterestCodes;
    numberTestTaken;
    //scoreChoice = scoreChoice.data.scoreChoice;
    scoreChoice;
    scoreSelected;
    
    //baseUrl = resource;
    //domainUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port();
    domainUrl;
    topScores = [];
    
    limit = 2; // by default, the limit is two fyi tests
    numberOfTestsLeft = 2;
    hasReachedLimit = false;
    isAbleToRetakeTest = false;

    combinedScoreSort = [];
    genderScoreSort = [];
    myClassCombined;
    myClassGender;

    combinedShareUrl;
    genderShareUrl;
    shareUrl;
    imageUrl;
    description;
    codeOne;
    codeTwo;
    interestCodeOne;
    interestCodeTwo;

    gender = '';

  constructor(private route: ActivatedRoute,
    private _router: Router,
    private FYIScoreService: FindYourInterestService,
    private _httpHelper: HttpHelperService,
    private dialog: MatDialog,
    private _configService: ConfigService,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _user: UserService,
  ) { }

  ngOnInit() {
      // this.domainUrl = this._configService.getBaseUrl();
      this.domainUrl = this._configService.getBaseFrontendUrl();
      this.accessCode = this.route.snapshot.data.accessCodeDetail;
      this.scores = this.route.snapshot.data.CombinedAndGenderScores[0];
      this.numberTestTaken = this.route.snapshot.data.CombinedAndGenderScores[1];
      this.topCombinedInterestCodes = this.FYIScoreService.getTopCombinedInterestCodes();
      this.topGenderInterestCodes = this.FYIScoreService.getTopGenderInterestCodes();
      window.sessionStorage.setItem('numberTestTaken', this.numberTestTaken);

      this.scoreChoice = window.sessionStorage.getItem('scoreChoice');

      let gender = window.sessionStorage.getItem('gender');
      this.gender = !gender || gender === 'null' ? '' : gender.trim();

      // If score choice is undefined or empty string then default to combined.
      this.scoreSelected = (!this.scoreChoice || !this.gender) ? 'combined' : this.scoreChoice;

          // Determines which section to highlight using CSS.
      this.myClassCombined = this.scoreSelected == 'combined' ? {
          selected : true,
      } : {
          selected : false
      };

      this.myClassGender = this.scoreSelected == 'gender' ? {
          selected : true
      } : {
          selected : false
      };
      
      // sort gender score list in desc order
      var genderScoreSort = [];
      for (var i = 0; i < this.scores.length; i++) {
          genderScoreSort.push(this.scores[i]);
      }
      genderScoreSort.sort(function(a, b) {
          return b.genderScore - a.genderScore
      });

      // When topCombinedInterestCodes is undefined, this means there are no tied
      // scores for combined.
      if (this.topGenderInterestCodes == undefined) {
          this.genderScoreSort = genderScoreSort;
      } else {

          // There are tied combined scores so need to order scores depending on
          // user's selections.
          var tempArray = [];

          // Use the user's tied score selection and move it to the top 3.
          for (var i = 0; i < this.topGenderInterestCodes.length; i++) {
              for (var f = 0; f < genderScoreSort.length; f++) {
                  if (this.topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
                      tempArray.push(genderScoreSort[f]);
                      break;
                  }
              }
          }

          // All other scores will be added to the bottom three.
          for (var f = 0; f < genderScoreSort.length; f++) {
              var isMatch = false;
              for (var i = 0; i < this.topGenderInterestCodes.length; i++) {
                  if (this.topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
                      isMatch = true;
                      break;
                  }
              }

              if (!isMatch) {
                  tempArray.push(genderScoreSort[f]);
              }
          }

          this.genderScoreSort = tempArray;
      }


      // sort combined score list in desc order
      var combinedScoreSort = [];
      for (var i = 0; i < this.scores.length; i++) {
          combinedScoreSort.push(this.scores[i]);
      }
      combinedScoreSort.sort(function(a, b) {
          return b.combinedScore - a.combinedScore
      });

      // When topCombinedInterestCodes is undefined, this means there are no tied
      // scores for combined.
      if (this.topCombinedInterestCodes == undefined) {
          this.combinedScoreSort = combinedScoreSort;
      } else {

          // There are tied combined scores so need to order scores depending on
          // user's selections.
          var tempArray = [];

          // Use the user's tied score selection and move it to the top 3.
          for (var i = 0; i < this.topCombinedInterestCodes.length; i++) {
              for (var f = 0; f < combinedScoreSort.length; f++) {
                  if (this.topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
                      tempArray.push(combinedScoreSort[f]);
                      break;
                  }
              }
          }

          // All other scores will be added to the bottom three.
          for (var f = 0; f < combinedScoreSort.length; f++) {
              var isMatch = false;
              for (var i = 0; i < this.topCombinedInterestCodes.length; i++) {
                  if (this.topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
                      isMatch = true;
                      break;
                  }
              }

              if (!isMatch) {
                  tempArray.push(combinedScoreSort[f]);
              }
          }

          this.combinedScoreSort = tempArray;
      }

    if(this.accessCode && this.accessCode.unlimitedFyi === 1) {
        this.isAbleToRetakeTest = true;
    } else {
        if (this.numberTestTaken < this.limit) {
            this.isAbleToRetakeTest = true;
            this.numberOfTestsLeft = (this.limit - this.numberTestTaken);
        } else {
            this.hasReachedLimit = true;
        }
    }

    this.updateMeta();
    this.updateScoreSelection(this.scoreSelected, true);
  }

  updateMeta() {
    this.interestCodeOne = this.scoreSelected === 'gender' && this.genderScoreSort[0] ? this.genderScoreSort[0].interestCd : this.combinedScoreSort[0].interestCd,
    this.interestCodeTwo = this.scoreSelected === 'gender' && this.genderScoreSort[1] ? this.genderScoreSort[1].interestCd : this.combinedScoreSort[1].interestCd,

    this.combinedShareUrl = this.domainUrl + '/find-your-interest-score-shared/' + this.combinedScoreSort[0].interestCd + '/' + this.combinedScoreSort[1].interestCd;
    this.genderShareUrl = this.domainUrl + '/find-your-interest-score-shared/' + this.genderScoreSort[0].interestCd + '/' + this.genderScoreSort[1].interestCd;
    this.shareUrl = this.scoreSelected === 'gender' ?  this.genderShareUrl : this.combinedShareUrl;

    var topScores = this.interestCodeOne + this.interestCodeTwo;
    this.imageUrl = this._configService.getImageUrl() + this._configService.getMediaCenterFolder() + 'thumbnails/social-share/fyi-score/FYI_facebook_' + topScores + '_IMG.jpg';
    this.description = "My interests are >>" + 
        this.getCodeName(this.interestCodeOne) + 
        " and " + 
        this.getCodeName(this.interestCodeTwo) + 
        " which means I like " + 
        this.getCodeDefinition(this.interestCodeOne) + 
        " and " + 
        this.getCodeDefinition(this.interestCodeTwo) + ".";

    this._titleTag.setTitle('My FYI Results | ASVAB Career Exploration Program');
    this._meta.updateTag({name: 'description', content: this.description});

    this._meta.updateTag({property: 'og:title', content: 'My FYI Results | ASVAB Career Exploration Program'});
    this._meta.updateTag({property: 'og:description', content: this.description});
    this._meta.updateTag({property: 'og:image', content: this.imageUrl});
    this._meta.updateTag({property: 'og:image:alt', content: 'My FYI Results | ASVAB Career Exploration Program'});
    this._meta.updateTag({property: 'og:url', content: this.shareUrl});

    this._meta.updateTag({ name: 'twitter:site', content: '@CareersintheMil'});
    this._meta.updateTag({ name: 'twitter:creator', content: '@CareersintheMil'});
    this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.updateTag({ name: 'twitter:title', content:  'My FYI Results | ASVAB Career Exploration Program'});
    this._meta.updateTag({ name: 'twitter:image', content: this.imageUrl});
    this._meta.updateTag({ name: 'twitter:description', content: this.description});
  }

  isUnlimitedFyi() {
    return this.accessCode 
        ? this.accessCode.unlimitedFyi === 1 ? true : false 
        : false;
  }
  
  // If score selection changes then update FYI scores asynchronously.
  updateScoreSelection(scoreSelected, force = false){
      if(this.scoreSelected == scoreSelected && !force){
          return;
      }
      
      this.scoreSelected = scoreSelected;
      if (this.scoreSelected != undefined) {

          this.updateMeta();
          
          // Setup score object.
          var scoreObject = {
              scoreChoice : this.scoreSelected,
              interestCodeOne : this.scoreSelected == 'gender' ? this.genderScoreSort[0].interestCd : this.combinedScoreSort[0].interestCd,
              interestCodeTwo : this.scoreSelected == 'gender' ? this.genderScoreSort[1].interestCd : this.combinedScoreSort[1].interestCd,
              interestCodeThree : this.scoreSelected == 'gender' ? this.genderScoreSort[2].interestCd : this.combinedScoreSort[2].interestCd
          }

          // Rest call to post users FYI scores.
          //FYIRestFactory.postUserScores(angular.toJson(scoreObject))
          this._httpHelper.httpHelper( 'POST', 'fyi/PostScore', null, scoreObject).then((response) => {

              // Remove session storage for interest codes.
              window.sessionStorage.removeItem("interestCodeOne");
              window.sessionStorage.removeItem("interestCodeTwo");
              window.sessionStorage.removeItem("interestCodeThree");
              
              // Reset values
              this.FYIScoreService.setCombinedTiedScoreObject(undefined);
              this.FYIScoreService.setGenderTiedScoreObject(undefined);
              window.sessionStorage.removeItem("combinedTiedScoreObject");
              window.sessionStorage.removeItem("genderTiedScoreObject");

              // Enables going back to the tied break page if user returns to
              // FYI page.
              /*FYIScoreService.setTopCombinedInterestCodes(undefined);
              FYIScoreService.setTopGenderInterestCodes(undefined);*/
              
              // Determines which section to highlight using CSS.
              this.myClassCombined = this.scoreSelected == 'combined' ? {
                  selected : true
              } : {
                  selected : false
              };
              this.myClassGender = this.scoreSelected == 'gender' ? {
                  selected : true
              } : {
                  selected : false
              };

              //TODO: angular way of updating
              /*UserFactory.getUserInterestCodes().then(function(response) {
                  // Update left navigation interest codes.
                  $rootScope.$emit("updateInterestCodes", {});
              }, function(error){
              });*/
              
            //   this.leftNavi.setUserInterestCodes();
          }, function errorCallback(response) {
              alert("failed to save test scores");
              /*var modalOptions = {
                  headerText : 'Error',
                  bodyText : 'Failed to save test scores. Try submitting again. If Error continues, please inform site administrator.'
              };

              modalService.showModal({
                  size : 'sm'
              }, modalOptions);*/
          });
      }
  }

  getCodeName = function (code) {
    switch (code) {
      case 'R' : return 'Realistic';
      case 'I' : return 'Investigative';
      case 'A' : return 'Artistic';
      case 'S' : return 'Social';
      case 'E' : return 'Enterprising';
      case 'C' : return 'Conventional';
      default:
          return '';
    }
  }

  getCodeDefinition = function(code) {
    switch (code) {
      case 'R' : return 'activities that often involve practical, hands-on problems and solutions';
      case 'I' : return 'activities that involve learning about new subject areas or ideas and allows me to use my knowledge to solve problems';
      case 'A' : return 'activities that allows me to be creative and use my imagination to do original work';
      case 'S' : return 'activities that allows me to use my skills and talents to interact effectively with others';
      case 'E' : return 'activities that allows me to take a leadership role';
      case 'C' : return 'activities that require attention to accuracy and detail';
      default:
          return '';
    }
  }
  
  genderInfoModal() {
      var modalOptions = {
          headerText: 'Gender Specific',
          bodyText: 'Growing up, males and females get different messages from their parents, schools, and the media about what careers are appropriate for them, resulting in exposure to different experiences. Three interest areas, Realistic, Artistic, and Social, tend to have significant difference in how males and females respond.  Gender-Specific results show your interests as they compare to those in your gender, and the scores may offer you a different set of careers to explore.'
      };

      const data = {
          'title': modalOptions.headerText,
          'message': modalOptions.bodyText
      };
      const dialogRef1 = this.dialog.open( MessageDialogComponent, {
          data: data,
          //hasBackdrop: true,
          //disableClose: true,
          maxWidth: '400px'
      } );
  }

  interestCodeInfo( interestCode ) {
      var modalOptions;
      switch ( interestCode ) {
          case "R":
              modalOptions = {
                  headerText: 'Realistic',
                  bodyText: '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
                  '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
                  '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
              };
              break;
          case "I":
              modalOptions = {
                  headerText: 'Investigative',
                  bodyText: '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
                  '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
                  '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
              };
              break;
          case "A":
              modalOptions = {
                  headerText: 'Artistic',
                  bodyText: '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
                  '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
                  '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
              };
              break;
          case "S":
              modalOptions = {
                  headerText: 'Social',
                  bodyText: '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
                  '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
                  '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
              };
              break;
          case "E":
              modalOptions = {
                  headerText: 'Enterprising',
                  bodyText: '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
                  '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
                  '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
              };
              break;
          case "C":
              modalOptions = {
                  headerText: 'Conventional',
                  bodyText: '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
                  '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
                  '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
              };
              break;
      }

      const data = {
          'title': modalOptions.headerText,
          'message': modalOptions.bodyText
      };
      const dialogRef1 = this.dialog.open( MessageDialogComponent, {
          data: data,
          //hasBackdrop: true,
          //disableClose: true,
          maxWidth: '400px'
      } );
  }
}
