import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FyiTestComponent } from './fyi-test.component';

describe('FyiTestComponent', () => {
  let component: FyiTestComponent;
  let fixture: ComponentFixture<FyiTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FyiTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FyiTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
