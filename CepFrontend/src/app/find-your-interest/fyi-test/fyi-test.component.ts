import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { FindYourInterestService } from '../../services/find-your-interest.service';
import { FyiTestService } from '../../services/fyi-test.service';

@Component( {
    selector: 'app-fyi-test',
    templateUrl: './fyi-test.component.html',
    styleUrls: ['./fyi-test.component.scss']
} )
export class FyiTestComponent implements OnInit {
    questionList: any;
    gender = "";
    validation = undefined;
    threshold: number = 0;
    submitted = false;
    disableSubmit = false;

    constructor( private route: ActivatedRoute,
        private _router: Router,
        private FYIScoreService: FindYourInterestService,
        private _fyiTestService: FyiTestService,
        private _dialog: MatDialog,
    ) { }

    ngOnInit() {
        const savedFYIQuestions = window.sessionStorage.getItem('FYIQuestions');
        let genderStored = window.sessionStorage.getItem('gender')
		this.gender = genderStored ? genderStored : '';

		if (savedFYIQuestions) {
			this.questionList = JSON.parse(savedFYIQuestions);
		} else {
			// this.questionList = this.route.snapshot.data.data[0].slice(0,3);  // for testing
            this.questionList = this.route.snapshot.data.data[0];
            this.questionList.forEach(q => q.answer = undefined);
			window.sessionStorage.setItem('FYIQuestions', JSON.stringify(this.questionList));
		}

        // this.questionList.forEach(q => q.answer = undefined);
        console.debug( this.questionList );
        this.calculateThreshold();
        
        //test
        /*for ( var i = 0; i < this.questionList.length; i++ ) {
            if ( i < 30 ) {
                this.questionList[i].answer = "2";
            } else if ( i < 60 ) {
                this.questionList[i].answer = "1";
            } else if ( i < 90 ) {
                this.questionList[i].answer = "0";
            }
        }*/
        /*for ( var i = 0; i < this.questionList.length; i++ ) {
            if ( i < 78 ) {
                this.questionList[i].answer = "2";
            } else if ( i < 90 ) {
                this.questionList[i].answer = "1";
            }
        }
        console.log( this.questionList );*/

    }

    /*submitAnswer = function() {
        this._router.navigate( ['/find-your-interest-score'] );
    }*/

    saveFYIQustions(answer = null, index = null) {
        if (answer && index >= 0) {
            window.sessionStorage.removeItem("FYIQuestions");
            this.questionList[index].answer = answer;
            window.sessionStorage.setItem('FYIQuestions', JSON.stringify(this.questionList));
        }
        
		// if (this.gender) {
            window.sessionStorage.removeItem("gender");
			window.sessionStorage.setItem('gender', this.gender);
		// }
	}

    setGender(param) {
        this.gender = param;
        this.resetValidation();
    }

    resetValidation() {
    	this.validation = undefined;
    }
    
    calculateThreshold() {
		var scoredItems = 0;
		for (var i = 0; i < this.questionList.length; i++) {
			if (!this.questionList[i].isSeeded) {
				scoredItems++;
			}
		}
		// scored items * 90% / 100 = threshold
        this.threshold = scoredItems * 90 / 100;
        // this.threshold = 100; // for testing
	}

    submitAnswer() {

        this.submitted = true;
        this.disableSubmit = true;

        // this will be used for post call
        var fyiObject = {
            gender: this.gender,
            responses: undefined,
            rawScores: undefined,
            testId: undefined,
        };

        // set complete questionAnswerList in service
        this.FYIScoreService.questionAnswerList = this.questionList;

        // form validate for safari support
        var len = this.questionList.length;
        for ( var i = 0; i < len; i++ ) {
            if (this.questionList[i].answer === undefined) {
				this.validation = 'Please answer all questions';
                this.disableSubmit = false;
				return false;
			}
        }

        // form validate for safari support
        // if ( this.gender == "" ) {
        //     this.validation = 'Please select gender';
        //     this.disableSubmit = false;
        //     return false;
        // }

        // reset values
        this.FYIScoreService.resetValues();

        // calculate score
        this.FYIScoreService.calculateRawScore();

        var likes = this.FYIScoreService.getInterestLikes();
        var indifferents = this.FYIScoreService.getInterestIndifferents();
        var dislikes = this.FYIScoreService.getInterestDislikes();

        // if 81 or more of the same responses alert user to vary response
        if ( likes >= this.threshold || indifferents >= this.threshold || dislikes >= this.threshold ) {
            const modalOptions = {
                title : 'Find Your Interest',
                message : '<p><b>Your response did not show a sufficient pattern of likes and dislikes for us to score your responses and provide you with your three Top Interest Codes.</b></p>' + '<p><b>Try retaking the inventory and using more varied responses.</b></p>' + '<ul><li>If you used a lot of Likes. try being more selective.</li><li>If you used a lot of Dislike, try being less selective.</li><li>If you used a lot of indifference\'s, move off the fence</li></ul>' + '<hr>' + '<b>Tips for retaking the FYI:</b>' + '<ul><li>Read each interest item carefully before responding.</li><li>Make sure that your responses accurately reflect your interests.</li>' + '<li>Think about hobbies that you enjoy and relate them to the items in the FYI.</li><li>Keep in mind that you are not selecting an occupation. You are selecting activities that you either like, dislike, or are indifferent to doing</li></ul>'
              };
          
              const dialogRef1 = this._dialog.open(MessageDialogComponent, {
                data: modalOptions,
                hasBackdrop: true,
                disableClose: true,
                maxWidth: 700,
              });

            this.disableSubmit = false;
            return false;
        }

        var calcScore = this.FYIScoreService.getInterestCodeRawScore();

        fyiObject.rawScores = [{
            interestCd: 'R',
            rawScore: calcScore.rRawScore
        }, {
            interestCd: 'I',
            rawScore: calcScore.iRawScore
        }, {
            interestCd: 'A',
            rawScore: calcScore.aRawScore
        }, {
            interestCd: 'S',
            rawScore: calcScore.sRawScore
        }, {
            interestCd: 'E',
            rawScore: calcScore.eRawScore
        }, {
            interestCd: 'C',
            rawScore: calcScore.cRawScore
        }];

        /*
         * $scope.interestCodeRawScore =
         * FYIScoreService.getInterestCodeRawScore();
         * 
         * var genderRest =
         * FYIRestFactory.getGenderScore(angular.toJson($scope.interestCodeRawScore)).then(function
         * successCallback(response) { FYIScoreService.genderScore =
         * response.data; }, function errorCallback(response) { alert(response); })
         * 
         * var combinedRest =
         * FYIRestFactory.getCombinedScore(angular.toJson($scope.interestCodeRawScore)).then(function
         * successCallback(response) { FYIScoreService.combinedScore =
         * response.data; }, function errorCallback(response) { alert(response); })
         */

        // delete properties not being used anymore before posting data
        let answers = JSON.parse(JSON.stringify(this.questionList));
        for ( var i = 0; i < answers.length; i++ ) {
            delete answers[i].stem;
        }

        this._fyiTestService.saveTest().then((success: any) => {
            fyiObject.testId = success;
            fyiObject.responses = JSON.stringify(answers);
            
            this.FYIScoreService.setFyiObject( fyiObject );
            this._router.navigate( ['/find-your-interest-score'] );
        }).catch(error => {
            console.error('Error: ' + error);
            this.validation = error && error.status &&  error['status'] >= 400 &&  error['status'] < 500 ? 'Error: Bad request' : 'Error: Server Error';
            this.disableSubmit = false;
          });
    };

}
