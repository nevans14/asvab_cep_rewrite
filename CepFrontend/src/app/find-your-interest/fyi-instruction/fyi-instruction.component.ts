import { Component, OnInit } from '@angular/core';
import { MediaCenterService } from 'app/services/media-center.service';
import { UtilityService } from 'app/services/utility.service';

@Component( {
    selector: 'app-fyi-instruction',
    templateUrl: './fyi-instruction.component.html',
    styleUrls: ['./fyi-instruction.component.scss']
} )
export class FyiInstructionComponent implements OnInit {

    mediaCenterList;
    constructor(
        private mediaCenterService: MediaCenterService,
        private _utilityService: UtilityService,
    ) { }

    ngOnInit() {
        this.mediaCenterService.getMediaCenterList().then(
            data => {
                this.mediaCenterList = data;
            } );
    }

}
