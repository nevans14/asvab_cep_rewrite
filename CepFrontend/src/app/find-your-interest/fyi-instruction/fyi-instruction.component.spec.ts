import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FyiInstructionComponent } from './fyi-instruction.component';

describe('FyiInstructionComponent', () => {
  let component: FyiInstructionComponent;
  let fixture: ComponentFixture<FyiInstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FyiInstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FyiInstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
