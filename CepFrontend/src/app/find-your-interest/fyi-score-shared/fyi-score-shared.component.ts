import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from '../../core/dialogs/message-dialog/message-dialog.component';
import { UtilityService } from 'app/services/utility.service';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-fyi-score-shared',
  templateUrl: './fyi-score-shared.component.html',
  styleUrls: ['./fyi-score-shared.component.scss']
})
export class FyiScoreSharedComponent implements OnInit {

  absUrl;
  codeOne;
  codeTwo;
  isUserLoggedIn;
  description;
  imageUrl;
  occuFindUserLoggedIn;

  constructor(
    private _utilityService: UtilityService,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _dialog: MatDialog,
    private _configService: ConfigService,
    private _meta: Meta,
    private _titleTag: Title,
  ) { }

  ngOnInit() {
    // canonical url
    this.absUrl = this._utilityService.getCanonicalUrl();
    this.codeOne = this._activatedRoute.snapshot.params.iCodeOne;
    this.codeTwo = this._activatedRoute.snapshot.params.iCodeTwo;
    this.description = undefined;
    
    this.updateMeta();

    if ( this._userService.isLoggedIn() ) {
      this.occuFindUserLoggedIn = true;
    } else {
        this.occuFindUserLoggedIn = false;
    }
  }

  updateMeta() {
    // sets up the image
    var topScores = this.codeOne + this.codeTwo;	
    this.imageUrl = this._configService.getImageUrl() + this._configService.getMediaCenterFolder() + 'thumbnails/social-share/fyi-score/FYI_facebook_' + topScores + '_IMG.jpg';
    this.description = "My interests are " + 
        this.getCodeName(this.codeOne) + 
        " and " + 
        this.getCodeName(this.codeTwo) + 
        " which means I like " + 
        this.getCodeDefinition(this.codeOne) + 
        " and " + 
        this.getCodeDefinition(this.codeTwo) + ".";

    this._titleTag.setTitle('My FYI Results | ASVAB Career Exploration Program');
    this._meta.updateTag({name: 'description', content: this.description});

    this._meta.updateTag({property: 'og:title', content: 'My FYI Results | ASVAB Career Exploration Program'});
    this._meta.updateTag({property: 'og:description', content: this.description});
    this._meta.updateTag({property: 'og:image', content: this.imageUrl});
    this._meta.updateTag({property: 'og:image:alt', content: 'My FYI Results | ASVAB Career Exploration Program'});
    // this._meta.updateTag({property: 'og:url', content: this.shareUrl});

    this._meta.updateTag({ name: 'twitter:site', content: '@CareersintheMil'});
    this._meta.updateTag({ name: 'twitter:creator', content: '@CareersintheMil'});
    this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.updateTag({ name: 'twitter:title', content:  'My FYI Results | ASVAB Career Exploration Program'});
    this._meta.updateTag({ name: 'twitter:image', content: this.imageUrl});
    this._meta.updateTag({ name: 'twitter:description', content: this.description});
  }

  getCodeName = function (code) {
		switch (code) {
			case 'R' : return 'Realistic';
			case 'I' : return 'Investigative';
			case 'A' : return 'Artistic';
			case 'S' : return 'Social';
			case 'E' : return 'Enterprising';
			case 'C' : return 'Conventional';
			default:
				return '';
		}
	}
	
	getCodeDefinition = function(code) {
		switch (code) {
			case 'R' : return 'activities that often involve practical, hands-on problems and solutions';
			case 'I' : return 'activities that involve learning about new subject areas or ideas and allows me to use my knowledge to solve problems';
			case 'A' : return 'activities that allows me to be creative and use my imagination to do original work';
			case 'S' : return 'activities that allows me to use my skills and talents to interact effectively with others';
			case 'E' : return 'activities that allows me to take a leadership role';
			case 'C' : return 'activities that require attention to accuracy and detail';
			default:
				return '';
		}
  }
  
  interestCodeInfo( interestCode ) {
    var modalOptions;
    switch ( interestCode ) {
        case "R":
            modalOptions = {
                headerText: 'Realistic',
                bodyText: '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
                '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
                '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
            };
            break;
        case "I":
            modalOptions = {
                headerText: 'Investigative',
                bodyText: '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
                '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
                '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
            };
            break;
        case "A":
            modalOptions = {
                headerText: 'Artistic',
                bodyText: '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
                '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
                '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
            };
            break;
        case "S":
            modalOptions = {
                headerText: 'Social',
                bodyText: '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
                '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
                '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
            };
            break;
        case "E":
            modalOptions = {
                headerText: 'Enterprising',
                bodyText: '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
                '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
                '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
            };
            break;
        case "C":
            modalOptions = {
                headerText: 'Conventional',
                bodyText: '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
                '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
                '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
            };
            break;
    }

    const data = {
        'title': modalOptions.headerText,
        'message': modalOptions.bodyText
    };

    const dialogRef1 = this._dialog.open( MessageDialogComponent, {
        data: data,
        //hasBackdrop: true,
        //disableClose: true,
        maxWidth: '400px'
    });
  }
}
