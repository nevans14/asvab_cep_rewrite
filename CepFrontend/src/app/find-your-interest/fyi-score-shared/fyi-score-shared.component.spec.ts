import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FyiScoreSharedComponent } from './fyi-score-shared.component';

describe('FyiScoreSharedComponent', () => {
  let component: FyiScoreSharedComponent;
  let fixture: ComponentFixture<FyiScoreSharedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FyiScoreSharedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FyiScoreSharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
