import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FyiTiedComponent } from './fyi-tied.component';

describe('FyiTiedComponent', () => {
  let component: FyiTiedComponent;
  let fixture: ComponentFixture<FyiTiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FyiTiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FyiTiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
