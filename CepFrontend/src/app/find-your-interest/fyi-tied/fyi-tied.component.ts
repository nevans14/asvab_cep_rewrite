import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { FindYourInterestService } from '../../services/find-your-interest.service';
import { HttpHelperService } from '../../services/http-helper.service';

@Component({
  selector: 'app-fyi-tied',
  templateUrl: './fyi-tied.component.html',
  styleUrls: ['./fyi-tied.component.scss']
})
export class FyiTiedComponent implements OnInit {

    /**
     * 
     * Combined Tie Score
     * 
     */
    combinedTiedScoreObject;
    showGenderTieSelection = false;
    
    combinedLimit;
    combinedChecked;
    combinedCheckChanged;
    
    
    /**
     * 
     * Gender Tie Score
     * 
     */
    genderTiedScoreObject;
    
    genderLimit;
    genderChecked;
    genderCheckChanged;
  constructor(
      private route: ActivatedRoute,
      private _router: Router,
      private _dialog: MatDialog,
      private FYIScoreService: FindYourInterestService,
      private _httpHelper: HttpHelperService,
    ) { }

  ngOnInit() {
      
      /**
       * Keep track and limit number of selections that can be made.
       */
      this.combinedTiedScoreObject = this.FYIScoreService.getCombinedTiedScoreObject();
      
      if (this.combinedTiedScoreObject !== undefined) {
          this.combinedLimit = this.combinedTiedScoreObject.numSlotsLeft;
          this.combinedChecked = 0;
          this.combinedCheckChanged = function(item) {
              if (item.selected)
                  this.combinedChecked++;
              else
                  this.combinedChecked--;
          }
      } else {
          this.showGenderTieSelection = true;
      }
      
      
      /**
       * Keep track and limit number of selections that can be made.
       */
      this.genderTiedScoreObject = this.FYIScoreService.getGenderTiedScoreObject();
      
      if (this.genderTiedScoreObject !== undefined) {
          this.genderLimit = this.genderTiedScoreObject.numSlotsLeft;
          this.genderChecked = 0;
          this.genderCheckChanged = function(item) {
              if (item.selected)
                  this.genderChecked++;
              else
                  this.genderChecked--;
          }
      }

  }
  
  

  
  /**
   * Submit score selection.
   */
  combinedSubmit() {
      var topInterestCodes = [];
      var scoreList = this.combinedTiedScoreObject.scoreList;

      // determine the top three interest scores
      var numSelection = 0;
      for (var i = 0; i < scoreList.length; i++) {
          if (topInterestCodes.length < 3) {
              if (scoreList[i].checkbox != true) {
                  topInterestCodes.push(scoreList[i]);
              } else {
                  if (scoreList[i].selected == true) {
                      topInterestCodes.push(scoreList[i]);
                      numSelection++;
                  }
              }
          }
      }

      // validate selections are made
      if (numSelection != this.combinedLimit) {
          const modalOptions = {
            title : 'Find Your Interest',
            message : 'Select ' + this.combinedTiedScoreObject.numSlotsLeft + ' of the ' + this.combinedTiedScoreObject.numTiedScore + ' tied interests.'
          };
      
          const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: modalOptions,
            hasBackdrop: true,
            disableClose: true,
            maxWidth: 700,
          });

          return false;
      }

      this.FYIScoreService.setTopCombinedInterestCodes(topInterestCodes);

      // Determines if gender scores need a tie breaker. // FIX
      if (this.genderTiedScoreObject != undefined
        && window.sessionStorage.getItem("gender")) {
          var numMatch = 0;
          var combinedNumberCheckboxes = 0;
          var genderNumberCheckboxes = 0;

          for (let i in this.combinedTiedScoreObject.scoreList) {
              if (this.combinedTiedScoreObject.scoreList[i].checkbox == true) {
                  combinedNumberCheckboxes++;
              }
          }

          for (let i in this.genderTiedScoreObject.scoreList) {
              if (this.genderTiedScoreObject.scoreList[i].checkbox == true) {
                  genderNumberCheckboxes++;
              }
          }
          if (combinedNumberCheckboxes == genderNumberCheckboxes) {
              for (var i = 0; i < scoreList.length; i++) {
                  for (var f = 0; f < this.genderTiedScoreObject.scoreList.length; f++) {
                      if (scoreList[i].checkbox == true && this.genderTiedScoreObject.scoreList[f].checkbox == true && scoreList[i].interestCd == this.genderTiedScoreObject.scoreList[f].interestCd) {
                          numMatch++;
                          break;
                      }
                  }
              }
          }

          // If interest codes are not the same then display gender score tie
          // breaker.
          if (numMatch != combinedNumberCheckboxes) {
              this.showGenderTieSelection = true;
              return false;
          } else {

              // Gender tie score breaker are the same for combined, so reuse
              // combined FYI selection for gender scores.
              var genderScoreList = this.genderTiedScoreObject.scoreList;
              var tempGenderScoreList = [];
              for (var f = 0; f < topInterestCodes.length; f++) {
                  for (var i = 0; i < genderScoreList.length; i++) {
                      if (topInterestCodes[f].interestCd == genderScoreList[i].interestCd) {
                          tempGenderScoreList.push(genderScoreList[i]);
                          break;
                      }
                  }
              }

              this.FYIScoreService.setTopGenderInterestCodes(tempGenderScoreList);
          }

      }

      // Save Top interest code to databse
      this.saveTopInterestCodeToDatabase();

      // Redirect to score page.
      this._router.navigate( ['/find-your-interest-score'] );

  }


  /**
   * Submit score selection.
   */
  genderSubmit() {
      var topInterestCodes = [];
      var scoreList = this.genderTiedScoreObject.scoreList;

      // determine the top three interest scores
      var numSelection = 0;
      for (var i = 0; i < scoreList.length; i++) {
          if (topInterestCodes.length < 3) {
              if (scoreList[i].checkbox != true) {
                  topInterestCodes.push(scoreList[i]);
              } else {
                  if (scoreList[i].selected == true) {
                      topInterestCodes.push(scoreList[i]);
                      numSelection++;
                  }
              }
          }
      }

      // validate selections are made
      if (numSelection != this.genderLimit) {
          const modalOptions = {
            title : 'Find Your Interest',
            message : 'Select ' + this.genderTiedScoreObject.numSlotsLeft + ' of the ' + this.genderTiedScoreObject.numTiedScore + ' tied interests.',
          };
      
          const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: modalOptions,
            hasBackdrop: true,
            disableClose: true,
            maxWidth: 700,
          });

          return false;
      }

      this.FYIScoreService.setTopGenderInterestCodes(topInterestCodes);

      // Save Top interest code to database
      this.saveTopInterestCodeToDatabase();

      // Redirect to score page.
      this._router.navigate( ['/find-your-interest-score'] );
  }

  /**
   * Saves the top selected interest codes for gender and combine to the
   * database.
   */
  saveTopInterestCodeToDatabase () {
      
      // Reset values
      this.FYIScoreService.setCombinedTiedScoreObject(undefined);
      this.FYIScoreService.setGenderTiedScoreObject(undefined);
      window.sessionStorage.removeItem("combinedTiedScoreObject");
      window.sessionStorage.removeItem("genderTiedScoreObject");
      
      var topGScore = this.FYIScoreService.getTopGenderInterestCodes();
      var topCScore = this.FYIScoreService.getTopCombinedInterestCodes();
      var tiedObject = {
          combineInterestOne : topCScore ? topCScore[0].interestCd : null,
          combineInterestTwo : topCScore ? topCScore[1].interestCd : null,
          combineInterestThree : topCScore ? topCScore[2].interestCd : null,
          genderInterestOne : topGScore ? topGScore[0].interestCd : null,
          genderInterestTwo : topGScore ? topGScore[1].interestCd : null,
          genderInterestThree : topGScore ? topGScore[2].interestCd : null,
      }
      
      //this.FYIRestFactory.insertTiedSelection(tiedObject);
      var promise = new Promise(( resolve, reject ) => {
          
          this._httpHelper.httpHelper( 'POST', 'fyi/insert-tied-selection', null, tiedObject ).then((success) => {
              resolve(success);
          }, function(error) {
              reject(error);
          });
      });
      
      // Reset value
      this.FYIScoreService.setTiedLinkClicked(false);
  }

}
