import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { ScoreService } from '../services/score.service';
import { MatDialog } from '@angular/material';
import { PortfolioTipsDialogComponent } from 'app/core/dialogs/portfolio-tips-dialog/portfolio-tips-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import * as cloneDeep from 'lodash/cloneDeep';
import { FormGroup } from "@angular/forms";
import { UtilityService } from 'app/services/utility.service';
import html2canvas from "html2canvas";
import { ElementRef } from "@angular/core";
import { SubmitToMentorDialogComponent } from 'app/core/dialogs/submit-to-mentor-dialog/submit-to-mentor-dialog.component';

declare var pdfMake: any;
@Component( {
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./portfolio.component.scss']
} )
export class PortfolioComponent implements OnInit {

    ssoUrl;
    domainName;
    myStyle = {};
    user: any;
    fullName: String;
    grade: any;
    userInfo: any;
    portfolioUserLoggedIn: any;
    portfolio: any;
    citmFavoriteOccupations: any;

    favoriteList;
    careerClusterFavoriteList;
    schoolFavoriteList;

    scoreChoice;
    hideFyi;
    fyiScore;

    workExperience;
    education;
    achievement;
    interest;
    skill;
    workValue;
    volunteer;
    actScore;
    asvabScore: any;
    //fyiScore;
    satScore;
    otherScore;
    plan;
    UserInfo;
    //citmFavoriteOccupations;
    isWorkValueResults;
    workValueTopResults;

    isCombinedPlanFormat: boolean;
    combinedPlanObj = {
        plan: {
			fourYearCollege: false,
			twoYearCollege: false,
			careerAndTech: false,
			military: false,
			work: false,
			gapYear: false,
            undecided: false,
            certificate: false,
            technicalSchool: false,
            vocationalTraining: false,
            advancedDegree: false
        },        
        id: undefined,
        userId: undefined,
		description: ""
    };
    originalPlanObj = {};
    
    public student:any;
    public studentName:string;

    mobileOrTablet: boolean = false;

    // new print and download
    public printObj: any;
    @ViewChildren('printMe') printElements: QueryList<ElementRef>;
    @ViewChild('printSectionNew') printSectionElem: ElementRef;

    //gpa = new FormControl(); 
    //gpaPatternValidation='/^((\d+\.?|\.(?=\d))?\d{0,3})$/';
    gpaPatternValidation = '/^\d*\.?\d*$/';
    constructor(
        private _portfolioService: PortfolioService,
        private _configService: ConfigService,
        private _userService: UserService,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _scoreService: ScoreService,
        private _router: Router,
        private _dialog: MatDialog,
        private route: ActivatedRoute,
        private _utilityService: UtilityService,
    ) {
    	this.printObj = {};
        this.mobileOrTablet = this._utilityService.mobileOrTablet();
    }


    ngOnInit() {

        this.ssoUrl = this._configService.getSsoUrl();
        this.domainName = this._configService.getBaseUrl();

        // Portfolio data resolver
        let portfolioResolveArray = this.route.snapshot.data.portfolioResolver;

        this.workExperience = portfolioResolveArray[0];
        this.education = portfolioResolveArray[1];
        this.achievement = portfolioResolveArray[2];
        this.interest = portfolioResolveArray[3];
        this.skill = portfolioResolveArray[4];
        this.workValue = portfolioResolveArray[5];
        this.volunteer = portfolioResolveArray[6];
        this.actScore = portfolioResolveArray[7];
        this.asvabScore = portfolioResolveArray[8];
        this.fyiScore = portfolioResolveArray[9];
        this.satScore = portfolioResolveArray[10];
        this.otherScore = portfolioResolveArray[11];
        this.favoriteList = portfolioResolveArray[12];
        this.careerClusterFavoriteList = portfolioResolveArray[13];
        this.plan = portfolioResolveArray[14];
        this.UserInfo = portfolioResolveArray[15];
        this.citmFavoriteOccupations = portfolioResolveArray[16];
        this.schoolFavoriteList = portfolioResolveArray[17];
        this.portfolio = portfolioResolveArray[18];
        this.student = portfolioResolveArray[19];
        if (this.citmFavoriteOccupations 
            && this.citmFavoriteOccupations.length > 0
        ) {
            this._userService.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL, 'true');
        }

        this.user = this._userService.getUser();

        if ( this._userService.isLoggedIn() ) {
			this.portfolioUserLoggedIn = true;
		} else {
			this.portfolioUserLoggedIn = false;
        }
        
        this.planObject = {
            plan: '',
            description: undefined
        };

        this.workExperienceObject = {
            companyName: undefined,
            jobTitle: undefined,
            startDate: undefined,
            endDate: undefined,
            jobDescription: undefined,
            id: undefined
        };

        this.educationObject = {
            schoolName: undefined,
            gpa: undefined,
            startDate: undefined,
            endDate: undefined,
            activities: undefined,
            achievements: undefined,
            id: undefined
        };

        this.achievementObject = {
            description: undefined
        };

        this.skillObject = {
            description: undefined
        };

        this.fyiObject = {
            interest_code_one: undefined,
            interest_code_two: undefined,
            interest_code_three: undefined
        };

        this.interestObject = {
            description: undefined
        };

        this.workValueObject = {
            description: undefined
        };

        this.volunteerObject = {
            organizationName: undefined,
            startDate: undefined,
            endDate: undefined,
            description: undefined
        };

        this.actObject = {
            readingScore: undefined,
            mathScore: undefined,
            writingScore: undefined,
            englishScore: undefined,
            scienceScore: undefined
        };

        this.satObject = {
            readingScore: undefined,
            mathScore: undefined,
            writingScore: undefined,
            subject: undefined
        };

        this.otherObject = {
            description: undefined
        };

        if ( this.UserInfo.length > 0 ) {
            this.fullName = this.UserInfo[0].name;
            this.grade = this.UserInfo[0].gpa;
        } else {
            this.fullName = '';
            this.grade = '';
        }

        this.isCombinedPlanFormat = this.plan.length > 0 && this._utilityService.isJson(this.plan[0].plan);

        this.setupCombinedPlanObj();

        // Retrieve scores from session
        if ( window.sessionStorage.getItem( 'sV' ) && window.sessionStorage.getItem( 'manualScores' ) == "false" ) {
            this.haveAsvabTestScores = true;
            this.asvabTestScores = {
                verbalScore: window.sessionStorage.getItem( 'sV' ),
                mathScore: window.sessionStorage.getItem( 'sM' ),
                scienceScore: window.sessionStorage.getItem( 'sS' ),
                afqtScore: this.afqtScore = window.sessionStorage.getItem( 'sA' )
            }
            // if the asvab score list has no scores, then add it from session
            if ( this.asvabScore.length == 0 ) {
                this.asvabScore.push( this.asvabTestScores );
            }
        }

        this.testSelected = this.actScore.length == 0 ? 'ACT' : this.asvabScore.length == 0 ? 'ASVAB' : this.satScore.length == 0 ? 'SAT' : 'OTHER'; // ACT

        /**
         * FYI. If user has FYI scores then use it as default.
         */
        // Use FYI test results if exists.
        this._userService.getUserInterestCodes().then(( response: any ) => {
            this.scoreChoice = response.scoreChoice;

            if ( response.length == 0 ) {
                this.hideFyi = false;
                this.fyiScore = this.fyiScore;
            } else {
                this.hideFyi = true; // hide fyi inputs
                var array = [];
                array.push( response );
                this.fyiScore = array;
            }

        }, reason => {
            this.hideFyi = false;
            if ( reason.length == 0 ) {
                this.fyiScore = this.fyiScore;
            } else {
                alert( "Error fetching FYI test result." + reason );
            }
        } );
        
    	this.isWorkValueResults = JSON.parse(window.sessionStorage.getItem('workValueTopResults')) ? true : false;
    	this.workValueTopResults = JSON.parse(window.sessionStorage.getItem('workValueTopResults'));

        //set student name
        if(this.student){
            if (!this.student.firstName || !this.student.lastName) {
                this.studentName = this.student.emailAddress;
            } else {
                this.studentName = this.student.firstName + " " + this.student.lastName;
            }
        } else {
            this.studentName = "";
        }
    }
    
    getInterestTitle(code) {
		switch (code) {
			case 'R': return 'Realistic';
			case 'I': return 'Investigative';
			case 'A': return 'Artistic';
			case 'S': return 'Social';
			case 'E': return 'Enterprising';
			case 'C': return 'Conventional';
			default:
				return '';
		}
	}

    /**
     * Saves name and grade to database
     */
    syncData() {
        this.userInfo = {
            name: this.fullName,
            gpa: this.grade,
        };

        var promise = this._portfolioService.updateInsertUserInfo( this.userInfo );
        promise.then( response => {
        }, reason => {
            console.error( "userinfo failed to saved" + reason );
        } );
    }

    setupCombinedPlanObj() {
		if (this.plan.length > 0 && this.isCombinedPlanFormat) {
			// Update combined plan case
			this.combinedPlanObj.plan = JSON.parse(this.plan[0].plan);
			this.combinedPlanObj.description = this.plan[0].description;
			this.combinedPlanObj.id = JSON.parse(this.plan[0].id);
			//this.combinedPlanObj.userId = JSON.parse(this.plan[0].userId);
		} else if (this.plan.length > 0 && !this.isCombinedPlanFormat) {
			// Migrate old to new format case
			this.plan.forEach(function(p) {
				switch(p.plan) {
					case '4 Year College/University':
						this.combinedPlanObj.plan.fourYearCollege = true;
						break;
					case '2 Year College':
						this.combinedPlanObj.plan.twoYearCollege = true;
						break;
					case 'Career and Technical Education':
						this.combinedPlanObj.plan.careerAndTech = true;
						break;
					case 'Military':
						this.combinedPlanObj.plan.military = true;
						break;
					case 'Work':
						this.combinedPlanObj.plan.work = true;
						break;
					case 'Gap Year':
						this.combinedPlanObj.plan.gapYear = true;
						break;
					case 'Undecided':
						this.combinedPlanObj.plan.undecided = true;
						break;
				}

				if (p.description) {
					this.combinedPlanObj.description =
						this.combinedPlanObj.description.concat(p.description, "\n");
				}
			})
		} // if neither, then it means we add combined plan

		this.originalPlanObj = cloneDeep( this.combinedPlanObj );
	}

	revertToOriginal(){
		this.combinedPlanObj =cloneDeep( this.originalPlanObj );
    }
    
    /**
     * User's selection for sections to include in print.
     */
    workExperiencePrint = true;
    educationPrint = true;
    testScorePrint = true;
    achievementPrint = true;
    skillsPrint = true;
    volunteerPrint = true;
    interestPrint = true;
    workValuesPrint = true;
    favoriteOccupationPrint = true;
    favoriteCitmOccupationPrint = true;
    favoriteCareerClusterPrint = true;
    favoriteSchoolPrint = true;
    planPrint = true;

    /**
     * Check all sections to print.
     */
    selectAll = function() {
        this.workExperiencePrint = true;
        this.educationPrint = true;
        this.testScorePrint = true;
        this.achievementPrint = true;
        this.skillsPrint = true;
        this.volunteerPrint = true;
        this.interestPrint = true;
        this.workValuesPrint = true;
        this.favoriteOccupationPrint = true;
        this.favoriteCitmOccupationPrint = true;
        this.favoriteCareerClusterPrint = true;
        this.favoriteSchoolPrint = true;
        this.planPrint = true;
    }

    /**
     * Uncheck all sections.
     */
    selectNone = function() {
        this.workExperiencePrint = false;
        this.educationPrint = false;
        this.testScorePrint = false;
        this.achievementPrint = false;
        this.skillsPrint = false;
        this.volunteerPrint = false;
        this.interestPrint = false;
        this.workValuesPrint = false;
        this.favoriteOccupationPrint = false;
        this.favoriteCitmOccupationPrint = false;
        this.favoriteCareerClusterPrint = false;
        this.favoriteSchoolPrint = false;
        this.planPrint = false;
    }

    /**
     * Tips modal popup
     */
    tipsModal = function() {
        let dialogRef = this._dialog.open( PortfolioTipsDialogComponent, {
        } );
    }

    /*
     * Work experience
     */
    showWorkExperienceInputs = false;
    showUpdateWorkExperienceInputs = false;
    savingWorkExperience = false;
    workExperienceEndDateRequired = true;
    updateWorkExperienceEndDateDisabled = true;
    workExperienceObject;
    updateWorkExperienceObject;
    selectedWorkEditIndex;

    /**
     * Setup for updating work experience input.
     */
    setupUpdateWorkExperience( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedWorkEditIndex = index;
        this.showWorkExperienceInputs = true;
        this.showUpdateWorkExperienceInputs = true;

        var length = this.workExperience.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.workExperience[i].id == id ) {
                this.updateWorkExperienceObject = cloneDeep( this.workExperience[i] );
            }
        }
        this.updateWorkExperienceEndDateDisabled = this.updateWorkExperienceObject.endDate == 'Present' ? true : false;
    }

    /**
     * Update work experience.
     */
    updateWorkExperience() {
        this.savingWorkExperience = true;
        var promise = this._portfolioService.updateWorkExperience( this.updateWorkExperienceObject );
        promise.then(( response ) => {
            this.savingWorkExperience = false;
            this.workExperienceEndDateRequired = true;
            this.updateWorkExperienceEndDateDisabled = true;
            this.showWorkExperienceInputs = false;
            this.showUpdateWorkExperienceInputs = false;
            this.selectedWorkEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.workExperience.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.workExperience[i].id == this.updateWorkExperienceObject.id ) {
                    this.workExperience[i] = this.updateWorkExperienceObject;
                }
            }
        }, ( reason ) => {
            this.savingWorkExperience = false;
            console.error( reason );
            alert( "Failed to update work experience. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert work experience.
     */
    insertWorkExperience( work: FormGroup ) {
        if ( !work.valid ) {
            return false;
        }
        this.savingWorkExperience = true;
        var newObject = cloneDeep( this.workExperienceObject );
        var promise = this._portfolioService.insertWorkExperience( newObject );
        promise.then(( response ) => {
            this.savingWorkExperience = false;
            this.workExperienceEndDateRequired = true;
            this.showWorkExperienceInputs = false;
            this.workExperience.push( response );

            // clear form data
            this.workExperienceObject = {
                companyName: undefined,
                jobTitle: undefined,
                startDate: undefined,
                endDate: undefined,
                jobDescription: undefined,
                id: undefined
            };
        }, ( reason ) => {
            this.savingWorkExperience = false;
            console.error( reason );
            alert( "Failed to save work experience. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete work experience
     */
    deleteWorkExperience( id ) {
        var promise = this._portfolioService.deleteWorkExperience(  id );
        promise.then(( response ) => {
            // remove work experience locally
            var length = this.workExperience.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.workExperience[i].id == id ) {
                    this.workExperience.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateWorkExperienceInputs = false;
            this.showWorkExperienceInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete work experience. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Education
     */
    gpaRegEx = '\^((\d+\.?|\.(?=\d))?\d{0,3})$';
    showEducationInputs = false;
    savingEducation = false;
    educationObject;
    updateEducationObject;
    showUpdateEducationInputs = false;
    selectedEducationEditIndex;

    /**
     * Setup for updating education input.
     */
    setupUpdateEducation = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedEducationEditIndex = index;
        this.showEducationInputs = true;
        this.showUpdateEducationInputs = true;
        var length = this.education.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.education[i].id == id ) {
                this.updateEducationObject = cloneDeep( this.education[i] );
            }
        }
    }

    /**
     * Update education.
     */
    updateEducation() {
        this.savingEducation = true;
        var promise = this._portfolioService.updateEducation( this.updateEducationObject );
        promise.then(( response ) => {
            this.savingEducation = false;
            // this.workExperienceEndDateRequired = true;
            this.showEducationInputs = false;
            this.showUpdateEducationInputs = false;
            this.selectedEducationEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.education.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.education[i].id == this.updateEducationObject.id ) {
                    this.education[i] = this.updateEducationObject;
                }
            }
        }, ( reason ) => {
            this.savingEducation = false;
            console.error( reason );
            alert( "Failed to update education. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert education.
     */
    insertEducation() {
        this.savingEducation = true;
        var newObject = cloneDeep( this.educationObject );
        var promise = this._portfolioService.insertEducation( newObject );
        promise.then(( response ) => {
            this.savingEducation = false;
            this.showEducationInputs = false;
            this.education.push( response );

            // clear form data
            this.educationObject = {
                schoolName: undefined,
                gpa: undefined,
                startDate: undefined,
                endDate: undefined,
                activities: undefined,
                achievements: undefined,
                id: undefined
            };

        }, ( reason ) => {
            this.savingEducation = false;
            console.error( reason );
            alert( "Failed to save education data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete education
     */
    deleteEducation( id ) {
        var promise = this._portfolioService.deleteEducation( id );
        promise.then(( response ) => {
            // remove education locally
            var length = this.education.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.education[i].id == id ) {
                    this.education.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateEducationInputs = false;
            this.showEducationInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete education data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Achievements
     */
    showAchievementInputs = false;
    savingAchievement = false;
    achievementObject;
    updateAchievementObject;
    showUpdateAchievementInputs = false;
    selectedAchievementEditIndex;

    /**
     * Setup for updating achievement input.
     */
    setupUpdateAchievement = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedAchievementEditIndex = index;
        this.showAchievementInputs = true;
        this.showUpdateAchievementInputs = true;
        var length = this.achievement.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.achievement[i].id == id ) {
                this.updateAchievementObject = cloneDeep( this.achievement[i] );
            }
        }
    }

    /**
     * Update education.
     */
    updateAchievement() {
        this.savingAchievement = true;
        var promise = this._portfolioService.updateAchievement( this.updateAchievementObject );
        promise.then(( response ) => {
            this.savingAchievement = false;
            this.showAchievementInputs = false;
            this.showUpdateAchievementInputs = false;
            this.selectedAchievementEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.achievement.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.achievement[i].id == this.updateAchievementObject.id ) {
                    this.achievement[i] = this.updateAchievementObject;
                }
            }
        }, ( reason ) => {
            this.savingAchievement = false;
            console.error( reason );
            alert( "Failed to update achievement. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert achievement.
     */
    insertAchievement() {
        this.savingAchievement = true;
        var newObject = cloneDeep( this.achievementObject );
        var promise = this._portfolioService.insertAchievement( newObject );
        promise.then(( response ) => {
            this.savingAchievement = false;
            this.showAchievementInputs = false;
            this.achievement.push( response );

            // clear form data
            this.achievementObject = {
                description: undefined
            };

        }, ( reason ) => {
            this.savingAchievement = false;
            console.error( reason );
            alert( "Failed to save achievement data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete achievement
     */
    deleteAchievement( id ) {
        var promise = this._portfolioService.deleteAchievement( id );
        promise.then(( response ) => {
            // remove achievement locally
            var length = this.achievement.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.achievement[i].id == id ) {
                    this.achievement.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateAchievementInputs = false;
            this.showAchievementInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete achievement data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Interests.
     */
    interestSelected = 'interest';
    showInterestInputs = false;
    savingInterest = false;
    interestObject;
    updateInterestObject;
    showUpdateInterestInputs = false;
    selectedInterestEditIndex;

    /**
     * Setup for updating interest input.
     */
    setupInterestSkill = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedInterestEditIndex = index;
        this.showInterestInputs = true;
        this.showUpdateInterestInputs = true;
        var length = this.interest.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.interest[i].id == id ) {
                this.updateInterestObject = cloneDeep( this.interest[i] );
            }
        }
    }

    /**
     * Update interest.
     */
    updateInterest() {
        this.savingInterest = true;
        var promise = this._portfolioService.updateInterest( this.updateInterestObject );
        promise.then(( response ) => {
            this.savingInterest = false;
            this.showInterestInputs = false;
            this.showUpdateInterestInputs = false;
            this.selectedInterestEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.interest.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.interest[i].id == this.updateInterestObject.id ) {
                    this.interest[i] = this.updateInterestObject;
                }
            }
        }, ( reason ) => {
            this.savingInterest = false;
            console.error( reason );
            alert( "Failed to update interest. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert interest.
     */
    insertInterest() {
        this.savingInterest = true;
        var newObject = cloneDeep( this.interestObject );
        var promise = this._portfolioService.insertInterest( newObject );
        promise.then(( response ) => {
            this.savingInterest = false;
            this.showInterestInputs = false;
            this.interest.push( response );

            // clear form data
            this.interestObject = {
                description: undefined
            };

        }, ( reason ) => {
            this.savingInterest = false;
            console.error( reason );
            alert( "Failed to save interest data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete interest.
     */
    deleteInterest( id ) {
        var promise = this._portfolioService.deleteInterest( id );
        promise.then(( response ) => {
            // remove interest locally
            var length = this.interest.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.interest[i].id == id ) {
                    this.interest.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateInterestInputs = false;
            this.showInterestInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete interest data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Skills.
     */
    showSkillInputs = false;
    savingSkill = false;
    skillObject;
    updateSkillObject;
    showUpdateSkillInputs = false;
    selectedSkillsEditIndex = undefined;

    /**
     * Setup for updating skill input.
     */
    setupUpdateSkill = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedSkillsEditIndex = index;
        this.showSkillInputs = true;
        this.showUpdateSkillInputs = true;
        var length = this.skill.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.skill[i].id == id ) {
                this.updateSkillObject = cloneDeep( this.skill[i] );
            }
        }
    }

    /**
     * Update skill.
     */
    updateSkill() {
        this.savingSkill = true;
        var promise = this._portfolioService.updateSkill( this.updateSkillObject );
        promise.then(( response ) => {
            this.savingSkill = false;
            this.showSkillInputs = false;
            this.showUpdateSkillInputs = false;
            this.selectedSkillsEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.skill.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.skill[i].id == this.updateSkillObject.id ) {
                    this.skill[i] = this.updateSkillObject;
                }
            }
        }, ( reason ) => {
            this.savingSkill = false;
            console.error( reason );
            alert( "Failed to update skill. Try again. If error persist, please contact site administrator." );
        } );
        
    }

    /*
     * Insert skills.
     */
    insertSkill() {
        this.savingSkill = true;
        var newObject = cloneDeep( this.skillObject );
        var promise = this._portfolioService.insertSkill( newObject );
        promise.then(( response ) => {
            this.savingSkill = false;
            this.showSkillInputs = false;
            this.skill.push( response );

            // clear form data
            this.skillObject = {
                description: undefined
            };

        }, ( reason ) => {
            this.savingSkill = false;
            console.error( reason );
            alert( "Failed to save skill data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete skill.
     */
    deleteSkill( id ) {
        var promise = this._portfolioService.deleteSkill( id );
        promise.then(( response ) => {
            // remove skill locally
            var length = this.skill.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.skill[i].id == id ) {
                    this.skill.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateSkillInputs = false;
            this.showSkillInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete skill data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Work Values.
     */
    showWorkValueInputs = false;
    savingWorkValue = false;
    workValueObject;
    updateWorkValueObject;
    showUpdateWorkValueInputs = false;
    selectedValuesEditIndex;

    /**
     * Setup for updating work value input.
     */
    setupUpdateWorkValue = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedValuesEditIndex = index;
        this.showWorkValueInputs = true;
        this.showUpdateWorkValueInputs = true;
        var length = this.workValue.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.workValue[i].id == id ) {
                this.updateWorkValueObject = cloneDeep( this.workValue[i] );
            }
        }
    }

    /**
     * Update work value.
     */
    updateWorkValue() {
        this.savingWorkValue = true;
        var promise = this._portfolioService.updateWorkValue( this.updateWorkValueObject );
        promise.then(( response ) => {
            this.savingWorkValue = false;
            this.showWorkValueInputs = false;
            this.showUpdateWorkValueInputs = false;
            this.selectedValuesEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.workValue.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.workValue[i].id == this.updateWorkValueObject.id ) {
                    this.workValue[i] = this.updateWorkValueObject;
                }
            }
        }, ( reason ) => {
            this.savingWorkValue = false;
            console.error( reason );
            alert( "Failed to update work value. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert work value.
     */
    insertWorkValue() {
        this.savingWorkValue = true;
        var newObject = cloneDeep( this.workValueObject );
        var promise = this._portfolioService.insertWorkValue( newObject );
        promise.then(( response ) => {
            this.savingWorkValue = false;
            this.showWorkValueInputs = false;
            this.workValue.push( response );

            // clear form data
            this.workValueObject = {
                description: undefined
            };

        }, ( reason ) => {
            this.savingWorkValue = false;
            console.error( reason );
            alert( "Failed to save work value data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete work value.
     */
    deleteWorkValue( id ) {
        var promise = this._portfolioService.deleteWorkValue( id );
        promise.then(( response ) => {
            // remove work value locally
            var length = this.workValue.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.workValue[i].id == id ) {
                    this.workValue.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateWorkValueInputs = false;
            this.showWorkValueInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete work value data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Volunteer
     */
    showVolunteerInputs = false;
    savingVolunteer = false;
    volunteerEndDateDisabled = true;
    updateVolunteerEndDateDisabled = true;
    volunteerObject;
    updateVolunteerObject;
    showUpdateVolunteerInputs = false;
    selectedVolunteerEditIndex;

    /**
     * Setup for updating volunteer input.
     */
    setupUpdateVolunteer( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedVolunteerEditIndex = index;
        this.showVolunteerInputs = true;
        this.showUpdateVolunteerInputs = true;
        var length = this.volunteer.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.volunteer[i].id == id ) {
                this.updateVolunteerObject = cloneDeep( this.volunteer[i] );
            }
        }
        this.updateVolunteerEndDateDisabled = this.updateVolunteerObject.endDate == 'Present' ? true : false;
    }

    /**
     * Update volunteer.
     */
    updateVolunteer() {
        this.savingVolunteer = true;
        var promise = this._portfolioService.updateVolunteer( this.updateVolunteerObject );
        promise.then(( response ) => {
            this.savingVolunteer = false;
            this.showVolunteerInputs = false;
            this.showUpdateVolunteerInputs = false;
            this.updateVolunteerEndDateDisabled = true;
            this.selectedVolunteerEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.volunteer.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.volunteer[i].id == this.updateVolunteerObject.id ) {
                    this.volunteer[i] = this.updateVolunteerObject;
                }
            }
        }, ( reason ) => {
            this.savingVolunteer = false;
            console.error( reason );
            alert( "Failed to update volunteer. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert volunteer.
     */
    insertVolunteer() {
        this.savingVolunteer = true;
        var newObject = cloneDeep( this.volunteerObject );
        var promise = this._portfolioService.insertVolunteer( newObject );
        promise.then(( response ) => {
            this.savingVolunteer = false;
            this.showVolunteerInputs = false;
            this.volunteerEndDateDisabled = true;
            this.volunteer.push( response );

            // clear form data
            this.volunteerObject = {
                organizationName: undefined,
                startDate: undefined,
                endDate: undefined,
                description: undefined
            };

        }, ( reason ) => {
            this.savingVolunteer = false;
            console.error( reason );
            alert( "Failed to save volunteer work. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete volunteer.
     */
    deleteVolunteer( id ) {
        var promise = this._portfolioService.deleteVolunteer( id );
        promise.then(( response ) => {
            // remove volunteer locally
            var length = this.volunteer.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.volunteer[i].id == id ) {
                    this.volunteer.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateVolunteerInputs = false;
            this.showVolunteerInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete volunteer data. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Test scores.
     */
    showScoreInputs = false; // used for all scores
    testSelected;
    testEditDisable = false;
    showUpdateFyiScoreInputs = false;
    showUpdateAsvabScoreInputs = false;
    showUpdateActScoreInputs = false;
    showUpdateSatScoreInputs = false;
    showUpdateOtherScoreInputs = false;
    selectedASVABEditIndex;

    // reset so only one section is open at a time.
    resetScoreEditFlags() {
        this.showUpdateFyiScoreInputs = false;
        this.showUpdateAsvabScoreInputs = false;
        this.showUpdateActScoreInputs = false;
        this.showUpdateSatScoreInputs = false;
        this.showUpdateOtherScoreInputs = false;
    }

    /**
     * ACT
     */
    savingActScore = false;
    actObject;
    updateActObject;
    selectedACTEditIndex;

    /**
     * Setup for updating ACT input.
     */
    setupUpdateActScore( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedACTEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateActScoreInputs = true;
        var length = this.actScore.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.actScore[i].id == id ) {
                this.updateActObject = cloneDeep( this.actScore[i] );
                break;
            }
        }
    }

    /**
     * Update ACT.
     */
    updateActScore() {
        this.savingActScore = true;
        var promise = this._portfolioService.updateActScore( this.updateActObject );
        promise.then(( response ) => {
            this.savingActScore = false;
            this.showScoreInputs = false;
            this.showUpdateActScoreInputs = false;
            this.selectedACTEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.testEditDisable = false;
            this.testSelected = undefined;
            var length = this.actScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.actScore[i].id == this.updateActObject.id ) {
                    this.actScore[i] = this.updateActObject;
                    break;
                }
            }
        }, ( reason ) => {
            this.savingActScore = false;
            console.error( reason );
            alert( "Failed to update ACT score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert ACT score.
     */
    insertActScore() {
        this.savingActScore = true;
        var newObject = cloneDeep( this.actObject );
        var promise = this._portfolioService.insertActScore( newObject );
        promise.then(( response ) => {
            this.savingActScore = false;
            this.showScoreInputs = false;
            this.actScore.push( response );

            // clear form data
            this.actObject = {
                readingScore: undefined,
                mathScore: undefined,
                writingScore: undefined,
                englishScore: undefined,
                scienceScore: undefined
            };
            this.testSelected = undefined;

        }, ( reason ) => {
            this.savingActScore = false;
            console.error( reason );
            alert( "Failed to save ACT score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete ACT score.
     */
    deleteActScore( id ) {
        var promise = this._portfolioService.deleteActScore( id );
        promise.then(( response ) => {
            // remove work value locally
            var length = this.actScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.actScore[i].id == id ) {
                    this.actScore.splice( i, 1 );
                    break;
                }
            }
            this.testSelected = undefined;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete ACT score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * ASVAB
     */
    haveAsvabTestScores = false;
    asvabTestScores = {
        verbalScore: undefined,
        mathScore: undefined,
        scienceScore: undefined,
        afqtScore: undefined
    };
    afqtScore;



    savingAsvabScore = false;
    asvabObject = {
        verbalScore: undefined,
        mathScore: undefined,
        afqtScore: undefined,
        scienceScore: undefined
    };
    updateAsvabObject;

    /**
     * Setup for updating ASVAB input.
     */
    setupUpdateAsvabScore( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedASVABEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateAsvabScoreInputs = true;
        var length = this.asvabScore.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.asvabScore[i].id == id ) {
                this.updateAsvabObject = cloneDeep( this.asvabScore[i] );
                break;
            }
        }
    }

    /**
     * Update ASVAB.
     */
    updateAsvabScore() {
        this.savingAsvabScore = true;
        window.sessionStorage.setItem( 'manualScores', 'true' );
        window.sessionStorage.setItem( 'sV', this.asvabObject.verbalScore );
        window.sessionStorage.setItem( 'sM', this.asvabObject.mathScore );
        window.sessionStorage.setItem( 'sS', this.asvabObject.scienceScore );
        window.sessionStorage.setItem( 'sA', this.asvabObject.afqtScore );

        let self = this;
        var promise = this._portfolioService.updateAsvabScore( this.updateAsvabObject );
        promise.then( function( response ) {
            self.savingAsvabScore = false;
            self.showScoreInputs = false;
            self.showUpdateAsvabScoreInputs = false;
            self.selectedASVABEditIndex = undefined;
            self.myStyle = {
                'cursor': 'pointer'
            };
            self.testEditDisable = false;
            self.testSelected = undefined;
            var length = self.asvabScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( self.asvabScore[i].id == self.updateAsvabObject.id ) {
                    self.asvabScore[i] = self.updateAsvabObject;
                    break;
                }
            }

        }, function( reason ) {
            self.savingAsvabScore = false;
            console.error( reason );
            alert( "Failed to update ASVAB score. Try again. If error persist, please contact site administrator." );
        });
    }

    /*
     * Insert ASVAB score.
     */
    insertAsvabScore(){
        this.savingAsvabScore = true;
        window.sessionStorage.setItem( 'manualScores', 'true' );
        window.sessionStorage.setItem( 'sV', this.asvabObject.verbalScore );
        window.sessionStorage.setItem( 'sM', this.asvabObject.mathScore );
        window.sessionStorage.setItem( 'sS', this.asvabObject.scienceScore );
        window.sessionStorage.setItem( 'sA', this.asvabObject.afqtScore );

        var newObject = cloneDeep( this.asvabObject );
        let self = this;
        var promise = this._portfolioService.insertAsvabScore( newObject );
        promise.then( function( response ) {
            self.savingAsvabScore = false;
            self.showScoreInputs = false;
            self.asvabScore.push( response );

            // clear form data
            self.asvabObject = {
                verbalScore: undefined,
                mathScore: undefined,
                afqtScore: undefined,
                scienceScore: undefined
            };
            self.testSelected = undefined;
        }, function( reason ) {
            self.savingAsvabScore = false;
            console.error( reason );
            alert( "Failed to save ASVAB score. Try again. If error persist, please contact site administrator." );
        });
    }

    /*
     * Delete ASVAB score.
     */
    deleteAsvabScore( id ) {
        let self = this;

        var promise = this._portfolioService.deleteAsvabScore( id );
        promise.then( function( response ) {
            window.sessionStorage.setItem( 'manualScores', 'false' );
            // remove work value locally
            var length = self.asvabScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( self.asvabScore[i].id == id ) {
                    self.asvabScore.splice( i, 1 );
                    break;
                }
            }
            self.testSelected = undefined;
        }, function( reason ) {
            console.error( reason );
            alert( "Failed to delete ASVAB score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * SAT
     */
    savingSatScore = false;
    satObject;
    updateSatObject;
    showSatInputs = false;
    selectedSATEditIndex;

    /**
     * Setup for updating SAT input.
     */
    setupUpdateSatScore( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedSATEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateSatScoreInputs = true;
        var length = this.satScore.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.satScore[i].id == id ) {
                this.updateSatObject = cloneDeep( this.satScore[i] );
                break;
            }
        }
    }

    /**
     * Update SAT.
     */
    updateSatScore() {
        this.savingSatScore = true;
        var promise = this._portfolioService.updateSatScore( this.updateSatObject );
        promise.then(( response ) => {
            this.savingSatScore = false;
            this.showSatInputs = false;
            this.showUpdateSatScoreInputs = false;
            this.testSelected = undefined;
            this.selectedSATEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.testEditDisable = false;
            this.testSelected = undefined;
            var length = this.satScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.satScore[i].id == this.updateSatObject.id ) {
                    this.satScore[i] = this.updateSatObject;
                    break;
                }
            }
        }, ( reason ) => {
            this.savingSatScore = false;
            console.error( reason );
            alert( "Failed to update SAT score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert SAT score.
     */
    insertSatScore() {
        this.savingSatScore = true;
        var newObject = cloneDeep( this.satObject );
        var promise = this._portfolioService.insertSatScore( newObject );
        promise.then(( response ) => {
            this.savingSatScore = false;
            this.showScoreInputs = false;
            this.satScore.push( response );

            // clear form data
            this.satObject = {
                readingScore: undefined,
                mathScore: undefined,
                writingScore: undefined,
                subject: undefined
            };

            this.testSelected = undefined;

        }, ( reason ) => {
            this.savingSatScore = false;
            console.error( reason );
            alert( "Failed to save SAT score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete SAT score.
     */
    deleteSatScore( id ) {
        var promise = this._portfolioService.deleteSatScore( id );
        promise.then(( response ) => {
            // remove work value locally
            var length = this.satScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.satScore[i].id == id ) {
                    this.satScore.splice( i, 1 );
                    break;
                }
            }
            this.testSelected = undefined;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete SAT score. Try again. If error persist, please contact site administrator." );
        } );
    }




    interestCodesInfo() {
        var score = this.scoreChoice == 'gender' ? 'Gender-Specific' : 'Combined';
        var modalOptions = {
            title: 'Interest Codes',
            message: "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
        };

        const dialogRef1 = this._dialog.open( MessageDialogComponent, {
            data: modalOptions,
            maxWidth: '400px'
        });
    }

    savingFyiScore = false;
    fyiObject;
    updateFyiObject;
    showFyiInputs = false;
    selectedFYIEditIndex;

    /**
     * Setup for updating FYI input.
     */
    setupUpdateFyiScore = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedFYIEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateFyiScoreInputs = true;
        var length = this.fyiScore.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.fyiScore[i].id == id ) {
                this.updateFyiObject = cloneDeep( this.fyiScore[i] );
                break;
            }
        }
    }

    /**
     * Update FYI.
     */
    updateFyiScore() {
        this.savingFyiScore = true;
        var promise = this._portfolioService.updateFyiScore( this.updateFyiObject );
        promise.then(( response ) => {
            this.savingFyiScore = false;
            this.showFyiInputs = false;
            this.showUpdateFyiScoreInputs = false;
            this.selectedFYIEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.testEditDisable = false;

            var length = this.fyiScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.fyiScore[i].id == this.updateFyiObject.id ) {
                    this.fyiScore[i] = this.updateFyiObject;
                    break;
                }
            }
        }, ( reason ) => {
            this.savingFyiScore = false;
            console.error( reason );
            alert( "Failed to update FYI score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert FYI score.
     */
    insertFyiScore() {
        this.savingFyiScore = true;
        var newObject = cloneDeep( this.fyiObject );
        var promise = this._portfolioService.insertFyiScore( newObject );
        promise.then(( response ) => {
            this.savingFyiScore = false;
            this.showInterestInputs = false;
            this.interestSelected = 'interest';
            this.fyiScore.push( response );

            // clear form data
            this.fyiObject = {
                interest_code_one: undefined,
                interest_code_two: undefined,
                interest_code_three: undefined
            }

        }, ( reason ) => {
            this.savingFyiScore = false;
            console.error( reason );
            alert( "Failed to save FYI score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete FYI score.
     */
    deleteFyiScore( id ) {
        var promise = this._portfolioService.deleteFyiScore( id );
        promise.then(( response ) => {
            // remove work value locally
            var length = this.fyiScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.fyiScore[i].id == id ) {
                    this.fyiScore.splice( i, 1 );
                    break;
                }
            }
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete FYI score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Other test scores.
     */
    savingOtherScore = false;
    otherObject;
    updateOtherObject;
    showOtherInputs = false;
    selectedOtherEditIndex;

    /**
     * Setup for updating other input.
     */
    setupUpdateOtherScore = function( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedOtherEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateOtherScoreInputs = true;
        var length = this.otherScore.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.otherScore[i].id == id ) {
                this.updateOtherObject = cloneDeep( this.otherScore[i] );
                break;
            }
        }
    }

    /**
     * Update other scores.
     */
    updateOtherScore() {
        this.savingOtherScore = true;
        var promise = this._portfolioService.updateOtherScore( this.updateOtherObject );
        promise.then(( response ) => {
            this.savingOtherScore = false;
            this.showOtherInputs = false;
            this.showUpdateOtherScoreInputs = false;
            this.selectedOtherEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.testEditDisable = false;

            var length = this.otherScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.otherScore[i].id == this.updateOtherObject.id ) {
                    this.otherScore[i] = this.updateOtherObject;
                    break;
                }
            }
        }, ( reason ) => {
            this.savingOtherScore = false;
            console.error( reason );
            alert( "Failed to update other score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Insert other scores.
     */
    insertOtherScore() {
        this.savingOtherScore = true;
        var newObject = cloneDeep( this.otherObject );
        var promise = this._portfolioService.insertOtherScore( newObject );
        promise.then(( response ) => {
            this.savingOtherScore = false;
            this.showScoreInputs = false;
            this.otherScore.push( response );

            // clear form data
            this.otherObject = {
                description: undefined
            };

        }, ( reason ) => {
            this.savingOtherScore = false;
            console.error( reason );
            alert( "Failed to save score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /*
     * Delete other score.
     */
    deleteOtherScore( id ) {
        var promise = this._portfolioService.deleteOtherScore( id );
        promise.then(( response ) => {
            // remove work value locally
            var length = this.otherScore.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.otherScore[i].id == id ) {
                    this.otherScore.splice( i, 1 );
                    break;
                }
            }
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete other score. Try again. If error persist, please contact site administrator." );
        } );
    }

    /**
     * Plan of action implementation
     */
    showPlanInputs = false;
    savingPlan:boolean = false;
    planObject;
    updatePlanObject;
    selectedPlanEditIndex;
    showUpdatePlanInputs;

    /**
     * Setup for updating plan of interest input.
     */
    setupPlanSkill( id, index ) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedPlanEditIndex = index;
        this.showPlanInputs = true;
        this.showUpdatePlanInputs = true;
        var length = this.plan.length;
        for ( var i = 0; i < length; i++ ) {
            if ( this.plan[i].id == id ) {
                this.updatePlanObject = cloneDeep( this.plan[i] );
            }
        }
    }

    	/*
	 * Add plan of action.
	 */
	addCombinedPlan() {
		this.savingPlan = true;
		var newObject = cloneDeep(this.combinedPlanObj);
		newObject.plan = JSON.stringify(this.combinedPlanObj.plan);

        let self = this;
		var promise = this._portfolioService.insertPlan(newObject);
		promise.then((response) => {
			self.originalPlanObj = cloneDeep(self.combinedPlanObj);
			self.plan.forEach(function(p) {
				self.deletePlan(p.id);
			})
			self.savingPlan = false;
		}, (reason) => {
			self.savingPlan = false;
			console.error(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		});
	};

	/*
	 * Update plan of action.
	 */
	updateCombinedPlan(){
		this.savingPlan = true;
		var newObject = cloneDeep(this.combinedPlanObj);
		newObject.plan = JSON.stringify(this.combinedPlanObj.plan,
			Object.keys(this.combinedPlanObj.plan));

		var promise = this._portfolioService.updatePlan(newObject);
		promise.then((response) => {
			this.originalPlanObj = cloneDeep(this.combinedPlanObj);
			this.savingPlan = false;
		}, (reason) => {
			this.savingPlan = false;
			console.error(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		});
    };

    /*
     * Delete plan of action.
     */
    deletePlan( id ) {
        var promise = this._portfolioService.deletePlan( id );
        promise.then(( response ) => {
            // remove plan locally
            var length = this.plan.length;
            for ( var i = 0; i < length; i++ ) {
                if ( this.plan[i].id == id ) {
                    this.plan.splice( i, 1 );
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdatePlanInputs = false;
            this.showPlanInputs = false;
        }, ( reason ) => {
            console.error( reason );
            alert( "Failed to delete plan of action data. Try again. If error persist, please contact site administrator." );
        } );
    }
    

    /**
     * Print page as pdf. Utilizes pdfmake library.
     */
    docDefinition;

    /*
     * function toDataUrl(url, callback, outputFormat){ var img = new Image();
     * img.crossOrigin = 'Anonymous'; img.onload = function(){ var canvas =
     * document.createElement('CANVAS'); var ctx = canvas.getContext('2d'); var
     * dataURL; canvas.height = this.height; canvas.width = this.width;
     * ctx.drawImage(this, 0, 0); dataURL = canvas.toDataURL(outputFormat);
     * callback(dataURL); canvas = null; }; img.src = url; }
     * 
     * toDataUrl('images/logo.png', function(base64Img){ logoImg = base64Img;
     * console.log(logoImg); });
     */

    /**
     * Setup PDF and print content.
     */
    setupContent() {

        var content = [];

        // User input for name and grade.
        var nameAndGrade = [{
            columns: [{
                text: 'Name: ' + this.fullName,
                alignment: 'left',
                fontSize: 18,
                bold: true
            }, {
                text: 'Grade: ' + this.grade,
                alignment: 'right',
                fontSize: 18,
                bold: true
            }]
        }, {
            canvas: [{
                type: 'line',
                x1: 0,
                y1: 5,
                x2: 595 - 2 * 40,
                y2: 5,
                lineWidth: 1
            }],
            style: 'horizontalLine'
        }];
        content.push( nameAndGrade );

        // future plan section
        if ( this.planPrint ) {
            var len = this.plan.length;
            var planObject = [];

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Future Plan:\n\n',
                    style: 'sectionTitle'
                } )
                
                for ( var i = 0; i < len; i++ ) {
                	var plan = this.combinedPlanObj.plan;
                	for(const property in plan){
                		if(plan[property]){
                			
	                		if(property == "fourYearCollege")
	                			planObject.push({text: "Bachelor's Degree (4 year)"});
	                		else if(property == "twoYearCollege")
	                			planObject.push({text: "Associate's Degree(2 year)"});
	                		else if(property == "careerAndTech")
	                			planObject.push({text: "Career and Technical Education"});
	                		else if(property == "military")
	                			planObject.push({text: "Military"});
	                		else if(property == "work")
	                			planObject.push({text: "Work Based Learning"});
	                		else if(property == "gapYear")
	                			planObject.push({text: "GAP Year"});
	                		else if(property == "undecided")
	                			planObject.push({text: "Undecided"});
	                		else if(property == "certificate")
	                			planObject.push({text: "Certificate"});
	                		else if(property == "technicalSchool")
	                			planObject.push({text: "Technical School"});
	                		else if(property == "vocationalTraining")
	                			planObject.push({text: "Vocational Training"});
	                		else if (property == "advancedDegree")
	                			planObject.push({text: "Advanced Degree"});
	                			
                		}
                	}
                	content.push(planObject);
                    content.push( [{
                        text: this.combinedPlanObj.description == null ? '' : this.combinedPlanObj.description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Work experience content.
        if ( this.workExperiencePrint ) {
            var len = this.workExperience.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Work Experience:\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: [{
                            text: '\n' + this.workExperience[i].jobTitle + ', ',
                            bold: true
                        }, this.workExperience[i].companyName]
                    }, {
                        text: this.workExperience[i].startDate + ' - ' + this.workExperience[i].endDate
                    }, {
                        text: this.workExperience[i].jobDescription
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Education content.
        if ( this.educationPrint ) {
            var len = this.education.length;
            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Education:\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: '\n' + this.education[i].schoolName + ', ',
                        bold: true
                    }, {
                        text: this.education[i].startDate + ' - ' + this.education[i].endDate
                    }, {
                        text: this.education[i].activities == null ? '' : this.education[i].activities
                    }, {
                        text: this.education[i].achievements == null ? '' : this.education[i].achievements
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Test score content.
        if ( this.testScorePrint ) {
            var actLen = this.actScore.length;
            var asvabLen = this.asvabScore.length;
            var satLen = this.satScore.length;
            var fyiLen = this.fyiScore.length;
            var otherLen = this.otherScore.length;

            if ( actLen > 0 || asvabLen > 0 || satLen > 0 || fyiLen > 0 || otherLen > 0 ) {
                // section title
                content.push( {
                    text: 'Test Score:\n',
                    style: 'sectionTitle'
                } );
            }

            // ACT score content
            if ( actLen > 0 ) {

                for ( var i = 0; i < actLen; i++ ) {

                    content.push( [{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'ACT Score',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Reading Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].readingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Math Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].mathScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Writing Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].writingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Science Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].scienceScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'English Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].englishScore,
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }] );
                }
            }

            // ASVAB score content
            if ( asvabLen > 0 ) {

                for ( var i = 0; i < asvabLen; i++ ) {

                    /*
                     * content.push([ { text : 'Verbal Score:' +
                     * this.asvabScore[i].verbalScore },{ text : 'Math Score:' +
                     * this.asvabScore[i].mathScore },{ text : 'Science
                     * Score:' + this.asvabScore[i].scienceScore },{ text :
                     * 'AFQT Score:' + this.asvabScore[i].afqtScore } ]);
                     */

                    content.push( [{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'ASVAB Score',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Verbal Skills Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.asvabScore[i].verbalScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Math Skills Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.asvabScore[i].mathScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Science and Technical Skills Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.asvabScore[i].scienceScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'AFQT Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.asvabScore[i].afqtScore,
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }] );
                }
            }

            // SAT score content
            if ( satLen > 0 ) {

                for ( var i = 0; i < satLen; i++ ) {

                    /*
                     * content.push([ { text : 'Reading Score:' +
                     * this.satScore[i].readingScore },{ text : 'Math Score:' +
                     * this.satScore[i].mathScore },{ text : 'Writing Score:' +
                     * this.satScore[i].writingScore },{ text :
                     * this.satScore[i].subject != null ? 'Subject Score:' +
                     * this.satScore[i].subject : '' } ]);
                     */

                    content.push( [{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'SAT Score',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Critical Reading',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.satScore[i].readingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Mathematics',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.satScore[i].mathScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Writing',
                                style: 'tableStyle'
                            }, {
                                text: this.satScore[i].writingScore != 0 ? + this.satScore[i].writingScore : 'None',
                                style: 'tableStyle'
                            }], [{
                                text: 'Other Subject Score',
                                style: 'tableStyle'
                            }, {
                                text: this.satScore[i].subject != null ? 'Subject Score:' + this.satScore[i].subject : 'None',
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }] );
                }
            }

            // FYI score content
            if ( fyiLen > 0 ) {

                content.push( {
                    text: '\nFYI Score:\n',
                    style: 'sectionTitle'
                } )

                for ( var i = 0; i < fyiLen; i++ ) {

                    /*
                     * content.push([ { text : 'Interest Code One:' +
                     * this.fyiScore[i].interestCodeOne },{ text : 'Interest
                     * Code Two:' + this.fyiScore[i].interestCodeTwo },{ text :
                     * 'Interest Code Three:' +
                     * this.fyiScore[i].interestCodeThree } ]);
                     */

                    var scoreTextOne = this.fyiScore[i].interestCodeOne == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeOne == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeOne == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeOne == 'S' ? 'Social' : this.fyiScore[i].interestCodeOne == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeOne == 'I' ? 'Investigative' : '';
                    var scoreTextTwo = this.fyiScore[i].interestCodeTwo == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeTwo == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeTwo == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeTwo == 'S' ? 'Social' : this.fyiScore[i].interestCodeTwo == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeTwo == 'I' ? 'Investigative' : '';
                    var scoreTextThree = this.fyiScore[i].interestCodeThree == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeThree == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeThree == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeThree == 'S' ? 'Social' : this.fyiScore[i].interestCodeThree == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeThree == 'I' ? 'Investigative' : '';

                    content.push( [{
                        text: this.fyiScore[i].interestCodeOne + ' | ' + this.fyiScore[i].interestCodeTwo + ' | ' + this.fyiScore[i].interestCodeThree + ' (' + scoreTextOne + ' | ' + scoreTextTwo + ' | ' + scoreTextThree + ')',
                        style: 'tableStyle'
                    }] );
                }
            }

            // Other score content
            if ( otherLen > 0 ) {

                for ( var i = 0; i < otherLen; i++ ) {

                    content.push( [{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'Other Scores',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Other Scores',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.otherScore[i].description,
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }] );
                }
            }

            content.push( {
                canvas: [{
                    type: 'line',
                    x1: 0,
                    y1: 5,
                    x2: 595 - 2 * 40,
                    y2: 5,
                    lineWidth: 1
                }],
                style: 'horizontalLine'
            } )

        }

        // Achievement content.
        if ( this.achievementPrint ) {
            var len = this.achievement.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Achievement:\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: '\nDescription:',
                        bold: true
                    }, {
                        text: this.achievement[i].description == null ? '' : this.achievement[i].description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Skill content.
        if ( this.skillsPrint ) {
            var len = this.skill.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Skill:\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: '\nDescription:',
                        bold: true
                    }, {
                        text: this.skill[i].description == null ? '' : this.skill[i].description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Volunteer content.
        if ( this.volunteerPrint ) {
            var len = this.volunteer.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Volunteer:\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: '\n' + this.volunteer[i].organizationName + ', ',
                        bold: true
                    }, {
                        text: this.volunteer[i].startDate + ' - ' + this.volunteer[i].endDate
                    }, {
                        text: this.volunteer[i].description == null ? '' : this.volunteer[i].description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Interests content.
        if ( this.interestPrint ) {
            var len = this.interest.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Interest:\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.interest[i].description == null ? '' : this.interest[i].description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Work value content.
        if ( this.workValuesPrint ) {
            var len = this.workValue.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: 'Work Value:\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.workValue[i].description == null ? '' : this.workValue[i].description
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Favorite occupation content.
        if ( this.favoriteCitmOccupationPrint ) {
            var len = this.citmFavoriteOccupations.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: len == 1 ? 'Favorite CITM Occupation:\n\n' : 'Favorite CITM Occupations\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.citmFavoriteOccupations[i].title == null ? '' : this.citmFavoriteOccupations[i].title
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Favorite occupation content.
        if ( this.favoriteOccupationPrint ) {
            var len = this.favoriteList.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: len == 1 ? 'Favorite Occupation:\n\n' : 'Favorite Occupations\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.favoriteList[i].title == null ? '' : this.favoriteList[i].title
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Favorite career cluster content.
        if ( this.favoriteCareerClusterPrint ) {
            var len = this.careerClusterFavoriteList.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: len == 1 ? 'Favorite Career Cluster:\n\n' : 'Favorite Career Clusters\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.careerClusterFavoriteList[i].title == null ? '' : this.careerClusterFavoriteList[i].title
                    }] );
                }

                content.push( {
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                } )
            }
        }

        // Favorite school content.
        if ( this.favoriteSchoolPrint ) {
            var len = this.schoolFavoriteList.length;

            if ( len > 0 ) {

                // section title
                content.push( {
                    text: len == 1 ? 'Favorite College:\n\n' : 'Favorite Colleges:\n\n',
                    style: 'sectionTitle'
                } )
                for ( var i = 0; i < len; i++ ) {

                    content.push( [{
                        text: this.schoolFavoriteList[i].schoolName == null ? '' : this.schoolFavoriteList[i].schoolName
                    }] );
                }
            }
        }

        /**
         * Set content.
         */
        // [{text: 'ASVAB Portfolio', style:
        // 'headerTitle', margin: [72,40]},
        this.docDefinition = {
            /*
             * header : [ { image : 'logo', margin : [ 0, 20, 0, 20 ], alignment :
             * 'center' }, headerContent ],
             */
            content: content,

            styles: {
                headerTitle: {
                    fontSize: 22,
                    bold: true,
                    alignment: 'center'
                },
                sectionTitle: {
                    margin: [0, 0, 0, 0],
                    bold: true,
                    alignment: 'left',
                    fontSize: 12
                },
                sectionContent: {
                    // margin: [0, 0, 0, 20]
                },
                tableStyle: {
                    alignment: 'left'
                },
                scoreTable: {
                    margin: [0, 30, 0, 0]
                },
                horizontalLine: {
                    margin: [0, 20, 0, 0]
                }
            }
        };
    }

    printPdf = function() {
        this.setupContent();

        // IE support
        if ( navigator.appName == 'Microsoft Internet Explorer' || !!( navigator.userAgent.match( /Trident/ ) || navigator.userAgent.match( /rv 11/ ) ) ) {
            var modalOptions = {
                headerText: 'Alert',
                bodyText: 'Your browser does not support this feature. Alternatively, you can download as PDF and then print.'
            };

            /*modalService.showModal({
                size : 'sm'
            }, modalOptions);*/
            return false;
        }

        pdfMake.createPdf( this.docDefinition ).print();
    };

    // downloadPdf = function() {
    //     this.setupContent();
    //      pdfMake.createPdf( this.docDefinition ).download( 'ASVAB_Portfolio.pdf' );
    // };

    ga_trackClick(eventTitle) {
        this._googleAnalyticsService.trackClick(eventTitle);
    }
    
    printPdfNew() {

        this.setPrintElements();

        setTimeout(() => this.triggerPrint());

      }

      downloadPdfNew() {

        this.setPrintElements();

        setTimeout(() => this.triggerDownload());

      }
      
      setPrintElements() {
    	    this.printElements.forEach((x: any) => {

    	      

    	    });
    	  }

    	  triggerDownload() {
    	    let t = this.getCanvas();
    	    let self = this;
    	    t.then((s: any) => {

    	      let docDefinition = {
    	        pageOrientation: 'portrait',
    	        pageSize: 'LETTER',
    	        content: s,
    	        pageBreak: 'after'
    	      };

              let fileName = this.studentName.replace(' ', '_') + '_portfolio.pdf';

    	      pdfMake.createPdf(docDefinition).download(fileName);
    	    });

    	    t.catch(error => {
    	      console.error("ERROR", error);
    	      alert(error);
    	    });
    	  }

    	  triggerPrint() {
    	    let t = this.getCanvas();
    	    let self = this;
    	    t.then((s: any) => {

    	      let docDefinition = {
    	        pageOrientation: 'portrait',
    	        pageSize: 'LETTER',
    	        content: s,
    	        pageBreak: 'after'
    	      };

    	      pdfMake.createPdf(docDefinition).print();

    	    });

    	    t.catch(error => {
    	      console.error("ERROR", error);
    	      alert(error);
    	    });
    	  }

    	  getCanvas() {
    	    let self = this;

    	    return new Promise((resolve, reject) => {

    	      html2canvas(self.printSectionElem.nativeElement, {
    	        imageTimeout: 2000,
    	        removeContainer: true,
    	        backgroundColor: '#fff',
    	        allowTaint: true,
    	        logging: true,
    	        scale: 1,
    	        ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
    	          return node.nodeName === 'IFRAME';
    	        },
    	        onclone: function (cloneDoc) {
    	          let printSection: HTMLElement = cloneDoc.getElementById("printSectionNew");
    	          printSection.style.display = 'block';
    	        }
    	      }).then(function (canvas) {

                // mobile height has to match height in getClippedRegion()
    	        let splitAt = self.mobileOrTablet ? 540 : 1200;

    	        let images = [];
    	        let y = 0;
    	        while (canvas.height > y) {
                  const region = self.getClippedRegion(canvas, 0, y, canvas.width, splitAt)

                  // Stop generate image if blank
                  if (!region.image) {
                      break;
                  }

    	          images.push(region);
    	          y += splitAt;
    	        }
    	        resolve(images);

    	      }).catch(function (error) {
    	        reject(error);
    	      });
    	    });

    	  }

    	  getClippedRegion(image, x, y, width, height) {
    	    var canvas = document.createElement("canvas"),
    	      ctx = canvas.getContext("2d");

    	    canvas.width = width;
    	    canvas.height = height;

    	    ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

            let contentLocation = ctx.getImageData(0, 0, canvas.width, canvas.height).data.find(x => x !== 255);

            if (!contentLocation) {
                return { image: undefined };
            }

            let val = {
    	      image: canvas.toDataURL(),
    	      width: 520,
    	      alignment: 'center',
    	    };
           
            return val;
    	  }

    showSubmitToMentor(){

        //retrieve current user and user role
        let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
        let userRole = currentUser ? currentUser.role : null;

        if (userRole != 'S') {
        const data = {
            'title': 'Portfolio Submission',
            'message': 'Students will be able to submit their Portfolio to their mentors, as well as send comments.'
        };

        const dialogRef = this._dialog.open(MessageDialogComponent, {
            data: data,
            maxWidth: '400px',
        });
        }else{
            let data = {
                title: "Portfolio"
            }
            this._dialog.open(SubmitToMentorDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });
        }
    

    }

}

