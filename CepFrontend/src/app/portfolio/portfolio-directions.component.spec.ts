import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioDirectionsComponent } from './portfolio-directions.component';

describe('ParentsComponent', () => {
  let component: PortfolioDirectionsComponent;
  let fixture: ComponentFixture<PortfolioDirectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioDirectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioDirectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
