import { Injectable } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable({
    providedIn: 'root'
})
export class PortfolioRestFactoryService {

    restURL: any;

    constructor(private _http: HttpClient,
        _config: ConfigService,
        private _user: UserService,
        private _httpHelper: HttpHelperService) {
        this.restURL = _config.getRestUrl() + '/';
    }


    isPortfolioStarted() {
        return this._http.get(this.restURL + 'portfolio/PortfolioStarted/');
    }

    /**
     * Work experience rest calls.
     */
    getWorkExperience = function () {
        return this._http.get(this.restURL + 'portfolio/WorkExperience/');
    }

    insertWorkExperience = function (workExperienceObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertWorkExperience', workExperienceObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteWorkExperience = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(
                this.restURL + 'portfolio/DeleteWorkExperience/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateWorkExperience = function (workExperienceObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateWorkExperience', workExperienceObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Education rest calls.
     */
    getEducation = function () {
        return this._http.get(this.restURL + 'portfolio/Education/');
    }

    insertEducation = function (educationObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertEducation', educationObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteEducation = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteEducation/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateEducation = function (educationObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateEducation', educationObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Achievement rest calls.
     */
    getAchievements = function () {
        return this._http.get(this.restURL + 'portfolio/Achievement/');
    }

    insertAchievement = function (achievementObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertAchievement', achievementObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteAchievement = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteAchievement/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateAchievement = function (achievementObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateAchievement', achievementObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Interests rest calls.
     */
    getInterests = function () {
        return this._http.get(this.restURL + 'portfolio/Interest/');
    }

    insertInterest = function (interestObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertInterest', interestObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteInterest = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteInterest/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateInterest = function (interestObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateInterest', interestObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Skills rest calls.
     */
    getSkills = function () {
        return this._http.get(this.restURL + 'portfolio/Skill/');
    }

    insertSkill = function (skillObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertSkill', skillObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteSkill = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteSkill/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateSkill = function (skillObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateSkill', skillObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Work values rest calls.
     */
    getWorkValues = function () {
        return this._http.get(this.restURL + 'portfolio/WorkValue/');
    }

    insertWorkValue = function (workValueObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertWorkValue', workValueObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteWorkValue = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteWorkValue/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateWorkValue = function (workValueObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateWorkValue', workValueObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Volunteer rest calls.
     */
    getVolunteers = function () {
        return this._http.get(this.restURL + 'portfolio/Volunteer/');
    }

    insertVolunteer = function (volunteerObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertVolunteer', volunteerObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteVolunteer = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteVolunteer/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateVolunteer = function (volunteerObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateVolunteer', volunteerObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * ACT score rest calls.
     */
    getActScore = function () {
        return this._http.get(this.restURL + 'portfolio/ACT/');
    }

    insertActScore = function (actObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertACT', actObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteActScore = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteACT/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateActScore = function (actObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateACT', actObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * ASVAB score rest calls.
     */
    getAsvabScore = function () {
        return this._http.get(this.restURL + 'portfolio/ASVAB/');
    }

    insertAsvabScore = function (asvabObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertASVAB', asvabObject).then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteAsvabScore = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http({
                method: 'DELETE',
                url: this.restURL + 'portfolio/DeleteASVAB/'
            }).then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateAsvabScore = function (asvabObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateASVAB', asvabObject).then(function successCallback(response) {
                //this._http.put(this.restURL + 'testScore/updateManualTestScore', asvabObject).then(function successCallback(response) { // returns bad request
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * FYI score rest calls.
     */
    getFyiScore = function () {
        return this._http.get(this.restURL + 'portfolio/FYI/');
    }

    insertFyiScore = function (fyiObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertFYI', fyiObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteFyiScore = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteFYI/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateFyiScore = function (fyiObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateFYI', fyiObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * SAT score rest calls.
     */
    getSatScore = function () {
        return this._http.get(this.restURL + 'portfolio/SAT/');
    }

    insertSatScore = function (satObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertSAT', satObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteSatScore = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteSAT/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateSatScore = function (satObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateSAT', satObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Other score rest calls.
     */
    getOtherScore = function () {
        return this._http.get(this.restURL + 'portfolio/Other/');
    }

    insertOtherScore = function (otherObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertOther', otherObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    deleteOtherScore = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeleteOther/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updateOtherScore = function (otherObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateOther', otherObject).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    /**
     * Plan of action rest calls.
     */
    getPlan = function () {
        return this._http.get(this.restURL + 'portfolio/Plan/');
    }

    insertPlan = function (planObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/InsertPlan', planObject).toPromise().then((response) => {
                resolve(response);
            }, (response) => {
                console.error("failed to insert");
                reject(response);
            });
        });
        return deferred;
    }

    deletePlan = function (id) {
        var deferred = new Promise((resolve, reject) => {
            this._http.delete(this.restURL + 'portfolio/DeletePlan/' + id + '/'
            ).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    updatePlan = function (planObject) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdatePlan', planObject).toPromise().then((response) => {
                resolve(response);
            }, (response) => {
                reject(response);
            });
        });
        return deferred;
    }

    selectNameGrade = function () {
        return this._http.get(this.restURL + 'portfolio/SelectNameGrade/');
    }

    updateInsertUserInfo = function (userInfo) {
        var deferred = new Promise((resolve, reject) => {
            this._http.put(this.restURL + 'portfolio/UpdateInsertUserInfo', userInfo).toPromise().then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    getCitmFavoriteOccupations = function () {
        return this._http.get(this.restURL + 'portfolio/get-citm-favorite-occupation/');
    }

    getCitmFavoriteOccupationsNF() {

        const endPoint = this.restURL + 'portfolio/get-citm-favorite-occupation/';
        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('get', endPoint, null, null, true)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });


        // return new Promise((resolve, reject) => {
        //     this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        //         .then((obj) => {
        //             resolve(obj);
        //         }).catch(error => {
        //             reject(error);
        //         });
        // });

        // return this._http.get(this.restURL + 'portfolio/get-citm-favorite-occupation/');
    }

    getAccessCode = function () {
        return this._http.get(this.restURL + 'portfolio/get-access-code/');
    }

    insertTeacherDemoIsTaken = function (obj) {
        var deferred = new Promise((resolve, reject) => {
            this._http.post(this.restURL + 'portfolio/insert-teacher-demo-taken', obj).then(function successCallback(response) {
                resolve(response);
            }, function errorCallback(response) {
                reject(response);
            });
        });
        return deferred;
    }

    getTeacherDemoIsTaken = function () {
        return this._http.get(this.restURL + 'portfolio/get-teacher-demo-taken/');
    }

}
