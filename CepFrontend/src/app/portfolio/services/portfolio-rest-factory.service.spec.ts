import { TestBed } from '@angular/core/testing';

import { PortfolioRestFactoryService } from './portfolio-rest-factory.service';

describe('PortfolioRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PortfolioRestFactoryService = TestBed.get(PortfolioRestFactoryService);
    expect(service).toBeTruthy();
  });
});
