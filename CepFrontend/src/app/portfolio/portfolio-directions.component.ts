import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { PortfolioService } from 'app/services/portfolio.service';

@Component({
  selector: 'app-portfolio-directions',
  templateUrl: './portfolio-directions.component.html',
  styleUrls: ['./portfolio-directions.component.scss']
})
export class PortfolioDirectionsComponent implements OnInit {

  portfolioUserLoggedIn: boolean = false;

  constructor(
    private _portfolioService: PortfolioService,
    private _userService: UserService,
    private _router: Router
  ) { }

  ngOnInit() {
    if ( this._userService.isLoggedIn() ) {
			this.portfolioUserLoggedIn = true;
		} else {
			this.portfolioUserLoggedIn = false;
    }
  }

    isLoggedIn() {
      return this._userService.isLoggedIn();
  }

  deleteSessionPortfolio() {
    return;
  }

  startPortfolio() {
    this._portfolioService.getPortfolio().then(res => {
      if (!res) {
        this._portfolioService.insertPlanNew().then((res) => {
          this._router.navigate(['/portfolio']);
        }, (error) => {
          console.error(error);
          alert("Failed creating portfolio");
        })
      } else {
        this._router.navigate(['/portfolio']);
      }
    })
  }
}
