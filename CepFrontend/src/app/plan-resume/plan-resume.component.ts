import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-resume',
  templateUrl: './plan-resume.component.html',
  styleUrls: ['./plan-resume.component.scss']
})
export class PlanResumeComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs

  constructor() { }

  ngOnInit() {
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = false;
    }
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

}
