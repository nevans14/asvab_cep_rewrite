import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanResumeComponent } from './plan-resume.component';

describe('PlanResumeComponent', () => {
  let component: PlanResumeComponent;
  let fixture: ComponentFixture<PlanResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
