import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { Router } from '@angular/router';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { AsvabZoomDialogComponent } from 'app/core/dialogs/asvab-zoom-dialog/asvab-zoom-dialog.component';
import { DbInfoService } from 'app/services/db-info.service';
import { ReCaptcha2Component } from 'ngx-captcha';
import { ContentManagementService } from 'app/services/content-management.service';
@Component({
  selector: 'app-asvab-cep-at-your-school',
  templateUrl: './asvab-cep-at-your-school.component.html',
  styleUrls: ['./asvab-cep-at-your-school.component.scss']
})
export class AsvabCepAtYourSchoolComponent implements OnInit {
  studentFormGroup: FormGroup;
  schedFormGroup: FormGroup;
  responseOne: '';
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  formDisabled = false;
  heardOther = false;
  errorMessage: string;
  cursorStyle = { 'cursor': 'pointer' };
  path: any;
  pageHtml = [];
  isDisplayBanner: any;
  maintenanceBannerText: any;
  showSchedule: boolean;

  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  constructor(
    // public _dialogRef: MatDialogRef<AsvabCepAtYourSchoolComponent>,
    private _dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _configService: ConfigService,
    private _http: HttpHelperService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _router: Router,
    private _dbInfoService: DbInfoService,
    private _contentManagementService: ContentManagementService,
  ) {
    this.studentFormGroup = this._formBuilder.group({
      recaptchaResponse: ['', Validators.required],
      counselorEmail: ['', Validators.required],
      studentHeardUs: [''],
      studentHeardUsOther: ['']
    });

    this.schedFormGroup = this._formBuilder.group({
      recaptcha: ['', Validators.required],
      title: ['', Validators.required],
      position: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      heardUs: [''],
      heardUsOther: [''],
      schoolName: ['', Validators.required],
      schoolAddress: ['', Validators.required],
      schoolCity: ['', Validators.required],
      schoolCounty: ['', Validators.required],
      schoolState: ['', Validators.required],
      schoolZip: ['', Validators.required],
      asvabTest: [''],
      pti: [{ value: false, disabled: false }],
      ptiVirtual: [{ value: false, disabled: true }],
      ptiInPerson: [{ value: false, disabled: true }],
      classroomActivities: [{ value: false, disabled: false }],
      classroomActivitiesVirtual: [{ value: false, disabled: true }],
      classroomActivitiesInPerson: [{ value: false, disabled: true }],
      presentation: [{ value: false, disabled: false }],
      presentationVirtual: [{ value: false, disabled: true }],
      presentationInPerson: [{ value: false, disabled: true }]
    });
  }

  ngOnInit() {
    this.path = this._router.url;
    this.isDisplayBanner = false;
    this.maintenanceBannerText = '';
    this._dbInfoService.getByName('cep_bring_modal_banner').subscribe((response: any) => {
      this.isDisplayBanner = response.value2 ? "true" : "false";
      this.maintenanceBannerText = this.isDisplayBanner ? response.value2 : '';
    });

    this.showSchedule = false;

    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  onShareWithCounselor() {
    this.cursorStyle = { 'cursor': 'wait' };
    this.errorMessage = undefined;
    const email = this.studentFormGroup.value.counselorEmail;
    const studentHeardUsOther = this.studentFormGroup.value.studentHeardUsOther;

    if (this.studentFormGroup.value.recaptchaResponse === '') {  // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {

      const emailObject = {
        email: email,
        recaptcha: this.studentFormGroup.value.recaptchaResponse,
        studentHeardUs: false,
        studentHeardUsOther: studentHeardUsOther
      };

      const self = this;
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactUs/shareWithCounselor`);
      return this._http.httpHelper('POST', fullUrl, null, emailObject, true, 'aws')
        .then(function (success) {
          self.ga(self.path + '#BRING_ASVAB_FORM_STUDENT_SHARE');

          const data = {
            'title': 'Schedule Notification',
            'message': 'Your information was submitted successfully.'
          };
          const dialogRef = self._dialog.open(MessageDialogComponent, {
            data: data
          });

          self.studentFormGroup.reset();

          return;
        })
        .catch(error => {
          console.error('Error: ' + error);
          if (!error['message'] || error['message'] === 'null') {
            self.errorMessage = 'There was an error proccessing your request. Please try again.';
          } else {
            self.errorMessage = error['message'];
          }
          self.responseOne = '';
          self.formDisabled = false;
          self.cursorStyle = { 'cursor': 'pointer' };
        });
    }
  }

  ga(param: string) {

    this._googleAnalyticsService.trackClick(param);
  }

  onStudentHeardChange() {
    if (this.studentFormGroup.value.studentHeardUs === 'other') {
      this.heardOther = true;
    } else {
      this.heardOther = false;
    }
  }

  onBringAsvabToYourSchool() {
    const dialogRef2: MatDialogRef<BringAsvabToYourSchoolComponent> = this._dialog.open(BringAsvabToYourSchoolComponent, {
      hasBackdrop: true,
      panelClass: 'new-modal',
      // height: '98%',
      autoFocus: true,
      disableClose: true
    });
    dialogRef2.componentInstance.showSchedule = true;
  }

  onScheduleSubmit() {
    this.errorMessage = undefined;
    this.formDisabled = true;

    if (this.schedFormGroup.value.recaptcha === '') { // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {

      const emailObject = {
        title: this.schedFormGroup.value.title,
        recaptcha: this.schedFormGroup.value.recaptcha,
        position: this.schedFormGroup.value.position,
        firstName: this.schedFormGroup.value.firstName,
        lastName: this.schedFormGroup.value.lastName,
        phone: this.schedFormGroup.value.phone,
        email: this.schedFormGroup.value.email,
        heardUs: this.schedFormGroup.value.heardUs,
        heardUsOther: this.schedFormGroup.value.heardUsOther,
        schoolName: this.schedFormGroup.value.schoolName,
        schoolAddress: this.schedFormGroup.value.schoolAddress,
        schoolCity: this.schedFormGroup.value.schoolCity,
        schoolCounty: this.schedFormGroup.value.schoolCounty,
        schoolState: this.schedFormGroup.value.schoolState,
        schoolZip: this.schedFormGroup.value.schoolZip,
        asvabActivities: this.getAsvabActivities(),
        asvabActivityFormats: this.getAsvabActivityFormats()
      };

      const self = this;
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactUs/scheduleEmail`);
      return this._http.httpHelper('POST', fullUrl, null, emailObject, true, 'aws')
        .then(function (success) {

          self.ga(self.path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT');

          const data = {
            'title': 'Schedule Notification',
            'message': 'Your information was submitted successfully.'
          };
          const dialogRef = self._dialog.open(MessageDialogComponent, {
            data: data
          });

          return;
        })
        .catch(error => {
          if (!error['message'] || error['message'] === 'null') {
            self.errorMessage = 'There was an error processing your request. Please try again.';
          } else {
            self.errorMessage = error['message'];
          }
          self.schedFormGroup.value.recaptcha = '';
          self.formDisabled = false;
        });
    }
  }

  getAsvabActivities(): any {
    return {
      asvabTest: this.schedFormGroup.value.asvabTest,
      pti: this.schedFormGroup.value.pti,
      classroomActivities: this.schedFormGroup.value.classroomActivities,
      presentation: this.schedFormGroup.value.presentation
    }
  }

  getAsvabActivityFormats(): any {
    return {
      pti: this.getActivityFormatValue(this.schedFormGroup.value.ptiVirtual, this.schedFormGroup.value.ptiInPerson),
      classroomActivities: this.getActivityFormatValue(this.schedFormGroup.value.classroomActivitiesVirtual, this.schedFormGroup.value.classroomActivitiesInPerson),
      presentation: this.getActivityFormatValue(this.schedFormGroup.value.presentationVirtual, this.schedFormGroup.value.presentationInPerson)
    }
  }

  getActivityFormatValue(virtual: boolean, inPerson: boolean): string {
    if (virtual === true && inPerson === true) {
      return 'virtual,in-person';
    } else {
      if (virtual === true) {
        return 'virtual';
      }
      if (inPerson === true) {
        return 'in-person';
      }
    }
    return '';
  }

  onHeardChange() {
    if (this.schedFormGroup.value.heardUs === 'other') {
      this.heardOther = true;
    } else {
      this.heardOther = false;
    }
  }

  updateCheckboxes(groupName, value) {

    if (value === true) {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").enable();
        this.schedFormGroup.get("ptiInPerson").enable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").enable();
        this.schedFormGroup.get("classroomActivitiesInPerson").enable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").enable();
        this.schedFormGroup.get("presentationInPerson").enable();
      }
    } else {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").disable();
        this.schedFormGroup.get("ptiInPerson").disable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").disable();
        this.schedFormGroup.get("classroomActivitiesInPerson").disable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").disable();
        this.schedFormGroup.get("presentationInPerson").disable();
      }
    }

  }

  imageZoom() {
    const dialogRef2 = this._dialog.open(AsvabZoomDialogComponent, {
      maxHeight: '200vh',
    });
  }

}
