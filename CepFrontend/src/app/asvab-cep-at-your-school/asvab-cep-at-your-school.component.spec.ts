import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsvabCepAtYourSchoolComponent } from './asvab-cep-at-your-school.component';

describe('AsvabCepAtYourSchoolComponent', () => {
  let component: AsvabCepAtYourSchoolComponent;
  let fixture: ComponentFixture<AsvabCepAtYourSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsvabCepAtYourSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsvabCepAtYourSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
