import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { RetrieveAsvabScoresDialogComponent } from 'app/core/dialogs/retrieve-asvab-scores-dialog/retrieve-asvab-scores-dialog.component';
import { ConfigService } from 'app/services/config.service';
import { ScoreService } from 'app/services/score.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-my-asvab-summary-results-manual',
  templateUrl: './my-asvab-summary-results-manual.component.html',
  styleUrls: ['./my-asvab-summary-results-manual.component.scss']
})
export class MyAsvabSummaryResultsManualComponent implements OnInit {

  scrollThreshold: any;
  showScrollToTop: boolean;
  scoreSummary: any;
  mediaCenterList: any;
  asvabScore: any;

  genderFull: any;
  testDateMonth: any;
  testDateDay: any;
  testDateYear: any;
  verbalScore: number;
  mathScore: number;
  scienceScore: number;
  haveManualAsvabTestScores: boolean;
  hsGrade: any;
  testLocation: any;
  ssoUrl: any;
  gender: any;

  constructor(private _configService: ConfigService,
    private _scoreService: ScoreService,
    private _dialog: MatDialog,
    private _congfigService: ConfigService,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _router: Router,) { }

  ngOnInit() {
    window.scroll(0, 0);

    this.showScrollToTop = false;
    this.scrollThreshold = 250;

    this.asvabScore = this._activatedRoute.snapshot.data.asvabScore;
    this.mediaCenterList = this._activatedRoute.snapshot.data.mediaCenter;
    //filter the media center list
    if (this.mediaCenterList) {
      this.mediaCenterList = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.positionId == 6 && mediaCenterItem.categoryId == 5);
    }

    this.haveManualAsvabTestScores = window.sessionStorage.getItem('manualScores') == "true";

    this.ssoUrl = this._configService.getSsoUrl();

    this.setLastTestData();

    this._userService.setCompletion(UserService.VIEWED_ASVAB_SCORE, 'true');
  }

  setLastTestData() {

    this.verbalScore = Number(this.asvabScore[this.asvabScore.length - 1].verbalScore);
    this.mathScore = Number(this.asvabScore[this.asvabScore.length - 1].mathScore);
    this.scienceScore = Number(this.asvabScore[this.asvabScore.length - 1].scienceScore);

    // set hs grade level
    this.hsGrade = this.asvabScore[this.asvabScore.length - 1].grade;

    // Set test date
    this.testDateYear = this.asvabScore[this.asvabScore.length - 1].testYear;
    this.testDateDay = this.asvabScore[this.asvabScore.length - 1].testDay;
    this.testDateMonth = this.asvabScore[this.asvabScore.length - 1].testMonth;
    this.testLocation = this.asvabScore[this.asvabScore.length - 1].testLocation;

    this.gender  = this.getGenderFull();
  

  }

  scrollTo(el: HTMLElement) {
    el.scrollIntoView();
  }

  scrollToTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  //sets the scroll to top display when threshold is met
  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (scrollPosition >= this.scrollThreshold) {
      this.showScrollToTop = true;
    } else {
      this.showScrollToTop = false;
    }

  }

  /**
   * Show video modal
  */
  showVideoDialog(videoName) {
    this._dialog.open(PopupPlayerComponent, {
      data: { videoId: videoName }
    });
  }

  moreInfo(area) {
    var modalOptions;
    switch (area) {
      case "ces":
        modalOptions = {
          title: 'Career Exploration Scores',
          message: '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' +
            '<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' +
            '<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' +
            '<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' +
            '<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' +
            '<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
        };
        break;
      case "cesStandard":
        modalOptions = {
          title: 'Career Exploration Scores',
          message: '<h4>Standard Scores</h4>' +
            '<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
        };
        break;
      case "cesPercent":
        modalOptions = {
          title: 'Career Exploration Scores',
          message: '<h4>Percentile Scores</h4>' +
            '<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
        };
        break;
      case "asvab":
        modalOptions = {
          title: 'ASVAB Test Scores',
          message: '<p>Need content or correct link.</p>'
        };
        break;
      case "afqt":
        modalOptions = {
          title: 'Military Entrance Score (AFQT)',
          message: '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' +
            '<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' +
            '<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
        };
        break;
    }

    //show modal
    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: modalOptions,
      hasBackdrop: true,
      disableClose: false,
      // maxWidth: '400px'
    });

  }

  showRetrieveAsvabScoresDialog() {

    this._dialog.open(RetrieveAsvabScoresDialogComponent, { data: { enter: false, retrieve: true } }).beforeClosed().subscribe(result => {

      if (result) {
        if (result !== 'cancel') {
          if (result.hasOwnProperty('aFQTRawSSComposites')) {
            // Retrieved asvab scores
            this.scoreSummary = result;
          }
        }
      }

    });

  };

  cepManualInput = function () {

    this._dialog.open(RetrieveAsvabScoresDialogComponent, { data: { enter: true, retrieve: false } }).beforeClosed().subscribe(result => {

      if (result) {
        if (result !== 'cancel') {

          this.haveAsvabTestScores = false;
          this.haveManualAsvabTestScores = true;
          this.asvabScore.push({
            verbalScore: result.verbalScore,
            mathScore: result.mathScore,
            scienceScore: result.scienceScore,
            gender: result.gender,
            grade: result.grade,
            testYear: result.testYear,
            testMonth: result.testMonth,
            testDay: result.testDay,
            testLocation: result.testLocation
          });

          // Update session storage
          window.sessionStorage.setItem('manualScores', this.haveManualAsvabTestScores);
          window.sessionStorage.setItem('sV', this.getScore('verbal'));
          window.sessionStorage.setItem('sM', this.getScore('math'));
          window.sessionStorage.setItem('sS', this.getScore('science'));
          window.sessionStorage.setItem('sA', this.getScore('afqt'));

          // TODO Do we update the book stacks? on legacy its not updated
          // // Update book stacks
          // this.vBooks.score = this.getScore('verbal');
          // this.mBooks.score = this.getScore('math');
          // this.sBooks.score = this.getScore('science');
          // this.scoreSorting();

          this.setLastTestData();
        }
      }

    });
  }

  /**
* Get scores for display
*/
  getScore = function (area) {
    var score = 0;

    switch (area) {
      case "verbal":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummary ? Number(this.scoreSummary.verbalAbility.va_SGS) : 0;
        } else if (this.haveManualAsvabTestScores) {
          score = Number(this.asvabScore[this.asvabScore.length - 1].verbalScore);
        }
        break;
      case "math":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummary ? Number(this.scoreSummary.mathematicalAbility.ma_SGS) : 0;
        } else if (this.haveManualAsvabTestScores) {
          score = Number(this.asvabScore[this.asvabScore.length - 1].mathScore);
        }
        break;
      case "science":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummary ? Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) : 0;
        } else if (this.haveManualAsvabTestScores) {
          score = Number(this.asvabScore[this.asvabScore.length - 1].scienceScore);
        }
        break;
      case "afqt": // no manual entry for AFQT
        score = this.scoreSummary ? Number(this.scoreSummary.aFQTRawSSComposites.afqt) : 0;
        break;
      default:
        break;
    }
    return score;
  }

  getGenderFull() {
		let genderCode = this.asvabScore[this.asvabScore.length-1].gender;
		if (genderCode)
			genderCode = genderCode.toLowerCase();
		switch (genderCode) {
			case "f":
				genderCode = "Female";
				break;
			case "m":
				genderCode = "Male";
				break;
			default:
				genderCode = null;
				break;
		}
		return genderCode;
	}

}
