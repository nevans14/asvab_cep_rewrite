import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAsvabSummaryResultsManualComponent } from './my-asvab-summary-results-manual.component';

describe('MyAsvabSummaryResultsManualComponent', () => {
  let component: MyAsvabSummaryResultsManualComponent;
  let fixture: ComponentFixture<MyAsvabSummaryResultsManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAsvabSummaryResultsManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAsvabSummaryResultsManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
