import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { ScoreService } from 'app/services/score.service';
import { MatDialog } from '@angular/material';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { MessageDialogComponent } from '../core/dialogs/message-dialog/message-dialog.component';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { RetrieveAsvabScoresDialogComponent } from 'app/core/dialogs/retrieve-asvab-scores-dialog/retrieve-asvab-scores-dialog.component';
import { Router } from '@angular/router';
import { UtilityService } from 'app/services/utility.service';
import { FyiRestFactoryService } from 'app/services/fyi-rest-factory-service';
import html2canvas from "html2canvas";

declare var pdfMake: any;

@Component({
	selector: 'app-my-asvab-summary-results',
	templateUrl: './my-asvab-summary-results.component.html',
	styleUrls: ['./my-asvab-summary-results.component.scss']
})
export class MyAsvabSummaryResultsComponent implements OnInit {

	@ViewChild('printMe') printMeElem: ElementRef;

	userRole: any;
	currentUser: any;
	scoreSummary: any;
	ssoUrl: any;
	genderFull: any;
	administeredDay: any;
	administeredYear: any;
	administeredMonth: any;
	mediaCenterArticle: any;
	mediaCenterList: any;
	resultsSelection: any;
	chartRef: any;
	gender: any;
	scrollThreshold: any;
	showScrollToTop: boolean;
	asvabData2_1: ChartDataSets[];
	asvabData2_2: ChartDataSets[];
	asvabData2_3: ChartDataSets[];
	asvabData2_4: ChartDataSets[];
	asvabData2_5: ChartDataSets[];
	asvabData2_6: ChartDataSets[];
	asvabData2_7: ChartDataSets[];
	asvabData2_8: ChartDataSets[];
	greenColors: any;
	redColors: any;
	purpleColors: any;
	blackColors: any;
	percentChartLabels: Label[];
	percentChartOptions: ChartOptions;
	standardScores: any;
	lowVals = [];
	highVals = [];
	public cesData1_1: ChartDataSets[];
	public cesData1_2: ChartDataSets[];
	public cesData1_3: ChartDataSets[];
	public cesData2_1: ChartDataSets[];
	public cesData2_2: ChartDataSets[];
	public cesData2_3: ChartDataSets[];
	public cesData2_4: ChartDataSets[];
	public horizontalBarChartType: ChartType = 'horizontalBar';
	public doughnutChartType: ChartType = 'doughnut';
	public barChartPlugins = [pluginDataLabels];

	// standardScoreColors0_1: any;
	standardScoreColors1: any;
	standardScoreColors1_2: any;
	standardScoreColors1_3: any;
	public cesLabels1_1: Label[];
	asvabData1_1: ChartDataSets[];
	asvabLabels1_2: Label[];
	asvabData1_2: ChartDataSets[];
	asvabData1_3: ChartDataSets[];
	asvabData1_4: ChartDataSets[];
	asvabData1_5: ChartDataSets[];
	asvabData1_6: ChartDataSets[];
	asvabData1_7: ChartDataSets[];
	asvabData1_8: ChartDataSets[];
	standardScoreColors2: any;
	standardScoreColors2_2: any;
	standardScoreColors2_3: any;
	standardScoreColors2_4: any;
	standardScoreColors2_5: any;
	standardScoreColors2_6: any;
	standardScoreColors2_7: any;
	standardScoreColors2_8: any;
	scoreChartOptions: ChartOptions;
	afqtChartData: ChartDataSets[];
	afqtChartLabels: Label[];
	afqtChartOptions: ChartOptions;
	haveAsvabTestScores: any;
	mySubscription: any;
	mobileOrTablet: any;
	scoreSummaryStudent: any;
	cesData1: any;
	asvabData1: any;
	testDay: any;
	testYear: any;
	testMonth: any;
	testSchool: any;
	educationLevel: any;
	educationLevelGr: any;
	accessCodeExpiration: any;
	afqtChartColors = ['#4c44b4', '#f2f2f2'];

	public student: any;
	public studentName: string;

	constructor(
		private _configService: ConfigService,
		private _scoreService: ScoreService,
		private _dialog: MatDialog,
		private _congfigService: ConfigService,
		private _activatedRoute: ActivatedRoute,
		private _userService: UserService,
		private _router: Router,
		private _utilityService: UtilityService,
		private _fyiRestFactoryService: FyiRestFactoryService
	) {
		//retrieve current user and user role
		this.currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
		this.userRole = this.currentUser ? this.currentUser.role : null;

		this.mySubscription = this._router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				this._router.navigated = false;
			}
		});

		this._userService.setCompletion(UserService.VIEWED_ASVAB_SCORE, 'true');
	}

	ngOnDestroy() {
		if (this.mySubscription) {
			this.mySubscription.unsubscribe();
		}
	}

	ngOnInit() {

		window.scroll(0, 0);
		this.scoreSummary = this._activatedRoute.snapshot.data.scoreSummary;
		this.mediaCenterList = this._activatedRoute.snapshot.data.mediaCenter;
		this.showScrollToTop = false;
		this.scrollThreshold = 250;
		this.mobileOrTablet = this._utilityService.mobileOrTablet();
		//filter the media center list
		if (this.mediaCenterList) {
			this.mediaCenterList = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.positionId == 6 && mediaCenterItem.categoryId == 5);
		}

		this.ssoUrl = this._configService.getSsoUrl();

		//set gender
		if (this.scoreSummary
			&& this.scoreSummary.controlInformation
			&& this.scoreSummary.controlInformation.gender != undefined
		) {
			this.gender = this.scoreSummary.controlInformation.gender.substring(0, 1).toLowerCase();
			if (this.gender === 'f') {
				this.genderFull = 'Female'
			} else {
				this.genderFull = 'Male'
			}
		} else {
			this.gender = 'm';
			this.genderFull = 'Male';
		}

		// Set test date
		this.administeredYear = this.scoreSummary.controlInformation.administeredYear;
		this.administeredDay = this.scoreSummary.controlInformation.administeredDay;
		this.administeredMonth = this.getAbbrMonth(this.scoreSummary.controlInformation.administeredMonth);

		this.mediaCenterArticle = 'media-center-article/';
		this.resultsSelection = 'student';

		this.setChartData();
		this.setAfqtScore();

		this.haveAsvabTestScores = (this.isObject(this.scoreSummary) && this.scoreSummary.hasOwnProperty('verbalAbility')); // do we have actual test scores



		this._fyiRestFactoryService.getStudentScoreResults().then((response: any) => {

			this.setupPrint(response);

		});

		this._userService.getUsername().then((response: any) => {
			this.student = response;
			//set student name
			if (this.student) {
				if (!this.student.firstName || !this.student.lastName) {
					this.studentName = this.student.emailAddress;
				} else {
					this.studentName = this.student.firstName + " " + this.student.lastName;
				}
			} else {
				this.studentName = "";
			}
		});
	}

	setupPrint(scoreSummaryStudent) {

		this.scoreSummaryStudent = scoreSummaryStudent;

		this.cesData1 = [
			this.getScore('verbal'),
			this.getScore('math'),
			this.getScore('science')
		]

		/**
		 * CES Percentile Scores
		 * display order female, male, all
		 * all others data use existing, e.g. asvabData2_1
		 */
		this.asvabData1 = [ // actual scores
			Number(this.scoreSummary.generalScience.gs_SGS) > 0 ? Number(this.scoreSummary.generalScience.gs_SGS) : 0,
			Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? Number(this.scoreSummary.arithmeticReasoning.ar_SGS) : 0,
			Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? Number(this.scoreSummary.wordKnowledge.wk_SGS) : 0,
			Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? Number(this.scoreSummary.paragraphComprehension.pc_SGS) : 0,
			Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) : 0,
			Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? Number(this.scoreSummary.electronicsInformation.ei_SGS) : 0,
			Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? Number(this.scoreSummary.autoShopInformation.as_SGS) : 0,
			Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? Number(this.scoreSummary.mechanicalComprehension.mc_SGS) : 0,
		]

		this.testYear = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredYear) ? '' : this.scoreSummary.controlInformation.administeredYear;
		this.testDay = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredDay) ? '' : this.scoreSummary.controlInformation.administeredDay;
		this.testMonth = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredMonth) ? '' : this.getAbbrMonth(this.scoreSummary.controlInformation.administeredMonth);

		this.testSchool = {
			"name": "",
			"city": "",
			"state": ""
		}
		this.testSchool.name = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolSiteName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolSiteName;
		this.testSchool.city = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolCityName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolCityName;
		this.testSchool.state = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolStateName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolStateName;

		this.educationLevel = (!this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.educationLevel) ? '' : this.scoreSummary.controlInformation.educationLevel;
		this.educationLevelGr = !this.educationLevel ? '' : this.scoreSummary.controlInformation.educationLevel + 'th Gr ';

		this.accessCodeExpiration = this._utilityService.convertUTCDateToLocalDate(new Date(this.scoreSummaryStudent.accessCodes.expireDate));
	}

	showRetrieveAsvabScoresDialog() {

		this._dialog.open(RetrieveAsvabScoresDialogComponent, { data: { enter: false, retrieve: true } }).beforeClosed().subscribe(result => {

			if (result) {
				if (result !== 'cancel') {
					if (result.hasOwnProperty('aFQTRawSSComposites')) {
						// Retrieved asvab scores
						this.scoreSummary = result;
					}
				}
			}

		});

	};

	scrollTo(el: HTMLElement) {
		el.scrollIntoView();
	}

	scrollToTop() {
		window.scroll({
			top: 0,
			left: 0,
			behavior: 'smooth'
		});
	}

	hiliteItems(area) {
		/**
		 * TODO: hilite areas of chart canvas
		 */
		let returnVal;
		if (area == 'none') {
			returnVal = 'Clear Highlights';
		} else {
			switch (area) {
				case 'verbal':
					returnVal = 'Highlight Verbal Skills';
					break;
				case 'math':
					returnVal = 'Highlight Math Skills';
					break;
				case 'science':
					returnVal = 'Highlight Science Skills';
					break;
				case 'other':
					returnVal = 'Highlight Other Skills';
					break;
				default:
					break;
			}
		}
	};

	getLow(num1, num2) {
		return Math.min(num1, num2);
	};

	getHigh(num1, num2) {
		return Math.max(num1, num2);
	};

	/**
	 * CES Percentile Scores
	 * display order female, male, all
	 */
	setChartData() {
		this.chartRef = this.scoreSummary.verbalAbility;

		this.cesData2_1 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.va_GS_Percentage) : Number(this.chartRef.va_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.va_GS_Percentage) : Number(this.chartRef.va_GOS_Percentage),
				Number(this.chartRef.va_COMP_Percentage)
			],
			barThickness: 16
		}];

		this.chartRef = this.scoreSummary.mathematicalAbility;

		this.cesData2_2 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.ma_GS_Percentage) : Number(this.chartRef.ma_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.ma_GS_Percentage) : Number(this.chartRef.ma_GOS_Percentage),
				Number(this.chartRef.ma_COMP_Percentage)
			],
			barThickness: 16
		}];
		this.chartRef = this.scoreSummary.scienceTechnicalAbility;
		this.cesData2_3 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.tec_GS_Percentage) : Number(this.chartRef.tec_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.tec_GS_Percentage) : Number(this.chartRef.tec_GOS_Percentage),
				Number(this.chartRef.tec_COMP_Percentage)
			],
			barThickness: 16
		}];

		/**
	 * ASVAB Percentile Scores
	 * display order femmale, male, all
	 */
		this.chartRef = this.scoreSummary.generalScience;
		this.asvabData2_1 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.gs_GS_Percentage) : Number(this.chartRef.gs_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.gs_GS_Percentage) : Number(this.chartRef.gs_GOS_Percentage),
				Number(this.chartRef.gs_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.arithmeticReasoning;
		this.asvabData2_2 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.ar_GS_Percentage) : Number(this.chartRef.ar_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.ar_GS_Percentage) : Number(this.chartRef.ar_GOS_Percentage),
				Number(this.chartRef.ar_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.wordKnowledge;
		this.asvabData2_3 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.wk_GS_Percentage) : Number(this.chartRef.wk_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.wk_GS_Percentage) : Number(this.chartRef.wk_GOS_Percentage),
				Number(this.chartRef.wk_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.paragraphComprehension;
		this.asvabData2_4 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.pc_GS_Percentage) : Number(this.chartRef.pc_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.pc_GS_Percentage) : Number(this.chartRef.pc_GOS_Percentage),
				Number(this.chartRef.pc_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.mathematicsKnowledge;
		this.asvabData2_5 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.mk_GS_Percentage) : Number(this.chartRef.mk_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.mk_GS_Percentage) : Number(this.chartRef.mk_GOS_Percentage),
				Number(this.chartRef.mk_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.electronicsInformation;
		this.asvabData2_6 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.ei_GS_Percentage) : Number(this.chartRef.ei_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.ei_GS_Percentage) : Number(this.chartRef.ei_GOS_Percentage),
				Number(this.chartRef.ei_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.autoShopInformation;
		this.asvabData2_7 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.as_GS_Percentage) : Number(this.chartRef.as_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.as_GS_Percentage) : Number(this.chartRef.as_GOS_Percentage),
				Number(this.chartRef.as_COMP_Percentage)
			]
		}
		];
		this.chartRef = this.scoreSummary.mechanicalComprehension;
		this.asvabData2_8 = [{
			data: [
				this.gender == 'f' ? Number(this.chartRef.mc_GS_Percentage) : Number(this.chartRef.mc_GOS_Percentage),
				this.gender == 'm' ? Number(this.chartRef.mc_GS_Percentage) : Number(this.chartRef.mc_GOS_Percentage),
				Number(this.chartRef.mc_COMP_Percentage)
			]
		}
		];

		this.greenColors = [
			{
				backgroundColor: '#29afb5',
				borderColor: '#29afb5',
				hoverBackgroundColor: '#29afb5',
				hoverBorderColor: '#29afb5',
			}
		];

		this.redColors = [
			{
				backgroundColor: '#8b2e33',
				borderColor: '#8b2e33',
				hoverBackgroundColor: '#8b2e33',
				hoverBorderColor: '#8b2e33'
			}
		];

		this.purpleColors = [
			{
				backgroundColor: '#413a92',
				borderColor: '#413a92',
				hoverBackgroundColor: '#413a92',
				hoverBorderColor: '#413a92'
			}
		];
		this.blackColors = [
			{
				backgroundColor: '#333333',
				borderColor: '#333333',
				hoverBackgroundColor: '#333333',
				hoverBorderColor: '#333333'
			}
		];

		/**
		 * All/Global Percent chart labels/options
		 */
		this.percentChartLabels = ['Female', 'Male', 'All'];
		this.percentChartOptions = {
			events: [],
			title: {
				display: false
			},
			legend: {
				display: false
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true,
						min: 0,
						max: 100,
						stepSize: 20
					},
					gridLines: {
						zeroLineColor: 'transparent',
						zeroLineWidth: 0
					}
				}],
				yAxes: [{
					gridLines: {
						zeroLineColor: 'transparent',
						zeroLineWidth: 0
					}
				}]
			},
			tooltips: {
				enabled: false
			},
			plugins: {
				datalabels: {
					color: 'white',
					anchor: 'end',
					align: 'start'
				}
			}
		};

		/**
		 * CES Standard Scores Chart Data
		 * chart.js does not have a range bar chart
		 * Using a stacked horz bar chart to create a range type chart
		 * inital bar is set to transparent
		 * LSL = lower score limit; USL = high score limit
		 * added getLow/getHigh due to USL coming in lower than LSL in some data
		 */
		this.lowVals = [];
		this.lowVals = [
			this.getLow(Number(this.scoreSummary.verbalAbility.va_LSL), Number(this.scoreSummary.verbalAbility.va_USL)),
			this.getLow(Number(this.scoreSummary.mathematicalAbility.ma_LSL), Number(this.scoreSummary.mathematicalAbility.ma_USL)),
			this.getLow(Number(this.scoreSummary.scienceTechnicalAbility.tec_LSL), Number(this.scoreSummary.scienceTechnicalAbility.tec_USL)),
		];
		this.highVals = [];
		this.highVals = [
			this.getHigh(Number(this.scoreSummary.verbalAbility.va_LSL), Number(this.scoreSummary.verbalAbility.va_USL)),
			this.getHigh(Number(this.scoreSummary.mathematicalAbility.ma_LSL), Number(this.scoreSummary.mathematicalAbility.ma_USL)),
			this.getHigh(Number(this.scoreSummary.scienceTechnicalAbility.tec_LSL), Number(this.scoreSummary.scienceTechnicalAbility.tec_USL)),
		];

		this.cesData1_1 = [
			{
				data: [ // score low range bar start value < actual score or 0 if no score
					Number(this.scoreSummary.verbalAbility.va_SGS) > 0 ? this.lowVals[0] + 2 : 0,
					Number(this.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					display: false
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.verbalAbility.va_SGS) > 0 ? this.highVals[0] - this.lowVals[0] + 2 : 0,
					Number(this.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? this.highVals[1] - this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					color: 'white',
					display: true,
					formatter: function (value, context) {
						// show the value from the first dataset 
						return context.chart.config.data.datasets[2].data[context.dataIndex];
					}
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.verbalAbility.va_SGS),
					Number(this.scoreSummary.mathematicalAbility.ma_SGS),
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS),
				],
				label: '',
				// datalabels: {
				// 	display: false,
				// }
			}
		];

		this.cesData1_2 = [
			{
				data: [ // score low range bar start value < actual score or 0 if no score
					Number(this.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					display: false
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? this.highVals[1] - this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					color: 'white',
					display: true,
					formatter: function (value, context) {
						// show the value from the first dataset 
						return context.chart.config.data.datasets[2].data[context.dataIndex];
					}
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.mathematicalAbility.ma_SGS),
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS),
				],
				label: '',
				// datalabels: {
				// 	display: false,
				// }
			}
		];

		this.cesData1_3 = [
			{
				data: [ // score low range bar start value < actual score or 0 if no score
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					display: false
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0
				],
				label: '',
				barThickness: 16,
				datalabels: {
					color: 'white',
					display: true,
					formatter: function (value, context) {
						// show the value from the first dataset 
						return context.chart.config.data.datasets[2].data[context.dataIndex];
					}
				}
			},
			{
				data: [ // score total bar range (high - low) > actual score or 0 if no score
					Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS),
				],
				label: '',
				// datalabels: {
				// 	display: false,
				// }
			}
		];

		this.cesLabels1_1 = [
			'Verbal Skills',
			'Math Skills',
			['Science/', 'Technical Skills']
		];

		this.standardScoreColors1_2 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor
				],
				borderColor: [
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			}
		];

		this.standardScoreColors1_3 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.purpleColors[0].backgroundColor
				],
				borderColor: [
					this.purpleColors[0].borderColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			}
		];

		this.standardScoreColors1 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.greenColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor
				],
				borderColor: [
					this.greenColors[0].borderColor,
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent']
			}
		];

		/**
		 * ASVAB Standard Scores Chart Data
		 * chart.js does not have a range bar chart
		 * Using a stacked horz bar chart to create a range type chart
		 * inital bar is set to transparent
		 * LSL = lower score limit; USL = high score limit
		 * added getLow/getHihg due to USL coming in lower than LSL in some data
		 */
		this.lowVals = [];
		this.lowVals = [
			this.getLow(Number(this.scoreSummary.generalScience.gs_LSL), Number(this.scoreSummary.generalScience.gs_USL)),
			this.getLow(Number(this.scoreSummary.arithmeticReasoning.ar_LSL), Number(this.scoreSummary.arithmeticReasoning.ar_USL)),
			this.getLow(Number(this.scoreSummary.wordKnowledge.wk_LSL), Number(this.scoreSummary.wordKnowledge.wk_USL)),
			this.getLow(Number(this.scoreSummary.paragraphComprehension.pc_LSL), Number(this.scoreSummary.paragraphComprehension.pc_USL)),
			this.getLow(Number(this.scoreSummary.mathematicsKnowledge.mk_LSL), Number(this.scoreSummary.mathematicsKnowledge.mk_USL)),
			this.getLow(Number(this.scoreSummary.electronicsInformation.ei_LSL), Number(this.scoreSummary.electronicsInformation.ei_USL)),
			this.getLow(Number(this.scoreSummary.autoShopInformation.as_LSL), Number(this.scoreSummary.autoShopInformation.as_USL)),
			this.getLow(Number(this.scoreSummary.mechanicalComprehension.mc_LSL), Number(this.scoreSummary.mechanicalComprehension.mc_USL)),
			this.getLow(Number(this.scoreSummary.assemblingObjects.ao_LSL), Number(this.scoreSummary.assemblingObjects.ao_USL))
		];
		this.highVals = [];
		this.highVals = [
			this.getHigh(Number(this.scoreSummary.generalScience.gs_LSL), Number(this.scoreSummary.generalScience.gs_USL)),
			this.getHigh(Number(this.scoreSummary.arithmeticReasoning.ar_LSL), Number(this.scoreSummary.arithmeticReasoning.ar_USL)),
			this.getHigh(Number(this.scoreSummary.wordKnowledge.wk_LSL), Number(this.scoreSummary.wordKnowledge.wk_USL)),
			this.getHigh(Number(this.scoreSummary.paragraphComprehension.pc_LSL), Number(this.scoreSummary.paragraphComprehension.pc_USL)),
			this.getHigh(Number(this.scoreSummary.mathematicsKnowledge.mk_LSL), Number(this.scoreSummary.mathematicsKnowledge.mk_USL)),
			this.getHigh(Number(this.scoreSummary.electronicsInformation.ei_LSL), Number(this.scoreSummary.electronicsInformation.ei_USL)),
			this.getHigh(Number(this.scoreSummary.autoShopInformation.as_LSL), Number(this.scoreSummary.autoShopInformation.as_USL)),
			this.getHigh(Number(this.scoreSummary.mechanicalComprehension.mc_LSL), Number(this.scoreSummary.mechanicalComprehension.mc_USL)),
			this.getHigh(Number(this.scoreSummary.assemblingObjects.ao_LSL), Number(this.scoreSummary.assemblingObjects.ao_USL))
		];

		this.asvabData1_1 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.generalScience.gs_SGS) > 0 ? this.lowVals[0] + 2 : 0,
				Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? this.lowVals[1] + 2 : 0,
				Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.lowVals[2] + 2 : 0,
				Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.lowVals[3] + 2 : 0,
				Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.lowVals[4] + 2 : 0,
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.generalScience.gs_SGS) > 0 ? this.highVals[0] - this.lowVals[0] + 2 : 0,
					Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? this.highVals[1] - this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0,
					Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.highVals[3] - this.lowVals[3] + 2 : 0,
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.highVals[4] - this.lowVals[4] + 2 : 0,
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.generalScience.gs_SGS),
					Number(this.scoreSummary.arithmeticReasoning.ar_SGS),
					Number(this.scoreSummary.wordKnowledge.wk_SGS),
					Number(this.scoreSummary.paragraphComprehension.pc_SGS),
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS),
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_2 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? this.lowVals[1] + 2 : 0,
				Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.lowVals[2] + 2 : 0,
				Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.lowVals[3] + 2 : 0,
				Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.lowVals[4] + 2 : 0,
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? this.highVals[1] - this.lowVals[1] + 2 : 0,
					Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0,
					Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.highVals[3] - this.lowVals[3] + 2 : 0,
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.highVals[4] - this.lowVals[4] + 2 : 0,
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.arithmeticReasoning.ar_SGS),
					Number(this.scoreSummary.wordKnowledge.wk_SGS),
					Number(this.scoreSummary.paragraphComprehension.pc_SGS),
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS),
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_3 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.lowVals[2] + 2 : 0,
				Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.lowVals[3] + 2 : 0,
				Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.lowVals[4] + 2 : 0,
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? this.highVals[2] - this.lowVals[2] + 2 : 0,
					Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.highVals[3] - this.lowVals[3] + 2 : 0,
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.highVals[4] - this.lowVals[4] + 2 : 0,
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.wordKnowledge.wk_SGS),
					Number(this.scoreSummary.paragraphComprehension.pc_SGS),
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS),
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_4 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.lowVals[3] + 2 : 0,
				Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.lowVals[4] + 2 : 0,
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? this.highVals[3] - this.lowVals[3] + 2 : 0,
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.highVals[4] - this.lowVals[4] + 2 : 0,
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.paragraphComprehension.pc_SGS),
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS),
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_5 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.lowVals[4] + 2 : 0,
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? this.highVals[4] - this.lowVals[4] + 2 : 0,
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.mathematicsKnowledge.mk_SGS),
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_6 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.lowVals[5] + 2 : 0,
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? this.highVals[5] - this.lowVals[5] + 2 : 0,
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.electronicsInformation.ei_SGS),
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_7 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.lowVals[6] + 2 : 0,
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? this.highVals[6] - this.lowVals[6] + 2 : 0,
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.autoShopInformation.as_SGS),
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabData1_8 = [{
			data: [ // // score low range bar start value < actual score
				Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.lowVals[7] + 2 : 0,
				Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.lowVals[8] + 2 : 0
			],
			barThickness: 16,
			datalabels: {
				display: false
			}
		},
		{
			data:
				[ // score total bar range (high - low) > actual score
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? this.highVals[7] - this.lowVals[7] + 2 : 0,
					Number(this.scoreSummary.assemblingObjects.ao_SGS) > 0 ? this.highVals[8] - this.lowVals[8] + 2 : 0
				],
			barThickness: 16,
			datalabels: {
				color: 'white',
				display: true,
				formatter: function (value, context) {
					// show the actual score
					return context.chart.config.data.datasets[2].data[context.dataIndex];
				}
			}
		},
		{
			data:
				[
					Number(this.scoreSummary.mechanicalComprehension.mc_SGS),
					Number(this.scoreSummary.assemblingObjects.ao_SGS)
				],
			barThickness: 16,
			datalabels: {
				display: false
			}
		}];

		this.asvabLabels1_2 = [
			['General', 'Science'],
			['Arithmetic', 'Reasoning'],
			['Word', 'Knowledge'],
			['Paragraph', 'Comprehension'],
			['Mathematics', 'Knowledge'],
			['Electronics', 'Information'],
			['Auto and', 'Shop Information'],
			['Mechanical', 'Comprehension']
		];

		this.standardScoreColors2 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.purpleColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.greenColors[0].backgroundColor,
					this.greenColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.purpleColors[0].borderColor,
					this.redColors[0].borderColor,
					this.greenColors[0].borderColor,
					this.greenColors[0].borderColor,
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_2 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.redColors[0].backgroundColor,
					this.greenColors[0].backgroundColor,
					this.greenColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.redColors[0].borderColor,
					this.greenColors[0].borderColor,
					this.greenColors[0].borderColor,
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_3 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.greenColors[0].backgroundColor,
					this.greenColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.greenColors[0].borderColor,
					this.greenColors[0].borderColor,
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_4 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.greenColors[0].backgroundColor,
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.greenColors[0].borderColor,
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_5 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.redColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.redColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_6 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.purpleColors[0].borderColor,
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_7 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.blackColors[0].backgroundColor,
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.blackColors[0].borderColor,
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.standardScoreColors2_8 = [
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
			{
				backgroundColor: [
					this.purpleColors[0].backgroundColor,
					this.blackColors[0].backgroundColor
				],
				borderColor: [
					this.purpleColors[0].borderColor,
					this.blackColors[0].backgroundColor
				]
			},
			{
				backgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBackgroundColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
				hoverBorderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
			},
		];

		this.scoreChartOptions = {
			events: [],
			title: {
				display: false
			},
			legend: {
				display: false
			},
			scales: {
				xAxes: [{
					stacked: true,
					ticks: {
						beginAtZero: true,
						min: 0,
						max: 100,
						stepSize: 20
					},
					gridLines: {
						zeroLineColor: 'transparent',
						zeroLineWidth: 0
					}
				}],
				yAxes: [{
					stacked: true,
					gridLines: {
						zeroLineColor: 'transparent',
						zeroLineWidth: 0
					},
					ticks: {
						fontColor: "#333",
						fontStyle: "600",
					}
				}]
			},
			tooltips: {
				enabled: false
			},
			plugins: {
				datalabels: {
					color: 'white'
				}
			}
		};
	}

	/**
	 * AFQT doughnut chart
	 */
	getAfqtScore() {
		const afqtScore = Number(this.scoreSummary.aFQTRawSSComposites.afqt);
		return afqtScore;
	};

	setAfqtScore() {
		this.afqtChartData = [{
			data: [this.getAfqtScore(), (100 - this.getAfqtScore())],
			backgroundColor: ['#4c44b4', '#f2f2f2'],
			datalabels: {
				display: false
			}
		}];

		this.afqtChartLabels = ['AFQT Score', 'Total'];
		this.afqtChartOptions = {
			events: [],
			tooltips: {
				enabled: false
			},
			elements: {
				arc: {
					borderWidth: 0
				}
			},
			animation: {
				duration: 0
			},
			legend: {
				display: false
			}
		};
	}

	moreInfo(area) {
		let modalOptions;
		switch (area) {
			case 'ces':
				modalOptions = {
					title: 'Career Exploration Scores',
					message: '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' +
						'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' +
						'<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' +
						'<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' +
						'<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' +
						'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
				};
				break;
			case 'cesStandard':
				modalOptions = {
					title: 'Career Exploration Scores',
					message: '<h4 class="navy">Standard Scores</h4>' +
						'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
				};
				break;
			case 'cesPercent':
				modalOptions = {
					title: 'Career Exploration Scores',
					message: '<h4 class="navy">Percentile Scores</h4>' +
						'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
				};
				break;
			case 'scoreBands':
				modalOptions = {
					title: 'Score Bands',
					message: '<p>On the ASVAB Summary Results sheet, the standard scores are shown in a graph with corresponding bands illustrating the range within which your scores would likely fall should you take the test again. You might want to focus your attention to any score bands that stand out (i.e., are located to the left or right of the other score bands). Such scores would suggest either a strength (to the right of the others) or a weakness (to the left of the others).</p>'
				};
				break;
			case 'asvabStandard':
				modalOptions = {
					title: 'ASVAB Test Scores',
					message: '<h4>Standard Scores</h4>' +
						'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
				};
				break;
			case 'asvabPercent':
				modalOptions = {
					title: 'ASVAB Test Scores',
					message: '<h4>Percentile Scores</h4>' +
						'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
				};
				break;
			case 'afqt':
				modalOptions = {
					title: 'Military Entrance Score (AFQT)',
					message: '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' +
						'<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' +
						'<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
				};
				break;
		}


		//show modal
		const dialogRef1 = this._dialog.open(MessageDialogComponent, {
			data: modalOptions,
			hasBackdrop: true,
			disableClose: false,
			// maxWidth: '400px'
		});
	};

	/***
 * Returns the Abbreviated Month with the corresponding month
 * Last Updated: 2/4/2019 - Changed from comparing string to int for cases
 */
	getAbbrMonth(monthStr): string {
		if (typeof monthStr === 'string') {
			monthStr = parseInt(monthStr);
		}
		switch (monthStr) {
			case 1: return 'Jan';
			case 2: return 'Feb';
			case 3: return 'Mar';
			case 4: return 'Apr';
			case 5: return 'May';
			case 6: return 'Jun';
			case 7: return 'Jul';
			case 8: return 'Aug';
			case 9: return 'Sep';
			case 10: return 'Oct';
			case 11: return 'Nov';
			case 12: return 'Dec';
			default:
				return '';
		}
	};


	/**
	 * Show video modal
	*/
	showVideoDialog(videoName) {
		this._dialog.open(PopupPlayerComponent, {
			data: { videoId: videoName }
		});
	}

	//sets the scroll to top display when threshold is met
	@HostListener('window:scroll')
	checkScroll() {

		const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

		if (scrollPosition >= this.scrollThreshold) {
			this.showScrollToTop = true;
		} else {
			this.showScrollToTop = false;
		}

	}

	/**
 * Get scores for display
 */
	getScore = function (area) {
		var score = 0;

		switch (area) {
			case "verbal":
				if (this.haveAsvabTestScores) {
					score = this.scoreSummary ? Number(this.scoreSummary.verbalAbility.va_SGS) : 0;
				} else if (this.haveManualAsvabTestScores) {
					score = Number(this.asvabScore[this.asvabScore.length - 1].verbalScore);
				}
				break;
			case "math":
				if (this.haveAsvabTestScores) {
					score = this.scoreSummary ? Number(this.scoreSummary.mathematicalAbility.ma_SGS) : 0;
				} else if (this.haveManualAsvabTestScores) {
					score = Number(this.asvabScore[this.asvabScore.length - 1].mathScore);
				}
				break;
			case "science":
				if (this.haveAsvabTestScores) {
					score = this.scoreSummary ? Number(this.scoreSummary.scienceTechnicalAbility.tec_SGS) : 0;
				} else if (this.haveManualAsvabTestScores) {
					score = Number(this.asvabScore[this.asvabScore.length - 1].scienceScore);
				}
				break;
			case "afqt": // no manual entry for AFQT
				score = this.scoreSummary ? Number(this.scoreSummary.aFQTRawSSComposites.afqt) : 0;
				break;
			default:
				break;
		}
		return score;
	}

	isObject(x: any): x is Object {
		return x != null && typeof x === 'object';
	}

	printScoreCard(isDownloadMode: boolean) {

		if (isDownloadMode) {
			this.triggerDownload();
		} else {
			this.triggerPrint();
		}

	}

	/**
	 * Print and download PDF.
	 * ael -> no longer used; html already in html
	 */
	createHTML() {
		/**
		 * CES Standard Scores Chart Data
		 */
		this.cesData1 = [
			this.getScore('verbal'),
			this.getScore('math'),
			this.getScore('science')
		]

		/**
		 * CES Percentile Scores
		 * display order female, male, all
		 * all others data use existing, e.g. asvabData2_1
		 */
		this.asvabData1 = [ // actual scores
			Number(this.scoreSummary.generalScience.gs_SGS) > 0 ? Number(this.scoreSummary.generalScience.gs_SGS) : 0,
			Number(this.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? Number(this.scoreSummary.arithmeticReasoning.ar_SGS) : 0,
			Number(this.scoreSummary.wordKnowledge.wk_SGS) > 0 ? Number(this.scoreSummary.wordKnowledge.wk_SGS) : 0,
			Number(this.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? Number(this.scoreSummary.paragraphComprehension.pc_SGS) : 0,
			Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? Number(this.scoreSummary.mathematicsKnowledge.mk_SGS) : 0,
			Number(this.scoreSummary.electronicsInformation.ei_SGS) > 0 ? Number(this.scoreSummary.electronicsInformation.ei_SGS) : 0,
			Number(this.scoreSummary.autoShopInformation.as_SGS) > 0 ? Number(this.scoreSummary.autoShopInformation.as_SGS) : 0,
			Number(this.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? Number(this.scoreSummary.mechanicalComprehension.mc_SGS) : 0,
		]

		this.testYear = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredYear) ? '' : this.scoreSummary.controlInformation.administeredYear;
		this.testDay = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredDay) ? '' : this.scoreSummary.controlInformation.administeredDay;
		this.testMonth = (this.haveAsvabTestScores && !this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.administeredMonth) ? '' : this.getAbbrMonth(this.scoreSummary.controlInformation.administeredMonth);

		this.testSchool = {
			"name": "",
			"city": "",
			"state": ""
		}
		this.testSchool.name = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolSiteName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolSiteName;
		this.testSchool.city = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolCityName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolCityName;
		this.testSchool.state = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolStateName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolStateName;

		this.educationLevel = (!this.scoreSummary.controlInformation || !this.scoreSummary.controlInformation.educationLevel) ? '' : this.scoreSummary.controlInformation.educationLevel;
		this.educationLevelGr = !this.educationLevel ? '' : this.scoreSummary.controlInformation.educationLevel + 'th Gr ';


		this.accessCodeExpiration = this._utilityService.convertUTCDateToLocalDate(new Date(this.scoreSummaryStudent.accessCodes.expireDate));

		// Top level element need some margin. Border should be more than 1px or else print/download won't show it correctly.
		// Adjust following attributes to have image showup when download or print:
		// * scoreHTML's top level table font-size.
		// * this.downloadPdf's image size. This determines how big the image is inside the pdf/print page.
		// * style.css' .extra-large-modal > .modal-dialog. This determines how big the modal is to fit the image to convert to pdf/print.
		let scoreHTML = '<div #asr id="asr" class="asr" style="background-color: white">' +
			'<div class="print-box">' +
			'<div class="print-header">' +
			'<div class="print-header-info">' +
			'<strong>Student</strong>' +
			'<p>' + this.educationLevelGr + this.genderFull + '</p>' +
			'<p>Test Date: ' + this.testMonth + ' ' + this.testDay + ', ' + this.testYear + '<br>' +
			this._utilityService.capitalizeFirstLetter(this.testSchool.name) + '<br>' +
			this._utilityService.capitalizeFirstLetter(this.testSchool.city) + '&nbsp;' + this.testSchool.state + '</p>' +
			'</div>' +
			'<div class="print-header-title">' +
			'<h2><span>ASVAB</span> Summary Results</h2>' +
			'</div>' +
			'</div>' +
			'<div class="print-content">' +
			'<div class="print-content-main">' +
			'<div class="print-content-results">' +
			'<div class="print-content-row">' +
			'<div class="print-content-col print-content-col-1">' +
			'<div class="print-content-col-head">' +
			'<h3>ASVAB Results</h3>' +
			'</div>' +
			'<h5>Career Exploration Scores</h5>' +
			'<ul>' +
			'<li>Verbal Skills</li>' +
			'<li>Math Skills</li>' +
			'<li>Science and Technical Skills</li>' +
			'</ul>' +
			'<h5>ASVAB Tests <span class="print-dashed">' +
			'</span></h5>' +
			'<ul>' +
			'<li>General Science</li>' +
			'<li>Arithmetic Reasoning</li>' +
			'<li>Word Knowledge</li>' +
			'<li>Paragraph Comprehension</li>' +
			'<li>Mathematics Knowledge</li>' +
			'<li>Electronics Information</li>' +
			'<li>Auto and Shop Information</li>' +
			'<li>Mechanical Comprehension</li>' +
			'</ul>' +
			'<h5>Military Entrance Score (AFQT)&nbsp; ' + this.getScore('afqt') + '</h5>' +
			'</div>' +
			'<div class="print-content-col print-content-col-2">' +
			'<div class="print-content-col-head">' +
			'<h5>Percentile Scores</h5>' +
			'<div class="print-content-col-scores-wrap">' +
			'<div class="print-content-col-scores col-1">' +
			'<h6>' + this.educationLevel + 'th Grade Males</h6>' +
			'</div>' +
			'<div class="print-content-col-scores col-2">' +
			'<h6>' + this.educationLevel + 'th Grade Females</h6>' +
			'</div>' +
			'<div class="print-content-col-scores col-3">' +
			'<h6>' + this.educationLevel + 'th Grade Students</h6>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-col-scores-wrap">' +
			'<div class="print-content-col-scores col-1">' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.cesData2_1[0].data[1] + '</li>' +
			'<li>' + this.cesData2_2[0].data[1] + '</li>' +
			'<li>' + this.cesData2_3[0].data[1] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.asvabData2_1[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_2[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_3[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_4[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_5[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_6[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_7[0].data[1] + '</li>' +
			'<li>' + this.asvabData2_8[0].data[1] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'</div>' +
			'<div class="print-content-col-scores col-2">' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.cesData2_1[0].data[0] + '</li>' +
			'<li>' + this.cesData2_2[0].data[0] + '</li>' +
			'<li>' + this.cesData2_3[0].data[0] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.asvabData2_1[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_2[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_3[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_4[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_5[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_6[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_7[0].data[0] + '</li>' +
			'<li>' + this.asvabData2_8[0].data[0] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'</div>' +
			'<div class="print-content-col-scores col-3">' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.cesData2_1[0].data[2] + '</li>' +
			'<li>' + this.cesData2_2[0].data[2] + '</li>' +
			'<li>' + this.cesData2_3[0].data[2] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.asvabData2_1[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_2[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_3[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_4[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_5[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_6[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_7[0].data[2] + '</li>' +
			'<li>' + this.asvabData2_8[0].data[2] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-col print-content-col-3">' +
			'<div class="print-content-col-head">' +
			'<h4>' + this.educationLevel + 'th Grade Standard Score Bands</h4>' +
			'</div>' +
			'<div class="print-content-col-bands">' +
			'<div class="print-content-col-range top">' +
			'<ul>' +
			'<li></li>' +
			'<li>20</li>' +
			'<li>30</li>' +
			'<li>40</li>' +
			'<li>50</li>' +
			'<li>60</li>' +
			'<li>70</li>' +
			'<li>80</li>' +
			'<li></li>' +
			'</ul>' +
			'</div>' +
			'<div class="print-content-col-range top">' +
			'<ul>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'<span class="verticle-class"></span>' +
			'</ul>' +
			'</div>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.cesData1[0]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.cesData1[1]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.cesData1[2]) + '%;">X</span></li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[0]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[1]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[2]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[3]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[4]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[5]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[6]) + '%;">X</span></li>' +
			'<li><span style="left: ' + this.scoreInterpolation(this.asvabData1[7]) + '%;">X</span></li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<div class="print-content-col-range bottom">' +
			'<ul>' +
			'<li></li>' +
			'<li>20</li>' +
			'<li>30</li>' +
			'<li>40</li>' +
			'<li>50</li>' +
			'<li>60</li>' +
			'<li>70</li>' +
			'<li>80</li>' +
			'<li></li>' +
			'</ul>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-col print-content-col-4">' +
			'<div class="print-content-col-head">' +
			'<h6>' + this.educationLevel + 'th Grade Standard Score</h6>' +
			'</div>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.cesData1[0] + '</li>' +
			'<li>' + this.cesData1[1] + '</li>' +
			'<li>' + this.cesData1[2] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'<ul>' +
			'<li>' + this.asvabData1[0] + '</li>' +
			'<li>' + this.asvabData1[1] + '</li>' +
			'<li>' + this.asvabData1[2] + '</li>' +
			'<li>' + this.asvabData1[3] + '</li>' +
			'<li>' + this.asvabData1[4] + '</li>' +
			'<li>' + this.asvabData1[5] + '</li>' +
			'<li>' + this.asvabData1[6] + '</li>' +
			'<li>' + this.asvabData1[7] + '</li>' +
			'</ul>' +
			'<h5>&nbsp;</h5>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-widget">' +
			'<h5 class="print-content-title">Explanation Of Your <br>ASVAB Percentile Scores</h5>' +
			'<p class="print-content-text">Your ASVAB results are reported as percentile scores in the three highlighted' +
			'columns to the left of the graph. Percentile scores show how you compare to other students - males and' +
			'females, and for all students - in your grade. For example, a percentile score of 65 for an 11th grade' +
			'female would mean she scored the same or better than 65 out of every 100 females in the 11th grade.</p>' +
			'<p class="print-content-text">For purposes of career planning, knowing your relative standing in these' +
			'comparison groups is important.Being male or female does not limit your career or educational choices.' +
			'There are noticeable differences in how men and women score in some areas. Viewing your scores in light of' +
			'your relative standing both to men and women may encourage you to explore areas that you might otherwise' +
			'overlook.</p>' +
			'<p class="print-content-text">You can use the <strong>Career Exploration Scores</strong> to evaluate your' +
			'knowledge and skills in three general areas (Verbal, Math, and Science and Technical Skills). You can use' +
			'the <strong>ASVAB Test Scores</strong> to gather information on specific skill areas. <i>Together, these' +
			'scores provide a snapshot of your current knowledge and skills.</i> This information will help you' +
			'develop and review your career goals and plans.</p>' +
			'<h5 class="print-content-title">Explanation Of Your <br>ASVAB Standard Scores</h5>' +
			'<p class="print-content-text">Your ASVAB results are reported as standard scores in the above graph.Your' +
			'score on each test is identified by the "<strong>X</strong>" in the corresponding bar graph. You should' +
			'view these scores as estimates of your true skill level in that area. If you took the test again, you' +
			'probably would receive a somewhat different score. Many things, such as how you were feeling during' +
			'testing, contribute to this difference. This difference is shown with gray score bands in the graph of' +
			'your results. Your standard scores are based on the ASVAB tests and composites based on your grade level.' +
			'</p>' +
			'<p class="print-content-text">The score bands provide a way to identify some of your strengths. Overlapping' +
			'score bands mean your true skill level is similar in both areas, so the real difference between specific' +
			'scores might not be meaningful. If the score bands do not overlap, you probably are stronger in the area' +
			'that has the higher score band.</p>' +
			'<p class="print-content-text">The ASVAB is an aptitude test. It is neither an absolute measure of your' +
			'skills and abilities nor a perfect predictor of your success or failure. A high score does not guarantee' +
			'success, and a low score does not guarantee failure, in a future educational program or occupation. For' +
			'example, if you have never worked with shop equipment or cars, you may not be familiar with the terms and' +
			'concepts assessed by the Auto and Shop Information test. Taking a course or obtaining a part-time job in' +
			'this area would increase your knowledge and improve your score if you were to take it again.</p>' +
			'<h5 class="print-content-title">Using ASVAB Results in <br>Career Exploration</h5>' +
			'<p class="print-content-text">Your career and educational plans may change over time as you gain more' +
			'experience and learn more about your interests. <i>Exploring Careers: The ASVAB Career Exploration' +
			'Guide</i> can help you learn more about yourself and the world of work,to identify and explore potential' +
			'goals, and develop an effective strategy to realize your goals. The Guide will help you identify occupa-' +
			'tions in line with your interests and skills. As you explore potentially satisfying careers, you will' +
			'develop your career exploration and planning skills.</p>' +
			'<p class="print-content-text">Meanwhile, your ASVAB results can help you in making well-informed choices' +
			'about future high school courses.</p>' +
			'<p class="print-content-text">We encourage you to discuss your ASVAB results with a teacher, counselor,' +
			'parent, family member or other interested adult. These individuals can help you to view your ASVAB results' +
			'in light of other important information, such as your interests, school grades, motivation, and personal' +
			'goals.</p>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-sidebar">' +
			'<div class="print-content-sidebar-top">' +
			'<div class="print-content-sidebar-widget">' +
			'<h5 class="print-content-title">Use Of Information</h5>' +
			'<p class="print-content-text">Personal identity information (name, social security number, street' +
			'address, and telephone number) and test scores will not be released to any agency outside of the' +
			'Department of Defense (DoD), the Armed Forces, the Coast Guard, and your school. Your school or local' +
			'school system can determine any further release of information. The DoD will use your scores for' +
			'recruiting and research purposes for up to two years.After that the information will be used by the DoD' +
			'for research purposes only.</p>' +
			'</div>' +
			'<div class="print-content-sidebar-widget">' +
			'<h5 class="print-content-title">Military <br>Entrance&nbspScores</h5>' +
			'<p class="print-content-text">The <strong>Military Entrance Score</strong> (also called AFQT, which stands' +
			'for the Armed Forces Qualification Test) is the score used to determine your qualifications for entry' +
			'into any branch of the United States Armed Forces or the Coast Guard. The <strong>Military Entrance' +
			' Score</strong> predicts in a general way how well you might do in training and on the job in military' +
			'occupations.Your score reflects your standing compared to American men and women 18 to 23 years of' +
			'age.</p>' +
			'</div>' +
			'</div>' +
			'<div class="print-content-sidebar-bottom">' +
			'<p style="padding-right: 10px">Use Access Code: ' + this.scoreSummaryStudent.accessCodes.accessCode + '</p>' +
			'<p>(for online Occu-Find and FYI)</p>' +
			'<p style="padding-right: 10px">Access code expires: ' + this.getAbbrMonth(this.accessCodeExpiration.getMonth() + 1) + ' ' + this.accessCodeExpiration.getDate() + ', ' + this.accessCodeExpiration.getFullYear() + '</p>' +
			'<div class="print-access">Explore career possibilities by using your Access Code at</div>' +
			'<div class="print-site-link"><i><a href="//www.asvabprogram.com">www.asvabprogram.com</a></i></div>' +
			'<div class="print-information"><i>See Your Counselor For Further Information</i></div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="print-footer">Dd Form 1304-5, Jul 05 - Previous Editions Of This Form Are Obsolete</div>' +
			'</div>'

		return scoreHTML;
	}

	// Score to plot mapping:
	// 11 - 20: -1 - 11
	// 21 - 30: 12 - 24
	// 31 - 40: 25 - 37
	// 41 - 50: 38 - 50
	// 51 - 60: 51 - 63
	// 61 - 70: 64 - 76
	// 71 - 80: 77 - 89
	// 81 - 90: 90 - 102
	scoreInterpolation(score): string {
		let result: any;
		switch (true) {
			case score < 11:
				result = -1;
				break;
			case 11 <= score && score <= 20:
				result = Math.ceil((score - 11) / 10 * 13 - 1);
				break;
			case 21 <= score && score <= 30:
				result = Math.ceil((score - 21) / 10 * 13 + 12);
				break;
			case 31 <= score && score <= 40:
				result = Math.ceil((score - 31) / 10 * 13 + 25);
				break;
			case 41 <= score && score <= 50:
				result = Math.ceil((score - 41) / 10 * 13 + 38);
				break;
			case 51 <= score && score <= 60:
				result = Math.ceil((score - 51) / 10 * 13 + 51);
				break;
			case 61 <= score && score <= 70:
				result = Math.ceil((score - 61) / 10 * 13 + 64);
				break;
			case 71 <= score && score <= 80:
				result = Math.ceil((score - 71) / 10 * 13 + 77);
				break;
			case 81 <= score && score <= 90:
				result = Math.ceil((score - 81) / 10 * 13 + 90);
				break;
			case score > 90:
				result = 102;
				break;

		}
		return result.toString() + '%'
	}

	getCanvas() {
		let self = this;
		return new Promise((resolve, reject) => {
			html2canvas(this.printMeElem.nativeElement, {
				imageTimeout: 2000,
				removeContainer: true,
				backgroundColor: '#fff',
				allowTaint: true,
				logging: false,
				ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
					return node.nodeName === 'IFRAME';
				},
				onclone: function (cloneDoc) {
					let printSection: HTMLElement = cloneDoc.getElementById("printMe");
					printSection.style.display = 'block';
				}
			}).then(function (canvas) {
				var img = canvas.toDataURL("image/jpeg");
				resolve(img);
			}, function (error) {
				reject(error);
			});
		});

	}

	triggerDownload() {
		let t = this.getCanvas();
		let self = this;
		t.then(function (s) {
			let docDefinition = {
				pageOrientation: 'landscape',
				pageSize: 'LETTER',
				content: [{
					image: s,
					alignment: 'center',
					fit: [1600, 1200]
				}]
			};



			pdfMake.createPdf(docDefinition).download(self.studentName.replace(' ', '_') + '_ASR.pdf');

			// pdfMake.createPdf(docDefinition).download('ASR.pdf');
		}, function (error) {
			console.error("ERROR", error);
			alert(error);
		});
	}

	async triggerPrint() {

		// IE support
		if (this.isInternetExplorer() === true) {
			let x = await this.getCanvasIE();

			let popup = window.open();
			popup.document.write('<img src=' + x + '>');
			popup.document.close();
			popup.focus();
			popup.print();
			popup.close();
			return false;
		}

		//NOT IE
		try {
			let t = await this.getCanvas();
			let docDefinition = {
				pageOrientation: 'landscape',
				pageSize: 'LETTER',
				content: [{
					image: t,
					alignment: 'center',
					fit: [1600, 1200]
				}]
			};

			pdfMake.createPdf(docDefinition).print();
		} catch (error) {
			console.error("ERROR", error);
			alert(error);
		}

	}

	getCanvasIE() {
		return new Promise((resolve, reject) => {
			html2canvas(this.printMeElem.nativeElement, {
				imageTimeout: 2000,
				backgroundColor: '#fff',
				allowTaint: true,
				removeContainer: true,
				logging: false,
				scale: 1,
				ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
					return node.nodeName === 'IFRAME';
				},
				onclone: function (cloneDoc) {
					let printSection: HTMLElement = cloneDoc.getElementById("printMe");
					printSection.style.display = 'block';

					// needed so the image doesn't show half way off the canvas
					let asr: HTMLElement = cloneDoc.getElementById("asr");
					asr.style.margin = "0";
				}
			}).then(function (canvas) {
				var img = canvas.toDataURL("image/jpeg");
				resolve(img);
			}, function (error) {
				reject(error);
			});
		});

	}

	isInternetExplorer(): boolean {
		if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
			return true;
		} else {
			return false;
		}
	}

	SetCompletion() {
		this._userService.setCompletion(UserService.VISITED_MILITARY_LINE_SCORE, 'true');
	}
}
