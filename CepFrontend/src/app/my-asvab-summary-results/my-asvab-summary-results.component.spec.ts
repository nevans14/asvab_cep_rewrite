import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAsvabSummaryResultsComponent } from './my-asvab-summary-results.component';

describe('MyAsvabSummaryResultsComponent', () => {
  let component: MyAsvabSummaryResultsComponent;
  let fixture: ComponentFixture<MyAsvabSummaryResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAsvabSummaryResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAsvabSummaryResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
