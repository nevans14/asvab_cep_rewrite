import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService } from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';

@Component({
  selector: 'app-sso-login',
  templateUrl: './sso-login.component.html',
  styleUrls: ['./sso-login.component.scss']
})
export class SsoLoginComponent implements OnInit {
  action: String;
  redirectPage: String;
  data: String;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _utilityService: UtilityService,
    private _config: ConfigService
  ) { }

  ngOnInit() {
    this.action = this._router.url.indexOf('/sso-login') > -1 ? 'login' : 'logoff';
  if (this.action === 'login') {
    this.ssoLogin();
  } else {
    this.ssoLogoff();
  }
    
  this._route.params.subscribe( res => {
    this.redirectPage = res.redirect;
    this.data = res.data;
  } );

 }

 ssoLogin() {
  console.debug('Pocess CEP SSO Login');
  if (this.data && this.data !== 'null') {
      const cookieData = JSON.parse(this._utilityService.urlDecode(this.data));
      this._utilityService.createCookie(cookieData);
    }
    window.location.href = this._config.getCitmBaseUrl() +  this.redirectPage;
 }

 ssoLogoff() {
   console.debug('Process Logoff');
   this._utilityService.deleteCookie();
   this._utilityService.clearSessionStorage();
   window.location.href = this._config.getCitmBaseUrl() + 'home';
 }

}
