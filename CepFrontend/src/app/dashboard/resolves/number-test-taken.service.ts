import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { FindYourInterestService } from '../../services/find-your-interest.service';

@Injectable( {
    providedIn: 'root'
} )
export class NumberTestTakenService implements Resolve<any> {

    constructor( private _FYIScoreService: FindYourInterestService ) { }

    resolve() {
        return this._FYIScoreService.getNumberOfTestTaken();
    }
}
