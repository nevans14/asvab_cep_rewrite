import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UserService } from 'app/services/user.service';
import { FindYourInterestService } from '../../services/find-your-interest.service';

@Injectable( {
    providedIn: 'root'
} )
export class TiedLinkResolveService implements Resolve<any>{

    constructor( private _httpHelper: HttpHelperService,
        private _user: UserService,
        private _FYIScoreService: FindYourInterestService ) { }

    resolve() {
        // If there is no top interest codes then there is not a tied
        // score. Set top interest code to undefined.
        var userRole = this._user.getUser().currentUser.role;

        /**
         * Check to see if a test with tied scores exist for user.
         */
        var promise = new Promise(( resolve, reject ) => {
            // Rest call that returns calculated combined and gender scores.
            this._httpHelper.httpHelper( 'GET', 'fyi/CombinedAndGenderScores', null, null ).then(( success ) => {

                var scores = success;

                // Check to see if there are tied scores.
                var isCombinedScoresTied = this._FYIScoreService.isTiedScore( this._FYIScoreService.getSortedCombinedScore( scores ), 'combined' );
                var isGenderScoresTied = this._FYIScoreService.isTiedScore( this._FYIScoreService.getSortedGenderScore( scores ), 'gender' );

                // Reset values
                this._FYIScoreService.setCombinedTiedScoreObject( undefined );
                this._FYIScoreService.setGenderTiedScoreObject( undefined );

                var isTiedObject = {
                    isCombinedScoresTied: isCombinedScoresTied,
                    isGenderScoresTied: isGenderScoresTied
                }

                resolve( isTiedObject );

            }, ( error ) => {
                reject( error );
            } );
        } );

        return promise;


    }
}
