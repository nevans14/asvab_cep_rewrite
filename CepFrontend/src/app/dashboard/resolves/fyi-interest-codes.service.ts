import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';

@Injectable( {
    providedIn: 'root'
} )
export class FyiInterestCodesService implements Resolve<any>  {

    constructor( private _user: UserService ) { }

    resolve() {
        return this._user.getUserInterestCodes();
    }
}
