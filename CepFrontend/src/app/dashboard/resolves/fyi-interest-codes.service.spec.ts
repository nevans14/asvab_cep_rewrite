import { TestBed } from '@angular/core/testing';

import { FyiInterestCodesService } from './fyi-interest-codes.service';

describe('FyiInterestCodesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FyiInterestCodesService = TestBed.get(FyiInterestCodesService);
    expect(service).toBeTruthy();
  });
});
