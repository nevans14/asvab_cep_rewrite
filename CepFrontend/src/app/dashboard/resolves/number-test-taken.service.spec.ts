import { TestBed } from '@angular/core/testing';

import { NumberTestTakenService } from './number-test-taken.service';

describe('NumberTestTakenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumberTestTakenService = TestBed.get(NumberTestTakenService);
    expect(service).toBeTruthy();
  });
});
