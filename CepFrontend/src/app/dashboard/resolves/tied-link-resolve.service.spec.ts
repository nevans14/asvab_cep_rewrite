import { TestBed } from '@angular/core/testing';

import { TiedLinkResolveService } from './tied-link-resolve.service';

describe('TiedLinkResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TiedLinkResolveService = TestBed.get(TiedLinkResolveService);
    expect(service).toBeTruthy();
  });
});
