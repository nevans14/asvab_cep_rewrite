import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentManagementService } from 'app/services/content-management.service';
@Component({
  selector: 'app-legislation',
  templateUrl: './legislation.component.html',
  styleUrls: ['./legislation.component.scss']
})
export class LegislationComponent implements OnInit {

  path = '';
  pageHtml = [];

  constructor(
    private _router: Router,
    private _contentManagementService: ContentManagementService,
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

}
