import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorStudentDetailComponent } from './mentor-student-detail.component';

describe('MentorStudentDetailComponent', () => {
  let component: MentorStudentDetailComponent;
  let fixture: ComponentFixture<MentorStudentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorStudentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorStudentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
