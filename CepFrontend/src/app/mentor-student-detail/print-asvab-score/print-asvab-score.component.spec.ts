import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintAsvabScoreComponent } from './print-asvab-score.component';

describe('PrintAsvabScoreComponent', () => {
  let component: PrintAsvabScoreComponent;
  let fixture: ComponentFixture<PrintAsvabScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintAsvabScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintAsvabScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
