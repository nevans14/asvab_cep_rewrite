import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MentorService } from 'app/services/mentor.service';
import { UtilityService } from 'app/services/utility.service';
import html2canvas from "html2canvas";

declare var pdfMake: any;
@Component({
  selector: 'app-print-asvab-score',
  templateUrl: './print-asvab-score.component.html',
  styleUrls: ['./print-asvab-score.component.scss']
})
export class PrintAsvabScoreComponent implements OnInit {

  isLoading: boolean;
  scoreSummaryStudent: any;
  cesData1: any[];
  asvabData1: number[];
  testYear: any;
  testDay: any;
  testMonth: any;
  haveAsvabTestScores: boolean;
  testSchool: { name: string; city: string; state: string; };
  educationLevel: any;
  educationLevelGr: string;
  accessCodeExpiration: any;
  mentorLinkId: any;
  printObj: {};
  asvabScore: any;
  cesData2_1: { data: number[]; barThickness: number; }[];
  chartRef: any;
  gender: string;
  cesData2_2: { data: number[]; barThickness: number; }[];
  cesData2_3: { data: number[]; barThickness: number; }[];
  asvabData2_1: { data: number[]; }[];
  asvabData2_2: { data: number[]; }[];
  asvabData2_3: { data: number[]; }[];
  asvabData2_4: { data: number[]; }[];
  asvabData2_5: { data: number[]; }[];
  asvabData2_6: { data: number[]; }[];
  asvabData2_7: { data: number[]; }[];
  asvabData2_8: { data: number[]; }[];
  lowVals: any[];
  highVals: number[];
  cesLabels1_1: (string | string[])[];
  genderFull: string;
  studentName: string;

  constructor(public dialogRef: MatDialogRef<PrintAsvabScoreComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _mentorService: MentorService,
    private _utilityService: UtilityService) {

    this.isLoading = true;

    this.mentorLinkId = this.data['mentorLinkId'];
    this.asvabScore = this.data['asvabScore'];
    this.studentName = this.data['studentName'];

    this.printObj = {};

  }

  ngOnInit() {

    this.dialogRef.updateSize("400px", "200px");
    
    Promise.resolve(this._mentorService.getStudentAsvabScores(this.mentorLinkId).then((response: any) => {

      //set gender
      if (response.controlInformation
        && response.controlInformation.gender != undefined
      ) {
        this.gender = response.controlInformation.gender.substring(0, 1).toLowerCase();
        if (this.gender === 'f') {
          this.genderFull = 'Female'
        } else {
          this.genderFull = 'Male'
        }
      } else {
        this.gender = 'm';
        this.genderFull = 'Male';
      }

      this.haveAsvabTestScores = (this.isObject(response) && response.hasOwnProperty('verbalAbility')); // do we have actual test scores

      this.setupPrint(response).then(() => {
        setTimeout(() => this.triggerDownload());
        this.dialogRef.close();
        this.isLoading = false;
      }).catch(error => {
        this.isLoading = false;
        console.error("ERROR", error);
      });

    }).catch((error: any) => {
      console.error("ERROR", error);
      this.isLoading = false;
    }));

  }

  async setupPrint(scoreSummaryStudent) {

    try {
      this.scoreSummaryStudent = scoreSummaryStudent;

      this.cesData1 = [
        this.getScore('verbal'),
        this.getScore('math'),
        this.getScore('science')
      ]

      /**
       * CES Percentile Scores
       * display order female, male, all
       * all others data use existing, e.g. asvabData2_1
       */
      this.asvabData1 = [ // actual scores
        Number(this.scoreSummaryStudent.generalScience.gs_SGS) > 0 ? Number(this.scoreSummaryStudent.generalScience.gs_SGS) : 0,
        Number(this.scoreSummaryStudent.arithmeticReasoning.ar_SGS) > 0 ? Number(this.scoreSummaryStudent.arithmeticReasoning.ar_SGS) : 0,
        Number(this.scoreSummaryStudent.wordKnowledge.wk_SGS) > 0 ? Number(this.scoreSummaryStudent.wordKnowledge.wk_SGS) : 0,
        Number(this.scoreSummaryStudent.paragraphComprehension.pc_SGS) > 0 ? Number(this.scoreSummaryStudent.paragraphComprehension.pc_SGS) : 0,
        Number(this.scoreSummaryStudent.mathematicsKnowledge.mk_SGS) > 0 ? Number(this.scoreSummaryStudent.mathematicsKnowledge.mk_SGS) : 0,
        Number(this.scoreSummaryStudent.electronicsInformation.ei_SGS) > 0 ? Number(this.scoreSummaryStudent.electronicsInformation.ei_SGS) : 0,
        Number(this.scoreSummaryStudent.autoShopInformation.as_SGS) > 0 ? Number(this.scoreSummaryStudent.autoShopInformation.as_SGS) : 0,
        Number(this.scoreSummaryStudent.mechanicalComprehension.mc_SGS) > 0 ? Number(this.scoreSummaryStudent.mechanicalComprehension.mc_SGS) : 0,
      ]

      this.testYear = (this.haveAsvabTestScores && !this.scoreSummaryStudent.controlInformation || !this.scoreSummaryStudent.controlInformation.administeredYear) ? '' : this.scoreSummaryStudent.controlInformation.administeredYear;
      this.testDay = (this.haveAsvabTestScores && !this.scoreSummaryStudent.controlInformation || !this.scoreSummaryStudent.controlInformation.administeredDay) ? '' : this.scoreSummaryStudent.controlInformation.administeredDay;
      this.testMonth = (this.haveAsvabTestScores && !this.scoreSummaryStudent.controlInformation || !this.scoreSummaryStudent.controlInformation.administeredMonth) ? '' : this.getAbbrMonth(this.scoreSummaryStudent.controlInformation.administeredMonth);

      this.testSchool = {
        "name": "",
        "city": "",
        "state": ""
      }
      this.testSchool.name = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolSiteName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolSiteName;
      this.testSchool.city = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolCityName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolCityName;
      this.testSchool.state = (this.haveAsvabTestScores && !this.scoreSummaryStudent.highSchoolInformation || !this.scoreSummaryStudent.highSchoolInformation.schoolStateName) ? '' : this.scoreSummaryStudent.highSchoolInformation.schoolStateName;

      this.educationLevel = (!this.scoreSummaryStudent.controlInformation || !this.scoreSummaryStudent.controlInformation.educationLevel) ? '' : this.scoreSummaryStudent.controlInformation.educationLevel;
      this.educationLevelGr = !this.educationLevel ? '' : this.scoreSummaryStudent.controlInformation.educationLevel + 'th Gr ';

      this.accessCodeExpiration = this._utilityService.convertUTCDateToLocalDate(new Date(this.scoreSummaryStudent.accessCodes.expireDate));

      this.setChartData();

      return Promise.resolve();

    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }
  }

  getScore(area) {
    var score = 0;

    switch (area) {
      case "verbal":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummaryStudent ? Number(this.scoreSummaryStudent.verbalAbility.va_SGS) : 0;
        } else {
          score = this.asvabScore.verbalScore;
        }
        break;
      case "math":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummaryStudent ? Number(this.scoreSummaryStudent.mathematicalAbility.ma_SGS) : 0;
        } else {
          score = this.asvabScore.mathScore;
        }
        break;
      case "science":
        if (this.haveAsvabTestScores) {
          score = this.scoreSummaryStudent ? Number(this.scoreSummaryStudent.scienceTechnicalAbility.tec_SGS) : 0;
        } else {
          score = this.asvabScore.scienceScore;
        }
        break;
      case "afqt": // no manual entry for AFQT
        score = this.scoreSummaryStudent ? Number(this.scoreSummaryStudent.afqtRawComposites.afqt) : 0;
        break;
      default:
        break;
    }
    return score;
  }

  triggerDownload() {
    let t = this.getCanvas();
    let self = this;
    t.then(function (s) {
      let docDefinition = {
        pageOrientation: 'landscape',
        pageSize: 'A3',
        pageMargins: [80, 10, 10, 10],
        content: [{
          image: s,
        }]
      };
      pdfMake.createPdf(docDefinition).download(self.studentName.replace(' ', '_') + '_ASR.pdf');
    }, function (error) {
      console.error("ERROR", error);
      alert(error);
    });
  }

  getCanvas() {
    const tag = document.getElementById('printMe');
    tag.style.opacity = "1.0";
    return new Promise((resolve, reject) => {
      html2canvas(tag, {
        imageTimeout: 2000,
        removeContainer: true,
        backgroundColor: '#fff',
        allowTaint: true,
        logging: false,
        scale: 1,
        ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
          return node.nodeName === 'IFRAME';
        }
      }).then(function (canvas) {
        var img = canvas.toDataURL("image/jpeg");
        tag.style.opacity = "0.0";
        resolve(img);
      }, function (error) {
        reject(error);
      });
    });

  }

  /***
* Returns the Abbreviated Month with the corresponding month
* Last Updated: 2/4/2019 - Changed from comparing string to int for cases
*/
  getAbbrMonth(monthStr): string {
    if (typeof monthStr === 'string') {
      monthStr = parseInt(monthStr);
    }
    switch (monthStr) {
      case 1: return 'Jan';
      case 2: return 'Feb';
      case 3: return 'Mar';
      case 4: return 'Apr';
      case 5: return 'May';
      case 6: return 'Jun';
      case 7: return 'Jul';
      case 8: return 'Aug';
      case 9: return 'Sep';
      case 10: return 'Oct';
      case 11: return 'Nov';
      case 12: return 'Dec';
      default:
        return '';
    }
  };

  // Score to plot mapping:
  // 11 - 20: -1 - 11
  // 21 - 30: 12 - 24
  // 31 - 40: 25 - 37
  // 41 - 50: 38 - 50
  // 51 - 60: 51 - 63
  // 61 - 70: 64 - 76
  // 71 - 80: 77 - 89
  // 81 - 90: 90 - 102
  scoreInterpolation(score): string {
    let result: any;
    switch (true) {
      case score < 11:
        result = -1;
        break;
      case 11 <= score && score <= 20:
        result = Math.ceil((score - 11) / 10 * 13 - 1);
        break;
      case 21 <= score && score <= 30:
        result = Math.ceil((score - 21) / 10 * 13 + 12);
        break;
      case 31 <= score && score <= 40:
        result = Math.ceil((score - 31) / 10 * 13 + 25);
        break;
      case 41 <= score && score <= 50:
        result = Math.ceil((score - 41) / 10 * 13 + 38);
        break;
      case 51 <= score && score <= 60:
        result = Math.ceil((score - 51) / 10 * 13 + 51);
        break;
      case 61 <= score && score <= 70:
        result = Math.ceil((score - 61) / 10 * 13 + 64);
        break;
      case 71 <= score && score <= 80:
        result = Math.ceil((score - 71) / 10 * 13 + 77);
        break;
      case 81 <= score && score <= 90:
        result = Math.ceil((score - 81) / 10 * 13 + 90);
        break;
      case score > 90:
        result = 102;
        break;

    }
    return result.toString() + '%'
  }

  isObject(x: any): x is Object {
    return x != null && typeof x === 'object';
  }

  getLow(num1, num2) {
    return Math.min(num1, num2);
  };

  getHigh(num1, num2) {
    return Math.max(num1, num2);
  };

  /**
 * CES Percentile Scores
 * display order female, male, all
 */
  setChartData() {
    this.chartRef = this.scoreSummaryStudent.verbalAbility;

    this.cesData2_1 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.va_GS_Percentage) : Number(this.chartRef.va_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.va_GS_Percentage) : Number(this.chartRef.va_GOS_Percentage),
        Number(this.chartRef.va_COMP_Percentage)
      ],
      barThickness: 16
    }];

    this.chartRef = this.scoreSummaryStudent.mathematicalAbility;

    this.cesData2_2 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.ma_GS_Percentage) : Number(this.chartRef.ma_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.ma_GS_Percentage) : Number(this.chartRef.ma_GOS_Percentage),
        Number(this.chartRef.ma_COMP_Percentage)
      ],
      barThickness: 16
    }];
    this.chartRef = this.scoreSummaryStudent.scienceTechnicalAbility;
    this.cesData2_3 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.tec_GS_Percentage) : Number(this.chartRef.tec_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.tec_GS_Percentage) : Number(this.chartRef.tec_GOS_Percentage),
        Number(this.chartRef.tec_COMP_Percentage)
      ],
      barThickness: 16
    }];

    /**
   * ASVAB Percentile Scores
   * display order femmale, male, all
   */
    this.chartRef = this.scoreSummaryStudent.generalScience;
    this.asvabData2_1 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.gs_GS_Percentage) : Number(this.chartRef.gs_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.gs_GS_Percentage) : Number(this.chartRef.gs_GOS_Percentage),
        Number(this.chartRef.gs_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.arithmeticReasoning;
    this.asvabData2_2 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.ar_GS_Percentage) : Number(this.chartRef.ar_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.ar_GS_Percentage) : Number(this.chartRef.ar_GOS_Percentage),
        Number(this.chartRef.ar_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.wordKnowledge;
    this.asvabData2_3 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.wk_GS_Percentage) : Number(this.chartRef.wk_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.wk_GS_Percentage) : Number(this.chartRef.wk_GOS_Percentage),
        Number(this.chartRef.wk_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.paragraphComprehension;
    this.asvabData2_4 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.pc_GS_Percentage) : Number(this.chartRef.pc_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.pc_GS_Percentage) : Number(this.chartRef.pc_GOS_Percentage),
        Number(this.chartRef.pc_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.mathematicsKnowledge;
    this.asvabData2_5 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.mk_GS_Percentage) : Number(this.chartRef.mk_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.mk_GS_Percentage) : Number(this.chartRef.mk_GOS_Percentage),
        Number(this.chartRef.mk_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.electronicsInformation;
    this.asvabData2_6 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.ei_GS_Percentage) : Number(this.chartRef.ei_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.ei_GS_Percentage) : Number(this.chartRef.ei_GOS_Percentage),
        Number(this.chartRef.ei_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.autoShopInformation;
    this.asvabData2_7 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.as_GS_Percentage) : Number(this.chartRef.as_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.as_GS_Percentage) : Number(this.chartRef.as_GOS_Percentage),
        Number(this.chartRef.as_COMP_Percentage)
      ]
    }
    ];
    this.chartRef = this.scoreSummaryStudent.mechanicalComprehension;
    this.asvabData2_8 = [{
      data: [
        this.gender == 'f' ? Number(this.chartRef.mc_GS_Percentage) : Number(this.chartRef.mc_GOS_Percentage),
        this.gender == 'm' ? Number(this.chartRef.mc_GS_Percentage) : Number(this.chartRef.mc_GOS_Percentage),
        Number(this.chartRef.mc_COMP_Percentage)
      ]
    }
    ];

  }

}
