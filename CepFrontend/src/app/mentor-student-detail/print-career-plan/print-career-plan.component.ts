import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserGapYearPlan } from 'app/core/models/userGapYearPlan.model';
import { UserMilitaryPlan } from 'app/core/models/userMilitaryPlan.model';
import { UserMilitaryPlanRotc } from 'app/core/models/userMilitaryPlanRotc.model';
import { UserMilitaryPlanServiceCollege } from 'app/core/models/userMilitaryPlanServiceCollege.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { IpedsService } from 'app/services/ipeds.service';
import { MentorService } from 'app/services/mentor.service';
import { MilitaryPlanService } from 'app/services/military-plan.service';
import { UtilityService } from 'app/services/utility.service';
import html2canvas from "html2canvas";

declare var pdfMake: any;

@Component({
  selector: 'app-print-career-plan',
  templateUrl: './print-career-plan.component.html',
  styleUrls: ['./print-career-plan.component.scss']
})
export class PrintCareerPlanComponent implements OnInit {

  public isLoading: boolean;
  private mentorLinkId: any;
  private planId: any;
  public printObj: any;
  private militaryOccupationTitles: any;
  private favorites: any;
  private serviceColleges: any;
  private planeTitle: any;
  studentName: string;
  mobileOrTablet: boolean = false;

  constructor(public dialogRef: MatDialogRef<PrintCareerPlanComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _mentorService: MentorService,
    private _utilityService: UtilityService,
    private _militaryPlanService: MilitaryPlanService,
    private _occufindRestFactory: OccufindRestFactoryService,
    private _portfolioRestFactory: PortfolioRestFactoryService,
    private _ipedsService: IpedsService) {
    this.isLoading = true;

    this.planId = this.data['planId'];
    this.mentorLinkId = this.data['mentorLinkId'];
    this.planeTitle = this.data['title'];
    this.studentName = this.data['studentName'];
    this.printObj = {};

    this.mobileOrTablet = this._utilityService.mobileOrTablet();
  }

  ngOnInit() {

    Promise.resolve(this.getStudentCareerPlanInfo(this.planId)).then((results: any) => {

      //get any other data needed for lookups
      let militaryTitles = this.getMilitaryTitles(results.socId);

      let favorites = this.getCitmFavoriteOccupations();

      let serviceColleges = this.getServiceColleges();

      Promise.all([militaryTitles, favorites, serviceColleges]).then((done: any) => {

        this.militaryOccupationTitles = done[0];
        this.favorites = done[1];
        this.serviceColleges = done[2];

        this.setPrintElements(results).then((success) => {
          setTimeout(() => this.triggerDownload());

          this.isLoading = false;
          this.dialogRef.close();
        }).catch(error => {
          this.isLoading = false;
          console.error("ERROR", error);
        });

      })

    }).catch(error => {
      this.isLoading = false;
      console.error("ERROR", error);
    });

  }

  async getCitmFavoriteOccupations() {
    try {
      let response = await this._portfolioRestFactory.getCitmFavoriteOccupationsNF();
      return await response;
    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }

  }

  async getMilitaryTitles(socId: any) {

    try {

      let militaryOccupationTitles = await this._occufindRestFactory.getMilitaryMoreDetails(socId).toPromise();

      return await militaryOccupationTitles;

    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }

  }

  async setPrintElements(results: any) {

    try {
      this.printObj.title = this.planeTitle;
      this.printObj.salary = (results.averageSalary) ? this._utilityService.convertToUSDCurrency(results.averageSalary) : "0";

      if (results.useNationalAverage) {
        this.printObj.salaryText = "National Average Starting Salary";
      } else {
        this.printObj.salaryText = "Starting Average Salary";
      }

      this.printObj.userSchoolPlan = results.schoolPlan;

      // this.printObj.userGapYearPlans = results.gapYearPlans;
      this.printObj.userGapYearPlans = [];
      if (results.gapYearPlans && results.gapYearPlans.length > 0) {
        //setup selected types
        results.gapYearPlans.forEach((gapYearPlan: any) => {
          this.printObj.userGapYearPlans.push({ ...gapYearPlan, ...this.setGapYearInfo(gapYearPlan) });
        });
      }

      this.printObj.userCollegePlans = [];
      if (results.collegePlans && results.collegePlans.length > 0) {
        //setup sources
        results.collegePlans.forEach((collegePlan: any) => {
          this.printObj.userCollegePlans.push({ ...collegePlan, ...this.setCollegePlanSources(collegePlan) });
        });
      }

      this.printObj.userWorkPlan = results.workPlan;
      this.printObj.additionalNotes = results.additionalNotes;

      this.printObj.userMilitaryPlans = [];
      this.printObj.userMilitaryPlans = await this.getUserMilitaryPlansInfo(results.militaryPlans);

      this.printObj.userCalendarDates = results.calendarDates;
      //add new property 'expanded' to calendar class
      //this will control whether a date can show its tasks to the user
      //default to true
      if (this.printObj.userCalendarDates) {
        this.printObj.userCalendarDates.forEach(element => {
          element.expanded = true;
        });
      }

      return Promise.resolve(this.printObj);

    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }
  }

  async getServiceColleges() {
    let services: string[] = ['A', 'F', 'C', 'M', 'N', 'S']
    //retrieve all service colleges
    //filter out within the specific user plan
    try {
      let serviceColleges: any = await this._ipedsService.getServiceColleges(services);

      return await serviceColleges;


    } catch (error) {
      console.error("ERROR:", error);
      return Promise.reject();
    }
  }

  async getUserMilitaryPlansInfo(userMilitaryPlans: any) {

    try {

      return Promise.all(userMilitaryPlans.map(async (userMilitaryPlan: any) => {

        let careerInfo = await this.setMilitaryPlanInfo(userMilitaryPlan);

        return await { ...userMilitaryPlan, careerInfo };

      }));

    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }

  }

  async getStudentCareerPlanInfo(planId) {

    try {

      let careerPlans = await this._mentorService.getStudentCareerPlanInfo(this.mentorLinkId, planId);

      return await careerPlans;

    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }

  }

  setGapYearInfo(userGapYearPlan: UserGapYearPlan) {

    let types: string[] = [];
    if (userGapYearPlan.types) {
      for (let x of userGapYearPlan.types) {
        if (x.isSelected) {
          types.push(x.name);
        }
      }
    }
    return ({ selectedTypes: types.join(', ') });
  }

  triggerDownload() {
    this.getCanvas().then((s: any) => {

      let docDefinition = {
        pageOrientation: 'portrait',
        pageSize: 'LETTER',
        content: s
      };

      pdfMake.createPdf(docDefinition).download(this.studentName.replace(' ', '_') + '_' + String(this.printObj.title).replace(' ', "_") + '_plan.pdf');

    }).catch(error => {
      console.error("ERROR", error);
      alert(error);
    });
  }

  triggerPrint() {
    this.getCanvas().then((s: any) => {

      let docDefinition = {
        pageOrientation: 'portrait',
        pageSize: 'LETTER',
        content: s
      };

      pdfMake.createPdf(docDefinition).print();

    }).catch(error => {
      console.error("ERROR", error);
      alert(error);
    });
  }

  getCanvas() {
    let self = this;

    return new Promise((resolve, reject) => {
      const tag = document.getElementById('printMe');
      tag.style.opacity = "1.0";

      const elements = document.querySelectorAll('.task_list_item_expanded');
      if (elements) {
        elements.forEach(x => x.className = "task_list_item_expanded show-expand")
      }

      html2canvas(tag, {
        imageTimeout: 2000,
        removeContainer: true,
        backgroundColor: '#fff',
        allowTaint: true,
        logging: false,
        scale: 1,
        ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
          return node.nodeName === 'IFRAME';
        }
      }).then(function (canvas) {

        let splitAt = self.mobileOrTablet ? 540 : 1050;

        let images = [];
        let y = 0;
        while (canvas.height > y) {
          const region = self.getClippedRegion(canvas, 0, y, canvas.width, splitAt)

          // Stop generate image if blank
          if (!region.image) {
            break;
          }

          images.push(region);
          y += splitAt;
        }
        const elements = document.querySelectorAll('.task_list_item_expanded show-expand');
        if (elements) {
          elements.forEach(x => x.className = "task_list_item_expanded")
        }
        tag.style.opacity = "0.0";

        resolve(images);
      }).catch(function (error) {
        reject(error);
      });
    });

  }

  getClippedRegion(image, x, y, width, height) {
    var canvas = document.createElement("canvas"),
      ctx = canvas.getContext("2d");

    canvas.width = width;
    canvas.height = height;

    ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

    let contentLocation = ctx.getImageData(0, 0, canvas.width, canvas.height).data.find(x => x !== 255);

    if (!contentLocation) {
        return { image: undefined };
    }

    return {
      image: canvas.toDataURL(),
      width: 520,
      alignment: 'center'
    };
  }

  getResourceLinks(userMilitaryPlans: any[]) {

    let returnResourceLinks = [];
    let didSelectAirForce: boolean = false;
    let didSelectArmy: boolean = false;
    let didSelectCoastGuard: boolean = false;
    let didSelectMarineCorps: boolean = false;
    let didSelectNavy: boolean = false;
    let didSelectSpaceForce: boolean = false;

    let didSelectReserve: boolean = false;
    let didSelectNationalGuard: boolean = false;
    let didSelectROTC: boolean = false;
    let academies = [];

    userMilitaryPlans.forEach((userMilitaryPlan: UserMilitaryPlan) => {

      if (!didSelectAirForce) {
        //only need to set once
        if (userMilitaryPlan.didSelectAirForce) {
          didSelectAirForce = true;
        }
      }

      if (!didSelectArmy) {
        //only need to set once
        if (userMilitaryPlan.didSelectArmy) {
          didSelectArmy = true;
        }
      }

      if (!didSelectCoastGuard) {
        //only need to set once
        if (userMilitaryPlan.didSelectCoastGuard) {
          didSelectCoastGuard = true;
        }
      }

      if (!didSelectMarineCorps) {
        //only need to set once
        if (userMilitaryPlan.didSelectMarineCorps) {
          didSelectMarineCorps = true;
        }
      }

      if (!didSelectNavy) {
        //only need to set once
        if (userMilitaryPlan.didSelectNavy) {
          didSelectNavy = true;
        }
      }

      if (!didSelectSpaceForce) {
        //only need to set once
        if (userMilitaryPlan.didSelectSpaceForce) {
          didSelectSpaceForce = true;
        }
      }

      if (!didSelectReserve) {
        //only need to set once
        if (userMilitaryPlan.isInterestedInReserves) {
          didSelectReserve = true;
        }
      }

      if (!didSelectNationalGuard) {
        //only need to set once
        if (userMilitaryPlan.isInterestedInNationalGuard) {
          didSelectNationalGuard = true;
        }
      }

      if (!didSelectROTC) {
        //only need to set once
        if (userMilitaryPlan.rotcPrograms.length > 0) {
          didSelectROTC = true;
        }
      }

      if (userMilitaryPlan.serviceColleges.length > 0) {
        userMilitaryPlan.serviceColleges.forEach(x => academies.push(x));
      }

    })

    if (didSelectArmy) {
      returnResourceLinks.push({ url: "https://www.goarmy.com/", title: "https://www.goarmy.com/" });
    }
    if (didSelectMarineCorps) {
      returnResourceLinks.push({ url: "https://www.marines.com/", title: "https://www.marines.com/" });
    }
    if (didSelectNavy) {
      returnResourceLinks.push({ url: "https://www.navy.com/", title: "https://www.navy.com/" });
    }
    if (didSelectAirForce) {
      returnResourceLinks.push({ url: "https://www.airforce.com/", title: "https://www.airforce.com/" });
    }
    if (didSelectSpaceForce) {
      returnResourceLinks.push({ url: "https://www.spaceforce.mil/", title: "https://www.spaceforce.mil/" });
    }
    if (didSelectCoastGuard) {
      returnResourceLinks.push({ url: "https://www.gocoastguard.com/", title: "https://www.gocoastguard.com/" });
    }
    if (didSelectROTC) {
      returnResourceLinks.push({ url: "https://www.careersinthemilitary.com/options-becoming-an-officer/rotc", title: "https://www.careersinthemilitary.com/options-becoming-an-officer" });
    }
    if (didSelectNationalGuard) {
      if (didSelectArmy) {
        returnResourceLinks.push({ url: "https://www.nationalguard.com/", title: "Army National Guard" });
      }

      if (didSelectAirForce) {
        returnResourceLinks.push({ url: "https://www.goang.com/", title: "Air National Guard" });
      }

    }
    if (didSelectReserve) {
      if (didSelectArmy) {
        returnResourceLinks.push({ url: "https://www.goarmy.com/about/serving-in-the-army/serve-your-way/army-reserve.html", title: "Army National Guard" });
      }
      if (didSelectMarineCorps) {
        returnResourceLinks.push({ url: "https://www.marines.com/", title: "Marine Corps Reserve" });
      }
      if (didSelectNavy) {
        returnResourceLinks.push({ url: "https://www.navy.com/", title: "Navy Reserve" });
      }
      if (didSelectAirForce) {
        returnResourceLinks.push({ url: "https://afreserve.com/", title: "Air Force Reserve" });
      }
      if (didSelectCoastGuard) {
        returnResourceLinks.push({ url: "https://www.gocoastguard.com/", title: "Coast Guard Reserve" });
      }
    }

    if (academies.length > 0) {
      academies.forEach((academy: any) => {

        let academyInfo = this.getAcademy(academy.unitId, academy.collegeTypeId);

        if (academyInfo) {

          let academyName = "";
          if (academyInfo.alternativeName) {
            academyName = academyInfo.alternativeName;
          } else {
            academyName = academyInfo.schoolProfile.institutionName;
          }

          returnResourceLinks.push({ url: academyInfo.schoolProfile.url, title: academyName });

        }

      });
    }

    return returnResourceLinks;
  }

  setCollegePlanSources(userCollegePlan: UserCollegePlan) {
    //tuition sources
    let sources = [];
    let sumTotalSource = 0;

    if (userCollegePlan.familyContribution) {
      sources.push({
        value: userCollegePlan.familyContribution,
        label: 'Family'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.familyContribution;
    }

    if (userCollegePlan.scholarshipContribution) {
      sources.push({
        value: userCollegePlan.scholarshipContribution,
        label: 'Scholarship'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.scholarshipContribution;
    }

    if (userCollegePlan.savings) {
      sources.push({
        value: userCollegePlan.savings,
        label: 'Savings/529'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.savings;
    }

    if (userCollegePlan.loan) {
      sources.push({
        value: userCollegePlan.loan,
        label: 'Loan'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.loan;
    }

    if (userCollegePlan.other) {
      sources.push({
        value: userCollegePlan.other,
        label: 'Other'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.other;
    };

    return ({ sources: sources, sumTotalSource: sumTotalSource })
  }

  async setMilitaryPlanInfo(userMilitaryPlan: UserMilitaryPlan) {

    let services = [];
    let rotc = [];
    let academies = [];
    let careers = [];

    if (userMilitaryPlan.didSelectArmy) services.push("Army");
    if (userMilitaryPlan.didSelectMarineCorps) services.push("Marine Corps");
    if (userMilitaryPlan.didSelectNavy) services.push("Navy");
    if (userMilitaryPlan.didSelectAirForce) services.push("Air Force");
    if (userMilitaryPlan.didSelectSpaceForce) services.push("Space Force");
    if (userMilitaryPlan.didSelectCoastGuard) services.push("Coast Guard");

    if (userMilitaryPlan.didSelectOfficer) {

      if (userMilitaryPlan.rotcPrograms) {
        userMilitaryPlan.rotcPrograms.forEach(async (userMilitaryPlanRotc: UserMilitaryPlanRotc) => {
          let printRotc: any = {};

          if (userMilitaryPlanRotc.wantsToApply == 1) {
            let schoolProfile = await this._occufindRestFactory.getSchoolProfile(userMilitaryPlanRotc.unitId).toPromise();

            if (schoolProfile) {
              printRotc.value = schoolProfile.institutionName;
              printRotc.label = '';
              if (schoolProfile.rotcAirForce) printRotc.label = 'AFROTC';
              if (schoolProfile.rotcNavy) printRotc.label = 'NROTC';
              if (schoolProfile.rotcArmy) printRotc.label = 'AROTC';

              if (printRotc.label == '') {
                printRotc.label = 'School'
              }

              rotc.push(printRotc);
            }

          }

        });
      }

      if (userMilitaryPlan.serviceColleges) {
        userMilitaryPlan.serviceColleges.forEach(async (serviceCollege: UserMilitaryPlanServiceCollege) => {

          let printServiceCollege: any = {};
          // let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(serviceCollege.unitId).toPromise();


          printServiceCollege.label = (serviceCollege.collegeTypeId == 1) ? 'Military Academy' :
            ((serviceCollege.collegeTypeId == 2) ? 'Senior Military College' : 'Maritime Academy');

          printServiceCollege.unitId = serviceCollege.unitId;
          printServiceCollege.collegeTypeId = serviceCollege.collegeTypeId;

          academies.push(printServiceCollege);


        })
      }

    }

    for (let userMilitaryPlanCareer of userMilitaryPlan.selectedCareers) {
      let jobTitle: string;
      let occuTitle: string;
      let service: string = "";
      if (userMilitaryPlanCareer.mcId) {

        let militaryOccuTitle: any = this.militaryOccupationTitles.find(x => x.mcId == userMilitaryPlanCareer.mcId);

        if (militaryOccuTitle) {

          let favoriteExists = this.favorites.find(x => x.mcId == militaryOccuTitle.mcId);
          if (favoriteExists) {
            occuTitle = militaryOccuTitle.title + "<i style='margin-left:5px' class='fa fa-heart'></i>";
          } else {
            occuTitle = militaryOccuTitle.title;
          }

        } else {
          occuTitle = "";
        }

        // army details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'A') {
          try {
            let career: any = await this._militaryPlanService.getArmyDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Army: ' + career.title;
              service = 'Army';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        //navy details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'N') {
          try {
            let career: any = await this._militaryPlanService.getNavyDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Navy: ' + career.title;
              service = 'Navy';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // air force details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'F') {
          try {
            let career: any = await this._militaryPlanService.getAirForceDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Air Force: ' + career.title;
              service = 'Air Force';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // marine corps details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'M') {
          try {
            let career: any = await this._militaryPlanService.getMarineDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Marine Corps: " + career.title;
              service = 'Marine Corps';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        //space force details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'S') {
          try {
            let career: any = await this._militaryPlanService.getSpaceForceDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Space Force: " + career.title;
              service = 'Air Force'; //No Space Force recruiter exists; TODO update when available
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // coast guard details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'C') {
          try {
            let career: any = await this._militaryPlanService.getCoastGuardDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Coast Guard: " + career.title;
              service = 'Coast Guard';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

      }

      careers.push({ occuTitle, jobTitle, service });

    }

    return ({ services: services.join(', '), rotc: rotc, academies: academies, careers: careers });

  }

  getAcademy(unitId: number, collegeTypeId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId && x.collegeTypeId == collegeTypeId);
    if (academy) {
      return academy;
    } else {
      return null;
    }
  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }
}
