import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintCareerPlanComponent } from './print-career-plan.component';

describe('PrintCareerPlanComponent', () => {
  let component: PrintCareerPlanComponent;
  let fixture: ComponentFixture<PrintCareerPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintCareerPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintCareerPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
