import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MentorCalendarDialogComponent } from 'app/core/dialogs/mentor-calendar-dialog/mentor-calendar-dialog.component';
import { MentorDateDialogComponent } from 'app/core/dialogs/mentor-date-dialog/mentor-date-dialog.component';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { PrintAsvabScoreComponent } from './print-asvab-score/print-asvab-score.component';
import { PrintCareerPlanComponent } from './print-career-plan/print-career-plan.component';
import { PrintPortfolioComponent } from './print-portfolio/print-portfolio.component';
import { PortfolioService } from 'app/services/portfolio.service';
import { MessageToStudentDialogComponent } from 'app/core/dialogs/message-to-student-dialog/message-to-student-dialog.component';
import { ClassroomActivity, ClassroomActivitySubmission, ClassroomActivitySubmissionComment, ClassroomActivitySubmissionLog } from 'app/core/models/classroomActivity';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { DirectMessageDialogComponent } from 'app/core/dialogs/direct-message-dialog/direct-message-dialog.component';
import { UserOccupationPlan, UserOccupationPlanComment, UserOccupationPlanLog } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { PlayerIndex } from '@angular/core/src/render3/interfaces/player';


//View Model definitions used in the template file
export interface CommentBaseListViewItem {
  hasBeenRead: Boolean,
  typeId: Number,
  timestamp: String,
  message: String,
  from: String,
  isFromYou: Boolean,
  id: Number,
  recipientId: Number
}
export interface PortfolioListViewItem extends CommentBaseListViewItem {
  timestampReviewed: string,
  alwaysShow: boolean,
  hasBeenReviewed: boolean,
  statusId
}

export interface ActivitySubmissionListViewItem extends CommentBaseListViewItem { }

export interface ActivitySubmissionListViewItem {
  files: String[],
  unreadCount: number,
  expandDownload: boolean
}

enum CommentType {
  COMMENT = 1,
  LOG = 2
}

const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
@Component({
  selector: 'app-mentor-student-detail',
  templateUrl: './mentor-student-detail.component.html',
  styleUrls: ['./mentor-student-detail.component.scss']
})
export class MentorStudentDetailComponent implements OnInit {

  table_data_group = false

  toggle1 = false;
  toggle2 = false;
  toggle3 = true;
  toggle4 = true;
  toggle5 = true;

  public expandDownload = false; //for testing remove after

  public careerPlans: any[];
  public calendarItems: any[];
  public tasksWithNoDate: any[];
  public portfolio: any;
  public activities: any[];
  public student: any;

  public mentorLinkId: any;
  public calendarCount: number;
  public lastCalendarUpdateDate: any;
  public portfolioComments: any;
  public portfolioListViewItems: PortfolioListViewItem[];
  public activityListViewItems: any[];
  public showPortfolioReadItems: boolean;
  public unreadPortfolioCount: number;
  public currentUserName: string;
  public portfolioLastUpdated: string;
  public portfolioReviewedByMentor: boolean;
  public filesToDownload: any[];
  public portfolioLogs: any[];
  public studentName: string;
  public showReadPlanItems: boolean;

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  charCutoff;

  constructor(private _router: Router,
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
    private _mentorService: MentorService,
    private _portfolioService: PortfolioService,
    private _user: UserService,
    private _utilityService: UtilityService,
    @Inject(LOCALE_ID) private locale: string,
    private _classRoomActivityService: ClassroomActivityService,
    private _userOccupationPlanService: UserOccupationPlanService) {

    this.portfolioListViewItems = [];
    this.activityListViewItems = [];
    this.showPortfolioReadItems = false;
    this.unreadPortfolioCount = 0;
    this.filesToDownload = [];
    this.portfolioLogs = [];
    this.studentName = "";
    this.portfolioReviewedByMentor = false;
    this.showReadPlanItems = false;
    this.charCutoff = 45;
  }

  ngOnInit() {

    if (window.innerWidth < 640) {
      this.leftNavCollapsed = this._user.getLeftNavCollapsed();;
    }

    this.mentorLinkId = this._activatedRoute.snapshot.paramMap.get('id');

    //career plans
    this.careerPlans = this._activatedRoute.snapshot.data.mentorStudentViewResolver[0];

    //calendar items
    this.calendarItems = this._activatedRoute.snapshot.data.mentorStudentViewResolver[1];
    this.calendarCount = this.getUpcomingDatesCount(this.calendarItems);

    //CEPR231	 the date should be the last updated date not the day of the added date.
    if (this.calendarItems && this.calendarItems.length > 0) {
      this.lastCalendarUpdateDate = this.getCalendarsLastUpdateDate(this.calendarItems);
    } else {
      this.lastCalendarUpdateDate = "";
    }

    //activities
    this.activities = this._activatedRoute.snapshot.data.mentorStudentViewResolver[2];

    //portfolio
    this.portfolio = this._activatedRoute.snapshot.data.mentorStudentViewResolver[3];

    if (this.portfolio) {
      //set portfolio last updated date
      let portfolioDateToUse = (this.portfolio.dateUpdated) ? this.portfolio.dateUpdated : this.portfolio.dateCreated;
      this.portfolioLastUpdated = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(portfolioDateToUse)), 'MM/dd/yyyy', this.locale);

      if (this.portfolio.mentors && this.portfolio.mentors.length > 0) {
        let portfolioMentor = this.portfolio.mentors.find(x => x.mentorId == this.mentorLinkId);
        if (portfolioMentor) {
          this.portfolioReviewedByMentor = portfolioMentor.didReview;
        }
      }
    } else {
      this.portfolioLastUpdated = null;
    }

    //student info
    this.student = this._activatedRoute.snapshot.data.mentorStudentViewResolver[4];
    if (this.student.moreAboutMe && Array.isArray(this.student.moreAboutMe) && this.student.moreAboutMe.length > 0) {
      let moreAboutMe = this.formatMoreAboutMeForView(this.student.moreAboutMe);
      this.student.moreAboutMe.likes = moreAboutMe.likes;
      this.student.moreAboutMe.dislikes = moreAboutMe.dislikes;
    }
    //set student name
    if (this.student) {
      if (!this.student.firstName || !this.student.lastName) {
        this.studentName = this.student.emailAddress;
      } else {
        this.studentName = this.student.firstName + " " + this.student.lastName;
      } 
    } else {
        this.studentName = "";
    }

    //tasks with no date
    this.tasksWithNoDate = this._activatedRoute.snapshot.data.mentorStudentViewResolver[5];

    //portfolio comments
    this.portfolioComments = this._activatedRoute.snapshot.data.mentorStudentViewResolver[6];

    //current user
    this.currentUserName = this._activatedRoute.snapshot.data.mentorStudentViewResolver[7]
                          && this._activatedRoute.snapshot.data.mentorStudentViewResolver[7].username
                          ? this._activatedRoute.snapshot.data.mentorStudentViewResolver[7].username
                          : '';

    //portfolio logs
    this.portfolioLogs = this._activatedRoute.snapshot.data.mentorStudentViewResolver[8];

    this.setPortfolioListView(this.portfolioComments, this.portfolioLogs);
    this.setActivityListView(this.activities);
    this.setCareerPlansListView(this.careerPlans);

    if (this.portfolioLogs && this.portfolioLogs.length > 0) {
      let sortedLogs = this.portfolioLogs.sort((a, b) => {
        let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
          db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
        return da - db;
      });

      let lastLog = sortedLogs[sortedLogs.length - 1];
      let lastLogDate = (lastLog.dateUpdated) ? lastLog.dateUpdated : lastLog.dateCreated;
      if (lastLogDate > this.portfolioLastUpdated) {
        this.portfolioLastUpdated = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastLogDate)), 'MM/dd/yyyy', this.locale);
      }

    }

    if (this.portfolioComments && this.portfolioComments.length > 0) {

      let sortedComments = this.portfolioComments.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
        return da - db;
      });

      let lastComment = sortedComments[sortedComments.length - 1];

      if (lastComment.dateCreated > this.portfolioLastUpdated) {
        this.portfolioLastUpdated = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastComment.dateCreated)), 'MM/dd/yyyy', this.locale);
      }
    }



  }

  setCareerPlansListView(careerPlans: any[], planId: number = null) {
    this.showReadPlanItems = false;
    if (careerPlans && careerPlans.length > 0) {
      careerPlans.forEach(element => {
        if (planId) {
          element.expanded = true;
        } else {
          element.expanded = false;
        }
        element.combinedList = this.combinePlanMessagesAndLogs(element.comments, element.logs);
        element.showReadPlanItems = false;
      });
    }
  }

  setActivityListView(activities: ClassroomActivity[]) {

    let activityViewItem: any;

    if (activities.length > 0) {
      activities.forEach((activity: ClassroomActivity) => {

        let unreadTotal: number = 0;
        let submittedSubmissions: any[] = [];

        //set the unread total based off of unread comments
        if (activity.comments.length > 0) {
          let commentsNotFromYou = activity.comments.filter(x => x.from != "You");
          if (commentsNotFromYou && commentsNotFromYou.length > 0) {
            unreadTotal = unreadTotal + (commentsNotFromYou.filter(x => x.to.filter(y => !y.hasBeenRead).length > 0).length);
          }
        }

        //filter out all submissions assigned to the mentor
        //only show if they are "submitted" or "reviewed"
        //this is used for downloads
        if (activity.submissions.length > 0) {

          // let allSubmittedSubmissions = activity.submissions.filter(x => x.statusId == 2 || x.statusId == 5);  //TODO enable once bug is fixed by Jason.
          let allSubmittedSubmissions = activity.submissions;
          submittedSubmissions = allSubmittedSubmissions.filter(x => x.mentors.find(y => y.mentorId == this.mentorLinkId));

        }

        //goto next activity (and not show in the ui) if both the submitted submissions length is 0
        //and if the number of logs is 0
        if (submittedSubmissions.length == 0) {
          let totalLogs: number = 0;
          activity.submissions.forEach((submission: ClassroomActivitySubmission) => {
            totalLogs = totalLogs + submission.logs.length;
          });
          if (totalLogs == 0) return;
        }

        let updateDate;


        //sort submissions based on dates in descending order
        let sortedSubmissions = activity.submissions.sort((a, b) => {
          let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
            db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
          return da - db;
        });

        let lastSubmission = sortedSubmissions[sortedSubmissions.length - 1];
        let lastSubmissionDate = (lastSubmission.dateUpdated) ? lastSubmission.dateUpdated : lastSubmission.dateCreated;
        updateDate = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastSubmissionDate)), 'MM/dd/yyyy', this.locale);

        //sort comments based on dates in descending order
        if (activity.comments && activity.comments.length > 0) {
          let sortedComments = activity.comments.sort((a, b) => {
            let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
              db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
            return da - db;
          });
          let lastComment = sortedComments[sortedComments.length - 1];
          if (lastComment.dateCreated > updateDate) {
            updateDate = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastComment.dateCreated)), 'MM/dd/yyyy', this.locale);
          }
        }

        //Create the activity view item to display to user
        activityViewItem = {};
        activityViewItem.classroomActivityId = activity.id;
        activityViewItem.title = activity.title;
        activityViewItem.expanded = false;
        activityViewItem.expandDownload = false;
        activityViewItem.showReadItems = false;
        activityViewItem.activityListViewItems = this.getActivityListViewItem(activity); //get list of all comments and logs to display

        if (activity.submissions.length > 0 && activityViewItem.activityListViewItems.length > 0) {
          unreadTotal = unreadTotal + (activityViewItem.activityListViewItems.filter(x => !x.hasBeenRead &&
            (x.typeId == 2 && x.statusId == 2))).length;
        }
        activityViewItem.unreadMessageCount = unreadTotal;

        activityViewItem.hasMultipleSubmissions = (submittedSubmissions.length > 1) ? true : false;
        //needed for the case when there is only 1 submission and user clicks download
        activityViewItem.submittedSubmissions = submittedSubmissions;

        //set the last updated date based on submissions first; otherwise goes to comments
        // activityViewItem.lastUpdated = (sortedSubmissions && sortedSubmissions.length > 0) ?
        //   formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(sortedSubmissions[0].dateUpdated)), 'MM/dd/yyyy', this.locale) :
        //   (sortedComments && sortedComments.length > 0) ? formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(sortedComments[0].dateCreated)), 'MM/dd/yyyy', this.locale) : "";
        activityViewItem.lastUpdated = updateDate;

        activityViewItem.activityStatusClass = this.getActivityCssStatusClass(activity);

        this.activityListViewItems.push(activityViewItem);
      });
    }

  }

  /**
   * 
   * @param activity 
   * @returns returns a combined list of comments and submission logs for the activity
   */
  getActivityListViewItem(activity: ClassroomActivity) {

    let returnObj: any[] = [];

    if (activity.comments.length > 0) {
      activity.comments.forEach(comment => {

        let isFromYou: boolean = (comment.from === 'You' || comment.from == this.currentUserName);
        let you: any = null;
        let hasBeenRead: boolean = false;
        if (isFromYou) {
          //read by all students
          if (comment.to.filter(x => !x.hasBeenRead).length == 0) {
            hasBeenRead = true;
          }
        } else {
          you = comment.to.find(x => x.username == 'You');
          hasBeenRead = you.hasBeenRead;
        }

        returnObj.push({
          hasBeenRead: hasBeenRead,
          typeId: 1, //comment
          timestampCreated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
          timestampUpdated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
          message: comment.message,
          from: (isFromYou) ? 'You' : this.studentName,
          isFromYou: isFromYou,
          id: comment.id,
          logId: null, //comment does not have a log
          recipientId: (you) ? you.id : comment.to[0].id,
          file: null,
          hasBeenReviewed: null,
          alwaysShow: false,
          statusId: null,
          fileName: null,
          originalTimeStamp: comment.dateCreated
        });

      })
    }

    if (activity.submissions.length > 0) {

      //loop through each submission; and display its logs to the user
      activity.submissions.forEach(submission => {

        let submissionReturnObj: any = {};
        let mentor = submission.mentors.find(x => x.mentorId == this.mentorLinkId);

        //if not currently assigned and there is no log history skip submission
        if (!mentor && submission.logs.length == 0) return;

        if (mentor) {
          //create a view item for submission
          //this is done due to the last submission being essentially the same thing as the last log item
          //therefore we will pop the last log item after we sort
          submissionReturnObj = {
            hasBeenRead: (mentor.didReview) ? true : false,
            typeId: 2, //submission
            timestampCreated: this._utilityService.convertDateToLocalDate(submission.dateCreated),
            timestampUpdated: (submission.dateUpdated) ? this._utilityService.convertDateToLocalDate(submission.dateUpdated)
              : this._utilityService.convertDateToLocalDate(submission.dateCreated),
            message: this.getSubmissionStatusDescription(submission.statusId),
            from: this.studentName,
            isFromYou: false, //a mentor will never submit a submission to himself
            id: submission.id, //workId
            logId: null,
            recipientId: null,
            file: submission.fileName,
            hasBeenReviewed: mentor.didReview,
            alwaysShow: false,
            fileName: submission.fileName,
            statusId: submission.statusId,
            originalTimeStamp: (submission.dateUpdated) ? submission.dateUpdated : submission.dateCreated //used for sorting on front end; use updated date if it exists
          };
        }

        //sort logs based on dates in descending order
        let sortedSubmissionLogs = submission.logs.sort((a, b) => {
          let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
            db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
          return da - db;
        });

        if (mentor) {
          //GET the read status of the last log from the last record in the log table

          let lastSubmissionLog = sortedSubmissionLogs[sortedSubmissionLogs.length - 1];
          if (lastSubmissionLog) {
            submissionReturnObj.logId = lastSubmissionLog.id;

            if (!mentor.didReview) {
              submissionReturnObj.hasBeenRead = lastSubmissionLog.hasBeenRead;
              submissionReturnObj.statusId = lastSubmissionLog.statusId;
            }

          }

          //is the submission "unsubmitted"? if so ; user does not need to mark as been read;
          if (submissionReturnObj.statusId == 3) { submissionReturnObj.hasBeenRead = true; submissionReturnObj.hasBeenReviewed = true; }
          //remove the last record of logs as it duplicates the status of the submission in the view
          sortedSubmissionLogs.pop();
        }

        //now add each log for the submission to the UI
        if (sortedSubmissionLogs.length > 0) {

          //create a view item for each submission log
          sortedSubmissionLogs.forEach((log: ClassroomActivitySubmissionLog) => {

            // if the submission has the status of '2' submitted
            // mark any other status id's of 2 to '99' for UI handling
            // this will only occur when the student resubmits
            // let submissionIsSubmitted: boolean = (submission.statusId == 2) ? true : false;
            // let submissionIsReviewed: boolean = (submission.statusId == 5) ? true : false;

            returnObj.push({
              hasBeenRead: true,  //logs do not need to be read;
              typeId: 3, //submission log
              timestampCreated: this._utilityService.convertDateToLocalDate(log.dateLogged),
              timestampUpdated: this._utilityService.convertDateToLocalDate(log.dateLogged),  //a log should never be updated
              message: this.getSubmissionStatusDescription(log.statusId),
              from: this.studentName,
              isFromYou: false,
              id: submission.id, //workId
              logId: log.id,
              recipientId: null,
              file: submission.fileName,
              hasBeenReviewed: true, //will only be marking the submission obj as reviewed
              alwaysShow: false,
              statusId: log.statusId,
              fileName: submission.fileName,
              originalTimeStamp: log.dateLogged //used for sorting on front end; use updated date if it exists
            });

          });
        }

        //add the submission object only if a mentor has a submission record
        if (mentor) {
          returnObj.push(submissionReturnObj);
        }

      });
    }

    return returnObj;

  }

  getActivityCssStatusClass(activity: any) {
    return this._utilityService.getActivityStatusCssClass(activity) + " mr_10";
  }

  setPortfolioListView(portfolioComments: PortfolioComment[], portfolioLogs: any[]) {

    let unreadCounter: number = 0;

    if (portfolioComments.length > 0) {
      portfolioComments.forEach((comment: PortfolioComment) => {
        this.portfolioListViewItems.push(this.getPortfolioListCommentViewItem(comment));
        let commentTo = comment.to.find(x => (x.username == this.currentUserName) || (x.username == 'You'));
        if (!commentTo) { commentTo = comment.to[0]; }

        unreadCounter = (!commentTo.hasBeenRead && comment.from != 'You') ? unreadCounter + 1 : unreadCounter;
      })
    }

    if (portfolioLogs.length > 0) {

      let sortedPortfolioLogs = portfolioLogs.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
        return da - db;
      });

      this.portfolioListViewItems.push(this.getPortfolioSubmissionViewItem(sortedPortfolioLogs[sortedPortfolioLogs.length - 1]));

      //if latest log is unread; add to counter
      let lastLog = sortedPortfolioLogs[sortedPortfolioLogs.length - 1];
      if (!lastLog.hasBeenRead && lastLog.statusId != 5) { unreadCounter++; }

    }

    this.unreadPortfolioCount = unreadCounter;
  }

  getPortfolioListCommentViewItem(comment: PortfolioComment) {
    let isFromYou: boolean = (comment.from == 'You' || comment.from == this.currentUserName);
    let commentTo = comment.to.find(x => (x.username == this.currentUserName) || (x.username == 'You'));
    if (!commentTo) { commentTo = comment.to[0]; }
    let returnObj: PortfolioListViewItem = {

      hasBeenRead: (isFromYou) ? true : commentTo.hasBeenRead,
      typeId: 1, //comment
      timestamp: this._utilityService.convertDateToLocalDate(comment.dateCreated),
      message: comment.message,
      from: (isFromYou) ? 'You' : this.studentName,
      isFromYou: isFromYou,
      id: commentTo.commentId,
      recipientId: commentTo.id,
      timestampReviewed: null,
      hasBeenReviewed: null,
      alwaysShow: false,
      statusId: null
    }

    return returnObj;
  }

  getPortfolioSubmissionViewItem(log: any) {
    let isFromYou: boolean = (log.mentorUsername === 'You' || log.mentorUsername == this.currentUserName);

    if (log.statusId == 2) {
      isFromYou = false;
    }

    let returnObj: PortfolioListViewItem = {
      hasBeenRead: (isFromYou) ? true : log.hasBeenRead,
      typeId: 2, //submission
      timestamp: this._utilityService.convertDateToLocalDate(log.dateLogged),
      message: this.getSubmissionStatusDescription(log.statusId),
      from: (isFromYou) ? 'You' : this.studentName,
      isFromYou: isFromYou,
      id: log.id,
      recipientId: null,
      timestampReviewed: (log.dateLogged) ? this._utilityService.convertDateToLocalDate(log.dateLogged) : null,
      hasBeenReviewed: this.portfolioReviewedByMentor,
      alwaysShow: false,
      statusId: log.statusId
    }

    return returnObj;
  }

  getSubmissionStatusDescription(statusId) {

    if (statusId == 1) {
      return "In Progress";
    }

    if (statusId == 2) {
      return "Submitted";
    }

    if (statusId == 3) {
      return "Unsubmitted";
    }

    if (statusId == 4) {
      return "Deleted";
    }

    if (statusId == 5) {
      return "Reviewed";
    }
  }

  downloadPlan(plan) {

    let data = {
      planId: plan.id,
      title: plan.title,
      mentorLinkId: this.mentorLinkId,
      studentName: this.studentName
    };

    this._dialog.open(PrintCareerPlanComponent, {
      data: data,
      maxWidth: '400px'
    });

  }

  downloadPortfolio() {
    let data = {
      mentorLinkId: this.mentorLinkId,
      studentName: this.studentName
    };

    this._dialog.open(PrintPortfolioComponent, {
      data: data,
      maxWidth: '400px'
    });
  }

  downloadASVAB() {
    let data = {
      mentorLinkId: this.mentorLinkId,
      asvabScore: this.student.asvabScore,
      studentName: this.studentName
    };

    this._dialog.open(PrintAsvabScoreComponent, {
      data: data,
      maxWidth: '400px'
    });
  }

  async getUserOccupationPlanCalendarTasks(planId) {

    try {

      let careerPlans = await this._mentorService.getStudentCareerPlanInfo(this.mentorLinkId, planId);

      return await careerPlans;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  /**
   * 
   * @param calendarItems 
   * @returns the number of dates upcoming i.e. > current date
   */
  getUpcomingDatesCount(calendarItems: any[]) {
    let returnCount: number = 0;

    if (Array.isArray(calendarItems)) {

      let futureItems = calendarItems.filter(x => Date.parse(x.date) > Date.now())

      return futureItems.length;
    }

    return returnCount;
  }

  addDate() {
    let data: any = {
      mentorLinkIds: [this.mentorLinkId]
    }

    const dialogRef = this._dialog.open(MentorDateDialogComponent, { data: data, maxWidth: '450px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        //increment date count
        this.calendarCount++;
      }


    });
  }

  showStudentCalendar() {

    let data = {
      careerPlans: this.careerPlans,
      calendarItems: this.calendarItems,
      tasks: this.tasksWithNoDate
    }

    const dialogRef = this._dialog.open(MentorCalendarDialogComponent, { data: data, maxWidth: '600px', maxHeight: '90%', autoFocus: false });

  }

  /**
 * format the json returned from the server
 * @param experiences 
 */
  formatMoreAboutMeForView(experiences: any[]) {
    let returnMoreAboutMe: {
      likes: any[],
      dislikes: any[]
    } = {
      likes: [],
      dislikes: []
    };

    let likes: string[] = [];
    let dislikes: string[] = [];

    for (let experience of experiences) {
      let parsedMoreAboutMe = JSON.parse(experience.moreAboutMe).values;
      if (parsedMoreAboutMe) {

        for (let like of this._utilityService.getValuesFromMoreAboutMe(parsedMoreAboutMe[1].likes)) {
          likes.push(like);
        }

        for (let dislike of this._utilityService.getValuesFromMoreAboutMe(parsedMoreAboutMe[2].dislikes)) {
          dislikes.push(dislike);
        }
      }
    }

    let uniqueLikes = Array.from(new Set(likes));
    returnMoreAboutMe.likes = uniqueLikes;

    let uniqueDislikes = Array.from(new Set(dislikes));
    returnMoreAboutMe.dislikes = uniqueDislikes;

    return returnMoreAboutMe;
  }

  getInterestDescription(type) {
    let interests = [{
      name: 'Realistic',
      code: 'R'
    }, {
      name: 'Investigative',
      code: 'I'
    }, {
      name: 'Artistic',
      code: 'A'
    }, {
      name: 'Social',
      code: 'S'
    }, {
      name: 'Enterprising',
      code: 'E'
    }, {
      name: 'Conventional',
      code: 'C'
    }];

    let interest = interests.find(x => x.code == type);

    if (interest) {
      return interest.name;
    } else {
      return "";
    }

  }

  getWorkValueDescription(type) {
    let workValues = [{
      name: 'Achievement',
      code: '1'
    }, {
      name: 'Independence',
      code: '2'
    }, {
      name: 'Recognition',
      code: '3'
    }, {
      name: 'Relationships',
      code: '4'
    }, {
      name: 'Support',
      code: '5'
    }, {
      name: 'Working Conditions',
      code: '6'
    }];

    let workValue = workValues.find(x => x.code == type);

    if (workValue) {
      return workValue.name;
    } else {
      return "";
    }

  }

  markAsReviewed(section: string, item, id: number = null) {

    if (section == 'Portfolio') {
      this._mentorService.setStudentPortfolioAsReviewed(this.mentorLinkId, true).then((rowsAffected: number) => {

        if (rowsAffected > 0) {
          //update UI
          let portfolioItemIndex = this.portfolioListViewItems.findIndex(x => x.id == item.id && x.typeId == 2);
          if (portfolioItemIndex > -1) {
            this.portfolioListViewItems[portfolioItemIndex].timestampReviewed = this._utilityService.convertDateToLocalDate(new Date(Date.now()), false);
            this.portfolioListViewItems[portfolioItemIndex].statusId = 5;
            this.portfolioReviewedByMentor = true;
            this.portfolio.statusId = 5
            //set the record as being read if it hasn't been
            this.markAsRead(item, section);

          }
        } else {
          console.error("Server ERROR");
        }
      }).catch(error => console.error("ERROR", error));
    }

    if (section == 'Activity') {
      this._mentorService.setStudentSubmissionAsReviewed(this.mentorLinkId, id, item.id, true).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          var activityIndex = this.activityListViewItems.findIndex(x => x.classroomActivityId == id);

          if (activityIndex > -1) {
            var activityItemIndex = this.activityListViewItems[activityIndex].activityListViewItems.findIndex(x => x.typeId == 2
              && x.id == item.id && x.logId == item.logId);

            if (activityItemIndex > -1) {
              this.activityListViewItems[activityIndex].activityListViewItems[activityItemIndex].timestampUpdated
                = this._utilityService.convertDateToLocalDate(new Date(Date.now()), false);
              this.activityListViewItems[activityIndex].activityListViewItems[activityItemIndex].hasBeenReviewed = true;
              this.activityListViewItems[activityIndex].activityListViewItems[activityItemIndex].statusId = 5;

              //multiple submissions? then set css class as completed
              //otherwise check to see if all submitted submissions are reviewed; if so set to completed otherwise do nothing
              if (this.activityListViewItems[activityIndex].hasMultipleSubmissions) {
                this.activityListViewItems[activityIndex].activityStatusClass = "completed mr_10";
              } else {
                let submissions = this.activityListViewItems[activityIndex].activityListViewItems.filter(x => x.typeId == 2);
                let reviewedSubmissions = submissions.filter(x => x.statusId == 5).length;
                if (submissions.length == reviewedSubmissions) {
                  this.activityListViewItems[activityIndex].activityStatusClass = "completed mr_10";
                }
              }

              //set the record as being read if it hasn't been
              if (!this.activityListViewItems[activityIndex].activityListViewItems[activityItemIndex].hasBeenRead) {
                this.markAsRead(item, section, id);
              }
            }
          }

        }

      }).catch(error => console.error(error));
    }

    if (section == 'Plan') {
      let planId = id;
      this._mentorService.markStudentOccupationPlanAsReviewed(this.mentorLinkId, planId, true).then((rowsAffected: any) => {

        if (rowsAffected > 0) {

          //set the record as being read if it hasn't been
          let careerPlan = this.careerPlans.find(x => x.id == id);
          if (careerPlan) {
            if (!careerPlan.logs.findIndex(x => x.id == item.id && x.typeId == CommentType.LOG).hasBeenRead) {
              this.markAsRead(item, section, id, true);
            } else {
              this._mentorService.getStudentCareerPlans(this.mentorLinkId).then((results: any) => {

                this.careerPlans = results;
                this.setCareerPlansListView(this.careerPlans, planId);

              });
            }
          }

        }

      }).catch(error => console.error(error));
    }
  }

  markAllAsRead(section: string, id: number = null) {

    if (section == 'Portfolio') {
      let items = this.portfolioListViewItems.filter(x => !x.hasBeenRead && !x.isFromYou);

      if (items.length > 0) {
        items.forEach(x => this.markAsRead(x, section));
      }
    }

    if (section == 'Activity') {
      let classroomActivityId = id;

      let activity = this.activityListViewItems.find(x => x.classroomActivityId == classroomActivityId);

      let items = activity.activityListViewItems.filter(x => !x.hasBeenRead && !x.isFromYou);

      if (items.length > 0) {
        items.forEach(x => this.markAsRead(x, section, classroomActivityId));
      }
    }

    if (section == 'Plan') {
      let planId = id;

      let plan = this.careerPlans.find(x => x.id == planId);

      let items = plan.combinedList.filter(x => !x.hasBeenRead && !x.isFromYou);

      if (items.length > 0) {
        items.forEach(x => this.markAsRead(x, section, planId));
      }
    }

  }

  markAsRead(item, section: string = null, id: number = null, fetchRecords: boolean = false) {

    if (section == 'Activity') {

      if (item.typeId == CommentType.COMMENT) {
        this._classRoomActivityService.updateActivityCommentAsRead(id, item.id, item.recipientId, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let activity = this.activityListViewItems.find(x => x.classroomActivityId == id);

            if (activity) {

              let index = activity.activityListViewItems.findIndex(x => x.id == item.id && x.typeId == 1);

              if (index > -1) {
                activity.activityListViewItems[index].hasBeenRead = true;
                activity.unreadMessageCount--;
              }
            }

          }

        });
      }

      if (item.typeId == CommentType.LOG) {
        this._mentorService.setStudentSubmissionLogAsRead(this.mentorLinkId, id, item.id, item.logId, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            var activityIndex = this.activityListViewItems.findIndex(x => x.classroomActivityId == id);

            if (activityIndex > -1) {
              var activityItemIndex = this.activityListViewItems[activityIndex].activityListViewItems.findIndex(x => x.typeId == 2 && x.id == item.id);

              if (activityItemIndex > -1) {
                this.activityListViewItems[activityIndex].activityListViewItems[activityItemIndex].hasBeenRead = true;
                this.activityListViewItems[activityIndex].unreadMessageCount--;

              }
            }

          }

        }).catch(error => console.error(error));

      }

    }

    if (section == 'Portfolio') {
      if (item.typeId == CommentType.COMMENT) {
        this._portfolioService.updateCommentByHasBeenRead(item.id, item.recipientId, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.portfolioListViewItems.findIndex(x => x.id == item.id && x.recipientId == item.recipientId && x.typeId == 1);

            if (index > -1) {
              this.portfolioListViewItems[index].hasBeenRead = true;
              this.unreadPortfolioCount--;
            }
          }

        });

      }

      if (item.typeId == CommentType.LOG) {
        this._mentorService.setStudentPortfolioLogAsRead(this.mentorLinkId, item.id, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.portfolioListViewItems.findIndex(x => x.id == item.id && x.typeId == 2);

            if (index > -1) {
              this.portfolioListViewItems[index].hasBeenRead = true;
              this.unreadPortfolioCount--;
            }
          }

        }).catch(error => console.error(error));

      }


    }

    if (section == 'Plan') {

      if (item.typeId == CommentType.COMMENT) {
        this._userOccupationPlanService.markCommentAsRead(id, item.id, item.recipientId).then((rowsAffected: any) => {
          if (rowsAffected > 0) {
            let careerPlanIndex = this.careerPlans.findIndex(x => x.id == id);
            if (careerPlanIndex > -1) {
              //update original list
              let commentIndex = this.careerPlans[careerPlanIndex].comments.findIndex(x => x.id == item.id && x.typeId == CommentType.COMMENT);
              if (commentIndex > -1) {
                this.careerPlans[careerPlanIndex].comments[commentIndex].hasBeenRead = true;
              }
              //update combine list (this is shown in the ui)
              let combineIndex = this.careerPlans[careerPlanIndex].combinedList.findIndex(x => x.id == item.id && x.typeId == CommentType.COMMENT);
              if (combineIndex > -1) {
                this.careerPlans[careerPlanIndex].combinedList[combineIndex].hasBeenRead = true;
              }
            }
          }

        });
      }

      if (item.typeId == CommentType.LOG) {
        this._mentorService.markStudentOccupationPlanLogAsRead(this.mentorLinkId, id, item.id, true).then((rowsAffected: any) => {
          if (rowsAffected > 0) {

            if (fetchRecords) {
              this._mentorService.getStudentCareerPlans(this.mentorLinkId).then((results: any) => {

                this.careerPlans = results;
                this.setCareerPlansListView(this.careerPlans, id);

              });
            } else {
              let careerPlanIndex = this.careerPlans.findIndex(x => x.id == id);
              if (careerPlanIndex > -1) {
                //update original list
                let logIndex = this.careerPlans[careerPlanIndex].logs.findIndex(x => x.id == item.id);
                if (logIndex > -1) {
                  this.careerPlans[careerPlanIndex].logs[logIndex].hasBeenRead = true;
                }
                //update combine list (this is shown in the ui)
                let combineIndex = this.careerPlans[careerPlanIndex].combinedList.findIndex(x => x.id == item.id && x.typeId == CommentType.LOG);
                if (combineIndex > -1) {
                  this.careerPlans[careerPlanIndex].combinedList[combineIndex].hasBeenRead = true;
                }
              }
            }

          }
        }).catch(error => console.error(error));

      }

    }
  }

  showAddComment(title, id = 0) {
    let data = {
      title: title,
      studentName: this.studentName,
      mentorLinkId: this.mentorLinkId,
      id: id
    }

    if (title == 'Portfolio') {
      this._dialog.open(MessageToStudentDialogComponent, { data: data, maxWidth: '600px', maxHeight: '90%', minHeight: '250px', autoFocus: false })
        .afterClosed().subscribe(response => {
          if (response) {
            if (response != 'cancel') {
              let responseComment: PortfolioComment = response;
              this.portfolioListViewItems.push(this.getPortfolioListCommentViewItem(responseComment));
            }
          }
        });
    }

    if (title == 'Activity') {
      this._dialog.open(MessageToStudentDialogComponent, { data: data, maxWidth: '600px', maxHeight: '90%', minHeight: '250px', autoFocus: false })
        .afterClosed().subscribe(response => {
          if (response) {
            if (response != 'cancel') {
              let newComment: ClassroomActivitySubmissionComment = response;

              //find existing activity in ui
              let item = this.activityListViewItems.find(x => x.classroomActivityId == id);
              if (item) {
                let isFromYou: boolean = true;
                //update ui element with the new comment
                item.activityListViewItems.push({
                  hasBeenRead: false, //was just created
                  typeId: 1, //comment
                  timestampCreated: this._utilityService.convertDateToLocalDate(newComment.dateCreated),
                  timestampUpdated: null,
                  message: newComment.message,
                  from: 'You',
                  isFromYou: isFromYou,
                  id: newComment.to[0].commentId,
                  recipientId: newComment.to[0].id,
                  file: null,
                  hasBeenReviewed: null,
                  logId: null,
                  alwaysShow: false,
                  statusId: null,
                  fileName: null
                });
              }
            }
          }
        });
    }

    if (title == 'Plan') {
      this._dialog.open(MessageToStudentDialogComponent, { data: data, maxWidth: '600px', maxHeight: '90%', minHeight: '250px', autoFocus: false })
        .afterClosed().subscribe(response => {
          if (response) {
            if (response != 'cancel') {
              let responseComment: UserOccupationPlanComment = response;

              let careerPlanIndex = this.careerPlans.findIndex(x => x.id == responseComment.planId);
              if (careerPlanIndex > -1) {
                this.careerPlans[careerPlanIndex].comments.push(responseComment);
                let responseCommentArr: UserOccupationPlanComment[] = [];
                responseCommentArr.push(responseComment);
                this.careerPlans[careerPlanIndex].combinedList.push(...this.combinePlanMessagesAndLogs(responseCommentArr, null));
              }
            }
          }
        });
    }

  }

  downloadSubmission(classroomActivityId, submissionId, fileName) {

    this._mentorService.getStudentSubmittedActivityFile(this.mentorLinkId, classroomActivityId, submissionId).then((response: any) => {

      let blob = new Blob([response], { type: 'application/pdf' });

      var downloadURL = URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = fileName;
      link.click();

    }).catch(error => console.error("ERROR", error))
  }

  onSubmissionFileChecked(e, submission, classroomActivityId) {
    if (e.target.checked) {
      this.filesToDownload.push({
        submissionId: submission.id,
        classroomActivityId: classroomActivityId,
        fileName: submission.fileName
      });
      //add file to array
    } else {
      //remove file from array
      let index = this.filesToDownload.findIndex(x => x.submissionId == submission.id);
      if (index > -1) {
        this.filesToDownload.splice(index, 1);
      }
    }
  }

  getCheckedSubmissionFileValue(submission, classroomActivityId) {

    if (this.filesToDownload && this.filesToDownload.length == 0) {
      return false;
    }
    let file = this.filesToDownload.find(x => x.submissionId == submission.id && x.classroomActivityId == classroomActivityId);
    if (file) {
      return true;
    } else {
      return false;
    }
  }

  downloadSelectedFiles(classroomActivityId) {
    let files = this.filesToDownload.filter(x => x.classroomActivityId == classroomActivityId);

    if (files.length > 0) {
      files.forEach(file => {
        this.downloadSubmission(classroomActivityId, file.submissionId, file.fileName)
      })
    }

    this.filesToDownload = [];
  }

  getSelectionCount(classroomActivityId) {
    return this.filesToDownload.filter(x => x.classroomActivityId == classroomActivityId).length;
  }

  getPortfolioFilters(showPortfolioReadItems: boolean) {

    let filterObj: any = {};

    if (!showPortfolioReadItems) {
      filterObj.hasBeenRead = [false];
      // filterObj.hasBeenReviewed = [false];
    } else {
      filterObj.hasBeenRead = [false, true];
    }
    // filterObj.alwaysShow = [true];

    return filterObj;
  }

  getActivityListItemsFilter(showReadItems: boolean) {

    let filterObj: any = {};

    if (!showReadItems) {
      filterObj.hasBeenRead = [false];
      // filterObj.hasBeenReviewed = [false];
    } else {
      filterObj.hasBeenRead = [false, true];
    }
    // filterObj.alwaysShow = [true];

    return filterObj;
  }

  getPlanListItemsFilter(showReadItems: boolean) {

    let filterObj: any = {};

    if (!showReadItems) {
      filterObj.hasBeenRead = [false];
      // filterObj.hasBeenReviewed = [false];
    } else {
      filterObj.hasBeenRead = [false, true];
    }
    // filterObj.alwaysShow = [true];

    return filterObj;
  }

  getPortfolioCssClass() {

    if (!this.portfolio) {
      return "not_started mr_10 ml_20";
    }

    if (this.portfolio.statusId == 5) {
      if (this.portfolio.mentors) {
        return "checkmark mr_10";
      } else {
        return "not_started mr_10 ml_20";
      }
    };

    if (this.portfolio.statusId == 2) {
      if (this.portfolio.mentors) {
        return "ready_for_review mr_10";
      } else {
        return "not_started mr_10 ml_20";
      }
    };

    if (this._utilityService.mobileOrTablet()) {
      return "in_progress mr_10";
    } else {
      return "in_progress mr_10 ml_20";
    }
  }

  showPortfolioDropdown() {
    if (this.getPortfolioCssClass().includes('not_started')
      || this.getPortfolioCssClass().includes('in_progress')
    ) {
      return false;
    }
    return true;
  }

  showDirectMessage() {
    let currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    let dialogRef = this._dialog.open(DirectMessageDialogComponent, {
      data: {
        isStudent: (userRole) ? (userRole != 'S') ? false : true : false,
        isReplyMessage: false,
        directMessageMentorLinkId: this.mentorLinkId,
        directMessageUsername: this.studentName
      },
      maxWidth: '450px',
      minHeight: '250px',
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        //do something?
      }
    })
  }

  getMoreAboutMeLastUpdateDate(moreAboutMeList: any[]) {

    //order submissions for the activity
    let sortedMoreAboutMeList = moreAboutMeList.sort((a, b) => {
      let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
        db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
      return da - db;
    });

    let lastMoreAboutMeRecord = sortedMoreAboutMeList[sortedMoreAboutMeList.length - 1];

    if (lastMoreAboutMeRecord && lastMoreAboutMeRecord.dateUpdated) {
      return formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastMoreAboutMeRecord.dateUpdated)),
        'MM/dd/yyyy', this.locale);
    } else {
      return "";
    }
  }

  getCalendarsLastUpdateDate(items: any[]) {

    //order submissions for the activity
    let sortedItems = items.sort((a, b) => {
      let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
        db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
      return da - db;
    });

    let lastRecord = sortedItems[sortedItems.length - 1];

    if (lastRecord && lastRecord.dateUpdated) {
      return formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastRecord.dateUpdated)),
        'MM/dd/yyyy', this.locale);
    } else if (lastRecord && lastRecord.dateCreated) {
      return formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastRecord.dateCreated)),
        'MM/dd/yyyy', this.locale);
    }
    else {
      return "";
    }
  }

  combinePlanMessagesAndLogs(messages: UserOccupationPlanComment[], logs: UserOccupationPlanLog[]) {

    let returnObjArr: any[] = [];

    if (messages && messages.length > 0) {

      messages.forEach((message: UserOccupationPlanComment) => {

        if (message.to.length > 0) {
          let messageFromYou: boolean = (message.from == 'You') || message.from == this.currentUserName ? true : false;
          returnObjArr.push({
            from: messageFromYou ? this.currentUserName : this.studentName,
            hasBeenRead: messageFromYou ? true :
              message.to.find(x => x.username == 'You' || x.username == this.currentUserName).hasBeenRead,
            message: message.message,
            dateCreated: this._utilityService.convertDateToLocalDate(message.dateCreated),
            id: message.id,
            typeId: CommentType.COMMENT,
            statusId: null,
            hasBeenReviewed: null,
            messageFromYou: messageFromYou,
            recipientId: message.to.length > 0 ? message.to[0].id : null
          })
        }

      })
    }

    if (logs && logs.length > 0) {

      let sortedLogs = logs.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
        return da - db;
      });

      let lastLog: UserOccupationPlanLog = sortedLogs[sortedLogs.length - 1];
      logs.forEach((log: UserOccupationPlanLog) => {

        //only review if statusId == 2 and lastLog == log.id
        returnObjArr.push({
          from: (log.statusId == 2) ? this.studentName : this.currentUserName,
          hasBeenRead: (log.statusId == 2) ? log.hasBeenRead : true, //only show log as unread to mentor if submitted status
          message: 'xxx',
          dateCreated: this._utilityService.convertDateToLocalDate(log.dateLogged),
          id: log.id,
          typeId: CommentType.LOG,
          statusId: log.statusId,
          hasBeenReviewed: (lastLog.statusId == 5) ? true : (log.statusId == 2 && log.id == lastLog.id) ? false : true,
          messageFromYou: null,
          recipientId: null
        })
      })
    }

    return returnObjArr;

  }

  getUnreadPlanCount(plan: any) {
    if (plan.combinedList) {
      return plan.combinedList.filter(x => !x.hasBeenRead).length;
    }
  }

  getOccupationPlanStatusCssClass(occupationPlan) {
    return this._utilityService.getUserOccupationStatusCssClass(occupationPlan, this.mentorLinkId) + ' mr_10';
  }

  showPlanDropdown(occupationPlan) {
    if ((this.getOccupationPlanStatusCssClass(occupationPlan).includes('not_started')
      || this.getOccupationPlanStatusCssClass(occupationPlan).includes('in_progress'))
      && occupationPlan.combinedList
      && occupationPlan.combinedList.length === 0
    ) {
      return false;
    }
    return true;
  }

  getAsvabDate(student) {

    if (student) {

      let date = new Date(student.administeredYear, student.administeredMonth - 1, student.administeredDay);

      return date;

    } else {
      return "";

    }
  }

  applyDateFormat(date) {

    if (date) {
      return formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(date)), 'MM/dd/yyyy', this.locale);
    }

    return "";
  }

  abbreviateTitle(title) {
    if (title && title.length > this.charCutoff) {
      return title.slice(0, this.charCutoff) + ' ...';
    }

    return title;
  }

}


