import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPortfolioComponent } from './print-portfolio.component';

describe('PrintPortfolioComponent', () => {
  let component: PrintPortfolioComponent;
  let fixture: ComponentFixture<PrintPortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintPortfolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
