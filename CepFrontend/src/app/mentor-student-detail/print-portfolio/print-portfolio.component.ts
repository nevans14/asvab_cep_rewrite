import { ElementRef, Inject, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MentorService } from 'app/services/mentor.service';
import { UtilityService } from 'app/services/utility.service';
import html2canvas from "html2canvas";
import * as cloneDeep from 'lodash/cloneDeep';

declare var pdfMake: any;

@Component({
  selector: 'app-print-portfolio',
  templateUrl: './print-portfolio.component.html',
  styleUrls: ['./print-portfolio.component.scss']
})
export class PrintPortfolioComponent implements OnInit {

  public isLoading: boolean;
  public printObj: any;

  public mentorLinkId: any;
  studentName: string;
  mobileOrTablet: boolean = false;

  constructor(public dialogRef: MatDialogRef<PrintPortfolioComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _mentorService: MentorService,
    private _utilityService: UtilityService) {

    this.isLoading = true;
    this.mentorLinkId = this.data['mentorLinkId'];
    this.printObj = {};
    this.studentName = this.data['studentName'];

    this.mobileOrTablet = this._utilityService.mobileOrTablet();
  }

  ngOnInit() {

    Promise.resolve(this._mentorService.getStudentPortfolioReport(this.mentorLinkId).then((response: any) => {

      this.setPrintObj(response).then((success) => {
        this.downloadPdfNew();
        this.isLoading = false;
        this.dialogRef.close();
      }).catch(error => {
        this.isLoading = false;
        console.error("ERROR", error);
      });

      this.dialogRef.close();

    }).catch((error: any) => {
      console.error("ERROR", error);
      this.isLoading = false;
    }));

  }

  async setPrintObj(response: any) {

    //set user info
    try {

      if (response.userInfo) {
        this.printObj.fullName = response.userInfo.name;
        this.printObj.grade = response.userInfo.gpa;
      } else {
        this.printObj.fullName = '';
        this.printObj.grade = '';
      }

      //set plan info
      this.printObj.combinedPlanObj = {};
      let isCombinedPlanFormat: boolean = (response.plans) && this._utilityService.isJson(response.plans.plan);
      if (response.plans && isCombinedPlanFormat) {
        // Update combined plan case
        this.printObj.combinedPlanObj.plan = JSON.parse(response.plans.plan);
        this.printObj.combinedPlanObj.description = response.plans.description;
        // this.printObj.combinedPlanObj.id = response.plans.id;
        //this.combinedPlanObj.userId = JSON.parse(this.plan[0].userId);
      } else if ((response.plans && Array.isArray(response.plans) && response.plans.length > 0) && !isCombinedPlanFormat) {
        let self = this;
        // Migrate old to new format case
        response.plans.forEach(function (p) {
          switch (p.plan) {
            case '4 Year College/University':
              self.printObj.combinedPlanObj.plan.fourYearCollege = true;
              break;
            case '2 Year College':
              self.printObj.combinedPlanObj.plan.twoYearCollege = true;
              break;
            case 'Career and Technical Education':
              self.printObj.combinedPlanObj.plan.careerAndTech = true;
              break;
            case 'Military':
              self.printObj.combinedPlanObj.plan.military = true;
              break;
            case 'Work':
              self.printObj.combinedPlanObj.plan.work = true;
              break;
            case 'Gap Year':
              self.printObj.combinedPlanObj.plan.gapYear = true;
              break;
            case 'Undecided':
              self.printObj.combinedPlanObj.plan.undecided = true;
              break;
          }

          if (p.description) {
            self.printObj.combinedPlanObj.description =
              self.printObj.combinedPlanObj.description.concat(p.description, "\n");
          }
        })
      } // if neither, then it means we add combined plan
      this.printObj.originalPlanObj = cloneDeep(this.printObj.combinedPlanObj);

      //set work experience info
      this.printObj.workExperiences = response.workExperiences;

      //set education info
      this.printObj.educations = response.educations;

      //set scores
      this.printObj.asvabScores = response.asvabScores;
      this.printObj.actScores = response.actScores;
      this.printObj.satScores = response.satScores;
      this.printObj.otherScores = response.otherScores;

      //set achievements
      this.printObj.achievements = response.achievements;

      //set skills
      this.printObj.skills = response.skills;

      //set volunteers
      this.printObj.volunteers = response.volunteers;

      //set fyiScores
      this.printObj.fyiScores = response.fyiScores;

      //set interests
      this.printObj.interests = response.interests;

      //set workValues
      this.printObj.workValues = response.workValues;
      this.printObj.workValueTopResults = null;  //TODO figure out

      //set fav occ
      this.printObj.favoriteOccupations = response.favoriteOnetOccupations;

      //set favorite MilitaryCareers
      this.printObj.favoriteMilitaryCareers = response.favoriteMilitaryCareers;

      //set favorite CareerClusters
      this.printObj.favoriteCareerClusters = response.favoriteCareerClusters;

      //set favorite Colleges
      this.printObj.favoriteColleges = response.favoriteColleges;

      return Promise.resolve();
    } catch (error) {
      console.error("ERROR", error);
      return Promise.reject();
    }

  }

  downloadPdfNew() {
    setTimeout(() => this.triggerDownload());
  }

  triggerDownload() {
    let t = this.getCanvas();
    t.then((s: any) => {

      let docDefinition = {
        pageOrientation: 'portrait',
        pageSize: 'LETTER',
        content: s,
        pageBreak: 'after'
      };

      let fileName = this.studentName.replace(' ', '_') + '_portfolio.pdf';
      pdfMake.createPdf(docDefinition).download(fileName);
    });

    t.catch(error => {
      console.error("ERROR", error);
      alert(error);
    });
  }

  getCanvas() {
    let self = this;
    const tag = document.getElementById('printSectionNew');
    if (tag && tag.style) {
      tag.style.opacity = "1.0";
    }

    return new Promise((resolve, reject) => {
      html2canvas(tag, {
        imageTimeout: 2000,
        removeContainer: true,
        backgroundColor: '#fff',
        allowTaint: true,
        logging: false,
        scale: 1,
        ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
          return node.nodeName === 'IFRAME';
        }
      }).then(function (canvas) {

        let splitAt = self.mobileOrTablet ? 540 : 1050;

        let images = [];
        let y = 0;
        while (canvas.height > y) {
          const region = self.getClippedRegion(canvas, 0, y, canvas.width, splitAt)

          // Stop generate image if blank
          if (!region.image) {
            break;
          }

          images.push(region);
          y += splitAt;
        }

        tag.style.opacity = "0.0";
        resolve(images);

      }).catch(function (error) {
        reject(error);
      });
    });

  }

  getClippedRegion(image, x, y, width, height) {
    var canvas = document.createElement("canvas"),
      ctx = canvas.getContext("2d");

    canvas.width = width;
    canvas.height = height;

    ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

    let contentLocation = ctx.getImageData(0, 0, canvas.width, canvas.height).data.find(x => x !== 255);

    if (!contentLocation) {
        return { image: undefined };
    }

    return {
      image: canvas.toDataURL(),
      width: 520,
      alignment: 'center'
    };
  }

  getInterestTitle(code) {
    switch (code) {
      case 'R': return 'Realistic';
      case 'I': return 'Investigative';
      case 'A': return 'Artistic';
      case 'S': return 'Social';
      case 'E': return 'Enterprising';
      case 'C': return 'Conventional';
      default:
        return '';
    }
  }

}
