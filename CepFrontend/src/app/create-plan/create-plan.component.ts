import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { SchoolPlanService } from 'app/services/school-plan.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-create-plan',
  templateUrl: './create-plan.component.html',
  styleUrls: ['./create-plan.component.scss']
})
export class CreatePlanComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  public createUserOccupationPlan: UserOccupationPlan;
  favoriteList: any[];

  private occupation: any;
  private certificate: any;
  private technicalSchool: any;
  private associatesDegree: any;
  private bachelorsDegree: any;
  private advanceDegree: any;
  private military: any;
  private workBasedLearning: any;
  private vocationalTraining: any;
  private careerTechnicalEducation: any;
  private gapYear: any;
  private numChecked: number;
  private hasOnetSocCd: boolean;
  public isSaving: boolean;

  constructor(private _activatedRoute: ActivatedRoute,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _schoolPlanService: SchoolPlanService,
    private _router: Router,
    private _user: UserService,
  ) {
    this.numChecked = 0;
    this.hasOnetSocCd = false;
    this.occupation = null;
    this.isSaving = false;
  }

  ngOnInit() {

    this.favoriteList = this._activatedRoute.snapshot.data.createPlanResolver[0];
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = false;
    }

  }

  setCompletionCreatedPlan() {
    this._user.setCompletion(UserService.CREATED_PLAN, 'true');
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  checkChanged(value) {
    if (value) {
      this.numChecked++;
    } else {
      this.numChecked--;
    } 
  }

  occupationWasChanged(){
    this.hasOnetSocCd = true;
  }

  createPlan() {

    this.isSaving = true;
    let pathwayObj: any = {};

    //get check values for pathway
    if (this.certificate) {
      pathwayObj.certificate = this.certificate;
    }
    if (this.technicalSchool) {
      pathwayObj.technicalSchool = this.technicalSchool;
    }
    if (this.associatesDegree) {
      pathwayObj.associatesDegree = this.associatesDegree;
    }
    if (this.bachelorsDegree) {
      pathwayObj.bachelorsDegree = this.bachelorsDegree;
    }
    if (this.advanceDegree) {
      pathwayObj.advanceDegree = this.advanceDegree;
    }
    if (this.military) {
      pathwayObj.military = this.military;
    }
    if (this.workBasedLearning) {
      pathwayObj.workBasedLearning = this.workBasedLearning;
    }
    if (this.vocationalTraining) {
      pathwayObj.vocationalTraining = this.vocationalTraining;
    }
    if (this.careerTechnicalEducation) {
      pathwayObj.careerTechnicalEducation = this.careerTechnicalEducation;
    }
    if (this.gapYear) {
      pathwayObj.gapYear = this.gapYear;
    }

    this.createUserOccupationPlan = {
      id: null, //new record
      socId: this.occupation,
      pathway: JSON.stringify(pathwayObj),
      additionalNotes: "",
      useNationalAverage: true,
      statusId: 1
    }

    this._userOccupationPlanService.save(this.createUserOccupationPlan).then((plan: UserOccupationPlan) => {
      let newPlan: UserOccupationPlan = plan;
      //create initial records before navigating
      this._schoolPlanService.save(newPlan.id).then((rowsAffected: any) => {
        this._router.navigateByUrl('/edit-plan/' + newPlan.id);
      }).catch(error => {
        this.isSaving = false;
        console.error("Error: ", error);
      });
    }).catch(error => {
      this.isSaving = false;
      console.error("Error: ", error);
    });

  }
}
