import { formatDate } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, Input, LOCALE_ID, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { DateDialogComponent } from 'app/core/dialogs/date-dialog/date-dialog.component';
import { TaskDialogComponent } from 'app/core/dialogs/task-dialog/task-dialog.component';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-plan-calendar',
  templateUrl: './plan-calendar.component.html',
  styleUrls: ['./plan-calendar.component.scss']
})
export class PlanCalendarComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs

  public userOccupationPlanCalendarTasks: UserOccupationPlanCalendarTask[];
  public userOccupationPlanCalendars: any[];
  public tasksUnassignedToDate: UserOccupationPlanCalendarTask[];
  public tasksUnassignedLength: number;
  public userOccupationPlans: any[];
  public showCompletedTasks: boolean;
  public unassignedProgress: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _planCalendarService: PlanCalendarService,
    private _utilityService: UtilityService,
    @Inject(LOCALE_ID) private locale: string
  ) {

    this.userOccupationPlanCalendarTasks = [];
    this.userOccupationPlanCalendars = [];
    this.userOccupationPlans = [];


    this.userOccupationPlanCalendarTasks = this._activatedRoute.snapshot.data.planCalendarResolver[0];
    this.userOccupationPlanCalendars = this._activatedRoute.snapshot.data.planCalendarResolver[1];
    this.userOccupationPlans = this._activatedRoute.snapshot.data.planCalendarResolver[2];

    this.tasksUnassignedToDate = [];
    this.tasksUnassignedLength = 0;
    this.showCompletedTasks = false;  //for unassigned tasks to dates

    //create initial filter option for dates with tasks
    //create initial option for expanding list to true
    this.userOccupationPlanCalendars.map((userOccupationPlanCalendar) => {
      userOccupationPlanCalendar.showCompletedTasks = false;
      userOccupationPlanCalendar.expanded = false;

      return userOccupationPlanCalendar;
    });

  }

  ngOnInit() {
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = false;
    }

    this.setView();

  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  addDate() {
    let data = {
      // planId: this.userOccupationPlan.id
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        let userOccupationPlanCalendar: any = result;

        if (userOccupationPlanCalendar.tasks) {
          if (userOccupationPlanCalendar.tasks.length > 0) {
            userOccupationPlanCalendar.showCompletedTasks = false;
            userOccupationPlanCalendar.expanded = true;
          }
        }
        this.userOccupationPlanCalendars.push(userOccupationPlanCalendar);

      }
    });

  }

  addTask() {

    let data = {
      // planId: this.userOccupationPlan.id
    }

    const dialogRef = this._dialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        if (userOccupationPlanCalendarTask.calendarId) {

          this.userOccupationPlanCalendars.find(x => x.id == userOccupationPlanCalendarTask.calendarId).tasks.push(userOccupationPlanCalendarTask);

        } else {
          this.userOccupationPlanCalendarTasks.push(userOccupationPlanCalendarTask);
        }

        this.setView();

      }
    });
  }

  setView() {

    //set tasks
    this.tasksUnassignedToDate = this.userOccupationPlanCalendarTasks.filter(x => (!x.calendarId && (x.completed == this.showCompletedTasks)));
    this.tasksUnassignedLength = this.tasksUnassignedToDate.length;
    this.unassignedProgress = this.getProgressBarValue(this.userOccupationPlanCalendarTasks.length, this.tasksUnassignedToDate.length);

  }

  getProgressBarValue(max: number, current: number) {
    return (max-current) / max * 100;
  }

  getProgressBarValueForCalendar(calendar: UserOccupationPlanCalendar) {

    //set tasks
    let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

    return (this.getProgressBarValue(calendar.tasks.length, notCompletedTasks.length));

  }

  getRemainingTasksForCalendar(calendar: UserOccupationPlanCalendar) {

    //get tasks
    let notCompletedTasks = calendar.tasks.filter(x => !x.completed);

    if (notCompletedTasks) {
      return notCompletedTasks.length;
    }

    return 0;
  }

  switchTaskViewByCompleted() {

    this.showCompletedTasks = !this.showCompletedTasks;

    if (this.showCompletedTasks) {
      this.tasksUnassignedToDate = this.userOccupationPlanCalendarTasks.filter(x => (!x.calendarId));
    } else {
      this.tasksUnassignedToDate = this.userOccupationPlanCalendarTasks.filter(x => (!x.calendarId && (x.completed == this.showCompletedTasks)));

    }

  }

  editTask(task: UserOccupationPlanCalendarTask, index, calendarIndex = null) {

    let data = {
      planId: task.planId,
      task: task,
      canEditPlan: true
    }

    const dialogRef = this._dialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let updatedPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        //check to see if updated record has a calendar id
        if (updatedPlanCalendarTask.calendarId) {

          //remove from previous calendar only if id's do not match up

          //check if accessed from calendar tasks
          if (calendarIndex == null) {

            //edit task was from unassigned
            //assign to new calendar
            //add record to ui
            this.userOccupationPlanCalendars.find(x => x.id == updatedPlanCalendarTask.calendarId).tasks.push(updatedPlanCalendarTask);

            //remove from unassigned tasks in ui
            this.userOccupationPlanCalendarTasks.splice(this.userOccupationPlanCalendarTasks.findIndex(x => x.id == updatedPlanCalendarTask.id), 1);
            this.setView();

          } else {
            let prevUserOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

            //remove from previous calendar only if previous record had a calendar id and it doesn't match the new one
            if (prevUserOccupationPlanCalendar.calendarId != updatedPlanCalendarTask.calendarId) {

              let removeIndex = prevUserOccupationPlanCalendar.tasks.findIndex(x => x.id == updatedPlanCalendarTask.id);
              this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);

              //add record to ui
              this.userOccupationPlanCalendars.find(x => x.id == updatedPlanCalendarTask.calendarId).tasks.push(updatedPlanCalendarTask);

            }

          }

        } else {

          //no calendar date was assigned to modified task

          //check if accessed from calendar tasks i.e. had a previously assigned calendar date
          //if so remove it from ui
          if (calendarIndex == null) {

            let taskIndex = this.userOccupationPlanCalendarTasks.findIndex(x => x.id == updatedPlanCalendarTask.id);
            
            if (taskIndex > -1) {
              //unassigned task to a date simply update based on response
              this.userOccupationPlanCalendarTasks[taskIndex] = updatedPlanCalendarTask;

              this.setView();
            }

          } else {
            let userOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

            let removeIndex = userOccupationPlanCalendar.tasks.findIndex(x => x.id == updatedPlanCalendarTask.id);
            this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);

            //add record to ui
            this.userOccupationPlanCalendarTasks.push(updatedPlanCalendarTask);
            this.setView();
          }

        }

      }
    });

  }

  removeTask(taskId, index, calendarIndex = null) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Task, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._planCalendarService.deleteTask(taskId).then((rowsAffected: number) => {

            if (rowsAffected > 0) {

              if (calendarIndex == null) {
                //update ui for unassigned tasks
                this.userOccupationPlanCalendarTasks.splice(this.userOccupationPlanCalendarTasks.findIndex(x => x.id == taskId), 1);
                this.setView();
              } else {
                let userOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

                let removeIndex = userOccupationPlanCalendar.tasks.findIndex(x => x.id == taskId);
                this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);
              }


            } else {
              console.error("ERROR, record was not deleted. Please try again.");
            }

          }).catch(error => console.error("ERROR", error));

        }
      }
    });
  }

  setTaskComplete(task: UserOccupationPlanCalendarTask, taskIndex, event, calendarIndex = null) {

    const verbiage: string = (event.target.checked) ? "Complete" : "Reset";

    let dialogData = {
      title: 'Confirm Completion',
      message: verbiage + ' task <strong>' + this.convertTaskName(task.taskName) + '</strong>, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          task.completed = event.target.checked;
          //updating an existing record
          this._planCalendarService.updateTask(task, task.id).then((rowsAffected: number) => {

            if (task.calendarId) {
              this.userOccupationPlanCalendars.find(x => x.id == task.calendarId).tasks.find(y => y.id == task.id).completed = event.target.checked;
              // this.userOccupationPlanCalendars[calendarIndex].tasks[taskIndex].completed = true;
            } else {
              this.userOccupationPlanCalendarTasks[this.userOccupationPlanCalendarTasks.findIndex(x => x.id == task.id)].completed = event.target.checked;
            }

            this.setView(); //refresh view

          }).catch((error: any) => {
            console.error("ERROR", error);
          });

        } else {
          event.target.checked = !event.target.checked;
        }
      } else {
        event.target.checked = !event.target.checked;
      }
    });

  }

  gotoPlan(planId) {
    this._router.navigate(["/view-plan/" + planId]);
  }

  getPlanTitle(planId) {

    let occuPlan = this.userOccupationPlans.find(x => x.id == planId);
    if (occuPlan) {
      return "Linked to " + occuPlan.occuTitle.title;
    } else {
      return "";
    }
    // return this.userOccupationPlans.find(x => x.id == planId).occuTitle.title;
  }

  editDate(date: UserOccupationPlanCalendar) {

    let data = {
      userOccupationPlanCalendar: date,
      canEditPlan: true
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendar: UserOccupationPlanCalendar = result;

        let existingIndex = this.userOccupationPlanCalendars.findIndex(x => x.id == userOccupationPlanCalendar.id);

        //update tasks
        if (userOccupationPlanCalendar.tasks) {
          userOccupationPlanCalendar.tasks.forEach((task: UserOccupationPlanCalendarTask) => {

            let index = this.userOccupationPlanCalendars[existingIndex].tasks.findIndex(x => x.id == task.id);

            if (index > -1) {
              //record was found; update
              this.userOccupationPlanCalendars[existingIndex].tasks[index] = task;
            } else {
              this.userOccupationPlanCalendars[existingIndex].tasks.push(task);
            }

          });
        }

        this.userOccupationPlanCalendars[existingIndex].planId = userOccupationPlanCalendar.planId;
        this.userOccupationPlanCalendars[existingIndex].title = userOccupationPlanCalendar.title;

        this.userOccupationPlanCalendars[existingIndex].date = 
          formatDate(new Date(userOccupationPlanCalendar.date).toLocaleDateString(),'MM/dd/yyyy', this.locale);
      }
    });

  }

  removeDate(calendarId) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Date, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._planCalendarService.deleteCalendar(calendarId).then((rowsAffected: number) => {

            if (rowsAffected > 0) {
              //remove record from ui
              let removeIndex = this.userOccupationPlanCalendars.findIndex(x => x.id == calendarId);
              this.userOccupationPlanCalendars.splice(removeIndex, 1);
            } else {
              console.error("ERROR, record was not deleted. Please try again.");
            }

          }).catch(error => console.error("ERROR", error));

        }
      }
    });
  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

  hasUrl(taskName): boolean {
    if (this._utilityService.isJson(taskName)) {
      return true;
    }

    return false;
  }

  getUrl(taskName) {
    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).url;
    }

    return taskName;
  }
}
