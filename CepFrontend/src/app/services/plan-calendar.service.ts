import { Injectable } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { UserGapYearPlanComponent } from 'app/edit-plan/gap-year-plan/user-gap-year-plan/user-gap-year-plan.component';
import { filter } from 'rxjs/operators';
import { HttpHelperService } from './http-helper.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class PlanCalendarService {

  private controllerName: string = "plan-your-future";


  constructor(private _httpHelper: HttpHelperService,
    private _utilityService: UtilityService) { }

  getCalendars() {
    const endPoint = this.controllerName + '/plans/calendars';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getTasks() {
    const endPoint = this.controllerName + '/plans/tasks';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updateTask(formData, taskId) {
    const endPoint = this.controllerName + '/plans/tasks/' + taskId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveTask(formData) {
    const endPoint = this.controllerName + '/plans/tasks';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteTask(id) {
    const endPoint = this.controllerName + '/plans/tasks/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  savePlanTask(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/tasks';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updateCalendar(formData, calendarId) {
    const endPoint = this.controllerName + '/plans/calendars/' + calendarId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updatePlanCalendar(formData, prevPlanId, calendarId) {
    const endPoint = this.controllerName + '/plans/' + prevPlanId + '/calendars/' + calendarId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveCalendar(formData) {
    const endPoint = this.controllerName + '/plans/calendars';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  savePlanCalendar(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/calendars';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteCalendar(id) {
    const endPoint = this.controllerName + '/plans/calendars/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getPlanCalendars(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/calendars';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getPlanTasks(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/tasks';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
   * 
   * @param type UserCollegePlan, UserMilitaryPlan
   * @param planId 
   * @param uniqueId 
   * @param taskUrl 
   * @param taskTitle 
   * @returns 
   */
  saveAutoDateTask(type: string, planId, uniqueId, taskUrl = "", taskTitle = "") {

    let task: UserOccupationPlanCalendarTask = {
      planId: planId,
      id: null,
      calendarId: null,
      completed: false,
      taskName: JSON.stringify(this.convertTaskToJSON(type, uniqueId, taskUrl, taskTitle))
    };

    return this.savePlanTask(planId, task);

  }

  deleteAutoTasks(type: string, planId: number, uniqueId: number) {

    return this.getPlanTasks(planId).then((planTasks: UserOccupationPlanCalendarTask[]) => {

      let autoPlanTasks = planTasks.filter(x => this._utilityService.isJson(x.taskName));

      if (autoPlanTasks) {

        let tasksToDelete = [];

        autoPlanTasks.forEach((x: UserOccupationPlanCalendarTask) => {

          let parseObj = JSON.parse(x.taskName);

          if (parseObj.type == type && parseObj.id == uniqueId) {
            tasksToDelete.push(x);
          }

        });

        return Promise.all(tasksToDelete.map((x: UserOccupationPlanCalendarTask) => {
          return this.deleteTask(x.id).then((success: any) => {
            return x;
          });
        }));

      }

    })
  }

  saveAutoDateForCollegePlan(planId, dateTitle, date) {


    let formData: UserOccupationPlanCalendar = {
      id: null,
      planId: planId,
      title: dateTitle,
      date: date,
      isGraduationDate: false,
      tasks: null
    }

    const endPoint = this.controllerName + '/plans/' + planId + '/calendars';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  saveAutoDateTaskForCollegePlan(planId, collegePlanId, dateTitle, date, taskUrl = "", taskTitle = "") {

    let tasks: UserOccupationPlanCalendarTask[] = [];
    let task: UserOccupationPlanCalendarTask = {
      planId: planId,
      id: null,
      calendarId: null,
      completed: false,
      taskName: JSON.stringify(this.convertTaskToJSON('UserCollegePlan', collegePlanId, taskUrl, taskTitle))
    };

    tasks.push(task);

    let formData: UserOccupationPlanCalendar = {
      id: null,
      planId: planId,
      title: dateTitle,
      date: date,
      isGraduationDate: false,
      tasks: tasks
    }

    const endPoint = this.controllerName + '/plans/' + planId + '/calendars';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  checkAutoTaskExistsForPlan(type, planId, id, title: string) {

    return this.getPlanCalendars(planId).then((response: UserOccupationPlanCalendar[]) => {

      if (response.length > 0) {

        let planCalendarsWithTitle = response.filter(x => x.title.toUpperCase() == title.toUpperCase());

        if (planCalendarsWithTitle.length > 0) {

          return true;
          // let filterRecords = response.filter(x => this._utilityService.isJson(x.taskName));

          // if (filterRecords.length > 0) {
          //   let autoJsonRecords = response.filter(x => JSON.parse(x.taskName).type == type && JSON.parse(x.taskName).id == id &&
          //     JSON.parse(x.taskName).title.toUpperCase() == title.toUpperCase());

          //   if (autoJsonRecords.length > 0) {
          //     return true;
          //   }
          // }

        } else {
          return false;
        }
      } else {
        return false;
      }
    });

  }

  /**
   * Only goes through unassigned tasks for the plan
   * @param type 
   * @param planId 
   * @param id 
   * @param title 
   * @returns 
   */
  getAutoTasksForPlan(type, planId, id) {

    return this.getPlanTasks(planId).then((response: UserOccupationPlanCalendarTask[]) => {

      if (response.length > 0) {
        let filterRecords = response.filter(x => this._utilityService.isJson(x.taskName));

        if (filterRecords.length > 0) {

          let autoJsonRecords = filterRecords.filter((x: UserOccupationPlanCalendarTask) => {
            let parseObj = JSON.parse(x.taskName);
            if(parseObj.type == type && parseObj.id == id){
              return x;
            }
          });

          if (autoJsonRecords.length > 0) {
            return autoJsonRecords;
          }
        }

      }

      return null;
    });

  }

  convertTaskToJSON(type: string, id: any, url: string, title: string) {

    let returnObj = {
      type: type,
      id: id,
      url: url,
      title: title
    }
    return returnObj;

  }

  // save(planId, formData){
  //   const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans';

  //   return new Promise((resolve, reject) => {
  //     this._httpHelper.httpHelper('post', endPoint, null, formData)
  //         .then((obj) => {
  //             resolve(obj);
  //         }).catch(error => {
  //             reject(error);
  //         });
  //   });
  // }

  // update(planId, id, formData){
  //   const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans/' + id;

  //   return new Promise((resolve, reject) => {
  //     this._httpHelper.httpHelper('put', endPoint, null, formData)
  //         .then((obj) => {
  //             resolve(obj);
  //         }).catch(error => {
  //             reject(error);
  //         });
  //   });
  // }

  // delete(planId, id){
  //   const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans/' + id;

  //   return new Promise((resolve, reject) => {
  //     this._httpHelper.httpHelper('delete', endPoint, null, null)
  //         .then((obj) => {
  //             resolve(obj);
  //         }).catch(error => {
  //             reject(error);
  //         });
  //   });
  // }

  // getAllGapYearOptions(){
  //   const endPoint = this.controllerName + '/plans/gap-year-plan-options';
  //   return new Promise((resolve, reject) => {
  //     this._httpHelper.httpHelper('get', endPoint, null, null)
  //         .then((obj) => {
  //             resolve(obj);
  //         }).catch(error => {
  //             reject(error);
  //         });
  //   });
  // }

  // getAllGapYearTypes() : Promise<RefGapYearType[]>{
  //   const endPoint = this.controllerName + '/plans/gap-year-plan-types';
  //   return new Promise((resolve, reject) => {
  //     this._httpHelper.httpHelper('get', endPoint, null, null)
  //         .then((obj: RefGapYearType[]) => {
  //             resolve(obj);
  //         }).catch(error => {
  //             reject(error);
  //         });
  //   });
  // }


}
