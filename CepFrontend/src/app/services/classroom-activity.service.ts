import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class ClassroomActivityService {

  private controllerName: string = "classroom-activity";

  constructor(private _httpHelper: HttpHelperService) { }

  saveWork(classroomActivityId, formData) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  resubmitWork(classroomActivityId, classroomActivitySubmissionId, formData) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions/' + classroomActivitySubmissionId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  unsubmitWork(classroomActivityId, classroomActivitySubmissionId, formData) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions/' + classroomActivitySubmissionId + '/unsubmit';


    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }


  getCategories() {
    const endPoint = this.controllerName + '/get-categories';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getActivities() {
    const endPoint = this.controllerName + '/get-activities';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getMyActivitySubmissions(classroomActivityId) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteFile(classroomActivityId, classroomActivitySubmissionId) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions/' + classroomActivitySubmissionId + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getActivityComments(classroomActivityId) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveActivityComment(classroomActivityId, formData) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/comments';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updateActivityCommentAsRead(classroomActivityId, commentId, recipientId, hasBeenRead) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/comments/' + commentId + '/recipients/' + recipientId + '/read/' + hasBeenRead;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getUnreadActivityComments() {
    const endPoint = this.controllerName + '/unread-comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getAllActivityComments() {
    const endPoint = this.controllerName + '/comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getMySubmissionLogs(workId) {
    const endPoint = this.controllerName + '/' + workId + '/logs';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getSubmissionMentors(classroomActivityId, workId) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions/' + workId;
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  markSubmissionLogAsRead(classroomActivityId, submissionId, logId, hasBeenRead) {
    const endPoint = this.controllerName + '/' + classroomActivityId + '/submissions/' + submissionId + "/logs/" + logId + "/read/" + hasBeenRead;
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

}
