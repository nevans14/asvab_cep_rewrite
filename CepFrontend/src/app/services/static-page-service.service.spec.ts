import { TestBed } from '@angular/core/testing';

import { StaticPageServiceService } from './static-page-service.service';

describe('StaticPageServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StaticPageServiceService = TestBed.get(StaticPageServiceService);
    expect(service).toBeTruthy();
  });
});
