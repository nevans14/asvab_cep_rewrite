import { TestBed } from '@angular/core/testing';

import { CollegePlanService } from './college-plan.service';

describe('CollegePlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollegePlanService = TestBed.get(CollegePlanService);
    expect(service).toBeTruthy();
  });
});
