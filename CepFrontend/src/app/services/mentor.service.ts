import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class MentorService {

  private controllerName: string = "mentors";
  private subject = new Subject<any>();

  constructor(private _httpHelper: HttpHelperService,
    private _http: HttpClient,
    private _config: ConfigService) { }

  save(formData) {
    const endPoint = this.controllerName + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  delete(mentorLinkId) {
    const endPoint = this.controllerName + '/' + mentorLinkId + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getPendingMentors() {
    const endPoint = this.controllerName + '/pending-mentors';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getAll() {
    const endPoint = this.controllerName + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentDetails(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentCareerPlans(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/occupations';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentCareerPlanInfo(mentorLinkId, planId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/occupations/' + planId + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
   * Returns a list of calendar entries.
   * @param mentorLinkId 
   */
  getStudentCalendarEntries(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/calendars';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
   * Returns a list of tasks that are not assigned to a date
   * @param mentorLinkId 
   * @returns 
   */
  getStudentGlobalTasks(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/tasks';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }



  /**
   * 
   * @param mentorLinkId 
   * @returns returns data for UI
   */
  getStudentPortfolio(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolios';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
   * 
   * @param mentorLinkId 
   * @returns data needed to create a report to print for the user
   */
  getStudentPortfolioReport(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolios/report';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentAsvabScores(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/asvab-scores/report';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentSubmittedActivities(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/classroom-activities';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentSubmittedActivityFile(mentorLinkId, classroomActivityId, submissionId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/classroom-activities/' + classroomActivityId + '/submissions/' + submissionId + '/';

    return new Promise((resolve, reject) => {

      this._http.get(this._config.getFullUrl(endPoint, null), { responseType: 'Blob' as 'json' })
        .toPromise()
        .then(obj => {
          resolve(obj);
        })
        .catch(err => {
          // errorContext = (err.error || 'Server error');
          reject((err.error || 'Server error'));
        });
    });


    // const userHttpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': 'Basic ' + this.authToken
    //   }),
    //   responseType: 'blob'
    // };


    // , {headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken))}

    // return new Promise((resolve, reject) => {
    //   this._httpHelper.httpHelper('get', endPoint, null, null)
    //     .then((obj) => {
    //       resolve(obj);
    //     }).catch(error => {
    //       reject(error);
    //     });
    // });
  }

  getFile(endPoint) {
    return new Promise((resolve, reject) => {

      this._http.get(this._config.getFullUrl(endPoint, null), { responseType: 'Blob' as 'json' })
        .toPromise()
        .then(obj => {
          resolve(obj);
        })
        .catch(err => {
          // errorContext = (err.error || 'Server error');
          reject((err.error || 'Server error'));
        });
    });
  }

  setStudentSubmissionAsReviewed(mentorLinkId, classroomActivityId, submissionId, hasBeenReviewed) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/classroom-activities/' + classroomActivityId + '/submissions/' + submissionId + '/reviewed/' + hasBeenReviewed + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  setStudentSubmissionLogAsRead(mentorLinkId, classroomActivityId, submissionId, logId, hasBeenRead) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/classroom-activities/' + classroomActivityId + '/submissions/' + submissionId + '/logs/' + logId + '/read/' + hasBeenRead + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
 * 
 * @param mentorLinkId 
 * @returns any comments by the student to the mentor or mentor to the student
 */
  getStudentPortfolioComments(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolios/comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudentSubmittedPortfolios(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolio';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  setStudentPortfolioAsReviewed(mentorLinkId, hasBeenReviewed) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolios/reviewed/' + hasBeenReviewed + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  setStudentPortfolioLogAsRead(mentorLinkId, portfolioLogId, hasBeenRead) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/portfolios/logs/' + portfolioLogId + '/read/' + hasBeenRead + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  sendReminderEmail(mentorLinkId) {
    const endPoint = this.controllerName + '/pending-mentors/' + mentorLinkId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getStudents() {
    const endPoint = this.controllerName + '/students/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getPendingStudents() {
    const endPoint = this.controllerName + '/students/pending/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  sendUpdate(message) {
    this.subject.next(message);
  }

  getStudentUpdates(): Observable<any> {
    return this.subject.asObservable();
  }

  saveStudentsToNewGroup(formData) {
    const endPoint = this.controllerName + '/groups';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveStudentsToExistingGroup(formData, groupId) {
    const endPoint = this.controllerName + '/groups/' + groupId + '/students';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  /**
   * removes a student relationship
   */
  removeStudent(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  removeStudentsFromGroup(groupId, formData) {
    const endPoint = this.controllerName + '/groups/' + groupId + '/students/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('DELETE_WITH_BODY', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  removeGroup(groupId) {
    const endPoint = this.controllerName + '/groups/' + groupId + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updateGroup(groupId, formData) {
    const endPoint = this.controllerName + '/groups/' + groupId + '/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getGroups() {
    const endPoint = this.controllerName + '/groups/';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  markStudentOccupationPlanLogAsRead(mentorLinkId, planId, logId, hasBeenRead) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/occupations/' + planId + '/logs/' + logId + '/read/' + hasBeenRead + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  markStudentOccupationPlanAsReviewed(mentorLinkId, planId, hasBeenReviewed) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/occupations/' + planId + '/reviewed/' + hasBeenReviewed + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  acceptStudentInvite(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/accept/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('POST', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  denyStudentInvite(mentorLinkId) {
    const endPoint = this.controllerName + '/students/' + mentorLinkId + '/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('DELETE', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }


}
