import { Injectable } from '@angular/core';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollegePlanObservableService {

  newCollegePlan: UserCollegePlan = {
    id: null,
    planId: null,
    unitId: null,
    isSeekingInStateTuition: null,
    degree: null,
    major: null,
    familyContribution: null,
    scholarshipContribution: null,
    savings: null,
    loan: null,
    other: null,
    schoolProfile: null
  }

  private collegePlanSubject$ = new BehaviorSubject<UserCollegePlan[]>([]);

  public readonly collegePlanChanged$: Observable<UserCollegePlan[]> = this.collegePlanSubject$.asObservable();

  constructor() { }

  set(_userCollegePlans: UserCollegePlan[]) {
    this.collegePlanSubject$.next([]);
    this.collegePlanSubject$.next(_userCollegePlans);
  }

  add(_userCollegePlan: UserCollegePlan) {
    this.newCollegePlan = _userCollegePlan;
    this.collegePlanSubject$.next([...this.collegePlanSubject$.value, _userCollegePlan]);
  }

  remove(_userCollegePlanId: number) {

    const userCollegePlans: UserCollegePlan[] = this.collegePlanSubject$.getValue();

    userCollegePlans.forEach((item, index) => {
      if (item.id === _userCollegePlanId) { userCollegePlans.splice(index, 1); }
    });
    this.collegePlanSubject$.next(userCollegePlans);

  }

  update(_userCollegePlan: UserCollegePlan){
    const userCollegePlans: UserCollegePlan[] = this.collegePlanSubject$.getValue();

    userCollegePlans.forEach((item, index) => {
      if (item.id === _userCollegePlan.id) { userCollegePlans[index] = _userCollegePlan; }
    });
    this.collegePlanSubject$.next(userCollegePlans);

  }

  unsubscribe(){
    this.collegePlanSubject$.next([]);
  }

}
