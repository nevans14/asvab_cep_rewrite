import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class StaticPageServiceService {

  constructor(private _httpHelper: HttpHelperService,
		    private _congfigService: ConfigService,
		    private _http: HttpClient) { }
	
	getPageById(pageId): Observable<any>  {
	    return this._http.get(this._congfigService.getAwsFullUrl('/api/pages/get-by-page-id/' + pageId + '/'));
	  }

	getPageByPageName(pageName): Observable<any>  {
	    return this._http.get(this._congfigService.getAwsFullUrl('/api/pages/get-by-page-name/' + pageName + '/'));
	  }

	
}
