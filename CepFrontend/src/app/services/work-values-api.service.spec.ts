import { TestBed } from '@angular/core/testing';

import { WorkValuesApiService } from './work-values-api.service';

describe('WorkValuesApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkValuesApiService = TestBed.get(WorkValuesApiService);
    expect(service).toBeTruthy();
  });
});
