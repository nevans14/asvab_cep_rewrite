import { TestBed } from '@angular/core/testing';

import { FlickrRestFactoryService } from './flickr-rest-factory.service';

describe('FlickrRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlickrRestFactoryService = TestBed.get(FlickrRestFactoryService);
    expect(service).toBeTruthy();
  });
});
