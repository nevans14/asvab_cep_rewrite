import { TestBed } from '@angular/core/testing';

import { MilitaryPlanService } from './military-plan.service';

describe('MilitaryPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MilitaryPlanService = TestBed.get(MilitaryPlanService);
    expect(service).toBeTruthy();
  });
});
