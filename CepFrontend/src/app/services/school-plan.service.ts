import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class SchoolPlanService {

  private controllerName: string = "plan-your-future";

  constructor(private _httpHelper: HttpHelperService) { }

  save(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/school-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  update(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/school-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  get(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/school-plans';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteSchoolCourse(planId, schoolCourseId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/school-courses/' + schoolCourseId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteSchoolActivity(planId, schoolActivityId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/school-activities/' + schoolActivityId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }
}
