import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { formatDate, ViewportScroller } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { CookieService } from 'ngx-cookie-service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { ConfigService } from 'app/services/config.service';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { AsvabSampleTestDialogComponent } from 'app/core/dialogs/asvab-sample-test-dialog/asvab-sample-test-dialog.component';
import { AsvabIcatSampleTestDialogComponent } from 'app/core/dialogs/asvab-icat-sample-test-dialog/asvab-icat-sample-test-dialog.component';

const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  facebookUrl = 'https://www.facebook.com/sharer/sharer.php?';
  twitterUrl = 'https://twitter.com/intent/tweet?';
  linkedInUrl = 'https://www.linkedin.com/sharing/share-offsite/?';

  domain =
    (window.location.href.indexOf('localhost') > -1) ?
      'localhost' :
      ".asvabprogram.com";

  /**
   * Used within the more about me dialog; when mapping checkboxes with text values
   * this will need to be updated when the checkbox values change
   */
  checkBoxMap = new Map([
    ["communication", "Communication"],
    ["collaboration", "Collaboration"],
    ["adaptability", "Adaptability"],
    ["timeManagement", "Time Management"],
    ["initiative", "Initiative"],
    ["creativity", "Creativity"],
    ["criticalThinking", "Critical Thinking/Problem Solving"],
    ["persuasion", "Persuasion"],
    ["digitalLiteracy", "Digital Literacy"],
    ["workingOutside", "Working outside"],
    ["workingWithHands", "Working with my hands"],
    ["workingWithHands2", "Working with elderly"],
    ["workingWithChildren", "Working with children"],
    ["dislikedItem", "Working in closed space"],
    ["dislikedItem2", "Working at high altitude"],
    ["managingPeople", "Managing people"],
    ["dislikedItem3", "Working in noisy environment"],
    ["dislikedItem4", "Working with computers"],
  ]);

  /**
   * 
   */
  futurePlanValueMap = new Map([
    ["communication", "Communication"],
    ["collaboration", "Collaboration"],
    ["adaptability", "Adaptability"],
    ["timeManagement", "Time Management"],
    ["initiative", "Initiative"],
    ["creativity", "Creativity"]
  ]);


  constructor(
    private _dialog: MatDialog,
    private _http: HttpClient,
    private _cookie: CookieService,
    private _ga: GoogleAnalyticsService,
    private _config: ConfigService,
    private _sanitizer: DomSanitizer,
    private _router: Router,
    private _viewPortScroller: ViewportScroller,
    private _meta: Meta,
    private _titleTag: Title,
    @Inject(LOCALE_ID) private locale: string
  ) { }

  isIE() {
    return navigator.userAgent.indexOf("MSIE ") > -1 || navigator.userAgent.indexOf("Trident/") > -1;
  }

  /**
   * return the canonical url as AsvabProgram.com in https
   **/
  getCanonicalUrl = function () {
    return 'https://www.asvabprogram.com' + window.location.pathname;
  }

  /**
   * returns the https version of the url if the url is unsecured  * 
   */
  getSecuredUrl = function (url) {
    // check if the url has value AND if the url is unsecured
    if (url != undefined && url.indexOf('https:') === -1) {
      url = 'https:' + url.substring(5);
    }
    // return url
    return url;
  }

  /**
   * shortens text based on given. if the text is greater than the given size, then it returns a substring and appends '...' at the end. else returns the full text
   **/
  shortenText = function (text, size) {
    if (text.length > size) {
      return text.substring(0, size) + "...";
    }
    return text;
  }

  modal(title, message) {
    const data = {
      title,
      message,
    };

    const dialogRef = this._dialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: 500,
    });
  }

  showVideoDialog(videoName) {
    this._dialog.open(PopupPlayerComponent, {
      data: { videoId: videoName }
    });
  }

  scrollToAnchor(elementId) {
    this._viewPortScroller.scrollToAnchor(elementId);
  }

  showWorkValueModal(valueName) {
    var description = null;
    switch (valueName) {
      case 'Achievement':
        description = 'Workers who score high on Achievement are results-oriented. These workers often pursue jobs where employees are able to apply their strengths and abilities. This gives the employee a sense of accomplishment.';
        break;
      case 'Independence':
        description = 'Workers who score high on Independence value the ability to approach work activities with creativity. These workers want to make their own decisions and plan their work with little supervision from a manager.';
        break;
      case 'Recognition':
        description = 'Workers who score high on Recognition pursue jobs with opportunities for advancement and leadership responsibilities that allow them to give direction and instruction to others. These workers are often considered prestigious by their peers and others in their organization and receive recognition for the work they contribute.';
        break;
      case 'Relationships':
        description = 'Workers who score high on Relationships prefer jobs that provide services to others and working with co-workers in a friendly, non-competitive environment. Workers in these jobs value getting along well with others and do not like to be pressured to do things that go against their morals or sense of what is right and wrong.';
        break;
      case 'Support':
        description = "Workers who score high on Support appreciate when their company's leadership stands behind and supports their employees. People in these types of jobs like to feel like they are being treated fairly by the company and have supervisors who spend time and effort training their workers to perform well.";
        break;
      case 'Working Conditions':
        description = 'Workers who score high on Working Conditions value job security and pleasant working conditions. These workers enjoy being busy and want to be paid well for the work they do. They enjoy developing ways of doing things with little or no supervision and depend on themselves to get the work done. These workers pursue steady employment that offers something different to do on a daily basis.';
        break;
    }

    const data = {
      'title': valueName,
      'message': description,
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: data,
      //hasBackdrop: true,
      //disableClose: true,
      maxWidth: '800px'
    });
  }

  public getByName(name) {
    return this._http.get(this._config.getFullUrl(`db-info/get-by-name/${name}/`, null));
  }

  urlEncode(url, params) {
    const encodedParams = encodeURI(params);
    return url + encodedParams;
  }

  urlDecode(data) {
    const decodedParams = decodeURI(data);
    return decodedParams;
  }

  /* 
  *    Social Sharing Functions
  */

  displaySocialShare(app, params, additionalInfo) {
    this.trackSocialShare(app, additionalInfo);
    const url = this.encodeSocialShareUrl(app, params);
    if (url) {
      window.open(url, 'popup', 'width=600,height=600');
    }
    return false;
  }

  encodeSocialShareUrl(app, params) {
    let url;
    switch (app) {
      case 'facebook':
        url = this.facebookUrl;
        break;
      case 'twitter':
        url = this.twitterUrl;
        break;
      case 'linkedin':
        url = this.linkedInUrl;
        break;
      default:
        this.modal('Social Share Error', 'Social Share for ' + app + ' is not supported.');
        return;
    }
    const specializedParams = this.parmsByAppCreation(app, params);
    return this.urlEncode(url, specializedParams);
  }

  parmsByAppCreation(app, params) {
    let appParams: String;
    switch (app) {
      case 'facebook':
        appParams = 'u=' + params.url;
        break;
      case 'twitter':
        appParams = 'url=' + params.url + '&text=' + params.shortDescription + '&via=' + params.via;
        break;
      case 'linkedin':
        appParams = 'url=' + params.url;
        break;
    }
    return appParams;
  }

  trackSocialShare(app, additionalInfo) {
    this._ga.trackSocialShare(app, additionalInfo);
  }

  /* 
  *    Cookie processing
  */

  createCookie(data) {
    this._cookie.set('globals', JSON.stringify(data), undefined, '/');
  }

  getCookie() {
    return this._cookie.check('globals') ? JSON.parse(this._cookie.get('globals')) : null;
  }

  deleteCookie() {
    this._cookie.delete('globals', '/', this.domain);
  }

  createCsrfCookie(data) {
    this._cookie.set('XSRF-TOKEN', String(data), undefined, '/', this.domain);
  }

  getCsrfCookie() {
    return this._cookie.check('XSRF-TOKEN') ? this._cookie.get('XSRF-TOKEN') : null;
  }

  deleteCsrfCookie() {
    this._cookie.delete('XSRF-TOKEN', '/', this.domain);
  }

  getAwsXsrfCookie() {
    return this._cookie.check('X-XSRF-TOKEN') ? this._cookie.get('X-XSRF-TOKEN') : null;
  }

  createAwsXsrfCookie(data) {
    this._cookie.set('X-XSRF-TOKEN', String(data), undefined, '/', this.domain);
  }

  //found from: https://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device
  //good article here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
  mobileOrTablet() {
    if (/Mobi|Android/i.test(navigator.userAgent)) {
      return true;
    } else {
      return false;
    }
  }

  /* 
  *    Encoding and Decoding of Data
  */

  convertUTCDateToLocalDate(date): any {
    var newDate = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;
  }

  /**
   * Converts date and returns in format: "MM/dd/yyyy at hh:mm a"
   * @param date 
   * @returns 
   */
  convertDateToLocalDate(date, isFromServer: boolean = true): any {

    if (!isFromServer) {
      let appendDate = date;
      return formatDate(new Date(appendDate.toLocaleString()), "MM/dd/yyyy 'at' hh:mm a", this.locale, timeZone);
    } else {
      return formatDate(new Date(this.convertServerUTCToUniversalUTC(date)), "MM/dd/yyyy 'at' hh:mm a", this.locale, timeZone);
    }

  }

  convertServerUTCToUniversalUTC(dateString: string) {
    if (dateString) {
      return dateString.replace(' ', 'T') + 'Z';
    } else {
      return "";
    }
  }

  capitalizeFirstLetter2(string) {
    let returnString = string.toLowerCase();
    return returnString[0].toUpperCase() + string.slice(1);
  }

  capitalizeFirstLetter = function (words) {
    var regExp = /[a-z]/i;
    return words
      .toLowerCase()
      .split(' ')
      .map(function (w) {
        var firstCharIndex = (w.match(regExp) || []).index;
        return firstCharIndex === undefined
          ? w
          : w.substring(0, firstCharIndex) + w[firstCharIndex].toUpperCase() + w.slice(firstCharIndex + 1);
      })
      .join(' ');
  }

  encodeData(value) {
    return Base64.encode(value);
  }

  decodeData(value) {
    return Base64.decode(value);
  }

  validateEmail(elementValue) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
  }

  clearSessionStorage() {
    window.sessionStorage.removeItem("interestCodeOne");
    window.sessionStorage.removeItem("interestCodeTwo");
    window.sessionStorage.removeItem("interestCodeThree");
    window.sessionStorage.removeItem("mediaCenterList");
    window.sessionStorage.removeItem("noteList");
    window.sessionStorage.removeItem("schoolNoteList");
    window.sessionStorage.removeItem("careerClusterNoteList");
    window.sessionStorage.removeItem("numberTestTaken");
    window.sessionStorage.removeItem("favoriteList");
    window.sessionStorage.removeItem("careerClusterFavoritesList");
    window.sessionStorage.removeItem("schoolFavoritesList");
    window.sessionStorage.removeItem("isPortfolioStarted");
    window.sessionStorage.removeItem("combinedTiedScoreObject");
    window.sessionStorage.removeItem("genderTiedScoreObject");
    window.sessionStorage.removeItem("topCombinedInterestCodes");
    window.sessionStorage.removeItem("topGenderInterestCodes");
    window.sessionStorage.removeItem("option-ready-photos");
    window.sessionStorage.removeItem("sV");
    window.sessionStorage.removeItem("sM");
    window.sessionStorage.removeItem("sS");
    window.sessionStorage.removeItem("sA");
    window.sessionStorage.removeItem("FYIQuestions");
    window.sessionStorage.removeItem("gender");
    window.sessionStorage.removeItem("scoreSummary");
    window.sessionStorage.removeItem("asvabScore");
    window.sessionStorage.removeItem("mBooks");
    window.sessionStorage.removeItem("sBooks");
    window.sessionStorage.removeItem("manualScores");
    window.sessionStorage.removeItem("vBooks");
    window.sessionStorage.removeItem("scoreChoice");
    window.sessionStorage.removeItem("mediaCenterList");
    window.sessionStorage.removeItem("currentSearchSelections");
    window.sessionStorage.removeItem("workValueTopResults");
  };

  /**
 * this is used in the more about me dialog;
 * loop through the properties of the object passed
 * if the property has a value add it to the return 
 * @param moreAboutMe 
 */
  getValuesFromMoreAboutMe(moreAboutMe: any): string[] {

    let returnArr: string[] = [];

    for (var key of Object.keys(moreAboutMe)) {
      if (moreAboutMe[key]) {
        if (moreAboutMe[key] !== true) {
          returnArr.push(moreAboutMe[key]);
        } else {
          if (this.checkBoxMap.get(key)) {
            returnArr.push(this.checkBoxMap.get(key));
          }
        }
      }
    }

    return returnArr;
  }

  isJson(str): boolean {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  parseCms(cmsHtml, parsedHtml) {
    if (typeof cmsHtml === "string") {
      const reg = /<\s*div\s*data-type="section-break"[^>]*><\s*hr[^>]*><\s*\/div[^>]*>/;
      cmsHtml.split(reg).forEach(section => {
        if (section) {
          parsedHtml.push(this._sanitizer.bypassSecurityTrustHtml(section));
        }
      })
    }
  }

  /**
   * 
   * @param pathway json object to parse
   * @returns 
   */
  parsePathway(pathway: string) {
    let returnObj: any;
    try {
      returnObj = JSON.parse(pathway);
    } catch (e) {
      returnObj = null;
    }
    return returnObj;
  }

  /**
   * 
   * @param params 	certificate, 	military, technicalSchool, workBasedLearning, associatesDegree, 
   * vocationalTraining, bachelorsDegree, careerTechnicalEducation, advanceDegree, gapYear   	
   */
  containsPathways(pathwayObj: any, ...params: string[]) {
    for (var i = 0; i < params.length; i++) {
      for (let key of Object.keys(pathwayObj)) {
        if (key == params[i]) {
          if (pathwayObj[key]) return true;
        }
      }
    }

    return false;
  }

  redirectTo(uri: string) {
    this._router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this._router.navigate([uri]));
  }

  /**
   * converts date format MM/dd/yyyy to yyyy-MM-dd
   * @param dateString 
   */
  convertDateFromServer(dateString): string {
    var p = dateString.split(/\D/g);
    return [p[2], p[0], p[1]].join("-");
  }

  /**
   * converts date format dd/MM/yyyy to yyyy-MM-dd
   * @param dateString 
   */
  convertDateToServer(dateString): string {
    var date = new Date(dateString);
    if (!isNaN(date.getTime())) {
      // Months use 0 index.
      var month = date.getMonth() + 1;
      // return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
      return date.getDate() + '/' + month + '/' + date.getFullYear();

    }
  }

  convertDateFromServerAfterUpdate(dateString): string {
    var p = dateString.split(/\D/g);
    return [p[2], p[1], p[0]].join("-");
  }

  showSampleTestModal() {
    const dialogRef2 = this._dialog.open(AsvabSampleTestDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      height: '95%',
      autoFocus: true
    });
  }

  showSampleIcatTestModal() {
    const dialogRef2 = this._dialog.open(AsvabIcatSampleTestDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      height: '95%',
      autoFocus: true
    });
  }

  // Create our number formatter.
  convertToUSDCurrency(value) {
    let valueToConvert: number = 0;

    if (value) {
      valueToConvert = +value;
    }

    var formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',

      // These options are needed to round to whole numbers if that's what you want.
      //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
      //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });

    return formatter.format(valueToConvert);
  }

  stripHtmlTags(value) {
    if ((value === null) || (value === '')) {
      return '';
    }

    // Regular expression to identify HTML tags in  
    // the input string. Replacing the identified  
    // HTML tag with a null string. 
    return value.toString().replace(/(<([^>]+)>)/ig, '');
  }

  scrollToTop() {
    this._viewPortScroller.scrollToPosition([0, 0]);
  }

  getClosestDate(days: any[], dateToCompare: Date) {
    var testDate = new Date(dateToCompare);

    var bestPrevDate = days.length;
    var bestNextDate = days.length - 1;

    var max_date_value = Math.abs((new Date(0, 0, 0)).valueOf());

    var bestNextDiff = -max_date_value;

    var currDiff = 0;
    var i = 0;

    for (i = 0; i < days.length - 1; i++) {

      //get difference in time then convert to days
      currDiff = (testDate.getTime() - new Date(days[i].date).getTime()) / (1000 * 3600 * 24);

      if (currDiff < 0 && currDiff > bestNextDiff) {
        // If currDiff is negative, then testDate is more in the past than days[i].
        // This means, that from testDate's point of view, days[i] is in the future
        // and thus by a candidate for the next date.
        bestNextDate = i;
        bestNextDiff = currDiff;
      }

    }

    return days[bestNextDate];

  }

  getActivityStatusCssClass(activity) {

    if (!activity) return "not_started";

    if (activity.submissions && activity.submissions.length > 0) {

      let totalSubmissions = activity.submissions.length;
      let totalCompleted = activity.submissions.filter(x => x.statusId == 5).length;
      let totalSubmitted = activity.submissions.filter(x => x.statusId == 2).length;
      let totalUnSubmitted = activity.submissions.filter(x => x.statusId == 3).length;

      if (totalSubmissions == totalCompleted) return "checkmark";

      if (totalUnSubmitted > 0) {
        return "in_progress";
      }

      return "ready_for_review";
      // if (totalSubmissions == totalSubmitted) return "ready_for_review";

      // return "in_progress";

    } else {
      return "not_started";
    }

  }

  getUserOccupationStatusCssClass(occupationPlan, mentorLinkId) {

    let cssClass = '';

    if (!occupationPlan) cssClass = "not_started";

    if (occupationPlan.statusId == 1) cssClass = "in_progress";

    //if mentor is not in the mentors array then return in progress
    if(occupationPlan.mentors && occupationPlan.mentors.length > 0){
      if(!occupationPlan.mentors.some(x => x.mentorId == mentorLinkId)){
        cssClass = "in_progress";
      }
    }

    if (occupationPlan.statusId == 2) cssClass = "ready_for_review";

    if (occupationPlan.statusId == 5) cssClass = "checkmark";

    // if (occupationPlan.combinedList
    //   && occupationPlan.combinedList.length === 0
    //   && !this.mobileOrTablet()) {
    //   cssClass = cssClass.concat(' ml_20');
    // }

    return cssClass;
  }

  setSocialMeta(seoTitle, metaDescription) {
    this._titleTag.setTitle(seoTitle);
    this._meta.updateTag({name: 'description', content: metaDescription});

    this._meta.updateTag({property: 'og:title', content: seoTitle});
    this._meta.updateTag({property: 'og:description', content: metaDescription});

    this._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
    this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
  }
}




// Base64 encoding service used by AuthenticationService
var Base64 = {

  keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

  encode: function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output = output + this.keyStr.charAt(enc1) + this.keyStr.charAt(enc2) + this.keyStr.charAt(enc3) + this.keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);

    return output;
  },

  decode: function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
      window.alert("There were invalid base64 characters in the input text.\n" + "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" + "Expect errors in decoding.");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
      enc1 = this.keyStr.indexOf(input.charAt(i++));
      enc2 = this.keyStr.indexOf(input.charAt(i++));
      enc3 = this.keyStr.indexOf(input.charAt(i++));
      enc4 = this.keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
        output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
        output = output + String.fromCharCode(chr3);
      }

      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);

    return output;
  }




};
