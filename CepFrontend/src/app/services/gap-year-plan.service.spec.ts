import { TestBed } from '@angular/core/testing';

import { GapYearPlanService } from './gap-year-plan.service';

describe('GapYearPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GapYearPlanService = TestBed.get(GapYearPlanService);
    expect(service).toBeTruthy();
  });
});
