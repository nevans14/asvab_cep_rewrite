import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpHelperService } from './http-helper.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class IpedsService {

  constructor(private _configService: ConfigService,
    private _httpHelper: HttpHelperService,
    private _http: HttpClient,
    private _utilityService: UtilityService) { }


  /**
   * 
   * @param data array of abbreviations ex: {'M', 'N'}
   */
  // async getServiceColleges(data) {
  //   const fullUrl = this._configService.getAwsFullUrl('/api/ipeds/get-service-colleges');
  //   return await this._httpHelper.httpHelper('post', fullUrl, null, data, true);
  // }

  getServiceColleges(data) {
    return this._http.post(this._configService.getAwsFullUrl('/api/ipeds/get-service-colleges'), data,
      { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(this._utilityService.getAwsXsrfCookie())) }).toPromise();
  }


}
