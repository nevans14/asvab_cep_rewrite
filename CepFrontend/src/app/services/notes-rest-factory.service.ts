import { Injectable } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable( {
    providedIn: 'root'
} )
export class NotesRestFactoryService {

    restURL: any;

    constructor( private _http: HttpClient,
        _config: ConfigService,
        private _httpHelper: HttpHelperService ) {
        this.restURL = _config.getRestUrl() + '/';
    }

    // getNotesfunction() {
    //     return this._http.get( this.restURL + 'favorite/getFavoriteOccupation/' ).toPromise();
    // }

    // insertFavoriteOccupation = function( favoriteObject ) {
    //     return this._http.post( this.restURL + 'favorite/insertFavoriteOccupation', favoriteObject ).toPromise();
    // }

    // deleteFavoriteOccupation = function( id ) {
    //     return this._http.delete( this.restURL + 'favorite/deleteFavoriteOccupation/' + id + '/' ).toPromise();
    // }

    // getFavoriteCareerCluster = function() {
    //     return this._http.get( this.restURL + 'favorite/getFavoriteCareerCluster/' ).toPromise();
    // }

    // insertFavoriteCareerCluster = function( favoriteObject ) {
    //     return this._http.post( this.restURL + 'favorite/insertFavoriteCareerCluster', favoriteObject ).toPromise();
    // }

    // deleteFavoriteCareerCluster = function( id ) {
    //     return this._http.delete( this.restURL + 'favorite/deleteFavoriteCareerCluster/' + id + '/' ).toPromise();
    // }

    // getFavoriteSchool = function() {
    //     return this._http.get( this.restURL + 'favorite/getFavoriteSchool/' ).toPromise();
    // }

    // insertFavoriteSchool = function( favoriteObject ) {
    //     return this._http.post( this.restURL + 'favorite/insertFavoriteSchool', favoriteObject ).toPromise();
    // }

    // deleteFavoriteSchool = function( id ) {
    //     return this._http.delete( this.restURL + 'favorite/deleteFavoriteSchool/' + id + '/' ).toPromise();
    // }




    getNotes() {
      return this._httpHelper.httpHelper('get', `notes/getNotes`, null, null);
    }
  
    insertNote(noteObject) {
      return this._httpHelper.httpHelper('post', `notes/insertNote`, null, noteObject);
    }
  
    updateNote(noteObject) {
      return this._httpHelper.httpHelper('put', `notes/updateNote`, null, noteObject);
    }
    
    deleteNote(noteId) {
      return this._httpHelper.httpHelper('delete', `notes/deleteNote/${noteId}/`, null, null);
    }

    getOccupationNotes(noteObject) {
      return this._http.get( this.restURL + 'notes/getOccupationNotes/' + noteObject.socId + '/').toPromise();
    }
    
    getCareerClusterNotes() {
      return this._http.get( this.restURL + 'notes/getCareerClusterNotes' ).toPromise();
    }
  
    insertCareerClusterNote(noteObject) {
      return this._httpHelper.httpHelper('post', `notes/insertCareerClusterNote`, null, noteObject);
    }
  
    updateCareerClusterNote(noteObject) {
      return this._httpHelper.httpHelper('put', `notes/updateCareerClusterNote`, null, noteObject);
    }
    
    deleteCareerClusterNote(ccNoteId) {
      return this._httpHelper.httpHelper('delete', `notes/deleteCareerClusterNote/${ccNoteId}/`, null, null);
    }
  
    getCareerClusterNote(noteObject) {
      return this._http.get( this.restURL + 'notes/getCareerClusterNote'  + noteObject.ccId + '/' ).toPromise();
    }
    
    getSchoolNotes = function() {
      return this._httpHelper.httpHelper('get', `notes/getSchoolNotes`, null, null);
    }
  
    insertSchoolNote = function(noteObject) {
      return this._httpHelper.httpHelper('post', `notes/insertSchoolNote`, null, noteObject);
    }
  
    updateSchoolNote = function(noteObject) {
      return this._httpHelper.httpHelper('put', `notes/updateSchoolNote`, null, noteObject);
    }
    
    deleteSchoolNote = function(schoolNoteId) {
      return this._httpHelper.httpHelper('delete', `notes/deleteSchoolNote/${schoolNoteId}/`, null, null);
    }
  
    getSchoolNote = function(noteObject) {
      return this._httpHelper.httpHelper('get', `notes/getSchoolNotes/${noteObject.unitId}/`, null, null);
    }
}
