import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/services/authentication.service';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UtilityService } from 'app/services/utility.service';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    restApiUrl: any;
    ssoURL: any; // TODO: hook up SSO

    //views, by default, show Email login
    public showAccessCodeLogin = false;
    public showEmailLogin = true;
    public showPasswordReset = false;
    // for email/password login
    public email;
    public password;
    // for resetting password
    public resetPasswordEmail;
    // global message
    public message;

    constructor(private _router: Router,
        public authenticationService: AuthenticationService,
        private _config: ConfigService,
        private _httpHelper: HttpHelperService,
        private _utilityService: UtilityService,
    ) {

        this.restApiUrl = this._httpHelper.getApiUrl();
        this.restApiUrl = this.restApiUrl[this.restApiUrl.length - 1] === '/' ? this.restApiUrl : this.restApiUrl + '/';
        this.ssoURL = this._httpHelper.getSsoUrl();

        this.getXSRFTokens();
    }


    // Mentor registration search school
    public searchSchool(schoolSearchObject) {
        const fullUrl = this._config.getAwsFullUrl(`/api/schools/search`);
        return this._httpHelper.httpHelper('post', fullUrl, null, schoolSearchObject, true);
    }

    readonly LOGIN_BASE_URL = 'applicationAccess/';

    // endpoint enum
    public serviceEndpoint = {
        'AccessCode': 'loginAccessCode',
        'Email': 'loginEmail',
        'Reset': 'passwordResetRequest',
        'CheckTeacherCode': 'testTeacherAccessCode'
    };

    public toggleService = {
        'AccessCode': 1,
        'Email': 2,
        'Reset': 3
    };


    getXSRFTokens() {
        let csrfToken = this._utilityService.getCsrfCookie();
        if (!csrfToken) {
            this._httpHelper.httpHelper('GET', 'get-token/', null, null)
                .then((csrfToken: any) => {
                    // Backend will set the XSRF cookie in browser

                    //if ('token' in csrfToken) {
                    //    this._utilityService.createCsrfCookie(csrfToken['token'])
                    //} else {
                    //    throw new Error('Returned CSRF token missing token property.');
                    //}
                })
                .catch((err) => {
                    console.error("Token initialization failed: " + err)
                })
        }

        let awsXsrfCookie = this._utilityService.getAwsXsrfCookie();
        if (!awsXsrfCookie) {
            const fullUrl = this._config.getAwsFullUrl(`/api/token`);
            this._httpHelper.httpHelper('GET', fullUrl, null, null, true).then((success: any) => {
                if (success.data && success.data.token) {
                    this._utilityService.createAwsXsrfCookie(success.data.token);
                }
            })
        }
    }

    /**
     * access code login
     */
    async accessCodeLogin(accessCode, fromDialog: boolean = false) {

        // set up data
        let data = {
            email: '',
            accessCode: accessCode,
            password: '',
            resetKey: ''
        };

        try {

            const teacherCodeAttempt: any = await this.checkCode(data, this.serviceEndpoint.CheckTeacherCode);

            //teacher code was entered
            if (teacherCodeAttempt.statusNumber === 0) {
                return Promise.resolve("TEACHER_CODE");
            }

            const accessCodeAttempt: any = await this.checkCode(data, this.serviceEndpoint.AccessCode);

            if (accessCodeAttempt.statusNumber === 0) {

                this.authenticationService.SetCredentials(data.email, 'null', data.accessCode, accessCodeAttempt.user_id, accessCodeAttempt.role,
                    accessCodeAttempt.schoolCode,
                    accessCodeAttempt.schoolName,
                    accessCodeAttempt.city,
                    accessCodeAttempt.state,
                    accessCodeAttempt.zipCode,
                    accessCodeAttempt.mepsId,
                    accessCodeAttempt.mepsName,
                    false);

                return Promise.resolve("ACCESS_CODE");
            } else {
                return Promise.resolve(accessCodeAttempt.status) //returns error message 
            }

        } catch (e) {

            console.error(e);
            return Promise.resolve('Request failed.');

        }

    }

    checkCode(formData, codeUrl) {
        let endPoint = this.LOGIN_BASE_URL + codeUrl;

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', endPoint, null, formData)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * email/password login
     */
    async emailLogin(fromDialog: boolean = false) {

        try {
            // set up data
            const data = {
                email: this.email,
                accessCode: '',
                password: this.password,
                resetKey: ''
            };

            // reset password
            this.password = '';

            // set up url
            let url = this.LOGIN_BASE_URL + this.serviceEndpoint.Email;

            //attempt login with the provided data and url
            const loginAttempt = await this.loginAttempt(data, url);

            //hide the error message after 5 seconds;  
            if (!loginAttempt) {
                return Promise.resolve(this.message); //returns error message 

            } else {
                return Promise.resolve("SUCCESS");
            }
        } catch (e) {
            console.error(e);
            return Promise.resolve('Request failed.');
        }

    }

    loginAttempt(data, url) {
        const loginAttempt = this._httpHelper.httpHelper('POST', url, null, data)
            .then((response) => {
                let res: any = response;
                if (res.statusNumber !== 0) {
                    //set error message label
                    this.message = res.status;
                    // return this.message;
                    return Promise.resolve(false);
                } else {

                    //log the user in
                    this.authenticationService.SetCredentials(data.email, 'null', res.user_id, res.role,
                        data.accessCode,
                        res.schoolCode,
                        res.schoolName,
                        res.city,
                        res.state,
                        res.zipCode,
                        res.mepsId,
                        res.mepsName,
                        true);
                }

                return Promise.resolve(true);
            })
            .catch((err) => {
                console.error(err);
                this.message = err;
                return Promise.reject(false);
            }
            );
        return loginAttempt;
    }

    /**
     * reset password
     */
    async sendResetCode(fromDialog: boolean = false) {
        // set up data
        let data = {
            email: this.resetPasswordEmail,
            accessCode: '',
            password: '',
            resetKey: ''
        };
        // set up url
        let url = this.LOGIN_BASE_URL + this.serviceEndpoint.Reset;

        try {
            const resetCode: any = await this.resetCode(data, url);

            //teacher code was entered
            if (resetCode.statusNumber === 0) {
                return Promise.resolve('SUCCESS');
            } else {
                return Promise.resolve(resetCode.status);
            }

        } catch (e) {
            console.error(e);
            return Promise.resolve('Request failed.');
        }

    }

    resetCode(formData, codeUrl) {
        let endPoint = codeUrl;

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', endPoint, null, formData)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    logout() {
        this.authenticationService.ClearCredentials();
        // TODO: UserFactory.clearSessionStorage();

        //$window.location.href = ssoUrl + 'perform_logout';

        /*var modalOptions = {
            headerText : 'Logged Out',
            bodyText : 'You have successfully logged out.'
        };

        modalService.showModal({
            size : 'sm'
        }, modalOptions);*/
    }

    redirectAfterLogin() {

        const globalValues = this._utilityService.getCookie();
        let user;

        if (globalValues) {
            user = globalValues;
        }

        if (!user) {
            this._router.navigate(['/']);
        }

        let userRole = user.currentUser.role;

        if (userRole) {
            if (userRole.toUpperCase() == "S") {
                this._router.navigate(['/learn-about-yourself']);
            } else {
                this._router.navigate(['/mentor-dashboard']);
            }
        } else {
            this._router.navigate(['/learn-about-yourself']);
        }

    }
}
