import { TestBed } from '@angular/core/testing';

import { UserOccupationService } from './user-occupation.service';

describe('UserOccupationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserOccupationService = TestBed.get(UserOccupationService);
    expect(service).toBeTruthy();
  });
});
