import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class FyiRestFactoryService {

  constructor(private _http: HttpClient,
    private _config: ConfigService,
    private _httpHelper: HttpHelperService) {
  }

  getStudentScoreResults() :any {
		return this._httpHelper.httpHelper('GET','testScore/get-student-score-results', null, null);
	};

  insertTiedSelection(tiedObject) {
    return this._httpHelper.httpHelper('POST','fyi/insert-tied-selection', null, tiedObject);
  };

  getNumberTestTaken(){
		return this._httpHelper.httpHelper('GET','fyi/NumberTestTaken', null, null);
	};

	getCombinedAndGenderScores() {
		return this._httpHelper.httpHelper('GET','fyi/CombinedAndGenderScores', null, null);
	};
    
	postManualAnswers(fyiObject) {
    const fullUrl = this._config.getAwsFullUrl('/api/fyi/save-manual-responses');
    return this._httpHelper.httpHelper('POST', fullUrl, null, fyiObject, true, 'aws');
	}

	postAnswers(fyiObject){
    const fullUrl = this._config.getAwsFullUrl('/api/fyi/save-responses');
    return this._httpHelper.httpHelper('POST', fullUrl, null, fyiObject, true, 'aws');
  };
  
}
