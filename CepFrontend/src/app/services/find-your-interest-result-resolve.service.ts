import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpHelperService } from './http-helper.service';
import { FindYourInterestService } from './find-your-interest.service';
import { ConfigService } from './config.service';
import { Router } from '@angular/router';
import { FyiRestFactoryService } from './fyi-rest-factory-service';

@Injectable({
    providedIn: 'root'
})
export class FindYourInterestResultResolveService implements Resolve<any> {

    fyiObject: any;

    constructor(private _httpHelper: HttpHelperService,
        private _fyiRestFactoryService: FyiRestFactoryService,
        private FYIScoreService: FindYourInterestService,
        private _configService: ConfigService,
        private _router: Router
    ) { }

    async resolve() {


        this.fyiObject = this.FYIScoreService.getFyiObject();

        window.sessionStorage.removeItem("FYIQuestions");
        // window.sessionStorage.removeItem("gender");

        // If user is coming from the FYI test page.
        if (this.fyiObject != undefined) {
            /*let defer = $q.defer();
            let deferNumTestTaken = $q.defer();*/
            //let defer = Promise;
            // let deferNumTestTaken;
            // let defer;
            let postAnswersPromise;

            // Set tied top scores to default.
            this.FYIScoreService.setTopGenderInterestCodes(undefined);
            this.FYIScoreService.setTopCombinedInterestCodes(undefined);
            let tiedObject = {
                combineInterestOne: null,
                combineInterestTwo: null,
                combineInterestThree: null,
                genderInterestOne: null,
                genderInterestTwo: null,
                genderInterestThree: null,
            }
            this._fyiRestFactoryService.insertTiedSelection(tiedObject);

            if (!this.fyiObject.responses) {

                postAnswersPromise = await this._fyiRestFactoryService.postManualAnswers(this.fyiObject).then((response: any) => {
                    this.FYIScoreService.setFyiObject(undefined);
                    return response;
                });

            } else {

                postAnswersPromise = await this._fyiRestFactoryService.postAnswers(this.fyiObject).then((response: any) => {
                    this.FYIScoreService.setFyiObject(undefined);
                    return response;
                });

            }


            // Rest call to post user's FYI answers to database.
            // Rest call this returns calculated combined and gender scores.

            let qScores = await this._fyiRestFactoryService.getCombinedAndGenderScores().then((response: any) => {

                var scores = response;
                let isCombinedScoresTied = this.FYIScoreService.isTiedScore(this.FYIScoreService.getSortedCombinedScore(scores), 'combined');

                let gender = window.sessionStorage.getItem("gender");
                let isGenderScoresTied = !gender || gender === 'null' ? false : this.FYIScoreService.isTiedScore(this.FYIScoreService.getSortedGenderScore(scores), 'gender');

                // If tied scores then redirect to tie breaker page.
                if (isCombinedScoresTied || isGenderScoresTied) {

                    let topCombinedScores = this.FYIScoreService.getTopCombinedInterestCodes();
                    let topGenderScores = this.FYIScoreService.getTopGenderInterestCodes();

                    // If either combined or gender score is tied
                    // and user did not break ties then redirect
                    // them to tie break page.
                    if ((isCombinedScoresTied && topCombinedScores == undefined) || (isGenderScoresTied && topGenderScores == undefined && !!gender)) {
                        this._router.navigate(['/find-your-interest-tied']);
                    }
                }
                return response;
            });

            // A promise for number of test taken.
            let numberTestTaken = await this._httpHelper.httpHelper('GET', 'fyi/NumberTestTaken', null, null)
                .then((response: any) => { return response; });

            return Promise.all([qScores, numberTestTaken, postAnswersPromise]);
        } else {

            /*let qScores = $q.defer();
            let tieSelectionPromise = $q.defer();*/
            //let qScores = Promise;
            let tieSelectionPromise;

            // Uses user's data to calculate score and determine if
            // there are ties.
            let qScores = new Promise((resolve, reject) => {

                this._httpHelper.httpHelper('GET', 'fyi/CombinedAndGenderScores', null, null).then((success) => {

                    let scores = success;
                    let isCombinedScoresTied = this.FYIScoreService.isTiedScore(this.FYIScoreService.getSortedCombinedScore(scores), 'combined');

                    let gender = window.sessionStorage.getItem("gender");
                    let isGenderScoresTied = !gender || gender === 'null' ? false : this.FYIScoreService.isTiedScore(this.FYIScoreService.getSortedGenderScore(scores), 'gender');

                    tieSelectionPromise = new Promise((resolve, reject) => {
                        // If tied scores then redirect to tie breaker page.
                        if (isCombinedScoresTied || isGenderScoresTied) {

                            let istiedLinkClicked = this.FYIScoreService.getTiedLinkClicked();
                            let s = undefined;

                            this._httpHelper.httpHelper('GET', 'fyi/get-tied-selection', null, null).then((success: any) => {

                                let data = success;

                                // If tied link is not clicked on the dashboard
                                // page then use database values to recover tie
                                // breaker values.
                                // This enables the user to come back to the
                                // score page without having to be asked to
                                // break a tie again.
                                if (!istiedLinkClicked) {
                                    if (data && data.genderInterestOne && data.genderInterestTwo && data.genderInterestThree) {
                                        let topGenderInterestCodes = [{
                                            interestCd: data.genderInterestOne
                                        }, {
                                            interestCd: data.genderInterestTwo
                                        }, {
                                            interestCd: data.genderInterestThree
                                        }];

                                        // Set the top gender scores to session
                                        // storage.
                                        this.FYIScoreService.setTopGenderInterestCodes(topGenderInterestCodes);
                                    }

                                    if (data && data.combineInterestOne && data.combineInterestTwo && data.combineInterestThree) {
                                        let topCombineInterestCodes = [{
                                            interestCd: data.combineInterestOne
                                        }, {
                                            interestCd: data.combineInterestTwo
                                        }, {
                                            interestCd: data.combineInterestThree
                                        }];

                                        // Set the top combine scores to session
                                        // storage.
                                        this.FYIScoreService.setTopCombinedInterestCodes(topCombineInterestCodes);
                                    }

                                } else {

                                    // User clicked on tie breaker link on the
                                    // dashboard page.
                                    // This enables them to break the tie again.
                                    this.FYIScoreService.setTopGenderInterestCodes(undefined);
                                    this.FYIScoreService.setTopCombinedInterestCodes(undefined);
                                }

                                // Gets top combined and gender score from
                                // session storage.
                                let topCombinedScores = this.FYIScoreService.getTopCombinedInterestCodes();
                                let topGenderScores = this.FYIScoreService.getTopGenderInterestCodes();

                                // Determines if user is redirected to the tie  // FIX
                                // page or not.
                                if (
                                    (isCombinedScoresTied && topCombinedScores == undefined) 
                                    || (isGenderScoresTied && topGenderScores == undefined && window.sessionStorage.getItem("gender"))
                                ) {
                                    this._router.navigate(['/find-your-interest-tied']);
                                } else {
                                    resolve(success);
                                }

                            }, function (error) {
                                reject(error);
                            });

                        } else {
                            resolve(success);
                        }
                    });
                    resolve(success);
                }, function (error) {
                    reject(error)
                });
            });

            // A promise for number of test taken.
            let numberTestTaken = new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('GET', 'fyi/NumberTestTaken', null, null).then((success) => {
                    resolve(success);
                }, function (error) {
                    reject(error);
                });
            });

            // Proceeds if all promises are resolved.
            return Promise.all([qScores, numberTestTaken, tieSelectionPromise]);

        }
    }
}