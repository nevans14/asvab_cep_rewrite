import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class OccufindOccupationSearchService {

  constructor(
    private cookie: CookieService,
  ) { }

  userSearchCookie = this.cookie.check('globals') && JSON.parse(this.cookie.get('globals')).globals && JSON.parse(this.cookie.get('globals')).globals.userSearch ? JSON.parse(this.cookie.get('globals')).globals.userSearch : undefined;
  
  userSearch : any = {
    interestCodeOne : this.userSearchCookie && this.userSearchCookie.interestCodeOne ? this.userSearchCookie.interestCodeOne : undefined,
    interestCodeTwo : this.userSearchCookie && this.userSearchCookie.interestCodeTwo ? this.userSearchCookie.interestCodeTwo : undefined,
    interestCodeThree : this.userSearchCookie && this.userSearchCookie.interestCodeThree ? this.userSearchCookie.interestCodeThree : undefined,
    brightOccupationFlag : this.userSearchCookie && this.userSearchCookie.brightOccupationFlag ? this.userSearchCookie.brightOccupationFlag : false,
    greenOccupationFlag : this.userSearchCookie && this.userSearchCookie.greenOccupationFlag ? this.userSearchCookie.greenOccupationFlag : false,
    stemOccupationFlag : this.userSearchCookie && this.userSearchCookie.stemOccupationFlag ? this.userSearchCookie.stemOccupationFlag : false,
    hotOccupationFlag : this.userSearchCookie && this.userSearchCookie.hotOccupationFlag ? this.userSearchCookie.hotOccupationFlag : false,
    userSearchString : this.userSearchCookie && this.userSearchCookie.userSearchString ? this.userSearchCookie.userSearchString : undefined
  };
  skillSelected : undefined;
  selectedOnetSoc : any[] = [];
  selectedOcupationTitle : undefined;
  occupationGreen : false;
  occupationStem : false;
  occupationBright : false;
  occupationHot : false;
  occupationInterestOne : undefined;
  occupationInterestTwo: undefined;
  occupationInterestThree: undefined;
  currentPage : 1;
  relatedCarreers : [];

  getOnetSoc(){
		return this.selectedOnetSoc[this.selectedOnetSoc.length - 1];
	};

	getSelectedTitleOne(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};

	getSelectedTitleTwo(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};
	
	getSelectedTitleThree(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};
}
