import { TestBed } from '@angular/core/testing';

import { FindYourInterestResultResolveService } from './find-your-interest-result-resolve.service';

describe('FindYourInterestResultResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindYourInterestResultResolveService = TestBed.get(FindYourInterestResultResolveService);
    expect(service).toBeTruthy();
  });
});
