import { TestBed } from '@angular/core/testing';

import { SchoolPlanService } from './school-plan.service';

describe('HighSchoolPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolPlanService = TestBed.get(SchoolPlanService);
    expect(service).toBeTruthy();
  });
});
