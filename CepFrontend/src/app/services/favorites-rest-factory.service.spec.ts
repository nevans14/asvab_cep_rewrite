import { TestBed } from '@angular/core/testing';

import { FavoritesRestFactoryService } from './favorites-rest-factory.service';

describe('FavoritesRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FavoritesRestFactoryService = TestBed.get(FavoritesRestFactoryService);
    expect(service).toBeTruthy();
  });
});
