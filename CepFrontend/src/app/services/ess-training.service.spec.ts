import { TestBed } from '@angular/core/testing';

import { EssTrainingService } from './ess-training.service';

describe('EssTrainingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EssTrainingService = TestBed.get(EssTrainingService);
    expect(service).toBeTruthy();
  });
});
