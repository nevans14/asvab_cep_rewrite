import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'app/services/config.service';
@Injectable({
  providedIn: 'root'
})
export class DbInfoService {
  restURL: any;

  constructor( private _http: HttpClient,
    _config: ConfigService
    ) {
      this.restURL = _config.getRestUrl() + '/';
    }

    getByName = function( name ) {
      return this._http.get(this.restURL + 'db-info/get-by-name/' + name + '/');
    }
}
