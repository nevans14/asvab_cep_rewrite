import { Injectable } from '@angular/core';
import { HttpHelperService } from 'app/services/http-helper.service';
import { ConfigService } from 'app/services/config.service';
@Injectable( {
    providedIn: 'root'
} )
export class MediaCenterService {

    constructor(
        private _httpHelper: HttpHelperService,
        private _config: ConfigService,
    ) { }

    /**
     * Media Center Session Storage
     */
    mediaCenterList = undefined;
    mediaCenterArticle = undefined;
    /**
     * Return favorite list.
     */
    getMediaCenterList = function() {

        var deferred = new Promise(( resolve, reject ) => {

            // if mediaCenterList is undefined then recover data from database
            if ( this.mediaCenterList == undefined ) {

                if ( typeof ( Storage ) !== "undefined" ) {

                    if ( window.sessionStorage.getItem( "mediaCenterList" ) === null || window.sessionStorage.getItem( "mediaCenterList" ) == 'undefined' ) {

                        // recover data using database
                        const fullUrl = this._config.getAwsFullUrl(`/api/media-center/getMediaCenterList/`);
                        this._httpHelper.httpHelper('GET', fullUrl, null, null, true)
                            .then(( res ) => {
                                // success now store in session object
                                window.sessionStorage.setItem( 'mediaCenterList', JSON.stringify( res ) );

                                // sync with in memory property
                                this.mediaCenterList = JSON.parse( window.sessionStorage.getItem( 'mediaCenterList' ) );

                                resolve( this.mediaCenterList );
                            }, ( error ) => {
                                console.error( 'Error' + status );
                                reject( "Error: request returned status" + status );
                            } );

                    } else {

                        // recover data using session storage
                        this.mediaCenterList = JSON.parse( window.sessionStorage.getItem( 'mediaCenterList' ) );
                        resolve( this.mediaCenterList );
                    }

                } else {

                    // no session storage support so get recover data using database
                    const fullUrl = this._config.getAwsFullUrl(`/api/media-center/getMediaCenterList/`);
                    this._httpHelper.httpHelper('GET', fullUrl, null, null, true)
                        .then(( res ) => {
                            // success now store in session object
                            this.mediaCenterList = res;
                            resolve( this.mediaCenterList );
                        }, ( error ) => {
                            console.error( 'Error' + status );
                            reject( "Error: request returned status" + status );
                        } );

                }

            } else {

                // data is available so dont need to use session storage or database
                // to recover
                resolve( this.mediaCenterList );
            }
        });
        return deferred;
    }

    	/**
	 * Media Center by Category
	 */
	getMediaCenterByCategoryId = function(categoryId) {

        var deferred = new Promise(( resolve, reject ) => {
        const fullUrl = this._config.getAwsFullUrl('/api/media-center/getMediaCenterByCategoryId/' + categoryId + '/');
        this._httpHelper.httpHelper('GET', fullUrl, null, null, true)
            .then(( res ) => {
                // success now store in session object
                var mediaCenterByCategory = res;
                resolve( mediaCenterByCategory );
            }, ( error ) => {
                console.error( 'Error' + status );
                reject( "Error: request returned status" + status );
            } );
        });
        return deferred;
    }

    getMediaCenterArticleById = function (mediaId) {
        var deferred = new Promise(( resolve, reject ) => {
        const fullUrl = this._config.getAwsFullUrl('/api/media-center/getMediaCenterArticleById/' + mediaId + '/');
        this._httpHelper.httpHelper('GET', fullUrl, null, null, true)
            .then(( res ) => {
                // success now store in session object
                var mediaCenterArticle = res;
                resolve( mediaCenterArticle );
            }, ( error ) => {
                console.error( 'Error' + status );
                reject( "Error: request returned status" + status );
            } );
        });
        return deferred;

	}

    getMediaCenterArticleByCategoryNameSlug = function (categoryName, slug) {
        var deferred = new Promise(( resolve, reject ) => {
        const fullUrl = this._config.getAwsFullUrl('/api/media-center/articles/' + categoryName + '/' + slug);
        this._httpHelper.httpHelper('GET', fullUrl, null, null, true)
            .then(( res ) => {
                // success now store in session object
                var mediaCenterArticle = res;
                resolve( mediaCenterArticle );
            }, ( error ) => {
                console.error( 'Error' + status );
                reject( "Error: request returned status" + status );
            } );
        });
        return deferred;

	}
    
}
