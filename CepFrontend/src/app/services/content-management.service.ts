import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Title, Meta } from '@angular/platform-browser';
import { UtilityService } from 'app/services/utility.service';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ContentManagementService {

  constructor(
    private _httpHelper: HttpHelperService,
    private _congfigService: ConfigService,
    private _http: HttpClient,
    private _contentManagementService: ContentManagementService,
    private _meta: Meta,
    private _titleTag: Title,
    private _utility: UtilityService,
  ) { }

  public getPageById(pageId): Observable<any> {
    return this._http.get(this._congfigService.getAwsFullUrl(`/api/pages/get-by-page-id/${pageId}/`));
  }

  public getPageByPageName(pageName): Observable<any> {
    return this._http.get(this._congfigService.getAwsFullUrl(`/api/pages/get-by-page-name-system/${pageName}/CEP/`));
  }

  getPageByName(pageName) {
    let pageHtml = [];

    // Remove leading slash before lookup
    if (pageName.indexOf('/') === 0) {
      pageName = pageName.substring(1);
    }
    //console.debug('pageName:', pageName);
    this.getPageByPageName(pageName).subscribe(
      page => {
        if (page && page.pageHtml) {
          this._utility.parseCms(page.pageHtml, pageHtml);
        }

        if (page && page.pageTitle) {
          this._titleTag.setTitle(page.pageTitle);
          this._meta.updateTag({property: 'og:title', content: page.pageTitle});
          this._meta.updateTag({name: 'twitter:title', content:  page.pageTitle});
        }

        if (page && page.pageDescription) {
          this._meta.updateTag({property: 'og:description', content: page.pageDescription});
          this._meta.updateTag({name: 'twitter:description', content: page.pageDescription});
        }
    });
    
    return pageHtml;
  }
}
