import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHelperService } from './http-helper.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { UtilityService } from 'app/services/utility.service';
import { of, Observable, Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
// import { SessionService } from './session.service';
import { MatDialog } from '@angular/material';
import { SessionKeepaliveDialogComponent } from 'app/core/dialogs/session-keepalive-dialog/session-keepalive-dialog.component';
import { FavoritesRestFactoryService } from './favorites-rest-factory.service';

const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: any;
  private userId: String;
  private subject = new Subject<any>();
  private userUpdateSubject = new Subject<any>();
  private controllerName: string = "user";

  public hiddenListObject: any;
  public favoriteCareerClusterLIstObject: any;
  public savedSearchesLIstObject: any;
  public notesLIstObject: any;
  public careerClusterNotesListObject: any;
  public restURL: any;
  sso: boolean = null
  existingTimer: any;
  timerSessionId: any;
  public leftNavCollapsed: boolean;

  public favoritesList: any;

  public static NONSTUDENT_DEMO_CLOSED_HINT = 'NONSTUDENT_DEMO_CLOSED_HINT';
  public static HAS_ASVAB_SCORE = 'HAS_ASVAB_SCORE';
  public static VIEWED_ASVAB_SCORE = 'VIEWED_ASVAB_SCORE';
  public static VIEWED_ASVAB_SCORE_CLOSED_HINT = 'VIEWED_ASVAB_SCORE_CLOSED_HINT';
  public static COMPLETED_FYI = 'COMPLETED_FYI';
  public static COMPLETED_FYI_CLOSED_HINT = 'COMPLETED_FYI_CLOSED_HINT';
  public static VISITED_MILITARY_LINE_SCORE = 'VISITED_MILITARY_LINE_SCORE';
  public static VISITED_MILITARY_LINE_SCORE_CLOSED_HINT = 'VISITED_MILITARY_LINE_SCORE_CLOSED_HINT';
  public static COMPLETED_WORK_VALUE = 'COMPLETED_WORK_VALUE';
  public static COMPLETED_WORK_VALUE_CLOSED_HINT = 'COMPLETED_WORK_VALUE_CLOSED_HINT';
  public static LIKE_DISLIKE_MORE_ABOUT_ME = 'LIKE_DISLIKE_MORE_ABOUT_ME';
  public static LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT = 'LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT';
  public static COMPLETED_OCCUPATION_SEARCH = 'COMPLETED_OCCUPATION_SEARCH';
  public static COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT = 'COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT';
  public static VIEWED_CAREER_DETAIL = 'VIEWED_CAREER_DETAIL';
  public static VIEWED_CAREER_DETAIL_CLOSED_HINT = 'VIEWED_CAREER_DETAIL_CLOSED_HINT';
  public static FAVORITED_OCCUPATION = 'FAVORITED_OCCUPATION';
  public static FAVORITED_OCCUPATION_CLOSED_HINT = 'FAVORITED_OCCUPATION_CLOSED_HINT';
  public static VISITED_RESOURCE = 'VISITED_RESOURCE';
  public static VISITED_RESOURCE_CLOSED_HINT = 'VISITED_RESOURCE_CLOSED_HINT';
  public static FAVORITED_OCCUPATION_ADDITIONAL = 'FAVORITED_OCCUPATION_ADDITIONAL';
  public static FAVORITED_OCCUPATION_ADDITIONAL_CLOSED_HINT = 'FAVORITED_OCCUPATION_ADDITIONAL_CLOSED_HINT';
  public static VISITED_OCCUPATION_DETAIL_LINK = 'VISITED_OCCUPATION_DETAIL_LINK';
  public static VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT = 'VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT';
  public static FAVORITED_COLLEGE = 'FAVORITED_COLLEGE';
  public static FAVORITED_COLLEGE_CLOSED_HINT = 'FAVORITED_COLLEGE_CLOSED_HINT';
  public static TO_CTM_FROM_OCCU_DETAIL = 'TO_CTM_FROM_OCCU_DETAIL';
  public static TO_CTM_FROM_OCCU_DETAIL_CLOSED_HINT = 'TO_CTM_FROM_OCCU_DETAIL_CLOSED_HINT';
  public static COMPARED_OCCUPATIONS = 'COMPARED_OCCUPATIONS';
  public static COMPARED_OCCUPATIONS_CLOSED_HINT = 'COMPARED_OCCUPATIONS_CLOSED_HINT';
  public static VISITED_CAREER_CLUSTER = 'VISITED_CAREER_CLUSTER';
  public static VISITED_CAREER_CLUSTER_CLOSED_HINT = 'VISITED_CAREER_CLUSTER_CLOSED_HINT';
  public static VISITED_NOTES = 'VISITED_NOTES';
  public static VISITED_NOTES_CLOSED_HINT = 'VISITED_NOTES_CLOSED_HINT';
  public static REFINED_OCCUFIND_SEARCH = 'REFINED_OCCUFIND_SEARCH';
  public static REFINED_OCCUFIND_SEARCH_CLOSED_HINT = 'REFINED_OCCUFIND_SEARCH_CLOSED_HINT';
  public static FAVORITED_CC = 'FAVORITED_CC';
  public static FAVORITED_CC_CLOSED_HINT = 'FAVORITED_CC_CLOSED_HINT';
  public static CREATED_PLAN = 'CREATED_PLAN';
  public static CREATED_PLAN_CLOSED_HINT = 'CREATED_PLAN_CLOSED_HINT';
  public static STARTED_PORTFOLIO = 'STARTED_PORTFOLIO';
  public static STARTED_PORTFOLIO_CLOSED_HINT = 'STARTED_PORTFOLIO_CLOSED_HINT';
  public static CONNECTED_TO_MENTOR = 'CONNECTED_TO_MENTOR';
  public static CONNECTED_TO_MENTOR_CLOSED_HINT = 'CONNECTED_TO_MENTOR_CLOSED_HINT';
  public static ADDED_DATE = 'ADDED_DATE';
  public static ADDED_TASK = 'ADDED_TASK';
  public static ADDED_CLASSROOM_ACTIVITY = 'ADDED_CLASSROOM_ACTIVITY';
  public static MENTOR_DASHBOARD_WELCOME = 'MENTOR_DASHBOARD_WELCOME';
  public static MENTOR_DASHBOARD_TEST_DRIVE = 'MENTOR_DASHBOARD_TEST_DRIVE';
  
  // Guided Tour
  public static GUIDED_TIP_CONFIG = 'GUIDED_TIP_CONFIG';
  public static VIEWED_RELATED_CAREERS = 'VIEWED_RELATED_CAREERS';
  public static VIEWED_RELATED_CC = 'VIEWED_RELATED_CC';
  public static RELATED_CAREERS_CC_CLOSED_HINT = 'RELATED_CAREERS_CC_CLOSED_HINT';
  public static FAV_CAREERS_SAME_CC = 'FAV_CAREERS_SAME_CC';
  public static FAV_CAREERS_SAME_CC_CLOSED_HINT = 'FAV_CAREERS_SAME_CC_CLOSED_HINT';
  public static COMPARE_JOB_ATTRIBUTES_CLOSED_HINT = "COMPARE_JOB_ATTRIBUTES_CLOSED_HINT";
  public static FAV_COLLEGE_CLOSED_HINT = "FAV_COLLEGE_CLOSED_HINT";
  public static COMPARE_MORE_THAN_TWO_FAV_CAREERS_CLOSED_HINT = "COMPARE_MORE_THAN_TWO_FAV_CAREERS_CLOSED_HINT";
  public static CREATED_PLAN_SHOW_CALENDAR_CLOSED_HINT = "CREATED_PLAN_SHOW_CALENDAR_CLOSED_HINT";
  public static MILITARY_PLAN_OFFICER_CLOSED_HINT = "MILITARY_PLAN_OFFICER_CLOSED_HINT";
  public static LESS_THAN_TWO_PLANS_CLOSED_HINT = "LESS_THAN_TWO_PLANS_CLOSED_HINT";
  public static VISITED_MILITARY_INFO = "VISITED_MILITARY_INFO";
  public static VISITED_MILITARY_INFO_CLOSED_HINT = "VISITED_MILITARY_INFO_CLOSED_HINT";
  public static MILITARY_PATHWAY_VIEW_CAREERS = "MILITARY_PATHWAY_VIEW_CAREERS";
  public static MILITARY_PATHWAY_VIEW_CAREERS_CLOSED_HINT = "MILITARY_PATHWAY_VIEW_CAREERS_CLOSED_HINT";
  public static DOWNLOAD_ACTIVITY = "DOWNLOAD_ACTIVITY";
  public static DOWNLOAD_ACTIVITY_CLOSED_HINT = "DOWNLOAD_ACTIVITY_CLOSED_HINT";
  public static SUBMIT_PLAN_TO_MENTOR = "SUBMIT_PLAN_TO_MENTOR";
  public static SUBMIT_PLAN_TO_MENTOR_CLOSED_HINT = "SUBMIT_PLAN_TO_MENTOR_CLOSED_HINT";
  
  constructor(
    private _http: HttpClient,
    private _configService: ConfigService,
    private _utilityService: UtilityService,
    private _httpHelper: HttpHelperService,
    private _cookie: CookieService,
    private _authentication: AuthenticationService,
    private _router: Router,
    private _favoritesRestFactory: FavoritesRestFactoryService,
    private _dialog: MatDialog,
  ) {
    this.restURL = _configService.getRestUrl() + '/';
    this.leftNavCollapsed = false;
    this.favoritesList = undefined;
  }

  getLeftNavCollapsed() {
    return this.leftNavCollapsed;
  }

  setLeftNavCollapsed(collapsed) {
    this.leftNavCollapsed = collapsed;
  }

  sendUserUpdate(message) {
    this.userUpdateSubject.next(message);
  }

  getUserUpdates(): Observable<any> {
    return this.userUpdateSubject.asObservable();
  }

  sendMessage(message: string) {
    // NOTE: might need to add a timeout if database is not 
    // getting updated fast enough before calling this
    // setTimeout(() => {
    //   this.subject.next({ text: message });
    // }, 1000);
    this.subject.next({ text: message });
  }

  clearMessages() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  checkUserCompletion(userCompletion, type) {
    return userCompletion.some(item => item.completionType === type && item.value.toLowerCase() === 'true');
  }

  getCompletionByType(userCompletion, type) {
    return userCompletion.find(item => item.completionType === type);
  }

  /**
   * Check for globals cookie and return it if it exists.
   */
  getUser() {
    if (!this.user) {
      const globalValues = this._utilityService.getCookie();
      if (globalValues) {
        this.user = globalValues;
      }
    }
    return this.user;
  }

  isLoggedIn() {
    return this.getUser() ? true : false;
  }

  getUsername() {
    return this._httpHelper.httpHelper('GET', 'user/getUsername/', null, null);
  }

  getUserId() {
    if (!this.userId && this.isLoggedIn()) {
      const userData = this._utilityService.decodeData(this.user);
      this.userId = userData['userId'];
    }
    return this.userId;
  }

  getSSO() {
    if (!this.sso && this.isLoggedIn()) {
      const userData = this._utilityService.decodeData(this.user);
      this.sso = userData.indexOf('SSO') > -1 ? true : false;
    }
    return this.sso;
  }

  logOff(checkSSO = true) {
    this._http.post(this._httpHelper.getFullUrl('auth/logout/', null), {}, { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(this._utilityService.getCsrfCookie())) })
      .toPromise()
      .then(res => {
        this._utilityService.deleteCookie();
        this._utilityService.clearSessionStorage();
        this.user = null;
        this.userId = null;

        this.stopSession();

        this._router.navigate(['']);
      })
      .catch((error) => {
        alert('Error encountered during logout.')
      });
  }

  logOffFromDialog() {
    this._http.post(this._httpHelper.getFullUrl('auth/logout/', null), {}, { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(this._utilityService.getCsrfCookie())) })
      .toPromise()
      .then(res => {
        this._utilityService.deleteCookie();
        this._utilityService.clearSessionStorage();
        this.user = null;
        this.userId = null;
        this._router.navigate(['']);
      })
      .catch((error) => {
        alert('Error encountered during logout.')
      });
  }

  getKeepAlivePing() {
    return this._http.get(this._httpHelper.getFullUrl('user/ping', null), { observe: 'response' });
  }

  /**
   * Occupation Favorites Implementation
   */
  // public favoritesList = undefined;

  /**
   * Add occupation to favorites
   */
  insertFavoriteOccupation(socId, title) {
    let favoriteObject = {
      onetSocCd: socId
    };

    let promise = new Promise((resolve, reject) => {

      this._favoritesRestFactory.insertFavoriteOccupation(favoriteObject).then((data) => {

        // add and set title property
        data.title = title;

        // push newly added occupation to property
        this.favoritesList.push(data);

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('favoriteList', JSON.stringify(this.favoritesList));

          // sync with in memory property
          this.favoritesList = window.sessionStorage.getItem('favoriteList');
        }

        resolve("Insert successful.");
      }, (error) => {
        console.error(error);
        reject("Error: request returned status " + error);
      });

    });
    return promise;

  }

  /**
   * Return favorite list.
   */
  getFavoritesList() {

    let promise = new Promise((resolve, reject) => {

      // if favoriteList is undefined then recover data from database
      if (this.favoritesList == undefined || (this.favoritesList && this.favoritesList.length === 0)) {

        if (typeof (Storage) !== "undefined") {

          if (window.sessionStorage.getItem("favoriteList") === null || window.sessionStorage.getItem("favoriteList") == 'undefined') {

            // recover data using database
            this._favoritesRestFactory.getFavoriteOccupation().then(data => {

              // success now store in session object
              window.sessionStorage.setItem('favoriteList', JSON.stringify(data));

              // sync with in memory property
              this.favoritesList = JSON.parse(window.sessionStorage.getItem('favoriteList'));

              resolve(this.favoritesList);

            }, error => {

              console.error('Error' + error);
              reject("Error: request returned status" + error);
            });
          } else {

            // recover data using session storage
            this.favoritesList = JSON.parse(window.sessionStorage.getItem('favoriteList'));
            resolve(this.favoritesList);
          }

        } else {

          // no session storage support so get recover data using database
          this._favoritesRestFactory.getFavoriteOccupation().then(data => {

            // success now store in session object
            this.favoritesList = data;
            resolve(this.favoritesList);

          }, error => {

            console.error('Error' + error);
            reject("Error: request returned status" + error);
          });
        }

      } else {

        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.favoritesList);
      }
    });

    return promise;
  }

  deleteFavorite(id) {
    let promise = new Promise((resolve, reject) => {
      this._favoritesRestFactory.deleteFavoriteOccupation(id).then((data) => {

        // sync with in memory property
        let length = this.favoritesList.length;
        for (let i = 0; i < length; i++) {
          if (this.favoritesList[i].id == id) {
            this.favoritesList.splice(i, 1);
            break;
          }
        }

        // sync with session storage
        this.setFavorites(this.favoritesList);
        // if (typeof (Storage) !== "undefined") {
        //   window.sessionStorage.setItem('favoriteList', JSON.stringify(this.favoritesList));
        //   this.favoritesList = JSON.parse(window.sessionStorage.getItem('favoriteList'));
        // }

        // this.setCompletionFavoriteOccupations();

        resolve(this.favoritesList);

      }, (error) => {

        console.error('Error' + error);
        reject("Error: request returned status" + error);
      });
    });

    return promise;

  }

  setCompletionFavoriteOccupations() {
    if (this.favoritesList) {
      if (this.favoritesList.length === 1) {
        this.setCompletion(UserService.FAVORITED_OCCUPATION, 'true');
        this.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL, 'false');
      } else if (this.favoritesList.length > 1) {
        this.setCompletion(UserService.FAVORITED_OCCUPATION, 'true');
        this.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL, 'true');
      } else {
        this.setCompletion(UserService.FAVORITED_OCCUPATION, 'false');
        this.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL, 'false');
      }
    } else {
      this.setCompletion(UserService.FAVORITED_OCCUPATION, 'false');
      this.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL, 'false');
    }
  }

  setFavorites = function (favorites) {
    if (typeof (Storage) !== "undefined") {
      window.sessionStorage.setItem('favoriteList', JSON.stringify(favorites));
      this.favoritesList = JSON.parse(window.sessionStorage.getItem('favoriteList'));
    }

    this.setCompletionFavoriteOccupations();
  }
  //end of occupation favorite implementation


  /**
   * Career Cluster Favorites Implementation
   */
  public careerClusterFavoritesList = undefined;

  /**
   * Add occupation to favorites
   */
  insertFavoriteCareerCluster(ccId, title) {
    let favoriteObject = {
      ccId: ccId
    };

    let promise = new Promise((resolve, reject) => {

      this._favoritesRestFactory.insertFavoriteCareerCluster(favoriteObject).then((data) => {

        // add and set title property
        data.title = title;

        // push newly added occupation to property
        this.careerClusterFavoritesList.push(data);

        // sync with session storage
        this.setCCFavorites(this.careerClusterFavoritesList);
        // if (typeof (Storage) !== "undefined") {
        //   window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(this.careerClusterFavoritesList));
        //   this.careerClusterFavoritesList = JSON.parse(window.sessionStorage.getItem('careerClusterFavoritesList'));
        // }

        resolve("Insert successful.");
      }, (error) => {
        console.error(error);
        reject("Error: request returned status " + error);
      });
    });

    return promise;
  }

  /**
   * Return career cluster favorite list.
   */
  getCareerClusterFavoritesList() {

    let promise = new Promise((resolve, reject) => {

      // if favoriteList is undefined then recover data from database
      if (this.careerClusterFavoritesList == undefined) {

        if (typeof (Storage) !== "undefined") {

          if (window.sessionStorage.getItem("careerClusterFavoritesList") === null || window.sessionStorage.getItem("careerClusterFavoritesList") == 'undefined') {

            // recover data using database
            this._favoritesRestFactory.getFavoriteCareerCluster().then((data) => {

              // success now store in session object
              window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(data));
              // sync with in memory property
              this.careerClusterFavoritesList = JSON.parse(window.sessionStorage.getItem('careerClusterFavoritesList'));
              resolve(this.careerClusterFavoritesList);

            }, (error) => {

              console.error('Error' + status);
              reject("Error: request returned status" + status);
            });
          } else {

            // recover data using session storage
            this.careerClusterFavoritesList = JSON.parse(window.sessionStorage.getItem('careerClusterFavoritesList'));
            resolve(this.careerClusterFavoritesList);
          }

        } else {

          // no session storage support so get recover data using database
          this._favoritesRestFactory.getFavoriteCareerCluster().then((data) => {

            // success now store in session object
            this.careerClusterFavoritesList = data;
            resolve(this.careerClusterFavoritesList);

          }, (error) => {

            console.error('Error' + error);
            reject("Error: request returned status" + error);
          });
        }

      } else {

        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.careerClusterFavoritesList);
      }
    });
    return promise;
  }

  deleteCareerClusterFavorite(id) {
    let promise = new Promise((resolve, reject) => {
      this._favoritesRestFactory.deleteFavoriteCareerCluster(id).then((data) => {

        // sync with in memory property
        let length = this.careerClusterFavoritesList.length;
        for (let i = 0; i < length; i++) {
          if (this.careerClusterFavoritesList[i].id == id) {
            this.careerClusterFavoritesList.splice(i, 1);
            break;
          }
        }

        // sync with session storage
        this.setCCFavorites(this.careerClusterFavoritesList);
        // if (typeof (Storage) !== "undefined") {
        //   window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(this.careerClusterFavoritesList));
        //   this.careerClusterFavoritesList = JSON.parse(window.sessionStorage.getItem('careerClusterFavoritesList'));
        // }

        resolve(this.careerClusterFavoritesList);

      }, (error) => {

        console.error('Error' + error);
        reject("Error: request returned status" + error);
      });

    });
    return promise;

  }

  setCompletionFavoriteCC() {
    if (this.careerClusterFavoritesList && this.careerClusterFavoritesList.length > 0) {
      this.setCompletion(UserService.FAVORITED_CC, 'true');
    } else {
      this.setCompletion(UserService.FAVORITED_CC, 'false');
    }
  }

  setCCFavorites(favorites) {
    if (typeof (Storage) !== "undefined") {
      window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(favorites));
      this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('careerClusterFavoritesList'));
    }

    this.setCompletionFavoriteCC();
  }

  // end of career cluster favorites

  /**********************************************************
   * School favorite implementation.
   **********************************************************/
  public schoolFavoritesList = undefined;



  /**
   * Add school to favorites
   */
  /*insertFavoriteSchool(unitId, title) {
      let favoriteObject = {
          unitId : unitId
      };

      let promise = new Promise(( resolve, reject ) => {

      this._favoritesRestFactory.insertFavoriteSchool(favoriteObject).then((data) => {

          // add and set title property
          data.schoolName = title;

          // push newly added occupation to property
          this.schoolFavoritesList.push(data);

          // sync with session storage
          if (typeof (Storage) !== "undefined") {
              window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(this.schoolFavoritesList));

              // sync with in memory property
              this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('schoolFavoritesList'));
          }

          resolve("Insert successful.");
      }, (error) => {
          console.error(error);
          reject("Error: request returned status " + error);
      });
      });

      return promise;
  }*/
  insertFavoriteSchool(favoriteObject) {
    return this._httpHelper.httpHelper('post', 'favorite/insertFavoriteSchool', null, favoriteObject)
      .then((data) => {
        // TODO: update to check session like favorite occupation does
        this.schoolFavoritesList.push(data);
        // NOTE: Caller of this function should call setSchoolFavorites
        // this.setSchoolFavorites(this.schoolFavoritesList);
        return data;
      });
  }

  /**
   * Return school favorite list.
   */
  getSchoolFavoritesList() {

    let promise = new Promise((resolve, reject) => {

      // if favoriteList is undefined then recover data from database
      if (this.schoolFavoritesList == undefined) {

        if (typeof (Storage) !== "undefined") {

          if (window.sessionStorage.getItem("schoolFavoritesList") === null || window.sessionStorage.getItem("schoolFavoritesList") == 'undefined') {

            // recover data using database
            this._favoritesRestFactory.getFavoriteSchool().then((data) => {

              // success now store in session object
              window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(data));

              // sync with in memory property
              this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('schoolFavoritesList'));

              resolve(this.schoolFavoritesList);

            }, (error) => {

              console.error('Error' + error);
              reject("Error: request returned status" + error);
            });
          } else {

            // recover data using session storage
            this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('schoolFavoritesList'));
            resolve(this.schoolFavoritesList);
          }

        } else {

          // no session storage support so get recover data using database
          this._favoritesRestFactory.getFavoriteSchool().then((data) => {

            // success now store in session object
            this.schoolFavoritesList = data;
            resolve(this.schoolFavoritesList);

          }, (error) => {

            console.error('Error' + status);
            reject("Error: request returned status" + status);
          });
        }

      } else {

        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.schoolFavoritesList);
      }

    });
    return promise;
  }

  deleteSchoolFavorite(id) {
    let promise = new Promise((resolve, reject) => {
      this._favoritesRestFactory.deleteFavoriteSchool(id).then((data) => {

        // sync with in memory property
        let length = this.schoolFavoritesList.length;
        for (let i = 0; i < length; i++) {
          if (this.schoolFavoritesList[i].id == id) {
            this.schoolFavoritesList.splice(i, 1);
            break;
          }
        }

        // sync with session storage
        this.setSchoolFavorites(this.schoolFavoritesList);
        // if (typeof (Storage) !== "undefined") {
        //   window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(this.schoolFavoritesList));
        //   this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('schoolFavoritesList'));
        // }

        resolve(this.schoolFavoritesList);

      }, (error) => {

        console.error('Error' + error);
        reject("Error: request returned status" + error);
      });

    });
    return promise;

  }

  setCompletionFavoriteSchools() {
    if (this.schoolFavoritesList && this.schoolFavoritesList.length > 0) {
      this.setCompletion(UserService.FAVORITED_COLLEGE, 'true');
    } else {
      this.setCompletion(UserService.FAVORITED_COLLEGE, 'false');
    }
  }

  setSchoolFavorites(favorites) {
    if (typeof (Storage) !== "undefined") {
      window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(favorites));
      this.schoolFavoritesList = JSON.parse(window.sessionStorage.getItem('schoolFavoritesList'));
    }

    this.setCompletionFavoriteSchools();
  }
  // end school favorites

  getFavoriteServices(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('favorite/get-favorite-service/', null));
  }

  insertFavoriteService(id) {
    const favoriteObject = {
      'svcId': id
    };

    return this._httpHelper.httpHelper('post', 'favorite/insert-favorite-service', null, favoriteObject)
      .then((data) => {
        return data;
      });
  }

  insertFavorites(favoriteObject) {
    return this._httpHelper.httpHelper('post', 'favorite/insertFavoriteOccupation', null, favoriteObject)
      .then((obj) => {
        return obj;
      });
  }

  getFavorites() {
    return this.favoritesList;
  }

  deleteFavoriteService(id) {
    return new Promise((resolve, reject) => {

      const params = this.createIdParam(id);

      return this._httpHelper.httpHelper('Delete', 'favorite/delete-favorite-service/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  deleteFavoriteCareerCluster(id) {
    return new Promise((resolve, reject) => {
      const params = this.createIdParam(id);

      return this._httpHelper.httpHelper('Delete', 'favorite/deleteFavoriteCareerCluster/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  deleteFavoriteSchool(id) {
    const params = this.createIdParam(id);

    return this._httpHelper.httpHelper('Delete', 'favorite/deleteFavoriteSchool/', params, null)
      .then((obj) => {
        return obj;
      });
  }


  /**
   * Hidden
  */
  getHiddenList(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('detail-page/get-hide-job-list/', null));

  }

  getHiddenListPromise() {
    return new Promise((resolve, reject) => {
      const self = this;
      return this._httpHelper.httpHelper('get', 'detail-page/get-hide-job-list/', null, null)
        .then((hiddenList) => {
          self.hiddenListObject = hiddenList;
          resolve(hiddenList);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  setHidden(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        return this._httpHelper.httpHelper('post', 'detail-page/set-hide-job-list', null, obj)
          .then((responseObj) => {
            resolve(responseObj);
          });
      }
    });
  }

  /**
   * Saved Searches
  */
  getSavedSearches(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('search/get-save-search-list/', null));
  }

  insertSavedSearch(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        return this._httpHelper.httpHelper('post', 'search/insert-save-search/', null, obj)
          .then((cfgObj) => {
            resolve(cfgObj);
          });
      }
    });
  }

  deleteSavedSearch(id) {
    return new Promise((resolve, reject) => {
      const params = [];
      params.push({
        key: null,
        value: id
      });

      return this._httpHelper.httpHelper('Delete', 'search/delete-save-search/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  /**
* Occupation Notes
*/
  getNotes(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('notes/getNotes/', null));
  }

  getNote(mcId): Observable<any> {
    const params = [];
    params.push({
      key: null,
      value: mcId
    });

    return this._http.get(this._httpHelper.getFullUrl('notes/getNote/', params));
  }


  insertNotes(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        return this._httpHelper.httpHelper('post', 'notes/insertNote', null, obj)
          .then((cfgObj) => {
            resolve(cfgObj);
          });
      }
    });
  }

  updateNotes(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        if (obj.noteId && !obj.notes) {
          const promise = this.deleteNote(obj.noteId);
          resolve({});
        }
        return this._httpHelper.httpHelper('put', 'notes/updateNote', null, obj)
          .then((cfgObj) => {
            resolve(cfgObj);
          });
      }
    });
  }

  deleteNote(id) {
    return new Promise((resolve, reject) => {

      const params = [];
      params.push({
        key: null,
        value: id
      });

      return this._httpHelper.httpHelper('Delete', 'notes/delete-note/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  /**
   * Career Cluster Notes
  */
  getCareerClusterNotes(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('notes/getCareerClusterNotes/', null));
  }

  getCareerClusterNote(socId): Observable<any> {
    const params = [];
    params.push({
      key: null,
      value: socId
    });

    return this._http.get(this._httpHelper.getFullUrl('notes/getCareerClusterNote/', params));
  }

  insertCareerClusterNotes(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        return this._httpHelper.httpHelper('post', 'notes/insertCareerClusterNote', null, obj)
          .then((cfgObj) => {
            resolve(cfgObj);
          });
      }
    });
  }

  updateCareerClusterNotes(obj) {
    return new Promise((resolve, reject) => {

      if (this.user) {
        return this._httpHelper.httpHelper('put', 'notes/updateCareerClusterNote', null, obj)
          .then((cfgObj) => {
            resolve(cfgObj);
          });
      }
    });
  }

  deleteCareerClusterNote(socId) {
    return new Promise((resolve, reject) => {

      const params = [];
      params.push({
        key: null,
        value: socId
      });

      return this._httpHelper.httpHelper('Delete', 'notes/delete-note/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  createIdParam(id) {
    const params = [];
    params.push({
      key: null,
      value: id
    });
    return params;
  }

  /*
   * Gets user's interest codes and stores in session storage if browser
   * supports.
   */
  getUserInterestCodes() {

    // Please don't remove 
    if (!this.getUser()) return;
    //var deferred = $q.defer();
    var promise = new Promise((resolve, reject) => {
      if (typeof (Storage) !== "undefined") {
        // utilize session storage

        if (window.sessionStorage.getItem("interestCodeOne") === null || window.sessionStorage.getItem("interestCodeOne") === 'undefined') {

          //FYIRestFactory.getUserInteresteCodes().success(function(results) {
          this._httpHelper.httpHelper('GET', 'fyi/UserInterestCodes', null, null).then(results => {
            if (results !== undefined && results[0] != undefined) {
              window.sessionStorage.setItem('interestCodeOne', results[0].interestCodeOne);
              window.sessionStorage.setItem('interestCodeTwo', results[0].interestCodeTwo);
              window.sessionStorage.setItem('interestCodeThree', results[0].interestCodeThree);
              window.sessionStorage.setItem('scoreChoice', results[0].scoreChoice);

              if (!!results[0].gender.match(/[a-z]/i)) {
                window.sessionStorage.setItem('gender', results[0].gender.trim());
              } else {
                window.sessionStorage.setItem('gender', '');
              }

              var interestCode = {
                interestCodeOne: window.sessionStorage.getItem('interestCodeOne'),
                interestCodeTwo: window.sessionStorage.getItem('interestCodeTwo'),
                interestCodeThree: window.sessionStorage.getItem('interestCodeThree'),
                scoreChoice: window.sessionStorage.getItem('scoreChoice'),
                gender: window.sessionStorage.getItem('gender')
              };

              resolve(interestCode);
            }
            resolve([]); // return empty array
          }, status => {
            reject("Error: request returned status " + status);
          });

          //return promise;

        } else {
          var interestCode = {
            interestCodeOne: window.sessionStorage.getItem('interestCodeOne'),
            interestCodeTwo: window.sessionStorage.getItem('interestCodeTwo'),
            interestCodeThree: window.sessionStorage.getItem('interestCodeThree'),
            scoreChoice: window.sessionStorage.getItem('scoreChoice'),
            gender: window.sessionStorage.getItem('gender')
          };

          resolve(interestCode);
          //return promise;

        }
      } else {
        // for browser that don't support session storage
        //FYIRestFactory.getUserInteresteCodes().success(function(data) {
        this._httpHelper.httpHelper('GET', 'fyi/UserInterestCodes', null, null).then(data => {
          if (data !== undefined && data[0] !== undefined) {
            interestCode = {
              interestCodeOne: data[0].interestCodeOne,
              interestCodeTwo: data[0].interestCodeTwo,
              interestCodeThree: data[0].interestCodeThree,
              scoreChoice: data[0].scoreChoice,
              gender: data[0].gender.trim(),
            };
            resolve(interestCode);
          }
        }, status => {
          reject("Error: request returned status " + status);
        });
        //return promise;
      }
    });

    return promise;

  }

  // User completion 
  getCompletion(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('user/get-user-completion/', null));
  }

  setCompletion(key, value) {
    return this._httpHelper.httpHelper('post', 'user/set-user-completion/', null, { completionType: key, value: value })
      .then((obj) => {
        this.sendMessage(key);
        return obj;
      });
  }

  /**
   * If existing session exists; reset it and then start anew; otherwise start new session
   */
  startSession() {
    if (!this.existingTimer) {
      this.sessionKeepAlive();
    } else {
      this.stopSession();
      this.sessionKeepAlive();
    }
  }


  /**
   * Starts a new session; opens a session timeout dialog when the duration timeout value
   * in the config service has been reached;  
   */
  sessionKeepAlive() {
    let self = this;
    this.existingTimer = function () {
      let data = {
        title: 'Session Expiration',
        message: 'Your session is about to expire. What would you like to do?',
      };

      const dialogRef = self._dialog.open(SessionKeepaliveDialogComponent, { data: data, maxWidth: '300px' })

      dialogRef.afterOpened().subscribe(() => {
        self.stopSession();
      })

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          if (result === 'logOff') {
            self.logOffFromDialog();
          } else if (result === 'keepalive') {

            //reset timer 
            self.getKeepAlivePing().subscribe((result: any) => {
              if (result.status == 200) {
                self.stopSession();
                // self.existingTimer = undefined;
                self.startSession();
              }
            });
          }
        }
      });
    };

    this.timerSessionId = setTimeout(this.existingTimer, this._configService.getDurationToTimeoutReminder());
  }

  /**
   * clear out the existing session timer
   */
  stopSession() {
    if (this.existingTimer) {
      clearTimeout(this.timerSessionId);
      this.existingTimer = undefined;
    }
  }

  sendDirectMessage(formData) {
    const endPoint = this.controllerName + '/messages';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getDirectMessages() {
    const endPoint = this.controllerName + '/messages';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  updateMessage(id){
    const endPoint = this.controllerName + '/messages/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }



  updateEmailSettings(formData){
    const endPoint = this.controllerName + '/email-preference';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getEmailSettings(){
    const endPoint = this.controllerName + '/email-preference';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getSelectedSchools(){
    const endPoint = this.controllerName + '/selected-schools';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveSelectedSchools(selectedSchools){
    const endPoint = this.controllerName + '/selected-schools';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, selectedSchools)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  deleteSelectedSchool(schoolCode){

    const endPoint = this.controllerName + '/selected-schools/' + schoolCode;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }
}
