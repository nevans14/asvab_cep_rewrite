import { TestBed } from '@angular/core/testing';

import { IpedsService } from './ipeds.service';

describe('IpedsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IpedsService = TestBed.get(IpedsService);
    expect(service).toBeTruthy();
  });
});
