import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'app/services/config.service';
import { UtilityService } from 'app/services/utility.service';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class WorkValuesApiService {

  constructor(private _http: HttpClient,
  private _config: ConfigService,
  private _utilityService: UtilityService,
  private _httpHelper: HttpHelperService) { }
  
  getTestInformation = function() {
		//return $http.get(appUrl + 'fyi/test');

		/*var xsrfCookie = _utilityService.getAwsXsrfCookie();
		var awsApiFullUrl = _config.getAwsFullUrl();

		if (!xsrfCookie) { 
			console.log("Missing AWS XSRF cookie!")
		}
		console.log(xsrfCookie + ' ' + awsApiFullUrl);
		return this._http.post(url: awsApiFullUrl + '/api/WorkValues/get-items',
			options: {headers: {
				'X-XSRF-TOKEN': xsrfCookie
			}
		});*/
		let fullUrl = this._config.getAwsFullUrl(`/api/WorkValues/get-items`);
        return this._httpHelper.httpHelper('POST', fullUrl, null, null, true);
	};
	
	postResults = function(results){
		let fullUrl = this._config.getAwsFullUrl(`/api/WorkValues/save-test`);
		return this._http.post(fullUrl, results);
	};
	
	getTestId = function(){
		return this._httpHelper.httpHelper('post', 'work-values/save-test', null, null);
	};
	
	getWorkValues = function(){
		let fullUrl = this._config.getFullUrl(`/work-values/get-profile`);
		return this._http.get(fullUrl);
	};
}
