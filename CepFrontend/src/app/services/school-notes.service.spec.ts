import { TestBed } from '@angular/core/testing';

import { SchoolNotesService } from './school-notes.service';

describe('SchoolNotesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolNotesService = TestBed.get(SchoolNotesService);
    expect(service).toBeTruthy();
  });
});
