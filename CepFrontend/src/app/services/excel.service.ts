import { Injectable } from '@angular/core';
import { sectionReportType, StudentReportExcelModel, subHeaderColumnNameType } from 'app/core/models/studentReportExcelModel';
import { Row, Style, Workbook, Worksheet } from 'exceljs';
import * as fs from 'file-saver';


const WHITE_COLOR = 'f4f3fa';

const PURPLE_COLOR = '423a88';
const LIGHT_PURPLE_COLOR = 'E0DFEC';

const RED_COLOR = '890019';
const LIGHT_RED_COLOR = 'F6DFE0';

const GRAY_COLOR = '44546A';
const LIGHT_GRAY_COLOR = 'D6DCE4';

const AQUA_COLOR = '1C989A';
const LIGHT_AQUA_COLOR = 'DDF1F2';

const BLACK_COLOR = '000000';


@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  private uniqueMentorLinkIds: string[];
  private selectedStudents: StudentReportExcelModel[];
  private selectedColumns: subHeaderColumnNameType[];

  //booleans used to detect which sections are enabled
  private hasStudentInfoColumnHeaders;
  private hasAsvabTestInfoColumnHeaders;
  private hasPercentileScoresColumnHeaders;
  private hasStandardScoresColumnHeaders;
  private hasMilitaryLinesScoresColumnHeaders;
  private hasAdditionalProgramInfoColumnHeaders;
  private hasFavoritesColumnHeaders;

  constructor() {
    this.uniqueMentorLinkIds = [];
    this.selectedStudents = [];
    this.selectedColumns = [];
    this.hasStudentInfoColumnHeaders = false;
    this.hasAsvabTestInfoColumnHeaders = false;
    this.hasPercentileScoresColumnHeaders = false;
    this.hasStandardScoresColumnHeaders = false;
    this.hasMilitaryLinesScoresColumnHeaders = false;
    this.hasAdditionalProgramInfoColumnHeaders = false;
    this.hasFavoritesColumnHeaders = false;
  }

  private headerRowFont: any = {
    name: 'Arial', size: 10, bold: true, color: { argb: WHITE_COLOR }
  }

  private subHeaderRowFont: any = {
    name: 'Arial', size: 10, bold: false, color: { argb: BLACK_COLOR }
  }

  private dataRowFont: any = {
    name: 'Arial', size: 10, bold: false, color: { argb: BLACK_COLOR }
  }

  generateStudentReport(selectedStudents: StudentReportExcelModel[]) {

    if (selectedStudents) {

      selectedStudents.forEach((student: StudentReportExcelModel) => {
      })
    } else {
      console.error("ERROR", 'No students were selected');
      return;
    }

    this.selectedStudents = selectedStudents;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('student-report');

    //get/set unique sections that were selected
    let selectedSections: sectionReportType[] = selectedStudents.map((student) => {
      return student.section;
    });
    const sectionReportTypes = Array.from(new Set(selectedSections));

    //get/set unique students
    let mentorLinkIds: string[] = selectedStudents.map((student) => {
      return student.mentorLinkId;
    });
    this.uniqueMentorLinkIds = Array.from(new Set(mentorLinkIds));

    //get/set unique sub-headers
    let selectedSubHeaderColumnNameTypes: subHeaderColumnNameType[] = selectedStudents.map((student) => {
      return student.columnName;
    });
    this.selectedColumns = Array.from(new Set(selectedSubHeaderColumnNameTypes));

    //Add Header Row
    this.addHeaderRow(worksheet, sectionReportTypes, this.uniqueMentorLinkIds);

    //add sub-header row
    this.addSubHeaderRow(worksheet, this.selectedColumns);

    //for each student loop through and add their data
    this.uniqueMentorLinkIds.forEach(mentorLinkId => this.addDataRow(worksheet, mentorLinkId, this.selectedColumns));

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      let currentTimeStamp = new Date();
      let fileName = currentTimeStamp.getMonth() + "_" + currentTimeStamp.getDate() + "_" + currentTimeStamp.getFullYear() + "_" + currentTimeStamp.getTime() + '_report.xlsx';
      fs.saveAs(blob, fileName);
    });
  }

  private addHeaderRow(worksheet: Worksheet, sectionReportTypes: sectionReportType[], mentorLinkIds: string[]) {

    let studentInfoColumnHeaders: string[] = [];
    let section_STUDENT_INFORMATION_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.STUDENT_INFORMATION);
    if (section_STUDENT_INFORMATION_count > 0) {
      this.hasStudentInfoColumnHeaders = true;
      studentInfoColumnHeaders = this.getColumnHeaders(section_STUDENT_INFORMATION_count, sectionReportType.STUDENT_INFORMATION);
    }

    let asvabTestInfoColumnHeaders: string[] = [];
    let section_ASVAB_TEST_INFORMATION_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.ASVAB_TEST_INFORMATION);
    if (section_ASVAB_TEST_INFORMATION_count > 0) {
      this.hasAsvabTestInfoColumnHeaders = true;
      asvabTestInfoColumnHeaders = this.getColumnHeaders(section_ASVAB_TEST_INFORMATION_count, sectionReportType.ASVAB_TEST_INFORMATION);
    }

    let percentileScoresColumnHeaders: string[] = []; //max 1
    let section_PERCENTILE_SCORES_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.PERCENTILE_SCORES);
    if (section_PERCENTILE_SCORES_count > 0) {
      this.hasPercentileScoresColumnHeaders = true;
      percentileScoresColumnHeaders = this.getColumnHeaders(section_PERCENTILE_SCORES_count, sectionReportType.PERCENTILE_SCORES);
    }

    let standardScoresColumnHeaders: string[] = []; //max 1
    let section_STANDARD_SCORES_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.STANDARD_SCORES);
    if (section_STANDARD_SCORES_count > 0) {
      this.hasStandardScoresColumnHeaders = true;
      standardScoresColumnHeaders = this.getColumnHeaders(section_STANDARD_SCORES_count, sectionReportType.STANDARD_SCORES);
    }

    let militaryLinesScoresColumnHeaders: string[] = []; //max 2
    let section_MILITARY_LINES_SCORES_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.MILITARY_LINES_SCORES);
    if (section_MILITARY_LINES_SCORES_count > 0) {
      this.hasMilitaryLinesScoresColumnHeaders = true;
      militaryLinesScoresColumnHeaders = this.getColumnHeaders(section_MILITARY_LINES_SCORES_count, sectionReportType.MILITARY_LINES_SCORES);
    }

    let additionalProgramInfoColumnHeaders: string[] = []; //max 4
    let section_ADDITIONAL_PROGRAM_INFORMATION_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.ADDITIONAL_PROGRAM_INFORMATION);
    if (section_ADDITIONAL_PROGRAM_INFORMATION_count > 0) {
      this.hasAdditionalProgramInfoColumnHeaders = true;
      additionalProgramInfoColumnHeaders = this.getColumnHeaders(section_ADDITIONAL_PROGRAM_INFORMATION_count, sectionReportType.ADDITIONAL_PROGRAM_INFORMATION);
    }

    let favoritesColumnHeaders: string[] = []; //max 4
    let section_FAVORITES_count = this.getSubColumnCountForSection(mentorLinkIds, sectionReportType.FAVORITES);
    if (section_FAVORITES_count > 0) {
      this.hasFavoritesColumnHeaders = true;
      favoritesColumnHeaders = this.getColumnHeaders(section_FAVORITES_count, sectionReportType.FAVORITES);
    }

    //combine all sections into the report header
    let combinedHeaders: any = [...studentInfoColumnHeaders, ...asvabTestInfoColumnHeaders,
    ...percentileScoresColumnHeaders, ...standardScoresColumnHeaders, ...militaryLinesScoresColumnHeaders,
    ...additionalProgramInfoColumnHeaders, ...favoritesColumnHeaders];

    let headerRow = worksheet.addRow(combinedHeaders);
    headerRow.font = this.headerRowFont;

    //set colors for each section;
    //ADJUST COLUMN HEADERS -> BACKGROUND COLORS; AND MERGE CELLS
    //*********************************************************************
    var currentColumnIndex = 1;

    if (studentInfoColumnHeaders && studentInfoColumnHeaders.length > 0) {

      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + studentInfoColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= studentInfoColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, PURPLE_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      worksheet.mergeCells(startCellRange + ':' + endCellRange);

    }

    if (asvabTestInfoColumnHeaders && asvabTestInfoColumnHeaders.length > 0) {

      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + asvabTestInfoColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= asvabTestInfoColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, AQUA_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      worksheet.mergeCells(startCellRange + ':' + endCellRange);
    }

    if (percentileScoresColumnHeaders && percentileScoresColumnHeaders.length > 0) {
      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + percentileScoresColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= percentileScoresColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, AQUA_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      if (percentileScoresColumnHeaders.length > 1) {
        worksheet.mergeCells(startCellRange + ':' + endCellRange);
      }

    }

    if (standardScoresColumnHeaders && standardScoresColumnHeaders.length > 0) {
      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + standardScoresColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= standardScoresColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, AQUA_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      if (standardScoresColumnHeaders.length > 1) {
        worksheet.mergeCells(startCellRange + ':' + endCellRange);
      }

    }

    if (militaryLinesScoresColumnHeaders && militaryLinesScoresColumnHeaders.length > 0) {
      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + militaryLinesScoresColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= militaryLinesScoresColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, AQUA_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      if (militaryLinesScoresColumnHeaders.length > 1) {
        worksheet.mergeCells(startCellRange + ':' + endCellRange);
      }

    }

    if (additionalProgramInfoColumnHeaders && additionalProgramInfoColumnHeaders.length > 0) {
      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + additionalProgramInfoColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= additionalProgramInfoColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, RED_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      if (additionalProgramInfoColumnHeaders.length > 1) {
        worksheet.mergeCells(startCellRange + ':' + endCellRange);
      }

    }

    if (favoritesColumnHeaders && favoritesColumnHeaders.length > 0) {
      let startCellRange: string = this.getExcelColumn(currentColumnIndex) + '1';
      let endCellRange: string = this.getExcelColumn(currentColumnIndex + favoritesColumnHeaders.length - 1) + '1';

      for (let i = 1; i <= favoritesColumnHeaders.length; i++) {
        this.adjustCellBackground(headerRow, GRAY_COLOR, currentColumnIndex);
        currentColumnIndex++;
      }

      if (favoritesColumnHeaders.length > 1) {
        worksheet.mergeCells(startCellRange + ':' + endCellRange);
      }

    }

  }

  private getSubColumnCountForSection(mentorLinkIds: string[], type: sectionReportType) {
    let returnCount = 0;

    mentorLinkIds.forEach((mentorLinkId: string) => {
      let tempCount = this.selectedStudents.filter(x => x.mentorLinkId == mentorLinkId && x.section == type).length;
      if (tempCount > returnCount) {
        returnCount = tempCount;
      }
    })

    return returnCount;
  }

  private getColumnHeaders(numRecords, type: sectionReportType) {
    let returnArr = [];

    for (let i = 0; i < numRecords; i++) {
      if (i == 0) {
        returnArr.push(type);
      } else {
        returnArr.push('');
      }
    }

    return returnArr;
  }

  private adjustCellBackground(row: Row, color: string, columnNum: number) {

    row.getCell(columnNum).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: color }
    }
  }

  private getExcelColumn(num): String {

    if (num < 27) {
      let code: number = "abcdefghijklmnopqrstuvwxyz".charCodeAt(num - 1);

      return String.fromCharCode(code).toUpperCase();

    } else {
      //AA = 27; AB = 28
      let difference = num - 26;
      let divisible = Math.floor(num / 26)
      let code1: number = "abcdefghijklmnopqrstuvwxyz".charCodeAt(divisible - 1);
      let code2: number = "abcdefghijklmnopqrstuvwxyz".charCodeAt(difference - 1);

      return String.fromCharCode(code1).toUpperCase() + String.fromCharCode(code2).toUpperCase();

    }



  }

  private addSubHeaderRow(worksheet: Worksheet, sectionReportTypes: subHeaderColumnNameType[]) {

    let subHeaders = [];

    /**
     * All headers are pushed in order; If new columns are added please take note.
     */

    if (this.hasStudentInfoColumnHeaders) {

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.FIRST_NAME)) {
        subHeaders.push(subHeaderColumnNameType.FIRST_NAME);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.LAST_NAME)) {
        subHeaders.push(subHeaderColumnNameType.LAST_NAME);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.STUDENT_ID)) {
        subHeaders.push(subHeaderColumnNameType.STUDENT_ID);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.USERNAME)) {
        subHeaders.push(subHeaderColumnNameType.USERNAME);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.GRADE)) {
        subHeaders.push(subHeaderColumnNameType.GRADE);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.GRADUATION)) {
        subHeaders.push(subHeaderColumnNameType.GRADUATION);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.GENDER)) {
        subHeaders.push(subHeaderColumnNameType.GENDER);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.ETHNICITY)) {
        subHeaders.push(subHeaderColumnNameType.ETHNICITY);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.SCHOOL)) {
        subHeaders.push(subHeaderColumnNameType.SCHOOL);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.SCHOOL_ADDRESS)) {
        subHeaders.push(subHeaderColumnNameType.SCHOOL_ADDRESS);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.SCHOOL_CODE)) {
        subHeaders.push(subHeaderColumnNameType.SCHOOL_CODE);
      }

    }

    if (this.hasAsvabTestInfoColumnHeaders) {
      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.ACCESS_CODE)) {
        subHeaders.push(subHeaderColumnNameType.ACCESS_CODE);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.EXPIRATION_DATE)) {
        subHeaders.push(subHeaderColumnNameType.EXPIRATION_DATE);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.TEST_DATE)) {
        subHeaders.push(subHeaderColumnNameType.TEST_DATE);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.TYPE_OF_TEST)) {
        subHeaders.push(subHeaderColumnNameType.TYPE_OF_TEST);
      }
    }

    if (this.hasPercentileScoresColumnHeaders) {
      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.ASVAB_PERCENTILE_SCORES)) {
        subHeaders.push(subHeaderColumnNameType.ASVAB_PERCENTILE_SCORES);
      }
    }

    if (this.hasStandardScoresColumnHeaders) {
      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.ASVAB_STANDARD_SCORES)) {
        subHeaders.push(subHeaderColumnNameType.ASVAB_STANDARD_SCORES);
      }
    }

    if (this.hasMilitaryLinesScoresColumnHeaders) {
      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.AFQT)) {
        subHeaders.push(subHeaderColumnNameType.AFQT);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.MILITARY_LINES_SCORES)) {
        subHeaders.push(subHeaderColumnNameType.MILITARY_LINES_SCORES);
      }

    }

    if (this.hasAdditionalProgramInfoColumnHeaders) {

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.COMBINED_FYI_SCORES)) {
        subHeaders.push(subHeaderColumnNameType.COMBINED_FYI_SCORES);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.GENDER_SPECIFIC_FYI_SCORES)) {
        subHeaders.push(subHeaderColumnNameType.GENDER_SPECIFIC_FYI_SCORES);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.WORK_VALUES)) {
        subHeaders.push(subHeaderColumnNameType.WORK_VALUES);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.POST_SECONDARY_INTENTIONS)) {
        subHeaders.push(subHeaderColumnNameType.POST_SECONDARY_INTENTIONS);
      }

    }

    if (this.hasFavoritesColumnHeaders) {

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.FAVORITE_OCCUPATIONS)) {
        subHeaders.push(subHeaderColumnNameType.FAVORITE_OCCUPATIONS);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.FAVORITE_CTM_CAREERS)) {
        subHeaders.push(subHeaderColumnNameType.FAVORITE_CTM_CAREERS);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.FAVORITE_COLLEGES)) {
        subHeaders.push(subHeaderColumnNameType.FAVORITE_COLLEGES);
      }

      if (sectionReportTypes.some(x => x == subHeaderColumnNameType.FAVORITE_CAREER_CLUSTERS)) {
        subHeaders.push(subHeaderColumnNameType.FAVORITE_CAREER_CLUSTERS);
      }

    }

    //set font for row
    let subHeaderRow = worksheet.addRow(subHeaders);
    subHeaderRow.font = this.subHeaderRowFont;

    //set background color
    subHeaderRow.eachCell((cell, number) => {
      let color = "";
      if (cell.value == subHeaderColumnNameType.FIRST_NAME
        || cell.value == subHeaderColumnNameType.LAST_NAME
        || cell.value == subHeaderColumnNameType.USERNAME
        || cell.value == subHeaderColumnNameType.GRADE
        || cell.value == subHeaderColumnNameType.GRADUATION
        || cell.value == subHeaderColumnNameType.GENDER
        || cell.value == subHeaderColumnNameType.ETHNICITY
        || cell.value == subHeaderColumnNameType.SCHOOL
        || cell.value == subHeaderColumnNameType.GRADUATION
        || cell.value == subHeaderColumnNameType.GENDER
        || cell.value == subHeaderColumnNameType.ETHNICITY
        || cell.value == subHeaderColumnNameType.SCHOOL
        || cell.value == subHeaderColumnNameType.SCHOOL_ADDRESS
        || cell.value == subHeaderColumnNameType.SCHOOL_CODE) {
        color = LIGHT_PURPLE_COLOR;
      } else if (cell.value == subHeaderColumnNameType.ACCESS_CODE
        || cell.value == subHeaderColumnNameType.EXPIRATION_DATE
        || cell.value == subHeaderColumnNameType.TEST_DATE
        || cell.value == subHeaderColumnNameType.TYPE_OF_TEST
        || cell.value == subHeaderColumnNameType.ASVAB_STANDARD_SCORES
        || cell.value == subHeaderColumnNameType.ASVAB_PERCENTILE_SCORES
        || cell.value == subHeaderColumnNameType.AFQT
        || cell.value == subHeaderColumnNameType.MILITARY_LINES_SCORES) {
        color = LIGHT_AQUA_COLOR;
      } else if (cell.value == subHeaderColumnNameType.COMBINED_FYI_SCORES
        || cell.value == subHeaderColumnNameType.GENDER_SPECIFIC_FYI_SCORES
        || cell.value == subHeaderColumnNameType.WORK_VALUES
        || cell.value == subHeaderColumnNameType.POST_SECONDARY_INTENTIONS) {
        color = LIGHT_RED_COLOR;
      } else {
        color = LIGHT_GRAY_COLOR;
      }

      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color }
      }
    });

  }

  private addDataRow(worksheet: Worksheet, mentorLinkId: string, availableColumns: subHeaderColumnNameType[]) {

    let studentData = this.selectedStudents.filter(x => x.mentorLinkId == mentorLinkId);

    if (studentData) {

      let excelData = studentData.map((x: any) => {
        return x.value;
      })
      let dataRow = worksheet.addRow(excelData);
      dataRow.font = this.dataRowFont;
    } else {
      console.error("ERROR", "cannot find student data");
    }
  }
  
}

