import { TestBed } from '@angular/core/testing';

import { MediaCenterService } from './media-center.service';

describe('MediaCenterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediaCenterService = TestBed.get(MediaCenterService);
    expect(service).toBeTruthy();
  });
});
