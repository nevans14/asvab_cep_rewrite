import { Injectable } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class FlickrRestFactoryService {

  restURL: any;

  constructor(private _http: HttpClient,
    private _config: ConfigService,
    private _user: UserService,
    private _httpHelper: HttpHelperService
  ) {
    this.restURL = _config.getRestUrl() + '/';
  }

  getFlickrPublicPhotos = function() {
    return this._http.get(this._config.getAwsFullUrl(`/api/flickr/get-public-photos`));
  }

  getFlickrGalleryPhotos = function(galleryId) {
    return this._http.get(this._config.getAwsFullUrl('/api/flickr/get-flickr-gallery-photos/' + galleryId + '/'));
  }
  
  getFlickrPhoto = function(photoId, photoSecret) {
    return this._http.get(this._config.getAwsFullUrl('/api/flickr/get-flickr-photo/' + photoId + '/' + photoSecret + '/'));
  }
}
