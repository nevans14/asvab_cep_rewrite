import { TestBed } from '@angular/core/testing';

import { NotesRestFactoryService } from './notes-rest-factory.service';

describe('NotesRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotesRestFactoryService = TestBed.get(NotesRestFactoryService);
    expect(service).toBeTruthy();
  });
});
