import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from './config.service';
@Injectable({
  providedIn: 'root'
})
export class EssTrainingService {

  constructor(
    private _httpHelper: HttpHelperService,
    private _congfigService: ConfigService
  ) { }

  getTrainingByTrainingId = function (trainingId) {
    const fullUrl = this._congfigService.getAwsFullUrl('/api/ess-training/get-ess-training-by-id/' + trainingId);
    return this._httpHelper.httpHelper('get', fullUrl, null, null, true);
  }

  getAlternateTrainingByTrainingId = function (trainingId) {
    const fullUrl = this._congfigService.getAwsFullUrl('/api/ess-training/get-available-sessions/' + trainingId);
    return this._httpHelper.httpHelper('get', fullUrl, null, null, true);
  }

  addTrainingRegistration = function (formData) {
    const fullUrl = this._congfigService.getAwsFullUrl(`/api/ess-training/add-ess-training-registration`);
    return this._httpHelper.httpHelper('post', fullUrl, null, formData, true);
  }

} 
