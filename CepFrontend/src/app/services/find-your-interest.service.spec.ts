import { TestBed } from '@angular/core/testing';

import { FindYourInterestService } from './find-your-interest.service';

describe('FindYourInterestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindYourInterestService = TestBed.get(FindYourInterestService);
    expect(service).toBeTruthy();
  });
});
