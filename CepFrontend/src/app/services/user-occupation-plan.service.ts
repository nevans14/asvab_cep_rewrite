import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class UserOccupationPlanService {

  private controllerName: string = "plan-your-future";

  constructor(private _httpHelper: HttpHelperService) { }

  save(formData) {
    const endPoint = this.controllerName + '/plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  update(formData, id) {
    const endPoint = this.controllerName + '/plans/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  get(id) {
    const endPoint = this.controllerName + '/plans/' + id;
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getAll() {
    const endPoint = this.controllerName + '/plans/';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  delete(planId) {
    const endPoint = this.controllerName + '/plans/' + planId;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  submit(classroomActivityId, formData) {
    const endPoint = this.controllerName + '/plans/' + classroomActivityId + '/submit/';


    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  saveComment(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/comments';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }
  
  markCommentAsRead(planId, commentId, recipientId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/comments/' + commentId + '/recipients/' + recipientId + '/read/true';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getComments(planId){
    const endPoint = this.controllerName + '/plans/' + planId + '/comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getUnreadComments(){
    const endPoint = this.controllerName + '/plans/unread-comments';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }
  

}
