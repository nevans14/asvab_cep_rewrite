import { TestBed } from '@angular/core/testing';

import { PlanCalendarService } from './plan-calendar.service';

describe('PlanCalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanCalendarService = TestBed.get(PlanCalendarService);
    expect(service).toBeTruthy();
  });
});
