import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from 'app/services/config.service';
import { UtilityService } from 'app/services/utility.service';

const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class HttpHelperService implements Resolve<any> {
  restUrl: String;
  csrfToken: String = undefined;
  awsXsrfToken: String = undefined;

  constructor(
    private _http: HttpClient,
    private _config: ConfigService,
    private _utilityService: UtilityService,
  ) {
    this.restUrl = this._config.getRestUrl();
  }

  resolve(route: ActivatedRouteSnapshot) {

    if (route.data.params.length < 3) {
      let x;
      for (x = 0; x < 3; x++) {
        if (x < 2) {
          route.data.params.push(null);
        } else {
          route.data.params.push(true);
        }
      }
    }
    return this.httpHelper(route.data.params[0], route.data.params[1], route.data.params[2], route.data.params[3]);
  }

  getFullUrl(url, params) {
    return this._config.getFullUrl(url, params);
  }

  public httpHelper(method: string, url: string, params, data: object, isFullUrl: boolean = false, xsrfTokenType: string = 'java') {
    let self = this;

    return new Promise((resolve, reject) => {

      method = method.toUpperCase();
      let errorContext = null;

      const fullUrl = !isFullUrl ? self.getFullUrl(url, params) : url;

      if (xsrfTokenType.toLowerCase() === 'java') {
        self.csrfToken = self._utilityService.getCsrfCookie();
      }

      if (xsrfTokenType.toLowerCase() === 'aws') {
        self.csrfToken = self._utilityService.getAwsXsrfCookie();
      }

      if (method === 'GET') {
        // this._http.get(fullUrl, {observe: 'response', headers:  new HttpHeaders().set('Authorization',
        // 'Bearer ' + Cookie.get('access_token'))})
        self._http.get(fullUrl)
          .toPromise()
          .then(obj => {
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'POST') {
        self._http.post(fullUrl, data, { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken)) })
          .toPromise()
          .then(obj => {
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'PUT') {
        self._http.put(fullUrl, data, { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken)) })
          .toPromise()
          .then(obj => {
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'DELETE') {
        self._http.delete(fullUrl, { headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken)) })
          .toPromise()
          .then(obj => {
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'DELETE_WITH_BODY') {

        const options = {
          headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken)),
          body: data
        }

        // self._http.delete(fullUrl,  {headers: new HttpHeaders().set('X-XSRF-TOKEN', String(self.csrfToken)), body: data})
        self._http.delete(fullUrl, options)
          .toPromise()
          .then(obj => {
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else {
        errorContext = '***ERROR:  Method ' + method + ' not supported.';
        reject(errorContext);
      }
    });
  }

  public getFromUrl(url: string) {
    return new Promise((resolve, reject) => {
      this._http.get(url)
        .toPromise()
        .then(obj => {
          resolve(obj);
        })
        .catch(err => {
          const errorContext = (err.error || 'Server error');
          reject(errorContext);
        });
    });
  }

  public getApiUrl() {
    return this._config.getRestUrl();
  }

  public getSsoUrl() {
    return this._config.getSsoUrl();
  }
}
