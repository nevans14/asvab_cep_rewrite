import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from './config.service';
import { UtilityService } from './utility.service';

@Injectable( {
    providedIn: 'root'
} )
export class FyiTestService implements Resolve<any> {

    constructor(
        private _httpHelper: HttpHelperService,
        private _configService: ConfigService,
        private _utilityService: UtilityService,
    ) { }

    resolve() {

        //TODO: hook up to user info service when that id in place.
        var userRole = 'A';

        var promiseTwo;
        var promise = new Promise(( resolve, reject ) => {
            // this._httpHelper.httpHelper( 'GET', 'fyi/test', null, null ).then( success => {

            // const awsXsrfCookie = this._utilityService.getAwsXsrfCookie(); // XSRF token(AWS or Java) is retrieved in httphelper

            const fullUrl = this._configService.getAwsFullUrl(`/api/fyi/get-items`);
            this._httpHelper.httpHelper('POST', fullUrl, null, null, true, 'aws').then( (success: any) => {
                console.debug( success );

                this._httpHelper.httpHelper( 'GET', 'fyi/NumberTestTaken/', null, null ).then( numberTestTakenResults => {

                    promiseTwo = new Promise(( resolve, reject ) => {
                        this._httpHelper.httpHelper( 'GET', 'user/getAccessCode/', null, null ).then( accessCodeResults => {
                            var accessCode : any = accessCodeResults;
                            if ( accessCode.unlimitedFyi == 1 ) {
                                resolve( numberTestTakenResults );
                            } else {
                                if ( numberTestTakenResults >= 2 ) {
                                    reject( 'FYI test limited to only ' + numberTestTakenResults + ' times.' );
                                }
                                resolve( success );
                                // resolve( success.slice(0, 10) );
                            }


                        }, error => {
                            reject( "Error getting access code by user id. " + error );
                        } );
                    } );


                }, error => {
                    reject( "Error getting number of tests taken. " + error );
                } );

                resolve( success );
                // resolve( success.slice(0, 10) );

            }, error => {
                reject( "Error getting test questions. " + error );
            });
        } );

        return Promise.all( [
            promise, promiseTwo
        ] );
    }

    saveTest() {
        return this._httpHelper.httpHelper('POST', 'fyi/save-test/', null, null);
    }

}
