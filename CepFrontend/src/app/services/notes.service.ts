import { Injectable } from '@angular/core';
import { NotesRestFactoryService } from 'app/services/notes-rest-factory.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  noteList = undefined;

  constructor(
    private _notesRestFactoryService: NotesRestFactoryService,
    private _userService: UserService,
  ) { }

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
  insertNote(selectedNotesObject) {
		let promise = new Promise((resolve, reject) => {

      let that = this;
      that._notesRestFactoryService.insertNote(selectedNotesObject).then(function(data) {

        // push newly added occupation to property
        that.noteList.push(data);

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('noteList', JSON.stringify(that.noteList));

          // sync with in memory property
          that.noteList = JSON.parse(window.sessionStorage.getItem('noteList'));
        }

        resolve("Insert successful.");
      }, error => {
        console.error(error);
        reject("Error: request returned status " + error);
      });
    })

    return promise;
  }
  
  /**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
  updateNote(selectedNotesObject) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.updateNote(selectedNotesObject).then(function(data) {

        if (data !== undefined) {
          // sync noteList
          var length = that.noteList.length;
          for (var i = 0; i < length; i++) {
            if (that.noteList[i].noteId == selectedNotesObject.noteId) {
              that.noteList[i] = selectedNotesObject;
            }
          }

          // sync with session storage
          if (typeof (Storage) !== "undefined") {
            window.sessionStorage.setItem('noteList', JSON.stringify(that.noteList));

            // sync with in memory property
            that.noteList = JSON.parse(window.sessionStorage.getItem('noteList'));
          }

          resolve("Update successful.");
        }
      }, error => {
        console.error(error);
        reject("Error: request returned status " + error);
      });
    })

    return promise;
  }

  deleteNote(noteId) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.deleteNote(noteId).then(function(data) {
        // sync noteList
        for (var i = 0; i < that.noteList.length; i++) {
          if (that.noteList[i].noteId === noteId) {
            that.noteList.splice(i, 1);
            break;
          }
        }
        // sync with session storage
        if (typeof(Storage) !== undefined) {
          window.sessionStorage.setItem('noteList', JSON.stringify(that.noteList));
        }
        resolve(that.noteList);
      }, error => {
        reject('Error: request returned status ' + error);
      });		
    })

    return promise;
  }

  /**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
  getNotes() {
    if (this._userService.getUser() === undefined) return;
    
    let promise = new Promise((resolve, reject) => {

      let that = this;
      // if favoriteList is undefined then recover data from database
      if (this.noteList == undefined) {
        if (typeof (Storage) !== "undefined") {
          if (window.sessionStorage.getItem("noteList") === null || window.sessionStorage.getItem("noteList") == 'undefined') {

            // recover data using database
            this._notesRestFactoryService.getNotes().then(function(data) {
              if (data !== undefined) {
                // success now store in session object
                window.sessionStorage.setItem('noteList', JSON.stringify(data));

                // sync with in memory property
                that.noteList = JSON.parse(window.sessionStorage.getItem('noteList'));
              }
              resolve(that.noteList);

            }, error => {

              console.error('Error' + error);
              reject("Error: request returned status" + error);
            });
          } else {
            // recover data using session storage
            this.noteList = JSON.parse(window.sessionStorage.getItem('noteList'));
            resolve(this.noteList);
          }
        } else {
           // no session storage support so get data from database
           this._notesRestFactoryService.getNotes().then((data: [])=> {
            that.noteList = [...data];
            resolve(that.noteList);
          }, error => {

            console.error('Error' + error);
            reject("Error: request returned status" + error);
          })
        }
      } else {
        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.noteList);
      }
    })

    return promise;
	}
}
