import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class LearnAboutYourselfService {

  constructor(  private _httpHelper: HttpHelperService) { }


    insertMoreAboutMe(formData){
      return new Promise((resolve, reject) => {
        this._httpHelper.httpHelper('post', 'learn-about-yourself/insert-more-about-me', null, formData)
            .then((obj) => {
                resolve(obj);
            }).catch(error => {
                reject(error);
            });
      });
    }

    updateMoreAboutMe(formData){
      return new Promise((resolve, reject) => {
        this._httpHelper.httpHelper('put', 'learn-about-yourself/update-more-about-me', null, formData)
            .then((obj) => {
                resolve(obj);
            }).catch(error => {
                reject(error);
            });
      });
    }

    getMoreAboutMe(){
      return new Promise((resolve, reject) => {
        this._httpHelper.httpHelper('get', 'learn-about-yourself/get-more-about-me', null, null)
            .then((obj) => {
                resolve(obj);
            }).catch(error => {
                reject(error);
            });
      });
    }

    deleteMoreAboutMe(id: Number){
      const params = [];
      params.push({
        key: null,
        value: id
      });

      return new Promise((resolve, reject) => {
        this._httpHelper.httpHelper('delete', 'learn-about-yourself/delete-more-about-me', params, null)
            .then((obj) => {
                resolve(obj);
            }).catch(error => {
                reject(error);
            });
      });
    }

}
