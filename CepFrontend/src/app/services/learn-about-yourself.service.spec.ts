import { TestBed } from '@angular/core/testing';

import { LearnAboutYourselfService } from './learn-about-yourself.service';

describe('LearnAboutYourselfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LearnAboutYourselfService = TestBed.get(LearnAboutYourselfService);
    expect(service).toBeTruthy();
  });
});
