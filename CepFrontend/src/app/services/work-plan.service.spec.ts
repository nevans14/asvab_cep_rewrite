import { TestBed } from '@angular/core/testing';

import { WorkPlanService } from './work-plan.service';

describe('WorkPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkPlanService = TestBed.get(WorkPlanService);
    expect(service).toBeTruthy();
  });
});
