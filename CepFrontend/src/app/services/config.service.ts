import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  keyValue = 'CepInfo';
  configurations: any = null;
  credentials: object = null;
  restUrl: any;
  awsRestUrl: any;
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  documentFolder = 'CEP_PDF_Contents/';
  mediaCenterFolder = 'MEDIA_CENTER/';
  compareOccupationRoutes = [];

  constructor(
    private _http: HttpClient
    ) {
    // removed due to multiple calls occuring for the assets/config.json
    // setters moved to loadConfiguration() method
    
    // this.getJSON().subscribe(data => {
    //   this.configurations = data;
    //   if (this.getBaseUrl()) {
    //    this.restUrl = this.getBaseUrl() + 'CEP/rest';
    //    this.awsRestUrl = this.getBaseUrl();
    //   } else {
    //     this.restUrl = '/CEP/rest';
    //   }
    // });
  }

  public getCompareOccupationRoutes() {
    return this.compareOccupationRoutes;
  }

  public setCompareOccupationRoutes(routes) {
    this.compareOccupationRoutes = [...routes];
  }

  public getJSON(): Observable<any> {
      return this._http.get('./assets/config.json');
  }

  loadConfigurationData(): Promise<Object> {
    return new Promise((resolve, reject) => {
      this._http.get('./assets/config.json')
      .toPromise()
      .then(obj => {
        this.configurations = obj;

          if (this.getBaseUrl()) {
           this.restUrl = this.getBaseUrl() + 'CEP/rest';
           this.awsRestUrl = this.getBaseUrl();
          } else {
            this.restUrl = '/CEP/rest';
          }

        resolve(this.configurations);
      });
    });
  }

  public getBaseUrl() {
    if (this.configurations['serviceUrl']) {
      const serviceUrl = this.configurations['serviceUrl'].endsWith('/') ?
            this.configurations['serviceUrl'] : this.configurations['serviceUrl'] + '/';
      return serviceUrl;
    }
    return null;
  }

  public getBaseFrontendUrl() {
    const baseUrl = this.getBaseUrl();
    if (baseUrl && baseUrl.indexOf('localhost') === -1) {
      if (baseUrl.indexOf('legacy-dev') !== -1) {
        return baseUrl.replace('legacy-dev', 'dev-www');
      } else if (baseUrl.indexOf('legacy-stg') !== -1) {
        return baseUrl.replace('legacy-stg', 'stg-www');
      } else {
        return baseUrl;
      }
    }
    return null;
  }

  public getSsoUrl() {
    const workUrl = this.configurations['ssoUrl'];
    if (this.configurations['ssoUrl']) {
      let ssoUrl = workUrl[workUrl.length - 1] === '/' ? this.configurations['ssoUrl'] : this.configurations['ssoUrl'] + '/';
      return ssoUrl;
    }
  return null;
  }

  public getRestUrl() {
    return this.restUrl;
  }

  public getSSOUrl() {
    return this.getRestUrl() + '/sso/confirm_login?redirect=' + this.getSsoUrl() + 'login_callback/test';
  }

  public getFullUrl(url, params) {
    const mainUrl = this.restUrl;

    let fullUrl = url[0] === '/' ? mainUrl + url : mainUrl + '/' + url;

    if (params) {
      for (const i in params) {
        if (params.hasOwnProperty(i)) {
          if (params[i].key) {
            fullUrl = fullUrl.endsWith('/') ? fullUrl + params[i].key : fullUrl + '/' + params[i].key;
          }
          fullUrl = fullUrl.endsWith('/') ? fullUrl + params[i].value : fullUrl + '/' + params[i].value;
        }
      }
    }
    if (!fullUrl.endsWith('/')) {
      fullUrl = fullUrl + '/';
    }
    return fullUrl;
  }

  public getAwsFullUrl(url) {
    const mainUrl = this.awsRestUrl;

    if (this.configurations.awsApiUrl) {
      if (mainUrl.indexOf('legacy-dev.asvabprogram.com') !== -1
        || mainUrl.indexOf('localhost') !== -1) {
        return this.configurations.awsApiUrl.dev + '/Dev' + url;
      } else if (mainUrl.indexOf('legacy-stg.asvabprogram.com') !== -1) {
        return this.configurations.awsApiUrl.stg + url;
      } else {
        return this.configurations.awsApiUrl.prod + '/Prod' + url;
      }
    }
  }

  public getImageUrl() {
    if (this.configurations['imageUrl']) {
      const imageUrl = this.configurations['imageUrl'].endsWith('/') ?
            this.configurations['imageUrl'] : this.configurations['imageUrl'] + '/';
      return imageUrl;
    }
    return null;
  }

  public getCtmImageUrl() {
    if (this.configurations['ctmImageUrl']) {
      const imageUrl = this.configurations['ctmImageUrl'].endsWith('/') ?
            this.configurations['ctmImageUrl'] : this.configurations['ctmImageUrl'] + '/';
      return imageUrl;
    }
    return null;
  }

  public getCitmBaseUrl() {
    const workUrl = this.configurations['citmBaseUrl'];
    if (this.configurations['citmBaseUrl']) {
      const cepBaseUrl = workUrl[workUrl.length - 1] === '/' ? this.configurations['citmBaseUrl'] : this.configurations['citmBaseUrl'] + '/';
      return cepBaseUrl;
    }
  return null;
  }

  public getRecaptchaKey() {
    return this.recaptchaKey;
  }

  public getVersion() {
    if (environment.VERSION) {
      return environment.VERSION;
    }
  return null;
  }

  public getVersionDate() {
    if (environment.VERSIONDATE) {
      return environment.VERSIONDATE;
    }
  return null;
  }

  public getDocumentFolder(){
    return this.documentFolder;
  }

  public getMediaCenterFolder(){
    return this.mediaCenterFolder;
  }

  public getDurationToTimeoutReminder(){
    return 15*60*1000; // 15 minutes
  }
  
}
