import { Injectable } from '@angular/core';
import { NotesRestFactoryService } from 'app/services/notes-rest-factory.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class SchoolNotesService {

  schoolNoteList = undefined;

  constructor(
    private _notesRestFactoryService: NotesRestFactoryService,
    private _userService: UserService,
  ) { }

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
  insertNote(selectedNotesObject) {
		let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.insertSchoolNote(selectedNotesObject).then(function(data) {

        // push newly added occupation to property
        that.schoolNoteList.push(data);

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('schoolNoteList', JSON.stringify(that.schoolNoteList));

          // sync with in memory property
          that.schoolNoteList = JSON.parse(window.sessionStorage.getItem('schoolNoteList'));
        }

        resolve("Insert successful.");
      }, error => {
        console.error(error);
        reject("Error: request returned status " + error);
      });
    })

    return promise;
  }
  
  /**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
  updateNote(selectedNotesObject) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.updateSchoolNote(selectedNotesObject).then(function(data) {

        if (data !== undefined) {
          // sync schoolNoteList
          var length = that.schoolNoteList.length;
          for (var i = 0; i < length; i++) {
            if (that.schoolNoteList[i].noteId == selectedNotesObject.noteId) {
              that.schoolNoteList[i] = selectedNotesObject;
            }
          }

          // sync with session storage
          if (typeof (Storage) !== "undefined") {
            window.sessionStorage.setItem('schoolNoteList', JSON.stringify(that.schoolNoteList));

            // sync with in memory property
            that.schoolNoteList = JSON.parse(window.sessionStorage.getItem('schoolNoteList'));
          }

          resolve("Update successful.");
        }
      }, error => {
        console.error(error);
        reject("Error: request returned status " + error);
      });
    })

    return promise;
  }

  deleteNote(noteId) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      that._notesRestFactoryService.deleteSchoolNote(noteId).then(function(data) {
        // sync schoolNoteList
        for (var i = 0; i < that.schoolNoteList.length; i++) {
          if (that.schoolNoteList[i].noteId === noteId) {
            that.schoolNoteList.splice(i, 1);
            break;
          }
        }
        // sync with session storage
        if (typeof(Storage) !== undefined) {
          window.sessionStorage.setItem('schoolNoteList', JSON.stringify(that.schoolNoteList));
        }
        resolve(that.schoolNoteList);
      }, error => {
        reject('Error: request returned status ' + error);
      });		
    })

    return promise;
  }

  /**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
  getNotes() {
    if (this._userService.getUser() === undefined) return;
    
    let promise = new Promise((resolve, reject) => {

      let that = this;
      // if favoriteList is undefined then recover data from database
      if (this.schoolNoteList == undefined) {
        if (typeof (Storage) !== "undefined") {
          if (window.sessionStorage.getItem("schoolNoteList") === null || window.sessionStorage.getItem("schoolNoteList") == 'undefined') {

            // recover data using database
            this._notesRestFactoryService.getSchoolNotes().then(function(data) {
              if (data !== undefined) {
                // success now store in session object
                window.sessionStorage.setItem('schoolNoteList', JSON.stringify(data));

                // sync with in memory property
                that.schoolNoteList = JSON.parse(window.sessionStorage.getItem('schoolNoteList'));
              }
              resolve(that.schoolNoteList);

            }, error => {

              console.error('Error' + error);
              reject("Error: request returned status" + error);
            });
          } else {
            // recover data using session storage
            this.schoolNoteList = JSON.parse(window.sessionStorage.getItem('schoolNoteList'));
            resolve(this.schoolNoteList);
          }
        } else {
          // no session storage support so get data from database
          this._notesRestFactoryService.getSchoolNotes().then((data: [])=> {
            that.schoolNoteList = [...data];
            resolve(that.schoolNoteList);
          }, error => {

            console.error('Error' + error);
            reject("Error: request returned status" + error);
          })
        }
      } else {
        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.schoolNoteList);
      }
    })

    return promise;
	}
}
