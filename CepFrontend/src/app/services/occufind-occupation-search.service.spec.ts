import { TestBed } from '@angular/core/testing';

import { OccufindOccupationSearchService } from './occufind-occupation-search.service';

describe('OccufindOccupationSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccufindOccupationSearchService = TestBed.get(OccufindOccupationSearchService);
    expect(service).toBeTruthy();
  });
});
