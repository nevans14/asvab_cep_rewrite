import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class WorkPlanService {

  private controllerName: string = "plan-your-future";

  constructor(private _httpHelper: HttpHelperService,
    private _configService: ConfigService,
    private _http: HttpClient) { }

  save(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/work-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  update(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/work-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  get(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/work-plans';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getServiceContacts(): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/search/get-service-contacts'));
  }

}
