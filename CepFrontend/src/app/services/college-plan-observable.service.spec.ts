import { TestBed } from '@angular/core/testing';

import { CollegePlanObservableService } from './college-plan-observable.service';

describe('CollegePlanObservableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollegePlanObservableService = TestBed.get(CollegePlanObservableService);
    expect(service).toBeTruthy();
  });
});
