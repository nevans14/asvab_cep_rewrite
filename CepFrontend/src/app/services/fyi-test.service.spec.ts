import { TestBed } from '@angular/core/testing';

import { FyiTestService } from './fyi-test.service';

describe('FyiTestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FyiTestService = TestBed.get(FyiTestService);
    expect(service).toBeTruthy();
  });
});
