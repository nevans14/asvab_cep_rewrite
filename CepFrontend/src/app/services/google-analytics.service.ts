import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';

declare let ga: Function;

@Injectable({
    providedIn: 'root'
})
export class GoogleAnalyticsService {

    constructor(private _configService: ConfigService) {
    }

    public trackClick(param): void {
        if (this._configService.getBaseUrl().indexOf('www.asvabprogram.com') >= 0) {
            const path = window.location.href;
            ga('send', 'pageview', path + param);
        }
    }

    public trackSampleTestClick(param): void {
        if (this._configService.getBaseUrl().indexOf('www.asvabprogram.com') >= 0) {
            const path = window.location.href;
            ga('send', 'pageview', path + param);
        }
    }

    public trackSocialShare(socialPlug, value): void {
        if (this._configService.getBaseUrl().indexOf('www.asvabprogram.com') >= 0) {
            ga('send', 'event', socialPlug, value);
        }
    }

    public trackStudent(userData, userId): void {
        if (this._configService.getBaseUrl().indexOf('www.asvabprogram.com') >= 0) {
            return;
        }
        if (userData.zipCode) {
            ga('send', 'event', 'StudentLogin', 'ZipCode', userData.zipCode);
        }
        if (userData.city) {
            ga('send', 'event', 'StudentLogin', 'City', userData.city);
        }
        if (userData.state) {
            ga('send', 'event', 'StudentLogin', 'State', userData.state);
        }
        if (userData.mepsId) {
            ga('send', 'event', 'StudentLogin', 'MEPs', userData.mepsId);
        }
        if (userData.mepsName) {
            ga('send', 'event', 'StudentLogin', 'MEPsName', userData.mepsName);
        }
        if (userData.userId) {
            ga('send', 'event', 'StudentLogin', 'UserID', userId);
        }
        if (userData.role) {
            ga('send', 'event', 'StudentLogin', 'Role', userData.role);
        }
    }

    public trackScheduleClick(): void {
        if (this._configService.getBaseUrl().indexOf('www.asvabprogram.com') >= 0) {
            const path = window.location.href;
            ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE');
        }
    }
}
