import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UtilityService } from 'app/services/utility.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { ConfigService } from 'app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private _httpHelper: HttpHelperService,
          private router: Router,
          private _config: ConfigService,
          private _ga: GoogleAnalyticsService,
          private _utilityService: UtilityService
          ) { }

  public Login(username, password, callback) {

      /*
       * Dummy authentication for testing, uses $timeout to simulate api call
       * ----------------------------------------------
       */
      setTimeout(function() {
          var response;
          /*
           * UserService.GetByUsername(username) .then(function (user) {
           */

          if ('test' === password) {
              response = {
                  success : true
              };
          } else {
              response = {
                  success : false,
                  message : 'Username or password is incorrect'
              };
          }
          callback(response);
          // });
      }, 1000);

      /*
       * Use this for real authentication
       * ----------------------------------------------
       */
      // $http.post('/api/authenticate', { username: username,
      // password: password })
      // .success(function (response) {
      // callback(response);
      // });
  }

  
  public SetCredentials(username, password, userId, role, accessCode, schoolCode, schoolName, city, state, zipCode, mepsId, mepsName, sso) {

      const globalCookie = this._utilityService.getCookie();

      if (!globalCookie) {
        return;
      }

		/**** Student Tracking ****/
    const mepsInfo = {
			schoolCode	: schoolCode,
			schoolName	: schoolName,
			city		: city,
			state		: state,
			zipCode		: zipCode,
			mepsId		: mepsId,
			mepsName	: mepsName
		};
		this._ga.trackStudent(mepsInfo, userId);
  }

  ClearCredentials() {
        this._utilityService.deleteCookie();
        this._utilityService.clearSessionStorage();
		this.router.navigate(['/']);
  }
}
