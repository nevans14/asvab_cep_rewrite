import { TestBed } from '@angular/core/testing';

import { ClassroomActivityService } from './classroom-activity.service';

describe('ClassroomActivityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClassroomActivityService = TestBed.get(ClassroomActivityService);
    expect(service).toBeTruthy();
  });
});
