import { Injectable } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable({
    providedIn: 'root'
})
export class FavoritesRestFactoryService {

    restURL: String;

    constructor(private _http: HttpClient,
        private _config: ConfigService,
        private _httpHelper: HttpHelperService) {
        this.restURL = this._config.getRestUrl() + '/';
    }

    getFavoriteOccupation() {
        return this._httpHelper.httpHelper('get', `favorite/getFavoriteOccupation/`, null, null);
    }

    insertFavoriteOccupation = function (favoriteObject) {
        return this._httpHelper.httpHelper('post', `favorite/insertFavoriteOccupation/`, null, favoriteObject);
    }

    deleteFavoriteOccupation(id) {
        return this._httpHelper.httpHelper('delete', `favorite/deleteFavoriteOccupation/${id}`, null, null);
    }

    getFavoriteCareerCluster() {
        return this._httpHelper.httpHelper('get', `favorite/getFavoriteCareerCluster/`, null, null);
    }

    insertFavoriteCareerCluster = function (favoriteObject) {
        return this._httpHelper.httpHelper('post', `favorite/insertFavoriteCareerCluster/`, null, favoriteObject);
    }

    deleteFavoriteCareerCluster(id) {
        return this._httpHelper.httpHelper('delete', `favorite/deleteFavoriteCareerCluster/${id}`, null, null);
    }

    getFavoriteSchool() {
        return this._httpHelper.httpHelper('get', `favorite/getFavoriteSchool/`, null, null);
    }
    
    getFavoriteSchoolBySocId(socId) {
        return this._httpHelper.httpHelper('get', `favorite/getFavoriteSchool/${socId}`, null, null);
    }

    insertFavoriteSchool(favoriteObject) {
        return this._httpHelper.httpHelper('post', `favorite/insertFavoriteSchool/`, null, favoriteObject);
    }

    deleteFavoriteSchool(id) {
        return this._httpHelper.httpHelper('delete', `favorite/deleteFavoriteSchool/${id}`, null, null);
    }

    updateFavoriteOccupationHearts(favoriteObject) {
        return this._httpHelper.httpHelper('post', `favorite/updateFavoriteOccupationHearts/`, null, favoriteObject);
    }
}
