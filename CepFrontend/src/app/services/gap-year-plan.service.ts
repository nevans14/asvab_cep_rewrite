import { Injectable } from '@angular/core';
import { RefGapYearType } from 'app/core/models/refGapYearType.model';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class GapYearPlanService {

  private controllerName: string = "plan-your-future";

  constructor(private _httpHelper: HttpHelperService) { }

  save(planId, formData){
    const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
          .then((obj) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  update(planId, id, formData){
    const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
          .then((obj) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  getAll(planId){
    const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
          .then((obj) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  delete(planId, id){
    const endPoint = this.controllerName + '/plans/' + planId + '/gap-year-plans/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
          .then((obj) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  getAllGapYearOptions(){
    const endPoint = this.controllerName + '/plans/gap-year-plan-options';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
          .then((obj) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  getAllGapYearTypes() : Promise<RefGapYearType[]>{
    const endPoint = this.controllerName + '/plans/gap-year-plan-types';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
          .then((obj: RefGapYearType[]) => {
              resolve(obj);
          }).catch(error => {
              reject(error);
          });
    });
  }

  
}
