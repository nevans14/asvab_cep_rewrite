import { Injectable } from '@angular/core';
import {  Observable } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
// import { UserService } from '../services/user.service';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ResourceService  {
  resourceTopics = undefined;
  quickLinkTopics = undefined;

  constructor(
    private _httpHelper: HttpHelperService,
    private _congfigService: ConfigService,
    private _http: HttpClient
  ) { }


  getResources(): Observable<any>  {
    return this._http.get(this._congfigService.getAwsFullUrl(`/api/resources/get-resources/`));
  }

  getQuickLinks(): Observable<any>  {
    return this._http.get(this._congfigService.getFullUrl( '/resources/get-quick-links/', null));
  }

}
