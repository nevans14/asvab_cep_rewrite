import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'app/services/user.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ScoreService {
   public scoreObject: any;
   public favorateObject: any;
    constructor(
    private _http: HttpClient,
    private _userService: UserService,
    private _httpHelper: HttpHelperService
  ) { }

  /**
 * Get Favorites
*/
    getScore(): Observable<any> {
         const params = this.createIdParam('2208286');
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score/', params));
    }

    insertScore(scoreObject) {
        return this._httpHelper.httpHelper('post', 'testScore/insertManualTestScore', null, scoreObject);
    }

    updateScore(obj) {
        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'testScore/updateManualTestScore', null, obj);
        }
    }

    deleteScore() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteManualTestScore', null, null);
    }

    createIdParam(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });
        return params;
    }

    setScoreObject(score) {
        this.scoreObject = score;
    }

    getCompositeFavoritesFlags(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/getCompositeFavoritesFlags/', null));
    }

    setCompositeFavoritesFlags( favorateObject ) {
        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', 'testScore/setCompositeFavoritesFlags', null, favorateObject)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    deleteCompositeFavoritesFlags() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteCompositeFavoritesFlags', null, null);
    }

    getServiceOfferingCompositeScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('detail-page/get-service-offering-composite/', null));
    }

    getServiceOffering(mcId): Observable<any> {
        const params = this.createMcIdParam(mcId);
        return this._http.get(this._httpHelper.getFullUrl('detail-page/get-service-offering/', params));
    }

    createMcIdParam(mcId) {
        const params = [];
        params.push({
        key: null,
        value: mcId
        });
        return params;
    }
}
