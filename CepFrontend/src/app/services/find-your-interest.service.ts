import { Injectable } from '@angular/core';
import * as cloneDeep from 'lodash/cloneDeep';
import { UserService } from 'app/services/user.service';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from 'app/services/config.service'

@Injectable( {
    providedIn: 'root'
} )
export class FindYourInterestService {

    constructor(private _user: UserService,
            private _httpHelper: HttpHelperService,
            private _configService: ConfigService) { }

    topCombinedInterestCodes;
    getTopCombinedInterestCodes() {
        if ( this.topCombinedInterestCodes == undefined ) {
            if ( typeof ( Storage ) !== "undefined" ) {
                if ( window.sessionStorage.getItem( "topCombinedInterestCodes" ) === null || window.sessionStorage.getItem( "topCombinedInterestCodes" ) == 'undefined' ) {
                    // TODO: redirect or get value from database
                } else {
                    this.topCombinedInterestCodes = JSON.parse( window.sessionStorage.getItem( 'topCombinedInterestCodes' ) );
                }
                return this.topCombinedInterestCodes;
            } else {
                // TODO: redirect or get value
            }
        } else {
            return this.topCombinedInterestCodes;
        }
    }

    setTopCombinedInterestCodes( topInterestCodes ) {
        this.topCombinedInterestCodes = topInterestCodes;

        if ( topInterestCodes === undefined ) {
            window.sessionStorage.removeItem( "topCombinedInterestCodes" );
        }

        if ( typeof ( Storage ) !== "undefined" && topInterestCodes !== undefined ) {
            window.sessionStorage.setItem( 'topCombinedInterestCodes', JSON.stringify( topInterestCodes ) );
        }
    }

    /**
     * Setup tied score object for combined tied score.
     */
    combinedTiedScoreObject;

    setCombinedTiedScoreObject( object ) {

        this.combinedTiedScoreObject = object;

        if ( object === undefined ) {
            window.sessionStorage.removeItem( "combinedTiedScoreObject" );
        }
        if ( typeof ( Storage ) !== "undefined" && object !== undefined ) {
            window.sessionStorage.setItem( 'combinedTiedScoreObject', JSON.stringify( this.combinedTiedScoreObject ) );
            this.combinedTiedScoreObject = JSON.parse( window.sessionStorage.getItem( 'combinedTiedScoreObject' ) );
        }
    }

    getCombinedTiedScoreObject() {
        if ( this.combinedTiedScoreObject == undefined ) {
            if ( typeof ( Storage ) !== "undefined" ) {
                if ( window.sessionStorage.getItem( "combinedTiedScoreObject" ) === null || window.sessionStorage.getItem( "combinedTiedScoreObject" ) == 'undefined' ) {
                    // TODO: redirect or get value from database
                } else {
                    this.combinedTiedScoreObject = JSON.parse( window.sessionStorage.getItem( 'combinedTiedScoreObject' ) );
                }
                return this.combinedTiedScoreObject;
            } else {
                // TODO: redirect or get value
            }
        } else {
            return this.combinedTiedScoreObject;
        }
    }

    topGenderInterestCodes;
    getTopGenderInterestCodes() {
        if ( this.topGenderInterestCodes == undefined ) {
            if ( typeof ( Storage ) !== "undefined" ) {
                if ( window.sessionStorage.getItem( "topGenderInterestCodes" ) === null || window.sessionStorage.getItem( "topGenderInterestCodes" ) == 'undefined' ) {
                    // TODO: redirect or get value from database
                } else {
                    this.topGenderInterestCodes = JSON.parse( window.sessionStorage.getItem( 'topGenderInterestCodes' ) );
                }
                return this.topGenderInterestCodes;
            } else {
                // TODO: redirect or get value
            }
        } else {
            return this.topGenderInterestCodes;
        }
    }

    setTopGenderInterestCodes( topInterestCodes ) {
        this.topGenderInterestCodes = topInterestCodes;

        if ( topInterestCodes === undefined ) {
            window.sessionStorage.removeItem( "topGenderInterestCodes" );
        }
        if ( typeof ( Storage ) !== "undefined" && topInterestCodes !== undefined ) {
            window.sessionStorage.setItem( 'topGenderInterestCodes', JSON.stringify( topInterestCodes ) );
        }
    }

    /**
     * Setup tied score object for gender tied score.
     */
    genderTiedScoreObject;

    setGenderTiedScoreObject( object ) {

        this.genderTiedScoreObject = object;

        if ( object === undefined ) {
            window.sessionStorage.removeItem( "genderTiedScoreObject" );
        }
        if ( typeof ( Storage ) !== "undefined" && object !== undefined ) {
            window.sessionStorage.setItem( 'genderTiedScoreObject', JSON.stringify( this.genderTiedScoreObject ) );
            this.genderTiedScoreObject = JSON.parse( window.sessionStorage.getItem( 'genderTiedScoreObject' ) );
        }
    }

    getGenderTiedScoreObject() {
        if ( this.genderTiedScoreObject == undefined ) {
            if ( typeof ( Storage ) !== "undefined" ) {
                if ( window.sessionStorage.getItem( "genderTiedScoreObject" ) === null || window.sessionStorage.getItem( "genderTiedScoreObject" ) == 'undefined' ) {
                    // TODO: redirect or get value from database
                } else {
                    this.genderTiedScoreObject = JSON.parse( window.sessionStorage.getItem( 'genderTiedScoreObject' ) );
                }
                return this.genderTiedScoreObject;
            } else {
                // TODO: redirect or get value
            }
        } else {
            return this.genderTiedScoreObject;
        }
    }

    /**
     * Used to POST test information to database.
     */
    fyiObject;

    setFyiObject( object ) {
        this.fyiObject = object;
    }
    getFyiObject() {
        return this.fyiObject;
    }

    // holds questions and answers
    questionAnswerList;
    interestLikes = 0;
    interestDislikes = 0;
    interestIndifferents = 0;
    userGender = 'M';
    genderScore;
    combinedScore;

    interestCodeRawScore = {
        rRawScore: 0,
        iRawScore: 0,
        aRawScore: 0,
        sRawScore: 0,
        eRawScore: 0,
        cRawScore: 0
    };

    // reset values
    resetValues() {
        this.interestLikes = 0;
        this.interestDislikes = 0;
        this.interestIndifferents = 0;
        this.interestCodeRawScore = {
            rRawScore: 0,
            iRawScore: 0,
            aRawScore: 0,
            sRawScore: 0,
            eRawScore: 0,
            cRawScore: 0
        };
    }

    setRawScores( rawScores ) {
        this.interestCodeRawScore = rawScores;
    }

    getCombinedScore() {
        return this.combinedScore;
    }

    getGenderScore() {
        return this.genderScore;
    }

    getInterestCodeRawScore() {
        return this.interestCodeRawScore;
    }

    getInterestLikes() {
        return this.interestLikes;
    }

    getInterestDislikes() {
        return this.interestDislikes;
    }

    getInterestIndifferents() {
        return this.interestIndifferents;
    }

    /*
     * Calculate raw score for each interest code (R, I, A, S, E, C). Like = 2
     * Indifferent = 1 Dislike = 0
     * 
     * Calculate the total number of likes, indifferents and dislikes for each
     * interest code. This is not being used.
     * 
     * Calculate the overall total number of likes, indifferents and dislikes.
     * 
     * 
     * What is tied score and how is it calculated?
     * 
     * How is Combined Score calculated?
     * 
     * How is Gender Score calculated?
     */
    calculateRawScore() {

        var len = this.questionAnswerList.length;

        // loop through answer object
        for ( var i = 0; i < len; i++ ) {
            var interestCd = this.questionAnswerList[i].interestCd;
            var answer = this.questionAnswerList[i].answer;
            var isSeeded = this.questionAnswerList[i].isSeeded;

            // if seeded, continue to the next item
			if (isSeeded) {continue;}

            if ( answer !== undefined && answer == 2) {
                // track likes
                this.interestLikes++;

                // add 2 to raw score for likes per interest
                // code
                switch ( interestCd ) {
                    case "R":
                        this.interestCodeRawScore.rRawScore = this.interestCodeRawScore.rRawScore + 2;
                        break;
                    case "I":
                        this.interestCodeRawScore.iRawScore = this.interestCodeRawScore.iRawScore + 2;
                        break;
                    case "A":
                        this.interestCodeRawScore.aRawScore = this.interestCodeRawScore.aRawScore + 2;
                        break;
                    case "S":
                        this.interestCodeRawScore.sRawScore = this.interestCodeRawScore.sRawScore + 2;
                        break;
                    case "E":
                        this.interestCodeRawScore.eRawScore = this.interestCodeRawScore.eRawScore + 2;
                        break;
                    case "C":
                        this.interestCodeRawScore.cRawScore = this.interestCodeRawScore.cRawScore + 2;
                        break;
                }

            } else if ( answer !== undefined && answer == 1 ) {
                // track indifferents
                this.interestIndifferents++;

                // add 1 to raw score for dislikes per interest
                // code
                switch ( interestCd ) {
                    case "R":
                        this.interestCodeRawScore.rRawScore++;
                        break;
                    case "I":
                        this.interestCodeRawScore.iRawScore++;
                        break;
                    case "A":
                        this.interestCodeRawScore.aRawScore++;
                        break;
                    case "S":
                        this.interestCodeRawScore.sRawScore++;
                        break;
                    case "E":
                        this.interestCodeRawScore.eRawScore++;
                        break;
                    case "C":
                        this.interestCodeRawScore.cRawScore++;
                        break;
                }
            } else {
                // track dislikes
                this.interestDislikes++;
            }
        }
    }

    /**
     * Returns sorted combined score list in desc order.
     */
    getSortedCombinedScore( scores ) {
        var combinedScoreSort = [];
        for ( var i = 0; i < scores.length; i++ ) {
            combinedScoreSort.push( scores[i] );
        }
        combinedScoreSort.sort( function( a, b ) {
            return b.combinedScore - a.combinedScore
        } );
        return combinedScoreSort;
    }

    /**
     * Returns sorted gender score list in desc order.
     */
    getSortedGenderScore( scores ) {

        var genderScoreSort = [];
        for ( var i = 0; i < scores.length; i++ ) {
            genderScoreSort.push( scores[i] );
        }
        genderScoreSort.sort( function( a, b ) {
            return b.genderScore - a.genderScore
        } );
        return genderScoreSort;
    }

    /**
     * Check to see if there are tied scores that requires the user's input.
     */
    isTiedScore( selectedScoreList, scoreType ) {
        var selectedScoreListCopy = cloneDeep( selectedScoreList );
        var numberOfSlotsLeft = 3;
        var len = selectedScoreListCopy.length;
        var tiedScoreList = [];

        for ( var i = 0; i < len; i = i + tiedScoreList.length ) {
            tiedScoreList = [];

            for ( var k = 0 + i; k < len; k++ ) {

                if ( scoreType == 'gender' ) {
                    if ( selectedScoreListCopy[i].genderScore == selectedScoreListCopy[k].genderScore ) {
                        tiedScoreList.push( selectedScoreListCopy[k] );
                    }
                } else {
                    if ( selectedScoreListCopy[i].combinedScore == selectedScoreListCopy[k].combinedScore ) {
                        tiedScoreList.push( selectedScoreListCopy[k] );
                    }
                }
            }

            // if number of slots left is less than 0 then user needs to choose
            // tied score
            if ( numberOfSlotsLeft - tiedScoreList.length < 0 ) {

                // setup checkbox input
                for ( var i = 0; i < len; i++ ) {
                    for ( var k = 0; k < tiedScoreList.length; k++ ) {
                        if ( selectedScoreListCopy[i].interestCd == tiedScoreList[k].interestCd ) {

                            selectedScoreListCopy[i].checkbox = true;
                        }

                    }
                }

                var tiedScoreObject = {
                    numSlotsLeft: numberOfSlotsLeft,
                    scoreList: selectedScoreListCopy,
                    scoreType: scoreType,
                    numTiedScore: tiedScoreList.length
                };

                if ( scoreType == 'gender' ) {
                    this.setGenderTiedScoreObject( tiedScoreObject );
                } else {
                    this.setCombinedTiedScoreObject( tiedScoreObject );
                }

                return true;
            }

            // track number of slots left
            numberOfSlotsLeft = numberOfSlotsLeft - tiedScoreList.length;

            // if number of slots is 0 then dont need input from user
            if ( numberOfSlotsLeft == 0 ) {
                return false;
            }
        }

        // if number of slots left is less than zero then needs input from user
        // return numberOfSlotsLeft
    }

    /**
     * Track if tied link is clicked.
     */
    isTiedLinkClicked = false;
    setTiedLinkClicked( isTiedLinkClicked ) {
        this.isTiedLinkClicked = isTiedLinkClicked;
    }
    getTiedLinkClicked() {
        return this.isTiedLinkClicked;
    }
    
    numberTestTaken = undefined;

    getNumberOfTestTaken = function() {
        //var deferred = $q.defer();
        
        var promise = new Promise(( resolve, reject ) => {

        if (typeof (Storage) !== "undefined") {

            if (window.sessionStorage.getItem("numberTestTaken") === null || window.sessionStorage.getItem("numberTestTaken") == 'undefined') {
                this._httpHelper.httpHelper( 'GET', 'fyi/NumberTestTaken', null, null ).then( data => {
                    window.sessionStorage.setItem('numberTestTaken', data);
                    this.numberTestTaken = data;
                    resolve(this.numberTestTaken);
                }, status => {
                    reject("Error: request returned status " + status);
                });
            } else {
                this.numberTestTaken = window.sessionStorage.getItem('numberTestTaken');
                resolve(this.numberTestTaken);
                return promise;

            }
        } else {
            // for browser that don't support session storage
            this._httpHelper.httpHelper( 'GET', 'fyi/NumberTestTaken', null, null ).then( data => {
                this.numberTestTaken = data;
                resolve(this.numberTestTaken);
            }, status => {
                reject("Error: request returned status " + status);
            });
        }
        });
        return promise;
    }
 
    
    getTestScoreByAccessCode = function (accessCode) {

        const fullUrl = this._configService.getRestUrl() + '/getTestScoreByAccessCode/' + accessCode + '/';
        
        return this._httpHelper.httpHelper('get', fullUrl, null, null, true );
      }
}
