import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ContactUsService  {
  resourceTopics = undefined;
  quickLinkTopics = undefined;

  constructor(
    private _httpHelper: HttpHelperService,
    private _congfigService: ConfigService,
    private _http: HttpClient
  ) { }


  emailUs(emailObject)  {
    const fullUrl = this._congfigService.getAwsFullUrl(`/api/contactus/email/`);
    return this._httpHelper.httpHelper('post', fullUrl, null, emailObject, true);
  }

  scheduleEmail(emailObject) {
    return this._httpHelper.httpHelper('post', 'contactUs/scheduleEmail', null, emailObject);
  }

  shareWithCounselor(emailObject) {
    return this._httpHelper.httpHelper('post', 'contactUs/shareWithCounselor', null, emailObject);
  }

  scoreRequest(emailObject)  {
    const fullUrl = this._congfigService.getAwsFullUrl(`/api/ContactUs/scoreRequest`);
    return this._httpHelper.httpHelper('post', fullUrl, null, emailObject, true);
  }

}
