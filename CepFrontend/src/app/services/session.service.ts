// Logic moved to user service due to the tight coupling of the classes

// import { Injectable } from '@angular/core';
// import { Router } from '@angular/router';
// import { UserService } from 'app/services/user.service';
// import { MatDialog } from '@angular/material';
// import { SessionKeepaliveDialogComponent } from 'app/core/dialogs/session-keepalive-dialog/session-keepalive-dialog.component';
// import { ConfigService } from './config.service';

// @Injectable({
//   providedIn: 'root'
// })
// export class SessionService {

//   existingTimer: any;

//   constructor(
//     private _userService: UserService,
//     private _dialog: MatDialog,
//     private _router: Router,
//     private _configService: ConfigService
//   ) { }

//   startSession() {
//     if (!this.existingTimer) {
//       this.sessionKeepAlive();
//     } else {
//       clearTimeout(this.existingTimer);
//       this.existingTimer = undefined;
//       this.sessionKeepAlive();
//     }
//   }

//   /**
//    * Starts a new session; opens a session timeout dialog when the duration timeout value
//    * in the config service has been reached;  
//    */
//   sessionKeepAlive() {
//     let self = this;
//     this.existingTimer = function () {
//       let data = {
//         title: 'Session Expiration',
//         message: 'Your session is about to expire. What would you like to do?',
//       };

//       self._dialog.open(SessionKeepaliveDialogComponent, { data: data }).afterClosed().subscribe(result => {
//         if (result) {
//           if (result === 'logoff') {
//             self.existingTimer = undefined;
//             self._userService.logOff();
//           } else if (result === 'keepalive') {
//             self.existingTimer = undefined;
//             self.startSession();
//           }
//         }
//       });

//       self.existingTimer = undefined;
//     };

//     setTimeout(this.existingTimer, this._configService.getDurationToTimeoutReminder());

//   }

//   stopSession() {
//     clearTimeout(this.existingTimer);
//   }

// }
