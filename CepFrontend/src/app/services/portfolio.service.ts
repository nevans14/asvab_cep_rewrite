import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from './http-helper.service';
import { UserService } from 'app/services/user.service';
import { of, Observable } from 'rxjs';
// import { resolve } from 'q';

// const headers = new HttpHeaders()
// .set('Accept', 'application/json')
// .set('Content-Type', 'application/json');

@Injectable({
    providedIn: 'root'
})
export class PortfolioService {

    constructor(
        private _http: HttpClient,
        private _userService: UserService,
        private _httpHelper: HttpHelperService

    ) { }

    getUserSystem(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-user-system/', null));
    }

    isPortfolioStarted(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/PortfolioStarted/', null));
    }

    /**
       * Work Experience
      */
    getWorkExperience(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/WorkExperience/', null));
    }

    insertWorkExperience(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertWorkExperience', null, obj);
        }
    }

    updateWorkExperience(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateWorkExperience', null, obj);
        }
    }

    deleteWorkExperience(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteWorkExperience/', params, null);
    }

    /**
      * Education
     */
    getEducation(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Education/', null));
    }

    insertEducation(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertEducation', null, obj);
        }
    }

    updateEducation(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateEducation', null, obj);
        }
    }

    deleteEducation(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteEducation/', params, null);
    }

    /**
      * Achievements
     */
    getAchievements(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Achievement/', null));
    }

    insertAchievement(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertAchievement', null, obj);
        }
    }

    updateAchievement(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateAchievement', null, obj);
        }
    }

    deleteAchievement(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteAchievement/', params, null);
    }

    /**
       * Interests
      */
    getInterests(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Interest/', null));
    }

    insertInterest(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertInterest', null, obj);
        }
    }

    updateInterest(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateInterest', null, obj);
        }
    }

    deleteInterest(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteInterest/', params, null);
    }

    /**
       *Skills
      */
    getSkill(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Skill/', null));
    }

    insertSkill(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertSkill', null, obj);
        }
    }

    updateSkill(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateSkill', null, obj);
        }
    }

    deleteSkill(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteSkill/', params, null);
    }

    /**
       * Work values
      */
    getWorkValues(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/WorkValue/', null));
    }

    insertWorkValue(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertWorkValue', null, obj);
        }
    }

    updateWorkValue(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateWorkValue', null, obj);
        }
    }

    deleteWorkValue(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteWorkValue/', params, null);
    }

    /**
       * Volunteer values
      */
    getVolunteers(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Volunteer/', null));
    }

    insertVolunteer(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertVolunteer', null, obj);
        }
    }

    updateVolunteer(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateVolunteer', null, obj);
        }
    }

    deleteVolunteer(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteVolunteer/', params, null);
    }

    /**
       * ACT score  values
      */
    getActScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/ACT/', null));
    }

    insertActScore(obj) {

        if (this._userService.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertACT', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateActScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateACT', null, obj);
        }
    }

    deleteActScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteACT/', params, null);
    }

    /**
       * ASVAB score  values
      */
    getAsvabScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/ASVAB/', null));
    }

    insertAsvabScore(obj) {

        if (this._userService.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertASVAB', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateAsvabScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateASVAB', null, obj);
        }
    }

    deleteAsvabScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteASVAB/', params, null);
    }

    /**
       * FYI score  values
      */
    getFyiScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/FYI/', null));
    }

    insertFyiScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertFYI', null, obj);
        }
    }

    updateFyiScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateFYI', null, obj);
        }
    }

    deleteFyiScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteFYI/', params, null);
    }

    /**
       * SAT score  values
      */
    getSatScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/SAT/', null));
    }

    insertSatScore(obj) {

        if (this._userService.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertSAT', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateSatScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateSAT', null, obj);
        }
    }

    deleteSatScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteSAT/', params, null);
    }

    /**
       * Other score  values
      */
    getOtherScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Other/', null));
    }

    insertOtherScore(obj) {

        if (this._userService.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertOther', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateOtherScore(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateOther', null, obj);
        }
    }

    deleteOtherScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteOther', params, null);
    }

    /**
       *  Plan of action
      */
    getPlan(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Plan/', null));
    }

    insertPlanNew() {
        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/', null, null);
        }
    }

    insertPlan(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertPlan', null, obj);
        }
    }

    updatePlan(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdatePlan', null, obj);
        }
    }

    deletePlan(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/Plan', params, null);
    }

    selectNameGrade(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/SelectNameGrade/', null));
    }

    updateInsertUserInfo(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateInsertUserInfo', null, obj);
        }
    }

    getUserInfo(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-user-info/', null));
    }

    /**
       *  Physical Fitness
      */
    getPhysicalFitness(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/PhysicalFitness/', null));
    }

    insertPhysicalFitness(obj) {

        if (this._userService.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertPhysicalFitness', null, obj)
                    .then((cfgObj) => {
                        console.log(cfgObj);
                        resolve(cfgObj);
                    });
            });
        }
    }

    updatePhysicalFitness(obj) {

        if (this._userService.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdatePhysicalFitness', null, obj);
        }
    }

    deletePhysicalFitness(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeletePhysicalFitness', params, null);
    }
    //
    getScoreSummary(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score/', null));
    }

    getAccessCode(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-access-code/', null));
    }

    getScoreSummarybyAccessCode(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score-access-code/', null));
    }

    getCepFavoriteOccupations(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/getFavoriteOccupation/', null));
    }

    getRelatedCareerList(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-related-career-list/', null));
    }

    getResourceSpecific(): Observable<any> {

        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-specific-resource', null));
    }

    getResourceGeneric(): Observable<any> {

        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-generic-resource', null));
    }

    submit(formData) {
        const endPoint = 'portfolio/submit';

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', endPoint, null, formData)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    sendComment(formData) {
        const endPoint = 'portfolio/comments';

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', endPoint, null, formData)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    updateCommentByHasBeenRead(commentId, id, hasBeenRead) {
        const endPoint = 'portfolio/comments/' + commentId + '/recipients/' + id + '/read/' + hasBeenRead;

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('put', endPoint, null, null)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    getPortfolio() {
        const endPoint = 'portfolio/';

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('get', endPoint, null, null)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    getPortfolioComments() {
        const endPoint = 'portfolio/comments';

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('get', endPoint, null, null)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    getPortfolioUnreadComments() {
        const endPoint = 'portfolio/unread-comments';

        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('get', endPoint, null, null)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    // /logs/{id}/read/{hasBeenRead}

    markPortfolioLogAsRead(id, hasBeenRead) {

        const endPoint = 'portfolio/logs/' + id + '/read/' + hasBeenRead;
        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('put', endPoint, null, null)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

}
