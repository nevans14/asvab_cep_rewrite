import { TestBed } from '@angular/core/testing';

import { FyiRestFactoryService } from './fyi-rest-factory-service';

describe('FyiRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FyiRestFactoryService = TestBed.get(FyiRestFactoryService);
    expect(service).toBeTruthy();
  });
});
