import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpHelperService } from './http-helper.service';

@Injectable({
  providedIn: 'root'
})
export class MilitaryPlanService {

  private controllerName: string = "plan-your-future";

  constructor(private _httpHelper: HttpHelperService,
    private _configService: ConfigService) { }

  save(planId, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/military-plans';

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('post', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  update(planId, id, formData) {
    const endPoint = this.controllerName + '/plans/' + planId + '/military-plans/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('put', endPoint, null, formData)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  get(planId) {
    const endPoint = this.controllerName + '/plans/' + planId + '/military-plans';
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  delete(planId, id) {
    const endPoint = this.controllerName + '/plans/' + planId + '/military-plans/' + id;

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }

  getServiceOffering(mcId) {
    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-service-offering/' + mcId + '/');

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getArmyDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-army-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getNavyDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-navy-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getAirForceDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-air-force-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getSpaceForceDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-space-force-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getCoastGuardDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-coast-guard-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }

  getMarineDetail(mcId, mocId) {

    const endPoint = this._configService.getAwsFullUrl('/api/detail-page/get-marine-detail/' + mcId + '/' + mocId + "/");

    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });

  }


  deleteUserMilitaryPlanCareer(militaryPlanId, id) {
    const endPoint = this.controllerName + '/plans/military-plans/' + militaryPlanId + '/careers/' + id;
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }


  deleteMilitaryPlanServiceCollege(militaryPlanId, id) {
    const endPoint = this.controllerName + '/plans/military-plans/' + militaryPlanId + '/service-colleges/' + id;
    return new Promise((resolve, reject) => {
      this._httpHelper.httpHelper('delete', endPoint, null, null)
        .then((obj) => {
          resolve(obj);
        }).catch(error => {
          reject(error);
        });
    });
  }


}
