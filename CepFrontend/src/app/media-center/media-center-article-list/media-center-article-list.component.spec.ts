import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaCenterArticleListComponent } from './media-center-article-list.component';

describe('MediaCenterArticleListComponent', () => {
  let component: MediaCenterArticleListComponent;
  let fixture: ComponentFixture<MediaCenterArticleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaCenterArticleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaCenterArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
