import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-media-center-article-list',
  templateUrl: './media-center-article-list.component.html',
  styleUrls: ['./media-center-article-list.component.scss']
})
export class MediaCenterArticleListComponent implements OnInit {

  mediaCenterList: any;
  articleList: any;
  title: any;
  description: any;
  categoryTitle: string;
  totalItems: any;
  currentPage: number;
  maxSize: number;
  itemsPerPage: number;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.mediaCenterList = this._activatedRoute.snapshot.data.mediaCenter;

    this.articleList = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId == this._activatedRoute.snapshot.params.id);

    //set page title
    this.categoryTitle = this.getCategoryTitle(this._activatedRoute.snapshot.params.id);
    this.title = this.getTitle(this._activatedRoute.snapshot.params.id);
    this.description = this.getDescription(this._activatedRoute.snapshot.params.id);

    //setup pagination
    this.totalItems = this.articleList.length;
    this.currentPage = 1;
    this.maxSize = 4;
    this.itemsPerPage = 10;

  }

  getCategoryTitle(id): string {

    switch (id) {
      case '1': {
        return "Student Articles";
      }
      case '2': {
        return "Parent Articles";
      }
      case '4': {
        return "Counselor Articles";
      }
      default: {
        return "Tutorials";
      }
    }

  }

  getTitle(id): string {

    switch (id) {
      case 1: {
        return "ASVAB CEP Resources for Students";
      }
      case 2: {
        return "ASVAB CEP Resources for Parents";
      }
      case 4: {
        return "ASVAB CEP Resources for Educators";
      }
      default: {
        return "Tutorials";
      }
    }

  }

  getDescription(id): string {

    switch (id) {
      case 1: {
        return "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to assist students in career planning.";
      }
      case 2: {
        return "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help parents help their children plan for the future.";
      }
      case 4: {
        return "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help school counselors advise students on career planning.";
      }
      default: {
        return "The ASVAB Career Exploration Program offers video tutorials to help students understand scores, find their interest, and explore careers in the OCCU-Find.";
      }
    }

  }

}
