import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { ConfigService } from 'app/services/config.service';
import { MediaCenterService } from 'app/services/media-center.service';

declare var twttr: any;
@Component({
  selector: 'app-media-center',
  templateUrl: './media-center.component.html',
  styleUrls: ['./media-center.component.scss']
})
export class MediaCenterComponent implements OnInit {

  resource = this._configService.getImageUrl();
  resources = this.resource + 'static/';
  documents = this.resource + this._configService.getDocumentFolder();

  mediaCenterList: any;
  studentNewsTopArticle: any;
  studentNewsMediaCenterArticles: any;
  counselorArticlesTopArticle: any;
  counselorArticlesMediaCenterArticles: any;
  parentArticlesTopArticle: any;
  parentArticlesMediaCenterArticles: any;
  tutorialsMediaCenterArticles: any;
  inTheNewsMediaCenterArticles: any;
  testimonialsMediaCenterArticles: any;
  sharablesMediaCenterArticles: any;

  /***
 * Resource Types enums
 */
  resourceType = {
    'Document': 1,
    'Video': 2,
    'Link': 3,
    'ExtLink': 4,
    'PopupVideo': 5
  };
  quickLinkType = {
    'Document': 1,
    'Link': 2,
    'JavaScript': 3
  };

  constructor(private _activatedRoute: ActivatedRoute,
    private _configService: ConfigService,
    private _mediaCenterService: MediaCenterService,
    private _meta: Meta,
    private _titleTag: Title,
  ) { }

  ngOnInit() {

    this.mediaCenterList = this._activatedRoute.snapshot.data.mediaCenter;

    if (this.mediaCenterList) {

      //set student news articles
      this.studentNewsTopArticle = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 1)[0];
      this.studentNewsMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 1).slice(1, 4);

      //set counselor articles
      this.counselorArticlesTopArticle = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 4)[0];
      this.counselorArticlesMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 4).slice(1, 4);

      //set partent articles
      this.parentArticlesTopArticle = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 2)[0];
      this.parentArticlesMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 2).slice(1, 4);

      //set tutorials
      this.tutorialsMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 5);
      //re-order tutorials so understanding your asvab results appears first
      this.tutorialsMediaCenterArticles.push(this.tutorialsMediaCenterArticles.splice(0, 1)[0]);

      //set in the news
      // Need to retrieve from DB due to external url not being included
      this._mediaCenterService.getMediaCenterByCategoryId(7).then(
        data => {
          this.inTheNewsMediaCenterArticles = data;
          this.inTheNewsMediaCenterArticles = this.inTheNewsMediaCenterArticles.sort(this.compareValues('positionId'));
        });

      //set testimonials
      this.testimonialsMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 8);

      //set shareables
      // Need to retrieve from DB due to shareable url not being included
      this._mediaCenterService.getMediaCenterByCategoryId(9).then(
        data => {
          this.sharablesMediaCenterArticles = data;
        });
    }

    const seoTitle = 'Blog and News | ASVAB Career Exploration Program';
    const metaDescription = 'Find the latest blogs, news, media, and communications about the ASVAB Career Exploration Program for students, counselors, and parents.';

    this._titleTag.setTitle(seoTitle);
    this._meta.updateTag({name: 'description', content: metaDescription});

    this._meta.updateTag({property: 'og:title', content: seoTitle});
    this._meta.updateTag({property: 'og:description', content: metaDescription});

    this._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
    this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
  }

  ngAfterViewInit(): void {
    twttr.widgets.load();
  }

  /* ngx slick carousel config */
  slideConfig = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }, {
        breakpoint: 639,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  /**
 * returns a sorted array based on the key and order passed
 * @param key 
 * @param order can be 'asc' or 'desc'; 'asc' is by default
 */
  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0;  //property doesn't exist

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if ((typeof varA === 'string') && (typeof varB === 'string')) {
        comparison = varA.localeCompare(varB);
      } else {
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
      }

      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

}
