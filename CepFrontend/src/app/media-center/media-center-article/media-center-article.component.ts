import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { UtilityService } from 'app/services/utility.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
@Component({
  selector: 'app-media-center-article',
  templateUrl: './media-center-article.component.html',
  styleUrls: ['./media-center-article.component.scss']
})
export class MediaCenterArticleComponent implements OnInit {

  article: any;
  mediaCenterList: any;
  studentMediaCenterArticles: any;
  counselorMediaCenterArticles: any;
  parentMediaCenterArticles: any;
  tutorialMediaCenterArticles: any;
  shareImgUrl: any;
  thumbnailUrl: any;
  absUrl: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _utility: UtilityService,
    private _dialog: MatDialog,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService,
  ) {
    this.shareImgUrl = '';
  }

  ngOnInit() {

    this._activatedRoute.data.subscribe(data => {
        this.article = data.article;

        if(this.article){
          this.thumbnailUrl = this.article.thumbnailUrl.replace(/(\n|\r)/gm, '');
          this.shareImgUrl = this.article.shareImgUrl.replace(/(\n|\r)/gm,'');
          	
          // get secured urls
          this.thumbnailUrl = this._utility.getSecuredUrl(this.thumbnailUrl);
          this.shareImgUrl = this._utility.getSecuredUrl(this.shareImgUrl);

          this.updateMeta();
        }
      });

    this.mediaCenterList = this._activatedRoute.snapshot.data.mediaCenter;

    //set student news articles
    this.studentMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 1).slice(0, 3);

    //set counselor articles
    this.counselorMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 4).slice(0, 3);

    //set parent articles
    this.parentMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 2).slice(0, 3);

    //set tutorials
    this.tutorialMediaCenterArticles = this.mediaCenterList.filter(mediaCenterItem => mediaCenterItem.categoryId === 5).slice(0, 3);

    this.absUrl = this._utility.getCanonicalUrl();
    
    this.updateMeta();

  }

  updateMeta() {
    this._titleTag.setTitle(this.article.seoTitle ? this.article.seoTitle : this.article.title);
    this._meta.updateTag({name: 'description', content: this.article.metaDescription ? this.article.metaDescription : this.article.articleDescription});
    this._meta.updateTag({name: 'keywords', content: this.article.keywords});

    this._meta.updateTag({property: 'og:title', content: this.article.seoTitle ? this.article.seoTitle : this.article.title});
    this._meta.updateTag({property: 'og:description', content: this.article.metaDescription ? this.article.metaDescription : this.article.articleDescription});
    this._meta.updateTag({property: 'og:image', content: this.shareImgUrl});
    this._meta.updateTag({property: 'og:url', content: this.absUrl});
    this._meta.updateTag({property: 'og:type', content: 'article'});

    this._meta.updateTag({ name: 'twitter:site', content: '@@ASVABCEP'});
    this._meta.updateTag({ name: 'twitter:creator', content: '@ASVABCEP'});
    this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.updateTag({ name: 'twitter:title', content: this.article.seoTitle ? this.article.seoTitle : this.article.title});
    this._meta.updateTag({ name: 'twitter:image', content: this.shareImgUrl});
    this._meta.updateTag({ name: 'twitter:description', content: this.article.metaDescription ? this.article.metaDescription : this.article.articleDescription});
  }

  showAsvabModal() {
    const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
      hasBackdrop: true,
      panelClass: 'new-modal',
      // height: '735px',
      autoFocus: false,
      disableClose: true
    });
  }

}
