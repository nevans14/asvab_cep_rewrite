import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaCenterArticleComponent } from './media-center-article.component';

describe('MediaCenterArticleComponent', () => {
  let component: MediaCenterArticleComponent;
  let fixture: ComponentFixture<MediaCenterArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaCenterArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaCenterArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
