import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { ContentManagementService } from 'app/services/content-management.service';
import { MediaCenterService } from 'app/services/media-center.service';
import { UtilityService } from 'app/services/utility.service';
import { ResourceService } from '../services/resource.service';
import { ConfigService } from 'app/services/config.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { RegisterDialogComponent } from 'app/core/dialogs/register-dialog/register-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
@Component({
  selector: 'app-parents',
  templateUrl: './parents.component.html',
  styleUrls: ['./parents.component.scss']
})
export class ParentsComponent implements OnInit {
  // Current page from the DB
  pageHtml = [];
  parents = true;
  parentsTest = false;
  parentsPostTest = false;
  parentsInformation = false;
  path;
  mediaCenterList;
  showParents = false;
  showParentsInformation = false;
  showParentsTest = false;
  showParentsPostTest = false;
  page;
  resourceTopics;
  resource;
  resources;
  documents;

  slideConfig = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 639,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
      ]
  };

  slideConfigResources = {
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }, 
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
      ]
  };

  slideConfigTestimonials = {
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  resourceType = {
    'Document': 1,
    'Video': 2,
    'Link': 3,
    'ExtLink': 4,
    'PopupVideo': 5
  };

  constructor(
    private _contentManagementService: ContentManagementService,
    private _router: Router,
    private _mediaCenterService: MediaCenterService,
    private _utility: UtilityService,
    private _meta: Meta,
    private _titleTag: Title,
    private _resourceService: ResourceService,
    private _dialog: MatDialog,
    private _config: ConfigService,
    private _activatedRoute: ActivatedRoute,
    private _googleAnalyticsService: GoogleAnalyticsService,
  ) { }

  ngOnInit() {
    this.path = this._router.url;

    this.setSelectedTab(this.path);
    // this.getPageByName(this.path);

    this._mediaCenterService.getMediaCenterList().then(
      data => {
          this.mediaCenterList = data;
      });

    switch (this.path) {
      case '/parents':
        this.showParents = true;
        break;
      case '/parents-test':
        this.showParentsTest = true;
        break;
      case '/parents-post-test':
        this.showParentsPostTest = true;
        break;
      case '/parents-information':
        this.showParentsInformation = true;
        break;
    }

    this.pageHtml = this._contentManagementService.getPageByName(this.path);

    this.resource = this._config.getImageUrl();
    this.resources = this.resource + 'static/';
    this.documents = this.resource + 'CEP_PDF_Contents/';

    this._resourceService.getResources().subscribe(
      data => {
          this.resourceTopics = data;
      }
    );
  }

  slickInit( e ) {
    console.debug( 'slick initialized' );
  }

  breakpoint( e ) {
      console.debug( 'breakpoint' );
  }

  afterChange( e ) {
      console.debug( 'afterChange' );
  }

  beforeChange( e ) {
      console.debug( 'beforeChange' );
  }

  redirect(path) {
    this.setSelectedTab(path);
    this._router.navigate([path]);
  }

  setSelectedTab(path) {
    if (path.indexOf('parents-test') > -1) {
      this.parents = false;
      this.parentsTest = true;
      this.parentsPostTest = false;
      this.parentsInformation = false;
    } else if (path.indexOf('parents-post-test') > -1) {
      this.parents = false;
      this.parentsTest = false;
      this.parentsPostTest = true;
      this.parentsInformation = false;
    } else if (path.indexOf('parents-information') > -1) {
      this.parents = false;
      this.parentsTest = false;
      this.parentsPostTest = false;
      this.parentsInformation = true;
    } else if (path.indexOf('parents') > -1) {
      this.parents = true;
      this.parentsTest = false;
      this.parentsPostTest = false;
      this.parentsInformation = false;
    }
  }

  openRegistrtionModal() {
    this._dialog.open(RegisterDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      height: '95%',
      autoFocus: true,
      disableClose: true
    });
  }

  snapChatModal() {
    this._googleAnalyticsService.trackClick('#CONNECT_WITH_US_SNAPCHAT')

    var data = {
        title: 'Connect with us on SnapChat!',
        message: '<p class="text-center"><img style="width: 400px;" src="assets/images/option-ready-snap.png" alt=""/></p>'
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '500px'
    });
  }
}
