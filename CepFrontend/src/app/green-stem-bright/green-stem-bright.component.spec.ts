import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenStemBrightComponent } from './green-stem-bright.component';

describe('GreenStemBrightComponent', () => {
  let component: GreenStemBrightComponent;
  let fixture: ComponentFixture<GreenStemBrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenStemBrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenStemBrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
