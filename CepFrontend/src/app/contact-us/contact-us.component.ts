import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ContactUsService } from '../services/contact-us.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';

import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { ScoreRequestComponent } from './score-request.component';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ReCaptcha2Component } from 'ngx-captcha';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  // Links within below html strings
  optInLink = 'https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_In_Letter.docx';
  optOutLink = 'https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_Out_Letter.docx';
  CITMLink = 'https://www.careersinthemilitary.com';
  asvabprogramLink = 'https://www.asvabprogram.com';
  asvbprogramStudentLink = 'https://www.asvabprogram.com/student-program';
  CITMCompositeScoreLink = 'https://www.careersinthemilitary.com/composite-scores';
  CITMOptionsLink = 'https://www.careersinthemilitary.com/options';
  contactRecruiter = 'https://www.careersinthemilitary.com/contact-us';
  officialasvabLink = 'http://www.officialasvab.com/faq_app.htm';
  mepcomLink = 'http://www.mepcom.army.mil/Units/';
  march2successLink = 'http://www.march2success.com';
  todaymilitaryLink = 'https://todaysmilitary.com/joining/asvab-test-sample-questions';

  about = [ {
    title : 'What is the ASVAB? ',
    description : 'The ASVAB is the Armed Services Vocational Aptitude Battery. It is an aptitude test that measures developed abilities and helps predict future academic and occupational success. '
  }, {
    title : 'What does ASVAB CEP stand for? ',
    description : 'Armed Services Vocational Aptitude Battery (ASVAB) Career Exploration Program (CEP). Pronounced "as-vab c.e.p."'
  }, {
    title : 'What is the purpose of the ASVAB CEP? ',
    description : 'The Department of Defense sponsors the ASVAB CEP with a two-part mission, to provide: a career exploration service to U.S. youth and qualified leads to military recruiters. The ASVAB CEP offers students a chance to explore all paths to careers - college, certifications, apprenticeships, licensure programs, and the Military - in one place.'
  }, {
    title : 'When did the ASVAB CEP begin? ',
    description : 'The ASVAB has been used in the Student Testing Program (STP) since 1968. The Career Exploration Program (CEP) component was added in 1992.'
  }, {
    title : 'What is the difference between ASVAB CEP and the enlistment ASVAB? ',
    description : 'The ASVAB CEP is a career planning and exploration program offered free to participating schools and students. ' +
    'The enlistment version of the ASVAB is primarily given at a Military Entrance Processing Station (MEPS) and is used for recruiting purposes only. '
  }, {
    title : 'If I take the ASVAB, will recruiters contact me? ',
    description : 'Military and college recruiters obtain student directory information from multiple sources to identify high-quality talent. Students may be contacted by college and military recruiters regardless of ASVAB CEP participation or the score release option your school chooses.<br><br>' +
    'Schools determine if ASVAB results are released to the Military. However, you can <a href="' + this.optInLink +
    '">opt in</a> to or <a href="' + this.optOutLink +
    '">opt out</a> of the release option your school chooses. Be sure to coordinate with your school counselor. Regardless, students participating in the ASVAB Career Exploration Program have no obligation to the Military or to talk to a military recruiter.'
  }, {
    title : 'Is the ASVAB CEP available outside of the United States? ',
    description : 'The ASVAB CEP is administered to high school and post-secondary students across the United States, Puerto Rico, Guam and the Marianas, and Department of Defense Education Activities (DODEA) overseas. The program is not available in other areas.'
  }, {
    title : 'What is the ASVAB CEP connection to the Military? ',
    description : 'ASVAB CEP test sessions are administered by DoD civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Also, 11th and 12th grade student ASVAB CEP scores are valid for enlistment for two years after test date. Students interested in military service can explore their career options at <a href="' + this.CITMLink + '">careersinthemilitary.com</a>.'
  } ];

  testing = [ {
    title : 'When is my high school testing? ',
    description : 'Contact your school counselor to find out when the ASVAB CEP is coming to your school. If it\'s not already scheduled, tell your counselor you\'re interested. <a (click)="asvabModal()">BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
  }, {
    title : 'Can reasonable accommodations be requested for the ASVAB CEP? ',
    description : 'Accommodations are provided for in the ASVAB CEP, including reading the test aloud, extra time, and enlarged print tests. However, all testing completed using an accommodation is coded Option 7 (Invalid for enlistment purposes). Official ASVAB testing sites (Military Entrance Processing Stations and Military Entrance Test sites) do not provide accommodations during testing.'
  }, {
    title : 'Can students who are homeschooled take the ASVAB CEP? ',
    description : 'The ASVAB CEP is offered in schools. If affiliated with a Homeschool Association or Network that can provide a testing facility, we can provide the ASVAB CEP to the group. If not affiliated with an Association or Network, you can contact a local Education Services Specialist at 1-800-323-0513 to find out if area schools provide the program. If they do, you can make arrangements with the school counselor to participate. There is no individual administration of the program.'
  }, {
    title : 'How do I request the ASVAB CEP for my school? ',
    description : 'To contact the nearest office, please dial 1-800-323-0513. Your call will automatically be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area. As an alternative, <a (click)="asvabModal()">BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
  }, {
    title : 'How do I sign up for the ASVAB CEP? ',
    description : 'High school and post-secondary students across the United States can participate in the ASVAB CEP. Testing schedules are based on school requests rather than individual requests. Your school counselor or our local representative will be able to provide information on school testing in your area. To contact the nearest office, please dial 1-800-323-0513. You will be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. (Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area.) As an alternative, <a (click)="asvabModal()"> BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
  } ];

  score = [ {
    title : 'I took the test at my high school, when do I get my scores? ',
    description : 'High schools tests are scored within two weeks of testing and returned to the school counselor with career exploration materials. You will receive your scores from your school counselor or an ASVAB Career Exploration Program Education Services Specialist. If you haven\'t received your scores, first check with your counselor for assistance. If your school counselor has not received the scores after 30 days, <a (click)=\'openScoreRequestModal()\'>click here to request your scores.</a>'
  }, {
    title : 'I took the test in high school three years ago, can you send me my scores? ',
    description : 'We maintain ASVAB test scores for two years after the date of testing. If you took the test within the last two years, we can retrieve your scores using your name, school name, school city/state, and estimated test date (month/year). <a (click)=\'openScoreRequestModal()\'>Click here to request your scores.</a>\n' +
    '<br><br>If you tested over two years ago, we can no longer retrieve your scores.'
  }, {
    title : 'Can I view my scores online?',
    description : 'Yes. Create an account using the access code found on your ASVAB Summary Results (ASR) sheet.<br>'+
    '<br><ul><li>Your career exploration scores are available at <a href="' + this.asvabprogramLink + '">asvabprogram.com</a>.</li>' +
    '<li>Your ASVAB CEP scores are converted to military line scores, <a href="' + this.CITMCompositeScoreLink + '">here</a>.</li></ul>' +
    'The username and password you create gives you access to both websites.'
  }, {
    title : 'The scores I received in school are different from the scores my recruiter has. Which scores are correct? ',
    description : 'Scores on the ASVAB CEP ASVAB Summary Results (ASR) sheet are provided for career exploration purposes only. The only score on the ASR that is the same for military recruiting use is the AFQT or Military Entrance Score. <br><br>' +
    'ASVAB CEP participants are encouraged to create an account using their access code to view their career exploration scores at <a href="' + this.asvabprogramLink + '">asvabprogram.com</a>. ASVAB CEP scores are converted to military line scores, <a href="' + this.CITMCompositeScoreLink + '">here</a>.<br><br>' +
    'You must create an account using the access code found on your ASR to view your scores online. The username and password you create gives you access to both websites.<br><br>' +
    'Note: MEPCOM can only retrieve scores from tests taken in 11th and 12th grade and post-secondary schools. 11th and 12th grade and post-secondary students can use their ASVAB CEP AFQT scores to enlist for up to two years after testing. All 10th grade scores are invalid for enlistment purposes (regardless of age) and are not available through the USMIRS database.',
  } ];

  access = [ {
    title : 'I lost my access code. Can you send it to me?',
    description : 'Yes, ASVAB CEP participants can request your access code, <a (click)=\'openScoreRequestModal()\'>here</a>.'
  },{
    title : 'Can I request an access code?',
    description : 'Parties interested in the career exploration activities ASVAB CEP offers can request a temporary access code, <a (click)=\'openScoreRequestModal()\'>here</a>.'
  }];

  military = [ {
    title : 'Why was there a recruiter at my testing session? ',
    description : 'ASVAB CEP test sessions are administered by Department of Defense civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Most schools elect to use military proctors rather than tying up school staff during test sessions.'
  }, {
    title : 'Where can I find military career information? ',
    description : '<a href="' + this.CITMLink + '">Careersinthemilitary.com</a> is a comprehensive online resource powered by ASVAB CEP that allows students to discover extensive details about military career opportunities across all Services, their Service-specific ASVAB line scores and which Services offer which jobs.'
  }, {
    title : 'I\'m interested in the Military, where can I take an ASVAB test? ',
    description : 'High school and post-secondary students may be able to test at their local school. Check with your school counselor to find out if the ASVAB CEP is offered. Talk to a recruiter to find out if your scores qualify you for service.<br><br>'+
    'If you\'re not in school, contact a military recruiter to request testing. The test is administered at 65 Military Entrance Processing Stations (MEPS) and over 300 Military Entrance Test (MET) Sites nationwide. You must be sponsored by a recruiter to test at these facilities. <br><br>' +
    'Find the nearest MEPS, <a (click)=mepcomLink()>here</a>. After selecting the MEPS, select the link to "Military Entrance Test (MET) Sites" under the dark blue bar to find the nearest MET sites for testing.'
  }, {
    title : 'Which occupations do I qualify for in the Military? ',
    description : 'The Military Entrance Score, or AFQT, is what the Military will use to determine enlistment eligibility. Each Service sets its own AFQT score requirement. Then, each military job has its own unique composite score requirement.*<br><br>'+
    'Your ASVAB CEP scores are converted to military lines scores <a href="' + this.CITMCompositeScoreLink + '">here</a>. For the most up-to-date requirements or to find out if you qualify for military service, <a href="' + this.contactRecruiter + '">contact a recruiter</a>. <br><br>'+
    'If you are in 11<sup>th</sup> grade or beyond and want to enlist using these scores, log in to <a href="' + this.asvabprogramLink + '">asvabprogram.com</a> or <a href="' + this.CITMLink + '">careersinthemilitary.com</a> to show recruiters your scores. Recruiters will then submit a formal request to pull your scores using FORM 680-3A. <br><br>' +
    '* These score requirements are subject to change without notice and vary depending on the recruiting environment and the need for new military applicants.'
  }, {
    title : 'I want to join the Air Force/Army/Marine Corps/National Guard/Navy. Can you help me? ',
    description : 'The ASVAB CEP helps students search the world of work for occupations that they may find satisfying. Some of these careers are found in the Military. You can use your ASVAB Career Exploration Scores to find military careers for which you would be best suited at <a href="' + this.CITMLink + '">careersinthemilitary.com</a>. However, ASVAB CEP personnel are not involved in recruiting activities. For information on how to join the Military, click <a href="' + this.CITMOptionsLink + '">here</a>. Then, <a href="' + this.contactRecruiter + '">contact a military recruiter</a> in your area.'
  }, {
    title : 'I want to join the Military, how do I improve my score? ',
    description : 'We recommend the Army\'s <a (click)=\'march2successLink()\'>March2Success</a> program for ASVAB test score improvement. This resource provides general test preparation materials for ACT, SAT, and high school completion. As the AFQT score is a combination of mathematics knowledge, arithmetic reasoning, paragraph comprehension, and vocabulary skills, studying the related material at this site in any of those areas may help you improve your qualifying score.'
  }, {
    title : 'How do I contact a recruiter? ',
    description : 'Contact a military recruiter in your area <a href="' + this.contactRecruiter + '">here</a>.'
  }, {
    title : 'Where can I find more information about the ASVAB? ',
    description : 'Be sure to review the sample test items and test taking strategies <a href="' + this.asvbprogramStudentLink + '">here</a>.<br>' +
    'Then, you can find more information and more sample test questions at <a href="' + this.officialasvabLink + '">officialasvab.com</a>. For even more practice questions, visit <a href="' + this.todaymilitaryLink + '">todaysmilitary.com</a>.'
  } ];

  userType;
  firstName;
  lastName;
  email;
  topics;
  message;
  formDisabled = false;
  recaptchaResponse = '';
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  validation;
  path;

  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  aFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _contactUsService: ContactUsService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _userService: UserService,
    private _router: Router,
    private _dialog: MatDialog,
    private _utilityService: UtilityService,
    private _meta: Meta,
    private _titleTag: Title,
  ) {
    this.path = this._router.url;
  }

  ngOnInit() {
    if (this.path.includes('contactUs=submitted')) {
      const modalOptions = {
        title : 'Contact Us',
        message : 'Thank you for contacting us!'
      };

      const dialogRef1 = this._dialog.open(MessageDialogComponent, {
        data: modalOptions,
        hasBackdrop: true,
        disableClose: true,
        maxWidth: 300,
      });

      dialogRef1.afterClosed().subscribe(result => {
        window.location.href = this.path.split('?')[0];
      })
    }

    if (this.path.includes('requestScore=submitted')) {
      this._dialog.open(ScoreRequestComponent , {
        data: {
          requestScoreResponse: window.sessionStorage.getItem("requestScoreResponse"),
        }
      });

      window.sessionStorage.removeItem("requestScoreResponse");
    }

    this.aFormGroup = this._formBuilder.group({
      userType: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      firstName: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      lastName: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      email: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      topics: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      message: new FormControl({value: '', disabled: this.formDisabled}, Validators.required),
      recaptcha: new FormControl({value: '', disabled: this.formDisabled}, Validators.required)
    });

    const metaTitle = 'FAQ ASVAB CEP | ASVAB Career Exploration Program';
    const metaDescription = 'Frequently asked questions about the ASVAB Career Exploration Program. For a faster response, please see the commonly asked questions & answers.';

    this._utilityService.setSocialMeta(metaTitle, metaDescription);
  }

  isLoggedIn() {
      return this._userService.isLoggedIn();
  }

  emailUs() {
    this.validation = undefined;
    this.formDisabled = true;
    const emailObject = {
      userType : this.aFormGroup.value.userType,
      firstName : this.aFormGroup.value.firstName,
      lastName : this.aFormGroup.value.lastName,
      email : this.aFormGroup.value.email,
      topics : this.aFormGroup.value.topics,
      message : this.aFormGroup.value.message,
      recaptchaResponse : this.aFormGroup.value.recaptcha
    };

    if (this.aFormGroup.value.userType === '' ||
        this.aFormGroup.value.firstName === '' ||
        this.aFormGroup.value.lastName === '' ||
        this.aFormGroup.value.email === '' ||
        this.aFormGroup.value.topics === '' ||
        this.aFormGroup.value.message === '') {
      this.validation = 'Fill out all fields.';
      this.formDisabled = false;
      return false;
    } else if (!this._utilityService.validateEmail(this.aFormGroup.value.email)) {
      this.validation = 'Invalid email format.';
      this.formDisabled = false;
      return false;
    }

    if (this.aFormGroup.value.recaptcha === '') { // if string
      // is empty
      this.validation = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {
      this._contactUsService.emailUs(emailObject).then(
        (data:any) => {

          this.formDisabled = false;

          // Track event in Google Analytics.
          this._googleAnalyticsService.trackClick('#' + emailObject.userType + '#' + emailObject.topics + '#contact-form-submitted' );

          // Reset values of form
          this.aFormGroup.setValue({userType: '', firstName: '', lastName: '', email: '', topics: '', message: '', recaptcha: ''});

          this.recaptchaResponse = '';
          // vcRecaptchaService.reload();
          this.captchaElem.reloadCaptcha();

          window.location.href = this.path + '?contactUs=submitted';
        }).catch((error:any) => {

          console.debug('error:', error);

          let modalOptions = { title : 'Error', message: 'There was an error when trying to contact us. Please try again.'};

          //if recaptcha error occured on server
          if(error.recaptchaResponse) modalOptions.message = error.recaptchaResponse[0];

          if(error.message) modalOptions.message = error.message;

          if(error['message']) modalOptions.message = error['message'];

          if(error.message) modalOptions.message = error.message;

          const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: modalOptions,
            hasBackdrop: true,
            disableClose: true,
            maxWidth: '300px'
          });

            // vcRecaptchaService.reload();
            this.captchaElem.reloadCaptcha();

          this.formDisabled = false;
        }
      );
    }
  }

  openScoreRequestModal() {
    // ScoreRequestModalService.showModal({
    //   size : 'lg'
    // }, {});
    // console.log('BringAsvabToYourSchoolComponent');
      const dialogRef2 = this._dialog.open(ScoreRequestComponent , {
      });
      // this.close();
  }

  // asvabModal = function() {
//    BringToSchoolModalService.show({}, {});
  // };
  public asvabModal() {
    // console.log('BringAsvabToYourSchoolComponent');
      const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent , {
        hasBackdrop: true,
        panelClass: 'new-modal',
        // height: '735px',
        autoFocus: false,
        disableClose: true
      });
      // this.close();
  }

}
