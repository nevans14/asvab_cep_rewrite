import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { ContactUsService } from '../services/contact-us.service';
import { ReCaptcha2Component } from 'ngx-captcha';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-score-request',
  templateUrl: './score-request.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ScoreRequestComponent implements OnInit {

// cepApp.service('ScoreRequestModalService', [ '$uibModal', '$sce', 'modalService', '$templateCache', 'FaqRestFactory', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', function($uibModal, $sce, modalService, $templateCache, FaqRestFactory, vcRecaptchaService, recaptchaKey, $location, $window) {

  working = false;
  stepOne = true;
  stepTwo = false;
  stepThree = false;

  formDisabled = false;
  userType = '';
  parentAllow = undefined;
  firstName = '';
  lastName = '';
  email = '';
  schoolName = '';
  city = '';
  county = '';
  state = '';
  country = '';
  message = '';
  regGradeLevel = '' 
  recaptchaResponse = '';
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  widgetId;
  validation;
  showDateError = false;
  dt: any = '';
  scoreRequestResponse = '';
  path = '';

  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;

  constructor(
    private _contactUsService: ContactUsService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _dialog: MatDialog,
    private _dialogRef: MatDialogRef<ScoreRequestComponent>,
    private _router: Router,
    private _utilityService: UtilityService,
    private _meta: Meta,
    private _titleTag: Title,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.path = this._router.url;
  }

  ngOnInit() {
    
    if (this.path.includes('requestScore=submitted')
      && this.data['requestScoreResponse']) {
      this.stepOne = false;
      this.stepTwo = false;
      this.stepThree = true;
      this.formDisabled = false;
      this.working = false;
      this.scoreRequestResponse = this.data['requestScoreResponse'];
      window.sessionStorage.removeItem('requestScoreResponse');
    }

    const seoTitle = 'Find my ASVAB Score | ASVAB Career Exploration Program';
    const metaDescription = 'Lost your ASVAB score sheet? Find your ASVAB score through the ASVAB score request form. Your career exploration results are available at asvabprogram.com.';

    this._titleTag.setTitle(seoTitle);
    this._meta.updateTag({name: 'description', content: metaDescription});

    this._meta.updateTag({property: 'og:title', content: seoTitle});
    this._meta.updateTag({property: 'og:description', content: metaDescription});

    this._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
    this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
  }

  ga_trackClick(param) {
    this._googleAnalyticsService.trackClick(param);
  }

  gaSocialShare(socialPlug, value) {
    this._googleAnalyticsService.trackSocialShare(socialPlug, value);
  }

  ga_trackSampleTestClick(param) {
    this._googleAnalyticsService.trackSampleTestClick(param);
  }

        notAllowedPopup() {
          const modalOptions = {
            title : 'Score Request',
            message : 'MEPCOM cannot release test scores to parents of children ages 18 and over.'
          };

          const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: modalOptions,
            hasBackdrop: true,
            disableClose: true,
            maxWidth: 300,
            // panelClass: 'modal-sm', // for reference if need to add class
          });

          this._dialogRef.close();
        }

        resetStateSelection() {
          // console.debug('state:', this.country, this.formDisabled, this.parentAllow, this.userType);
          if (this.country === 'US') {
            // this.aFormGroup.get('state').enable();
          } else {
            this.state = '';
            // this.aFormGroup.get('state').setValue('');
            // this.aFormGroup.get('state').disable();
          }
        }

        submit() {
          this.formDisabled = true;
          this.working = true;
          this.validation = undefined;
          this.showDateError = false;

          // if (!(this.dt instanceof Date)) {
          //   this.validation = 'Invalid Date.';
          //   this.formDisabled = false;
          //   this.working = false;
          //   return false;
          // }

          const selectedTimeStamp = new Date(this.dt + 'T00:00:00');

          const emailObject = {
            userType    : this.userType,
            firstName   : this.firstName,
            lastName    : this.lastName,
            email       : this.email,
            schoolName  : this.schoolName,
            city        : this.city,
            county      : this.county,
            state       : this.state,
            country     : this.country,
            message     : this.message,
            gradeLevel  : this.regGradeLevel,
            dt          : (selectedTimeStamp.getMonth() + 1) + '/' + selectedTimeStamp.getDate() + '/' + selectedTimeStamp.getFullYear(),
            recaptchaResponse : this.recaptchaResponse
          };

          const threeMonthTimeStamp = new Date();
          threeMonthTimeStamp.setMonth(threeMonthTimeStamp.getMonth() - 3);

          const twoYearTimeStamp = new Date();
          twoYearTimeStamp.setMonth(twoYearTimeStamp.getMonth() - 24);

          if (emailObject.userType === '' ||
              emailObject.firstName === '' ||
              emailObject.lastName === '' ||
              emailObject.email === '' ||
              emailObject.schoolName === '' ||
              emailObject.city === '' ||
              (emailObject.county === '' && emailObject.country === 'US') ||
              (emailObject.state === '' && emailObject.country === 'US') ||
              emailObject.country === '' ||
              emailObject.gradeLevel === '' ||
              emailObject.dt === '') {
            this.validation = 'Fill out all fields.';
            this.formDisabled = false;
            this.working = false;
            return false;
          } else if (!this._utilityService.validateEmail(emailObject.email)) {
            this.validation = 'Invalid email format.';
            this.formDisabled = false;
            this.working = false;
            return false;
          } else if (twoYearTimeStamp > selectedTimeStamp ||
                     threeMonthTimeStamp < selectedTimeStamp) {
            this.validation = 'Invalid Date.';
            this.formDisabled = false;
            this.working = false;
            return false;
          }

          if (this.recaptchaResponse === '') { // if string
            // is empty
            this.validation = 'Please resolve the captcha and submit!';
            this.formDisabled = false;
            this.working = false;
            return false;
          } else {
            this.validation = '';
            this._contactUsService.scoreRequest(emailObject).then(
              (success: any) => {

              // Track event in Google Analytics.
              this._googleAnalyticsService.trackClick('#' + emailObject.userType + '#score-request' + '#contact-form-submitted');
//              var path = $location.path();
//              $window.ga('create', 'UA-83809749-1', 'auto');
//              $window.ga('send', 'pageview', path + '#' + emailObject.userType + '#score-request' + '#contact-form-submitted' );

              if (success &&
                success.resultCode &&
                (success.resultCode == 1 ||
                success.resultCode == 2) && 
                success.resultMessage) {
                this.scoreRequestResponse = success.resultMessage;
              } else {
                this.scoreRequestResponse = "Someone will email you soon.";
              }

              window.sessionStorage.setItem('requestScoreResponse', this.scoreRequestResponse);

              this.userType = '';
              this.parentAllow = undefined;
              this.firstName = '';
              this.lastName = '';
              this.email = '';
              this.schoolName = '';
              this.city = '';
              this.county = '';
              this.state = '';
              this.country = '';
              this.message = '';
              this.regGradeLevel = '',
              this.recaptchaResponse = '';
              this.validation= '';
              this.showDateError = false;
              this.dt = '';

              this.recaptchaResponse = '';
//              vcRecaptchaService.reload();
              this.captchaElem.reloadCaptcha();

              window.location.href = this.path + '?requestScore=submitted';
            }, data => {
                this.working = false;
							var message = '';
							if (data.data.title) {
								// convert server response property "errors" to base obj
								var errors = data.data.errors;
								data.data = {};
								// error object is using pascal case, need to convert to camel case
								for (var key in errors) {
									data.data[key.charAt(0).toLowerCase() + key.substring(1, key.length)] = errors[key];
								}
							}
							for (var key in emailObject) {
								if (data.data && data.data[key]) {
									message += '\n' + data.data[key][0];
								}
							}
							// if status is 400 (bad request), show validation message, else show generic error message
							if (data.status === 400 && message !== '') {
								this.validation = message;
							} else {
								this.validation = 'There was an error processing your request. Please try again.';
                this.captchaElem.reloadCaptcha();
							}
							this.formDisabled = false;
            });
          }
        }

        routeToBaseUrl() {
          window.location.href = this.path.split('?')[0];
        }
  }
