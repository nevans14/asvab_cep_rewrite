import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreRequestComponent } from './score-request.component';

describe('ContactUsComponent', () => {
  let component: ScoreRequestComponent;
  let fixture: ComponentFixture<ScoreRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
