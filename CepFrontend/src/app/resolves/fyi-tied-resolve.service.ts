import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { FindYourInterestService } from 'app/services/find-your-interest.service';

@Injectable({
  providedIn: 'root'
})
export class FyiTiedResolveService implements Resolve<any>{

  constructor(private FYIScoreService: FindYourInterestService, 
    private _router: Router) { }
    
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    let defer: any;

    let self = this;

    let redirect = function(){

      // Gets top combined and gender score from session storage.
      let topCombinedScores = self.FYIScoreService.getTopCombinedInterestCodes();
      let topGenderScores = self.FYIScoreService.getTopGenderInterestCodes();

      let istiedLinkClicked = self.FYIScoreService.getTiedLinkClicked();

      if ((topCombinedScores !== undefined && !istiedLinkClicked) || (topGenderScores !== undefined && !istiedLinkClicked)) {
        self._router.navigate(['/find-your-interest-score']);
      } else {
        defer.resolve("success");
      }

      return Promise.all([defer]); 
    }

  }
  
}
