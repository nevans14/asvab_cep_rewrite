import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';

@Injectable({
  providedIn: 'root'
})
export class PortfolioAsvabScoreResolveService implements Resolve<any>  {

  constructor(private _portfolioRestFactory: PortfolioRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let asvabScore = this._portfolioRestFactory.getAsvabScore().toPromise();

    return asvabScore;
  }
}
