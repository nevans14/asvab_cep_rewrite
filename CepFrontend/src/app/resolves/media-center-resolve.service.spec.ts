import { TestBed } from '@angular/core/testing';

import { MediaCenterResolveService } from './media-center-resolve.service';

describe('MediaCenterResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediaCenterResolveService = TestBed.get(MediaCenterResolveService);
    expect(service).toBeTruthy();
  });
});
