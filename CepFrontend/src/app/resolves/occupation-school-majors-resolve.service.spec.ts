import { TestBed } from '@angular/core/testing';

import { OccupationSchoolMajorsResolveService } from './occupation-school-majors-resolve.service';

describe('OccupationSchoolMajorsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationSchoolMajorsResolveService = TestBed.get(OccupationSchoolMajorsResolveService);
    expect(service).toBeTruthy();
  });
});
