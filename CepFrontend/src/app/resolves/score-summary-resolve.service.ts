import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UserService } from 'app/services/user.service';

@Injectable( {
    providedIn: 'root'
} )
export class ScoreSummaryResolveService implements Resolve<any> {

    constructor( private _httpHelper: HttpHelperService,
        private _user: UserService ) { }

    resolve() {

        var userRole = this._user.getUser().currentUser.role;

        var scoreSummaryPromise = new Promise(( resolve, reject ) => {
            // If role is teacher (T)
            if ( userRole == 'T' ) {




                // Get access code to pull teacher scores.
                this._httpHelper.httpHelper( 'GET', 'portfolio/get-access-code', null, null ).then(( s: any ) => {
                    var accessCode = s.r;
                    var scoreAccessCode;

                    // Uses teacher specific data.
                    if ( accessCode.indexOf( 'CEP123' ) !== -1 ) {
                        scoreAccessCode = 'SHANNON-A';
                    } else {
                        scoreAccessCode = 'KATE-A';
                    }

                    // Get test score by access code.
                    this._httpHelper.httpHelper( 'GET', 'testScore/get-test-score-access-code', [{ key: null, value: scoreAccessCode }], null ).then(( sTwo ) => {
                        //FYIRestFactory.getTestScoreWithAccessCode(scoreAccessCode).then(function(sTwo) {
                        resolve( sTwo );
                    }, ( e ) => {
                        reject( e );
                    } )
                }, ( e ) => {
                    reject( e );
                } );

                //return $q.all([ pTwo.promise, p.promise ]);

            } else {

                this._httpHelper.httpHelper( 'GET', 'testScore/get-test-score', null, null ).then(( sTwo ) => {
                    //FYIRestFactory.getTestScoreWithAccessCode(scoreAccessCode).then(function(sTwo) {
                    resolve( sTwo );
                }, ( e ) => {
                    reject( e );
                } )
            }
        } );


        return scoreSummaryPromise;
    }

}
