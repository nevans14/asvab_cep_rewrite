import { TestBed } from '@angular/core/testing';

import { FavoritesResolveService } from './favorites-resolve.service';

describe('FavoritesResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FavoritesResolveService = TestBed.get(FavoritesResolveService);
    expect(service).toBeTruthy();
  });
});
