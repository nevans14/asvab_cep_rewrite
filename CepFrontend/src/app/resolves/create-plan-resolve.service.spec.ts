import { TestBed } from '@angular/core/testing';

import { CreatePlanResolveService } from './create-plan-resolve.service';

describe('CreatePlanResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreatePlanResolveService = TestBed.get(CreatePlanResolveService);
    expect(service).toBeTruthy();
  });
});
