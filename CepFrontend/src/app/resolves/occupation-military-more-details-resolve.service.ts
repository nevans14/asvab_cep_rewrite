import { Injectable } from '@angular/core';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OccupationMilitaryMoreDetailsResolveService {

  constructor(private _occufindRestFactoryService: OccufindRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot){
    let socId = route.paramMap.get('socId');

    return this._occufindRestFactoryService.getMilitaryMoreDetails( socId ).toPromise();
  }
}
