import { TestBed } from '@angular/core/testing';

import { PortfolioAsvabScoreResolveService } from './portfolio-asvab-score-resolve.service';

describe('PortfolioAsvabScoreResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PortfolioAsvabScoreResolveService = TestBed.get(PortfolioAsvabScoreResolveService);
    expect(service).toBeTruthy();
  });
});
