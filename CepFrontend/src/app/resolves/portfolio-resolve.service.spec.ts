import { TestBed } from '@angular/core/testing';

import { PortfolioResolveService } from './portfolio-resolve.service';

describe('PortfolioResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PortfolioResolveService = TestBed.get(PortfolioResolveService);
    expect(service).toBeTruthy();
  });
});
