import { TestBed } from '@angular/core/testing';

import { EssTrainingResolveService } from './ess-training-resolve.service';

describe('EssTrainingResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EssTrainingResolveService = TestBed.get(EssTrainingResolveService);
    expect(service).toBeTruthy();
  });
});
