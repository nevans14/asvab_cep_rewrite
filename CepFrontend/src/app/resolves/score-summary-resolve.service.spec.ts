import { TestBed } from '@angular/core/testing';

import { ScoreSummaryResolveService } from './score-summary-resolve.service';

describe('ScoreSummaryResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScoreSummaryResolveService = TestBed.get(ScoreSummaryResolveService);
    expect(service).toBeTruthy();
  });
});
