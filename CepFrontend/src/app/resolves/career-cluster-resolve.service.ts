import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';

@Injectable({
  providedIn: 'root'
})
export class CareerClusterResolveService implements Resolve<any>  {

  constructor(
    private careerClusterRestFactory: CareerClusterRestFactoryService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    let career = this.careerClusterRestFactory.getCareerCluster();

    return Promise.all([career]);

  }
}
