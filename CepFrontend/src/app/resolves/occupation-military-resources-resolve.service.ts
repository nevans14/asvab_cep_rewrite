import { Injectable } from '@angular/core';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OccupationMilitaryResourcesResolveService {

  constructor(private _careerClusterRestFactoryService: CareerClusterRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot){
    let socId = route.paramMap.get('socId');

    return this._careerClusterRestFactoryService.getMilitaryResources( socId ).toPromise();
  }
}
