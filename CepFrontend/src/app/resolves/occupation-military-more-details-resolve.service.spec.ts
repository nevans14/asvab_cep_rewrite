import { TestBed } from '@angular/core/testing';

import { OccupationMilitaryMoreDetailsResolveService } from './occupation-military-more-details-resolve.service';

describe('OccupationMilitaryMoreDetailsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationMilitaryMoreDetailsResolveService = TestBed.get(OccupationMilitaryMoreDetailsResolveService);
    expect(service).toBeTruthy();
  });
});
