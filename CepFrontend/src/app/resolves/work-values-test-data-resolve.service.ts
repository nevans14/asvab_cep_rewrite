import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { WorkValuesApiService } from 'app/services/work-values-api.service';

@Injectable({
  providedIn: 'root'
})
export class WorkValuesTestDataResolveService implements Resolve<any> {

  constructor(private workValueApi: WorkValuesApiService) { }
  
  resolve() {
   return this.workValueApi.getTestInformation();
  }
}