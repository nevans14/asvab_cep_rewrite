import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable( {
    providedIn: 'root'
} )
export class SchoolDetailsResolveService implements Resolve<any>  {
    restURL: any;

    constructor( private _http: HttpClient,
        private _config: ConfigService,
        private _user: UserService,
        private _httpHelper: HttpHelperService,
      ) {
        this.restURL = this._config.getRestUrl() + '/';
    }

    resolve( route: ActivatedRouteSnapshot ) {

        let socId = route.paramMap.get( 'socId' );
        let unitId = route.paramMap.get( 'unitId' );
        let occupationTitleDescription = undefined;
        let schoolDetails = undefined;
        let favoriteSchools = undefined;
        let favorites = undefined;
        if (socId != null) {
            occupationTitleDescription = this._http.get(this._config.getAwsFullUrl(`/api/occufind/occupationTitleDescription/${socId}/`)).toPromise();
        }
        if (unitId != null) {
            schoolDetails = this._http.get(this._config.getAwsFullUrl(`/api/occufind/SchoolProfile/${unitId}/`)).toPromise();
        }

        if (this._user.getUser() == undefined) {
            return Promise.all([
                occupationTitleDescription,
                schoolDetails,
                favoriteSchools,
                favorites,
            ])
        } else {
            favoriteSchools = new Promise((resolve, reject) => {
                this._user.getSchoolFavoritesList().then(
                    res => {
                        resolve(res)
                    }, 
                    err => {
                        reject(err)
                    }
                )
            })
            favorites = new Promise((resolve, reject) => {
                this._user.getFavoritesList().then(
                    res => {
                        resolve(res)
                    }, 
                    err => {
                        reject(err)
                    }
                )
            })
            return Promise.all([
                occupationTitleDescription,
                schoolDetails,
                favoriteSchools,
                favorites,
            ])
        }
    }
}
