import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';

@Injectable({
  providedIn: 'root'
})
export class FavoritesOccupationResolveService implements Resolve<any> {

  constructor(private _user: UserService,
    private favoritesRestFactoryService: FavoritesRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot) {

    let favorites;
    if (this._user.getUser() === undefined) {
      favorites = undefined;
    } else {
      favorites = this.favoritesRestFactoryService.getFavoriteOccupation();
    }

    return favorites;
  }
}
