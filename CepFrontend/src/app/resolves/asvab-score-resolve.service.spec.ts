import { TestBed } from '@angular/core/testing';

import { AsvabScoreResolveService } from './asvab-score-resolve.service';

describe('AsvabScoreResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsvabScoreResolveService = TestBed.get(AsvabScoreResolveService);
    expect(service).toBeTruthy();
  });
});
