import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable( {
    providedIn: 'root'
} )
export class EmploymentDetailsResolveService implements Resolve<any>  {
    restURL: any;

    constructor( private _http: HttpClient,
        private _config: ConfigService,
        private _user: UserService ) {
        this.restURL = _config.getRestUrl() + '/';
    }

    resolve( route: ActivatedRouteSnapshot ) {
        let stateSalaryYear = this._http.get(this._config.getAwsFullUrl('/api/occufind/BlsProjectionYearUpdated')).toPromise();
        let socId = route.paramMap.get( 'socId' );
        let occupationTitleDescription = undefined;
        let employmentMoreDetails = undefined;
        let favorites = undefined;
        if (socId != null) {
            occupationTitleDescription = this._http.get(this._config.getAwsFullUrl('/api/occufind/occupationTitleDescription/' + socId + '/')).toPromise();
            employmentMoreDetails = this._http.get(this._config.getAwsFullUrl('/api/occufind/EmploymentMoreDetails/' + socId.slice(0, -3) + '/')).toPromise();
        }
        if (this._user.getUser() == undefined) {
            return Promise.all([
                stateSalaryYear,
                occupationTitleDescription,
                employmentMoreDetails,
                favorites,
            ])
        } else {
            favorites = new Promise((resolve, reject) => {
                this._user.getFavoritesList().then(
                    res => {
                        resolve(res)
                    }, 
                    err => {
                        reject(err)
                    }
                )
            })
            return Promise.all([
                stateSalaryYear,
                occupationTitleDescription,
                employmentMoreDetails,
                favorites,
            ])
        }
    }
}
