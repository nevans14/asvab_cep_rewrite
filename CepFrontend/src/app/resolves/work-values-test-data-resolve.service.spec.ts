import { TestBed } from '@angular/core/testing';

import { WorkValuesTestDataResolveService } from './work-values-test-data-resolve.service';

describe('WorkValuesTestDataResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkValuesTestDataResolveService = TestBed.get(WorkValuesTestDataResolveService);
    expect(service).toBeTruthy();
  });
});
