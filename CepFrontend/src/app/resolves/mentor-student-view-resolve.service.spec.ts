import { TestBed } from '@angular/core/testing';

import { MentorStudentViewResolveService } from './mentor-student-view-resolve.service';

describe('MentorStudentViewResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MentorStudentViewResolveService = TestBed.get(MentorStudentViewResolveService);
    expect(service).toBeTruthy();
  });
});
