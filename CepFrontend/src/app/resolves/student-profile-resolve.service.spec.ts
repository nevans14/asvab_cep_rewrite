import { TestBed } from '@angular/core/testing';

import { StudentProfileResolveService } from './student-profile-resolve.service';

describe('StudentProfileResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentProfileResolveService = TestBed.get(StudentProfileResolveService);
    expect(service).toBeTruthy();
  });
});
