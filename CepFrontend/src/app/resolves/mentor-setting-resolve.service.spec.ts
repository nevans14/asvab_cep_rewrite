import { TestBed } from '@angular/core/testing';

import { MentorSettingResolveService } from './mentor-setting-resolve.service';

describe('MentorSettingResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MentorSettingResolveService = TestBed.get(MentorSettingResolveService);
    expect(service).toBeTruthy();
  });
});
