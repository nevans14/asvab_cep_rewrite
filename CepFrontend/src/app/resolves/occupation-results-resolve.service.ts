import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';

@Injectable( {
    providedIn: 'root'
} )
export class OccupationResultsResolveService implements Resolve<any>  {
  restURL: any;

  constructor( private _http: HttpClient,
      _config: ConfigService,
      private _user: UserService,
      private careerClusterRestFactory: CareerClusterRestFactoryService) {
      this.restURL = _config.getRestUrl() + '/';
  }

  resolve( route: ActivatedRouteSnapshot ) {

    let occupationResults = undefined;
    let careerClusterById = undefined;
    let favorites = undefined;

    let ccId = route.paramMap.get( 'ccId' );

    if (ccId != null) {
      occupationResults = this.careerClusterRestFactory.getCareerClusterOccupations(ccId).toPromise();
      careerClusterById = this.careerClusterRestFactory.getCareerClusterById(ccId).toPromise();
    }

    if (this._user.getUser() == undefined) {
      return Promise.all([
        occupationResults,
        careerClusterById,
        favorites,
      ])
    } else {
      favorites = new Promise((resolve, reject) => {
        this._user.getFavoritesList().then(
          res => {
            resolve(res)
          }, 
          err => {
            reject(err)
          }
        )
      })
      return Promise.all([
        occupationResults,
        careerClusterById,
        favorites,
      ])
    }
  }
}
