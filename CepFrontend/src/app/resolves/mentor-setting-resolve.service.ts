import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class MentorSettingResolveService implements Resolve<any> {

  constructor(private _userService: UserService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {


    let userName = this._userService.getUsername();
    let emailPreference = this._userService.getEmailSettings();
    let selectedSchools = this._userService.getSelectedSchools();

    return Promise.all([userName, emailPreference, selectedSchools]);

  }
}
