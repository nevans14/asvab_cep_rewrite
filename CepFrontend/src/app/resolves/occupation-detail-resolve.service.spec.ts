import { TestBed } from '@angular/core/testing';

import { OccupationDetailResolveService } from './occupation-detail-resolve.service';

describe('OccupationDetailResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationDetailResolveService = TestBed.get(OccupationDetailResolveService);
    expect(service).toBeTruthy();
  });
});
