import { TestBed } from '@angular/core/testing';

import { OccupationMilitaryResourcesResolveService } from './occupation-military-resources-resolve.service';

describe('OccupationMilitaryResourcesResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationMilitaryResourcesResolveService = TestBed.get(OccupationMilitaryResourcesResolveService);
    expect(service).toBeTruthy();
  });
});
