import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DbInfoService } from 'app/services/db-info.service';

@Injectable({
  providedIn: 'root'
})
export class DbInfoResolveService implements Resolve<any>{

  constructor(private _dbInfoService: DbInfoService) { }

  resolve(){
    return this._dbInfoService.getByName('ipeds_data_disclaimer').toPromise();
  }

}
