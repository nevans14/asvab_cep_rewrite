import { TestBed } from '@angular/core/testing';

import { ViewPlanResolveService } from './view-plan-resolve.service';

describe('ViewPlanResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewPlanResolveService = TestBed.get(ViewPlanResolveService);
    expect(service).toBeTruthy();
  });
});
