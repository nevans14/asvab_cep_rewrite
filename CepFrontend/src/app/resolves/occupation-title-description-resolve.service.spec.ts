import { TestBed } from '@angular/core/testing';

import { OccupationTitleDescriptionResolveService } from './occupation-title-description-resolve.service';

describe('OccupationTitleDescriptionResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationTitleDescriptionResolveService = TestBed.get(OccupationTitleDescriptionResolveService);
    expect(service).toBeTruthy();
  });
});
