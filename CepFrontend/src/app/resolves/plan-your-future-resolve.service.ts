import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { PortfolioService } from 'app/services/portfolio.service';

@Injectable({
  providedIn: 'root'
})
export class PlanYourFutureResolveService {

  constructor(
    private _userOccupationPlanService: UserOccupationPlanService, 
    private _planCalendarService: PlanCalendarService,
    private _userService: UserService,
    private _classroomActivityService: ClassroomActivityService,
    private _portfolioService: PortfolioService) { }

  resolve(route: ActivatedRouteSnapshot) {

    //get all user dates
    let userOccupationPlanCalendars = this.getAllUserOccupationPlanCalendars();

    //get all user occupation plans
    let userOccupationPlan = this._userOccupationPlanService.getAll();

    //get favorites
    let favoriteList = this._userService.getFavoritesList();
    
    //get classroom activities
    let classroomActivities = this._classroomActivityService.getActivities();
    
    //get classroom categories
    let classroomCategories = this._classroomActivityService.getCategories();

    let portfolio = this._portfolioService.getPortfolio();


    return Promise.all([userOccupationPlanCalendars, userOccupationPlan, favoriteList, classroomActivities, classroomCategories, portfolio]);

  }

  async getAllUserOccupationPlanCalendars() {

    try {

      let userOccupationPlanCalendars = await this._planCalendarService.getCalendars();

      return await userOccupationPlanCalendars;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

}
