import { TestBed } from '@angular/core/testing';

import { DbInfoResolveService } from './db-info-resolve.service';

describe('DbInfoResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbInfoResolveService = TestBed.get(DbInfoResolveService);
    expect(service).toBeTruthy();
  });
});
