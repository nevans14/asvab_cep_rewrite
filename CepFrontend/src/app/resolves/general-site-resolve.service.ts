import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { StaticPageServiceService } from '../services/static-page-service.service';

@Injectable({
  providedIn: 'root'
})
export class GeneralSiteResolveService implements Resolve<any> {

  constructor(private staticPageService: StaticPageServiceService) { }
  
  resolve() {
	  return this.staticPageService.getPageByPageName('general-site');
  }
}
