import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MediaCenterService } from 'app/services/media-center.service';

@Injectable({
  providedIn: 'root'
})
export class MediaCenterResolveService implements Resolve<any> {

  constructor(private _mediaCenterService: MediaCenterService) { }

  resolve(){

    return this._mediaCenterService.getMediaCenterList();
  }

}
