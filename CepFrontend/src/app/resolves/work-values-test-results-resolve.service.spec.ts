import { TestBed } from '@angular/core/testing';

import { WorkValuesTestResultsResolveService } from './work-values-test-results-resolve.service';

describe('WorkValuesTestResultsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkValuesTestResultsResolveService = TestBed.get(WorkValuesTestResultsResolveService);
    expect(service).toBeTruthy();
  });
});
