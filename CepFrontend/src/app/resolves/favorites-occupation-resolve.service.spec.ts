import { TestBed } from '@angular/core/testing';

import { FavoritesOccupationResolveService } from './favorites-occupation-resolve.service';

describe('FavoritesOccupationResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FavoritesOccupationResolveService = TestBed.get(FavoritesOccupationResolveService);
    expect(service).toBeTruthy();
  });
});
