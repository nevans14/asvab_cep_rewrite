import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';

@Injectable( {
    providedIn: 'root'
} )
export class FavoritesResolveService implements Resolve<any> {

    constructor(private _user: UserService) { }

    resolve() {
        
        let favoriteList = this._user.getFavoritesList();
        let careerClusterFavoriteList = this._user.getCareerClusterFavoritesList();
        let schoolFavoriteList = this._user.getSchoolFavoritesList();

        return Promise.all([favoriteList, careerClusterFavoriteList, schoolFavoriteList]);
    }
}
