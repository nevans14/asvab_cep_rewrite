import { TestBed } from '@angular/core/testing';

import { GeneralSiteResolveService } from './general-site-resolve.service';

describe('GeneralSiteResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralSiteResolveService = TestBed.get(GeneralSiteResolveService);
    expect(service).toBeTruthy();
  });
});
