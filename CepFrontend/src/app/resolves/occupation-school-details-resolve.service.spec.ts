import { TestBed } from '@angular/core/testing';

import { OccupationSchoolDetailsResolveService } from './occupation-school-details-resolve.service';

describe('OccupationSchoolDetailsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationSchoolDetailsResolveService = TestBed.get(OccupationSchoolDetailsResolveService);
    expect(service).toBeTruthy();
  });
});
