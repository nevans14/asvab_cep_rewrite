import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class MentorStudentViewResolveService implements Resolve<any> {

  constructor(private _mentorService: MentorService,
    private _userService: UserService) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    //get asvab scores
    const mentorLinkId = route.params.id;

    // career plans
    let careerPlans = await this._mentorService.getStudentCareerPlans(mentorLinkId);

    //calendar
    let calendarEntries = await this._mentorService.getStudentCalendarEntries(mentorLinkId);

    //activities
    let studentActivities = await this._mentorService.getStudentSubmittedActivities(mentorLinkId);

    //portfolio
    let studentPortfolio = await this._mentorService.getStudentPortfolio(mentorLinkId);

    // student info
    let studentInfo = await this._mentorService.getStudentDetails(mentorLinkId);

    //calendar
    let tasksWithNoDate = await this._mentorService.getStudentGlobalTasks(mentorLinkId);

    //portfolio comments
    let portfolioComments = await this._mentorService.getStudentPortfolioComments(mentorLinkId);

    //current user
    let currentUser = await this._userService.getUsername();

    //portfolio logs
    let portfolioLogs = await this._mentorService.getStudentSubmittedPortfolios(mentorLinkId);

    return Promise.all([careerPlans, calendarEntries, studentActivities, studentPortfolio, studentInfo, tasksWithNoDate, portfolioComments, currentUser, portfolioLogs]);

  }

}
