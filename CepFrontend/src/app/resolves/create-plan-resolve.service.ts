import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class CreatePlanResolveService implements Resolve<any> {

  constructor(private _userService: UserService ) { }

  resolve(route: ActivatedRouteSnapshot) {

    let favoriteList = this._userService.getFavoritesList();

    return Promise.all([favoriteList]);

  }
}
