import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OccupationSchoolMajorsResolveService implements Resolve<any>{

  constructor(private _occufindRestFactoryService: OccufindRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot){
    let socId = route.paramMap.get('socId');

    return this._occufindRestFactoryService.getSchoolMajors( socId ).toPromise();
  }
}
