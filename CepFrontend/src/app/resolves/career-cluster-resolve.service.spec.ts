import { TestBed } from '@angular/core/testing';

import { CareerClusterResolveService } from './career-cluster-resolve.service';

describe('CareerClusterResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerClusterResolveService = TestBed.get(CareerClusterResolveService);
    expect(service).toBeTruthy();
  });
});
