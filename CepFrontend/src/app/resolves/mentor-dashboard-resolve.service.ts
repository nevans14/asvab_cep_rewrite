import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class MentorDashboardResolveService implements Resolve<any> {

  constructor(private _mentorService: MentorService,
    private _userService: UserService) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    //current user
    let userName = this._userService.getUsername();
    let groups = this._mentorService.getGroups();

    //get students for the mentor
    let students:any = await this._mentorService.getStudents();

    //get more info for each student
    //this could be removed if the getStudents() returned detailed info
    await Promise.all(students.map(async (student: any) => {

      //for each submission retrieve its logs
      let studentDetails: any = await this._mentorService.getStudentDetails(student.mentorLinkId);
      student.fyiProfile = studentDetails.fyiProfile;
      student.moreAboutMe = studentDetails.moreAboutMe;
      student.workValueProfile = studentDetails.workValueProfile;
    }))

    let studentsPending = await this._mentorService.getPendingStudents();

    let selectedSchools = this._userService.getSelectedSchools();

    return Promise.all([userName, students, groups, studentsPending, selectedSchools]);
  }

}
