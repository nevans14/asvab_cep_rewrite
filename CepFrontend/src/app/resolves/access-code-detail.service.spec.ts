import { TestBed } from '@angular/core/testing';

import { AccessCodeDetailService } from './access-code-detail.service';

describe('AccessCodeDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccessCodeDetailService = TestBed.get(AccessCodeDetailService);
    expect(service).toBeTruthy();
  });
});
