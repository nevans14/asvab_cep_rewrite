import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class EditPlanResolveService implements Resolve<any> {

  constructor(private _userService: UserService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _occufindRestFactory: OccufindRestFactoryService) { }

  async resolve(route: ActivatedRouteSnapshot) {

    const planId = route.params.id;

    let userOccupationPlan: any = await this._userOccupationPlanService.get(planId);

    let occupationDescription: any = await this._occufindRestFactory
      .getOccupationTitleDescription(userOccupationPlan.socId).toPromise().then((success: any) => {
        return success.title;
      }).catch(error => {
        console.error("Error", error);
      });

    let onetSocTrimmed = userOccupationPlan.socId.slice(0, -3);

    let nationalSalary = await this._occufindRestFactory.getNationalSalary(onetSocTrimmed).toPromise();
    let nationalEntrySalary = await this._occufindRestFactory.getNationalEntrySalary(onetSocTrimmed).toPromise();

    return Promise.all([userOccupationPlan, occupationDescription, nationalSalary, nationalEntrySalary]);

  }
}
