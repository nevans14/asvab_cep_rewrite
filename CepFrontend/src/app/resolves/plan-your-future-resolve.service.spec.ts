import { TestBed } from '@angular/core/testing';

import { PlanYourFutureResolveService } from './plan-your-future-resolve.service';

describe('PlanYourFutureResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanYourFutureResolveService = TestBed.get(PlanYourFutureResolveService);
    expect(service).toBeTruthy();
  });
});
