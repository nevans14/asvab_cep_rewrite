import { TestBed } from '@angular/core/testing';

import { FyiTiedResolveService } from './fyi-tied-resolve.service';

describe('FyiTiedResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FyiTiedResolveService = TestBed.get(FyiTiedResolveService);
    expect(service).toBeTruthy();
  });
});
