import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { PortfolioService } from 'app/services/portfolio.service';

@Injectable( {
    providedIn: 'root'
} )
export class PortfolioResolveService implements Resolve<any>  {

    restURL: any;

    constructor( private _http: HttpClient,
        _config: ConfigService,
        private _user: UserService, 
        private _portfolioRestFactory: PortfolioRestFactoryService,
        private _portfolioService: PortfolioService,
        private _userService: UserService) {
        this.restURL = _config.getRestUrl() + '/';
    }

    resolve() {
        let workExperience = this._portfolioRestFactory.getWorkExperience().toPromise();
        let education = this._portfolioRestFactory.getEducation().toPromise();
        let achievement = this._portfolioRestFactory.getAchievements().toPromise();
        let interest = this._portfolioRestFactory.getInterests().toPromise();
        let skill = this._portfolioRestFactory.getSkills().toPromise();
        let workValue = this._portfolioRestFactory.getWorkValues().toPromise();
        let volunteer = this._portfolioRestFactory.getVolunteers().toPromise();
        let actScore = this._portfolioRestFactory.getActScore().toPromise();
        let asvabScore = this._portfolioRestFactory.getAsvabScore().toPromise();
        let fyiScore = this._portfolioRestFactory.getFyiScore().toPromise();
        let satScore = this._portfolioRestFactory.getSatScore().toPromise();
        let otherScore = this._portfolioRestFactory.getOtherScore().toPromise();
        let favoriteList =  this._user.getFavoritesList();
        let careerClusterFavoriteList = this._user.getCareerClusterFavoritesList();
        let plan = this._portfolioRestFactory.getPlan().toPromise();
        let UserInfo = this._portfolioRestFactory.selectNameGrade().toPromise();
        let citmFavoriteOccupations = this._portfolioRestFactory.getCitmFavoriteOccupations().toPromise();
        let schoolFavoriteList = this._user.getSchoolFavoritesList();
        let portfolio = this._portfolioService.getPortfolio();
		let userName = this._userService.getUsername();
        
        return Promise.all( [workExperience, education, achievement, interest, skill, workValue, volunteer, actScore, asvabScore, fyiScore, satScore, otherScore, favoriteList, careerClusterFavoriteList, plan, UserInfo, citmFavoriteOccupations, schoolFavoriteList, portfolio, userName] );
    }
}
