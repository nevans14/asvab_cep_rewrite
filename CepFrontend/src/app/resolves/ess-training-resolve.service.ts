import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';
import { EssTrainingService } from 'app/services/ess-training.service';

@Injectable({
  providedIn: 'root'
})
export class EssTrainingResolveService implements Resolve<any> {

  constructor(private _httpHelper: HttpHelperService,
    private _essTrainingService: EssTrainingService,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {

    let trainingData: any;
    let alternateTraining: any;

    trainingData = new Promise((resolve, reject) => {
      this._essTrainingService.getTrainingByTrainingId(route.params.id).then(
        res => {
          resolve(res)
        },
        err => {
          reject(err)
        }
      )
    })
    alternateTraining = new Promise((resolve, reject) => {
      this._essTrainingService.getAlternateTrainingByTrainingId(route.params.id).then(
        res => {
          resolve(res)
        },
        err => {
          reject(err)
        }
      )
    })
    return Promise.all([
      trainingData,
      alternateTraining,
    ])

  }

}
