import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';
import { OccufindCareerOneFactoryService } from 'app/occufind/occupation-details/services/occufind-career-one-factory.service';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';


@Injectable( {
    providedIn: 'root'
} )
export class OccupationDetailResolveService implements Resolve<any> {

    restURL: any;


    constructor( private _http: HttpClient,
        _config: ConfigService,
        private _user: UserService,
        private CareerOneStopFactory: OccufindCareerOneFactoryService,
        private OccufindRestFactory: OccufindRestFactoryService,
        private careerClusterRestFactory: CareerClusterRestFactoryService,
        private favoritesRestFactoryService: FavoritesRestFactoryService) {
        this.restURL = _config.getRestUrl() + '/'
    }

    async resolve( route: ActivatedRouteSnapshot ) {

        let socId = route.paramMap.get( 'socId' );
        let onetSocTrimmed = socId.slice( 0, -3 );

        let occupationCertificates = this.CareerOneStopFactory.getOccupationCertificates( socId ).toPromise();
        let occupationTitleDescription = this.OccufindRestFactory.getOccupationTitleDescription( socId ).toPromise();
        let stateSalary = this.careerClusterRestFactory.getStateSalary( onetSocTrimmed ).toPromise();
        let stateEntrySalary = this.careerClusterRestFactory.getStateEntrySalary( onetSocTrimmed ).toPromise();
        let nationalSalary = this.OccufindRestFactory.getNationalSalary( onetSocTrimmed ).toPromise();
        let nationalEntrySalary = this.OccufindRestFactory.getNationalEntrySalary( onetSocTrimmed ).toPromise();
        let blsTitle = this.OccufindRestFactory.getBLSTitle( onetSocTrimmed ).toPromise();
        let skillImportance = this.careerClusterRestFactory.getSkillImportance( socId ).toPromise();
        let tasks = this.careerClusterRestFactory.getTasks( socId ).toPromise();
        let occupationInterestCodes = this.OccufindRestFactory.getOccupationInterestCodes( socId ).toPromise();
        let educationLevel = this.careerClusterRestFactory.getEducationLevel( socId ).toPromise();
        let relatedOccupations = this.careerClusterRestFactory.getRelatedOccupations( socId ).toPromise();
        let hotGreenStemFlags = this.OccufindRestFactory.getHotGreenStemFlags( socId ).toPromise();
        let servicesOffering = this.OccufindRestFactory.getServiceOfferingCareer( socId ).toPromise();
        let relatedCareerCluster = this.OccufindRestFactory.getRelatedCareerCluster( socId ).toPromise();
        let moreResources = this.OccufindRestFactory.getMoreResources( socId ).toPromise();
        let schoolDetails = this.OccufindRestFactory.getSchoolMoreDetails( socId ).toPromise();
        let getAlternateView = this.OccufindRestFactory.getAlternateView( socId ).toPromise();
        let imageAltText = this.OccufindRestFactory.getImageAltText( socId ).toPromise();
        let jobZone = this.OccufindRestFactory.getJobZone( socId ).toPromise();
        let certificateExplanation = this.OccufindRestFactory.getCertificateExplanation( onetSocTrimmed ).toPromise();
        let alternateTitles = this.OccufindRestFactory.getAlternateTitles( socId ).toPromise();
        let stateSalaryYear = this.OccufindRestFactory.getStateSalaryYear().toPromise();
        let workValues = this.OccufindRestFactory.getOccupationWorkValues(socId).toPromise();



        //TODO:
        //let noteList = this.NotesService.getNotes();
        
        let favorites;
        if ( this._user.getUser() === undefined ) {
            favorites = undefined;
        } else {
            favorites = this.favoritesRestFactoryService.getFavoriteOccupation();
        }
             
            
        return Promise.all( [occupationCertificates,
            occupationTitleDescription,
            stateSalary,
            stateEntrySalary,
            nationalSalary,
            nationalEntrySalary,
            blsTitle,
            skillImportance,
            tasks,
            occupationInterestCodes,
            educationLevel,
            relatedOccupations,
            hotGreenStemFlags,
            servicesOffering,
            relatedCareerCluster,
            moreResources,
            schoolDetails,
            getAlternateView,
            imageAltText,
            jobZone,
            certificateExplanation,
            alternateTitles,
            favorites,
            stateSalaryYear,
            workValues
        ] );
    }
}
