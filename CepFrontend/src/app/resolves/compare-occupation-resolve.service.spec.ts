import { TestBed } from '@angular/core/testing';

import { CompareOccupationResolveService } from './compare-occupation-resolve.service';

describe('CompareOccupationResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompareOccupationResolveService = TestBed.get(CompareOccupationResolveService);
    expect(service).toBeTruthy();
  });
});
