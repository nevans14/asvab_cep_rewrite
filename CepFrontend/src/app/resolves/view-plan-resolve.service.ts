import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class ViewPlanResolveService {

  constructor(private _userOccupationPlanService: UserOccupationPlanService,
    private _occufindRestFactory: OccufindRestFactoryService,
    private _portfolioRestFactory: PortfolioRestFactoryService,
    private _userService: UserService) { }

  async resolve(route: ActivatedRouteSnapshot) {

    const planId = route.params.id;

    let userOccupationPlan: any = await this._userOccupationPlanService.get(planId);

    let occupationDescription: any = await this._occufindRestFactory
      .getOccupationTitleDescription(userOccupationPlan.socId).toPromise().then((success: any) => {
        return success.title;
      }).catch(error => {
        console.error("Error", error);
      });

    let militaryOccupationTitles = await this._occufindRestFactory.getMilitaryMoreDetails(userOccupationPlan.socId).toPromise().then((response: any) => {
      return response;
    }).catch(error => {
      console.error("Error", error);
    });

    let favorites = await this.getCitmFavoriteOccupations();
		let userName = await this._userService.getUsername();
    

    return Promise.all([userOccupationPlan, occupationDescription, militaryOccupationTitles, favorites, userName]);

  }


  async getCitmFavoriteOccupations() {
    try {
      let response = await this._portfolioRestFactory.getCitmFavoriteOccupationsNF();
      return await response;
    } catch (error) {
      console.error("ERROR", error);
    }

  }

}
