import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { OccufindOccupationSearchService } from 'app/services/occufind-occupation-search.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';

@Injectable( {
    providedIn: 'root'
} )
export class OccupationSearchResultsResolveService implements Resolve<any>  {

  constructor(
      private _user: UserService,
      private _occuFindRestFactory: OccufindRestFactoryService,
      private _occuFindOccupationSearchService: OccufindOccupationSearchService,
      private _favoritesRestFactoryService: FavoritesRestFactoryService,
      private _utilityService: UtilityService,
      ) {
  }

  resolve( route: ActivatedRouteSnapshot ) {

    let occupationResults = undefined;
    let favorites = undefined;
    let onetVersion = undefined;

    occupationResults = this._occuFindRestFactory.getOccupationResults(this._occuFindOccupationSearchService.userSearch);
    onetVersion = this._utilityService.getByName('onet_version').toPromise();

    if (this._user.getUser() == undefined) {
      return Promise.all([
        occupationResults,
        favorites,
        onetVersion,
      ])
    } else {
      favorites = new Promise((resolve, reject) => {
        this._favoritesRestFactoryService.getFavoriteOccupation().then(
          res => {
            resolve(res)
          }, 
          err => {
            reject(err)
          }
        )
      })
      return Promise.all([
        occupationResults,
        favorites,
        onetVersion,
      ])
    }
  }
}
