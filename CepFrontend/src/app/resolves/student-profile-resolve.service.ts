import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ClassroomActivitySubmission } from 'app/core/models/classroomActivity';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MentorService } from 'app/services/mentor.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class StudentProfileResolveService implements Resolve<any> {

  constructor(private _mentorService: MentorService,
    private _portfolioService: PortfolioService,
    private _classroomActivityService: ClassroomActivityService,
    private _userService: UserService,
    private _userOccupationPlanService: UserOccupationPlanService) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    //get all mentors
    let mentors = this._mentorService.getAll();

    //get portfolio
    let portfolio = this._portfolioService.getPortfolio();

    //get activity comments
    let activityComments = this._classroomActivityService.getAllActivityComments();

    //get activities
    let activities: any = await this._classroomActivityService.getActivities();
    await Promise.all(activities.map(async (activity: any) => {

      await Promise.all(activity.submissions.map(async (classroomActivitySubmission: ClassroomActivitySubmission) => {
        //for each submission retrieve its logs
        let activityLogs: any = await this._classroomActivityService.getMySubmissionLogs(classroomActivitySubmission.id);
        classroomActivitySubmission.logs = activityLogs;
      }))

    }))

    //get direct messages
    let directMessages = await this._userService.getDirectMessages();

    //get occupation plans
    let occuPlans: any = await this._userOccupationPlanService.getAll();
    await Promise.all(occuPlans.map(async (plan: any) => {

      //for each submission retrieve its logs
      let comments: any = await this._userOccupationPlanService.getComments(plan.id);
      plan.comments = comments;

    }))


    let userName = this._userService.getUsername();

    return Promise.all([mentors, portfolio, activityComments, activities, directMessages, userName, occuPlans]);
  }

}
