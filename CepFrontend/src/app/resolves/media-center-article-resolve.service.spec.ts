import { TestBed } from '@angular/core/testing';

import { MediaCenterArticleResolveService } from './media-center-article-resolve.service';

describe('MediaCenterArticleResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediaCenterArticleResolveService = TestBed.get(MediaCenterArticleResolveService);
    expect(service).toBeTruthy();
  });
});
