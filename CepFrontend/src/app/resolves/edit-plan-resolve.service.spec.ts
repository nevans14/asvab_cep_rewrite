import { TestBed } from '@angular/core/testing';

import { EditPlanResolveService } from './edit-plan-resolve.service';

describe('EditPlanResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditPlanResolveService = TestBed.get(EditPlanResolveService);
    expect(service).toBeTruthy();
  });
});
