import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';
import { NotesRestFactoryService } from 'app/services/notes-rest-factory.service';

@Injectable( {
    providedIn: 'root'
} )
export class CompareOccupationResolveService implements Resolve<any>  {
    restURL: any;
    socIds = [];

    constructor(
        private _http: HttpClient,
        _config: ConfigService,
        private _user: UserService,
        private _occufindRestFactory: OccufindRestFactoryService,
        private _careerClusterRestFactory: CareerClusterRestFactoryService,
        private _notesRestFactory: NotesRestFactoryService,
        private _activatedRoute: ActivatedRoute,
    ) {
        this.restURL = _config.getRestUrl() + '/';
    }

    resolve( route: ActivatedRouteSnapshot ) {

            let allResponses = [];
            Object.keys(route.params).forEach(key => {
                if (key && route.params[key] !== 'null') {
                    this.socIds.push(route.params[key]);
                    const hotGreenStemFlags = this._occufindRestFactory.getHotGreenStemFlags(route.params[key]).toPromise();
                    const socId = route.params[key];
                    const title = this._occufindRestFactory.getOccupationTitleDescription(socId).toPromise();
                    const skillImportance = this._careerClusterRestFactory.getSkillImportance(socId).toPromise();
                    const occupationInterestCodes = this._occufindRestFactory.getOccupationInterestCodes(socId).toPromise();
                    const educationLevel = this._careerClusterRestFactory.getEducationLevels(socId).toPromise();
                    const servicesOffering = this._occufindRestFactory.getServiceOfferingCareer(socId).toPromise();
                    const onetSocTrimmed = socId.slice( 0, -3 );
                    const nationalSalary = this._occufindRestFactory.getNationalSalary(onetSocTrimmed).toPromise();
                    const notes = this._notesRestFactory.getOccupationNotes({socId: socId});
                    const tasks = this._careerClusterRestFactory.getTasks(socId).toPromise();
                    const favoriteList = this._user.getFavoritesList();
                    const certificateExplanation = this._occufindRestFactory.getCertificateExplanation( onetSocTrimmed ).toPromise();

                    allResponses.push(Promise.all([title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes, socId, hotGreenStemFlags, tasks, favoriteList, certificateExplanation]));
                }
            })

            return Promise.all(allResponses);
    }
}
