import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UserService } from 'app/services/user.service';

@Injectable( {
    providedIn: 'root'
} )
export class AsvabScoreResolveService implements Resolve<any>{

    constructor( private _httpHelper: HttpHelperService,
        private _user: UserService ) { }

    resolve() {
        return this._httpHelper.httpHelper( 'GET', 'portfolio/ASVAB', null, null );
    }

}
