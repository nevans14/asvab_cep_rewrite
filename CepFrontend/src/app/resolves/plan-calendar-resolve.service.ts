import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';

@Injectable({
  providedIn: 'root'
})
export class PlanCalendarResolveService {

  constructor(
    private _planCalendarService: PlanCalendarService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _occufindRestFactory: OccufindRestFactoryService) { }

  resolve(route: ActivatedRouteSnapshot) {

    //get tasks
    let userOccupationPlanCalendarTasks = this.getAllUserOccupationPlanCalendarTasks();

    //get all user dates
    let userOccupationPlanCalendars = this.getAllUserOccupationPlanCalendars();

    //get all plans 
    let userOccupationPlans = this.getUserOccupationPlans();

    return Promise.all([userOccupationPlanCalendarTasks, userOccupationPlanCalendars, userOccupationPlans]);

  }

  async getAllUserOccupationPlanCalendarTasks() {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getTasks();

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getAllUserOccupationPlanCalendars() {

    try {

      let uerOccupationPlanCalendars = await this._planCalendarService.getCalendars();

      return await uerOccupationPlanCalendars;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getUserOccupationPlans() {

    try {

      let userOccupationPlans: any = await this._userOccupationPlanService.getAll();

      return Promise.all(userOccupationPlans.map(async (userOccupationPlan: any) => {

        let occuTitle = await this._occufindRestFactory
          .getOccupationTitleDescriptionNF(userOccupationPlan.socId);

        return await {...userOccupationPlan, occuTitle};

      }));

    } catch (error) {
      console.error("ERROR", error);
    }

  }

}
