import { TestBed } from '@angular/core/testing';

import { OccupationSearchResultsResolveService } from './occupation-search-results-resolve.service';

describe('OccupationSearchResultsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccupationSearchResultsResolveService = TestBed.get(OccupationSearchResultsResolveService);
    expect(service).toBeTruthy();
  });
});
