import { TestBed } from '@angular/core/testing';

import { MentorDashboardResolveService } from './mentor-dashboard-resolve.service';

describe('MentorDashboardResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MentorDashboardResolveService = TestBed.get(MentorDashboardResolveService);
    expect(service).toBeTruthy();
  });
});
