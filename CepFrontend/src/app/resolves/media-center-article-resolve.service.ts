import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MediaCenterService } from 'app/services/media-center.service';

@Injectable({
  providedIn: 'root'
})
export class MediaCenterArticleResolveService implements Resolve<any> {

  constructor(private _mediaCenterService: MediaCenterService) { }

  resolve(route: ActivatedRouteSnapshot){
    if (route.params.id) {
      return this._mediaCenterService.getMediaCenterArticleById(route.params.id);
    }

    if (route.params.categoryName && route.params.slug) {
      return this._mediaCenterService.getMediaCenterArticleByCategoryNameSlug(route.params.categoryName, route.params.slug);
    }
  }
}
