import { TestBed } from '@angular/core/testing';

import { PlanCalendarResolveService } from './plan-calendar-resolve.service';

describe('PlanCalendarResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanCalendarResolveService = TestBed.get(PlanCalendarResolveService);
    expect(service).toBeTruthy();
  });
});
