import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareOccupationComponent } from './compare-occupation.component';

describe('CompareOccupationComponent', () => {
  let component: CompareOccupationComponent;
  let fixture: ComponentFixture<CompareOccupationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareOccupationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareOccupationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
