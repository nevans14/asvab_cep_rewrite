import { Component, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';
import { NotesService } from 'app/services/notes.service';
import { UserService } from 'app/services/user.service';

import $ from 'jquery'
declare var $: $

declare var pdfMake: any;
import html2canvas from "html2canvas";
import { ElementRef } from "@angular/core";
import { UtilityService } from 'app/services/utility.service';


@Component({
    selector: 'app-compare-occupation',
    templateUrl: './compare-occupation.component.html',
    styleUrls: ['./compare-occupation.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CompareOccupationComponent implements OnInit {

    // first occupation
    occupationOneTitle;
    occupationOneInterestCdOne;
    occupationOneInterestCdTwo;
    occupationOneInterestCdThree;
    occupationOneSkillRating;
    occupationOneEducation;
    occupationOneServiceOffering;
    occupationOneNationalSalary;
    occupationOneNotes;

    // second occupation
    occupationTwoTitle;
    occupationTwoInterestCdOne;
    occupationTwoInterestCdTwo;
    occupationTwoInterestCdThree;
    occupationTwoSkillRating;
    occupationTwoEducation;
    occupationTwoServiceOffering;
    occupationTwoNationalSalary;
    occupationTwoNotes;

    // third selection
    occupationThreeTitle;
    occupationThreeInterestCdOne;
    occupationThreeInterestCdTwo;
    occupationThreeInterestCdThree;
    occupationThreeSkillRating;
    occupationThreeEducation;
    occupationThreeServiceOffering;
    occupationThreeNationalSalary;
    occupationThreeNotes;
    occupationThree;

    occupationsTitle = [];
    occupationsInterestCdOne = [];
    occupationsInterestCdTwo = [];
    occupationsInterestCdThree = [];
    occupationsSkillRating = [];
    occupationsEducation = [];
    occupationsServiceOffering = [];
    occupationsNationalSalary = [];
    occupationsNotes = [];
    occupationsHotGreenStemFlags = [];
    occupationsDescription = [];
    socIds = [];
    favoriteList = [];
    error = '';
    noteList = [];
    noteChanged = [];
    scrollListIndex = [0, 1];
    rowsToPrint = [];
    certificateExplanation = [];
    shrinkCredentialsValLimit: number = 160;
    shrinkCredentialsVal = [];
    showLessHeight: number = 0;

    // Print/Export PDF element
    @ViewChildren('printList') elems: QueryList<ElementRef>;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _favoriteService: FavoritesRestFactoryService,
        private _notesService: NotesService,
        private _userService: UserService,
        private _utilityService: UtilityService
    ) {
        this._activatedRoute.snapshot.data.occupationSelectionDetails.forEach(occ => {
            this.shrinkCredentialsVal.push(this.shrinkCredentialsValLimit);
        })
    }

    ngOnInit() {

        console.log('occ', this._activatedRoute.snapshot.data.occupationSelectionDetails);

        this._activatedRoute.snapshot.data.occupationSelectionDetails.forEach(occ => {
            this.occupationsTitle.push(occ[0].title);
            this.occupationsInterestCdOne.push(occ[2].length > 0 ? occ[2][0].interestCd : "");
            this.occupationsInterestCdTwo.push(occ[2].length > 1 ? occ[2][1].interestCd : "");
            this.occupationsInterestCdThree.push(occ[2].length > 2 ? occ[2][2].interestCd : "");
            this.occupationsSkillRating.push(occ[1] ? occ[1] : { verbal: 0, math: 0, sciTech: 0 });
            this.occupationsEducation.push(occ[3]);
            this.occupationsServiceOffering.push(occ[4]);
            this.occupationsNationalSalary.push(occ[5]);
            this.occupationsNotes.push(occ[6].note);
            this.occupationsHotGreenStemFlags.push(occ[8]);
            this.socIds.push(occ[7]);
            this.occupationsDescription.push(occ[0].description)
            this.noteChanged.push(false);
            this.certificateExplanation.push(occ[11] ? (occ[11].licensing + occ[11].training) : '');
        })

        this.favoriteList = this._activatedRoute.snapshot.data.occupationSelectionDetails[
            this._activatedRoute.snapshot.data.occupationSelectionDetails.length - 1
        ][10];

        let that = this;
        this._notesService.getNotes().then(function (data: []) {
            that.noteList = [...data];
        })

        this.matchHeights();

        this.setupPrintPdf();

        this._userService.setCompletion(UserService.COMPARED_OCCUPATIONS, 'true');
    }

    scrollLeft() {
        if (this.scrollListIndex[0] > 0) {
            this.scrollListIndex.unshift(this.scrollListIndex[0] - 1);
            this.scrollListIndex.pop();
            this.matchHeights();
        }
    }

    scrollRight() {
        if (this.scrollListIndex[0] < this.socIds.length - 2) {
            this.scrollListIndex.push(this.scrollListIndex[1] + 1);
            this.scrollListIndex.shift();
            this.matchHeights();
        }
    }

    matchHeights() {
        $(document).ready(function () {

            var heights = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var i;
            for (i = 2; i <= 10; i++) {
                $("[data-match=match" + i + "]").each(function (index) {
                    if ($(this).outerHeight() > heights[i]) {
                        heights[i] = $(this).outerHeight();
                    }
                });
            }
            for (i = 1; i <= 10; i++) {
                $("[data-match=match" + i + "]").css('height', heights[i]);
            }
        });
    }

    adjustCertificationHeight(showMoreWasClicked: boolean = false) {
        let self = this;
        $(document).ready(function () {

            var currentHeight: number = 0;

            $("[data-match=match6]").each(function (index) {

                if (showMoreWasClicked) {
                    self.showLessHeight = $(this).outerHeight();

                    if ($(this)[0].scrollHeight > currentHeight) {
                        currentHeight = $(this)[0].scrollHeight;
                    }

                    if (index == 0) {
                        $(this)[0].style.width = '176px'

                    }

                } else {
                    currentHeight = self.showLessHeight;
                }

            });

            $("[data-match=match6]").css('height', currentHeight);

        });

    }

    noteUpdate(i) {
        let notesExist = false;
        let note;
        let noteListSession = null;

        noteListSession = window.sessionStorage.getItem("noteList");
        if (noteListSession !== null && noteListSession !== 'undefined') {
            const tempNote = JSON.parse(noteListSession).find(n => n.socId == this.socIds[i]);
            notesExist = !!tempNote;
            if (notesExist) {
                note = {
                    socId: tempNote.socId,
                    title: tempNote.title,
                    notes: this.occupationsNotes[i],
                    noteId: tempNote.noteId
                };
            } else {
                note = {
                    socId: this.socIds[i],
                    title: this.occupationsTitle[i],
                    notes: this.occupationsNotes[i],
                };
            }
        } else {
            const noteTemp = this.noteList.find(n => n.socId === this.socIds[i]);
            notesExist = !!noteTemp;
            if (notesExist) {
                note = {
                    socId: noteTemp.socId,
                    title: noteTemp.title,
                    notes: this.occupationsNotes[i],
                    noteId: noteTemp.noteId
                };
            } else {
                note = {
                    socId: this.socIds[i],
                    title: this.occupationsTitle[i],
                    notes: this.occupationsNotes[i],
                };
            }
        }

        let that = this;
        if (!notesExist) {
            this._notesService.insertNote(note).then(function (success) {
                that.noteChanged[i] = false;
            }, function (error) {
                that.error = "There was an error proccessing your request. Please try again."
            });
        } else {
            this._notesService.updateNote(note).then(function (success) {
                that.noteChanged[i] = false;
            }, function (error) {
                that.error = "There was an error proccessing your request. Please try again."
            });
        }
    }


    getFavOccupation(socId) {
        return this.favoriteList.filter(occ => occ.onetSocCd === socId)[0];
    }

    setOccupationHeart(numHearts, favId) {
        this.favoriteList.filter(f => {
            if (f.id === favId) {
                f['hearts'] = numHearts;
                this._favoriteService.updateFavoriteOccupationHearts({ id: favId, hearts: numHearts });
            }
        });
    }

    /**
     * retrieve canvas of a htmlelement based on its index within the #printList
     * this will equal to one row
     */
    getCanvasByIndex(index: number) {

        let self = this;

        return new Promise((resolve, reject) => {

            html2canvas(self.elems.toArray()[index].nativeElement, {
                imageTimeout: 2000,
                removeContainer: false,
                backgroundColor: '#fff',
                scale: 1,
                logging: false,
                ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
                    return node.nodeName === 'IFRAME';
                },
                allowTaint: true, //needed for font awesome; svg elements etc..
                onclone: function (cloneDoc) {

                    // show the printing section in the clone document
                    let printSection: HTMLElement = cloneDoc.getElementById("printSection");
                    printSection.style.display = 'block';

                    // Loop through data-match elements within the print section and set their heights to match
                    // Start
                    let heights = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    let i;
                    let itemCounter = 12;
                    let dataMatchElements;
                    for (i = 0; i < 10; i++) {

                        dataMatchElements = cloneDoc.body.querySelectorAll('[data-match=match' + itemCounter + ']');

                        for (let e = 0; e < dataMatchElements.length; e++) {
                            if (dataMatchElements[e].offsetHeight > heights[i]) {
                                heights[i] = dataMatchElements[e].getBoundingClientRect().height;
                            }
                        }

                        for (let e = 0; e < dataMatchElements.length; e++) {
                            dataMatchElements[e].style.height = heights[i].toString() + 'px';
                        }

                        itemCounter++;
                    }

                }
            }).then(function (canvas) {
                let splitAt = 800;

                let images = [];
                let y = 0;
                let numClippedRegions = 0;
                while (canvas.height > y) {
                    images.push(self.getClippedRegion(canvas, 0, y, canvas.width, splitAt));
                    y += splitAt;
                    numClippedRegions += 1;
                }
                images[numClippedRegions - 1].pageBreak = "after";
                resolve(images);
            }).catch(function (error) {
                reject(error);
            });

        });
    }

    setupPrintPdf() {
        //determine how many rows for printing
        let numRows = Math.round(this.socIds.length / 2);

        let itemCounter = 0;

        for (let i = 0; i < numRows; i++) {

            if (numRows == 1) {
                this.rowsToPrint.push([itemCounter, itemCounter + 1]); //at minimum 2 items have to be compared.
                return;
            }

            //is this the last row?
            //if so check if odd number of values then only add one to the multi-array
            if (i == numRows - 1) {
                if (this.socIds.length % 2 > 0) {
                    this.rowsToPrint.push([itemCounter]);
                } else {
                    this.rowsToPrint.push([itemCounter, itemCounter + 1]);
                }
            } else {
                //otherwise add 2 numbers
                this.rowsToPrint.push([itemCounter, itemCounter + 1]);
            }

            itemCounter += 2; //iterate to the next group
        }
    }

    printPdf(downloadMode = false) {

        //retrieve the rows to print and build array based on length
        let arrRows = [];
        for (let i = 0; i < this.rowsToPrint.length; i++) {
            arrRows.push(i);
        }

        //get a canvas of each row
        const promises = arrRows.map((size) => {
            return this.getCanvasByIndex(size);
        });

        let contentImages = [];

        // wait until all promises are finished
        Promise.all(promises).then((values: any) => {

            for (let i = 0; i < values.length; i++) {

                for (let g = 0; g < values[i].length; g++) {

                    if((g == values[i].length-1) && (i == values.length - 1)){
                        values[i][g].pageBreak = '';
                    }
                    contentImages.push(values[i][g]);
                }
            }

            let docDefinition = {
                pageSize: 'A2',
                content: contentImages,
                styles: {
                    header: {
                        fontSize: 24,
                        alignment: 'center',
                        bold: true
                    }
                }
            }

            if (downloadMode) {
                pdfMake.createPdf(docDefinition).download('compare.pdf');
            } else {
                pdfMake.createPdf(docDefinition).print();
            }

        });

    };


    getClippedRegion(image, x, y, width, height) {
        var canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");

        canvas.width = width;
        canvas.height = height;

        ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

        return {
            image: canvas.toDataURL(),
            width: 800,
            alignment: 'center'
        };
    }

    /**
     * IE support for printing element.
     */
    printIE(canvas) {

        canvas.then(function (img) {
            var popup = window.open();
            popup.document.write('<img src=' + img + '>');
            popup.document.close();
            popup.focus();
            popup.print();
            popup.close();

        }, function (error) {

            alert('Printing failed.');
        });

    }

    downloadPdf = function () {
        this.printPdf(true);
    }

}
