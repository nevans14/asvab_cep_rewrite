import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'skill'
})
export class SkillPipe implements PipeTransform {

  transform(values: any[], skillImportance: string, view: string): any {

    let filtered = [];

      if (skillImportance == 'M') {
        if (view == 'hi') {
          values.forEach(v => {
            if ((v.mathSkill >= 4 && v.mathSkill <= 5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'md') {
          values.forEach(v => {
            if ((v.mathSkill >= 2.5 && v.mathSkill <= 3.5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'lw') {
          values.forEach(v => {
            if ((v.mathSkill >= 0 && v.mathSkill <= 2)) {
              filtered.push(v);
            }
          });
        }
      } else if (skillImportance == 'V') {
        if (view == 'hi') {
          values.forEach(v => {
            if ((v.verbalSkill >= 4 && v.verbalSkill <= 5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'md') {
          values.forEach(v => {
            if ((v.verbalSkill >= 2.5 && v.verbalSkill <= 3.5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'lw') {
          values.forEach(v => {
            if ((v.verbalSkill >= 0 && v.verbalSkill <= 2)) {
              filtered.push(v);
            }
          });
        }
      } else if (skillImportance == 'S') {
        if (view == 'hi') {
          values.forEach(v => {
            if ((v.sciTechSkill >= 4 && v.sciTechSkill <= 5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'md') {
          values.forEach(v => {
            if ((v.sciTechSkill >= 2.5 && v.sciTechSkill <= 3.5)) {
              filtered.push(v);
            }
          });
        } else if (view == 'lw') {
          values.forEach(v => {
            if ((v.sciTechSkill >= 0 && v.sciTechSkill <= 2)) {
              filtered.push(v);
            }
          });
        }
      } else {
        return values;
      }
  
      return filtered;
    }
}
