import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mentorDashboardStudentFilter',
  pure: false
})
export class MentorDashboardStudentFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (!value) return null;

    if (!args) return value;

    let filteredItems = [];
    if (args) {

      let items = <Array<any>>value;

      items.forEach((item: any) => {

        //automatically skip record if they are looking for students who belong in a grp and they dont' have a grp
        if (args.groups && args.groups.length > 0) {
          if (!item.group) { return };
        }

        if (args.genderFilter) {
          if (item.gender) {
            let studentGender = (item.gender.toLocaleUpperCase() === "M") ? "Male" : "Female";

            if (args.genderFilter.toUpperCase() !== studentGender.toUpperCase()) {
              return;
            }
          } else {
            return;
          }
        }

        if (args.asvabAdminMonth) {
          if (item.administeredMonth) {
            if (args.asvabAdminMonth !== item.administeredMonth) {
              return;
            }
          } else {
            return;
          }
        }

        if (args.asvabAdminYear) {
          if (item.administeredYear) {
            if (args.asvabAdminYear !== item.administeredYear) {
              return;
            }
          } else {
            return;
          }
        }

        if (args.studentName) {
          if (item.firstName && item.lastName) {

            let fullName: string = item.firstName + ' ' + item.lastName;

            if (fullName.toUpperCase().indexOf(args.studentName.toUpperCase()) == -1) {
              return;
            }
          } else {
            return;
          }
        }

        if (args.educationLevels && args.educationLevels.length > 0) {

          if (item.grade) {
            if (args.educationLevels.filter(x => x == item.grade).length == 0) {
              return;
            }
          } else {
            return;
          }

        }

        if (args.graduationDates && args.graduationDates.length > 0) {

          if (item.grade) {
            //determine years
            let gradDates = "";
            let currentFullYear = new Date().getFullYear();
            if (item.grade.toString().indexOf('12') > -1) {
              gradDates = (currentFullYear + ' - ' + (currentFullYear + 1));
            }

            if (item.grade.toString().indexOf('11') > -1) {
              gradDates = (currentFullYear + 1) + ' - ' + (currentFullYear + 2);
            }

            if (item.grade.toString().indexOf('10') > -1) {
              gradDates = (currentFullYear + 2) + ' - ' + (currentFullYear + 3);
            }

            if (args.graduationDates.filter(x => x == gradDates).length == 0) {
              return;
            }
          } else {
            return;
          }

        }

        filteredItems.push(item);

      })
    }

    return filteredItems;
  }

}
