import { Pipe, PipeTransform } from '@angular/core';
import { RefGapYearPlan } from './models/refGapYearPlan.model';

@Pipe({
  name: 'gapYearPlanFilter',
  pure: false
})
export class GapYearPlanFilterPipe implements PipeTransform {

  transform(gapYearOptions: RefGapYearPlan[], filters: string[]): any {

    //nothing to filter
    if (!gapYearOptions) {
      return null;
    }

    //return empty list 
    if (gapYearOptions.length == 0) {
      return gapYearOptions;
    }

    //do filters exist?
    if (!filters || filters.length == 0) {
      return gapYearOptions
    }

    let hasStudyAbroadFilter: boolean = this.filtersContain(filters, 'isStudyAbroad');
    let hasWorkAbroadFilter: boolean = this.filtersContain(filters, 'isWorkAbroad');
    let hasInternshipFilter: boolean = this.filtersContain(filters, 'isInternship');
    let hasVolunteerFilter: boolean = this.filtersContain(filters, 'isVolunteer');
    let hasTravelFilter: boolean = this.filtersContain(filters, 'isTravel');

    return gapYearOptions.filter(function (item: any) {
      if (hasStudyAbroadFilter && item.studyAbroad) {
        return item;
      }
      if (hasWorkAbroadFilter && item.workAbroad) {
        return item;
      }
      if (hasInternshipFilter && item.internship) {
        return item;
      }
      if (hasVolunteerFilter && item.volunteer) {
        return item;
      }
      if (hasTravelFilter && item.travel)  {
        return item;
      }
    });

  }

  filtersContain(filters: string[], filter: string): boolean {

    return filters.indexOf(filter) > -1;

  }

}
