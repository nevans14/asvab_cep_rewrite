import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isAccPresent'
})
export class IsAccPresentPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var returnValue = '';
    var accList;
    if (typeof value === "undefined" || !value || value.length == 0 ) {
       return '';
    }
    
    if ( typeof value === "string" )  {
      accList = value.split(",");
    } else {
      accList = new Array();
      accList.push(value);
    }

    for (var i = 0; i < accList[0].length; i++) {
      if (accList[0][i].Name.toLowerCase() == 'ncca') {  returnValue = returnValue + '<img src="assets/images/icons/ncca-accredited.png" height="16" width="16" >' }
      if (accList[0][i].Name.toLowerCase() == 'ansi') {  returnValue = returnValue + '<img src="assets/images/icons/ansi-accredited.png" height="16" width="16" >' }
      if (accList[0][i].Name.toLowerCase() == 'job corps') {  returnValue = returnValue + '<img src="assets/images/icons/job-corps.png" height="16" width="16"  >' }
      if (accList[0][i].Name.toLowerCase() == 'nam') {  returnValue = returnValue + '<img src="assets/images/icons/third-party-industry-endorsed-cert.png" height="16" width="16" >' }
      if (accList[0][i].Name.toLowerCase() == 'military') {  returnValue = returnValue + '<img src="assets/images/icons/military-occ-specialties.png" height="16" width="16" >' }
      if ( accList[0][i].Name.toLowerCase() == 'in-demand') {  returnValue = returnValue + '<img src="assets/images/icons/in-demand-3.png" height="16" width="16" >' }
    }

    return  returnValue;
  }
}
