import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpSessionInterceptor implements HttpInterceptor {

  constructor(
    private _router: Router,
    private _userService: UserService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {

          if (!this._router.url.includes('faq')
            && !event.url.includes('ping')
            && !event.url.includes('config.json') //intereceptor won't be called on init
            && !event.url.includes('logout') //wont' be called on it logout
            && (event.url.includes('amazonaws')
              || event.url.includes('CEP/rest')
            )
          ) {

            let loggedIn = this._userService.isLoggedIn();

            if (loggedIn) {
              this._userService.getKeepAlivePing().subscribe((result: any) => {
                if (result.status == 200) {
                  //keep alive existing session or start a new one
                  this._userService.startSession();
                }
              });
            }

          }

        }
        return event;

      }));

  };

}
