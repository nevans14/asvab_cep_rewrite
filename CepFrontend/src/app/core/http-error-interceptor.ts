import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorInterceptor implements HttpInterceptor {

  pathList = [
    { url: '/training-registration-form' },
    {url: '/faq' },
    {url: '/plan-your-future'},
    {url: '/portfolio'},
    {url: '/view-plan'},
    {url: '/student-profile'}
  ]

  apiList = [
    {
      api: 'contactUs/scheduleEmail',
    }
  ]

  constructor(
    private _router: Router,
    private _userService: UserService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          }

          if (error.status == 403) {
            console.debug("403 encountered...redirecting...");
            this._userService.logOff();
            return throwError(error);
          }
          //get current path; if current path is in list then return full error
          const currentPath = this._router.url;
          if (this.currentPathInPathList(currentPath) || this.currentPathInAPIList(error)) {
            return throwError(error);
          }

          //errorMessage = this.createErrorMsg(error)
          // window.alert(errorMessage);
          return throwError(errorMessage);
        })
      )
  }

  createErrorMsg(error) {
    let msg = error['message'] && error['message'] !== "null" ? error['message'] : error['error'];
    msg = msg ? msg : error['statusText'];
    msg = error['status'] === 404 ? 'This site can\'t be reached' : msg;
    msg = msg ? msg : 'Service is not available currently.';
    msg = msg.replace(new RegExp('^[0-9]{3} \w*'), '');
    return msg;
  }

  /**
   * Checks url against a list; if url is present return true
   * @param path url to check
   */
  currentPathInPathList(path): boolean {
    let foundPath = this.pathList.find(x => path.match(x.url));
    return (foundPath) ? true : false;
  }

  currentPathInAPIList(error) {
    return error && error.url && this.apiList.find(x => error.url.includes(x.api)) ? true : false;
  }

}