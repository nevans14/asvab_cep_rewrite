import { Pipe, PipeTransform } from '@angular/core';


enum workValueType {
  Achievement = 1,
  Independence,
  Recognition,
  Relationships,
  Support,
  WorkingConditions
}

@Pipe({
  name: 'workValue'
})
export class WorkValuePipe implements PipeTransform {

  transform(values: any[], checkboxWorkValues: any, workValueItems: any): any {

    if (!values) {
      return [];
    }

    let valuesSelected = [];

    for (var y = 0; y < checkboxWorkValues.length; y++) {
      //workValueItems['A']:true
      if (workValueItems[checkboxWorkValues[y].code]) {

        if (checkboxWorkValues[y].code == 'A') {
          valuesSelected.push(1)
        }

        if (checkboxWorkValues[y].code == 'I') {
          valuesSelected.push(2)
        }

        if (checkboxWorkValues[y].code == 'R') {
          valuesSelected.push(3)
        }

        if (checkboxWorkValues[y].code == 'E') {
          valuesSelected.push(4)
        }

        if (checkboxWorkValues[y].code == 'S') {
          valuesSelected.push(5)
        }

        if (checkboxWorkValues[y].code == 'W') {
          valuesSelected.push(6)
        }

      }
    }

    if (valuesSelected.length == 0) {
      return values;
    }

    return values.filter(v => {

      if(valuesSelected.length == 1){
        if (valuesSelected.indexOf(v.workValueOne) > -1) {
          return true;
        }
        if (valuesSelected.indexOf(v.workValueTwo) > -1) {
          return true;
        }
        if (valuesSelected.indexOf(v.workValueThree) > -1) {
          return true;
        }
      }else if(valuesSelected.length == 2){
        if ((valuesSelected.indexOf(v.workValueOne) > -1) && (valuesSelected.indexOf(v.workValueTwo) > -1)){
          return true;
        }
        if ((valuesSelected.indexOf(v.workValueOne) > -1) && (valuesSelected.indexOf(v.workValueThree) > -1)){
          return true;
        }
        if ((valuesSelected.indexOf(v.workValueTwo) > -1) && (valuesSelected.indexOf(v.workValueThree) > -1)){
          return true;
        }
      }else{
        if ((valuesSelected.indexOf(v.workValueOne) > -1) && 
            (valuesSelected.indexOf(v.workValueTwo) > -1) && 
            (valuesSelected.indexOf(v.workValueThree) > -1)){
          return true;
        }
      }
      
      return false;
    });

  }

}
