import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'interest',
  pure: false,
})
export class InterestPipe implements PipeTransform {

  transform(values: any[], checkboxInterest: any, filterItems: any, filterItemsTwo: any, filterItemsThree: any): any {

    if (!values) {
      return [];
    }

    let filtered = values.filter(v => {
      for (var i = 0; i < checkboxInterest.length; i++) {
        if (filterItems[checkboxInterest[i].code]
          || (filterItemsTwo && filterItemsTwo[checkboxInterest[i].code])
          || (filterItemsThree && filterItemsThree[checkboxInterest[i].code])) {
          switch (checkboxInterest[i].code) {
            case 'A':
              if (v.interestCdOne != 'A' && v.interestCdTwo != 'A' && v.interestCdThree != 'A') return false;
              break;
            case 'C':
              if (v.interestCdOne != 'C' && v.interestCdTwo != 'C' && v.interestCdThree != 'C') return false;
              break;
            case 'E':
              if (v.interestCdOne != 'E' && v.interestCdTwo != 'E' && v.interestCdThree != 'E') return false;
              break;
            case 'I':
              if (v.interestCdOne != 'I' && v.interestCdTwo != 'I' && v.interestCdThree != 'I') return false;
              break;
            case 'R':
              if (v.interestCdOne != 'R' && v.interestCdTwo != 'R' && v.interestCdThree != 'R') return false;
              break;
            case 'S':
              if (v.interestCdOne != 'S' && v.interestCdTwo != 'S' && v.interestCdThree != 'S') return false;
              break;
          }
        }
      }
      return true;
    })
    return filtered;
  }
}
