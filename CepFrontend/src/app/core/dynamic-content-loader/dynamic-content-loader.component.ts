import {
  Compiler, Component, Input, NgModule, NgModuleFactory,
  ViewChild, OnInit, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA
} from '@angular/core';
import { CommonModule, ViewportScroller } from '@angular/common';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { ViewDetailDialogComponent } from 'app/core/dialogs/view-detail-dialog/view-detail-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { AsvabZoomDialogComponent } from 'app/core/dialogs/asvab-zoom-dialog/asvab-zoom-dialog.component';
import { AsvabSampleTestDialogComponent } from 'app/core/dialogs/asvab-sample-test-dialog/asvab-sample-test-dialog.component';
import { AsvabIcatSampleTestDialogComponent } from 'app/core/dialogs/asvab-icat-sample-test-dialog/asvab-icat-sample-test-dialog.component';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { ContactUsService } from 'app/services/contact-us.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { MediaCenterService } from 'app/services/media-center.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from 'app/material.module';
import { ReCaptcha2Component, NgxCaptchaModule } from 'ngx-captcha';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { ScheduleAsvabComponent } from '../dialogs/schedule-asvab/schedule-asvab.component';
import { ScoreRequestComponent } from 'app/contact-us/score-request.component';
import { RegisterDialogComponent } from '../dialogs/register-dialog/register-dialog.component';

// import { HeaderComponent } from '../../core/page-partials/header/header.component';
// import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-dynamic-content-loader',
  template: '<ng-container *ngComponentOutlet="dynamicComponent;ngModuleFactory: dynamicModule;"></ng-container>',
  styleUrls: ['./dynamic-content-loader.component.scss']
})
export class DynamicContentLoaderComponent implements OnInit {

  dynamicComponent: any;
  dynamicModule: NgModuleFactory<any>;

  @Input() text: string;
  @Input() parent: any;

  constructor(
    private compiler: Compiler,
  ) { }

  ngOnInit() {
    this.createDynamicComponent();
  }

  ngOnChanges() {
    this.createDynamicComponent();
  }

  createDynamicComponent() {
    if (this.text === undefined || this.text === null) {
      this.text = '<div></div>';
    }
    this.dynamicComponent = this.createNewComponent(this.text, this.parent);
    this.dynamicModule = this.compiler.compileModuleSync(
      this.createComponentModule(this.dynamicComponent));
  }

  protected createComponentModule(componentType: any) {
    @NgModule({
      imports: [MaterialModule,
        CommonModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCaptchaModule],
      declarations: [
        componentType,

      ],
      entryComponents: [componentType],
      providers: [
        // ],
        // schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA
      ]
    })
    class RuntimeComponentModule {
    }
    // a module for just this Type
    return RuntimeComponentModule;
  }

  protected createNewComponent(text: string, parent: any) {
    const template = text;
    // console.debug('template:', template);

    @Component({
      selector: 'app-dynamic-component',
      template: template,
      styleUrls: ['./dynamic-content-loader.component.scss']
    })
    class DynamicComponent implements OnInit {
      text: any;
      resource = this._config.getImageUrl();
      resources = this.resource + 'static/';
      // documents = this.resource + 'pdf/';
      documents = this.resource + 'CEP_PDF_Contents/';
      selectedTabId = 'first';
      panelOpenState = false;
      studentFormGroup: FormGroup;

      // objects
      mediaCenterList: any;

      /**
       * Bring ASVAB to your school form implementation.
       */
      formDisabled = false;
      errorSharing = '';
      responseOne = '';
      responseTwo = '';
      recaptchaResponse = '';
      recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
      @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
      counselorEmail = '';
      studentHeardUs = '';
      studentHeardUsOther = '';
      path = '';

      urlPrefix = 'https://dev-media.asvabprogram.com/OCCUFIND_IMGS/'
      urlSuffix = '_IMG.jpg';
      url = '/occufind-occupation-details/';

      constructor(
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _config: ConfigService,
        private _userService: UserService,
        private _contactUsService: ContactUsService,
        private _mediaCenter: MediaCenterService,
        private _router: Router,
        private _dialog: MatDialog,
        private _formBuilder: FormBuilder,
        private viewPortScroller: ViewportScroller,
      ) { }

      ngOnInit() {
        this.text = text;
        this._mediaCenter.getMediaCenterList().then(
          data => {
            this.mediaCenterList = data;
          });
        this.path = this._router.url;
        this.studentFormGroup = this._formBuilder.group({
          recaptcha: [{ value: '', disabled: this.formDisabled }, [Validators.required]],
          counselorEmail: [{ value: '', disabled: this.formDisabled }, Validators.required],
        });
      }

      showWorkValueModal(valueName) {
        var description = null;
        switch(valueName) {
          case 'Achievement':
            description = 'Workers who score high on Achievement are results-oriented. These workers often pursue jobs where employees are able to apply their strengths and abilities. This gives the employee a sense of accomplishment.';
            break;
          case 'Independence':
            description = 'Workers who score high on Independence value the ability to approach work activities with creativity. These workers want to make their own decisions and plan their work with little supervision from a manager.';
            break;
          case 'Recognition':
            description = 'Workers who score high on Recognition pursue jobs with opportunities for advancement and leadership responsibilities that allow them to give direction and instruction to others. These workers are often considered prestigious by their peers and others in their organization and receive recognition for the work they contribute.';
            break;
          case 'Relationships':
            description = 'Workers who score high on Relationships prefer jobs that provide services to others and working with co-workers in a friendly, non-competitive environment. Workers in these jobs value getting along well with others and do not like to be pressured to do things that go against their morals or sense of what is right and wrong.';
            break;
          case 'Support':
            description = "Workers who score high on Support appreciate when their company's leadership stands behind and supports their employees. People in these types of jobs like to feel like they are being treated fairly by the company and have supervisors who spend time and effort training their workers to perform well.";
            break;
          case 'Working Conditions':
            description = 'Workers who score high on Working Conditions value job security and pleasant working conditions. These workers enjoy being busy and want to be paid well for the work they do. They enjoy developing ways of doing things with little or no supervision and depend on themselves to get the work done. These workers pursue steady employment that offers something different to do on a daily basis.';
            break;
        }
    
        const data = {
          'title': valueName,
          'message': description,
        };
    
        const dialogRef1 = this._dialog.open( MessageDialogComponent, {
            data: data,
            //hasBackdrop: true,
            //disableClose: true,
            maxWidth: '800px'
        });
      }

      isLoggedIn() {
        return this._userService.isLoggedIn();
      }

      redirect(path) {
        if ((path.indexOf('student') ||
          path.indexOf('parents') ||
          path.indexOf('educators') > -1) &&
          parent) {
          parent.getPageByName(path);
        }

        this._router.navigate([path]);
      }


      changeTab(evt) {
        this.selectedTabId = evt.nextId;
        console.debug('tab:', this.selectedTabId);
      }

      mediaCenterItem(fld, item) {
        return this.mediaCenterList.filter(it => it[fld] === item);
      }

      mediaCenterItem2(fld1, item1, fld2, item2) {
        const newMediaCenterList = this.mediaCenterList.filter(it => it[fld1] === item1);

        return newMediaCenterList.filter(it => it[fld2] === item2);
      }

      trackClick(param): void {
        this._googleAnalyticsService.trackClick(param);
      }

      trackSampleTestClick(param): void {
        this._googleAnalyticsService.trackSampleTestClick(param);
      }
      
      trackSocialShare(socialPlug, param): void {
        this._googleAnalyticsService.trackSocialShare(socialPlug, param);
      }

      showLoginModal() {
        this._dialog.open(LoginDialogComponent, {
        });
      }

      /**
       * Show teacher registration modal
       */
      openRegistrtionModal() {

        this._dialog.open(RegisterDialogComponent, {
          hasBackdrop: true,
          panelClass: 'custom-dialog-container',
          height: '95%',
          autoFocus: true,
          disableClose: true
      });

      }

      goToAnchor(route, anchor) {
        this._router.navigate(['/'+route], {fragment: anchor});
      }

      scrollToAnchor(elementId) {
        this.viewPortScroller.scrollToAnchor(elementId);
      }

      /**
       * Show sample test modal.
       */
      showSampleTestModal() {
        // ASVABSampleTestModalService.show({}, {});
        const dialogRef2 = this._dialog.open(AsvabSampleTestDialogComponent, {
          hasBackdrop: true,
          panelClass: 'custom-dialog-container',
          height: '95%',
          autoFocus: true
        });
      }

      showSampleIcatTestModal() {
        // ASVABICATSampleTestModalService.show({}, {});
        const dialogRef2 = this._dialog.open(AsvabIcatSampleTestDialogComponent, {
          hasBackdrop: true,
          panelClass: 'custom-dialog-container',
          height: '95%',
          autoFocus: true
        });
      }

      /**
       * Show Bring the ASVAB CEP to Your School modal
       */
      asvabModal() {
        const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
          hasBackdrop: true,
          panelClass: 'new-modal',
          // height: '735px',
          autoFocus: false,
          disableClose: true
        });
        // const dialogRef2 = this._dialog.open(ScheduleAsvabComponent , {
        // });

        this._googleAnalyticsService.trackScheduleClick();
      }

      /**
       * Show video modal
       */
      showVideoDialog(videoName) {
        this._dialog.open(PopupPlayerComponent, {
          data: { videoId: videoName }
        });
      }

      showViewDetailModal() {
        if (this.isLoggedIn()) {
          window.location.href = '/occufind-occupation-search';
        } else {
          this._dialog.open(ViewDetailDialogComponent , {
            hasBackdrop: true,
            panelClass: 'new-modal',
            // height: '95%',
            width: '300px',
            autoFocus: true,
            disableClose: true
          });
        }
      }

      trackScheduleClick() {
        this._googleAnalyticsService.trackScheduleClick();
      }

      imageZoom() {
        // ASVABImageZoom.show({},{});
        const dialogRef2 = this._dialog.open(AsvabZoomDialogComponent, {
        });
      }

      shareWithCounselor() {

        this.formDisabled = true;
        this.errorSharing = undefined;
        const email = this.studentFormGroup.value.counselorEmail;

        // if (this.responseOne === '') { // if string is empty
        if (this.studentFormGroup.value.recaptcha === '') {  // if string is empty
          this.errorSharing = 'Please resolve the captcha and submit!';
          this.formDisabled = false;
          return false;
        } else {

          const emailObject = {
            email: email,
            recaptcha: this.studentFormGroup.value.recaptcha,
            // studentHeardUs: this.studentHeardUs,
            // studentHeardUsOther: this.studentHeardUsOther,
            // recaptcha: this.responseOne
          }
          this._contactUsService.shareWithCounselor(emailObject).then(
            data => {
              // Track event in Google Analytics.
              this._googleAnalyticsService.trackClick('#BRING_ASVAB_FORM_STUDENT_SHARE');

              this.studentFormGroup.setValue({ counselorEmail: '', recaptcha: '' });
              this.captchaElem.reloadCaptcha();

              const modalOptions = {
                title: 'Schedule Notification',
                message: 'Your information was submitted successfully.'
              };

              const dialogRef1 = this._dialog.open(MessageDialogComponent, {
                data: modalOptions,
                hasBackdrop: true,
                disableClose: true
              });

              this.formDisabled = false;
            }, error => {
              const error1 = error.data.errorMessage;
              if (error1 === undefined) {
                this.errorSharing = 'There was an error processing your request. Please try again.';
                this.captchaElem.reloadCaptcha()
              } else {
                this.errorSharing = error1;
              }
              this.formDisabled = false;
            });
        }

      }

      back() {
        window.history.back();
      }

      scrollTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }

      openScoreRequestModal() {
        const dialogRef = this._dialog.open(ScoreRequestComponent, {
        });
      }

      filter(items, criteria, returnAttribute = null) {
        if (items) {
          const item = items.filter(i => i[Object.keys(criteria)[0]] === Object.values(criteria)[0])[0];
          if (returnAttribute && item.hasOwnProperty(returnAttribute)) {
            return item[returnAttribute];
          } else {
            return item;
          }
        }
      }
    }
    return DynamicComponent;
  }
}
