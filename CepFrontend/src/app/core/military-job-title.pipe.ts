import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'militaryJobTitleFilter'
})
export class MilitaryJobTitlePipe implements PipeTransform {

  transform(availableJobTitles: any[], mcId: any, serviceFilterAll: any,
    filterAirForce: any,
    filterArmy: any,
    filterCoastGuard: any,
    filterMarineCorps: any,
    filterNavy: any,
    filterSpaceForce: any,
    selectAllFilters: string[]): any {

    //nothing to filter
    if (!availableJobTitles) {
      return null;
    }

    //return empty list 
    if (availableJobTitles.length == 0 || !mcId) {
      return availableJobTitles;
    }

    //do filters exist?
    var filters = this.getServiceFilters(filterAirForce,
      filterArmy,
      filterCoastGuard,
      filterMarineCorps,
      filterNavy,
      filterSpaceForce);

    //only allowing filtering of job titles related to the service(s) selected
    let jobTitlesForServiceSelected = availableJobTitles.filter(function (item) {
      return selectAllFilters.indexOf(item.service) !== -1 && item.mcId == mcId
    });

    //if filter all was not checked and no filters exist
    //return an empty list
    if (!serviceFilterAll && filters.length == 0) {
      return jobTitlesForServiceSelected;
    }

    //if filter all was selected return all
    if (serviceFilterAll) {
      return jobTitlesForServiceSelected;
    }

    //if one or more filters were selected then return items based on filter
    return jobTitlesForServiceSelected.filter(function (item) {
      return filters.indexOf(item.service) !== -1 && item.mcId == mcId
    });

  }

  getServiceFilters(
    filterAirForce: any,
    filterArmy: any,
    filterCoastGuard: any,
    filterMarineCorps: any,
    filterNavy: any,
    filterSpaceForce): string[] {

    let returnFilters: string[] = [];

    if (filterAirForce) {
      returnFilters.push('F');
    }

    if (filterArmy) {
      returnFilters.push('A');
    }

    if (filterCoastGuard) {
      returnFilters.push('C');
    }

    if (filterMarineCorps) {
      returnFilters.push('M');
    }

    if (filterNavy) {
      returnFilters.push('N');
    }

    if (filterSpaceForce) {
      returnFilters.push('S');
    }

    return returnFilters;
  }

}
