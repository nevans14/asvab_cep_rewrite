import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { SpinnerComponent } from 'app/core/spinner/spinner.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../material.module';
import { AsvabIcatSampleTestDialogComponent } from './dialogs/asvab-icat-sample-test-dialog/asvab-icat-sample-test-dialog.component';
import { AsvabSampleTestDialogComponent } from './dialogs/asvab-sample-test-dialog/asvab-sample-test-dialog.component';
import { AsvabZoomDialogComponent } from './dialogs/asvab-zoom-dialog/asvab-zoom-dialog.component';
import { BringAsvabToYourSchoolComponent } from './dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { FindYourInterestManualScoreInputComponent } from './dialogs/find-your-interest-manual-score-input/find-your-interest-manual-score-input.component';
import { LearnMoreDialogComponent } from './dialogs/learn-more-dialog/learn-more-dialog.component';
// import { PrivacySecurityDialogComponent } from './dialogs/privacy-security-dialog/privacy-security-dialog.component';
import { LoginDialogComponent } from './dialogs/login-dialog/login-dialog.component';
// import { ServiceContactUsDialogComponent } from './dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { MessageDialogComponent } from './dialogs/message-dialog/message-dialog.component';
import { ScheduleAsvabComponent } from './dialogs/schedule-asvab/schedule-asvab.component';
import { DynamicContentLoaderComponent } from './dynamic-content-loader/dynamic-content-loader.component';
import { FilterPipe } from './filter.pipe';
import { FooterComponent, HeaderComponent } from './page-partials/index';
import { LeftNavigationMobileComponent } from './page-partials/left-navigation-mobile/left-navigation-mobile.component';
import { LeftNavigationComponent } from './page-partials/left-navigation/left-navigation.component';
import { MobileInterestCodesComponent } from './page-partials/mobile-interest-codes/mobile-interest-codes.component';
import { SiteSearchComponent } from './page-partials/site-search/site-search.component';
import { SafePipe } from './safe.pipe';
import { PortfolioTipsDialogComponent } from './dialogs/portfolio-tips-dialog/portfolio-tips-dialog.component';
import { ApprenticeshipListComponent } from './dialogs/career-one-stop-dialogs/apprenticeship-list/apprenticeship-list.component';
import { CertificationDetailsComponent } from './dialogs/career-one-stop-dialogs/certification-details/certification-details.component';
import { CertificationListComponent } from './dialogs/career-one-stop-dialogs/certification-list/certification-list.component';
import { LicenseDetailsComponent } from './dialogs/career-one-stop-dialogs/license-details/license-details.component';
import { LicenseListComponent } from './dialogs/career-one-stop-dialogs/license-list/license-list.component';
import { OccufindAddFavoritesComponent } from './page-partials/occufind-add-favorites/occufind-add-favorites.component';
import { SearchPipe } from './search.pipe';
import { InterestPipe } from './interest.pipe';
import { CategoryPipe } from './category.pipe';
import { SkillPipe } from './skill.pipe';
import { OccufindGridAddFavoritesComponent } from './page-partials/occufind-grid-add-favorites/occufind-grid-add-favorites.component';
import { CareerClusterAddFavoritesComponent } from './page-partials/career-cluster-add-favorites/career-cluster-add-favorites.component';
import { CareerClusterNotesDialogComponent } from './dialogs/career-cluster-notes-dialog/career-cluster-notes-dialog.component';
import { LeftNavigationV2ExpandedComponent } from './page-partials/left-navigation-v2-expanded/left-navigation-v2-expanded.component';
import { MyTopResultsExpandedComponent } from './page-partials/my-top-results-expanded/my-top-results-expanded.component';
import { LeftNavigationV2MobileComponent } from './page-partials/left-navigation-v2-mobile/left-navigation-v2-mobile.component';
import { MyTopResultsMobileComponent } from './page-partials/my-top-results-mobile/my-top-results-mobile.component';
import { LeftNavigationV2CollapsedComponent } from './page-partials/left-navigation-v2-collapsed/left-navigation-v2-collapsed.component';
import { MyTopResultsCollapsedComponent } from './page-partials/my-top-results-collapsed/my-top-results-collapsed.component';
import { RetrieveAsvabScoresDialogComponent } from './dialogs/retrieve-asvab-scores-dialog/retrieve-asvab-scores-dialog.component';
import { AlternateTrainingDialogComponent } from './dialogs/alternate-training-dialog/alternate-training-dialog.component';
import { MoreAboutMeComponent } from './dialogs/more-about-me/more-about-me.component';
import { NotesDialogComponent } from './dialogs/notes-dialog/notes-dialog.component';
import { HttpErrorInterceptor } from './http-error-interceptor';
import { RegisterDialogComponent } from './dialogs/register-dialog/register-dialog.component';
import { MergeAccountDialogComponent } from './dialogs/merge-account-dialog/merge-account-dialog.component';
import { ScorecardDialogComponent } from './dialogs/scorecard-dialog/scorecard-dialog.component';
import { SessionKeepaliveDialogComponent } from './dialogs/session-keepalive-dialog/session-keepalive-dialog.component';
import { HttpSessionInterceptor } from './http-session-interceptor';
import { UtilityService } from 'app/services/utility.service';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { DateDialogComponent } from './dialogs/date-dialog/date-dialog.component';
import { TaskDialogComponent } from './dialogs/task-dialog/task-dialog.component';
import { ViewDetailDialogComponent } from './dialogs/view-detail-dialog/view-detail-dialog.component';
import { SearchSchoolsDialogComponent } from './dialogs/search-schools-dialog/search-schools-dialog.component';
import { MilitaryJobTitlePipe } from './military-job-title.pipe';
import { MilitaryAcademyAddDialogComponent } from './dialogs/military-academy-add-dialog/military-academy-add-dialog.component';
import { WorkValuesRetakeDialogComponent } from './dialogs/work-values-retake-dialog/work-values-retake-dialog.component';
import { GapYearPlanFilterPipe } from './gap-year-plan-filter.pipe';
import { OrderByPipe } from './order-by.pipe';
import { ClassroomActivitiesDialogComponent } from './dialogs/classroom-activities-dialog/classroom-activities-dialog.component';
import { SampleGapProgramsDialogComponent } from './dialogs/sample-gap-programs-dialog/sample-gap-programs-dialog.component';
import { ContactRecruiterDialogComponent } from './dialogs/contact-recruiter-dialog/contact-recruiter-dialog.component';
import { LearnMoreExploreDialogComponent } from './dialogs/learn-more-explore-dialog/learn-more-explore-dialog.component';
import { EditAccountDialogComponent } from './dialogs/edit-account-dialog/edit-account-dialog.component';
import { PlanCalendarPipe } from './plan-calendar.pipe';
import { WorkValuePipe } from './work-value.pipe';
import { MyAsvabHeaderComponent } from './page-partials/my-asvab-header/my-asvab-header.component';
import { RecentMessagesDialogComponent } from './dialogs/recent-messages-dialog/recent-messages-dialog.component';
import { EmailSettingsDialogComponent } from './dialogs/email-settings-dialog/email-settings-dialog.component';
import { ManageMentorsDialogComponent } from './dialogs/manage-mentors-dialog/manage-mentors-dialog.component';
import { SubmitToMentorDialogComponent } from './dialogs/submit-to-mentor-dialog/submit-to-mentor-dialog.component';
import { MentorDateDialogComponent } from './dialogs/mentor-date-dialog/mentor-date-dialog.component';
import { MentorCalendarDialogComponent } from './dialogs/mentor-calendar-dialog/mentor-calendar-dialog.component';
import { MessageToStudentDialogComponent } from './dialogs/message-to-student-dialog/message-to-student-dialog.component';
import { MultiParamFilterPipePipe } from './multi-param-filter-pipe.pipe';
import { DirectMessageDialogComponent } from './dialogs/direct-message-dialog/direct-message-dialog.component';
import { MentorTaskDialogComponent } from './dialogs/mentor-task-dialog/mentor-task-dialog.component';
import { MentorGroupDialogComponent } from './dialogs/mentor-group-dialog/mentor-group-dialog.component';
import { MentorDashboardStudentFilterPipe } from './mentor-dashboard-student-filter.pipe';
import { MentorDashboardGroupFilterPipe } from './mentor-dashboard-group-filter.pipe';
import { GuidedTipsConfigDialogComponent } from './dialogs/guided-tips-config-dialog/guided-tips-config-dialog.component';
import { MilitaryCareersDialogComponent } from './dialogs/military-careers-dialog/military-careers-dialog.component';
import { LeftNavigationV2MentorComponent } from './page-partials/left-navigation-v2-mentor/left-navigation-v2-mentor.component';
import { NonstudentDemoDialogComponent } from './dialogs/nonstudent-demo-dialog/nonstudent-demo-dialog.component';
import { StudentNotificationDialogComponent } from './dialogs/student-notification-dialog/student-notification-dialog.component';
import { LinkedSchoolsDialogComponent } from './dialogs/linked-schools-dialog/linked-schools-dialog.component';
import { MentorStudentReportDialogComponent } from './dialogs/mentor-student-report-dialog/mentor-student-report-dialog.component';
import { StatusLegendComponent } from './dialogs/status-legend/status-legend.component';
import { IsAccPresentPipe } from './is-acc-present.pipe';
import { StrategyTakingAsvabDialogComponent } from './dialogs/strategy-taking-asvab-dialog/strategy-taking-asvab-dialog.component';

@NgModule({
  imports: [
    MaterialModule,
    NgbModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserModule,
    CommonModule,
    NgxYoutubePlayerModule
  ],
  exports: [
    AppRoutingModule,
    NgxYoutubePlayerModule,
    HeaderComponent, FooterComponent,
    SafePipe, FilterPipe, SearchPipe, InterestPipe, CategoryPipe, SkillPipe, MilitaryJobTitlePipe, GapYearPlanFilterPipe, OrderByPipe, MultiParamFilterPipePipe, MentorDashboardStudentFilterPipe, MentorDashboardGroupFilterPipe, WorkValuePipe, 
    DynamicContentLoaderComponent,
    LeftNavigationComponent,
    LeftNavigationMobileComponent,
    MobileInterestCodesComponent,
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    SiteSearchComponent,
    OccufindAddFavoritesComponent,
    OccufindGridAddFavoritesComponent,
    CareerClusterAddFavoritesComponent,
    LeftNavigationV2ExpandedComponent,
    MyTopResultsExpandedComponent,
    LeftNavigationV2MobileComponent,
    MyTopResultsMobileComponent,
    LeftNavigationV2CollapsedComponent,
    MyTopResultsCollapsedComponent,
    MyAsvabHeaderComponent,
    LeftNavigationV2MentorComponent,
    IsAccPresentPipe,
  ],

  declarations: [
    // PrivacySecurityDialogComponent,
    LoginDialogComponent,
    //  ServiceContactUsDialogComponent,
    MessageDialogComponent,
    MoreAboutMeComponent,
    PopupPlayerComponent,
    SpinnerComponent,
    BringAsvabToYourSchoolComponent,
    ScheduleAsvabComponent,
    HeaderComponent, FooterComponent,
    LearnMoreDialogComponent,
    SafePipe,
    DynamicContentLoaderComponent,
    LeftNavigationComponent,
    LeftNavigationMobileComponent,
    MobileInterestCodesComponent,
    FilterPipe,
    AsvabZoomDialogComponent,
    AsvabSampleTestDialogComponent,
    AsvabIcatSampleTestDialogComponent,
    FindYourInterestManualScoreInputComponent,
    SiteSearchComponent,
    PortfolioTipsDialogComponent,
    ApprenticeshipListComponent,
    CertificationDetailsComponent,
    CertificationListComponent,
    LicenseDetailsComponent,
    LicenseListComponent,
    OccufindAddFavoritesComponent,
    SearchPipe,
    InterestPipe,
    CategoryPipe,
    SkillPipe,
    OccufindGridAddFavoritesComponent,
    CareerClusterAddFavoritesComponent,
    CareerClusterNotesDialogComponent,
    LeftNavigationV2ExpandedComponent,
    MyTopResultsExpandedComponent,
    LeftNavigationV2MobileComponent,
    MyTopResultsMobileComponent,
    LeftNavigationV2CollapsedComponent,
    MyTopResultsCollapsedComponent,
    RetrieveAsvabScoresDialogComponent,
    AlternateTrainingDialogComponent,
    MoreAboutMeComponent,
    NotesDialogComponent,
    RegisterDialogComponent,
    MergeAccountDialogComponent,
    ScorecardDialogComponent,
    SessionKeepaliveDialogComponent,
    ConfirmDialogComponent,
    DateDialogComponent,
    TaskDialogComponent,
    SearchSchoolsDialogComponent,
    ViewDetailDialogComponent,
    MilitaryJobTitlePipe,
    MilitaryAcademyAddDialogComponent,
    WorkValuesRetakeDialogComponent,
    GapYearPlanFilterPipe,
    OrderByPipe,
    ClassroomActivitiesDialogComponent,
    SampleGapProgramsDialogComponent,
    ContactRecruiterDialogComponent,
    LearnMoreExploreDialogComponent,
    EditAccountDialogComponent,
    PlanCalendarPipe,
    WorkValuePipe,  
    MyAsvabHeaderComponent,
    RecentMessagesDialogComponent,
    EmailSettingsDialogComponent,
    ManageMentorsDialogComponent,
    SubmitToMentorDialogComponent,
    MentorDateDialogComponent,
    MentorCalendarDialogComponent,
    MessageToStudentDialogComponent,
    MultiParamFilterPipePipe,
    DirectMessageDialogComponent,
    MentorTaskDialogComponent,
    MentorGroupDialogComponent,
    MentorDashboardStudentFilterPipe,
    MentorDashboardGroupFilterPipe,
    GuidedTipsConfigDialogComponent,
    MilitaryCareersDialogComponent,
    LeftNavigationV2MentorComponent,
    NonstudentDemoDialogComponent,
    StudentNotificationDialogComponent,
    LinkedSchoolsDialogComponent,
    MentorStudentReportDialogComponent,
    StatusLegendComponent,
    IsAccPresentPipe,
    StrategyTakingAsvabDialogComponent,
  ],
  providers: [
    HttpHelperService,
    GoogleAnalyticsService,
    UtilityService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpSessionInterceptor,
      multi: true
    },
  ],
  entryComponents: [
    PopupPlayerComponent,
    //    DynamicContentLoaderComponent,
    // PrivacySecurityDialogComponent,
    LoginDialogComponent,
    //   ServiceContactUsDialogComponent,
    MessageDialogComponent,
    MoreAboutMeComponent,
    BringAsvabToYourSchoolComponent,
    ScheduleAsvabComponent,
    LearnMoreDialogComponent,
    AsvabZoomDialogComponent,
    AsvabSampleTestDialogComponent,
    AsvabIcatSampleTestDialogComponent,
    FindYourInterestManualScoreInputComponent,
    PortfolioTipsDialogComponent,
    ApprenticeshipListComponent,
    CertificationDetailsComponent,
    CertificationListComponent,
    LicenseDetailsComponent,
    LicenseListComponent,
    CareerClusterNotesDialogComponent,
    RetrieveAsvabScoresDialogComponent,
    AlternateTrainingDialogComponent,
    NotesDialogComponent,
    RegisterDialogComponent,
    MergeAccountDialogComponent,
    ScorecardDialogComponent,
    SessionKeepaliveDialogComponent,
    ConfirmDialogComponent,
    SearchSchoolsDialogComponent,
    ViewDetailDialogComponent,
    MilitaryAcademyAddDialogComponent,
    WorkValuesRetakeDialogComponent,
    SampleGapProgramsDialogComponent,
    ContactRecruiterDialogComponent,
    TaskDialogComponent,
    DateDialogComponent,
    LearnMoreExploreDialogComponent,
    EditAccountDialogComponent,
    ClassroomActivitiesDialogComponent,
    RecentMessagesDialogComponent,
    EmailSettingsDialogComponent,
    ManageMentorsDialogComponent,
    MentorDateDialogComponent,
    MentorCalendarDialogComponent,
    SubmitToMentorDialogComponent,
    MessageToStudentDialogComponent,
    DirectMessageDialogComponent,
    MentorTaskDialogComponent,
    MentorGroupDialogComponent,
    GuidedTipsConfigDialogComponent,
    MilitaryCareersDialogComponent,
    NonstudentDemoDialogComponent,
    StudentNotificationDialogComponent,
    LinkedSchoolsDialogComponent,
    MentorStudentReportDialogComponent,
    StatusLegendComponent,
    StrategyTakingAsvabDialogComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule { }
