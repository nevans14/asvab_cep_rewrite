import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';
@Pipe({
  name: 'orderBy',
  pure: false
})
export class OrderByPipe implements PipeTransform {

  /*transform(value: any, args?: any): any {
    return null;
  }*/
  
  transform = orderBy;

}
