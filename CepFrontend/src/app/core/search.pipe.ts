import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(values: any[], keyword: string): any {
    if (!keyword) {
      return values;
    }

    return values.filter(v => 
      v.occupationTitle.toLowerCase().indexOf(keyword.toLowerCase()) > -1
    )
  }
}
