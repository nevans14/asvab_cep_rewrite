import { FormGroup } from '@angular/forms';
    
export function ConfirmPwdValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.confirmPwdValidator) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmPwdValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}