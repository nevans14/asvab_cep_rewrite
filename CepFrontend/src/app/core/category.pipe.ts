import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(values: any[], checkboxCategory: any, categoryItems: any): any {

    if (!values) {
      return [];
    }

    let filtered = values.filter(v => {
      for (var i = 0; i < checkboxCategory.length; i++) {
        if (categoryItems[checkboxCategory[i].code]) {
          switch (checkboxCategory[i].code) {
            case 'B':
              if (!v.isBrightOccupation) return false;
              break;
            case 'H':
              if (!v.isHotOccupation) return false;
              break;
            case 'S':
              if (!v.isStemOccupation) return false;
              break;
            case 'G':
              if (!v.isGreenOccupation) return false;
              break;
            case 'M':
              if (!v.isAvailableInTheMilitary) return false;
              break;
          }
        }
      }
      return true;
    })
    return filtered;
  }
}
