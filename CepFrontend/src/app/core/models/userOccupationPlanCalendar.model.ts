import { UserOccupationPlanCalendarTask } from "./userOccupationPlanCalendarTask.model";

export interface UserOccupationPlanCalendar {
    id: number, planId: number,
    title: string, date: string,
    isGraduationDate: boolean,
    tasks: UserOccupationPlanCalendarTask[];
}