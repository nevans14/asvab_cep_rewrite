import { SchoolProfile } from "./schoolProfile.model";

export interface UserCollegePlan {
    id : number, planId : number, unitId : number,
    isSeekingInStateTuition : any,
    degree : string, 
    major : string;
    familyContribution , scholarshipContribution, savings, loan, other
    schoolProfile: SchoolProfile;
}