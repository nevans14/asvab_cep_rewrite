export interface UserOccupationPlan {
    id, socId, pathway, additionalNotes, useNationalAverage, statusId
}

export interface UserOccupationPlanComment {
    id, planId,commentTypeId: number
    from, message, dateCreated: string,
    to: UserOccupationPlanCommentRecipient[],
    selectedUsers: string[];
}

export interface UserOccupationPlanCommentRecipient {
    id, commentId: number,
    username, dateUpdated: string,
    hasBeenRead: boolean;
}

export interface UserOccupationPlanLog {

    id, planId: number,
    statusId: number,
    dateLogged, dateUpdated: string,
    hasBeenRead: boolean;

}