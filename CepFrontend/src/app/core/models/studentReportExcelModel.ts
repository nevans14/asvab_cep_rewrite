export interface StudentReportExcelModel {
    section: sectionReportType;  //Student Information
    columnName: subHeaderColumnNameType; //'ID'
    value: string; //12344
    mentorLinkId: string;
  }

export enum sectionReportType{
    STUDENT_INFORMATION = "Student Information",
    ASVAB_TEST_INFORMATION = "ASVAB Test Information",
    PERCENTILE_SCORES = "Percentile scores",
    STANDARD_SCORES = "Standard scores",
    MILITARY_LINES_SCORES = "Military lines scores",
    ADDITIONAL_PROGRAM_INFORMATION = "Additional Program Information",
    FAVORITES = "Favorites"
}

export enum subHeaderColumnNameType{
    FIRST_NAME = "First name",
    LAST_NAME = "Last name",
    STUDENT_ID = "ID",
    USERNAME = "Username/email",
    GRADE = "Grade",
    GRADUATION = "Graduation",
    GENDER = "Gender",
    ETHNICITY = "Ethnicity",
    SCHOOL = "School",
    SCHOOL_ADDRESS = "School Address",
    SCHOOL_CODE = "School Code",
    ACCESS_CODE = "Access Code",
    EXPIRATION_DATE = "Expiration date",
    TEST_DATE = "Test date",
    ASVAB_PERCENTILE_SCORES = "ASVAB Percentile Scores",
    ASVAB_STANDARD_SCORES = "ASVAB Standard Scores",
    AFQT = "AFQT",
    MILITARY_LINES_SCORES = "Military lines scores",
    TYPE_OF_TEST = "Type of test",
    COMBINED_FYI_SCORES = "Combined FYI Scores",
    GENDER_SPECIFIC_FYI_SCORES = "Gender Specific FYI Scores",
    WORK_VALUES = "Work Values",
    FAVORITE_OCCUPATIONS = "Favorite occupations",
    FAVORITE_COLLEGES = "Favorite colleges",
    FAVORITE_CAREER_CLUSTERS = "Favorite career clusters",
    FAVORITE_CTM_CAREERS = "Favorite CTM careers",
    POST_SECONDARY_INTENTIONS = "Post-secondary Intentions"
}