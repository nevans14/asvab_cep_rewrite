export interface DirectMessage {
    id: number;
    messageTypeId: messageType;
    mentorLinkId, fromUser, toUser, message, dateCreated, dateUpdated, calendarDate: string;
    hasBeenRead: boolean;
}

export enum messageType {
    CLASSROOM_ACTIVITY = 1,
    CAREER_PLAN = 2,
    PORTFOLIO = 3,
    DIRECT_MESSAGE = 4,
    CALENDAR_MESSAGE = 5,
    TASK_MESSAGE = 6
}
