export interface FavoriteSchool {
	id: number,
	unitId : number,
	schoolName: string,
	city: string,
	state: string;
}