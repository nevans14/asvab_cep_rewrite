
export interface Standard {
    id, activityId, title, url
}

export interface ClassroomActivityPdf {
    id, activityId, title, role, s3Filename
}

export interface ClassroomActivitySubmission {
    id, classroomActivityId, fileName, dateCreated, dateUpdated, statusId,
    mentors: ClassroomActivitySubmissionMentor[],
    logs: ClassroomActivitySubmissionLog[]
}

export interface ClassroomActivitySubmissionMentor {
    mentorId, didReview
}

export interface ClassroomActivitySubmissionLog {
    id, dateLogged, dateUpdated, workId, statusId, hasBeenRead
}

export interface ClassroomActivitySubmissionComment {
    id, classroomActivityId, from, message, dateCreated,
    selectedUsers: string[],
    to: ClassroomActivitySubmissionCommentUser[]
}

export interface ClassroomActivitySubmissionCommentUser {
    id, commentId, from, username, dateUpdated, hasBeenRead
}

export interface ClassroomActivity {
    id, numOfUnreadMessages, title, shortDescription, longDescription,
    categoryIds: number[],
    standards: Standard[],
    pdfs: ClassroomActivityPdf[],
    submissions: ClassroomActivitySubmission[],
    comments: ClassroomActivitySubmissionComment[]
}

