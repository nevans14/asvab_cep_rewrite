import { UserGapYearPlanType } from "./userGapYearPlanType.model";

export interface UserGapYearPlan {
    id: number, planId: number, gapYearId: number,
    goals: string,
    planName: string,
    fundingSource: string;
    programCost: number, costOfLiving: number, estimatedAmount: number, totalAnticipatedWages: number;
    isVisaRequired: boolean, isVaccinationsRequired: boolean;
    types: UserGapYearPlanType[];
}

