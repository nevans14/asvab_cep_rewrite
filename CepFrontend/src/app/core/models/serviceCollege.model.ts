import { SchoolProfile } from "./schoolProfile.model"
export interface ServiceCollege {
    unitId: number,
    collegeTypeId: number,
    services: string[],
    schoolProfile: SchoolProfile,
    applicationTimelinePdfKey: string,
    checklistPdfKey: string,
    alternativeName: string,
    collegeType: {
        typeId: number,
        typeName: string
    }
}
