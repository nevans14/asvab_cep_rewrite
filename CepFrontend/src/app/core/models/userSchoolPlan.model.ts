import { UserSchoolPlanActivity } from "./userSchoolPlanActivity.model";
import { UserSchoolPlanCourse } from "./userSchoolPlanCourse.model";

export interface UserSchoolPlan {
    planId, graduationDate;
    courses: UserSchoolPlanCourse[];
    activities: UserSchoolPlanActivity[];
}