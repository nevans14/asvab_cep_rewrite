export interface UserMilitaryPlanRotc {
    id: number, militaryPlanId: number, unitId: number, collegePlanId: number,
    wantsToApply: number; //0 = N, 1 = Y, 2 = M
}
