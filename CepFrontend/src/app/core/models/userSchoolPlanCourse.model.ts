export interface UserSchoolPlanCourse {
    id, planId, type, course, location, benefit;
}