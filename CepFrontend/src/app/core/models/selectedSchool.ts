export interface SelectedSchool {
    schoolCode: number;
    schoolName: string;
}