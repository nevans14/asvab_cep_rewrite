export interface PortfolioCommentUser {
    id, commentId: number,
    username, dateUpdated: string,
    hasBeenRead: boolean;
}