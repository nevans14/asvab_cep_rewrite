export interface UserMilitaryPlanServiceCollege {
    id: number, militaryPlanId: number, unitId: number, collegeTypeId: number;
}
