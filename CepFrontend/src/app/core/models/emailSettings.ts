export interface StudentUserEmailSettings {
    wouldLikeToReceiveNotifications: boolean; 
    wouldLikeToReceiveMarketingComm: boolean; 
    wouldLikeToReceiveMonthlyNewsletter: boolean;
}
export interface UserEmailSettings extends StudentUserEmailSettings {
    sendDesignatedStudentSummaries: boolean; 
    sendDesignatedStudentSummariesWeekly: boolean; 
    sendDesignatedStudentSummariesMonthly: boolean;
}