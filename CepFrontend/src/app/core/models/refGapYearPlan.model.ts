export interface RefGapYearPlan {
    id: number;
    title: string, url: string;
    isStudyAbroad: boolean, isWorkAbroad: boolean, isInternship: boolean, isVolunteer: boolean, isTravel: boolean;
}