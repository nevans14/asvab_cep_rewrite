export interface UserOccupationPlanCalendarTask {
    id: number, planId: number, calendarId: number,
    taskName: string,
    completed: boolean
}



