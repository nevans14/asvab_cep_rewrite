export interface SchoolProfile {
    unitId : number,
    institutionName : string, 
    city : string,
    state: string,
    inStateTutition
	,outOfStateTutition
	,totalApplicants
	,totalAdmitted
	,retentionRate
	,satCritica25Percent
	,satCritica75Percent
	,satMath25Percent
	,satMath75Percent
	,actCritical25Percent
	,actCritical75Percent
	,certificate
	,associates
	,bachelors
	,masters
	,doctoral
	,rotc	
	,rotcArmy
	,rotcNavy
	,rotcAirForce
	,url : string
	,collegeMajors
}