import { PortfolioCommentUser } from "./portfolioCommentUser.model";

export interface PortfolioComment {
    id, commentTypeId: number
    from, message, dateCreated: string,
    to: PortfolioCommentUser[],
    selectedUsers: string[];
}