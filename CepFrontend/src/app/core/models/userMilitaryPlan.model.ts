import { UserMilitaryPlanCareer } from "./userMilitaryPlanCareer.model";
import { UserMilitaryPlanRotc } from "./userMilitaryPlanRotc.model";
import { UserMilitaryPlanServiceCollege } from "./userMilitaryPlanServiceCollege.model";

export interface UserMilitaryPlan {
    id : number, planId : number,
    didSelectEnlisted: boolean, didSelectOfficer: boolean, 
    didSelectArmy: boolean, didSelectNavy:boolean, didSelectAirForce:boolean, 
    didSelectMarineCorps: boolean, didSelectCoastGuard: boolean, didSelectSpaceForce: boolean;
    isInterestedInActiveDuty: boolean, isInterestedInReserves: boolean, 
    isInterestedInNationalGuard: boolean, isNotSure:boolean, 
    selectedCareers: UserMilitaryPlanCareer[];
    rotcPrograms: UserMilitaryPlanRotc[],
    serviceColleges: UserMilitaryPlanServiceCollege[],
}
