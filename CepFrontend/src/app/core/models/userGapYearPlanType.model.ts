export interface UserGapYearPlanType
 {
    id: number;
    name: string;
    isSelected: boolean;
}