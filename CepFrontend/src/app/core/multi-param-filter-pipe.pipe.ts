import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiParamFilterPipe',
  pure: false
})
export class MultiParamFilterPipePipe implements PipeTransform {

  transform(value: any[], filter?: any): any {

    if (!value || !Object.keys(filter).length) {
      return value;
    }

    return value.filter(item => {
      return Object.keys(filter)
        .filter(_ => filter.hasOwnProperty(_))
        .some(key => {
          // if (!item[key]) return false;
          // if(item[key]){
            const arrayValues = filter[key] as any[];
            return arrayValues.some(_ => _ === item[key]);
          // }

        });
    });

  }

}
