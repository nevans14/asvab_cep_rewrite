import { Component, OnInit, OnDestroy, ViewEncapsulation, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit, OnDestroy {

  constructor(
    private _router: Router,
    private _spinner: NgxSpinnerService
  ) {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this._spinner.show();

      } else if ( event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this._spinner.hide();
       }
    }, () => {
      this._spinner.hide();
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this._spinner.hide();
  }

}
