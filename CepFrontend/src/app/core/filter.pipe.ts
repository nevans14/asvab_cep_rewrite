import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], field: string, value: string): any[] {
    if (!items) { return []; }
    if (value == null || value == undefined) { return items; }

    return items.filter(it => {
      if (typeof it[field] === 'string' || it[field] instanceof String) {
        if (typeof value === 'string') {
          if (value.length === 0) return items;

          return it[field].toLowerCase().indexOf(value.toLowerCase()) !== -1;
        } else {
          return it[field].toLowerCase() === value;
        }
      } else {
        if (typeof field === 'string' && typeof value === 'string') {
          return value === field;
        }
        return it[field] === value;
      }
    });
  }

}
