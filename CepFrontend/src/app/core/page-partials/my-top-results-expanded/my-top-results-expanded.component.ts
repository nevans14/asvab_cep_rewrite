import { Component, OnInit, AfterViewInit} from '@angular/core';
import { MatDialog } from '@angular/material';
import { FindYourInterestManualScoreInputComponent } from '../../dialogs/find-your-interest-manual-score-input/find-your-interest-manual-score-input.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';

import { NumberTestTakenService } from 'app/dashboard/resolves/number-test-taken.service';
import { FyiInterestCodesService } from 'app/dashboard/resolves/fyi-interest-codes.service';
import { AccessCodeDetailService } from 'app/resolves/access-code-detail.service';
import { AsvabScoreResolveService } from 'app/resolves/asvab-score-resolve.service';
import { ScoreSummaryResolveService } from 'app/resolves/score-summary-resolve.service';
import { TiedLinkResolveService } from 'app/dashboard/resolves/tied-link-resolve.service';
import { MediaCenterService } from 'app/services/media-center.service';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { RetrieveAsvabScoresDialogComponent } from 'app/core/dialogs/retrieve-asvab-scores-dialog/retrieve-asvab-scores-dialog.component';
import $ from 'jquery'
declare var $: $

@Component({
  selector: 'app-my-top-results-expanded',
  templateUrl: './my-top-results-expanded.component.html',
  styleUrls: ['./my-top-results-expanded.component.scss']
})
export class MyTopResultsExpandedComponent implements OnInit, AfterViewInit {

  domainName;
  accessCode: any;
  currentUser: any;
  numberTestTaken: any;
  //profileExist = profileExist.data;
  brightCareers;
  stemCareers: any;
  mediaCenterList;
  //noteList = noteList;
  //domainName = resource;
  tiedLink: any;
  isDemoTaken = 0;
  //isDemoTaken = isDemoTaken.data.r;
  vBooks;
  mBooks;
  sBooks;

  userRole: any;

  interestCodeOne: any;
  interestCodeTwo: any;
  interestCodeThree: any;
  scoreChoice: any;

  scoreSummary: any; // actual test score data
  asvabScore: any; // manually entered score data

  hasReachedLimit: boolean;
  limit = 2;

  constructor(
    private _dialog: MatDialog,
    private mediaCenterService: MediaCenterService,
    private route: ActivatedRoute,
    private _router: Router,
    private _FYIScoreService: FindYourInterestService,
    private _user: UserService,
    private _httpHelper: HttpHelperService,
    private _portfolioRestFactory: PortfolioRestFactoryService,
    private _numberTestTakenService: NumberTestTakenService,
    private _fyiInterestCodeService: FyiInterestCodesService,
    private _accessCodeDetailService: AccessCodeDetailService,
    private _asvabScoreService: AsvabScoreResolveService,
    private _scoreSummaryService: ScoreSummaryResolveService,
    private _tiedLinkService: TiedLinkResolveService,
  ) {
    this.currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    this.userRole = this.currentUser ? this.currentUser.role : null;
    this.hasReachedLimit = false;
  }

  ngOnInit() {
    this.mediaCenterService.getMediaCenterList().then(
      data => {
          this.mediaCenterList = data;
      } );

    this._numberTestTakenService.resolve().then((data: any) => {
      this.numberTestTaken = parseInt(data);
    })

    this._accessCodeDetailService.resolve().then((data: any) => {
      this.accessCode = data;
    })

    // set up interest codes for dashboard
    this._fyiInterestCodeService.resolve().then((data: any) => {
      var userInterestCodes  = data;
      this.interestCodeOne = userInterestCodes.interestCodeOne;
      this.interestCodeTwo = userInterestCodes.interestCodeTwo;
      this.interestCodeThree = userInterestCodes.interestCodeThree;
      this.scoreChoice = userInterestCodes.scoreChoice;

      if (this.interestCodeOne !== undefined) {
        this._user.setCompletion(UserService.COMPLETED_FYI, 'true');
      }

       // setup stem careers
      var params = this.createIdParam( this.interestCodeOne );
      params = this.createIdParam( this.interestCodeTwo );
      params = this.createIdParam( this.interestCodeThree );
      this._httpHelper.httpHelper( 'GET', 'occufind/DashboardStemCareers/' + this.interestCodeOne + '/' + this.interestCodeTwo + '/' + this.interestCodeThree + '/', null, null ).then( data => {
          this.stemCareers = data;
      } );

      this._httpHelper.httpHelper( 'GET', 'occufind/DashboardBrightOutlook/' + this.interestCodeOne + '/' + this.interestCodeTwo + '/' + this.interestCodeThree + '/', null, null ).then( data => {
          this.brightCareers = data;
      } );
    })

    Promise.all([
        this._scoreSummaryService.resolve(),
        this._asvabScoreService.resolve(),
    ]).then((data: any) => {
        this.scoreSummary = data[0];
        this.asvabScore = data[1];

        this.haveAsvabTestScores = ( this.isObject( this.scoreSummary ) && this.scoreSummary.hasOwnProperty( 'verbalAbility' ) ); // do we have actual test scores
        this.haveManualAsvabTestScores = ( this.asvabScore.length > 0 && !this.haveAsvabTestScores ); // do we have manual test scores
        this.showInitial = ( this.interestCodeOne == undefined && !this.haveAsvabTestScores && !this.haveManualAsvabTestScores );

        if (this.haveAsvabTestScores || this.haveManualAsvabTestScores) {
            this._user.setCompletion(UserService.HAS_ASVAB_SCORE, 'true');
        }

        // Add scores to session.
        if ( this.haveAsvabTestScores ) {
            window.sessionStorage.setItem( 'manualScores', String( false ) );
            window.sessionStorage.setItem( 'sV', String( this.getScore( 'verbal' ) ) );
            window.sessionStorage.setItem( 'sM', String( this.getScore( 'math' ) ) );
            window.sessionStorage.setItem( 'sS', String( this.getScore( 'science' ) ) );
            window.sessionStorage.setItem( 'sA', String( this.getScore( 'afqt' ) ) );
        } else if ( this.haveManualAsvabTestScores ) {
            window.sessionStorage.setItem( 'manualScores', String( true ) );
            window.sessionStorage.setItem( 'sV', this.asvabScore[this.asvabScore.length - 1].verbalScore );
            window.sessionStorage.setItem( 'sM', this.asvabScore[this.asvabScore.length - 1].mathScore );
            window.sessionStorage.setItem( 'sS', this.asvabScore[this.asvabScore.length - 1].scienceScore );
        }

        // Create book objects for sort/ranking - display gray book (score: 0) for manual scores
        this.vBooks = {
            name: "verbal",
            score: window.sessionStorage.getItem( 'sV' )
        }

        this.mBooks = {
            name: "math",
            score: window.sessionStorage.getItem( 'sM' )
        }

        this.sBooks = {
            name: "science",
            score: window.sessionStorage.getItem( 'sS' )
        }

        // Initial book sorting/ranking
        this.scoreSorting();

        window.sessionStorage.setItem('scoreSummary', JSON.stringify(this.scoreSummary));
        window.sessionStorage.setItem('asvabScore', JSON.stringify(this.asvabScore));
    })
    
    // setup tied link
    this._tiedLinkService.resolve().then((data: any) => {
      this.tiedLink = data;
    })
  }

  cepManualInput() {
    this._dialog.open(RetrieveAsvabScoresDialogComponent, { data: {enter: false, retrieve: false}}).beforeClosed().subscribe(result => {
        if (result) {
            if (result !== 'cancel') {

                if (result.hasOwnProperty('aFQTRawSSComposites')) {
                    // Retrieved asvab scores
                    this.scoreSummary = result;
                    this.haveAsvabTestScores = true;
                    this.haveManualAsvabTestScores = false;
                    this.showInitial = false;
                }else {
                    // Entered asvab scores
                    this.haveAsvabTestScores = false;
                    this.haveManualAsvabTestScores = true;
                    this.showInitial = false;
                    this.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
                }

                // Update session storage
                window.sessionStorage.setItem('manualScores', String(this.haveManualAsvabTestScores));
                window.sessionStorage.setItem('sV', String(this.getScore('verbal')));
                window.sessionStorage.setItem('sM', String(this.getScore('math')));
                window.sessionStorage.setItem('sS', String(this.getScore('science')));
                window.sessionStorage.setItem('sA', String(this.getScore('afqt')));

                // Update book stacks
                this.vBooks.score = this.getScore('verbal');
                this.mBooks.score = this.getScore('math');
                this.sBooks.score = this.getScore('science');
                this.scoreSorting();
         
            }
        } 
    });
  }

  asvabStrengths() {
    const modalOptions = {
      title : 'ASVAB Strengths',
      message : 'Your top strength will have the most books.'
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: modalOptions,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: 700,
    });
  }

  interestCodes() {
    const modalOptions = {
      title : 'Interest Code Legend',
      message : '<center><img src="/assets/images/interest_codes_computer.png"></center> '
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: modalOptions,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: 700,
    });
  }

  createIdParam( id ) {
    const params = [];
    params.push( {
        key: null,
        value: id
    } );
    return params;
  }

  isObject( x: any ): x is Object {
      return x != null && typeof x === 'object';
  }

  haveAsvabTestScores: boolean; // do we have actual test scores
  haveManualAsvabTestScores: boolean; // do we have manual test scores

  // Show initial option to enter scores full width (no interest codes, no actual scores, no entered scores)
  showInitial: boolean;
  /**** For Testing ****/
  //console.log(this.scoreSummary);
  //console.log(this.asvabScore);
  //this.haveAsvabTestScores = false;
  //this.haveManualAsvabTestScores = false;
  /*********************/

  getGenderCode = function() {
      // data should be f or m, but had m1 at one point hence substring
      var genderCode = ""; // none
      if ( this.haveAsvabTestScores ) {
          genderCode = this.scoreSummary.controlInformation.gender.substring( 0, 1 ).toLowerCase();
      }
      return genderCode;
  }

  getGenderFull = function( genderCode ) {
      switch ( genderCode ) {
          case "f":
              genderCode = "Female";
              break;
          case "m":
              genderCode = "Male";
              break;
          default:
              genderCode = "Not Available";
              break;
      }
      return genderCode;
  }

  tiedLinkClick = function() {
      this._FYIScoreService.setTiedLinkClicked( true );
      this._router.navigate( ['/find-your-interest-score'] );
  }

  isAbleToReTakeTest = function() {
      var numOfTestTaken = parseInt( this.numberTestTaken );
      // if the user has taken the FYI, show the "Re-Take the FYI"
      if ( numOfTestTaken > 0 ) {
          // if the user is an Admin or PTI user, allow unlimited FYI tests
          if ( this.isUnlimitedFyi() ) {
              return true;
          }
          // if the user has taken 1 test, then the user is able to take 1 more
          if ( numOfTestTaken < 2 ) {
              return true;
          }
      }
      return false;
  }

  // Only Admins and PTI users can have unlimited FYI Tests
  isUnlimitedFyi = function() {
      return this.accessCode && this.accessCode.unlimitedFyi === 1 ? true : false;
  }

  fyiManualInput = function() {
      let dialogRef = this._dialog.open( FindYourInterestManualScoreInputComponent, {
      } );
  }

  /**
   * Route user to portfolio page.
   */
  routeToPortfolio = function() {
    var promise = this._portfolioRestFactory.isPortfolioStarted().toPromise();
      promise.then(success => {

          // if portfolio exists then skip instructions
          if (success > 0) {
              this._router.navigate( ['/portfolio'] );
          } else {
              this._router.navigate( ['/portfolio-directions'] );
          }
      }, error => {
          console.error(error);
      });

  }


  selectedOccupation = function( onetSoc ) {
      this._router.navigate( ['/occufind-occupation-details/' + onetSoc] );
  };

  interestCodeInfo = function( interestCode ) {
      var modalOptions;
      switch ( interestCode ) {
          case "R":
              modalOptions = {
                  headerText: 'Realistic',
                  bodyText: '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
                  '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
                  '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
              };
              break;
          case "I":
              modalOptions = {
                  headerText: 'Investigative',
                  bodyText: '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
                  '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
                  '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
              };
              break;
          case "A":
              modalOptions = {
                  headerText: 'Artistic',
                  bodyText: '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
                  '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
                  '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
              };
              break;
          case "S":
              modalOptions = {
                  headerText: 'Social',
                  bodyText: '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
                  '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
                  '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
              };
              break;
          case "E":
              modalOptions = {
                  headerText: 'Enterprising',
                  bodyText: '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
                  '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
                  '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
              };
              break;
          case "C":
              modalOptions = {
                  headerText: 'Conventional',
                  bodyText: '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
                  '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
                  '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
              };
              break;
      }

      const data = {
          'title': modalOptions.headerText,
          'message': modalOptions.bodyText
      };
      const dialogRef1 = this._dialog.open( MessageDialogComponent, {
          data: data
          //maxWidth: '400px'
      } );

  }

  showVideoDialog( videoName ) {
      this._dialog.open(PopupPlayerComponent, {
          data: { videoId: videoName}
      });
  }

  /**
   * Get scores for display
   */
  getScore = function( area ) {
      var score = 0;

      switch ( area ) {
          case "verbal":
              if ( this.haveAsvabTestScores ) {
                  score = this.scoreSummary ? Number( this.scoreSummary.verbalAbility.va_SGS ) : 0;
              } else if ( this.haveManualAsvabTestScores ) {
                  score = Number( this.asvabScore[this.asvabScore.length - 1].verbalScore );
              }
              break;
          case "math":
              if ( this.haveAsvabTestScores ) {
                  score = this.scoreSummary ? Number( this.scoreSummary.mathematicalAbility.ma_SGS ) : 0;
              } else if ( this.haveManualAsvabTestScores ) {
                  score = Number( this.asvabScore[this.asvabScore.length - 1].mathScore );
              }
              break;
          case "science":
              if ( this.haveAsvabTestScores ) {
                  score = this.scoreSummary ? Number( this.scoreSummary.scienceTechnicalAbility.tec_SGS ) : 0;
              } else if ( this.haveManualAsvabTestScores ) {
                  score = Number( this.asvabScore[this.asvabScore.length - 1].scienceScore );
              }
              break;
          case "afqt": // no manual entry for AFQT
              score = this.scoreSummary ? Number( this.scoreSummary.aFQTRawSSComposites.afqt ) : 0;
              break;
          default:
              break;
      }
      return score;
  }

  // Add scores to session.
  /* if (this.haveAsvabTestScores) {
      window.sessionStorage.setItem('manualScores', false);
      window.sessionStorage.setItem('sV', this.getScore('verbal'));
      window.sessionStorage.setItem('sM', this.getScore('math'));
      window.sessionStorage.setItem('sS', this.getScore('science'));
      window.sessionStorage.setItem('sA', this.getScore('afqt'));
  } else if (this.haveManualAsvabTestScores) {
      window.sessionStorage.setItem('manualScores', true);
      window.sessionStorage.setItem('sV', Number(this.asvabScore[this.asvabScore.length-1].verbalScore));
      window.sessionStorage.setItem('sM', Number(this.asvabScore[this.asvabScore.length-1].mathScore));
      window.sessionStorage.setItem('sS', Number(this.asvabScore[this.asvabScore.length-1].scienceScore));
  }*/

  // Create book objects for sort/ranking - display gray book (score: 0) for manual scores
  /*vBooks = { 
      name: "verbal",
      //score: this.haveAsvabTestScores ? window.sessionStorage.getItem('sV') : 0
      score: window.sessionStorage.getItem('sV')
  }

  mBooks = {
      name: "math",
      //score: this.haveAsvabTestScores ? window.sessionStorage.getItem('sM'): 0
      score: window.sessionStorage.getItem('sM')
  }

  sBooks = {
      name: "science",
      //score: this.haveAsvabTestScores ? window.sessionStorage.getItem('sS') : 0
      score: window.sessionStorage.getItem('sS')
  }   
  */
  /**
   * Calculate book sprite x pos from ranked score
   * 1 book = lowest score
   * 2 books = middle score
   * 3 books = highest score
   * Ties would use just 2- and 3-book stacks, with the duplicate 
   * determined by whether the tie was with the highest or the lowest score
   */
  scoreSorting = function() {
      this.scoresToSort = [this.vBooks, this.mBooks, this.sBooks];
      this.sortedScores = [];

      for ( var i = 0; i < this.scoresToSort.length; i++ ) {
          this.sortedScores.push( this.scoresToSort[i] );
      }

      this.sortedScores.sort( function( a, b ) {
          return a.score - b.score;
      } );

      for ( var i = 0; i < this.sortedScores.length; i++ ) {
          this.sortedScores[i].rank = i + 1;
          this.sortedScores[i].tie = false;
      }
      this.scoreRanking();
  }

  scoreRanking = function() {
      for ( var k = 0; k < this.sortedScores.length; k++ ) {
          for ( var h = 1; h < this.sortedScores.length + 1; h++ ) {
              if ( this.sortedScores[k + h] !== undefined ) {
                  if ( this.sortedScores[k + h].tie !== true ) {
                      if ( this.sortedScores[k].score === this.sortedScores[h + k].score ) {
                          this.sortedScores[k].rank = k + 1;
                          this.sortedScores[h + k].rank = k + 1;
                          this.sortedScores[k].tie = true;
                          this.sortedScores[h + k].tie = true;
                      }
                  }
              }
          }

          // if tie scores, 2 books for low score tie, 3 books for high score tie
          if ( this.sortedScores[k].tie ) {
              if ( this.sortedScores[k].rank == 1 ) {
                  this.sortedScores[k].rank = 2; // low tie
              } else {
                  this.sortedScores[k].rank = 3; // high tie
              }
          }

          // no books for 0 score
          if ( this.sortedScores[k].score == 0 ) {
              this.sortedScores[k].rank = 0; // no score
          }

          // set xPos based on rank, -60px incraments
          this.sortedScores[k].xPos = this.sortedScores[k].rank * -32; // larger book stack (on dash)
          this.sortedScores[k].xPosSmall = this.sortedScores[k].rank * -32; // smaller book stack (in left nav)

          // set height based on rank, 6px incraments
          this.sortedScores[k].height = this.sortedScores[k].rank * 6 + 14; // larger book stack (on dash)

      }


      // Add book objects to session
      window.sessionStorage.setItem('vBooks', JSON.stringify(this.vBooks));
      window.sessionStorage.setItem('mBooks', JSON.stringify(this.mBooks));
      window.sessionStorage.setItem('sBooks', JSON.stringify(this.sBooks));
  }

  /**
   * Get book sprite x pos
   */
  getBookPos = function( area ) {
      // return bsackground style position object
      switch ( area ) {
          case "verbal":
              return { "height": this.vBooks.height + "px", "background-position": this.vBooks.xPos + "px bottom" };
          case "math":
              return { "height": this.mBooks.height + "px", "background-position": this.mBooks.xPos + "px bottom" };
          case "science":
              return { "height": this.sBooks.height + "px", "background-position": this.sBooks.xPos + "px bottom" };
      }
  }

  /**
   * Toggle score book/table display
   */
  showScoresTable = false;
  toggleScoreView = function() {
      this.showScoresTable = this.showScoresTable === false;
  }

  // Get gender from asvab scor data if we have it, store it globally
  //$rootScope.globals.currentUser.gender = this.getGenderFull(this.getGenderCode());



  demoButtonDisable = false;
  ngAfterViewInit() {

      /**
       * Flyout JQuery. Teacher demo.
       */
      if ( this.userRole == 'T' && this.isDemoTaken == 0 ) {

          $( function() {

              $( "document" ).ready( function() {
                  $( "#load-popup" ).trigger( "click" );
                  $( ".swlFlyout " ).addClass( "hiddan_arrow" );
              } );
              $( '#load-popup' ).flyout( {
                  content: '<h2 id="tour-title" class="swlFlyout_title">Get familiar with<br/> MY ASVAB CEP</h2><a class="next1">TAKE THE TOUR</a><i class="click1">&times;</i>',
                  html: true,
                  placement: 'top'
              } );
              $( document ).on( 'click', '.next1', function( e ) {
                  /*$("#sample_score").trigger('click');*/
                  $( "#sample_score" ).flyout( 'show' );
                  $( "#load-popup" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#sample_score' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click1', function( e ) {
                  $( "#load-popup" ).trigger( 'click' );
              } );


              $( '#sample_score' ).flyout( {
                  title: 'UNDERSTAND ASVAB SCORES',
                  content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><a class="next2">next</a><i class="click2">&times;</i>',
                  html: true,
                  placement: 'left',
                  trigger: 'manual'
              } );
              $( document ).on( 'click', '.next2', function( e ) {
                  $( "#skill_strength" ).trigger( 'click' );
                  $( ".click2" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#skill_strength' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click2', function( e ) {
                  /*$("#sample_score").trigger('click');*/
                  $( "#sample_score" ).flyout( 'hide' );
              } );


              $( '#skill_strength' ).flyout( {
                  title: 'UNDERSTAND ASVAB SCORES',
                  content: '<p>The most books signify skill strength.</p><a class="next3">next</a><i class="click3">&times;</i>',
                  html: true,
                  placement: 'right'
              } );
              $( document ).on( 'click', '.next3', function( e ) {
                  /*$("#see_result").trigger('click');*/
                  $( "#see_result" ).flyout( 'show' );
                  $( ".click3" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#see_result' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click3', function( e ) {
                  $( "#skill_strength" ).trigger( 'click' );
              } );


              $( '#see_result' ).flyout( {
                  title: 'UNDERSTAND ASVAB SCORES',
                  content: '<p>Access online score report and Understanding Scores tutorial.</p><a class="next4">next</a><i class="click4">&times;</i>',
                  html: true,
                  placement: 'left',
                  trigger: 'manual'
              } );
              $( document ).on( 'click', '.next4', function( e ) {
                  $( "#step1" ).trigger( 'click' );
                  $( ".click4" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#step1' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click4', function( e ) {
                  /*$("#see_result").trigger('click');*/
                  $( "#see_result" ).flyout( 'hide' );
              } );


              $( '#step1' ).flyout( {
                  title: 'FIND YOUR INTERESTS ',
                  content: '<p>90 item interest inventory based on John Holland<em>s</em> codes of career choice.</p><a class="next5">next</a><i class="click5">&times;</i>',
                  html: true,
                  placement: 'top'

              } );
              $( document ).on( 'click', '.next5', function( e ) {
                  $( "#step2" ).trigger( 'click' );
                  $( ".click5" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#step2' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click5', function( e ) {
                  $( "#step1" ).trigger( 'click' );
              } );


              $( '#step2' ).flyout( {
                  title: 'SEARCH THE OCCU-FIND',
                  content: '<p>Sort 1,000+ careers using your unique skill & interest combination to find careers that match. Learn about the variety of ways to get started in any career.</p><a class="next6">next</a><i class="click6">&times;</i>',
                  html: true,
                  placement: 'top'
              } );
              $( document ).on( 'click', '.next6', function( e ) {
                  $( "#step3" ).trigger( 'click' );
                  $( ".click6" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#step3' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click6', function( e ) {
                  $( "#step2" ).trigger( 'click' );
              } );


              $( '#step3' ).flyout( {
                  title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
                  content: '<p>Create a plan, track accomplishments, and favorite careers. </p><a class="next7">next</a><i class="click7">&times;</i>',
                  html: true,
                  placement: 'top'
              } );
              $( document ).on( 'click', '.next7', function( e ) {
                  /*$("#classroom_activities").trigger('click');*/
                  $( "#classroom_activities" ).flyout( 'show' );
                  $( ".click7" ).trigger( 'click' );
                  $( 'html, body' ).animate( { scrollTop: $( '#classroom_activities' ).offset().top - 300 }, 600 );
              } );
              $( document ).on( 'click', '.click7', function( e ) {
                  $( "#step3" ).trigger( 'click' );
              } );


              $( '#classroom_activities' ).flyout( {
                  title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
                  content: '<p>Access activities you can use now to connect your classroom to the real world.</p><a class="next8">Finished</a><i class="click8">&times;</i>',
                  html: true,
                  trigger: 'manual'
              } );


              $( document ).on( 'click', '.click8,.next8', function( e ) {

                  // Store teacher demo completion information to database.
                  if ( !this.demoButtonDisable ) { // disable if but was clicked

                      this.demoButtonDisable = true; // disable button

                      //temp
                      $( "#classroom_activities" ).flyout( 'hide' );
                      /*var promise = PortfolioRestFactory.insertTeacherDemoIsTaken(obj);
                      promise.then(function(response) {
                          $("#classroom_activities").flyout('hide');
                      }, function(reason) {
                          this.demoButtonDisable = false; // enable button if failed
                      }, function(update) {
                      });*/
                  }

              } );


              $( document ).on( 'click', '.swlFlyout_content > a', function( e ) {
                  return false;
              } );

          } );

          $( function() {
              if ( $( window ).width() < 640 ) {

                  $( "document" ).ready( function() {
                      $( 'html, body' ).animate( { scrollTop: $( '#load-popup' ).offset().top - 260 } );
                  } );


                  $( '#sample_score' ).flyout( {
                      title: 'UNDERSTAND ASVAB SCORES',
                      content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><a class="next2">next</a><i class="click2">x</i>',
                      html: true,
                      placement: 'top'
                  } );


                  $( '#skill_strength' ).flyout( {
                      title: 'UNDERSTAND ASVAB SCORES',
                      content: '<p>The most books signify skill strength.</p><a class="next3">next</a><i class="click3">x</i>',
                      html: true,
                      placement: 'bottom'
                  } );


                  $( '#see_result' ).flyout( {
                      title: 'UNDERSTAND ASVAB SCORES',
                      content: '<p>Access online score report and Understanding Scores tutorial.</p><a class="next4">next</a><i class="click4">x</i>',
                      html: true,
                      placement: 'bottom'
                  } );
              }


          } );
      }
  }

  trackClick(formName) {
      return;
  }
}
