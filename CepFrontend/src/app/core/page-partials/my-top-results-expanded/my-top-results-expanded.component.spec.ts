import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTopResultsExpandedComponent } from './my-top-results-expanded.component';

describe('MyTopResultsComponent', () => {
  let component: MyTopResultsExpandedComponent;
  let fixture: ComponentFixture<MyTopResultsExpandedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTopResultsExpandedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTopResultsExpandedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
