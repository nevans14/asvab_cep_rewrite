import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { MessageDialogComponent } from '../../dialogs/message-dialog/message-dialog.component';
import { MatDialog } from '@angular/material';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';

@Component({
  selector: 'app-occufind-grid-add-favorites',
  templateUrl: './occufind-grid-add-favorites.component.html',
  styleUrls: ['./occufind-grid-add-favorites.component.scss']
})
export class OccufindGridAddFavoritesComponent implements OnInit {

    public favorites;
    @Input() parent;
    @Input() occupation;

    constructor( private _user: UserService,
        private _favoritesRestService: FavoritesRestFactoryService,
        private _dialog: MatDialog ) { }

    ngOnInit() {
        this.favorites = this.parent.favorites;
    }

    showModal( message ) {

        const data = {
            'title': 'Favorites',
            'message': message
        };
        const dialogRef1 = this._dialog.open( MessageDialogComponent, {
            data: data,
            //hasBackdrop: true,
            //disableClose: true,
            maxWidth: '400px'
        } );
    }

    /**
     * Checks if the occupation is favorited
     */
    isFavorited( socId ) {
        if ( this.parent.occuFindUserLoggedIn) {
            return this.doesOccExistInFavorites( this.occupation.onetSoc );
        }
        return false;
    }

    /**
     * Remove from favorites
     */
    removeFromFavorites( socId ) {
        var favoriteObj = this.getOccInFavorites( this.occupation.onetSoc );
        this._favoritesRestService.deleteFavoriteOccupation( favoriteObj.id ).then(( success ) => {
            // remove get and remove occ from favorites
            var index = this.getIndexOfOccInFavorites( this.occupation.onetSoc );
            this.favorites.splice( index, 1 );
            //update favorites
            this._user.setFavorites( this.favorites )
            // prepare and show modal
            this.showModal( '<b>' + favoriteObj.title + '</b> was removed from your favorites.' );
        }, ( error ) => {
            console.error( error );
        } )
    }

    /**
     * Add to favorites.
     */
    addToFavorites() {
        if ( this.favorites.length >= 10 ) {
            this.showModal( 'Favorite Occupation list is full.' );
            return;
        }

        var favoriteObject = {
            onetSocCd: this.occupation.onetSoc,
        };

        this._favoritesRestService.insertFavoriteOccupation( favoriteObject ).then(( success ) => {
            // add to the scope of favorites
            this.favorites.push( {
                id: success.id,
                onetSocCd: this.occupation.onetSoc,
                title: this.occupation.occupationTitle
            } );
            //update favorites
            this._user.setFavorites( this.favorites )
            this.showModal( '<b>' + this.occupation.occupationTitle + '</b> is added to your favorites.' );
        }, ( error ) => {
            console.error( error );
        } );
    }

    /**
     * Helpers methods
     */
    /**
     * Checks if the current occupation has been favorited
     */
    doesOccExistInFavorites = function( socId ) {
        var isFound = false;
        for ( var i = 0; i < this.favorites.length; i++ ) {
            if ( this.favorites[i].onetSocCd == socId )
                return isFound = true;
        }
        return isFound;
    }

    /**
     * Returns the occupation in the list of favorites
     */
    getOccInFavorites = function( socId ) {
        var occ = undefined;
        for ( var i = 0; i < this.favorites.length; i++ ) {
            if ( this.favorites[i].onetSocCd == socId )
                return occ = this.favorites[i];
        }
        return occ;
    }

    /**
     * Returns the index of the occupation in the list of favorites
     */
    getIndexOfOccInFavorites = function( socId ) {
        var index = -1;
        for ( var i = 0; i < this.favorites.length; i++ ) {
            if ( this.favorites[i].onetSocCd == socId )
                return index = i;
        }
        return index;
    }

}
