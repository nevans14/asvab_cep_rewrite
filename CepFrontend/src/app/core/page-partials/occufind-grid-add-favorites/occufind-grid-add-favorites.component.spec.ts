import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccufindGridAddFavoritesComponent } from './occufind-grid-add-favorites.component';

describe('OccufindGridAddFavoritesComponent', () => {
  let component: OccufindGridAddFavoritesComponent;
  let fixture: ComponentFixture<OccufindGridAddFavoritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccufindGridAddFavoritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccufindGridAddFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
