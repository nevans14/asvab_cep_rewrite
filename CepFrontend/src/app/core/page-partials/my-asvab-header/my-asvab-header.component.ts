import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-my-asvab-header',
  templateUrl: './my-asvab-header.component.html',
  styleUrls: ['./my-asvab-header.component.scss']
})
export class MyAsvabHeaderComponent implements OnInit {

  userRole: any;
  currentUser: any;

  //ignore mentor only pages
  pathList = [
    { url: '/mentor-dashboard' },
    { url: '/mentor-student-detail' },
    { url: '/mentor-setting' }
  ]

  ignorePath: boolean;

  constructor(private _userService: UserService,
    private _router: Router) {
    this.currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    this.userRole = this.currentUser ? this.currentUser.role : null;
    this.ignorePath = false;
  }

  ngOnInit() {

    //get current path; if current path is in list then return full error
    const currentPath = this._router.url;
    if (this.currentPathInPathList(currentPath)) {
      this.ignorePath = true;
    }

  }


  /**
 * Checks url against a list; if url is present return true
 * @param path url to check
 */
  currentPathInPathList(path): boolean {
    let foundPath = this.pathList.find(x => path.match(x.url));
    return (foundPath) ? true : false;
  }

  routeToOverview() {
    this._router.navigate(['/mentor-dashboard']);
  }
}
