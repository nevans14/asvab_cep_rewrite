import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAsvabHeaderComponent } from './my-asvab-header.component';

describe('MyAsvabHeaderComponent', () => {
  let component: MyAsvabHeaderComponent;
  let fixture: ComponentFixture<MyAsvabHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAsvabHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAsvabHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
