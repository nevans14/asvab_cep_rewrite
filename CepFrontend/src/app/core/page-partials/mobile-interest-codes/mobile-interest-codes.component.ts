import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfigService } from 'app/services/config.service';

@Component({
    selector: 'app-mobile-interest-codes',
    templateUrl: './mobile-interest-codes.component.html',
    styleUrls: ['./mobile-interest-codes.component.scss']
})
export class MobileInterestCodesComponent implements OnInit {

    showScoresTable = false;

    // Retrieve scores from session.
    verbalScore;
    mathScore;
    scienceScore;
    afqtScore;
    haveManualAsvabTestScores = true;

    // Retrieve book objects from session.
    vBooks: any;
    mBooks: any;
    sBooks: any;
    // vBooks = {
    //     name: 'verbal',
    //     //score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sV') : 0
    //     score: sessionStorage.getItem('sV'),
    //     xPosSmall: 10
    // };

    // mBooks = {
    //     name: 'math',
    //     //score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sM'): 0
    //     score: sessionStorage.getItem('sM'),
    //     xPosSmall: 10
    // };

    // sBooks = {
    //     name: 'science',
    //     //score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sS') : 0
    //     score: sessionStorage.getItem('sS'),
    //     xPosSmall: 10
    // };	

    constructor(
        private _configService: ConfigService,
        private _router: Router,
        private _dialog: MatDialog
    ) { }

    ngOnInit() {
        // Retrieve scores from session.
        this.verbalScore = sessionStorage.getItem('sV');
        this.mathScore = sessionStorage.getItem('sM');
        this.scienceScore = sessionStorage.getItem('sS');
        this.afqtScore = sessionStorage.getItem('sA');
        this.haveManualAsvabTestScores = sessionStorage.getItem('manualScores') === 'true';

        // Retrieve book objects from session.
        this.vBooks = sessionStorage.getItem('vBooks');
        this.mBooks = sessionStorage.getItem('mBooks');
        this.sBooks = sessionStorage.getItem('sBooks');
    }

    toggleScoreView() {
        this.showScoresTable = this.showScoresTable === false;
    }

    /**
	  * Get scores for display
	  */
    getScore(area) {
        let score = 0;
        switch (area) {
            case 'verbal':
                score = this.verbalScore;
                break;
            case 'math':
                score = this.mathScore;
                break;
            case 'science':
                score = this.scienceScore;
                break;
            case 'afqt':
                score = this.afqtScore;
                break;
            default:
                break;
        }
        return score;
    }

    /**
    * Get book sprite x pos
    */
    getBookPos(area) {
        // return bsackground style position object
        switch (area) {
            case 'verbal':
                return { 'background-position': this.vBooks.xPosSmall + 'px 0px' };
            case 'math':
                return { 'background-position': this.mBooks.xPosSmall + 'px 0px' };
            case 'science':
                return { 'background-position': this.sBooks.xPosSmall + 'px 0px' };
        }
    }

}
