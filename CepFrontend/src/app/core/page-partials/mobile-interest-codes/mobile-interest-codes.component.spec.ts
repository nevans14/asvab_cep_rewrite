import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileInterestCodesComponent } from './mobile-interest-codes.component';

describe('HeaderComponent', () => {
  let component: MobileInterestCodesComponent;
  let fixture: ComponentFixture<MobileInterestCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileInterestCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileInterestCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
