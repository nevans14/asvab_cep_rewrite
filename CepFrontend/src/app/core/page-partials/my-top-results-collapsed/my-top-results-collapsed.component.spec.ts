import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTopResultsCollapsedComponent } from './my-top-results-collapsed.component';

describe('MyTopResultsCollapsedComponent', () => {
  let component: MyTopResultsCollapsedComponent;
  let fixture: ComponentFixture<MyTopResultsCollapsedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTopResultsCollapsedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTopResultsCollapsedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
