import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'app/services/user.service';
import { MatDialog } from '@angular/material/dialog';

import $ from 'jquery'
import { GuidedTipsConfigDialogComponent } from 'app/core/dialogs/guided-tips-config-dialog/guided-tips-config-dialog.component';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { ClassroomActivitySubmission } from 'app/core/models/classroomActivity';
import { StudentNotificationDialogComponent } from 'app/core/dialogs/student-notification-dialog/student-notification-dialog.component';
import { UtilityService } from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';
declare var $: $

@Component({
  selector: 'app-left-navigation-v2-expanded',
  templateUrl: './left-navigation-v2-expanded.component.html',
  styleUrls: ['./left-navigation-v2-expanded.component.scss']
})
export class LeftNavigationV2ExpandedComponent implements OnInit, OnDestroy {
  @Input() selectedTab: string;
  @Output() menuCollapsed = new EventEmitter<boolean>();

  collapsed: boolean = false;
  userCompletion;
  learnPercentCompleted = 0;
  learnPlusCompleted = false;
  explorePercentCompleted = 0;
  explorePlusCompleted = false;
  planPercentCompleted = 0;
  planPlusCompleted = false;
  subscription: Subscription;
  userUpdatesSubscription: Subscription;


  userRole: any;
  currentUser: any;
  currentUserName: any;
  unreadMessageCounter: number = 0;
  helpImg: any;

  // Guided Tour
  guidedTipConfig = {
    selectALL: false,
    exploreCareer: false,
    helpLearn: false,
    helpPlan: false,
    goToMilitary: false,
    goToCollege: false,
    workBasedLearn: false,
    involveCounselorParent: false,
  }

  constructor(
    private _user: UserService,
    private _dialog: MatDialog,
    private _portfolioService: PortfolioService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _classroomActivityService: ClassroomActivityService,
    private _utilityService: UtilityService,
    private _configService: ConfigService,
  ) {
    this.currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    this.userRole = this.currentUser ? this.currentUser.role : null;
  }

  ngOnInit() {
    this.checkUserCompletion();
    this.subscription = this._user.getMessage().subscribe(msg => {
      this.checkUserCompletion();
    })
    this.collapsed = this._user.getLeftNavCollapsed();

    this._user.getUsername().then((results: any) => {
      if (results) {
        this.currentUserName = results.username;
      }
    })

    this.setUnreadMessageCount();
    this.setUserSubscription();

    this.helpImg = this._configService.getImageUrl() + 'images/help.svg';
  }

  setUserSubscription() {
    this.userUpdatesSubscription = this._user.getUserUpdates().subscribe(hasUpdate => {
      this.setUnreadMessageCount();
    })
  }

  async setUnreadMessageCount() {
    this.unreadMessageCounter = 0;
    let directMessages = await this._user.getDirectMessages(); //get all direct messages involving the current signed in user

    let unreadCommentsPortfolio = await this._portfolioService.getPortfolioUnreadComments();
    let unreadCommentsPlan = await this._userOccupationPlanService.getUnreadComments();

    //get all user occupation plans
    let userOccupationPlan = await this._userOccupationPlanService.getAll();

    //get classroom activities
    let classroomActivities: any = await this._classroomActivityService.getActivities();
    await Promise.all(classroomActivities.map(async (activity: any) => {
      await Promise.all(activity.submissions.map(async (classroomActivitySubmission: ClassroomActivitySubmission) => {
        //for each submission retrieve its logs
        let activityLogs: any = await this._classroomActivityService.getMySubmissionLogs(classroomActivitySubmission.id);
        classroomActivitySubmission.logs = activityLogs;
      }))
    }))

    //get portfolio
    let portfolio = await this._portfolioService.getPortfolio();

    await Promise.all([unreadCommentsPortfolio, unreadCommentsPlan, userOccupationPlan, classroomActivities, portfolio, directMessages]).then((done: any) => {

      let _unreadCommentsPortfolio: any = done[0];
      let _unreadCommentsPlan: any = done[1];
      let _userOccupationPlans: any = done[2];
      let _classroomActivities: any = done[3];
      let _portfolio: any = done[4];
      let _directMessages: any = done[5];

      if (_unreadCommentsPortfolio) {
        this.unreadMessageCounter += _unreadCommentsPortfolio.length;
      }
      if (_unreadCommentsPlan) {
        this.unreadMessageCounter += _unreadCommentsPlan.length;
      }

      if (_userOccupationPlans && _userOccupationPlans.length > 0) {
        _userOccupationPlans.forEach((userOccupationPlan: any) => {
          if (userOccupationPlan.logs && userOccupationPlan.logs.length > 0) {
            let sortedLogs = userOccupationPlan.logs.sort((a, b) => {
              let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
              return da - db;
            });

            let lastLog = sortedLogs[sortedLogs.length - 1];
            if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
              this.unreadMessageCounter += 1;
            }
          }
        })

      }

      if (_classroomActivities && _classroomActivities.length > 0) {
        _classroomActivities.forEach((activity: any) => {
          this.unreadMessageCounter = this.unreadMessageCounter + activity.numOfUnreadMessages;  //for unread comments

          if (activity.submissions && activity.submissions.length > 0) {
            activity.submissions.forEach((submission: any) => {
              //for logs
              if (submission.logs && submission.logs.length > 0) {
                let sortedLogs = submission.logs.sort((a, b) => {
                  let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                    db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
                  return da - db;
                });

                let lastLog = sortedLogs[sortedLogs.length - 1];
                if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
                  this.unreadMessageCounter += 1;
                }
              }
            })
          }

        })

      }

      if (_portfolio && _portfolio.logs && _portfolio.logs.length > 0) {
        let sortedLogs = _portfolio.logs.sort((a, b) => {
          let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
            db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
          return da - db;
        });

        let lastLog = sortedLogs[sortedLogs.length - 1];
        if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
          this.unreadMessageCounter += 1;
        }

      }

      if (_directMessages && _directMessages.length > 0) {
        this.unreadMessageCounter = _directMessages.filter(x => !x.hasBeenRead && x.fromUser != this.currentUserName).length + this.unreadMessageCounter;
      }
    })
  }

  checkUserCompletion() {
    this._user.getCompletion().subscribe(data => {
      this.userCompletion = data;
      this.learnPercentCompleted = 0;
      this.learnPlusCompleted = false;
      this.explorePercentCompleted = 0;
      this.explorePlusCompleted = false;
      this.planPercentCompleted = 0;
      this.planPlusCompleted = false;

      if (this._user.checkUserCompletion(this.userCompletion, UserService.HAS_ASVAB_SCORE)
        && this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_ASVAB_SCORE)) {
        this.learnPercentCompleted = this.learnPercentCompleted + 50;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_FYI)) {
        this.learnPercentCompleted = this.learnPercentCompleted + 50;
      }

      if (this.learnPercentCompleted >= 99) {
        if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_WORK_VALUE)
          || this._user.checkUserCompletion(this.userCompletion, UserService.LIKE_DISLIKE_MORE_ABOUT_ME)) {
          this.learnPlusCompleted = true;
        } else {
          this.learnPlusCompleted = false;
        }
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_OCCUPATION_SEARCH)) {
        this.explorePercentCompleted = this.explorePercentCompleted + 33;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_CAREER_DETAIL)) {
        this.explorePercentCompleted = this.explorePercentCompleted + 33;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION)) {
        this.explorePercentCompleted = this.explorePercentCompleted + 33;
      }

      if (this.explorePercentCompleted >= 99) {
        if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_RESOURCE)
          || this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION_ADDITIONAL)
          || this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_OCCUPATION_DETAIL_LINK)
          || this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_COLLEGE)
          || this._user.checkUserCompletion(this.userCompletion, UserService.TO_CTM_FROM_OCCU_DETAIL)
          || this._user.checkUserCompletion(this.userCompletion, UserService.COMPARED_OCCUPATIONS)
          || this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_CAREER_CLUSTER)
          || this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_NOTES)
          || this._user.checkUserCompletion(this.userCompletion, UserService.REFINED_OCCUFIND_SEARCH)
          || this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_CC)
        ) {
          this.explorePlusCompleted = true;
        } else {
          this.explorePlusCompleted = false;
        }
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CREATED_PLAN)) {
        this.planPercentCompleted = this.planPercentCompleted + 33;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.STARTED_PORTFOLIO)) {
        this.planPercentCompleted = this.planPercentCompleted + 33;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CONNECTED_TO_MENTOR)) {
        this.planPercentCompleted = this.planPercentCompleted + 33;
      }

      if (this.planPercentCompleted >= 99) {
        if (this._user.checkUserCompletion(this.userCompletion, UserService.ADDED_DATE)
          || this._user.checkUserCompletion(this.userCompletion, UserService.ADDED_TASK)
          || this._user.checkUserCompletion(this.userCompletion, UserService.ADDED_CLASSROOM_ACTIVITY)
        ) {
          this.planPlusCompleted = true;
        } else {
          this.planPlusCompleted = false;
        }
      }

      // Set guided tour selection for the first time
      const guidedTipConfigs = this._user.getCompletionByType(data, UserService.GUIDED_TIP_CONFIG);
      if (!guidedTipConfigs) {
        this._user.setCompletion(UserService.GUIDED_TIP_CONFIG, JSON.stringify(this.guidedTipConfig));
      }
    });
  }

  collapseMenu() {
    this.collapsed = true;
    this.menuCollapsed.emit(true);
    this._user.setLeftNavCollapsed(true);
  }

  guidedTour() {
    this._dialog.open(GuidedTipsConfigDialogComponent, {
      maxWidth: 500,
    })
  }

  ngAfterViewInit() {
    // let angularThis = this;
    // $( function() {
    //   function getRelativeCoords(percentage) {
    //     var exprBase = ((-percentage) * 2 * Math.PI) + 0.5 * Math.PI,
    //       coordX = Math.cos(exprBase),
    //       coordY = -Math.sin(exprBase);
    //     return [coordX, coordY];
    //   }
    //   function getArcCoords(initCoords, radius, percentage) {
    //     var relativeCoords = getRelativeCoords(percentage),
    //       coordX = Math.round((relativeCoords[0] * radius + initCoords[0]) * 100) / 100,
    //       coordY = Math.round((relativeCoords[1] * radius + initCoords[1]) * 100) / 100;
    //     return [coordX, coordY];
    //   }
    //   function DrawArc() {
    //     $('.circular-progress-bar').each(function(){
    //       var input = $(this).find('.percentage_number span'),
    //         percentage = (+input.text()) / 100,
    //         coordsArc1 = getArcCoords([50, 50], 49, percentage),
    //         coordsArc2 = getArcCoords([50, 50], 35, percentage),
    //         humanPercentage = Math.round(percentage * 100),
    //         revertArc = humanPercentage > 50 ? '1' : '0',
    //         path1Str = $(this).find('.outer-arc').attr('d').split(','),
    //         path2Str = $(this).find('.inner-arc').attr('d').split(','),
    //         useArcs = humanPercentage > 0 && humanPercentage < 100,
    //         arcs = $(this).find('.arcs'),
    //         circles = $(this).find('.circles');
    //         arcs.toggle(useArcs);
    //         circles.toggle(!useArcs);

    //       if (useArcs) {
    //         path2Str[2] = path1Str[2] = revertArc;
    //         path1Str[4] = coordsArc1.join(' ');
    //         path2Str[4] = coordsArc2.join(' ');
    //         $(this).find('.outer-arc').attr('d', path1Str.join(' '));
    //         $(this).find('.inner-arc').attr('d', path2Str.join(' '));
    //       } else {
    //         var elClass = circles.attr('class');
    //         if (humanPercentage && ~elClass.indexOf('empty-circles')) {
    //         circles.attr('class', elClass.replace(' empty-circles', ''));
    //         } else if (!humanPercentage && !~elClass.indexOf('empty-circles')) {
    //         circles.attr('class', elClass + ' empty-circles');
    //         }
    //       }
    //     });
    //   }
    //   DrawArc();
    // });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.userUpdatesSubscription) {
      this.userUpdatesSubscription.unsubscribe();
    }
  }


  showNotification() {

    let dialogRef = this._dialog.open(StudentNotificationDialogComponent, {
      maxWidth: "800px",
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.setUnreadMessageCount();
      }, 1000)
    })

  }
}
