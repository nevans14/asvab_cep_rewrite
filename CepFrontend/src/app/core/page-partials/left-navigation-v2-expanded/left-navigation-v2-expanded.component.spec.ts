import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationV2ExpandedComponent } from './left-navigation-v2-expanded.component';

describe('LeftNavigationV2ExpandedComponent', () => {
  let component: LeftNavigationV2ExpandedComponent;
  let fixture: ComponentFixture<LeftNavigationV2ExpandedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationV2ExpandedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationV2ExpandedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
