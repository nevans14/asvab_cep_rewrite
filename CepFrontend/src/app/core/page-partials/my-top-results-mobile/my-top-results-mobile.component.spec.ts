import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTopResultsMobileComponent } from './my-top-results-mobile.component';

describe('MyTopResultsMobileComponent', () => {
  let component: MyTopResultsMobileComponent;
  let fixture: ComponentFixture<MyTopResultsMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTopResultsMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTopResultsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
