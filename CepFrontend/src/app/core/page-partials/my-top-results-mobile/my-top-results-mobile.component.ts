import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-top-results-mobile',
  templateUrl: './my-top-results-mobile.component.html',
  styleUrls: ['./my-top-results-mobile.component.scss']
})
export class MyTopResultsMobileComponent implements OnInit {

  myResultscollapsed = true;

  constructor() { }

  ngOnInit() {
  }

}
