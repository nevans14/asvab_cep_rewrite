import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-left-navigation-v2-mentor',
  templateUrl: './left-navigation-v2-mentor.component.html',
  styleUrls: ['./left-navigation-v2-mentor.component.scss']
})
export class LeftNavigationV2MentorComponent implements OnInit {

  @Input() selectedTab: string;

  path;
  isParent: boolean;

  constructor(
    private _router: Router,
    private _user: UserService,
  ) {
    this.isParent = false;
  }

  ngOnInit() {
    this.path = this._router.url;
    this.isParent = (this._user.getUser().currentUser.role.toLowerCase() == 'd') ? true : false;
  }

  toDashboard() {
    if (this._router.url !== '/mentor-dashboard') {
      this._router.navigate(['/mentor-dashboard']);
    }
  }

  routeToDashboardStyle() {
    if (this.path !== '/mentor-dashboard') {
      return { "pointer-events": "auto", "cursor": "pointer" }
    }
  }

}
