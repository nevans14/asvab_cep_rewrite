import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationV2MentorComponent } from './left-navigation-v2-mentor.component';

describe('LeftNavigationV2MentorComponent', () => {
  let component: LeftNavigationV2MentorComponent;
  let fixture: ComponentFixture<LeftNavigationV2MentorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationV2MentorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationV2MentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
