import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationV2CollapsedComponent } from './left-navigation-v2-collapsed.component';

describe('LeftNavigationV2CollapsedComponent', () => {
  let component: LeftNavigationV2CollapsedComponent;
  let fixture: ComponentFixture<LeftNavigationV2CollapsedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationV2CollapsedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationV2CollapsedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
