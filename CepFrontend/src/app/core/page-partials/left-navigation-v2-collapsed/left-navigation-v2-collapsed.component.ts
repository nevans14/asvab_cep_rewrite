import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FindYourInterestManualScoreInputComponent } from 'app/core/dialogs/find-your-interest-manual-score-input/find-your-interest-manual-score-input.component';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { UserService } from 'app/services/user.service';
import $ from 'jquery'
import { GuidedTipsConfigDialogComponent } from 'app/core/dialogs/guided-tips-config-dialog/guided-tips-config-dialog.component';
import { Subscription } from 'rxjs';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { ClassroomActivity, ClassroomActivitySubmission } from 'app/core/models/classroomActivity';
import { StudentNotificationDialogComponent } from 'app/core/dialogs/student-notification-dialog/student-notification-dialog.component';
import { UtilityService } from 'app/services/utility.service';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { NotesService } from 'app/services/notes.service';
import { SchoolNotesService } from 'app/services/school-notes.service';
import { CareerClusterNotesService } from 'app/career-cluster/services/career-cluster-notes.service';
import { ConfigService } from 'app/services/config.service';

declare var $: $

@Component({
  selector: 'app-left-navigation-v2-collapsed',
  templateUrl: './left-navigation-v2-collapsed.component.html',
  styleUrls: ['./left-navigation-v2-collapsed.component.scss']
})
export class LeftNavigationV2CollapsedComponent implements OnInit {
  @Input() selectedTab: string;

  numberTestTaken: any;
  userUpdatesSubscription: Subscription;

  userRole: any;
  currentUser: any;
  unreadMessageCounter: number = 0;
  currentUserName: any;
  isWorkValueResults;
  hasNotes: boolean = false;
  helpImg: any;

  constructor(private _matDialog: MatDialog,
    private _FYIScoreService: FindYourInterestService,
    private _user: UserService,
    private _portfolioService: PortfolioService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _classroomActivityService: ClassroomActivityService,
    private _utilityService: UtilityService,
    private _notesService: NotesService,
    private _schoolNotesService: SchoolNotesService,
    private _careerClusterNotesService: CareerClusterNotesService,
    private _configService: ConfigService,
    ) {
    this.currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    this.userRole = this.currentUser ? this.currentUser.role : null;
  }

  async ngOnInit() {
    this.isWorkValueResults = JSON.parse(window.sessionStorage.getItem('workValueTopResults')) ? true : false;

    this._FYIScoreService.getNumberOfTestTaken().then(response => {
      this.numberTestTaken = response;
    });

    this._user.getUsername().then((results: any) => {
      if (results) {
        this.currentUserName = results.username;
      }
    })

    this.setUnreadMessageCount();
    this.setUserSubscription();
    this.setNoteCount();

    this.helpImg = this._configService.getImageUrl() + 'images/help.svg';
  }

  ngAfterViewInit() {

    var $menu = $('.sidebar-sticky').first(),
      offset = $menu.offset().top,
      menu_ht = $menu.outerHeight(true) + 20,
      footer = $('#content-wrap').outerHeight();

    $(window).scroll(function () {
      if ($(window).outerWidth() > 639) {
        if (($(window).scrollTop() + menu_ht + 91) >= ($('#content-wrap').outerHeight() + 30)) {
          $menu.css({ 'z-index': 1, 'position': 'absolute', 'top': 'auto', 'bottom': 20 });
        } else if ($(window).scrollTop() >= 10) {
          $menu.css({ 'z-index': 1, 'position': 'fixed', 'top': 91, 'width': 70 });
        } else {
          $menu.removeAttr('style');
        }
      } else {
        if ($(window).scrollTop() >= $('header').first().outerHeight()) {
          $menu.css({ 'z-index': 1, 'position': 'fixed', 'top': 0, 'width': '100%' }).parent().css('margin-bottom', $menu.outerHeight());
        } else {
          $menu.removeAttr('style').parent().removeAttr('style');
        }
      }
    });

    $(window).on("resize", function () {
      $(window).trigger('scroll');
    });

    $(function () {
      function getRelativeCoords(percentage) {
        var exprBase = ((-percentage) * 2 * Math.PI) + 0.5 * Math.PI,
          coordX = Math.cos(exprBase),
          coordY = -Math.sin(exprBase);
        return [coordX, coordY];
      }
      function getArcCoords(initCoords, radius, percentage) {
        var relativeCoords = getRelativeCoords(percentage),
          coordX = Math.round((relativeCoords[0] * radius + initCoords[0]) * 100) / 100,
          coordY = Math.round((relativeCoords[1] * radius + initCoords[1]) * 100) / 100;
        return [coordX, coordY];
      }
      function DrawArc() {
        $('.view_plan .circular-progress-bar').each(function () {
          var input = $(this).find('.percentage_number span'),
            percentage = (+input.text()) / 100,
            coordsArc1 = getArcCoords([50, 50], 49, percentage),
            coordsArc2 = getArcCoords([50, 50], 35, percentage),
            humanPercentage = Math.round(percentage * 100),
            revertArc = humanPercentage > 50 ? '1' : '0',
            path1Str = $(this).find('.outer-arc').attr('d').split(','),
            path2Str = $(this).find('.inner-arc').attr('d').split(','),
            useArcs = humanPercentage > 0 && humanPercentage < 100,
            arcs = $(this).find('.arcs'),
            circles = $(this).find('.circles');
          arcs.toggle(useArcs);
          circles.toggle(!useArcs);

          if (useArcs) {
            path2Str[2] = path1Str[2] = revertArc;
            path1Str[4] = coordsArc1.join(' ');
            path2Str[4] = coordsArc2.join(' ');
            $(this).find('.outer-arc').attr('d', path1Str.join(' '));
            $(this).find('.inner-arc').attr('d', path2Str.join(' '));
          } else {
            var elClass = circles.attr('class');
            if (humanPercentage && ~elClass.indexOf('empty-circles')) {
              circles.attr('class', elClass.replace(' empty-circles', ''));
            } else if (!humanPercentage && !~elClass.indexOf('empty-circles')) {
              circles.attr('class', elClass + ' empty-circles');
            }
          }
        });
      }
      DrawArc();
    });
  }

  setUserSubscription() {
    this.userUpdatesSubscription = this._user.getUserUpdates().subscribe(hasUpdate => {
      this.setUnreadMessageCount();
    })
  }

  async setUnreadMessageCount() {
    this.unreadMessageCounter = 0;
    let directMessages = await this._user.getDirectMessages(); //get all direct messages involving the current signed in user
    let unreadCommentsPortfolio = await this._portfolioService.getPortfolioUnreadComments();
    let unreadCommentsPlan = await this._userOccupationPlanService.getUnreadComments();

    //get all user occupation plans
    let userOccupationPlan = await this._userOccupationPlanService.getAll();

    //get classroom activities
    let classroomActivities: any = await this._classroomActivityService.getActivities();
    await Promise.all(classroomActivities.map(async (activity: any) => {
      await Promise.all(activity.submissions.map(async (classroomActivitySubmission: ClassroomActivitySubmission) => {
        //for each submission retrieve its logs
        let activityLogs: any = await this._classroomActivityService.getMySubmissionLogs(classroomActivitySubmission.id);
        classroomActivitySubmission.logs = activityLogs;
      }))
    }))

    //get portfolio
    let portfolio = await this._portfolioService.getPortfolio();

    await Promise.all([unreadCommentsPortfolio, unreadCommentsPlan, userOccupationPlan, classroomActivities, portfolio, directMessages]).then((done: any) => {

      let _unreadCommentsPortfolio: any = done[0];
      let _unreadCommentsPlan: any = done[1];
      let _userOccupationPlans: any = done[2];
      let _classroomActivities: any = done[3];
      let _portfolio: any = done[4];
      let _directMessages: any = done[5];

      if (_unreadCommentsPortfolio) {
        this.unreadMessageCounter += _unreadCommentsPortfolio.length;
      }
      if (_unreadCommentsPlan) {
        this.unreadMessageCounter += _unreadCommentsPlan.length;
      }

      if (_userOccupationPlans && _userOccupationPlans.length > 0) {
        _userOccupationPlans.forEach((userOccupationPlan: any) => {
          if (userOccupationPlan.logs && userOccupationPlan.logs.length > 0) {
            let sortedLogs = userOccupationPlan.logs.sort((a, b) => {
              let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
              return da - db;
            });

            let lastLog = sortedLogs[sortedLogs.length - 1];
            if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
              this.unreadMessageCounter += 1;
            }
          }
        })

      }

      if (_classroomActivities && _classroomActivities.length > 0) {
        _classroomActivities.forEach((activity: any) => {
          this.unreadMessageCounter = this.unreadMessageCounter + activity.numOfUnreadMessages;  //for unread comments

          if (activity.submissions && activity.submissions.length > 0) {
            activity.submissions.forEach((submission: any) => {
              //for logs
              if (submission.logs && submission.logs.length > 0) {
                let sortedLogs = submission.logs.sort((a, b) => {
                  let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                    db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
                  return da - db;
                });

                let lastLog = sortedLogs[sortedLogs.length - 1];
                if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
                  this.unreadMessageCounter += 1;
                }
              }
            })
          }

        })

      }

      if (_portfolio && _portfolio.logs && _portfolio.logs.length > 0) {
        let sortedLogs = _portfolio.logs.sort((a, b) => {
          let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
            db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
          return da - db;
        });

        let lastLog = sortedLogs[sortedLogs.length - 1];
        if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
          this.unreadMessageCounter += 1;
        }
      }

      if (_directMessages && _directMessages.length > 0) {
        this.unreadMessageCounter = _directMessages.filter(x => !x.hasBeenRead && x.fromUser != this.currentUserName).length + this.unreadMessageCounter;
      }

    })
  }

  async setNoteCount() {

    let notes = this._notesService.getNotes();

    let schoolNotes = this._schoolNotesService.getNotes()

    let careerClusterNotes = this._careerClusterNotesService.getCareerClusterNotes()

    await Promise.all([notes, schoolNotes, careerClusterNotes]).then((done: any) => {
      let totalCount: number = 0;
      let notes = done[0];
      let schoolNotes = done[1];
      let careerClusterNotes = done[2];

      if (notes && notes.length > 0) {
        totalCount += notes.length;
      }

      if (schoolNotes && schoolNotes.length > 0) {
        totalCount += schoolNotes.length;
      }

      if (careerClusterNotes && careerClusterNotes.length > 0) {
        totalCount += careerClusterNotes.length;
      }

      this.hasNotes = (totalCount > 0) ? true : false;

    })

  }

  openManualInput() {
    this._matDialog.open(FindYourInterestManualScoreInputComponent, {
      maxWidth: '800px'
    });
  }

  guidedTour() {
    this._matDialog.open(GuidedTipsConfigDialogComponent, {
      maxWidth: 500,
    })
  }

  ngOnDestroy() {

    if (this.userUpdatesSubscription) {
      this.userUpdatesSubscription.unsubscribe();
    }
  }

  showNotification() {

    this._matDialog.open(StudentNotificationDialogComponent, {
      maxWidth: "800px",
      autoFocus: false
    });

  }

  showNotes() {
    this._matDialog.open(NotesDialogComponent, {
      data: {
        // title: title,
        // socId: socId
      },
      maxWidth: '900px',
    });
  }

}
