import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UserService } from 'app/services/user.service';
import { LoginService } from 'app/services/login.service';
import { UtilityService } from 'app/services/utility.service';
import $ from 'jquery';
import { RegisterDialogComponent } from 'app/core/dialogs/register-dialog/register-dialog.component';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { ConfigService } from 'app/services/config.service';
declare var $: $;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

    password = '';
    errorMessage = '';

    email;
    accessCode;
    resetPasswordEmail;
    menuOpen;

    //TODO:
    showEmailLogin = true;
    showPasswordReset = false;
    showAccessCodeLogin = false;

    message; // TODO:
    dropdownMobileMenu = false;
    isAtHomepage = this._router.url === '/learn-about-yourself';
    status = {
        isopen: this.isAtHomepage ? true : false
    };
    // if the status is open and if the user is not logged in, open the login panel, else hide it
    toggleLogin = this.status.isopen && !this._userService.getUser() ? "block" : "none";

    constructor(
        private _userService: UserService,
        public _loginService: LoginService,
        private _router: Router,
        private _dialog: MatDialog,
        private _utility: UtilityService,
        private _config: ConfigService,
    ) {
    }

    setLeftNavCollapsed(collapsed) {
        this._userService.setLeftNavCollapsed(collapsed);

        if (this._router.url === '/learn-about-yourself') {
            this._utility.redirectTo('/learn-about-yourself');
        } else {
            this._router.navigate(['/learn-about-yourself']);
        }
    }

    /* on click for the "Where do I get my access code?" */
    goToGeneralHelp() {
        window.open('general-help', '_self', 'resizable,location,menubar,toolbar,scrollbars,status');
    }

    ngOnInit() {
        var HeaderHeight = $('#header').outerHeight();
        var BannerHeight = $('#banner').outerHeight();
        var offset = ($('.logged-out').length === 1) ? (HeaderHeight + BannerHeight) : 10;

        $(window).scroll(function () {
            if ($(window).outerWidth() > 767) {
                if ($(window).scrollTop() > offset) {
                    $('#header').addClass('scrolled');
                    HeaderHeight = $('#header').height();
                    $('#header').parent().parent().css('padding-top', HeaderHeight);
                    $('#banner').addClass('banner_sticky').css('top', HeaderHeight);

                    $('#main').css('padding-top', BannerHeight);
                } else {
                    $('#header').removeClass('scrolled');
                    $('#header').parent().parent().css('padding-top', '');
                    $('#banner').removeClass('banner_sticky').css('top', '');
                    $('#main').css('padding-top', '');
                    if ($('.scroll-to-fixed-fixed').length > 0) {
                        $('.top-blue-panel, #sidebar.not-logged-in, #sidebar.logged-in').removeClass('scroll-to-fixed-fixed').removeAttr('style');
                        $('.top-blue-panel+div, #sidebar.not-logged-in+div, #sidebar.logged-in+div').hide();
                    }
                }
            }
        }).scroll();

        $(window).on("resize", function () {
            $(window).trigger('scroll');
        });
    }

    ngAfterViewInit() {


        $('.dropdown.menu > li > a').click(function () {
            const li = $(this).closest('li');
            if (li.hasClass('open')) {
                li.removeClass('open');
            } else {
                $('.dropdown.menu > li').removeClass('open');
                li.addClass('open');
            }
            return false;
        });

    }


    /**
     * Toggles the login dropdown panel
     */
    toggleDropdown($event) {
        // only on the button to toggle the login
        // if ( $event.target.id === 'header-login' && !this.isAtHomepage ) {
        if ($event.target.id === 'header-login' && !this.isLoggedIn()) {
            // !$rootScope.globals.currentUser && TODO: add back when cookies globals added
            this.status.isopen = !this.status.isopen;
            this.toggleLogin = (this.status.isopen ? 'block' : 'none');
        }
    };


    toggleMobileDropdowMenu() {
        this.dropdownMobileMenu = !this.dropdownMobileMenu;
    }

    toggleService = {
        'AccessCode': 1,
        'Email': 2,
        'Reset': 3
    }

    /**
     * Clears all properties based on the service that has been selected
     */
    clearProperties(service) {
        switch (service) {
            case this.toggleService.AccessCode: // clears email/password reset properties
                this.email = this.password = this.resetPasswordEmail = '';
                break;
            case this.toggleService.Email: // clears access/password reset properties
                this.accessCode = this.resetPasswordEmail = '';
                break;
            case this.toggleService.Reset: // clears access/email login properties
                this.accessCode = this.email = this.password = '';
                break;
            default: // clears all properties
                this.email = this.password = this.accessCode = this.resetPasswordEmail = '';
                break;
        }
    }


    /**
     * Toggles views
     */
    toggleAccessCodeLogin() {
        this.showAccessCodeLogin = true;
        this.showEmailLogin = this.showPasswordReset = false;
        this.clearProperties(this.toggleService.AccessCode);
    };

    toggleEmailLogin() {
        this.showEmailLogin = true;
        this.showAccessCodeLogin = this.showPasswordReset = false;
        this.clearProperties(this.toggleService.Email);
    };

    togglePasswordReset() {
        this.showPasswordReset = true;
        this.showAccessCodeLogin = this.showEmailLogin = false;
        this.clearProperties(this.toggleService.Reset);
    };

    // TODO:
    isLoggedIn() {
        return this._userService.isLoggedIn();
    }

    logout() {
        this._userService.logOff();
    }

    openAccountRegistratinoPopUp() {
        this._dialog.open(RegisterDialogComponent, {
            hasBackdrop: true,
            panelClass: 'custom-dialog-container',
            height: '95%',
            autoFocus: true,
            disableClose: true
        });
    }

    /**
     * Redirect users to the dashboard
     */
    goToDashboard = function () {
        this._router.navigate(['/mentor-dashboard']);
    }

    goToLearn = function () {
        this._router.navigate(['/learn-about-yourself']);
    }

    async emailLogin() {
        this._loginService.email = this.email;
        this._loginService.password = this.password;

        //wait for the response from the server
        const loginAttempt = await this._loginService.emailLogin();

        //if success; redirect the user to /learn-about-yourself

        if (loginAttempt == "SUCCESS") {
            this._loginService.redirectAfterLogin();
            // window.location.href = this._config.getSSOUrl();
        } else {
            this.message = loginAttempt;
            setTimeout(() => { this.message = ''; }, 5000);
        }

    }

    showLoginModal() {
        this._dialog.open(LoginDialogComponent, {
            hasBackdrop: true,
            panelClass: 'new-modal',
            width: '300px',
            autoFocus: true,
            disableClose: true
        });
    }

    async submitAccessCodeLogin() {

        const accessCodeLogin = await this._loginService.accessCodeLogin(this.accessCode, true);

        if (accessCodeLogin) {

            //teacher code
            if (accessCodeLogin == "TEACHER_CODE") {
                // show the register modal and pass in the access code
                this._dialog.open(RegisterDialogComponent, {
                    data: { accessCode: this.accessCode.toUpperCase() },
                    hasBackdrop: true,
                    panelClass: 'custom-dialog-container',
                    height: '95%',
                    autoFocus: true,
                    disableClose: true
                });
            } else if (accessCodeLogin == "ACCESS_CODE") {

                // NOTE: SSO login to the CTM website will not happen with this login type because 
                // switches to Email/Password login if linked

                this._loginService.redirectAfterLogin();
                // window.location.href = this._config.getSSOUrl();
            } else {
                //if we got here then an error msg was returned
                this.message = accessCodeLogin;
                setTimeout(() => { this.message = ''; }, 5000);
            }

        }
    }

    async sendResetCode() {
        this._loginService.resetPasswordEmail = this.resetPasswordEmail;
        const resetCodeAttempt = await this._loginService.sendResetCode();

        if (resetCodeAttempt === "SUCCESS") {
            this.message = 'Email with Reset key has been sent.';
            setTimeout(() => { this.message = ''; }, 5000);
        } else {
            this.message = resetCodeAttempt;
            setTimeout(() => { this.message = ''; }, 5000);
        }
    }
}
