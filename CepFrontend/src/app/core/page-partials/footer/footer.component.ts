import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    version;
    lastUpdated;
    path;

    constructor(
        private _dialog: MatDialog,
        private _config: ConfigService,
        private _userService: UserService,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _router: Router,
    ) {
        this.path = this._router.url;
    }

    ngOnInit() {
        this.version = this._config.getVersion();
        this.lastUpdated = this._config.getVersionDate();

        if (this.path.includes('bringToSchool=submitted')) {
            const data = {
                'title': 'Schedule Notification',
                'message': 'Your information was submitted successfully.'
              };
              const dialogRef = this._dialog.open(MessageDialogComponent, {
                data: data,
                maxWidth: '300px'
              });

              dialogRef.afterClosed().subscribe(result => {
                window.location.href = this.path.split('?')[0];
              })
        }
    }

    onBringAsvabToYourSchool() {
        const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
            hasBackdrop: true,
            panelClass: 'new-modal',
            // height: '735px',
            autoFocus: false,
            disableClose: true
        });

    }

    isLoggedIn() {
        return this._userService.isLoggedIn();
    }

    snapChatModal() {
        this._googleAnalyticsService.trackClick('#CONNECT_WITH_US_SNAPCHAT')

        var data = {
            title: 'Connect with us on SnapChat!',
            message: '<p class="text-center"><img style="width: 400px;" src="assets/images/option-ready-snap.png" alt=""/></p>'
        };

        const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: data,
            maxWidth: '500px'
        });
    }
}
