import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from '../../dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-career-cluster-add-favorites',
  templateUrl: './career-cluster-add-favorites.component.html',
  styleUrls: ['./career-cluster-add-favorites.component.scss']
})
export class CareerClusterAddFavoritesComponent implements OnInit {

  @Input() parent;

  constructor(
    private _user: UserService,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  showModal(message) {

    const data = {
      'title': 'Favorites',
      'message': message
    };
    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '600px'
    });
  }

  addFavorite() {

    let that = this;
    // get the current favorites list
    var favoritePromise = this._user.getCareerClusterFavoritesList();
    favoritePromise.then(function (response: any) {

      var len = response.length;

      // check to see if career cluster was already added because we don't want duplicates
      for (var i = 0; i < len; i++) {
        if (response[i].ccId == that.parent.ccId) {

          // inform user that the career cluster was already added to the list
          that.showModal('<b>' + that.parent.careerCluster.ccTitle + '</b>' + ' was already added to the list. Select another career cluster to add.')
          return false;
        }
      }

      // limit 2 career clusters can be added
      if (len >= 2) {
        that.showModal('Your favorite career cluster list is full. that career cluster was not added.');

        return false;
      }

      // save the career cluster id to the database if not yet saved
      var promise = that._user.insertFavoriteCareerCluster(that.parent.ccId, that.parent.careerCluster.ccTitle);
      promise.then(function (response) {
        that.showModal('<b>' + that.parent.careerCluster.ccTitle + '</b> is added to your favorites.')


      }, function (reason) {

        //inform user that there was an error and to try again.
        console.error(reason);
        var modalOptions = {
          'title': 'Error',
          'message': 'There was an error proccessing your request. Please try again.',
        };

        that._dialog.open(MessageDialogComponent, {
          data: modalOptions,
          maxWidth: '400px'
        })

        return false;
      });
    }, function (reason) {
      console.error(reason);

      var modalOptions = {
        'title': 'Error',
        'message': 'There was an error proccessing your request. Please try again.',
      };

      that._dialog.open(MessageDialogComponent, {
        data: modalOptions,
        maxWidth: '400px'
      })

      return false;
    });
  }
}
