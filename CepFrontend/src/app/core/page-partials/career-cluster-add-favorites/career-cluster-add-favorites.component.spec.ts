import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterAddFavoritesComponent } from './career-cluster-add-favorites.component';

describe('CareerClusterAddFavoritesComponent', () => {
  let component: CareerClusterAddFavoritesComponent;
  let fixture: ComponentFixture<CareerClusterAddFavoritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterAddFavoritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterAddFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
