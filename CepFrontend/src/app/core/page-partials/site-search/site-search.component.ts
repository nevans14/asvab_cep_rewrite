import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import $ from 'jquery';
declare var $: $;

@Component({
  selector: 'app-site-search',
  templateUrl: './site-search.component.html',
  styleUrls: ['./site-search.component.scss']
})
export class SiteSearchComponent implements OnInit, AfterViewInit {
  searchString = undefined;
  gcsesearch;
  constructor(private _router: Router, private _sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.gcsesearch = this._sanitizer.bypassSecurityTrustHtml(
      '<form id="search" class="mini-form"><gcse:searchbox-only resultsUrl="search-result" queryParameterName="q"></gcse:searchbox-only></form>'
    );

    // const cx = '016306542328905981446:wkbq-87up34';
    // const gcse = document.createElement('script');
    // gcse.type = 'text/javascript';
    // gcse.async = true;
    // gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    // let s = document.getElementsByTagName('script')[0];
    // s.parentNode.insertBefore(gcse, s);
  }

  search() {
    // this.bootstrapGcseScript();

    // store search parameter into session storage
    if (this.searchString) {
      this._router.navigate(['/search-result/keyword/' + this.searchString]);
    } else {
      // do nothing
    }
  }

  ngAfterViewInit() {
    this.bootstrapGcseScript();
  }

  bootstrapGcseScript() {
    const cx = '016306542328905981446:wkbq-87up34';
    const gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    let s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
    setTimeout(function() {
      $(document)
        .find(':input[id^=gsc-i-id]')
    }, 200);
  }
}
