import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationV2MobileComponent } from './left-navigation-v2-mobile.component';

describe('LeftNavigationV2MobileComponent', () => {
  let component: LeftNavigationV2MobileComponent;
  let fixture: ComponentFixture<LeftNavigationV2MobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationV2MobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationV2MobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
