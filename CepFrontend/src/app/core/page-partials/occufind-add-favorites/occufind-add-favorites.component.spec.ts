import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccufindAddFavoritesComponent } from './occufind-add-favorites.component';

describe('OccufindAddFavoritesComponent', () => {
  let component: OccufindAddFavoritesComponent;
  let fixture: ComponentFixture<OccufindAddFavoritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccufindAddFavoritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccufindAddFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
