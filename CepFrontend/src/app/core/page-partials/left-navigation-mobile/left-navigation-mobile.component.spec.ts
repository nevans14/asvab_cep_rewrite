import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationMobileComponent } from './left-navigation-mobile.component';

describe('LeftNavigationComponent', () => {
  let component: LeftNavigationMobileComponent;
  let fixture: ComponentFixture<LeftNavigationMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
