import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FindYourInterestManualScoreInputComponent } from '../../dialogs/find-your-interest-manual-score-input/find-your-interest-manual-score-input.component';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { UserService } from 'app/services/user.service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { Router } from '@angular/router';

@Component( {
    selector: 'app-left-navigation-mobile',
    templateUrl: './left-navigation-mobile.component.html',
    styleUrls: ['./left-navigation-mobile.component.scss']
} )
export class LeftNavigationMobileComponent implements OnInit {

    constructor( private dialog: MatDialog,
        private _FYIScoreService: FindYourInterestService,
        private _user: UserService,
        private _router: Router ) { }


    openDialogFYIManualInput() {
        let dialogRef = this.dialog.open( FindYourInterestManualScoreInputComponent, {
        } );
    }

    numberTestTaken: any;
    showScoresTable = false;
    userRole: any;
    verbalScore: any;
    mathScore: any;
    scienceScore: any;
    afqtScore: any;
    haveManualAsvabTestScores: any;
    vBooks: any;
    mBooks: any;
    sBooks: any;
    loadingIndicator = false;

    interestCodeOne: any;
    interestCodeTwo: any;
    interestCodeThree: any;
    scoreChoice: any;
    ngOnInit() {

        //TODO: hook up notes when notes implementation is complete.
        /*NotesService.getNotes().then(function(response) {
        $scope.noteList = response;
        });*/

        this._FYIScoreService.getNumberOfTestTaken().then( response => {
            this.numberTestTaken = response;
        } );

        this.userRole = this._user.getUser().currentUser.role;

        // Retrieve scores from session.
        this.verbalScore = window.sessionStorage.getItem( 'sV' );
        this.mathScore = window.sessionStorage.getItem( 'sM' );
        this.scienceScore = window.sessionStorage.getItem( 'sS' );
        this.afqtScore = window.sessionStorage.getItem( 'sA' );
        this.haveManualAsvabTestScores = window.sessionStorage.getItem( 'manualScores' ) == "true";

        // Retrieve book objects from session.
        this.vBooks = JSON.parse( window.sessionStorage.getItem( 'vBooks' ) );
        this.mBooks = JSON.parse( window.sessionStorage.getItem( 'mBooks' ) );
        this.sBooks = JSON.parse( window.sessionStorage.getItem( 'sBooks' ) );


        // set user interest codes
        this._user.getUserInterestCodes().then(( response: any ) => {
            this.interestCodeOne = response.interestCodeOne;
            this.interestCodeTwo = response.interestCodeTwo;
            this.interestCodeThree = response.interestCodeThree;
            this.scoreChoice = response.scoreChoice;
        } );
    }

    //if ( typeof $rootScope.globals.currentUser === 'undefined') return;

    toggleScoreView = function() {
        this.showScoresTable = this.showScoresTable === false;
    }

    setUserInterestCodes() {
        // set user interest codes
        this._user.getUserInterestCodes().then(( response: any ) => {
            this.interestCodeOne = response.interestCodeOne;
            this.interestCodeTwo = response.interestCodeTwo;
            this.interestCodeThree = response.interestCodeThree;
            this.scoreChoice = response.scoreChoice;
        } );
    }


    /**
     * Get scores for display
     */
    getScore = function( area ) {
        var score = 0;
        switch ( area ) {
            case "verbal":
                score = this.verbalScore;
                break;
            case "math":
                score = this.mathScore;
                break;
            case "science":
                score = this.scienceScore;
                break;
            case "afqt":
                score = this.afqtScore;
                break;
            default:
                break;
        }
        return score;
    }

    /**
     * Get book sprite x pos
     */
    getBookPos = function( area ) {
        // return bsackground style position object
        switch ( area ) {
            case "verbal":
                return { "background-position": this.vBooks.xPosSmall + "px 0px" };
            case "math":
                return { "background-position": this.mBooks.xPosSmall + "px 0px" };
            case "science":
                return { "background-position": this.sBooks.xPosSmall + "px 0px" };
        }
    }

    isShowTest = function() {
        if ( this._user.getUser() != undefined )
            var userRole: any = this._user.getUser().currentUser.role;
        else
            var userRole: any = 'S';

        // student role can only
        if ( userRole != 'A' ) {
            if ( this.numberTestTaken < 2 ) {
                return true;
            }
            return false;
        }
        return true;
    }


    /**
     * Route user to portfolio page.
     */
    routeToPortfolio = function() {

        //TODO: dependency on portfoliio page
        /*var promise = PortfolioService.getIsPortfolioStarted();
        promise.then(function(success) {

            // if portfolio exists then skip instructions
            if (success > 0) {
                $location.path('/portfolio');
            } else {
                $location.path('/portfolio-directions');
            }
        }, function(error) {
            console.log(error);
        });*/

    }



    /*$rootScope.$on("updateInterestCodes", function(){
        $scope.loadingIndicator = true;
        UserFactory.getUserInterestCodes().then(function(response) {
            $scope.interestCodeOne = response.interestCodeOne;
            $scope.interestCodeTwo = response.interestCodeTwo;
            $scope.interestCodeThree = response.interestCodeThree;
            $scope.scoreChoice = response.scoreChoice;
            $scope.loadingIndicator = false;
        }, function(error){
            $scope.loadingIndicator = false;
        });
     });*/

    resultsInfo = function() {
        const data = {
            'title': 'Career Exploration Scores',
            'message': '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating. Your top strength will have the most books.</p>' +
            '<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal, Math, Science/Technical skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' +
            '<p><b>Verbal Skills</b> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) test.</p>' +
            '<p><b>Math Skills</b> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' +
            '<p><b>Science/Technical Skills</b> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' +
            '<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    interestCodeInfo = function( interestCode ) {
        var data;
        switch ( interestCode ) {
            case "R":
                data = {
                    'title': 'Realistic',
                    'message': '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
                    '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
                    '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>'
                };
                break;
            case "I":
                data = {
                    'title': 'Investigative',
                    'message': '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
                    '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
                    '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>'
                };
                break;
            case "A":
                data = {
                    'title': 'Artistic',
                    'message': '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
                    '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
                    '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>'
                };
                break;
            case "S":
                data = {
                    'title': 'Social',
                    'message': '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
                    '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
                    '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>'
                };
                break;
            case "E":
                data = {
                    'title': 'Enterprising',
                    'message': '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
                    '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
                    '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>'
                };
                break;
            case "C":
                data = {
                    'title': 'Conventional',
                    'message': '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
                    '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
                    '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'
                };
                break;
        }


        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    interestCodesInfo = function() {

        var score = this.scoreChoice == 'gender' ? 'Gender-Specific' : 'Combined';
        var data = {
            title: 'Interest Codes',
            message: "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
            /*bodyText : '<p><strong style="color: #2279BF">Realistic</strong></p>' +
            '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
            '<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
            '<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>' +
            '<p><strong style="color: #892C32">Investigative</strong></p>' +
            '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
            '<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
            '<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>' +
            '<p><strong style="color: #F3913D">Artistic</strong></p>' +
            '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
            '<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
            '<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>' +
            '<p><strong style="color: #174377">Social</strong></p>' +
            '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
            '<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
            '<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>' +
            '<p><strong style="color: #ea292d">Enterprising</strong></p>' +
            '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
            '<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
            '<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>' +
            '<p><strong style="color: #52843c">Conventional</strong></p>' +
            '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
            '<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
            '<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'*/
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    showSortByStrength = function() {
        var data = {
            title: 'Career Exploration Scores',
            message: '<p>Your Career Exploration Scores relate to the three skill areas: math, verbal, science &amp; technical.</p>' +
            '<p>Choose the skill you want to investigate. Consider whether your corresponding Career Exploration Score is above average, average, or below average.</p>' +
            '<p>You\'ll see the occupations organized in three levels of importance: Most Important, Moderately Important, and Less Important. Explore the importance level that matches your score level.</p>' +
            '<p>(For example, is your math skills score above average? Sort by math and explore occupations in the Most Important category. If your verbal skills score is below average, sort by verbal and explore the occupations in the less important category).</p>'
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    /**
     * 
     * Popup modal used on Occu-find pages.
     * 
     */
    careerDefinitionsNoCareerCluster = function() {
        var data = {
            title: 'Categories',
            message: "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
            "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
            "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
            "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
            "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    careerDefinitions = function() {
        var data = {
            title: '',
            message: "<p><b>Available in the Military</b> indicates occupations that are offered in one of more of the Military Services, each with its own unique requirements.</p>" +
            "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
            "<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
            "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
            "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
            "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
            "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    stemDefinition = function() {
        var data = {
            title: '',
            message: "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
            "<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    brightDefinition = function() {
        var data = {
            title: '',
            message: "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
            "<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    greenDefinition = function() {
        var data = {
            title: '',
            message: "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
            "<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
        };

        const dialogRef1 = this.dialog.open( MessageDialogComponent, {
            data: data
            //maxWidth: '400px'
        } );
    }

    returnResultsDisabled = function() {
        return this._router.url === '/my-asvab-summary-results';
    };

    returnManualResultsDisabled = function() {
        return this._router.url === '/my-asvab-summary-results-manual';
    };

    occufindDisabled = function() {
        return this._router.url === '/occufind-occupation-search';
    };

    favoritesDisabled = function() {
        return this._router.url === '/favorites';
    }

    returnFyiDisabled = function() {
        return this._router.url === '/find-your-interest-score';
    };

    takeFyiDisabled = function() {
        return this._router.url === '/find-your-interest';
    };

    portfolioDisabled = function() {
        return this._router.url === '/portfolio-directions';
    };
}
