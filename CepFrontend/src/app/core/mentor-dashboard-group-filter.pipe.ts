import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mentorDashboardGroupFilter',
  pure: false
})
export class MentorDashboardGroupFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (!value) return null;

    if (!args) return value;

    let filteredItems = [];
    if (args) {

      let items = <Array<any>>value;

      items.forEach((item: any) => {
        if (args.groups && args.groups.length > 0) {
          if (args.groups.filter(x => x == item.groupName).length > 0) {
            filteredItems.push(item);
          }else{
            return;
          }
        }else{
          filteredItems.push(item);
        }
      })
    }

    return filteredItems;
  }

}
