import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from 'app/services/user.service';

/**
 * Redirects the user to the home page if not logged in; 
 * The home page will show a login modal when redirected.
 */
@Injectable({
  providedIn: 'root'
})
export class LoginRedirectService implements CanActivate {

  constructor(private userService: UserService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate([''], {
        queryParams: {
          redirect: 'login' 
        }
      });
      return false;
    }
  }

}