import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-military-academy-add-dialog',
  templateUrl: './military-academy-add-dialog.component.html',
  styleUrls: ['./military-academy-add-dialog.component.scss']
})
export class MilitaryAcademyAddDialogComponent implements OnInit {

  private selectedSchools: any[];
  private filteredSchoolProfiles: any[];
  private schoolProfiles: any[];
  private allMilitaryAcademies: any[];
  public occupationDescription: string;

  // filter
  private stateFilter: any;
  private sort: any;

  // pagination
  public totalItems: number;
  public currentPage: number;
  public maxSize: number;
  public itemsPerPage: number;

  constructor(public _dialogRef: MatDialogRef<MilitaryAcademyAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.selectedSchools = data.selectedSchools;
    this.allMilitaryAcademies = data.allMilitaryAcademies;
    this.occupationDescription = data.occupationDescription;

  }

  ngOnInit() {
    //retrieve schools and init pagination
    this.getSchools();
  }

  getSchools() {
    let self = this;
    //only allow selection of academies that have not already been selected
    this.schoolProfiles = this.allMilitaryAcademies.filter(function (school: any) {
      if (!self.selectedSchools.find(x => x.unitId == school.unitId && x.collegeTypeId == school.collegeTypeId)) {
        return school;
      }
    });
    this.initPagination(this.schoolProfiles);
  }

  initPagination(schoolProfiles: any[]) {
    this.totalItems = schoolProfiles.length;
    this.filteredSchoolProfiles = schoolProfiles;
    this.currentPage = 1;
    this.maxSize = 4;
    this.itemsPerPage = 5;
    this.stateFilter = "";
    this.sort = "schoolName";
  }

  onSortChange(event) {
    const value = event.target.value;
    this.sort = value;

    this.filteredSchoolProfiles = this.applySort(this.filteredSchoolProfiles, value);
  }

  applyStateFilter(schoolDetails, value): any[] {
    if ((!value) || (value === '')) {
      return schoolDetails;
    } else {
      return schoolDetails.filter(school => school.state === value);
    }
  }

  applySort(schoolDetails, value): any[] {

    if (value.charAt(0) === '-') {
      schoolDetails.sort(this.compareValues(value.slice(1), 'desc'));
    } else {
      schoolDetails.sort(this.compareValues(value));
    }

    return schoolDetails;
  }

  /**
   * returns a sorted array based on the key and order passed
   * @param key 
   * @param order can be 'asc' or 'desc'; 'asc' is by default
   */
  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.schoolProfile.hasOwnProperty(key) || !b.schoolProfile.hasOwnProperty(key)) return 0;  //property doesn't exist

      const varA = (typeof a.schoolProfile[key] === 'string')
        ? a.schoolProfile[key].toUpperCase() : a.schoolProfile[key];
      const varB = (typeof b.schoolProfile[key] === 'string')
        ? b.schoolProfile[key].toUpperCase() : b.schoolProfile[key];

      let comparison = 0;
      if ((typeof varA === 'string') && (typeof varB === 'string')) {
        comparison = varA.localeCompare(varB);
      } else {
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
      }

      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  selectSchool(school) {
    this._dialogRef.close(school);
  }

}
