import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilitaryAcademyAddDialogComponent } from './military-academy-add-dialog.component';

describe('MilitaryAcademyAddDialogComponent', () => {
  let component: MilitaryAcademyAddDialogComponent;
  let fixture: ComponentFixture<MilitaryAcademyAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilitaryAcademyAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilitaryAcademyAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
