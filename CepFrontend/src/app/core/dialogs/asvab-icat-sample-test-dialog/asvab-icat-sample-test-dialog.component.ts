import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-asvab-icat-sample-test-dialog',
  templateUrl: './asvab-icat-sample-test-dialog.component.html',
  styleUrls: ['./asvab-icat-sample-test-dialog.component.scss']
})
export class AsvabIcatSampleTestDialogComponent implements OnInit {

  public answers = [{
      answer : 'a',
      selection: undefined,
      message: undefined
    }, {
      answer : 'c',
      selection: undefined,
      message: undefined
    }, {
      answer : 'c',
      selection: undefined,
      message: undefined
    }, {
      answer : 'c',
      selection: undefined,
      message: undefined
    }, {
      answer : 'd',
      selection: undefined,
      message: undefined
    }, {
      answer : 'd',
      selection: undefined,
      message: undefined
    }, {
      answer : 'b',
      selection: undefined,
      message: undefined
    }, {
      answer : 'b',
      selection: undefined,
      message: undefined
    }, {
      answer : 'a',
      selection: undefined,
      message: undefined
    }, {
      answer : 'd',
      selection: undefined,
      message: undefined
    }, {
      answer : 'd',
      selection: undefined,
      message: undefined
    }, {
      answer : 'c',
      selection: undefined,
      message: undefined
  }];

  constructor(
    private _googleAnalyticsService: GoogleAnalyticsService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  checkAnswers() {
    for (let i = 0; i < this.answers.length; i++) {
      if (this.answers[i].selection !== undefined && this.answers[i].answer === this.answers[i].selection) {
        this.answers[i].message = this.sanitizer.bypassSecurityTrustHtml('<span style="color: green">Correct!</span>');
      } else if (this.answers[i].selection !== undefined && this.answers[i].answer !== this.answers[i].selection) {
        this.answers[i].message = this.sanitizer.bypassSecurityTrustHtml('<span style="color: red">Incorrect: The correct answer is ' + this.answers[i].answer + '!</span>');
      } else {
        this.answers[i].message = this.sanitizer.bypassSecurityTrustHtml('<span style="color: orange">No answer selected.</span>');
      }
    }

  }

  trackGoogleAnalytics() {
    this._googleAnalyticsService.trackSampleTestClick('#ASVAB_CA_SAMPLE_TEST_ITEMS_CHECKED');
  }

}
