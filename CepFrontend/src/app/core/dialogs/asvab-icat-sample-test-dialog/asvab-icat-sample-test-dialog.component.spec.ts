import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsvabIcatSampleTestDialogComponent } from './asvab-icat-sample-test-dialog.component';

describe('AsvabIcatSampleTestDialogComponent', () => {
  let component: AsvabIcatSampleTestDialogComponent;
  let fixture: ComponentFixture<AsvabIcatSampleTestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsvabIcatSampleTestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsvabIcatSampleTestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
