import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomActivitiesDialogComponent } from './classroom-activities-dialog.component';

describe('ClassroomActivitiesDialogComponent', () => {
  let component: ClassroomActivitiesDialogComponent;
  let fixture: ComponentFixture<ClassroomActivitiesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomActivitiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomActivitiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
