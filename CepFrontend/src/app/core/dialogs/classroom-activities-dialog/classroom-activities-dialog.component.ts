import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { UserService } from 'app/services/user.service';
import { MentorService } from 'app/services/mentor.service';
import { UtilityService } from 'app/services/utility.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

function validateEmails(emails: string) {
	return (emails.split(',')
		.map(email => Validators.email(<AbstractControl>{ value: email }))
		.find(_ => _ !== null) === undefined);
}

function emailsValidator(control: AbstractControl): ValidationErrors | null {
	if (control.value === '' || !control.value || validateEmails(control.value)) {
		return null
	}
	return { emails: true };
}

@Component({
	selector: 'app-classroom-activities-dialog',
	templateUrl: './classroom-activities-dialog.component.html',
	styleUrls: ['./classroom-activities-dialog.component.scss']
})
export class ClassroomActivitiesDialogComponent implements OnInit {


	mentors: any = {};
	selectedActivity;
	studentActivity;
	isNotStudent;
	teacherGuide;
	isStudentView;
	resourceBaseUrl;
	mentorsList;
	file: File = null;
	newSubmissions = [];
	public mentorInviteForm: FormGroup;
	public mentorInviteFormSubmitted: boolean;
	public errorText: string;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private _config: ConfigService,
		private _classroomActivityService: ClassroomActivityService,
		public _dialogRef: MatDialogRef<ClassroomActivitiesDialogComponent>,
		private _userService: UserService,
		private _formBuilder: FormBuilder,
		private _mentorService: MentorService,
		private _utilityService: UtilityService,
		private _matDialog: MatDialog,
	) {
	}

	ngOnInit() {

		this.selectedActivity = this.data.activity;
		this.isNotStudent = this.data.isNotStudent;
		this.mentorsList = this.data.mentors;
		this.mentorInviteForm = this._formBuilder.group({
			inviteEmails: ['',
				Validators.compose([Validators.required, emailsValidator])
			]
		});
		this.setupMentorCheckbox();
		this.isStudentView = !this.isNotStudent ? true : false;
		this.resourceBaseUrl = this._config.getImageUrl();

		console.log('this.isNotStudent', this.isNotStudent);
		this.studentActivity = this.selectedActivity.pdfs.length > 0
			? this.resourceBaseUrl + 'CEP_PDF_Contents/' + this.selectedActivity.pdfs.filter(function (x) { return x.role === 'S' })[0].s3Filename
			: '';

		if (this.isNotStudent) {
			this.teacherGuide = this.selectedActivity.pdfs.length >= 2
				? this.resourceBaseUrl + 'CEP_PDF_Contents/' + this.selectedActivity.pdfs.filter(function (x) { return x.role !== 'S' })[0].s3Filename
				: '';
		}
		this.errorText = "";
		this.mentorInviteFormSubmitted = false;
	}

	submitMentorInvite() {

		this.mentorInviteFormSubmitted = true;

		let formData: any = {
			mentorEmail: this.mentorInviteForm.value.inviteEmails
		}

		this._mentorService.save(formData).then((response: any) => {
			this.mentorInviteFormSubmitted = false;
			this.close();
			this.mentorInviteForm.reset();
		}).catch((error: any) => {
			this.mentorInviteFormSubmitted = false;
			this.errorText = error;
			setTimeout(() => { this.errorText = ""; }, 5000);
			console.error("ERROR", error);
		})
	}

	showActivity = function (activity) {
		this._userService.setCompletion(UserService.DOWNLOAD_ACTIVITY, 'true');

		alert('Download the PDF to complete the activity.');
		var win = window.open(activity, '_blank');
		win.focus();
	}

	handleFileInput(files: FileList) {
		for (let i = 0; i < this.newSubmissions.length; i++) {
			if (files.item(0).name == this.newSubmissions[i].file.name) {
				let r = confirm("The filename exists, do you want to replace? Press OK to replace. Please make sure to resubmit to save. ");
				if (r == true) {
					this.newSubmissions[i].file = files.item(0);
					return;
				} else {
					return;
				}

			}
		}

		for (let i = 0; i < this.selectedActivity.submissions.length; i++) {
			if (files.item(0).name == this.selectedActivity.submissions[i].fileName) {
				if (this.selectedActivity.submissions[i].statusId == 2) {
					alert("The filename exists. Please unsubmit before overwriting it.")
					return;
				}
				let r = confirm("The filename exists, do you want to replace? Press OK to replace. Please make sure to resubmit to save. ");
				if (r == true) {
					this.selectedActivity.submissions[i].file = files.item(0);
					return;
				} else {
					return;
				}

			}
		}

		this.newSubmissions.unshift({ file: files.item(0), mentors: new FormControl() })
	}

	submitForm(mentorForm, newSubIndex) {

		this._userService.setCompletion(UserService.ADDED_CLASSROOM_ACTIVITY, 'true');

		this.newSubmissions[newSubIndex].submitDisabled = true;
		let mentors = [];

		for (let i = 0; i < this.newSubmissions[newSubIndex].mentors.value.length; i++) {
			mentors.push({ "mentorId": this.newSubmissions[newSubIndex].mentors.value[i].id });
		}

		let model = { mentors: mentors };
		let formData: FormData = new FormData();
		formData.append('file', this.newSubmissions[newSubIndex].file, this.newSubmissions[newSubIndex].file.name);
		formData.append('model', new Blob([JSON.stringify(model)],
			{
				type: "application/json"
			}));

		let classroomSubmission = this._classroomActivityService.saveWork(this.selectedActivity.id, formData);
		classroomSubmission.then((results: any) => {
			results.mentorsFormData = new FormControl();

			let mentors: any[] = [];
			for (let k = 0; k < results.mentors.length; k++) {
				for (let f = 0; f < this.mentorsList.length; f++) {
					if (results.mentors[k].mentorId == this.mentorsList[f].id) {
						mentors.push(this.mentorsList[f]);
					}
				}
			}
			results.mentorsFormData.setValue(mentors);
			this.selectedActivity.submissions.push(results);
			this.newSubmissions.splice(newSubIndex, 1);
		}, (error) => {
			this.newSubmissions[newSubIndex].submitDisabled = false;
			console.error(error);
		});

	}

	deleteSubmission(submission) {
		let dialogData = {
			title: 'Confirm Submission Removal',
			message: 'Remove Submission, are you sure?'
		};

		this._matDialog.open(ConfirmDialogComponent, {
			data: dialogData,
			maxWidth: '300px'
		}).beforeClosed().subscribe(response => {
			if (response) {
				if (response === 'ok') {
					let deleteSubmission = this._classroomActivityService.deleteFile(this.selectedActivity.id, submission.id);

					deleteSubmission.then((results) => {
						var index = this.selectedActivity.submissions.map(x => {
							return x.id;
						}).indexOf(submission.id);

						this.selectedActivity.submissions.splice(index, 1);
					}, (error) => {
						console.error(error);
					})
				}
			}
		});

	}

	deleteNewSubmission(newSubIndex) {

		let dialogData = {
			title: 'Confirm Submission Removal',
			message: 'Remove Submission, are you sure?'
		};

		this._matDialog.open(ConfirmDialogComponent, {
			data: dialogData,
			maxWidth: '300px'
		}).beforeClosed().subscribe(response => {
			if (response) {
				if (response === 'ok') {
					this.newSubmissions.splice(newSubIndex, 1);
				}
			}
		});

	}

	unsubmitSubmission(submission) {

		const formData: FormData = new FormData();

		let unsubmitSubmission = this._classroomActivityService.unsubmitWork(this.selectedActivity.id, submission.id, formData);

		unsubmitSubmission.then((results) => {
			var index = this.selectedActivity.submissions.map(x => {
				return x.id;
			}).indexOf(submission.id);

			this.selectedActivity.submissions[index].statusId = 3;
		}, (error) => {
			console.error(error);
		})
	}

	resubmitSubmission(resubmitForm, submissionId) {

		let mentors = [];
		let submissions = this.selectedActivity.submissions;
		let index = submissions.map(x => {
			return x.id;
		}).indexOf(submissionId);

		submissions[index].submitDisabled = true;

		for (let i = 0; i < submissions[index].mentorsFormData.value.length; i++) {
			mentors.push({ "mentorId": submissions[index].mentorsFormData.value[i].id });
		}

		let model = { mentors: mentors };
		let formData: FormData = new FormData();

		if (submissions[index].file) {
			formData.append('file', submissions[index].file, submissions[index].file.name);
		}
		formData.append('model', new Blob([JSON.stringify(model)],
			{
				type: "application/json"
			}));

		let classroomSubmission = this._classroomActivityService.resubmitWork(this.selectedActivity.id, submissionId, formData);

		classroomSubmission.then((results) => {
			submissions[index].statusId = 2;
			this.mentors = new FormControl();
			submissions[index].submitDisabled = false;
		}, (error) => {
			submissions[index].submitDisabled = false;
			console.error(error);
		});
	}

	commentMentor = new FormControl();

	submitComment(mentorForm) {

		let selectedUsers = [];

		for (let i = 0; i < this.commentMentor.value.length; i++) {
			selectedUsers.push(this.commentMentor.value[i].id);
		}

		let model = { selectedUsers: selectedUsers, message: mentorForm.form.value.comment }
		let submitComment = this._classroomActivityService.saveActivityComment(this.selectedActivity.id, model);

		submitComment.then((results) => {
			this.selectedActivity.comments.unshift(results);
			mentorForm.reset();
		}, (error) => {
			console.error(error);
		})
	}

	getMentorEmail(uuid) {
		var index = this.mentorsList.map(x => {
			return x.id;
		}).indexOf(uuid);

		return this.mentorsList[index].mentorEmail;
	}

	sortSubmissionDescOrder(submissions) {
		return submissions.sort((a, b) => b.id - a.id);
	}

	setupMentorCheckbox() {

		for (let i = 0; i < this.selectedActivity.submissions.length; i++) {
			this.selectedActivity.submissions[i].mentorsFormData = new FormControl();

			let mentors: any[] = [];
			for (let k = 0; k < this.selectedActivity.submissions[i].mentors.length; k++) {
				for (let f = 0; f < this.mentorsList.length; f++) {
					if (this.selectedActivity.submissions[i].mentors[k].mentorId == this.mentorsList[f].id) {
						mentors.push(this.mentorsList[f]);
					}
				}
			}

			this.selectedActivity.submissions[i].mentorsFormData.setValue(mentors);
		}
	}

	close() {
		this._dialogRef.close({ activity: this.selectedActivity });
	}
}
