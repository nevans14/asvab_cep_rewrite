import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsvabSampleTestDialogComponent } from './asvab-sample-test-dialog.component';

describe('AsvabSampleTestDialogComponent', () => {
  let component: AsvabSampleTestDialogComponent;
  let fixture: ComponentFixture<AsvabSampleTestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsvabSampleTestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsvabSampleTestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
