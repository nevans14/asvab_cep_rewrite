import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/services/authentication.service';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';

@Component({
  selector: 'app-merge-account-dialog',
  templateUrl: './merge-account-dialog.component.html',
  styleUrls: ['./merge-account-dialog.component.scss']
})
export class MergeAccountDialogComponent implements OnInit {

  registerEmail: any;
  accessCode: any;
  mergeForm: FormGroup;
  isLoading: Boolean;
  hasStatusMessage: Boolean;
  message: any;

  constructor(public _dialogRef: MatDialogRef<MergeAccountDialogComponent>,
    private _router: Router,
    private _config: ConfigService,
    private _dialog: MatDialog,
    private _httpHelper: HttpHelperService,
    private _formBuilder: FormBuilder,
    public _authenticationService: AuthenticationService,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {

    this.registerEmail = this.data.email;
    this.accessCode = this.data.accessCode;
    this.isLoading = false;
    this.hasStatusMessage = false;

    //set troops form controls default values and assign to form
    this.mergeForm = this._formBuilder.group({
      profileFlag: new FormControl(true),
      careerPlanFlag: new FormControl(true)
    });

  }

  /**
   * send merge account info to server
   */
  mergeAccount() {
    
    this.isLoading = true;
    
    //map form data to send
    let formData = {
      profileFlag: this.mergeForm.value.profileFlag,
      careerPlanFlag: this.mergeForm.value.careerPlanFlag
    }

    //build url to post data to
    const url = 'applicationAccess/merge/' + this.registerEmail + '/' + this.accessCode + '/' + formData.profileFlag + '/' + formData.careerPlanFlag + '/';

    let self = this;
    this._httpHelper.httpHelper('GET', url, null, null).then((success: any) => {
        let userData = success;
        if(userData.statusNumber == 0){
          this._dialogRef.close(userData);
        }else if (userData.statusNumber == 1012) {
          this.message = "Merge Failed! Access Code is already registered";
          this.hasStatusMessage = true;
        } else {
          this.message = "Unknown Status Error";
          this.hasStatusMessage = true;
        }
        this._dialogRef.close('errorEncountered');
    }, function (error) {
        //Show error to user? TODO ; legacy does not have error handling here
        self.message = error;
        self.hasStatusMessage = true;
        self.isLoading = false;
    });

  }

}

