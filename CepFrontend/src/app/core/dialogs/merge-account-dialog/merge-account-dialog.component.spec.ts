import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeAccountDialogComponent } from './merge-account-dialog.component';

describe('MergeAccountDialogComponent', () => {
  let component: MergeAccountDialogComponent;
  let fixture: ComponentFixture<MergeAccountDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeAccountDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeAccountDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
