import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alternate-training-dialog',
  templateUrl: './alternate-training-dialog.component.html',
  styleUrls: ['./alternate-training-dialog.component.scss']
})
export class AlternateTrainingDialogComponent implements OnInit {

  modalOptions: any;

  constructor(public dialogRef: MatDialogRef<AlternateTrainingDialogComponent>,
    private _router: Router,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    this.modalOptions = this.data;
  }

  close() {
    this.dialogRef.close('cancel');
  };


  reregister() {
    this.dialogRef.close(this.modalOptions.alternateTrainingSelected);
  };

}
