import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlternateTrainingDialogComponent } from './alternate-training-dialog.component';

describe('AlternateTrainingDialogComponent', () => {
  let component: AlternateTrainingDialogComponent;
  let fixture: ComponentFixture<AlternateTrainingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternateTrainingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlternateTrainingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
