import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-learn-more-dialog',
  templateUrl: './learn-more-dialog.component.html',
  styleUrls: ['./learn-more-dialog.component.scss']
})
export class LearnMoreDialogComponent implements OnInit {

  constructor(
    public _dialogRef: MatDialogRef<LearnMoreDialogComponent>
  ) { }

  ngOnInit() {
  }

  onClose() {
    this._dialogRef.close();
  }
}
