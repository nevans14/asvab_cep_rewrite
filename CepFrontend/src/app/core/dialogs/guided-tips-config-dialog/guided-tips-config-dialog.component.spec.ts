import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidedTipsConfigDialogComponent } from './guided-tips-config-dialog.component';

describe('GuidedTipsConfigDialogComponent', () => {
  let component: GuidedTipsConfigDialogComponent;
  let fixture: ComponentFixture<GuidedTipsConfigDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidedTipsConfigDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidedTipsConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
