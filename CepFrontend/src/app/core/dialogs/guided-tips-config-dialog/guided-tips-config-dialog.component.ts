import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-guided-tips-config-dialog',
  templateUrl: './guided-tips-config-dialog.component.html',
  styleUrls: ['./guided-tips-config-dialog.component.scss']
})
export class GuidedTipsConfigDialogComponent implements OnInit {

  guidedTipConfig: any;

  constructor(
    public _dialogRef: MatDialogRef<GuidedTipsConfigDialogComponent>,
    private _user: UserService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.guidedTipConfig = {
      selectALL: false,
      exploreCareer: false,
      helpLearn: false,
      helpPlan: false,
      goToMilitary: false,
      goToCollege: false,
      workBasedLearn: false,
      involveCounselorParent: false,
    }
  }

  ngOnInit() {
    if (!this.data) {
      this._user.getCompletion().subscribe(data => {
        if (data) {
          const guidedTipConfigs = this._user.getCompletionByType(data, UserService.GUIDED_TIP_CONFIG);

          if (guidedTipConfigs && guidedTipConfigs.value) {
            this.guidedTipConfig = JSON.parse(guidedTipConfigs.value);
          } else {
            // Shouldn't happen since it is initialized in left navigation
            this._user.setCompletion(UserService.GUIDED_TIP_CONFIG, JSON.stringify(this.guidedTipConfig));
          }
        }
      });
    } else {
      this.guidedTipConfig = this.data;
    }
  }

  selectAll() {
    if (this.guidedTipConfig.selectAll) {
      Object.keys(this.guidedTipConfig).forEach(key => this.guidedTipConfig[key] = true)
    } else {
      Object.keys(this.guidedTipConfig).forEach(key => this.guidedTipConfig[key] = false)
    }
  }

  submit() {
    this._user.setCompletion(UserService.GUIDED_TIP_CONFIG, JSON.stringify(this.guidedTipConfig));
  }

  onNoClick(): void {
    this._dialogRef.close();
  }
}
