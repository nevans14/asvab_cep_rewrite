import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterNotesDialogComponent } from './career-cluster-notes-dialog.component';

describe('CareerClusterNotesDialogComponent', () => {
  let component: CareerClusterNotesDialogComponent;
  let fixture: ComponentFixture<CareerClusterNotesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterNotesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterNotesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
