import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { CareerClusterNotesService } from 'app/career-cluster/services/career-cluster-notes.service';

@Component({
  selector: 'app-career-cluster-notes-dialog',
  templateUrl: './career-cluster-notes-dialog.component.html',
  styleUrls: ['./career-cluster-notes-dialog.component.scss']
})
export class CareerClusterNotesDialogComponent implements OnInit {
  initialNotesObject;
  selectedNotesObject;
  careerClusterNoteList;
  error = '';
  savingNotes = false;
  deletingNote = false;
  isPerformingAction = false;

  constructor(
    public _dialogRef: MatDialogRef<CareerClusterNotesDialogComponent>,
    public _careerClusterNotesService: CareerClusterNotesService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.initialNotesObject = this.selectedNotesObject = {};
    this.careerClusterNoteList = [];

    this.initializeSetup();
  }

  initializeSetup() {
    let that = this;
    this._careerClusterNotesService.getCareerClusterNotes().then(function (data) {
      that.careerClusterNoteList = data;
      
      that.initialNotesObject = that.selectedNotesObject = {
        ccId	: that.data.ccId,
        title	: that.data.title,
        notes	: undefined
      };
      
      for (var i = 0; i < that.careerClusterNoteList.length; i++) {
        if (that.careerClusterNoteList[i].ccId === parseInt(that.data.ccId)) {
          that.initialNotesObject = that.selectedNotesObject = {
            userId	: that.careerClusterNoteList[i].userId,
            ccId	: that.careerClusterNoteList[i].ccId,
            title	: that.careerClusterNoteList[i].title,
            notes	: that.careerClusterNoteList[i].notes,
            noteId	: that.careerClusterNoteList[i].noteId
          }
        }
      }
    })
  }

  /**
   * Reset to current career cluster object.
   */
  resetObject() {
    this.initialNotesObject = this.selectedNotesObject = {
      ccId : this.data.ccId,
      title : this.data.title,
      notes : undefined
    };

    // set notes if saved previously
    for (var i = 0; i < this.careerClusterNoteList.length; i++) {
      if (this.careerClusterNoteList[i].ccId === this.selectedNotesObject.ccId) {
        this.initialNotesObject = this.selectedNotesObject = {
          ccId : this.careerClusterNoteList[i].ccId,
          title : this.careerClusterNoteList[i].title,
          notes : this.careerClusterNoteList[i].notes,
          noteId : this.careerClusterNoteList[i].noteId
        };
      }
    }
  }

  /**
   * Save Notes
   */
  save() {
    this.error = '';
    this.savingNotes = true;
    this.isPerformingAction = true;
    var notesExist = false;

    if (this.selectedNotesObject.notes == undefined || this.selectedNotesObject.notes == '') {
      this.error = 'Note is blank';
      this.savingNotes = false;
      this.isPerformingAction = false;
      return false;
    }

    // check to see if notes was already saved
    for (var i = 0; i < this.careerClusterNoteList.length; i++) {
      if (this.careerClusterNoteList[i].ccId == this.selectedNotesObject.ccId) {
        notesExist = true;
      }
    }

    // if notes were never saved then save notes to database,
    // otherwise update notes
    let that = this;
    if (!notesExist) {
      this._careerClusterNotesService.insertCareerClusterNote(this.selectedNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    } else {
      this._careerClusterNotesService.updateCareerClusterNote(this.selectedNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    }
  };
  
  deleteNote() {
    var isConfirmed = confirm('Do you want to remove this note?');
    if (!isConfirmed) return;
    this.error = '';
    this.deletingNote = true;
    this.isPerformingAction = true;

    let that = this;
    this._careerClusterNotesService.deleteNote(this.selectedNotesObject.noteId).then(function(success) {
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.initializeSetup();
      if (that.careerClusterNoteList.length === 0) {
        that.close();
      }
    }, function (error) {
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.error = 'There was an error processing your request. Please try again.';
    })
    
  }

  /**
   * Within the modal, user can toggle between career clusters
   * that they saved notes for.
   */
  selectCareerCluster(noteId) {

    // set the information to display to the user depending on
    // their selection
    for (var i = 0; i < this.careerClusterNoteList.length; i++) {
      if (this.careerClusterNoteList[i].noteId === noteId) {
        this.selectedNotesObject = {
          ccId : this.careerClusterNoteList[i].ccId,
          title : this.careerClusterNoteList[i].title,
          notes : this.careerClusterNoteList[i].notes,
          noteId : this.careerClusterNoteList[i].noteId
        };
      }
    }
  }				



  close() {
    this._dialogRef.close();
  }

  displayCCNoteList() {
    return this.careerClusterNoteList.filter(
      item => this.initialNotesObject
        && this.initialNotesObject.ccId
        && this.initialNotesObject.ccId != item.ccId
    )
  }
  

}
