import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { ConfigService } from 'app/services/config.service';

@Component({
  selector: 'app-view-detail-dialog',
  templateUrl: './view-detail-dialog.component.html',
  styleUrls: ['./view-detail-dialog.component.scss']
})
export class ViewDetailDialogComponent implements OnInit {

  constructor(
    private _dialog: MatDialog,
    private _dialogRef: MatDialogRef<ViewDetailDialogComponent>,
    private _config: ConfigService,
  ) { }

  ngOnInit() {
  }

  close() {
    this._dialogRef.close();
  }

  showLoginModal() {
    this._dialog.open(LoginDialogComponent, {
    });

    this.close();
  }

  goToBringToSchool() {
		window.location.href = '/asvab-cep-at-your-school';
  }
}
