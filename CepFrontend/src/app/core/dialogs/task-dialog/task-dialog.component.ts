import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UtilityService } from 'app/services/utility.service';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public taskFormGroup: FormGroup;
  public userOccupationPlanCalendars: UserOccupationPlanCalendar[];
  public originalUserOccupationPlanCalendars: UserOccupationPlanCalendar[];
  public userOccupationPlans: any[];
  public wasOpenInPlan: boolean;
  public plan: any;

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<TaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _planCalendarService: PlanCalendarService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _occufindRestFactory: OccufindRestFactoryService,
    private _utilityService: UtilityService,
    private _userService: UserService,
    ) {

    this.isSaving = false;
    this.isLoading = true;
    this.wasOpenInPlan = false;
    this.plan = {};

    this.taskFormGroup = this._formBuilder.group({
      id: [''],
      taskName: ['', [Validators.required, Validators.maxLength(500)]],
      userOccupationPlan: [''],
      userOccupationPlanCalendar: ['']
    });

  }

  ngOnInit() {

    //get all plans
    let userOccupationPlans = this.getUserOccupationPlans();

    //get all user dates
    let userOccupationPlanCalendars = this.getAllUserOccupationPlanCalendars();

    Promise.all([userOccupationPlans, userOccupationPlanCalendars]).then((done: any) => {

      this.userOccupationPlans = done[0];
      this.userOccupationPlanCalendars = done[1];

      if (Array.isArray(done[1])) {
        let occuCalendars = done[1] as Array<UserOccupationPlanCalendar>;
        this.originalUserOccupationPlanCalendars = occuCalendars;
      }
      //if plan id was given then filter out dates to only show plan dates or no dates assigned to a plan
      if (this.data.planId) {
        this.wasOpenInPlan = true;
        if (Array.isArray(done[1])) {
          if(this.data.canEditPlan){
            this.userOccupationPlanCalendars = this.originalUserOccupationPlanCalendars.filter(x => x.planId == this.data.planId || !x.planId);
          }else{
            this.userOccupationPlanCalendars = this.originalUserOccupationPlanCalendars.filter(x => x.planId == this.data.planId);
          }
        }
        //is task being edited on the view plan page?
        if (this.data.canEditPlan) {
          this.wasOpenInPlan = false; //set to false so user can see the drop down
          //set initial drop down value.
          this.taskFormGroup.get("userOccupationPlan").setValue(this.data.planId);
        } else {
          this.plan = this.userOccupationPlans.find(x => x.id == this.data.planId);
        }
      }

      if(this.data.calendarId){
        this.taskFormGroup.get("userOccupationPlanCalendar").setValue(this.data.calendarId);
      }

      //preload data if editing a task
      if (this.data.task) {
        this.taskFormGroup.get("taskName").setValue(this.convertTaskName(this.data.task.taskName));

        if (this.data.task.calendarId) {
          this.taskFormGroup.get("userOccupationPlanCalendar").setValue(this.data.task.calendarId);
        }
      }

      this.isLoading = false;
    });

  }

  async getUserOccupationPlans() {

    try {

      let userOccupationPlans: any = await this._userOccupationPlanService.getAll();

      return Promise.all(userOccupationPlans.map(async (userOccupationPlan: any) => {

        let occuTitle = await this._occufindRestFactory
          .getOccupationTitleDescriptionNF(userOccupationPlan.socId);

        return await { ...userOccupationPlan, occuTitle };

      }));


    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getAllUserOccupationPlanCalendars() {

    try {

      let userOccupationPlanCalendars = await this._planCalendarService.getCalendars();

      return await userOccupationPlanCalendars;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  saveTaskFormGroup() {

    this.isSaving = true;

    this._userService.setCompletion(UserService.ADDED_TASK, 'true');

    let formData: any = {
      id: null,
      planId: (this.wasOpenInPlan) ? this.data.planId :
        (this.taskFormGroup.value.userOccupationPlan == '') ? null : this.taskFormGroup.value.userOccupationPlan,
      calendarId: (this.taskFormGroup.value.userOccupationPlanCalendar == '') ? null : +this.taskFormGroup.value.userOccupationPlanCalendar,
      taskName: this.taskFormGroup.value.taskName,
      completed: false
    }

    if (this.data.task) {

      formData.id = this.data.task.id;
      formData.planId = (this.taskFormGroup.value.userOccupationPlan == '') ? null : this.taskFormGroup.value.userOccupationPlan;

      //updating an existing record
      this._planCalendarService.updateTask(formData, this.data.task.id).then((rowsAffected: number) => {
        this.isSaving = false;
        this.dialogRef.close(formData);
      }).catch((error: any) => {
        this.isSaving = false;
        console.error("ERROR", error);
      });
    } else {
      //new record

      //save task to a plan
      if (formData.planId) {
        this._planCalendarService.savePlanTask(formData.planId, formData).then((newRecord: UserOccupationPlanCalendarTask) => {
          this.isSaving = false;
          this.dialogRef.close(newRecord);
        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });
      } else {
        //save task not to a plan
        this._planCalendarService.saveTask(formData).then((newRecord: UserOccupationPlanCalendarTask) => {
          this.isSaving = false;
          this.dialogRef.close(newRecord);
        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });
      }

    }


  }

  close() {
    this.dialogRef.close();
  }

  onPlanChange(event) {
    this.userOccupationPlanCalendars = this.originalUserOccupationPlanCalendars.filter(x => x.planId == event.target.value || !x.planId);
  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

}
