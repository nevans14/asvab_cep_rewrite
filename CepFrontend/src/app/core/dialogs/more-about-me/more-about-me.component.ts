import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { LearnAboutYourselfService } from 'app/services/learn-about-yourself.service';
import { UtilityService } from 'app/services/utility.service';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-more-about-me',
  templateUrl: './more-about-me.component.html',
  styleUrls: ['./more-about-me.component.scss']
})
export class MoreAboutMeComponent implements OnInit {

  experiences: any[];
  experienceForm: FormGroup;
  updateExperienceForm: FormGroup;
  inverseCheckBoxMap = new Map();
  public savedNewGap : boolean = false;
  
  constructor(
    public _dialogRef: MatDialogRef<MoreAboutMeComponent>,
    private _formBuilder: FormBuilder,
    private _learnAboutYourselfService: LearnAboutYourselfService,
    private dialog: MatDialog,
    private _utility: UtilityService,
    private _user: UserService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { 
    this.experienceForm = this._formBuilder.group({
      experienceId: new FormControl(''),
      jobTitle: new FormControl('', Validators.required),
      likes: this._formBuilder.group({   
        communication: new FormControl(''),
        collaboration: new FormControl(''),
        adaptability: new FormControl(''),
        timeManagement: new FormControl(''),
        initiative: new FormControl(''),
        creativity: new FormControl(''),
        criticalThinking: new FormControl(''),
        persuasion: new FormControl(''),
        digitalLiteracy: new FormControl(''),
        otherLike: new FormControl(''),
        otherLikeText: new FormControl('')
      }),
      dislikes: this._formBuilder.group({  
        workingOutside: new FormControl(''),
        workingWithHands: new FormControl(''),
        workingWithHands2: new FormControl(''),
        workingWithChildren: new FormControl(''),
        dislikedItem: new FormControl(''),
        dislikedItem2: new FormControl(''),
        managingPeople: new FormControl(''),
        dislikedItem3: new FormControl(''),
        dislikedItem4: new FormControl(''),
        otherDislike: new FormControl(''),
        otherDislikeText: new FormControl('')
      })
    });

    this.updateExperienceForm = this._formBuilder.group({
      experienceId: new FormControl(''),
      jobTitle: new FormControl('', Validators.required),
      likes: this._formBuilder.group({   
        communication: new FormControl(''),
        collaboration: new FormControl(''),
        adaptability: new FormControl(''),
        timeManagement: new FormControl(''),
        initiative: new FormControl(''),
        creativity: new FormControl(''),
        criticalThinking: new FormControl(''),
        persuasion: new FormControl(''),
        digitalLiteracy: new FormControl(''),
        otherLike: new FormControl(''),
        otherLikeText: new FormControl('')
      }),
      dislikes: this._formBuilder.group({  
        workingOutside: new FormControl(''),
        workingWithHands: new FormControl(''),
        workingWithHands2: new FormControl(''),
        workingWithChildren: new FormControl(''),
        dislikedItem: new FormControl(''),
        dislikedItem2: new FormControl(''),
        managingPeople: new FormControl(''),
        dislikedItem3: new FormControl(''),
        dislikedItem4: new FormControl(''),
        otherDislike: new FormControl(''),
        otherDislikeText: new FormControl('')
      })
    });
  }

  ngOnInit() {

    this.experiences = this.convertExperiencesForView(this.data);
    
    //invert checkbox map to be used when editing a record
    for (let entry of Array.from(this._utility.checkBoxMap.entries())) {
      this.inverseCheckBoxMap.set(entry[1], entry[0]);
    }

  }

  likeDislikeOnForm():any {
    return Object.values(this.experienceForm.value.likes).some((i:boolean) => i) 
      || Object.values(this.experienceForm.value.dislikes).some((i:boolean) => i);
  }

  likeDislikeFromExisting() {
    return this.experiences.some((i:any) => i.likes.length > 0 || i.dislikes.length > 0);
  }

  setCompletionFromExistingLikeDislike() {
    if (this.likeDislikeFromExisting()) {
      this._user.setCompletion(UserService.LIKE_DISLIKE_MORE_ABOUT_ME, 'true');
    } else {
      this._user.setCompletion(UserService.LIKE_DISLIKE_MORE_ABOUT_ME, 'false');
    }

    // this._user.sendMessage(UserService.LIKE_DISLIKE_MORE_ABOUT_ME);
  }

  submitExperienceForm(){

    let isUpdate: boolean = false;
    
    this.adjustOtherText();

    //map form data to send
    let formData = {
      position: 1,  //TODO what is position used for? ordering?
      id: this.experienceForm.value.experienceId,
      moreAboutMe: this.getMoreAboutMeAsJSON(this.experienceForm),
    };

    let index : number = -1;
    //if experience id exists we are modifying an existing record
    if(this.experienceForm.value.experienceId){
      if(this.experienceForm.value.experienceId > 0){
        isUpdate = true;

        for(var i = 0; i < this.experiences.length; i++) {
            if(this.experiences[i].id === this.experienceForm.value.experienceId) {
                index = i;
                break;
            }
        }
      }
    }

    if(!isUpdate){

      //new record
      this._learnAboutYourselfService.insertMoreAboutMe(formData).then((success:any) => {
        this.experiences.push(this.formatExperience(success));
        this.experienceForm.reset();
        this.setCompletionFromExistingLikeDislike();
        this.savedNewGap = true;
      }).catch(error => {
        //TODO update?
        window.alert("Error!!!!: " + error);
      });
    }else{
      //update record
      this._learnAboutYourselfService.updateMoreAboutMe(formData).then((success:any) => {
        if(index >= 0){
          //since the server does not return the updated model
          //then manually format the form data into a returned object
          this.experiences.splice(index, 1, this.formatExperience(formData));
          this.experienceForm.reset();
          this.setCompletionFromExistingLikeDislike();
        }
      }).catch(error => {
        //TODO update?
        window.alert("Error!!!!: " + error);
      });
    }

  }

  submitUpdateExperienceForm(){
    let isUpdate: boolean = false;

    //if the other check box is not checked ; clear out other text
    this.adjustOtherText(true);

    //map form data to send
    let formData = {
      position: 1,  
      id: this.updateExperienceForm.value.experienceId,
      moreAboutMe: this.getMoreAboutMeAsJSON(this.updateExperienceForm),
    };

    let index : number = -1;
    //if experience id exists we are modifying an existing record
    if(this.updateExperienceForm.value.experienceId){
      if(this.updateExperienceForm.value.experienceId > 0){
        isUpdate = true;

        for(var i = 0; i < this.experiences.length; i++) {
            if(this.experiences[i].id === this.updateExperienceForm.value.experienceId) {
                index = i;
                break;
            }
        }
      }
    }

    if(!isUpdate){

      //new record
      this._learnAboutYourselfService.insertMoreAboutMe(formData).then((success:any) => {
        this.experiences.push(this.formatExperience(success));
        this.updateExperienceForm.reset();
        this.setCompletionFromExistingLikeDislike();
        this.savedNewGap = true;
      }).catch(error => {
        //TODO update?
        window.alert("Error!!!!: " + error);
      });
    }else{
      //update record
      this._learnAboutYourselfService.updateMoreAboutMe(formData).then((success:any) => {
        if(index >= 0){
          //since the server does not return the updated model
          //then manually format the form data into a returned object
          this.experiences.splice(index, 1, this.formatExperience(formData));
          this.updateExperienceForm.reset();
          this.setCompletionFromExistingLikeDislike();
        }
      }).catch(error => {
        //TODO update?
        window.alert("Error!!!!: " + error);
      });
    }

  }

  /**
   * format: 
   * {
        "values": [
          {
            "fieldName": "fieldValue"
          }
        ]
      }
   * }
   */
  getMoreAboutMeAsJSON(form: FormGroup): String{
    let valuesObj = {
        values : []
    };
    for (let field in form.controls) { 
      let control = form.get(field); 

      //ignore hidden field
      if(field.toLowerCase() === "experienceid"){
        continue;
      }

      if(control.value){
        valuesObj.values.push({ [field]: control.value});
      }else{
        valuesObj.values.push({ [field]: ""});
      }
    }

    let json = JSON.stringify(valuesObj);
    console.debug('array value: ', json);
 
    return json;
  }
  
  convertExperiencesForView(experiences: any[]) : any[]{
    let returnArr = [];

    for (let experience of experiences) { 
      returnArr.push( this.formatExperience(experience));
    }
    
    return returnArr;
  }
  
  formatExperience(experience: any){

    let moreAboutMe = JSON.parse(experience.moreAboutMe).values;
    let returnObj : {
      jobTitle: string,
      id: number,
      position: number,
      likes: any[],
      dislikes: any[]
    } = {
      jobTitle: "",
      id: undefined,
      position: undefined,
      likes : [],
      dislikes: []  
    };
    
    returnObj.jobTitle = (moreAboutMe) ? moreAboutMe[0].jobTitle : "",
    returnObj.id = experience.id,
    returnObj.position = experience.position,
    returnObj.likes = this._utility.getValuesFromMoreAboutMe(moreAboutMe[1].likes),
    returnObj.dislikes = this._utility.getValuesFromMoreAboutMe(moreAboutMe[2].dislikes)

    return returnObj;

  }

  removeExperience(jobTitle, id, index){

    let jobTitleText : string = (jobTitle) ? jobTitle : "job";

    let data = {
      title: 'Confirm Deletion',
      message: 'Remove ' + jobTitleText + ', Are you sure?'
    };

    this.dialog.open(ConfirmDialogComponent, {
        data: data,
        maxWidth: '400px'
    }).beforeClosed().subscribe( response => {
      if(response){
        if(response === 'ok'){

          //remove record from db
          this._learnAboutYourselfService.deleteMoreAboutMe(id).then((success:any) => {
            
            //if currently modifying clear the form
            if(this.experienceForm.value.experienceId == id){
              this.experienceForm.reset();
            }

            //remove record from view
            this.experiences.splice(index, 1);

            this.setCompletionFromExistingLikeDislike();            
          }).catch(error => {
            //TODO update?
            window.alert("Error!!!!: " + error);
          });

        }
      }
    });

  }

  async editExperience(experience, index){

    //if either the update form or the new form is dirty prompt user to whether they wish to cancel changes.
    //before editing the new record
    if(this.updateExperienceForm.dirty || this.experienceForm.dirty){
      let data = {
        title: 'Cancel',
        message: 'Cancel current changes, are you sure?'
      };

      // wait for response
      const result = await this.openConfirmDialog(data);

      if(result){
        if(result === 'ok'){
          this.updateExperienceForm.reset();
          this.experienceForm.reset();
        }else{
          return;
        }
      }else{
        return;
      }
    }
    
    this.updateExperienceForm.get("experienceId").setValue(experience.id);
    this.updateExperienceForm.get("jobTitle").setValue(experience.jobTitle);
    let otherText : string[] = [];
    //mark check boxes for each like
    for(let x of experience.likes){
      const control = this.updateExperienceForm.get("likes").get(this.inverseCheckBoxMap.get(x));
      if(control){
        control.setValue(true); //checkbox control was found set the checkbox value to true
      }else{
        //if value is present but not in the map; add the value to an array and then put data into the other text box
        if(x){
          const otherLikeTextControl = this.updateExperienceForm.get("likes").get("otherLikeText");
          otherText.push(x);
          otherLikeTextControl.setValue(otherText.join(","));  
          const otherLikeCheckBox = this.updateExperienceForm.get("likes").get("otherLike");
          otherLikeCheckBox.setValue(true);
        }
      }
    }

    otherText = [];

    //mark check boxes for each dislike
    for(let x of experience.dislikes){
      const control = this.updateExperienceForm.get("dislikes").get(this.inverseCheckBoxMap.get(x));
      if(control){
        control.setValue(true); //checkbox control was found set the checkbox value to true
      }else{
          //if value is present but not in the map; add the value to an array and then put data into the other text box
          if(x){
            const otherDislikeTextControl = this.updateExperienceForm.get("dislikes").get("otherDislikeText");
            otherText.push(x);
            otherDislikeTextControl.setValue(otherText.join(","));
            const otherLikeCheckBox = this.updateExperienceForm.get("dislikes").get("otherDislike");
            otherLikeCheckBox.setValue(true);
          }
      }
    }

  }

  promptClose(){
    if(this.experienceForm.dirty){
      let data = {
        title: 'Cancel',
        message: 'Closing Dialog will cancel any changes, are you sure?'
      };
  
      this.dialog.open(ConfirmDialogComponent, {
          data: data,
          maxWidth: '400px'
      }).beforeClosed().subscribe( response => {
        if(response){
          if(response === 'ok'){
            //reset form
            this.experienceForm.reset();
            //close as normal
            this._dialogRef.close(this.formatMoreAboutMeReturn(this.experiences));
          }
        }
      });
    }else{
      //close as normal
      this._dialogRef.close(this.formatMoreAboutMeReturn(this.experiences));
    }
  }

  promptCancel(){
    if(this.experienceForm.dirty){
      let data = {
        title: 'Cancel',
        message: 'Cancel changes, are you sure?'
      };
  
      this.dialog.open(ConfirmDialogComponent, {
          data: data,
          maxWidth: '400px'
      }).beforeClosed().subscribe( response => {
        if(response){
          if(response === 'ok'){
            //reset form
            this.experienceForm.reset();
            this.savedNewGap = true;
          }
        }
      });
    }else{
      //reset form
      this.experienceForm.reset();
      this.savedNewGap = true;
    }
  }

  promptEditCancel(){
    let data = {
      title: 'Cancel',
      message: 'Cancel current changes, are you sure?'
    };
      
    this.dialog.open(ConfirmDialogComponent, {
        data: data,
        maxWidth: '400px'
    }).beforeClosed().subscribe( response => {
      if(response){
        if(response === 'ok'){
          //reset form
          this.updateExperienceForm.reset();
          this.savedNewGap = true;
        }
      }
    });
  }

  promptAddNew(){
    if(this.experienceForm.dirty || this.updateExperienceForm.dirty){
      let data = {
        title: 'Add New Experience',
        message: 'Cancel current changes and start a new Experience, are you sure?'
      };
  
      this.dialog.open(ConfirmDialogComponent, {
          data: data,
          maxWidth: '400px'
      }).beforeClosed().subscribe( response => {
        if(response){
          if(response === 'ok'){
            //reset forms
            this.updateExperienceForm.reset();
            this.experienceForm.reset();
    this.savedNewGap = false;
            
          }
        }
      });
    }else{
      //reset form
      this.updateExperienceForm.reset();
      this.experienceForm.reset();
      this.savedNewGap = false;

    }
       
  }

  /**
   * returns an object that is passed back to the component that called the dialog
   */
  formatMoreAboutMeReturn(experiences: any[]){

    let returnObj : {
      likes: any[],
      dislikes: any[]
    } = {
      likes: [],
      dislikes: []
    };

    for (let experience of experiences) { 

      
      for(let like of experience.likes){
        returnObj.likes.push(like);
      }
      
      for(let dislike of experience.dislikes){
        returnObj.dislikes.push(dislike);
      }
    }

    return returnObj; 

  }

  /**
   * returns true if the update form is being shown
   * this is detected by whether the experience id is populated
   */
  updateFormIsShowing() : boolean{

    if(this.updateExperienceForm.value.experienceId > 0 ){
      return true;
    }else{
      return false;
    }

  }

  async openConfirmDialog(data): Promise<any> {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: "400px",
      data: data
    });
   
    return dialogRef.afterClosed()
      .toPromise()
      .then(result => {
         return Promise.resolve(result); //return decision
      });
   }

   /**
    * 
    */
   adjustOtherText(isUpdateMode: boolean = false){
    
    if(isUpdateMode){
      //update form
      if(this.updateExperienceForm.value.dislikes.otherDislike){
        if(this.updateExperienceForm.value.dislikes.otherDislike === false){
          this.updateExperienceForm.value.dislikes.otherDislikeText = '';
        }
      }else{
        this.updateExperienceForm.value.dislikes.otherDislikeText = '';
      }

      if(this.updateExperienceForm.value.likes.otherLike){
        if(this.updateExperienceForm.value.likes.otherLike === false){
          this.updateExperienceForm.value.likes.otherLikeText = '';
        }
      }else{
        this.updateExperienceForm.value.likes.otherLikeText = '';
      }

    }else{
      //add new form
      if(this.experienceForm.value.dislikes.otherDislike){
        if(this.experienceForm.value.dislikes.otherDislike === false){
          this.experienceForm.value.dislikes.otherDislikeText = '';
        }
      }else{
        this.experienceForm.value.dislikes.otherDislikeText = '';
      }

      if(this.experienceForm.value.likes.otherLike){
        if(this.experienceForm.value.likes.otherLike === false){
          this.experienceForm.value.likes.otherLikeText = '';
        }
      }else{
        this.experienceForm.value.likes.otherLikeText = '';
      }
    }

   }


}