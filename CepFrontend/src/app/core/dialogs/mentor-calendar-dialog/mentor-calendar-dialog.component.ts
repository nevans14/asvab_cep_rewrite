import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-mentor-calendar-dialog',
  templateUrl: './mentor-calendar-dialog.component.html',
  styleUrls: ['./mentor-calendar-dialog.component.scss']
})
export class MentorCalendarDialogComponent implements OnInit {

  // public userOccupationPlanCalendarTasks: UserOccupationPlanCalendarTask[];
  public userOccupationPlanCalendars: any[];
  public tasksUnassignedToDate: UserOccupationPlanCalendarTask[];
  public tasksUnassignedLength: number;
  public userOccupationPlans: any[];
  public showCompletedTasks: boolean;
  public unassignedProgress: any;
  public tasksWithNoDate: any[];
  public originalTasksWithNoDate: any[];

  constructor(private _planCalendarService: PlanCalendarService,
    private _utilityService: UtilityService,
    public dialogRef: MatDialogRef<MentorCalendarDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    // this.userOccupationPlanCalendarTasks = [];
    this.userOccupationPlanCalendars = this.data.calendarItems;
    this.userOccupationPlans = this.data.careerPlans;
    this.originalTasksWithNoDate = this.data.tasks;
    this.tasksWithNoDate = this.data.tasks;


    this.tasksUnassignedToDate = [];
    this.tasksUnassignedLength = 0;
    this.showCompletedTasks = false;  //for unassigned tasks to dates

    //create initial filter option for dates with tasks
    //create initial option for expanding list to true
    this.userOccupationPlanCalendars.map((userOccupationPlanCalendar) => {
      userOccupationPlanCalendar.showCompletedTasks = false;
      userOccupationPlanCalendar.expanded = true;

      return userOccupationPlanCalendar;
    });

  }

  ngOnInit() {

    if(this.originalTasksWithNoDate){
      this.tasksWithNoDate = this.originalTasksWithNoDate.filter(x => x.completed == false);
    }
  }

  getProgressBarValue(max: number, current: number) {
    return (max - current) / max * 100;
  }

  getProgressBarValueForCalendar(calendar: UserOccupationPlanCalendar) {

    //set tasks
    let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

    return (this.getProgressBarValue(calendar.tasks.length, notCompletedTasks.length));

  }

  getRemainingTasksForCalendar(calendar: UserOccupationPlanCalendar) {

    //get tasks
    let notCompletedTasks = calendar.tasks.filter(x => !x.completed);

    if (notCompletedTasks) {
      return notCompletedTasks.length;
    }

    return 0;
  }

  getUnassignedProgress() {

    let notCompletedTasks = this.originalTasksWithNoDate.filter(x => x.completed == false);

    return (this.getProgressBarValue(this.originalTasksWithNoDate.length, notCompletedTasks.length));
  }

  switchTaskViewByCompleted() {

    this.showCompletedTasks = !this.showCompletedTasks;

    if(this.showCompletedTasks){
      this.tasksWithNoDate = this.originalTasksWithNoDate;
    }else{
      this.tasksWithNoDate = this.originalTasksWithNoDate.filter(x => x.completed == false);
      
    }

  }

  getPlanTitle(planId) {

    let occuPlan = this.userOccupationPlans.find(x => x.id == planId);
    if (occuPlan) {
      return "Linked to " + occuPlan.title;
    } else {
      return "";
    }
  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

  hasUrl(taskName): boolean {
    if (this._utilityService.isJson(taskName)) {
      return true;
    }

    return false;
  }

  getUrl(taskName) {
    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).url;
    }

    return taskName;
  }

  close() {
    this.dialogRef.close();
  }

}
