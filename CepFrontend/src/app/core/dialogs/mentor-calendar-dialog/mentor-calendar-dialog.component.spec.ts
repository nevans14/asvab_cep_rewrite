import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorCalendarDialogComponent } from './mentor-calendar-dialog.component';

describe('MentorCalendarDialogComponent', () => {
  let component: MentorCalendarDialogComponent;
  let fixture: ComponentFixture<MentorCalendarDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorCalendarDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorCalendarDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
