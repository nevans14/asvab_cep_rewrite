import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedSchoolsDialogComponent } from './linked-schools-dialog.component';

describe('LinkedSchoolsDialogComponent', () => {
  let component: LinkedSchoolsDialogComponent;
  let fixture: ComponentFixture<LinkedSchoolsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedSchoolsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedSchoolsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
