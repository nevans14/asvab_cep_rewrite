import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectedSchool } from 'app/core/models/selectedSchool';
import { LoginService } from 'app/services/login.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-linked-schools-dialog',
  templateUrl: './linked-schools-dialog.component.html',
  styleUrls: ['./linked-schools-dialog.component.scss']
})
export class LinkedSchoolsDialogComponent implements OnInit {

  public isLoading: boolean;
  public searchSchoolList: any[];
  public selectedSchoolList: any[];
  public searchSchoolError: any;
  public schoolForm: FormGroup;
  public isSearchSchoolButtonDisabled: boolean;
  public isDirty: boolean;
  public isSearching: boolean;
  public currentSelectedSchools: any[]; //records currently in db

  constructor(public _dialogRef: MatDialogRef<LinkedSchoolsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _loginService: LoginService,
    private _formBuilder: FormBuilder,
    private _userService: UserService) {

    this.isLoading = true;
    //set troops form controls default values and assign to form
    this.schoolForm = this._formBuilder.group({
      schoolzipcode: new FormControl(''),
      schoolCity: new FormControl(''),
      schoolState: new FormControl(''),
      schoolcode: new FormControl('')
    });
    this.searchSchoolList = [];
    this.selectedSchoolList = [];
    this.isDirty = false;
    this.isSearching = false;
    this.currentSelectedSchools = [];
  }

  ngOnInit() {

    this.getData().then(() => {
      this.isLoading = false;
      this._dialogRef.updateSize("600px");
    });

  }

  async getData() {
    //any data calls go here
    this.currentSelectedSchools = this.data.selectedSchools;
  }


  searchSchool(form) {
    this.isSearchSchoolButtonDisabled = true;
    this.searchSchoolError = undefined;
    this.isSearching = true;

    let searchSchoolObject = {
      schoolCode: form.value.schoolcode,
      city: form.value.schoolCity,
      state: form.value.schoolState,
      zipCode: form.value.schoolzipcode
    }

    if ((searchSchoolObject.city && searchSchoolObject.state) || searchSchoolObject.schoolCode || searchSchoolObject.zipCode) {

      this._loginService.searchSchool(searchSchoolObject).then((results: any) => {
        this.searchSchoolList = results;

        if (this.searchSchoolList.length == 0) {
          this.searchSchoolError = 'Nothing found. Please try again.'
        } else {

          if (this.currentSelectedSchools && this.currentSelectedSchools.length > 0) {

            //filter out any values that already are saved for the user
            this.searchSchoolList  = this.searchSchoolList.filter((school: any) => {

              let currentSelectedSchoolAlreadyExists = this.currentSelectedSchools.find(x => x.schoolCode == school.schoolCode);

              if(!currentSelectedSchoolAlreadyExists){
                return school;
              }

            })

          }
        }

        this.isSearching = false;
        this.isSearchSchoolButtonDisabled = false;
      }, error => {
        console.error(error);
        this.searchSchoolError = 'There was an error searching for school. Please try again.'
        this.isSearchSchoolButtonDisabled = false;
        this.isSearching = false;
      });

    } else {
      this.searchSchoolError = 'You must search by city/state or school code or zip code';
      this.isSearchSchoolButtonDisabled = false;
      this.isSearching = false;
    }

  }

  /**
   * This function sets user's selected schools.
   */
  setSchooltoSelectedList(schoolObject) {

    let i = this.selectedSchoolList.findIndex(o => o.schoolCode == schoolObject.schoolCode);

    if (i > -1) {
      schoolObject.toggleSchoolSelectedView = !schoolObject.toggleSchoolSelectedView;
      this.selectedSchoolList[i].toggleSchoolSelectedView = !this.selectedSchoolList[i].toggleSchoolSelectedView;
    } else {
      this.selectedSchoolList.push(schoolObject);
    }
  }

  close() {
    this._dialogRef.close();
  }

  saveSelectedSchools() {

    if (this.selectedSchoolList && this.selectedSchoolList.length > 0) {

      const selectedSchools = this.selectedSchoolList.map((school: any) => {
        return school.schoolCode;
      })

      this._userService.saveSelectedSchools(selectedSchools).then((response: SelectedSchool[]) => {

        this._dialogRef.close(response);

      }).catch((error: any) => {
        console.error("ERROR", error);
      })

    }
  }

}
