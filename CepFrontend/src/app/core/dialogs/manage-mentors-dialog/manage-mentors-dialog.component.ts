import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MentorService } from 'app/services/mentor.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-manage-mentors-dialog',
  templateUrl: './manage-mentors-dialog.component.html',
  styleUrls: ['./manage-mentors-dialog.component.scss']
})
export class ManageMentorsDialogComponent implements OnInit {

  private isLoading: boolean;
  public mentors: any[];
  public pendingMentors: any[];
  public successText: string;

  constructor(public _dialogRef: MatDialogRef<ManageMentorsDialogComponent>,
    private _mentorService: MentorService,
    private _matDialog: MatDialog) {
    this.isLoading = true;
    this.mentors = [];
    this.successText = null;
  }

  ngOnInit() {


    //get info
    //get all mentors
    let mentors = this._mentorService.getAll();

    //get all pending mentors
    let pendingMentors = this._mentorService.getPendingMentors();

    Promise.all([mentors, pendingMentors]).then((done: any) => {

      this.mentors = done[0];
      this.pendingMentors = done[1];

      this.isLoading = false;

    });

  }

  closeMe() {
    this._dialogRef.close();
  }

  removeMentor(mentor) {
    let dialogData = {
      title: 'Confirm Mentor Removal',
      message: 'Remove ' + mentor.mentorEmail + ', are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          this._mentorService.delete(mentor.id).then(success => {
            let mentorIndex = this.mentors.findIndex(x => x.mentorEmail == mentor.mentorEmail);
            if (mentorIndex > -1) {
              this.mentors.splice(this.mentors.indexOf(mentorIndex), 1);
            }
          }).catch(error => { console.error("ERROR", error) });
        }
      }
    });
  }

  remindMentor(mentorEmail, id) {

    let formData: any = {
      mentorEmail: mentorEmail
    }

    let dialogData = {
      title: 'Send Mentor Reminder',
      message: 'Send reminder to ' + mentorEmail + ', are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          this._mentorService.sendReminderEmail(id).then(success => {
            this.successText = "Reminder sent successfully!";
            setTimeout(() => { this.successText = ""; }, 5000);
          }).catch(error => { console.error("ERROR", error) });
        }
      }
    });
  }

}
