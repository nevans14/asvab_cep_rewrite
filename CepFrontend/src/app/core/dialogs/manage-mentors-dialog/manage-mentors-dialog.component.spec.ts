import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMentorsDialogComponent } from './manage-mentors-dialog.component';

describe('ManageMentorsDialogComponent', () => {
  let component: ManageMentorsDialogComponent;
  let fixture: ComponentFixture<ManageMentorsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMentorsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMentorsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
