import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-session-keepalive-dialog',
  templateUrl: './session-keepalive-dialog.component.html',
  styleUrls: ['./session-keepalive-dialog.component.scss']
})
export class SessionKeepaliveDialogComponent implements OnInit {

  title: any;
  message: any;

  constructor(
    private _dialogRef: MatDialogRef<SessionKeepaliveDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    this.message = this.data.message;
    this.title = this.data.title
  }

  logout() {
    this._dialogRef.close('logOff');
  }

  continue() {
    this._dialogRef.close('keepalive');
  }

}
