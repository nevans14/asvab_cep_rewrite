import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionKeepaliveDialogComponent } from './session-keepalive-dialog.component';

describe('SessionKeepaliveDialogComponent', () => {
  let component: SessionKeepaliveDialogComponent;
  let fixture: ComponentFixture<SessionKeepaliveDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionKeepaliveDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionKeepaliveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
