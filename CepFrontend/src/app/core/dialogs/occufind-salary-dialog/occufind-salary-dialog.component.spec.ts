import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccufindSalaryDialogComponent } from './occufind-salary-dialog.component';

describe('OccufindSalaryDialogComponent', () => {
  let component: OccufindSalaryDialogComponent;
  let fixture: ComponentFixture<OccufindSalaryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccufindSalaryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccufindSalaryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
