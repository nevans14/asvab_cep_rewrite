import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
  selector: 'app-occufind-salary-dialog',
  templateUrl: './occufind-salary-dialog.component.html',
  styleUrls: ['./occufind-salary-dialog.component.scss']
})
export class OccufindSalaryDialogComponent implements OnInit {

  public id: any;
  public stateSalary: any;
  public stateEntrySalary: any;
  public nationalSalary: any;
  public nationalEntrySalary: any;
  public blsTitle: any;
  public chart; //salary map chart
  public entryLevelSalaryChart; // entry level salary map chart
  public chartMobile //mobile salary map chart
  public entryLevelSalaryChartMobile; //mobile entry level salary map chart
  public isLoading: boolean = true;

  constructor(public _dialogRef: MatDialogRef<OccufindSalaryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _careerClusterRestFactory: CareerClusterRestFactoryService,
    private _dialog: MatDialog,
    private _occufindRestFactory: OccufindRestFactoryService) {
      this.id = data.id;
  }

  ngOnInit() {

    Promise.all([this._careerClusterRestFactory.getStateSalary(this.id).toPromise().then(response => this.stateSalary = response),
    this._careerClusterRestFactory.getStateEntrySalary(this.id).toPromise().then(response => this.stateEntrySalary = response),
    this._occufindRestFactory.getNationalSalary(this.id).toPromise().then(response => this.nationalSalary = response),
    this._occufindRestFactory.getNationalEntrySalary(this.id).toPromise().then(response => this.nationalEntrySalary = response),
    this._occufindRestFactory.getBLSTitle(this.id).toPromise().then(response => this.blsTitle = response)])
      .then((success: any) => {
        this.isLoading = false;
        this.setupCharts();
    });

  }

  setupCharts() {

    var outerArray = [];
    var innerArray = ['State', 'AverageSalary', {
      type: 'string',
      role: 'tooltip'
    }];
    outerArray[0] = innerArray;
    for (var i = 0; i < this.stateSalary.length; i++) {
      if (this.stateSalary[i].avgSalary != -1) {
        innerArray = [this.stateSalary[i].state, this.stateSalary[i].avgSalary, 'Average Salary: $' + this.stateSalary[i].avgSalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')];
      } else {
        innerArray = [this.stateSalary[i].state, 187200, 'Average Salary: $' + '187,200 or more'];
      }

      outerArray[i + 1] = innerArray;
    }

    var chart1 = { chartType: undefined, dataTable: undefined, options: undefined };
    chart1.chartType = "GeoChart";
    chart1.dataTable = outerArray;
    chart1.options = {
      width: 480,
      /*
       * width : 406, height : 297,
       */
      region: "US",
      resolution: "provinces",
      colors: ['#ffffff', '#02767b']
    };
    this.chart = chart1;

    var entryOuterArray = [];
    var entryInnerArray = ['State', 'EntrySalary', {
      type: 'string',
      role: 'tooltip'
    }];
    entryOuterArray[0] = entryInnerArray;
    for (var i = 0; i < this.stateEntrySalary.length; i++) {
      if (this.stateEntrySalary[i].entrySalary == undefined) {
        entryInnerArray = [this.stateEntrySalary[i].state, 0, 'Entry Salary: N/A'];
      } else if (this.stateEntrySalary[i].entrySalary != -1) {
        entryInnerArray = [this.stateEntrySalary[i].state, this.stateEntrySalary[i].entrySalary, 'Entry Salary: $' + this.stateEntrySalary[i].entrySalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')];
      } else {
        entryInnerArray = [this.stateEntrySalary[i].state, 208000, 'Entry Salary: $208000 or more'];
      }
      entryOuterArray[i + 1] = entryInnerArray;
    }

    // entry-level state salary
    var entryLevelSalaryChart = { chartType: undefined, dataTable: undefined, options: undefined };
    entryLevelSalaryChart.chartType = "GeoChart";
    entryLevelSalaryChart.dataTable = entryOuterArray;
    entryLevelSalaryChart.options = {
      width: 480,
      region: "US",
      resolution: "provinces",
      colors: ['#fff', '#02737b']
    }
    this.entryLevelSalaryChart = entryLevelSalaryChart;

    // mobile version
    var chart2 = { chartType: undefined, dataTable: undefined, options: undefined };
    chart2.chartType = "GeoChart";
    chart2.dataTable = outerArray;
    chart2.options = {
      width: 300,
      /*
       * width : 406, height : 297,
       */
      region: "US",
      resolution: "provinces",
      colors: ['#ffffff', '#02767b']
    };
    this.chartMobile = chart2;

    var entryLevelSalaryChartMobile = { chartType: undefined, dataTable: undefined, options: undefined };
    entryLevelSalaryChartMobile.chartType = "GeoChart";
    entryLevelSalaryChartMobile.dataTable = entryOuterArray;
    entryLevelSalaryChartMobile.options = {
      width: 300,
      region: "US",
      resolution: "provinces",
      colors: ['#fff', '#02767b']
    }
    this.entryLevelSalaryChartMobile = entryLevelSalaryChartMobile;

  }

  openDescription() {

    const modalOptions = {
      title: 'Poverty Level',
      message: 'This number is issued each year by the Department of Health and Human Services (HHS). It represents the poverty guidelines for a single individual living in the 48 contiguous states. The number varies for citizens of Alaska and Hawaii, and increases with each additional member of the household.'
    };

    const dialogRef1 = this._dialog.open(MessageDialogComponent, {
      data: modalOptions,
      hasBackdrop: true,
      disableClose: true
    });

  }

}
