import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorStudentReportDialogComponent } from './mentor-student-report-dialog.component';

describe('MentorStudentReportDialogComponent', () => {
  let component: MentorStudentReportDialogComponent;
  let fixture: ComponentFixture<MentorStudentReportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorStudentReportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorStudentReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
