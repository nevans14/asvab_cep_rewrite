import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExcelService } from 'app/services/excel.service';
import { sectionReportType, StudentReportExcelModel, subHeaderColumnNameType } from 'app/core/models/studentReportExcelModel';
import { resolve } from 'url';
import { MentorService } from 'app/services/mentor.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-mentor-student-report-dialog',
  templateUrl: './mentor-student-report-dialog.component.html',
  styleUrls: ['./mentor-student-report-dialog.component.scss']
})
export class MentorStudentReportDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public reportOptionsFormGroup: FormGroup;
  public allStudentReportItems: string[];
  public selectedStudents: any[];

  public allStudentChecked: boolean;
  public allStudentScores: boolean;
  public allAdditionalProgramInfo: boolean;

  constructor(private _formBuilder: FormBuilder,
    public _dialogRef: MatDialogRef<MentorStudentReportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public _excelService: ExcelService,
    public _mentorService: MentorService,
    public _utilityService: UtilityService) {

    this.isSaving = false;
    this.isLoading = true;

    this.allStudentChecked = false;
    this.allStudentScores = false;
    this.allAdditionalProgramInfo = false;

    this.reportOptionsFormGroup = this._formBuilder.group({

    });

    this.allStudentReportItems = [];

  }

  ngOnInit() {

    this._dialogRef.updateSize("800px");

    this.getData().then(() => this.isLoading = false);
  }

  close() {
    this._dialogRef.close();
  }

  async getData() {

    this.selectedStudents = this.data.students;

  }

  setStudentCheck(value, field) {

    if (!value) {
      this.allStudentChecked = false;

      //remove existing record
      let removeIndex = this.allStudentReportItems.findIndex(x => x == field);
      if (removeIndex > -1) {
        this.allStudentReportItems.splice(removeIndex, 1);
      }

    } else {

      //add to existing record
      this.allStudentReportItems.push(field);
    }

  }


  getStudentInfoCheckValue(field): boolean {
    return (this.allStudentReportItems && this.allStudentReportItems.length > 0) ? this.allStudentReportItems.some(x => x == field) : false;
  }

  setAllStudentChecked(value) {
    this.allStudentChecked = value;

    if (this.allStudentChecked) {

      //add all report items if they don't exist
      if (!this.getStudentInfoCheckValue('firstName')) {
        this.allStudentReportItems.push('firstName');
      }

      if (!this.getStudentInfoCheckValue('lastName')) {
        this.allStudentReportItems.push('lastName');
      }

      if (!this.getStudentInfoCheckValue('studentId')) {
        this.allStudentReportItems.push('studentId');
      }

      if (!this.getStudentInfoCheckValue('username')) {
        this.allStudentReportItems.push('username');
      }

      if (!this.getStudentInfoCheckValue('grade')) {
        this.allStudentReportItems.push('grade');
      }

      if (!this.getStudentInfoCheckValue('graduation')) {
        this.allStudentReportItems.push('graduation');
      }

      if (!this.getStudentInfoCheckValue('gender')) {
        this.allStudentReportItems.push('gender');
      }

      if (!this.getStudentInfoCheckValue('ethnicity')) {
        this.allStudentReportItems.push('ethnicity');
      }

      if (!this.getStudentInfoCheckValue('schoolName')) {
        this.allStudentReportItems.push('schoolName');
      }

      if (!this.getStudentInfoCheckValue('schoolId')) {
        this.allStudentReportItems.push('schoolId');
      }

      if (!this.getStudentInfoCheckValue('schoolAddress')) {
        this.allStudentReportItems.push('schoolAddress');
      }

      if (!this.getStudentInfoCheckValue('schoolCode')) {
        this.allStudentReportItems.push('schoolCode');
      }

    }
  }

  setAllStudentScoresChecked(value) {
    this.allStudentScores = value;
    if (this.allStudentScores) {
      //add all report items if they don't exist
      if (!this.getStudentInfoCheckValue('accessCode')) {
        this.allStudentReportItems.push('accessCode');
      }

      if (!this.getStudentInfoCheckValue('accessCodeExpDate')) {
        this.allStudentReportItems.push('accessCodeExpDate');
      }

      if (!this.getStudentInfoCheckValue('testDate')) {
        this.allStudentReportItems.push('testDate');
      }

      if (!this.getStudentInfoCheckValue('asvabPercentileScores')) {
        this.allStudentReportItems.push('asvabPercentileScores');
      }

      if (!this.getStudentInfoCheckValue('asvabStandardScores')) {
        this.allStudentReportItems.push('asvabStandardScores');
      }

      if (!this.getStudentInfoCheckValue('afqt')) {
        this.allStudentReportItems.push('afqt');
      }

      if (!this.getStudentInfoCheckValue('militaryLinesScores')) {
        this.allStudentReportItems.push('militaryLinesScores');
      }

      if (!this.getStudentInfoCheckValue('testType')) {
        this.allStudentReportItems.push('testType');
      }

    }
  }

  setAllProgramInfoChecked(value) {
    this.allAdditionalProgramInfo = value;
    if (this.allAdditionalProgramInfo) {
      //add all report items if they don't exist
      if (!this.getStudentInfoCheckValue('combinedFYIScores')) {
        this.allStudentReportItems.push('combinedFYIScores');
      }

      if (!this.getStudentInfoCheckValue('genderSpecificFYIScores')) {
        this.allStudentReportItems.push('genderSpecificFYIScores');
      }

      if (!this.getStudentInfoCheckValue('workValues')) {
        this.allStudentReportItems.push('workValues');
      }

      if (!this.getStudentInfoCheckValue('favoriteOccupations')) {
        this.allStudentReportItems.push('favoriteOccupations');
      }

      if (!this.getStudentInfoCheckValue('favoriteColleges')) {
        this.allStudentReportItems.push('favoriteColleges');
      }

      if (!this.getStudentInfoCheckValue('favoriteCareerClusters')) {
        this.allStudentReportItems.push('favoriteCareerClusters');
      }

      if (!this.getStudentInfoCheckValue('favoriteCTMCareers')) {
        this.allStudentReportItems.push('favoriteCTMCareers');
      }

      if (!this.getStudentInfoCheckValue('postSecondaryIntentions')) {
        this.allStudentReportItems.push('postSecondaryIntentions');
      }

    }
  }

  async runReport() {
    try {
      this.isSaving = true;
      let studentData = await Promise.all(this.selectedStudents.map(async (student) => {
        let portfolioData = await this._mentorService.getStudentPortfolioReport(student);
        let asvabData = await this._mentorService.getStudentAsvabScores(student);
        let studentData = await this.getStudentInfo(student);
        studentData.portfolio = portfolioData;
        studentData.asvabInfo = asvabData;
        return studentData;
      }));

      //generate excel model data to pass to report
      if (studentData && studentData.length > 0) {

        let returnStudentReportExcelModels: StudentReportExcelModel[] = [];
        studentData.forEach((data: any) => {
          {

            /**
             * School Info section; if the value was selected add it to the array
             */
            if (this.getStudentInfoCheckValue('firstName')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.FIRST_NAME,
                value: (data.firstName) ? (data.firstName) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('lastName')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.LAST_NAME,
                value: (data.lastName) ? (data.lastName) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('studentId')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.STUDENT_ID,
                value: (data.studentId) ? (data.studentId) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('username')) {

              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.USERNAME,
                value: (data.emailAddress) ? (data.emailAddress) : "",
                mentorLinkId: data.mentorLinkId
              })

            }

            if (this.getStudentInfoCheckValue('grade')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.GRADE,
                value: (data.grade) ? (data.grade) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('graduation')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.GRADUATION,
                value: (data.administeredYear) ? (data.administeredYear + 1) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('gender')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.GENDER,
                value: (data.gender) ? (data.gender) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('ethnicity')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.ETHNICITY,
                value: (data.ethnicity) ? this.formatEthnicity(data.ethnicity) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('schoolName')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.SCHOOL,
                value: (data.schoolName) ? (data.schoolName) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('schoolAddress')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.SCHOOL_ADDRESS,
                value: (data.asvabInfo && data.asvabInfo.highSchoolInformation) ? this.formatHighSchoolAddress(data.asvabInfo.highSchoolInformation) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('schoolCode')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STUDENT_INFORMATION,
                columnName: subHeaderColumnNameType.SCHOOL_CODE,
                value: (data.asvabInfo && data.asvabInfo.highSchoolInformation) ? (data.asvabInfo.highSchoolInformation.schoolCode) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            /**
           * Student Scores section; if the value was selected add it to the array
           */
            if (this.getStudentInfoCheckValue('accessCode')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ASVAB_TEST_INFORMATION,
                columnName: subHeaderColumnNameType.ACCESS_CODE,
                value: (data.asvabInfo && data.asvabInfo.accessCodes && data.asvabInfo.accessCodes.accessCode) ?
                  (data.asvabInfo.accessCodes.accessCode) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('accessCodeExpDate')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ASVAB_TEST_INFORMATION,
                columnName: subHeaderColumnNameType.EXPIRATION_DATE,
                value: (data.asvabInfo && data.asvabInfo.accessCodes && data.asvabInfo.accessCodes.expireDate) ?
                  new Date(data.asvabInfo.accessCodes.expireDate).toLocaleDateString() : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('testDate')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ASVAB_TEST_INFORMATION,
                columnName: subHeaderColumnNameType.TEST_DATE,
                value: (data.administeredDay && data.administeredMonth && data.administeredYear) ?
                  data.administeredMonth + "/" + data.administeredDay + "/" + data.administeredYear : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('testType')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ASVAB_TEST_INFORMATION,
                columnName: subHeaderColumnNameType.TYPE_OF_TEST,
                value: (data.testType) ? (data.testType) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            //TODO NEED
            if (this.getStudentInfoCheckValue('asvabPercentileScores')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.PERCENTILE_SCORES,
                columnName: subHeaderColumnNameType.ASVAB_PERCENTILE_SCORES,
                value: (data.asvabInfo) ?
                  this.formatAsvabPercentileScores(data.asvabInfo, data.grade, data.gender) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('asvabStandardScores')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.STANDARD_SCORES,
                columnName: subHeaderColumnNameType.ASVAB_STANDARD_SCORES,
                value: (data.asvabScore) ? this.formatAsvabStandardScores(data.asvabScore) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('afqt')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.MILITARY_LINES_SCORES,
                columnName: subHeaderColumnNameType.AFQT,
                value: (data.asvabScore && data.asvabScore.afqtScore) ? (data.asvabScore.afqtScore) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('militaryLinesScores')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.MILITARY_LINES_SCORES,
                columnName: subHeaderColumnNameType.MILITARY_LINES_SCORES,
                value: (data.asvabInfo && data.asvabInfo.afqtRawComposites) ?
                  this.formatAFQTCompositeScores(data.asvabInfo.afqtRawComposites) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            /**
             * Additional Program INfo
             */

            if (this.getStudentInfoCheckValue('combinedFYIScores')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ADDITIONAL_PROGRAM_INFORMATION,
                columnName: subHeaderColumnNameType.COMBINED_FYI_SCORES,
                value: (data.fyiScores) ? this.formatCombinedFYIScores(data.fyiScores) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('genderSpecificFYIScores')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ADDITIONAL_PROGRAM_INFORMATION,
                columnName: subHeaderColumnNameType.GENDER_SPECIFIC_FYI_SCORES,
                value: (data.fyiScores) ? this.formatGenderFYIScores(data.fyiScores) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('workValues')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ADDITIONAL_PROGRAM_INFORMATION,
                columnName: subHeaderColumnNameType.WORK_VALUES,
                value: (data.workValueScores) ? this.formatWorkValueScores(data.workValueScores) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('postSecondaryIntentions')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.ADDITIONAL_PROGRAM_INFORMATION,
                columnName: subHeaderColumnNameType.POST_SECONDARY_INTENTIONS,
                value: (data.portfolio && data.portfolio.plans && data.portfolio.plans.plan) ?
                  this.formatIntentions(data.portfolio.plans.plan) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('favoriteOccupations')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.FAVORITES,
                columnName: subHeaderColumnNameType.FAVORITE_OCCUPATIONS,
                value: (data.portfolio && data.portfolio.favoriteOnetOccupations) ? this.formatFavoriteOnetOccupations(data.portfolio.favoriteOnetOccupations) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('favoriteColleges')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.FAVORITES,
                columnName: subHeaderColumnNameType.FAVORITE_COLLEGES,
                value: (data.portfolio && data.portfolio.favoriteColleges) ? this.formatFavoriteColleges(data.portfolio.favoriteColleges) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('favoriteCareerClusters')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.FAVORITES,
                columnName: subHeaderColumnNameType.FAVORITE_CAREER_CLUSTERS,
                value: (data.portfolio && data.portfolio.favoriteCareerClusters) ? this.formatCareerClusters(data.portfolio.favoriteCareerClusters) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

            if (this.getStudentInfoCheckValue('favoriteCTMCareers')) {
              returnStudentReportExcelModels.push({
                section: sectionReportType.FAVORITES,
                columnName: subHeaderColumnNameType.FAVORITE_CTM_CAREERS,
                value: (data.portfolio && data.portfolio.favoriteMilitaryCareers) ? this.formatCTMCareers(data.portfolio.favoriteMilitaryCareers) : "",
                mentorLinkId: data.mentorLinkId
              })
            }

          };

        })

        this._excelService.generateStudentReport(returnStudentReportExcelModels);
      }

      this.isSaving = false;

    } catch (error) {
      this.isSaving = false;
      console.error("ERROR", error);
    }

  }

  async getStudentInfo(student) {

    let studentInfo: any = await this._mentorService.getStudentDetails(student);

    return await studentInfo;

  }

  /**
   * returns: V 00, M 00, S&T 00
   * @param scores 
   */
  formatAsvabStandardScores(scores) {

    let returnString = "";

    if (scores.verbalScore) {
      returnString = "V " + scores.verbalScore;
    }

    if (scores.mathScore) {
      returnString = (returnString != "") ? returnString + ", M " + scores.mathScore : "V " + scores.mathScore;
    }

    if (scores.scienceScore) {
      returnString = (returnString != "") ? returnString + ", S&T " + scores.scienceScore : "S&T " + scores.scienceScore;
    }

    return returnString;
  }

  formatFavoriteOnetOccupations(occupations: any[]) {

    let returnMap = occupations.map((x: any) => {
      return x.title;
    });

    return returnMap.join(' \r\n');
  }

  formatFavoriteColleges(colleges: any[]) {
    let returnMap = colleges.map((x: any) => {
      return x.schoolName;
    });

    return returnMap.join(' \r\n');
  }

  formatCTMCareers(careers: any[]) {
    let returnMap = careers.map((x: any) => {
      return x.title;
    });

    return returnMap.join(' \r\n');
  }

  formatCareerClusters(careerClusters: any[]) {
    let returnMap = careerClusters.map((x: any) => {
      return x.title;
    });

    return returnMap.join(' \r\n');
  }

  formatAFQTCompositeScores(afqtRawComposites) {

    let armyValues = [];
    let marinesValues = [];
    let navyValues = [];
    let afValues = [];
    let cgValues = [];

    let returnString = "";

    armyValues.push("GT " + afqtRawComposites.gt_army);
    armyValues.push("CL " + afqtRawComposites.cl_army);
    armyValues.push("CO " + afqtRawComposites.co_army);
    armyValues.push("EL " + afqtRawComposites.el_army);
    armyValues.push("FA " + afqtRawComposites.fa_army);
    armyValues.push("GM " + afqtRawComposites.gm_army);
    armyValues.push("MM " + afqtRawComposites.mm_army);
    armyValues.push("OF " + afqtRawComposites.of_army);
    armyValues.push("SC " + afqtRawComposites.sc_army);
    armyValues.push("ST " + afqtRawComposites.st_army);
    returnString = "Army: " + armyValues.join(', ');

    marinesValues.push("MM " + afqtRawComposites.mm_mc);
    marinesValues.push("GT " + afqtRawComposites.gt_mc);
    marinesValues.push("EL " + afqtRawComposites.el_mc);
    marinesValues.push("CL " + afqtRawComposites.cl_mc);
    returnString += ' \r\n' + "Marine Corps: " + marinesValues.join(', ');

    navyValues.push("GT " + afqtRawComposites.gt_navy);
    navyValues.push("EL " + afqtRawComposites.el_navy);
    navyValues.push("BEE " + afqtRawComposites.bee_navy);
    navyValues.push("ENG " + afqtRawComposites.eng_navy);
    navyValues.push("MEC " + afqtRawComposites.mec_navy);
    navyValues.push("MEC2 " + afqtRawComposites.mec2_navy);
    navyValues.push("NUC " + afqtRawComposites.nuc_navy);
    navyValues.push("OPS " + afqtRawComposites.ops_navy);
    navyValues.push("HM " + afqtRawComposites.hm_navy);
    navyValues.push("ADM " + afqtRawComposites.adm_navy);
    returnString += ' \r\n' + "Navy: " + navyValues.join(', ');

    afValues.push("M " + afqtRawComposites.m_af);
    afValues.push("A " + afqtRawComposites.a_af);
    afValues.push("G " + afqtRawComposites.g_af);
    afValues.push("E " + afqtRawComposites.e_af);
    returnString += ' \r\n' + "Air Force: " + afValues.join(', ');

    //TODO Need Coast Guard data
    // cgValues.push("M " + afqtRawComposites.m_af);
    // cgValues.push("A " + afqtRawComposites.a_af);
    // cgValues.push("G " + afqtRawComposites.g_af);
    // cgValues.push("E " + afqtRawComposites.e_af);
    // returnString += ' \r\n' + "Coast Guard: " + cgValues.join(', ');
    return returnString;
  }

  formatAsvabPercentileScores(obj, grade, gender) {
    let returnString = "";
    var _grade = (!grade) ? 0 : grade;

    if(!obj.wordKnowledge || !obj.arithmeticReasoning || !obj.generalScience){
      return returnString;
    }

    let allValues = [];
    allValues.push("V " + obj.wordKnowledge.wk_COMP_Percentage);
    allValues.push("M " + obj.arithmeticReasoning.ar_COMP_Percentage);
    allValues.push("S&T " + obj.generalScience.gs_COMP_Percentage);
    returnString = _grade + "th Grade/students: " + allValues.join(', ');

    if (gender) {

      let isFemale = (gender.toUpperCase() == 'F' || gender.toUpperCase() == 'FEMALE') ? true : false;
      let femaleValues = [];
      femaleValues.push("V " + (isFemale) ? obj.wordKnowledge.wk_GS_Percentage : obj.arithmeticReasoning.wk_GOS_Percentage);
      femaleValues.push("M " + (isFemale) ? obj.arithmeticReasoning.ar_GS_Percentage : obj.arithmeticReasoning.ar_GOS_Percentage);
      femaleValues.push("S&T " + (isFemale) ? obj.generalScience.gs_GS_Percentage : obj.generalScience.gs_GOS_Percentage);
      returnString += ' \r\n' + _grade + "th Grade/Female: " + femaleValues.join(', ');

      let maleValues = [];
      maleValues.push("V " + (isFemale) ? obj.wordKnowledge.wk_GS_Percentage : obj.arithmeticReasoning.wk_GOS_Percentage);
      maleValues.push("M " + (isFemale) ? obj.arithmeticReasoning.ar_GS_Percentage : obj.arithmeticReasoning.ar_GOS_Percentage);
      maleValues.push("S&T " + (isFemale) ? obj.generalScience.gs_GS_Percentage : obj.generalScience.gs_GOS_Percentage);
      returnString += ' \r\n' + _grade + "th Grade/Male: " + maleValues.join(', ');
    }


    return returnString;
  }

  formatIntentions(intentions) {

    if (this._utilityService.isJson(intentions)) {

      let intentionsObj = JSON.parse(intentions);
      let returnArr = [];
      if (intentionsObj.fourYearCollege == true) {
        returnArr.push("4-Year College");
      };

      if (intentionsObj.twoYearCollege == true) {
        returnArr.push("2-Year College");
      };

      if (intentionsObj.careerAndTech == true) {
        returnArr.push("Career and Technical Education");
      };

      if (intentionsObj.military == true) {
        returnArr.push("Military");
      };

      if (intentionsObj.work == true) {
        returnArr.push("Work");
      };

      if (intentionsObj.gapYear == true) {
        returnArr.push("Gap Year");
      };

      if (intentionsObj.undecided == true) {
        returnArr.push("Undecided");
      };

      if (intentionsObj.certificate == true) {
        returnArr.push("Certificate");
      };

      if (intentionsObj.technicalSchool == true) {
        returnArr.push("Technical School");
      };

      if (intentionsObj.vocationalTraining == true) {
        returnArr.push("Vocational Training");
      };

      if (intentionsObj.advancedDegree == true) {
        returnArr.push("Advanced Degree");
      };

      return returnArr.join(' \r\n');

    }

    return "";
  }

  formatEthnicity(ethnicity) {

    let returnArr = [];
    if (ethnicity.africanAmerican == true) {
      returnArr.push("African American");
    }
    if (ethnicity.americanIndian == true) {
      returnArr.push("Native American");
    }
    if (ethnicity.asian == true) {
      returnArr.push("Asian");
    }
    if (ethnicity.hispanic == true) {
      returnArr.push("Hispanic");
    }
    if (ethnicity.nativeHawaiian == true) {
      returnArr.push("Native Hawaiian");
    }
    if (ethnicity.notHispanic == true) {
      returnArr.push("non-Hispanic");
    }
    if (ethnicity.white == true) {
      returnArr.push("Caucasian");
    }

    return returnArr.join(', ');

  }

  formatHighSchoolAddress(school) {
    let returnString = "";
    if (school.schoolCityName) {
      returnString = school.schoolCityName;
    }

    if (school.schoolStateName) {
      returnString = (returnString != "") ? returnString + ", " + school.schoolStateName : school.schoolStateName;
    }

    return returnString;
  }

  formatGenderFYIScores(scores: any[]) {

    let returnStr = "";

    if (scores.length > 0) {

      let sortedScores = scores.sort((a, b) => b.genderScore - a.genderScore);
      returnStr = sortedScores.map((x: any) => {
        return this.getInterestCode(x.interestCd) + " " + (parseFloat(x.genderScore) * 100).toFixed(0);
      }).join(", ");
    }

    return returnStr;
  }

  formatCombinedFYIScores(scores: any[]) {
    let returnStr = "";

    if (scores.length > 0) {

      let sortedScores = scores.sort((a, b) => b.combinedScore - a.combinedScore);
      returnStr = sortedScores.map((x: any) => {
        return this.getInterestCode(x.interestCd) + " " + (parseFloat(x.combinedScore) * 100).toFixed(0);
      }).join(", ");
    }

    return returnStr;
  }

  formatWorkValueScores(workValueScores: any[]) {
    let returnStr = "";

    if (workValueScores.length > 0) {

      let sortedScores = workValueScores.sort((a, b) => b.score - a.score);
      returnStr = sortedScores.map((x: any) => {
        return x.type + " " + x.score;
      }).join(", ");
    }

    return returnStr;
  }

  getInterestCode(interestCd) {
    if (interestCd != null) {
      switch (interestCd.toUpperCase()) {
        case "R":
          return "Realistic";
        case "I":
          return "Investigative";
        case "A":
          return "Artistic";
        case "S":
          return "Social";
        case "E":
          return "Enterprising";
        case "C":
          return "Conventional";
      }
    }
  };

}
