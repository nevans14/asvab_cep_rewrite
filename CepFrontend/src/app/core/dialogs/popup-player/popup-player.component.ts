import { Component, OnInit, AfterContentInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-popup-player',
    templateUrl: './popup-player.component.html',
    styleUrls: ['./popup-player.component.scss']
})

export class PopupPlayerComponent implements OnInit, AfterContentInit {
    videoId: string;
    youTube = false;
    videoURL: string;

    constructor(
        public dialogRef: MatDialogRef<PopupPlayerComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private sanitizer: DomSanitizer,
    ) { }

    ngOnInit() {
        if (/Mobi|Android/i.test(navigator.userAgent)) {
            this.dialogRef.updateSize('80%');
        }
    }

    ngAfterContentInit() {
        this.videoId = this.data.videoId;
        const videoIdLowerCase = this.videoId.toLowerCase();
        if (videoIdLowerCase.indexOf('http') === -1) {
            this.videoId = 'https://www.youtube.com/embed/' + this.videoId;
        }
        if (this.videoId.indexOf('www.youtube.com/embed/') > -1) {
            if (videoIdLowerCase.indexOf('?') > -1) {
                const params = this.videoId.substr(videoIdLowerCase.indexOf('?'));
                this.videoURL = this.videoId.replace(params, '?rel=0; autoplay=1&mute=1');
            } else {
                this.videoURL = this.videoId + '?autoplay=1&mute=1';
            }
            this.youTube = true;
        } else {
            this.videoURL = this.videoId;
            this.youTube = false;
        }
    }

    safeURL(url: string): SafeResourceUrl {
        console.debug(this.sanitizer.bypassSecurityTrustResourceUrl(url));
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    closeDialog(): void {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
