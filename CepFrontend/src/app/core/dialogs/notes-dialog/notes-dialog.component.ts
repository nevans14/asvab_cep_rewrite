import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotesService } from 'app/services/notes.service';
import { SchoolNotesService } from 'app/services/school-notes.service';
import { CareerClusterNotesService } from 'app/career-cluster/services/career-cluster-notes.service';
import { faUserInjured } from '@fortawesome/free-solid-svg-icons';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.scss']
})
export class NotesDialogComponent implements OnInit {

  view = {
    'Occupation': 1,
    'School'	: 2,
    'CC'		: 3
  }

  viewSetting = {
    showOccupationNotes: false,
    showSchoolNotes: false,
    showCCNotes: false,
  };

  noteList = [];
  noteListSchool = [];
  noteListCC = [];

  tempSocId;
  tempUnitId;
  tempCcId;
  tempSocTitle;
  tempSchoolName;
  tempCcTitle;

  socId;
  unitId;
  ccId;
  title;
  schoolName;
  ccTitle;
  
  initialNotesObject= {
    socId: null,
    title: null,
    notes: null,
    userId: null,
    noteId: null,
  };
  selectedNotesObject = {
    socId: null,
    title: null,
    notes: null,
    userId: null,
    noteId: null,
  };

  error = '';
  savingNotes = false;
  deletingNote = false;
  isPerformingAction = false;

  initialSchoolNotesObject = {
    unitId: null,
    schoolName: null,
    noteId: null,
    userId: null,
    notes: null,
  };
  selectedSchoolNotesObject= {
    unitId: null,
    schoolName: null,
    noteId: null,
    userId: null,
    notes: null,
  };

  initialCCNotesObject = {
    ccId: null,
    title: null,
    notes: null,
    noteId: null,
    userId: null,
  };
  selectedCCNotesObject = {
    ccId: null,
    title: null,
    notes: null,
    noteId: null,
    userId: null,
  };

  constructor(
    private _notesService: NotesService,
    private _schoolNotesService: SchoolNotesService,
    private _careerClusterNotesService: CareerClusterNotesService,
    public _dialogRef: MatDialogRef<NotesDialogComponent>,
    private _userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    this.socId = data.socId;
    this.unitId = data.unitId;
    this.ccId = data.ccId;
    this.title = data.title;
    this.schoolName = data.schoolName;
    this.ccTitle = data.ccTitle;
  }

  ngOnInit() {
    if (this.unitId != undefined) {
      this.viewSetting.showSchoolNotes = true;
      this.viewSetting.showOccupationNotes = this.viewSetting.showCCNotes = false;					
    } else if (this.ccId != undefined) { 
      this.viewSetting.showCCNotes = true;
      this.viewSetting.showOccupationNotes = this.viewSetting.showSchoolNotes = false;
    } else {
      this.viewSetting.showOccupationNotes = true;
      this.viewSetting.showSchoolNotes = this.viewSetting.showCCNotes = false;					
    }
    
    this.initializeSetup();
    this.initializeSchoolSetup();
    this.initializeCCSetup();

    this._userService.setCompletion(UserService.VISITED_NOTES, 'true');
  }

  toggleView(view) {
    switch (view) {
      case this.view.Occupation:
        this.viewSetting.showOccupationNotes = true;
        this.viewSetting.showSchoolNotes = this.viewSetting.showCCNotes = false;
        break;
      case this.view.School:
        this.viewSetting.showSchoolNotes = true;
        this.viewSetting.showOccupationNotes = this.viewSetting.showCCNotes = false;
        break;
      case this.view.CC:
        this.viewSetting.showCCNotes = true;
        this.viewSetting.showSchoolNotes = this.viewSetting.showOccupationNotes = false;
        break;
      default:
        this.viewSetting.showOccupationNotes = true;
        this.viewSetting.showSchoolNotes = this.viewSetting.showCCNotes = false;
        break;
    }
  };

  displayNoteList() {
    return this.noteList.filter(
      item => this.initialNotesObject
        && this.initialNotesObject.socId
        && this.initialNotesObject.socId != item.socId
    )
  }

  displayNoteListSchool() {
    return this.noteListSchool.filter(
      item => this.initialSchoolNotesObject
        && this.initialSchoolNotesObject.unitId
        && this.initialSchoolNotesObject.unitId != item.unitId
    )
  }

  displayNoteListCC() {
    return this.noteListCC.filter(
      item => this.initialCCNotesObject
        && this.initialCCNotesObject.ccId
        && this.initialCCNotesObject.ccId != item.ccId
    )
  }

  hasAnyNotes() {
    return this.noteList.length > 0 ||
      this.noteListSchool.length > 0 ||
      this.noteListCC.length > 0;
  }

  setView() {
    if (this.noteList.length > 0)
      this.toggleView(this.view.Occupation);
    if (this.noteListSchool.length > 0)
      this.toggleView(this.view.School);
    if (this.noteListCC.length > 0)
      this.toggleView(this.view.CC);
  }
  
  initializeSetup() {
    let that = this;
    this._notesService.getNotes().then(function (data: []) {
      // set note list to the results
      that.noteList = [...data];
      // if on the dashboard, assign the first SOC as the title/socid
      if (that.socId === undefined && that.noteList.length > 0) {
        that.tempSocId = that.noteList[0].socId;
        that.tempSocTitle = that.noteList[0].title;
      } else if (that.noteList.length === 0) {
        that.tempSocId = undefined;
        that.tempSocTitle = undefined;
      }
      // set default
      that.initialNotesObject = that.selectedNotesObject = {
        socId	: (that.tempSocId ? that.tempSocId : that.socId),
        title	: (that.tempSocTitle ? that.tempSocTitle : that.title),
        notes	: undefined,
        userId: undefined,
        noteId: undefined,
      };
      // if list has data, assign the initial/selected notes object (if applicable)
      for (var i = 0; i < that.noteList.length; i++) {
        if (that.noteList[i].socId === that.selectedNotesObject.socId) {
          that.initialNotesObject = that.selectedNotesObject = {
            userId	: that.noteList[i].userId,
            socId	: that.noteList[i].socId,
            title	: that.noteList[i].title,
            notes	: that.noteList[i].notes,
            noteId	: that.noteList[i].noteId
          };
          break;
        }
      }						
    })
  }

  resetObject() {
    this.initialNotesObject = this.selectedNotesObject = {
      socId	: (this.tempSocId ? this.tempSocId : this.socId),
      title	: (this.tempSocTitle ? this.tempSocTitle : this.title),
      notes	: undefined,
      userId: undefined,
      noteId: undefined,
    };

    // set notes if saved previously
    for (var i = 0; i < this.noteList.length; i++) {
      if (this.noteList[i].socId === this.selectedNotesObject.socId) {
        this.initialNotesObject = this.selectedNotesObject = {
          userId	: this.noteList[i].userId,
          socId	: this.noteList[i].socId,
          title	: this.noteList[i].title,
          notes	: this.noteList[i].notes,
          noteId	: this.noteList[i].noteId
        };
      }
    }
  }

  save() {
    this.error = '';
    this.savingNotes = true;
    this.isPerformingAction = true;
    var notesExist = false;

    if (this.selectedNotesObject.notes == undefined || this.selectedNotesObject.notes == '') {
      this.error = 'Note is blank';
      this.savingNotes = false;
      this.isPerformingAction = false;
      return false;
    }

    // check to see if notes was already saved
    for (var i = 0; i < this.noteList.length; i++) {
      if (this.noteList[i].socId == this.selectedNotesObject.socId) {
        notesExist = true;
      }
    }

    // if notes were never saved then save notes to database,
    // otherwise update notes
    let that = this;
    if (!notesExist) {
      this._notesService.insertNote(this.selectedNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    } else {
      this._notesService.updateNote(this.selectedNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    }
  };

  deleteNote() {
    var isConfirmed = confirm('Do you want to remove this note?');
    if (!isConfirmed) return;
    this.error = '';
    this.deletingNote = true;
    this.isPerformingAction = true;
    
    let that = this;
    this._notesService.deleteNote(this.selectedNotesObject.noteId).then(function(success: []) {
      that.noteList = [...success];
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.initializeSetup();
      // determine which view to see 
      if (that.hasAnyNotes()) {
        // if the current note list is empty, select a different view
        if (that.noteList.length === 0)
          that.setView();
      } else {
        that.close();
      }
    }, function (error) {
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.error = 'There was an error processing your request. Please try again.';
    })
  }

	selectOccupation(noteId) {
    // set the information to display to the user depending on
    // their selection
    for (var i = 0; i < this.noteList.length; i++) {
      if (this.noteList[i].noteId == noteId) {
        this.selectedNotesObject = {
          userId	: this.noteList[i].userId,
          socId	: this.noteList[i].socId,
          title	: this.noteList[i].title,
          notes	: this.noteList[i].notes,
          noteId	: this.noteList[i].noteId
        };
      }
    }
  }

  initializeSchoolSetup() {
    let that = this;
    this._schoolNotesService.getNotes().then(function (data: []) {
      // set school notes from results (db/localstorage)
      that.noteListSchool = [...data];
      // assign default unitId/schoolName if on dashboard
      if (that.unitId === undefined && that.noteListSchool.length > 0) {
        that.tempUnitId = that.noteListSchool[0].unitId;
        that.tempSchoolName = that.noteListSchool[0].schoolName;
      } else if (that.noteListSchool.length === 0) {
        that.tempUnitId = undefined;
        that.tempSchoolName = undefined;
      }
      // initialize initial/selected school notes object
      that.initialSchoolNotesObject = that.selectedSchoolNotesObject = {
        unitId		: (that.tempUnitId ? that.tempUnitId : that.unitId),
        schoolName	: (that.tempSchoolName ? that.tempSchoolName : that.schoolName),
        notes		: undefined,
        userId  : undefined,
        noteId  : undefined,
      };
      // assign initial/selected school notes object, if applicable
      for (var i = 0; i < that.noteListSchool.length; i++) {
        if (that.noteListSchool[i].unitId === parseInt(that.selectedSchoolNotesObject.unitId)) {
          that.initialSchoolNotesObject = that.selectedSchoolNotesObject = {
            userId		: that.noteListSchool[i].userId,
            unitId		: that.noteListSchool[i].unitId,
            schoolName	: that.noteListSchool[i].schoolName,
            notes		: that.noteListSchool[i].notes,
            noteId		: that.noteListSchool[i].noteId
          }
        }
      }
    })
  }

  resetSchoolObject() {
    this.initialSchoolNotesObject = this.selectedSchoolNotesObject = {
      unitId: (this.tempUnitId ? this.tempUnitId : this.unitId),
      schoolName: (this.tempSchoolName ? this.tempSchoolName : this.schoolName),
      notes: undefined,
      noteId: undefined,
      userId: undefined,
    };

    // set notes if saved previously
    for (var i = 0; i < this.noteListSchool.length; i++) {
      if (this.noteListSchool[i].unitId == this.selectedSchoolNotesObject.unitId) {
        this.initialSchoolNotesObject = this.selectedSchoolNotesObject = {
          unitId		: this.noteListSchool[i].unitId,
          schoolName	: this.noteListSchool[i].schoolName,
          notes		: this.noteListSchool[i].notes,
          noteId		: this.noteListSchool[i].noteId,
          userId: undefined,
        };
      }
    }
  }

  saveSchool() {
    this.error = '';
    this.savingNotes = true;
    this.isPerformingAction = true;
    var notesExist = false;

    if (this.selectedSchoolNotesObject.notes == undefined || this.selectedSchoolNotesObject.notes == '') {
      this.error = 'Note is blank';
      this.savingNotes = false;
      this.isPerformingAction = false;
      return false;
    }

    // check to see if notes was already saved
    for (var i = 0; i < this.noteListSchool.length; i++) {
      if (this.noteListSchool[i].unitId == this.selectedSchoolNotesObject.unitId) {
        notesExist = true;
      }
    }
    // if notes were never saved then save notes to database,
    // otherwise update notes
    let that = this;
    if (!notesExist) {
      this._schoolNotesService.insertNote(this.selectedSchoolNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    } else {
      that._schoolNotesService.updateNote(that.selectedSchoolNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    }
  };
  
  deleteSchoolNote() {
    var isConfirmed = confirm('Do you want to remove this note?');
    if (!isConfirmed) return;
    this.error = '';					
    this.deletingNote = true;
    this.isPerformingAction = true;
    let that = this;
    this._schoolNotesService.deleteNote(this.selectedSchoolNotesObject.noteId).then(function (success: []) {
      that.noteListSchool = [...success];
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.initializeSchoolSetup();
      // determine which view to see 
      if (that.hasAnyNotes()) {
        if (that.noteListSchool.length === 0)
          that.setView();
      } else {
        // close modal
        that.close();
      }
    }, function (error) {
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.error = 'There was an error processing your request. Please try again.';						
    });
  };

  /**
   * Within the modal, user can toggle between schools that
   * they saved notes for.
   */
  selectSchool(noteId) {

    // set the information to display to the user depending on
    // their selection
    for (var i = 0; i < this.noteListSchool.length; i++) {
      if (this.noteListSchool[i].noteId == noteId) {
        this.selectedSchoolNotesObject = {
          unitId		: this.noteListSchool[i].unitId,
          schoolName	: this.noteListSchool[i].schoolName,
          notes		: this.noteListSchool[i].notes,
          noteId		: this.noteListSchool[i].noteId,
          userId  : undefined,
        };
      }
    }
  }

  

  initializeCCSetup() {
    let that = this;
    this._careerClusterNotesService.getCareerClusterNotes().then(function (data: []) {
      // set notes for career clusters from result (db/localstorage)
      that.noteListCC = [...data];
      // assign default ccId/title if on dashboard
      if (that.ccId === undefined && that.noteListCC.length > 0) {
        that.tempCcId = that.noteListCC[0].ccId;
        that.tempCcTitle = that.noteListCC[0].title;
      } else if (that.noteListCC.length === 0) {
        that.tempCcId = undefined;
        that.tempCcTitle = undefined;
      }
      // assign initial/selected notes object
      that.initialCCNotesObject = that.selectedCCNotesObject = {
        ccId	: (that.tempCcId ? that.tempCcId : that.ccId),
        title	: (that.tempCcTitle ? that.tempCcTitle : that.ccTitle),
        notes	: undefined,
        userId: undefined,
        noteId: undefined,
      };						
      // set notes if saved previously
      for (var i = 0; i < that.noteListCC.length; i++) {
        if (that.noteListCC[i].ccId === that.selectedCCNotesObject.ccId) {
          that.initialCCNotesObject = that.selectedCCNotesObject = {
            userId	: that.noteListCC[i].userId,
            ccId	: that.noteListCC[i].ccId,
            title	: that.noteListCC[i].title,
            notes	: that.noteListCC[i].notes,
            noteId	: that.noteListCC[i].noteId
          };
        }
      }
    });
  }
  
  /**
   * Reset to current occupation object.
   */
  resetCCObject() {
    this.initialCCNotesObject = this.selectedCCNotesObject = {
      ccId	: (this.tempCcId ? this.tempCcId : this.ccId),
      title	: (this.tempCcTitle ? this.tempCcTitle : this.ccTitle),
      notes	: undefined,
      noteId : undefined,
      userId : undefined,
    };

    // set notes if saved previously
    for (var i = 0; i < this.noteListCC.length; i++) {
      if (this.noteListCC[i].ccId === this.selectedCCNotesObject.ccId) {
        this.initialCCNotesObject = this.selectedCCNotesObject = {
          userId	: this.noteListCC[i].userId,
          ccId	: this.noteListCC[i].ccId,
          title	: this.noteListCC[i].title,
          notes	: this.noteListCC[i].notes,
          noteId	: this.noteListCC[i].noteId
        };
      }
    }
  }
  
  /**
   * Save Notes
   */
  saveCC() {
    this.error = '';
    this.savingNotes = true;
    var notesExist = false;
    this.isPerformingAction = true;

    if (this.selectedCCNotesObject.notes == undefined || this.selectedCCNotesObject.notes == '') {
      this.error = 'Note is blank';
      this.savingNotes = false;
      this.isPerformingAction = false;
      return false;
    }

    // check to see if notes was already saved
    for (var i = 0; i < this.noteListCC.length; i++) {
      if (this.noteListCC[i].ccId == this.selectedCCNotesObject.ccId) {
        notesExist = true;
        break;
      }
    }
    // if notes were never saved then save notes to database,
    // otherwise update notes
    let that = this;
    if (!notesExist) {
      this._careerClusterNotesService.insertCareerClusterNote(this.selectedCCNotesObject).then(function(success) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.isPerformingAction = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    } else {
      that._careerClusterNotesService.updateCareerClusterNote(that.selectedCCNotesObject).then(function(success) {
        that.savingNotes = false;
        that.close();
      }, function(error) {
        that.savingNotes = false;
        that.error = "There was an error proccessing your request. Please try again."
        return false
      });
    }
  };
  
  deleteCCNote() {
    var isConfirmed = confirm('Do you want to remove this note?');
    if (!isConfirmed) return;
    this.error = '';					
    this.deletingNote = true;
    this.isPerformingAction = true;
    let that = this;
    this._careerClusterNotesService.deleteNote(this.selectedCCNotesObject.noteId).then(function (success: []) {
      that.noteListCC = [...success];
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.initializeCCSetup();
      // determine which view to see 
      if (that.hasAnyNotes()) {
        if (that.noteListCC.length === 0)
          that.setView();
      } else {
        // close modal
        that.close();
      }
    }, function (error) {
      that.deletingNote = false;
      that.isPerformingAction = false;
      that.error = 'There was an error processing your request. Please try again.';						
    });
  };
  
  /**
   * Within the modal, user can toggle between occupations that
   * they saved notes for.
   */
  selectCC(noteId) {

    // set the information to display to the user depending on
    // their selection
    for (var i = 0; i < this.noteListCC.length; i++) {
      if (this.noteListCC[i].noteId == noteId) {
        this.selectedCCNotesObject = {
          ccId	: this.noteListCC[i].ccId,
          title	: this.noteListCC[i].title,
          notes	: this.noteListCC[i].notes,
          noteId	: this.noteListCC[i].noteId,
          userId : undefined,
        };
      }
    }
  }

  close() {
    // $uibModalInstance.dismiss('cancel');
    this._dialogRef.close();
  }
}
