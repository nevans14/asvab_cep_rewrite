import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSchoolsDialogComponent } from './search-schools-dialog.component';

describe('SearchSchoolsDialogComponent', () => {
  let component: SearchSchoolsDialogComponent;
  let fixture: ComponentFixture<SearchSchoolsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSchoolsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSchoolsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
