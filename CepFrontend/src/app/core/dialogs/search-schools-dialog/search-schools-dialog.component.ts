import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SchoolProfile } from 'app/core/models/schoolProfile.model';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-search-schools-dialog',
  templateUrl: './search-schools-dialog.component.html',
  styleUrls: ['./search-schools-dialog.component.scss']
})
export class SearchSchoolsDialogComponent implements OnInit {

  private userOccupationPlan: UserOccupationPlan;
  private selectedSchools: any[];
  private filteredSchoolProfiles: any[];
  private schoolProfiles: any[];
  private occupationDescription: string;
  public selectedPathways: any;

  // filter
  private stateFilter: any;
  private sort: any;

  // pagination
  public totalItems: number;
  public currentPage: number;
  public maxSize: number;
  public itemsPerPage: number;

  constructor(public _dialogRef: MatDialogRef<SearchSchoolsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _utilityService: UtilityService) {
    this.selectedSchools = data.selectedSchools;
    this.userOccupationPlan = data.userOccupationPlan;
    this.occupationDescription = data.occupationDescription;
    this.selectedPathways = data.selectedPathways;
  }

  ngOnInit() {
    //retrieve schools and init pagination
    this.getSchools();
  }

  getSchools() {
    let self = this;
    this._occufindRestFactoryService.getSchoolMoreDetails(this.userOccupationPlan.socId).toPromise()
      .then((schoolMoreDetails: any[]) => {

        //only allow selection of colleges based on pathway
        let schoolsToShow: any[] = [];

        //filter through and only show favorite schools based on the pathway selected
        if (schoolMoreDetails) {
          schoolMoreDetails.forEach((school: any) => {

            if (this._utilityService.containsPathways(this.selectedPathways, 'certificate', 'technicalSchool', 'vocationalTraining', 'careerTechnicalEducation')) {
              if (school.certificate) {
                schoolsToShow.push(school);
                return;
              }
            }
            if (this._utilityService.containsPathways(this.selectedPathways, 'associatesDegree', 'technicalSchool', 'vocationalTraining', 'careerTechnicalEducation')) {
              if (school.associates) {
                schoolsToShow.push(school);
                return;
              }
            }
            //only include military when they are interested in becoming an officer
            if (this._utilityService.containsPathways(this.selectedPathways, 'bachelorsDegree', 'military')) {
              if (school.bachelors) {
                schoolsToShow.push(school);
                return;
              }
            }
            if (this._utilityService.containsPathways(this.selectedPathways, 'advanceDegree')) {
              if (school.doctoral || school.masters) {
                schoolsToShow.push(school);
                return;
              }
            }
          });
        }

        //only allow selection of colleges that have not already been selected
        this.schoolProfiles = schoolsToShow.filter(function (school: any) {
          if (!self.selectedSchools.find(x => x.unitId == school.unitId)) {
            return school;
          }
        });

        this.initPagination(this.schoolProfiles);
      });
  }

  initPagination(schoolProfiles: any[]) {
    this.totalItems = schoolProfiles.length;
    this.filteredSchoolProfiles = schoolProfiles;
    this.currentPage = 1;
    this.maxSize = 4;
    this.itemsPerPage = 5;
    this.stateFilter = "";
    this.sort = "schoolName";
  }

  onStateFilterSelect(event) {
    const value = event.target.value;
    this.stateFilter = value;

    this.filteredSchoolProfiles = this.applyStateFilter(this.schoolProfiles, value);
    this.totalItems = this.filteredSchoolProfiles.length;
  }

  onSortChange(event) {
    const value = event.target.value;
    this.sort = value;

    this.filteredSchoolProfiles = this.applySort(this.filteredSchoolProfiles, value);
  }

  applyStateFilter(schoolDetails, value): any[] {
    if ((!value) || (value === '')) {
      return schoolDetails;
    } else {
      return schoolDetails.filter(school => school.state === value);
    }
  }

  applySort(schoolDetails, value): any[] {

    if (value.charAt(0) === '-') {
      schoolDetails.sort(this.compareValues(value.slice(1), 'desc'));
    } else {
      schoolDetails.sort(this.compareValues(value));
    }

    return schoolDetails;
  }

  /**
   * returns a sorted array based on the key and order passed
   * @param key 
   * @param order can be 'asc' or 'desc'; 'asc' is by default
   */
  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0;  //property doesn't exist

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if ((typeof varA === 'string') && (typeof varB === 'string')) {
        comparison = varA.localeCompare(varB);
      } else {
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
      }

      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  /**
   * Closes the dialog, and returns the unit id to be saved
   * @param unitId the selected schools unit id
   */
  selectSchool(unitId) {
    this._dialogRef.close(unitId);
  }

}
