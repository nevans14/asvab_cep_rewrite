import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
  selector: 'app-contact-recruiter-dialog',
  templateUrl: './contact-recruiter-dialog.component.html',
  styleUrls: ['./contact-recruiter-dialog.component.scss']
})
export class ContactRecruiterDialogComponent implements OnInit {


  serviceFormGroup: FormGroup;
  service;
  userType: String = "";
  userTypeOptions = [
    { id: 1, name: "---Select One---", value: "" },
    { id: 2, name: "Student", value: "Student" },
    { id: 3, name: "Parent", value: "Parent" },
    { id: 4, name: "Educator", value: "Educator" },
    { id: 5, name: "Other", value: "Other" }
  ];
  firstName;
  lastName;
  email;
  message;
  city;
  state;
  zip;
  contactType = 1;
  asvabParticipant;
  formDisabled = false;
  recaptchaResponse = '';
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  validation;

  constructor(public dialogRef: MatDialogRef<ContactRecruiterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private _httpHelper: HttpHelperService,
    private _dialog: MatDialog,
    private _configService: ConfigService,
  ) { }

  ngOnInit() {
    this.service = this.data;
    this.serviceFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }

  close() {
    this.dialogRef.close();
  }

  emailUs = function () {
    this.validation = undefined;
    this.formDisabled = true;
    var emailObject = {
      userType: this.userType,
      asvabParticipant: this.asvabParticipant ? 'Yes' : 'No',
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      message: this.message,
      city: this.city,
      state: this.state,
      zip: this.zip,
      contactType: this.contactType,
      recaptchaResponse: this.serviceFormGroup.value.recaptcha,
      svcList: [this.service.serviceCode],
      questionType: 1,
    }

    if (this.userType == undefined || this.asvabParticipant == undefined || this.firstName == undefined || this.lastName == undefined || this.email == undefined || this.message == undefined) {
      this.validation = "Fill out all fields.";
      this.formDisabled = false;
      return false;
    }

    if (this.serviceFormGroup.value.recaptcha === "") {
      this.validation = "Please resolve the captcha and submit!";
      this.formDisabled = false;
      return false;
    } else {
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactus/service-contact-us/`);
      this._httpHelper.httpHelper('POST', fullUrl, null, emailObject, true)
        .then(
          (val) => {
            this.userType = undefined;
            this.asvabParticipant = undefined;
            this.firstName = undefined;
            this.lastName = undefined;
            this.email = undefined;
            this.message = undefined;
            this.city = undefined;
            this.state = undefined;
            this.zip = undefined;
            this.recaptchaResponse = '';
            this.formDisabled = false;

            const data = {
              'title': 'Contact Us',
              'message': '<p>Thanks for contacting us!</p>'
            };

            this._dialog.open(MessageDialogComponent, {
              data: data,
              hasBackdrop: true,
              disableClose: true,
              minWidth: '400px',
              autoFocus: true
            });
          },
          (err) => {

            var errorMessage = '';

            if (err === 'Server error') {
              errorMessage = err;
            } else {
              for (const property in err) {
                errorMessage += `${err[property]}` + '<br>';
              }
            }

            const data = {
              'title': 'Error',
              'message': errorMessage
            };

            this._dialog.open(MessageDialogComponent, {
              data: data,
              hasBackdrop: true,
              disableClose: true,
              minWidth: '400px',
              autoFocus: true
            });
            this.formDisabled = false;
          }
        );

    }

  }

  openNewTab(url) {
    window.open(url, '_blank');
  }

}
