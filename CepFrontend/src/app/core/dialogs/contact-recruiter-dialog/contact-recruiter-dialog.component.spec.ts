import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactRecruiterDialogComponent } from './contact-recruiter-dialog.component';

describe('ContactRecruiterDialogComponent', () => {
  let component: ContactRecruiterDialogComponent;
  let fixture: ComponentFixture<ContactRecruiterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactRecruiterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactRecruiterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
