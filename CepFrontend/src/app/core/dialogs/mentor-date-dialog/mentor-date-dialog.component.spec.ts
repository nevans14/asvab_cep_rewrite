import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorDateDialogComponent } from './mentor-date-dialog.component';

describe('MentorDateDialogComponent', () => {
  let component: MentorDateDialogComponent;
  let fixture: ComponentFixture<MentorDateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorDateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorDateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
