import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-mentor-date-dialog',
  templateUrl: './mentor-date-dialog.component.html',
  styleUrls: ['./mentor-date-dialog.component.scss']
})
export class MentorDateDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public dateFormGroup: FormGroup;
  public mentorLinkIds: string[];

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<MentorDateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _planCalendarService: PlanCalendarService,
    private _matDialog: MatDialog,
    private _userService: UserService) {

    this.isSaving = false;
    this.isLoading = true;
    this.dateFormGroup = this._formBuilder.group({
      id: [''],
      title: ['', [Validators.required, Validators.maxLength(500)]],
      date: ['', [Validators.required]]
    });
    this.mentorLinkIds = [];

  }

  ngOnInit() {
    this.isLoading = false;

    this.mentorLinkIds = (this.data.mentorLinkIds) ? this.data.mentorLinkIds : null;

  }

  async saveDateFormGroup() {

    this.isSaving = true;

    if (this.mentorLinkIds && this.mentorLinkIds.length > 0) {

      await Promise.all(this.mentorLinkIds.map((mentorLinkId: string) => {

        let formData: DirectMessage = {
          id: null, //handled on server
          messageTypeId: messageType.CALENDAR_MESSAGE,
          message: this.dateFormGroup.value.title,
          dateCreated: null, //handled on server
          fromUser: null, //handled on server
          toUser: null, //handled on server
          mentorLinkId: mentorLinkId,
          hasBeenRead: false,
          calendarDate: this.dateFormGroup.value.date,
          dateUpdated: null  //handled on server
        }

        this._userService.sendDirectMessage(formData).then((result: DirectMessage) => {
          return Promise.resolve(result);
        }).catch((error: any) => {
          return Promise.reject("error");
        })

      })).then((done: any) => {
        this.isSaving = false;
        this.dialogRef.close();
      }).catch((error: any) => {
        console.error("ERROR", error);
      });

    } else {
      let formData: UserOccupationPlanCalendar = {
        id: null,
        planId: null,
        title: this.dateFormGroup.value.title,
        date: this.dateFormGroup.value.date,
        isGraduationDate: false,
        tasks: null
      }

      //save calendar not to a plan and all of its tasks if any
      //TODO update for mentor calendar save
      this._planCalendarService.saveCalendar(formData).then((newRecord: UserOccupationPlanCalendar) => {
        this.isSaving = false;
        this.dialogRef.close(newRecord);
      }).catch((error: any) => {
        this.isSaving = false;
        console.error("ERROR", error);
      });
    }

  }

  close() {
    this.dialogRef.close();
  }

}
