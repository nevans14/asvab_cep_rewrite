import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/login.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { RegisterDialogComponent } from '../register-dialog/register-dialog.component';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {
  email;
  password;
  accessCode;

  showAccessCodeLogin: boolean;
  showEmailLogin: boolean;
  showPasswordReset: boolean;

  // for resetting password
  public resetPasswordEmail;

  // toggles form
  public disabled = false;
  public AccessCode;

  // login dialog error message
  public loginDialogErrorMessage;
  public loginDialogPasswordErrorMessage;
  public loginAccessCodeMessage;


  constructor(
    private _dialogRef: MatDialogRef<LoginDialogComponent>,
    public _loginService: LoginService,
    private _dialog: MatDialog,
    private _config: ConfigService,
  ) { }

  ngOnInit() {
    //by default, show Email login
    this.showEmailLogin = true;
    this.showAccessCodeLogin = false;
    this.showPasswordReset = false;
  }

  showRegisterDialog() {
    this._dialog.open(RegisterDialogComponent, {});
    this.close();
  }

  close() {
    this._dialogRef.close();
  }

  async submitEmailLogin() {
    //wait for the response from the server
    this._loginService.email = this.email;
    this._loginService.password = this.password;
    
    const loginAttempt = await this._loginService.emailLogin(true);

    //if success; redirect 
    if (loginAttempt == "SUCCESS") {
      this._loginService.redirectAfterLogin();
      this.close();
      // window.location.href = this._config.getSSOUrl();
    } else {
      this.loginDialogErrorMessage = loginAttempt;
      setTimeout(() => { this.loginDialogErrorMessage = ''; }, 5000);
    }

  }

  async submitAccessCodeLogin() {

    const accessCodeLogin = await this._loginService.accessCodeLogin(this.accessCode, true);

    if (accessCodeLogin) {

      //teacher code
      if (accessCodeLogin == "TEACHER_CODE") {
        // show the register modal and pass in the access code
        this._dialog.open(RegisterDialogComponent, {
          data: { accessCode: this.accessCode.toUpperCase() },
          hasBackdrop: true,
          panelClass: 'custom-dialog-container',
          height: '95%',
          autoFocus: true,
          disableClose: true
        });
      } else if (accessCodeLogin == "ACCESS_CODE") {
        //access code

        // NOTE: SSO login to the CTM website will not happen with this login type because 
        // switches to Email/Password login if linked

        this._loginService.redirectAfterLogin();
        this.close();
        // window.location.href = this._config.getSSOUrl();
      } else {
        //if we got here then an error msg was returned
        this.loginAccessCodeMessage = accessCodeLogin;
        setTimeout(() => { this.loginAccessCodeMessage = ''; }, 5000);
      }

    }
  }

  async sendResetCode() {
    this._loginService.resetPasswordEmail = this.resetPasswordEmail;
    const resetCodeAttempt = await this._loginService.sendResetCode();

    if (resetCodeAttempt === "SUCCESS") {
      this.loginDialogPasswordErrorMessage = 'Email with Reset key has been sent.';
      setTimeout(() => { this.loginDialogPasswordErrorMessage = ''; }, 5000);
    } else {
      this.loginDialogPasswordErrorMessage = resetCodeAttempt;
      setTimeout(() => { this.loginDialogPasswordErrorMessage = ''; }, 5000);
    }
  }

}
