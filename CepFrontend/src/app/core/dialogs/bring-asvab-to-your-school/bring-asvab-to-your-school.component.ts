import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { Router } from '@angular/router';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { DbInfoService } from 'app/services/db-info.service';
import { ReCaptcha2Component } from 'ngx-captcha';
import { UtilityService } from 'app/services/utility.service';
// import { WINDOW_PROVIDERS } from 'ngx-owl-carousel-o/lib/services/window-ref.service';
@Component({
  selector: 'app-bring-asvab-to-your-school',
  templateUrl: './bring-asvab-to-your-school.component.html',
  styleUrls: ['./bring-asvab-to-your-school.component.scss']
})
export class BringAsvabToYourSchoolComponent implements OnInit {
  studentFormGroup: FormGroup;
  schedFormGroup: FormGroup;
  responseOne: '';
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  formDisabled = false;
  heardOther = false;
  errorMessage: string;
  cursorStyle = { 'cursor': 'pointer' };
  path: any;
  isDisplayBanner: any;
  maintenanceBannerText: any;

  isIE: boolean;
  dateOpen: Array<boolean> = [false, false, false];
  testIndex: number = 0;
  testReservations: Array<any> = [];
  ptiIndex: number = 0;
  ptiReservations: Array<any> = [];
  submitType: number = 0;  //0: prev, 1: add test 2: remove test

  progressBarsIndex: number = 0;
  progressBarsTotal: number = 2;

  notShow: boolean = false;
  showSchedule: boolean = false;  //NOTE: toggle to true to test schedule directly
  // showSchedule: boolean = true;
  showSchool: boolean = false;
  // showTest: boolean = true;
  showTest: boolean = false;
  // showTest: boolean = true;
  showPTI1: boolean = false;
  // showPTI2: boolean = true;
  showPTI2: boolean = false;
  showActivities: boolean = false;
  showParents: boolean = false;
  showSummary: boolean = false;

  isProcessing: boolean = false;

  testFirstChoiceStartTime: any = {hour: 0, minute: 0};
  testSecondChoiceStartTime: any = {hour: 0, minute: 0};
  testThirdChoiceStartTime: any = {hour: 0, minute: 0};
  ptiFirstChoiceStartTime: any = {hour: 0, minute: 0};
  ptiSecondChoiceStartTime: any = {hour: 0, minute: 0};
  ptiThirdChoiceStartTime: any = {hour: 0, minute: 0};
  activityFirstChoiceStartTime: any = {hour: 0, minute: 0};
  activitySecondChoiceStartTime: any = {hour: 0, minute: 0};
  activityThirdChoiceStartTime: any = {hour: 0, minute: 0};
  parentFirstChoiceStartTime: any = {hour: 0, minute: 0};
  parentSecondChoiceStartTime: any = {hour: 0, minute: 0};
  parentThirdChoiceStartTime: any = {hour: 0, minute: 0};

  focusForIE: boolean = false;

  ptiMissing: boolean;
  classroomActivitiesMissing: boolean;
  presentationMissing: boolean;

  today: string;

  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  @ViewChild('topSection') topSection: ElementRef;

  constructor(
    public _dialogRef: MatDialogRef<BringAsvabToYourSchoolComponent>,
    private _dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _configService: ConfigService,
    private _http: HttpHelperService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _router: Router,
    private _dbInfoService: DbInfoService,
    private _utilityService: UtilityService,
    private _renderer: Renderer2,
  ) {
    this.studentFormGroup = this._formBuilder.group({
      recaptchaResponse: ['', Validators.required],
      counselorEmail: ['', Validators.required],
      studentHeardUs: [''],
      studentHeardUsOther: ['']
    });

    this.schedFormGroup = this._formBuilder.group({
      recaptcha: ['', Validators.required],
      title: ['', Validators.required],
      position: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      phoneExt: [''],
      email: ['', Validators.required],
      heardUs: [''],
      heardUsOther: [''],
      schoolName: ['', Validators.required],
      schoolAddress: ['', Validators.required],
      schoolAddress2: ['', Validators.required],
      schoolCity: ['', Validators.required],
      schoolCounty: ['', Validators.required],
      schoolState: ['', Validators.required],
      schoolZip: ['', Validators.required],
      studentNum10thGrade: [0],
      studentNum11thGrade: [0],
      studentNum12thGrade: [0],
      studentNumOther: [0],
      asvabTest: [false],
      pti: [{ value: false, disabled: false }],
      ptiVirtual: [{ value: false, disabled: true }],
      ptiInPerson: [{ value: false, disabled: true }],
      classroomActivities: [{ value: false, disabled: false }],
      classroomActivitiesVirtual: [{ value: false, disabled: true }],
      classroomActivitiesInPerson: [{ value: false, disabled: true }],
      presentation: [{ value: false, disabled: false }],
      presentationVirtual: [{ value: false, disabled: true }],
      presentationInPerson: [{ value: false, disabled: true }],
      testFirstChoiceDate: ['', Validators.required],
      testSecondChoiceDate: [''],
      testThirdChoiceDate: [''],
      testFirstChoiceStartTime: ['', Validators.required],
      testSecondChoiceStartTime: [''],
      testThirdChoiceStartTime: [''],
      testStudentNum10thGrade: [0],
      testStudentNum11thGrade: [0],
      testStudentNum12thGrade: [0],
      testStudentNumOther: [0],
      testInterestInIcat: [{ value: undefined, disabled: false }],
      testNumOfAccommodatedStudents: [0],
      testScoreReleaseOption: [0],
      testLocation: [''],
      testRemarks: [''],
      testNeedLapboards: [{ value: undefined, disabled: false }],
      testNeedLapboardsNumber: [{ value: '', disabled: false }],

      ptiFirstChoiceDate: [''],
      ptiSecondChoiceDate: [''],
      ptiThirdChoiceDate: [''],
      ptiFirstChoiceStartTime: [''],
      ptiSecondChoiceStartTime: [''],
      ptiThirdChoiceStartTime: [''],
      ptiStudentNum10thGrade: [0],
      ptiStudentNum11thGrade: [0],
      ptiStudentNum12thGrade: [0],
      ptiStudentNumOther: [0],
      ptiLocation: [''],
      ptiRemarks: [''],
      ptiProjectorAvailable: [{ value: undefined, disabled: false }],
      ptiInternetAvailable: [{ value: undefined, disabled: false }],
      ptiComputerOrPhone: [{ value: undefined, disabled: false }],
      ptiNeedLapboards: [{ value: undefined, disabled: false }],
      ptiNeedLapboardsNumber: [{ value: '', disabled: false }],

      activityFirstChoiceDate: [''],
      activitySecondChoiceDate: [''],
      activityThirdChoiceDate: [''],
      activityFirstChoiceStartTime: [''],
      activitySecondChoiceStartTime: [''],
      activityThirdChoiceStartTime: [''],
      activityAttendees: [0],
      activityLocation: [''],
      activityRemarks: [''],

      parentFirstChoiceDate: [''],
      parentSecondChoiceDate: [''],
      parentThirdChoiceDate: [''],
      parentFirstChoiceStartTime: [''],
      parentSecondChoiceStartTime: [''],
      parentThirdChoiceStartTime: [''],
      parentAttendees: [0],
      parentLocation: [''],
      parentRemarks: [''],

      notifyEmailAddresses: [''],
    });

    // this.isIE = true; //this._utilityService.isIE();
    this.isIE = this._utilityService.isIE();
  }

  ngOnInit() {
    this.path = this._router.url;
    this.isDisplayBanner = false;
    this.maintenanceBannerText = '';
    this._dbInfoService.getByName('cep_bring_modal_banner').subscribe((response: any) => {
      this.isDisplayBanner = response.value2 ? "true" : "false";
      this.maintenanceBannerText = this.isDisplayBanner ? response.value2 : '';
    });

    this.setFormDisabled();

    // TEST
    if (this.showTest) {
      this.schedFormGroup.get("asvabTest").setValue(true);
    }

    this.today = (new Date()).getFullYear() + '-' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + ("0" + new Date().getDate()).slice(-2);
  }

  

  updateStartTime(time, config) {
    if (config.test) {
      switch(config.index) {
        case 0:
          this.schedFormGroup.get("testFirstChoiceStartTime").setValue(this.testFirstChoiceStartTime);
          break;
        case 1:
          this.schedFormGroup.get("testSecondChoiceStartTime").setValue(this.testSecondChoiceStartTime);
          break;
        case 2:
          this.schedFormGroup.get("testThirdChoiceStartTime").setValue(this.testThirdChoiceStartTime);
          break;
      }
    }
    if (config.pti) {
      switch(config.index) {
        case 0:
          this.schedFormGroup.get("ptiFirstChoiceStartTime").setValue(this.ptiFirstChoiceStartTime);
          break;
        case 1:
          this.schedFormGroup.get("ptiSecondChoiceStartTime").setValue(this.ptiSecondChoiceStartTime);
          break;
        case 2:
          this.schedFormGroup.get("ptiThirdChoiceStartTime").setValue(this.ptiThirdChoiceStartTime);
          break;
      }
    }
    if (config.activity) {
      switch(config.index) {
        case 0:
          this.schedFormGroup.get("activityFirstChoiceStartTime").setValue(this.activityFirstChoiceStartTime);
          break;
        case 1:
          this.schedFormGroup.get("activitySecondChoiceStartTime").setValue(this.activitySecondChoiceStartTime);
          break;
        case 2:
          this.schedFormGroup.get("activityThirdChoiceStartTime").setValue(this.activityThirdChoiceStartTime);
          break;
      }
    }
    if (config.parent) {
      switch(config.index) {
        case 0:
          this.schedFormGroup.get("parentFirstChoiceStartTime").setValue(this.parentFirstChoiceStartTime);
          break;
        case 1:
          this.schedFormGroup.get("parentSecondChoiceStartTime").setValue(this.parentSecondChoiceStartTime);
          break;
        case 2:
          this.schedFormGroup.get("parentThirdChoiceStartTime").setValue(this.parentThirdChoiceStartTime);
          break;
      }
    }
  }

  openDatePicker(index) {
    this.dateOpen[index] = true;
  };

  setFormDisabled() {
    let that = this;
    setTimeout(function() {

      that.markingMissingType();

      that.formDisabled = that.showSchedule && (
        !that.schedFormGroup.value.asvabTest && !that.schedFormGroup.value.pti && !that.schedFormGroup.value.classroomActivities && !that.schedFormGroup.value.presentation
        || (that.schedFormGroup.value.pti && !that.schedFormGroup.value.ptiVirtual && !that.schedFormGroup.value.ptiInPerson)
        || (that.schedFormGroup.value.classroomActivities && !that.schedFormGroup.value.classroomActivitiesVirtual && !that.schedFormGroup.value.classroomActivitiesInPerson)
        || (that.schedFormGroup.value.presentation && !that.schedFormGroup.value.presentationVirtual && !that.schedFormGroup.value.presentationInPerson)
      )

    }, 200);

    // this.formDisabled = this.showSchedule && (
    //   !this.schedFormGroup.value.asvabTest && !this.schedFormGroup.value.pti && !this.schedFormGroup.value.classroomActivities && !this.schedFormGroup.value.presentation
    //   || (this.schedFormGroup.value.pti && !this.schedFormGroup.value.ptiVirtual && !this.schedFormGroup.value.ptiInPerson)
    //   || (this.schedFormGroup.value.classroomActivities && !this.schedFormGroup.value.classroomActivitiesVirtual && !this.schedFormGroup.value.classroomActivitiesInPerson)
    //   || (this.schedFormGroup.value.presentation && !this.schedFormGroup.value.presentationVirtual && !this.schedFormGroup.value.presentationInPerson)
    // )
  }

  showPTISummary() {
    return this.showSummary &&
      this.ptiReservations.some(function(i) {
        return Object.values(i).some(function(j) {
          return j !== undefined && j !== null && j !== 0 && j !== ""
        })
      });
  }

  booleanToText = function(val) {
    return (val === undefined || val === null) ? null : !!val ? 'No' : (typeof val === 'string' || val instanceof String) && val.toLowerCase() === 'true' ? 'Yes' : 'No';
  }

  // Test methods
  addTest(index = undefined) {

    index = typeof index !== 'undefined' ? index : undefined;

    var testObj = {
      testFirstChoiceDate: this.schedFormGroup.value.testFirstChoiceDate,
      testFirstChoiceStartTime: this.schedFormGroup.value.testFirstChoiceStartTime,
      testSecondChoiceDate: this.schedFormGroup.value.testSecondChoiceDate,
      testSecondChoiceStartTime: this.schedFormGroup.value.testSecondChoiceStartTime,
      testThirdChoiceDate: this.schedFormGroup.value.testThirdChoiceDate,
      testThirdChoiceStartTime: this.schedFormGroup.value.testThirdChoiceStartTime,
      testStudentNum10thGrade: this.schedFormGroup.value.testStudentNum10thGrade,
      testStudentNum11thGrade: this.schedFormGroup.value.testStudentNum11thGrade,
      testStudentNum12thGrade: this.schedFormGroup.value.testStudentNum12thGrade,
      testStudentNumOther: this.schedFormGroup.value.testStudentNumOther,
      testInterestInIcat: this.schedFormGroup.value.testInterestInIcat,
      testNumOfAccommodatedStudents: this.schedFormGroup.value.testNumOfAccommodatedStudents,
      testScoreReleaseOption: this.schedFormGroup.value.testScoreReleaseOption,
      testLocation: this.schedFormGroup.value.testLocation,
      testRemarks: this.schedFormGroup.value.testRemarks,
      testNeedLapboards: this.schedFormGroup.value.testNeedLapboards,
      testNeedLapboardsNumber: this.schedFormGroup.value.testNeedLapboardsNumber,
    }

    if (!isNaN(index)) {
      this.testReservations.splice(index, 0, testObj)
      return;
    }

    this.testReservations.push(testObj)
  }

  setTest() {
    this.schedFormGroup.get("testFirstChoiceDate").setValue(this.testReservations[this.testIndex].testFirstChoiceDate);
    this.schedFormGroup.get("testFirstChoiceStartTime").setValue(this.testReservations[this.testIndex].testFirstChoiceStartTime);
    this.schedFormGroup.get("testSecondChoiceDate").setValue(this.testReservations[this.testIndex].testSecondChoiceDate);
    this.schedFormGroup.get("testSecondChoiceStartTime").setValue(this.testReservations[this.testIndex].testSecondChoiceStartTime);
    this.schedFormGroup.get("testThirdChoiceDate").setValue(this.testReservations[this.testIndex].testThirdChoiceDate);
    this.schedFormGroup.get("testThirdChoiceStartTime").setValue(this.testReservations[this.testIndex].testThirdChoiceStartTime);
    this.schedFormGroup.get("testStudentNum10thGrade").setValue(this.testReservations[this.testIndex].testStudentNum10thGrade);
    this.schedFormGroup.get("testStudentNum11thGrade").setValue(this.testReservations[this.testIndex].testStudentNum11thGrade);
    this.schedFormGroup.get("testStudentNum12thGrade").setValue(this.testReservations[this.testIndex].testStudentNum12thGrade);
    this.schedFormGroup.get("testStudentNumOther").setValue(this.testReservations[this.testIndex].testStudentNumOther);
    this.schedFormGroup.get("testInterestInIcat").setValue(this.testReservations[this.testIndex].testInterestInIcat);
    this.schedFormGroup.get("testNumOfAccommodatedStudents").setValue(this.testReservations[this.testIndex].testNumOfAccommodatedStudents);
    this.schedFormGroup.get("testScoreReleaseOption").setValue(this.testReservations[this.testIndex].testScoreReleaseOption);
    this.schedFormGroup.get("testLocation").setValue(this.testReservations[this.testIndex].testLocation);
    this.schedFormGroup.get("testRemarks").setValue(this.testReservations[this.testIndex].testRemarks);
    this.schedFormGroup.get("testNeedLapboards").setValue(this.testReservations[this.testIndex].testNeedLapboards);
    this.schedFormGroup.get("testNeedLapboardsNumber").setValue(this.testReservations[this.testIndex].testNeedLapboardsNumber);
  }

  updateSavedTest() {
    this.testReservations[this.testIndex].testFirstChoiceDate = this.schedFormGroup.value.testFirstChoiceDate;
    this.testReservations[this.testIndex].testFirstChoiceStartTime = this.schedFormGroup.value.testFirstChoiceStartTime;
    this.testReservations[this.testIndex].testSecondChoiceDate = this.schedFormGroup.value.testSecondChoiceDate;
    this.testReservations[this.testIndex].testSecondChoiceStartTime = this.schedFormGroup.value.testSecondChoiceStartTime;
    this.testReservations[this.testIndex].testThirdChoiceDate = this.schedFormGroup.value.testThirdChoiceDate;
    this.testReservations[this.testIndex].testThirdChoiceStartTime = this.schedFormGroup.value.testThirdChoiceStartTime;
    this.testReservations[this.testIndex].testStudentNum10thGrade = this.schedFormGroup.value.testStudentNum10thGrade;
    this.testReservations[this.testIndex].testStudentNum11thGrade = this.schedFormGroup.value.testStudentNum11thGrade;
    this.testReservations[this.testIndex].testStudentNum12thGrade = this.schedFormGroup.value.testStudentNum12thGrade;
    this.testReservations[this.testIndex].testStudentNumOther = this.schedFormGroup.value.testStudentNumOther;
    this.testReservations[this.testIndex].testInterestInIcat = this.schedFormGroup.value.testInterestInIcat;
    this.testReservations[this.testIndex].testNumOfAccommodatedStudents = this.schedFormGroup.value.testNumOfAccommodatedStudents;
    this.testReservations[this.testIndex].testScoreReleaseOption = this.schedFormGroup.value.testScoreReleaseOption;
    this.testReservations[this.testIndex].testLocation = this.schedFormGroup.value.testLocation;
    this.testReservations[this.testIndex].testRemarks = this.schedFormGroup.value.testRemarks;
    this.testReservations[this.testIndex].testNeedLapboards = this.schedFormGroup.value.testNeedLapboards;
    this.testReservations[this.testIndex].testNeedLapboardsNumber = this.schedFormGroup.value.testNeedLapboardsNumber;
  }

  clearTest() {
     this.schedFormGroup.get("testFirstChoiceDate").setValue(undefined);
     this.schedFormGroup.get("testFirstChoiceStartTime").setValue(undefined);
     this.schedFormGroup.get("testSecondChoiceDate").setValue(undefined);
     this.schedFormGroup.get("testSecondChoiceStartTime").setValue(undefined);
     this.schedFormGroup.get("testThirdChoiceDate").setValue(undefined);
     this.schedFormGroup.get("testThirdChoiceStartTime").setValue(undefined);
     this.schedFormGroup.get("testStudentNum10thGrade").setValue(0);
     this.schedFormGroup.get("testStudentNum11thGrade").setValue(0);
     this.schedFormGroup.get("testStudentNum12thGrade").setValue(0);
     this.schedFormGroup.get("testStudentNumOther").setValue(0);
     this.schedFormGroup.get("testInterestInIcat").setValue(false);
     this.schedFormGroup.get("testNumOfAccommodatedStudents").setValue(0);
     this.schedFormGroup.get("testScoreReleaseOption").setValue(undefined);
     this.schedFormGroup.get("testLocation").setValue(undefined);
     this.schedFormGroup.get("testRemarks").setValue(undefined);
     this.schedFormGroup.get("testNeedLapboards").setValue(false);
     this.schedFormGroup.get("testNeedLapboardsNumber").setValue(undefined);
  }

  removeTest(index) {
    index = typeof index !== 'undefined' ? index : undefined;

    if (!isNaN(index)) {
      this.testReservations.splice(index, 1);
    } else {
      this.testReservations.pop();
    }
  }

  // PTI methods
  addPti(index = undefined) {

    index = typeof index !== 'undefined' ? index : undefined;

    var testObj = {
      ptiFirstChoiceDate: this.schedFormGroup.value.ptiFirstChoiceDate,
      ptiSecondChoiceDate: this.schedFormGroup.value.ptiSecondChoiceDate,
      ptiThirdChoiceDate: this.schedFormGroup.value.ptiThirdChoiceDate,
      ptiFirstChoiceStartTime: this.schedFormGroup.value.ptiFirstChoiceStartTime,
      ptiSecondChoiceStartTime: this.schedFormGroup.value.ptiSecondChoiceStartTime,
      ptiThirdChoiceStartTime: this.schedFormGroup.value.ptiThirdChoiceStartTime,
      ptiNeedLapboards: this.schedFormGroup.value.ptiNeedLapboards,
      ptiNeedLapboardsNumber: this.schedFormGroup.value.ptiNeedLapboardsNumber,
      ptiStudentNum10thGrade: this.schedFormGroup.value.ptiStudentNum10thGrade,
      ptiStudentNum11thGrade: this.schedFormGroup.value.ptiStudentNum11thGrade,
      ptiStudentNum12thGrade: this.schedFormGroup.value.ptiStudentNum12thGrade,
      ptiStudentNumOther: this.schedFormGroup.value.ptiStudentNumOther,
      ptiLocation: this.schedFormGroup.value.ptiLocation,
      ptiRemarks: this.schedFormGroup.value.ptiRemarks,
      ptiProjectorAvailable: this.schedFormGroup.value.ptiProjectorAvailable,
      ptiInternetAvailable: this.schedFormGroup.value.ptiInternetAvailable,
      ptiComputerOrPhone: this.schedFormGroup.value.ptiComputerOrPhone,
    }

    if (!isNaN(index)) {
      this.ptiReservations.splice(index, 0, testObj)
      return;
    }

    this.ptiReservations.push(testObj)
  }

  setPti() {
    this.schedFormGroup.get("ptiFirstChoiceDate").setValue(this.ptiReservations[this.ptiIndex].ptiFirstChoiceDate);
    this.schedFormGroup.get("ptiFirstChoiceStartTime").setValue(this.ptiReservations[this.ptiIndex].ptiFirstChoiceStartTime);
    this.schedFormGroup.get("ptiSecondChoiceDate").setValue(this.ptiReservations[this.ptiIndex].ptiSecondChoiceDate);
    this.schedFormGroup.get("ptiSecondChoiceStartTime").setValue(this.ptiReservations[this.ptiIndex].ptiSecondChoiceStartTime);
    this.schedFormGroup.get("ptiThirdChoiceDate").setValue(this.ptiReservations[this.ptiIndex].ptiThirdChoiceDate);
    this.schedFormGroup.get("ptiThirdChoiceStartTime").setValue(this.ptiReservations[this.ptiIndex].ptiThirdChoiceStartTime);
    this.schedFormGroup.get("ptiStudentNum10thGrade").setValue(this.ptiReservations[this.ptiIndex].ptiStudentNum10thGrade);
    this.schedFormGroup.get("ptiStudentNum11thGrade").setValue(this.ptiReservations[this.ptiIndex].ptiStudentNum11thGrade);
    this.schedFormGroup.get("ptiStudentNum12thGrade").setValue(this.ptiReservations[this.ptiIndex].ptiStudentNum12thGrade);
    this.schedFormGroup.get("ptiStudentNumOther").setValue(this.ptiReservations[this.ptiIndex].ptiStudentNumOther);
    this.schedFormGroup.get("ptiLocation").setValue(this.ptiReservations[this.ptiIndex].ptiLocation);
    this.schedFormGroup.get("ptiRemarks").setValue(this.ptiReservations[this.ptiIndex].ptiRemarks);
    this.schedFormGroup.get("ptiNeedLapboards").setValue(this.ptiReservations[this.ptiIndex].ptiNeedLapboards);
    this.schedFormGroup.get("ptiNeedLapboardsNumber").setValue(this.ptiReservations[this.ptiIndex].ptiNeedLapboardsNumber);
    this.schedFormGroup.get("ptiProjectorAvailable").setValue(this.ptiReservations[this.ptiIndex].ptiProjectorAvailable);
    this.schedFormGroup.get("ptiInternetAvailable").setValue(this.ptiReservations[this.ptiIndex].ptiInternetAvailable);
    this.schedFormGroup.get("ptiComputerOrPhone").setValue(this.ptiReservations[this.ptiIndex].ptiComputerOrPhone);
  }

  updateSavedPti() {
    this.ptiReservations[this.ptiIndex].ptiFirstChoiceDate = this.schedFormGroup.value.ptiFirstChoiceDate;
    this.ptiReservations[this.ptiIndex].ptiFirstChoiceStartTime = this.schedFormGroup.value.ptiFirstChoiceStartTime;
    this.ptiReservations[this.ptiIndex].ptiSecondChoiceDate = this.schedFormGroup.value.ptiSecondChoiceDate;
    this.ptiReservations[this.ptiIndex].ptiSecondChoiceStartTime = this.schedFormGroup.value.ptiSecondChoiceStartTime;
    this.ptiReservations[this.ptiIndex].ptiThirdChoiceDate = this.schedFormGroup.value.ptiThirdChoiceDate;
    this.ptiReservations[this.ptiIndex].ptiThirdChoiceStartTime = this.schedFormGroup.value.ptiThirdChoiceStartTime;
    this.ptiReservations[this.ptiIndex].ptiStudentNum10thGrade = this.schedFormGroup.value.ptiStudentNum10thGrade;
    this.ptiReservations[this.ptiIndex].ptiStudentNum11thGrade = this.schedFormGroup.value.ptiStudentNum11thGrade;
    this.ptiReservations[this.ptiIndex].ptiStudentNum12thGrade = this.schedFormGroup.value.ptiStudentNum12thGrade;
    this.ptiReservations[this.ptiIndex].ptiStudentNumOther = this.schedFormGroup.value.ptiStudentNumOther;
    this.ptiReservations[this.ptiIndex].ptiLocation = this.schedFormGroup.value.ptiLocation;
    this.ptiReservations[this.ptiIndex].ptiRemarks = this.schedFormGroup.value.ptiRemarks;
    this.ptiReservations[this.ptiIndex].ptiNeedLapboards = this.schedFormGroup.value.ptiNeedLapboards;
    this.ptiReservations[this.ptiIndex].ptiNeedLapboardsNumber = this.schedFormGroup.value.ptiNeedLapboardsNumber;
    this.ptiReservations[this.ptiIndex].ptiProjectorAvailable = this.schedFormGroup.value.ptiProjectorAvailable;
    this.ptiReservations[this.ptiIndex].ptiInternetAvailable = this.schedFormGroup.value.ptiInternetAvailable;
    this.ptiReservations[this.ptiIndex].ptiComputerOrPhone = this.schedFormGroup.value.ptiComputerOrPhone;
  }

  clearPti() {
    this.schedFormGroup.get("ptiFirstChoiceDate").setValue(undefined);
    this.schedFormGroup.get("ptiSecondChoiceDate").setValue(undefined);
    this.schedFormGroup.get("ptiThirdChoiceDate").setValue(undefined);
    this.schedFormGroup.get("ptiFirstChoiceStartTime").setValue(undefined);
    this.schedFormGroup.get("ptiSecondChoiceStartTime").setValue(undefined);
    this.schedFormGroup.get("ptiThirdChoiceStartTime").setValue(undefined);
    this.schedFormGroup.get("ptiNeedLapboards").setValue(false);
    this.schedFormGroup.get("ptiNeedLapboardsNumber").setValue(undefined);
    this.schedFormGroup.get("ptiStudentNum10thGrade").setValue(0);
    this.schedFormGroup.get("ptiStudentNum11thGrade").setValue(0);
    this.schedFormGroup.get("ptiStudentNum12thGrade").setValue(0);
    this.schedFormGroup.get("ptiStudentNumOther").setValue(0);
    this.schedFormGroup.get("ptiLocation").setValue(undefined);
    this.schedFormGroup.get("ptiRemarks").setValue(undefined);
    this.schedFormGroup.get("ptiProjectorAvailable").setValue(undefined);
    this.schedFormGroup.get("ptiInternetAvailable").setValue(false);
    this.schedFormGroup.get("ptiComputerOrPhone").setValue(undefined);
  }

  removePti(index) {
    index = typeof index !== 'undefined' ? index : undefined;

    if (!isNaN(index)) {
      this.ptiReservations.splice(index, 1);
    } else {
      this.ptiReservations.pop();
    }
  }


  pageSelector() {
    if (this.submitType === 0) {
      this.pageToShow(false);
    } else if (this.submitType === 2) {
      this.pageToShow(false, {remove: true})
    } else if (this.submitType === 1) {
      this.pageToShow(false, {add: true})
    } else if (this.submitType === 3) {
      this.pageToShow();
    }

    this.scrollToTop();
  }

  setDeliveryFormat() {
    this.schedFormGroup.value.pti = this.schedFormGroup.value.ptiVirtual ? 'Virtual' : '';
    if (!this.schedFormGroup.value.pti) {
      this.schedFormGroup.value.pti = this.schedFormGroup.value.ptiInPerson ? 'In-person' : '';
    } else if (this.schedFormGroup.value.ptiInPerson) {
      this.schedFormGroup.value.pti = this.schedFormGroup.value.pti.concat(', ' + (this.schedFormGroup.value.ptiInPerson ? 'In-person' : ''));
    }

    this.schedFormGroup.value.classroomActivities = this.schedFormGroup.value.classroomActivitiesVirtual ? 'Virtual' : '';
    if (!this.schedFormGroup.value.classroomActivities) {
      this.schedFormGroup.value.classroomActivities = this.schedFormGroup.value.classroomActivitiesInPerson ? 'In-person' : '';
    } else if (this.schedFormGroup.value.classroomActivitiesInPerson) {
      this.schedFormGroup.value.classroomActivities = this.schedFormGroup.value.classroomActivities.concat(', ' + (this.schedFormGroup.value.classroomActivitiesInPerson ? 'In-person' : ''));
    }

    this.schedFormGroup.value.presentation = this.schedFormGroup.value.presentationVirtual ? 'Virtual' : '';
    if (!this.schedFormGroup.value.presentation) {
      this.schedFormGroup.value.presentation = this.schedFormGroup.value.presentationInPerson ? 'In-person' : '';
    } else if (this.schedFormGroup.value.presentationInPerson) {
      this.schedFormGroup.value.presentation = this.schedFormGroup.value.presentation.concat(', ' + (this.schedFormGroup.value.presentationInPerson ? 'In-person' : ''));
    }
  }

  pageToShow(next = undefined, reservations = undefined) {

    next = typeof next !== 'undefined' ? next : true;
    reservations = typeof reservations !== 'undefined' ? reservations : {};

    if (this.isIE && this.focusForIE) {
      this.focusForIE = false;
      return;
    }

    if (reservations.add && this.showTest) {
      if (this.testIndex === this.testReservations.length) {
        this.addTest();
        this.testIndex++;
        this.clearTest();	
      } else {
        this.updateSavedTest();
        this.addTest(this.testIndex);
        this.testIndex++;
        this.clearTest();
      }
      
      this.progressBarsIndex++;
      this.progressBarsTotal++;
      return;
    } else if (reservations.remove && this.showTest) {
      this.removeTest(this.testIndex);
      this.testIndex--;
      this.setTest();

      this.progressBarsIndex--;
      this.progressBarsTotal--;
      return;
    }

    if (reservations.add && (this.showPTI1 || this.showPTI2)) {
      if (this.ptiIndex === this.ptiReservations.length) {
        this.addPti();
        this.ptiIndex++;
        this.clearPti();	
      } else {
        this.updateSavedPti();
        this.addPti(this.ptiIndex);
        this.ptiIndex++;
        this.clearPti();
      }
      
      this.progressBarsIndex++;
      this.progressBarsTotal++;
      return;
    } else if (reservations.remove && (this.showPTI1 || this.showPTI2)) {
      this.removePti(this.ptiIndex);
      this.ptiIndex--;
      this.setPti();

      this.progressBarsIndex--;
      this.progressBarsTotal--;
      return;
    }


    if (this.showSchool
      && next
    ) {
      this.showSchool = false;
      if (this.schedFormGroup.value.asvabTest) {
        this.showTest = true;
      } else if (this.schedFormGroup.value.pti) {
        this.showPTI2 = true;
      } else if (this.schedFormGroup.value.classroomActivities) {
        this.showActivities = true;
      } else if (this.schedFormGroup.value.presentation) {
        this.showParents = true;
      }
      this.progressBarsIndex++;

      return;
    }

    if (this.showTest
      && !next
    ) {
      if (this.testIndex === 0) {
        this.showTest = false;
        this.showSchool = true;
      } else if (this.testIndex === this.testReservations.length) {
        this.addTest();
        this.testIndex--;
        this.setTest();
      } else {
        this.updateSavedTest();
        this.testIndex--;
        this.setTest();
      }
      this.progressBarsIndex--;
      return;
    }

    if (this.showTest
      && next
    ) {
      if (this.testIndex > this.testReservations.length - 1) {
        this.addTest();
        this.showTest = false;
        this.showPTI1 = true;
      } else if (this.testIndex + 1 <= this.testReservations.length - 1) {
        this.updateSavedTest();
        this.testIndex++;
        this.setTest();
      } else {
        this.updateSavedTest();
        this.showTest = false;
        this.showPTI1 = true;
      }
      this.progressBarsIndex++;
      return;
    }

    if (this.showPTI1
      && next
    ) {

      if (this.ptiIndex > this.ptiReservations.length - 1) {
          this.addPti();
          this.showPTI1 = false;
          if (this.schedFormGroup.value.classroomActivities) {
            this.showActivities = true;
          } else if (this.schedFormGroup.value.presentation) {
            this.showParents = true;
          } else {
            this.showSummary = true;
          }
        } else if (this.ptiIndex + 1 <= this.ptiReservations.length - 1) {
          this.updateSavedPti();
          this.ptiIndex++;
          this.setPti();
        } else {
          this.updateSavedPti();
          this.showPTI1 = false;
          if (this.schedFormGroup.value.classroomActivities) {
            this.showActivities = true;
          } else if (this.schedFormGroup.value.presentation) {
            this.showParents = true;
          } else {
            this.showSummary = true;
          }
        }

        this.progressBarsIndex++;

        return;
    }

    if (this.showPTI1
      && !next
    ) {
      if (this.ptiIndex === 0) {
        this.showPTI1 = false;
        this.showTest = true;
        this.setTest();
      } else if (this.ptiIndex === this.ptiReservations.length) {
        this.addPti();
        this.ptiIndex--;
        this.setPti();
      } else {
        this.updateSavedPti();
        this.ptiIndex--;
        this.setPti();
      }

      this.progressBarsIndex--;
      return;
    }


    if (this.showPTI2
      && next
    ) {

      if (this.ptiIndex > this.ptiReservations.length - 1) {
          this.addPti();
          this.showPTI2 = false;
          if (this.schedFormGroup.value.classroomActivities) {
            this.showActivities = true;
          } else if (this.schedFormGroup.value.presentation) {
            this.showParents = true;
          } else {
            this.showSummary = true;
          }
        } else if (this.ptiIndex + 1 <= this.ptiReservations.length - 1) {
          this.updateSavedPti();
          this.ptiIndex++;
          this.setPti();
        } else {
          this.updateSavedPti();
          this.showPTI2 = false;
          if (this.schedFormGroup.value.classroomActivities) {
            this.showActivities = true;
          } else if (this.schedFormGroup.value.presentation) {
            this.showParents = true;
          } else {
            this.showSummary = true;
          }
        }

        this.progressBarsIndex++;
        return;
    }

    if (this.showPTI2
      && !next
    ) {
      if (this.ptiIndex === 0) {
        this.showPTI2 = false;
        this.showSchool = true;
      } else if (this.ptiIndex === this.ptiReservations.length) {
        this.addPti();
        this.ptiIndex--;
        this.setPti();
      } else {
        this.updateSavedPti();
        this.ptiIndex--;
        this.setPti();
      }
      this.progressBarsIndex--;
      return;
    }

    if (this.showActivities
      && !next
    ) {
      this.showActivities = false;
      if (this.schedFormGroup.value.asvabTest) {
        this.showPTI1 = true;
      } else if (this.schedFormGroup.value.pti) {
        this.showPTI2 = true;
      } else {
        this.showSchool = true;
      }
      this.progressBarsIndex--;
      return;
    }

    if (this.showActivities
      && next
    ) {
      this.showActivities = false;
      if (this.schedFormGroup.value.presentation) {
        this.showParents = true;
      } else {
        this.showSummary = true;
      }
      this.progressBarsIndex++;
      return;
    }

    if (this.showParents
      && !next
    ) {
      this.showParents = false;
      if (this.schedFormGroup.value.classroomActivities) {
        this.showActivities = true;
      } else if (this.schedFormGroup.value.asvabTest) {
        this.showPTI1 = true;
      } else if (this.schedFormGroup.value.pti) {
        this.showPTI2 = true;
      } else {
        this.showSchool = true;
      }
      this.progressBarsIndex--;
      return
    }

    if (this.showParents
      && next
    ) {
      this.showParents = false;
      this.showSummary = true;
      this.progressBarsIndex++;
      return;
    }

    if (this.showSummary
      && !next
    ) {
      this.showSummary = false;
      if (this.schedFormGroup.value.presentation) {
        this.showParents = true;
      } else if (this.schedFormGroup.value.classroomActivities) {
        this.showActivities = true;
      } else if (this.schedFormGroup.value.asvabTest) {
        this.showPTI1 = true;
      } else if (this.schedFormGroup.value.pti) {
        this.showPTI2 = true;
      } else {
        this.showSchool = true;
      }
      this.progressBarsIndex--;
      return
    }
  }

  scrollToTop() {
    setTimeout(function() {
      document.getElementById('topSection').scrollIntoView();
    }, 100);
  }

  progressBarsToShowInital() {
    if (this.schedFormGroup.value.asvabTest) {
      this.progressBarsTotal = this.progressBarsTotal + 2;
    }

    if (!this.schedFormGroup.value.asvabTest && this.schedFormGroup.value.pti) {
      this.progressBarsTotal = this.progressBarsTotal + 1;
    }

    if (this.schedFormGroup.value.classroomActivities) {
      this.progressBarsTotal = this.progressBarsTotal + 1;
    }

    if (this.schedFormGroup.value.presentation) {
      this.progressBarsTotal = this.progressBarsTotal + 1;
    }
  }

  focusMissingField() {
    if(this.isIE) {
      if(this.showTest && !this.schedFormGroup.value.testFirstChoiceStartTime) {
        this.focusForIE = true;
        setTimeout(function() {
          document.getElementById('testFirstChoiceStartTime').scrollIntoView();
        }, 500);
      }

      if (this.showPTI1 && this.schedFormGroup.value.pti && !this.schedFormGroup.value.ptiFirstChoiceStartTime) {
        this.focusForIE = true;
        setTimeout(function() {
          document.getElementById('pti1FirstChoiceStartTime').scrollIntoView();
        }, 500);
      }

      if (this.showPTI2 && this.schedFormGroup.value.pti && !this.schedFormGroup.value.ptiFirstChoiceStartTime) {
        this.focusForIE = true;
        setTimeout(function() {
          document.getElementById('pti2FirstChoiceStartTime').scrollIntoView();
        }, 500);
      }

      if (this.showActivities && !this.schedFormGroup.value.activityFirstChoiceStartTime) {
        this.focusForIE = true;
        setTimeout(function() {
          document.getElementById('activityFirstChoiceStartTime').scrollIntoView();
        }, 500);
      }

      if (this.showParents && !this.schedFormGroup.value.parentFirstChoiceStartTime){
        this.focusForIE = true;
        setTimeout(function() {
          document.getElementById('parentFirstChoiceStartTime').scrollIntoView();
        }, 500);
      }
    }
  }



  onShareWithCounselor() {
    this.cursorStyle = { 'cursor': 'wait' };
    this.errorMessage = undefined;
    const email = this.studentFormGroup.value.counselorEmail;
    const studentHeardUs = this.studentFormGroup.value.studentHeardUs;
    const studentHeardUsOther = this.studentFormGroup.value.studentHeardUsOther;

    if (this.studentFormGroup.value.recaptchaResponse === '') {  // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {

      const emailObject = {
        email: email,
        recaptcha: this.studentFormGroup.value.recaptchaResponse,
        studentHeardUs: studentHeardUs,
        studentHeardUsOther: studentHeardUsOther
      };

      const self = this;
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactUs/shareWithCounselor`);
      return this._http.httpHelper('POST', fullUrl, null, emailObject, true, 'aws')
        .then(function (success) {
          self.ga(self.path + '#BRING_ASVAB_FORM_STUDENT_SHARE');

          const data = {
            'title': 'Schedule Notification',
            'message': 'Your information was submitted successfully.'
          };
          const dialogRef = self._dialog.open(MessageDialogComponent, {
            data: data
          });

          self._dialogRef.close();
          return;
        })
        .catch(error => {
          console.error('Error: ' + error);
          if (!error['message'] || error['message'] === 'null') {
            self.errorMessage = 'There was an error proccessing your request. Please try again.';
          } else {
            self.errorMessage = error['message'];
          }
          self.responseOne = '';
          self.formDisabled = false;
          self.cursorStyle = { 'cursor': 'pointer' };
        });
    }
  }

  ga(param: string) {

    this._googleAnalyticsService.trackClick(param);
  }

  onStudentHeardChange() {
    if (this.studentFormGroup.value.studentHeardUs === 'other') {
      this.heardOther = true;
    } else {
      this.heardOther = false;
    }
  }

  onScheduleSubmit() {
    this.errorMessage = undefined;
    this.formDisabled = true;

    if (this.schedFormGroup.value.recaptcha === '') { // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {

      const emailObject = {
        title: this.schedFormGroup.value.title,
        recaptcha: this.schedFormGroup.value.recaptcha,
        position: this.schedFormGroup.value.position,
        firstName: this.schedFormGroup.value.firstName,
        lastName: this.schedFormGroup.value.lastName,
        phone: this.schedFormGroup.value.phone,
        email: this.schedFormGroup.value.email,
        heardUs: this.schedFormGroup.value.heardUs,
        heardUsOther: this.schedFormGroup.value.heardUsOther,
        schoolName: this.schedFormGroup.value.schoolName,
        schoolAddress: this.schedFormGroup.value.schoolAddress,
        schoolCity: this.schedFormGroup.value.schoolCity,
        schoolCounty: this.schedFormGroup.value.schoolCounty,
        schoolState: this.schedFormGroup.value.schoolState,
        schoolZip: this.schedFormGroup.value.schoolZip,
        asvabActivities: this.getAsvabActivities(),
        asvabActivityFormats: this.getAsvabActivityFormats()
      };

      const self = this;
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactUs/scheduleEmail`);
      return this._http.httpHelper('POST', fullUrl, null, emailObject, true, 'aws')
        .then(function (success) {

          self.ga(self.path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT');

          const data = {
            'title': 'Schedule Notification',
            'message': 'Your information was submitted successfully.'
          };
          const dialogRef = self._dialog.open(MessageDialogComponent, {
            data: data,
            maxWidth: '300px'
          });

          self._dialogRef.close();
          return;
        })
        .catch(error => {
          if (!error['message'] || error['message'] === 'null') {
            self.errorMessage = 'There was an error proccessing your request. Please try again.';
          } else {
            self.errorMessage = error['message'];
          }
          self.schedFormGroup.value.recaptcha = '';
          self.formDisabled = false;
        });
    }
  }

  matDatePickerToDateObj(datePickerObj) {
    const tmpTestFirstChoiceDate = datePickerObj.split('-');
    return new Date(tmpTestFirstChoiceDate[0], tmpTestFirstChoiceDate[1] - 1, tmpTestFirstChoiceDate[2]);
  }

  inputTimeToDateObj(time) {
    const tmpTestFirstChoiceStartTime = time.split(':');
    return new Date(0, 0, 0, tmpTestFirstChoiceStartTime[0], tmpTestFirstChoiceStartTime[1]);
  }

  ngbTimerPickerToDateObj(ngbObj) {
    const tmpTestFirstChoiceStartTime = ngbObj;
    return new Date(0, 0, 0, tmpTestFirstChoiceStartTime.hour, tmpTestFirstChoiceStartTime.minute);
  }

  scheduleFormSubmitSummary() {
    this.errorMessage = undefined;
    this.formDisabled = true;

    if (this.schedFormGroup.value.recaptcha === '') { // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    }
    else 
    {
      this.errorMessage = undefined;

      // Test data
      var alternativeTimes = [];
      var that = this;
      var asvabTestReservations = this.testReservations.map(function(t) {
        alternativeTimes = [];

        if (!t.testFirstChoiceDate || !t.testFirstChoiceStartTime) {
          return {}
        }

        t['testFirstChoiceDateObj'] = t.testFirstChoiceDate;
        t['testSecondChoiceDateObj'] = t.testSecondChoiceDate;
        t['testThirdChoiceDateObj'] = t.testThirdChoiceDate;
        t['testFirstChoiceStartTimeObj'] = '';
        t['testSecondChoiceStartTimeObj'] = '';
        t['testThirdChoiceStartTimeObj'] = '';

        if (!that.isIE && !(t.testFirstChoiceDate instanceof Date)) {
          const tmpTestFirstChoiceDate = t.testFirstChoiceDate.split('-');
          t.testFirstChoiceDateObj = new Date(tmpTestFirstChoiceDate[0], tmpTestFirstChoiceDate[1] - 1, tmpTestFirstChoiceDate[2]);

          if (t.testSecondChoiceDate) {
            const tmpTestSecondChoiceDate = t.testSecondChoiceDate.split('-');
            t.testSecondChoiceDateObj = new Date(tmpTestSecondChoiceDate[0], tmpTestSecondChoiceDate[1] - 1, tmpTestSecondChoiceDate[2]);
          }

          if (t.testThirdChoiceDate) {
            const tmpTestThirdChoiceDate = t.testThirdChoiceDate.split('-');
            t.testThirdChoiceDateObj = new Date(tmpTestThirdChoiceDate[0], tmpTestThirdChoiceDate[1] - 1, tmpTestThirdChoiceDate[2]);
          }

          if (t.testFirstChoiceStartTime) {
            const tmpTestFirstChoiceStartTime = t.testFirstChoiceStartTime.split(':');
            t.testFirstChoiceStartTimeObj = new Date(0, 0, 0, tmpTestFirstChoiceStartTime[0], tmpTestFirstChoiceStartTime[1]);
          }

          if (t.testSecondChoiceStartTime) {
            const tmpTestSecondChoiceStartTime = t.testSecondChoiceStartTime.split(':');
            t.testSecondChoiceStartTimeObj = new Date(0, 0, 0, tmpTestSecondChoiceStartTime[0], tmpTestSecondChoiceStartTime[1]);
          }

          if (t.testThirdChoiceStartTime) {
            const tmpTestThirdChoiceStartTime = t.testThirdChoiceStartTime.split(':');
            t.testThirdChoiceStartTimeObj = new Date(0, 0, 0, tmpTestThirdChoiceStartTime[0], tmpTestThirdChoiceStartTime[1]);
          }
        }

        if (that.isIE && !(t.testFirstChoiceStartTime instanceof Date)) {
        // if (!(t.testFirstChoiceStartTime instanceof Date)) {
          if (t.testFirstChoiceStartTime) {
            const tmpTestFirstChoiceStartTime = t.testFirstChoiceStartTime;
            t.testFirstChoiceStartTimeObj = new Date(0, 0, 0, tmpTestFirstChoiceStartTime.hour, tmpTestFirstChoiceStartTime.minute);
          }

          if (t.testSecondChoiceStartTime) {
            const tmpTestSecondChoiceStartTime = t.testSecondChoiceStartTime;
            t.testSecondChoiceStartTimeObj = new Date(0, 0, 0, tmpTestSecondChoiceStartTime.hour, tmpTestSecondChoiceStartTime.minute);
          }

          if (t.testThirdChoiceStartTime) {
            const tmpTestThirdChoiceStartTime = t.testThirdChoiceStartTime;
            t.testThirdChoiceStartTimeObj = new Date(0, 0, 0, tmpTestThirdChoiceStartTime.hour, tmpTestThirdChoiceStartTime.minute);
          }
        }

        if (t.testSecondChoiceDate && t.testSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testSecondChoiceDateObj.getFullYear(), t.testSecondChoiceDateObj.getMonth(), t.testSecondChoiceDateObj.getDate(), t.testSecondChoiceStartTimeObj.getHours(), t.testSecondChoiceStartTimeObj.getMinutes(), t.testSecondChoiceStartTimeObj.getSeconds())})
        } else if (t.testSecondChoiceDate && !t.testSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testSecondChoiceDateObj.getFullYear(), t.testSecondChoiceDateObj.getMonth(), t.testSecondChoiceDateObj.getDate(), t.testFirstChoiceStartTimeObj.getHours(), t.testFirstChoiceStartTimeObj.getMinutes(), t.testFirstChoiceStartTimeObj.getSeconds())})
        } else if (!t.testSecondChoiceDate && t.testSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testFirstChoiceDateObj.getFullYear(), t.testFirstChoiceDateObj.getMonth(), t.testFirstChoiceDateObj.getDate(), t.testSecondChoiceStartTimeObj.getHours(), t.testSecondChoiceStartTimeObj.getMinutes(), t.testSecondChoiceStartTimeObj.getSeconds())})
        }

        if (t.testThirdChoiceDate && t.testThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testThirdChoiceDateObj.getFullYear(), t.testThirdChoiceDateObj.getMonth(), t.testThirdChoiceDateObj.getDate(), t.testThirdChoiceStartTimeObj.getHours(), t.testThirdChoiceStartTimeObj.getMinutes(), t.testThirdChoiceStartTimeObj.getSeconds())})
        } else if (t.testThirdChoiceDate && !t.testThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testThirdChoiceDateObj.getFullYear(), t.testThirdChoiceDateObj.getMonth(), t.testThirdChoiceDateObj.getDate(), t.testFirstChoiceStartTimeObj.getHours(), t.testFirstChoiceStartTimeObj.getMinutes(), t.testFirstChoiceStartTimeObj.getSeconds())})
        } else if (!t.testThirdChoiceDate && t.testThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.testFirstChoiceDateObj.getFullYear(), t.testFirstChoiceDateObj.getMonth(), t.testFirstChoiceDateObj.getDate(), t.testThirdChoiceStartTimeObj.getHours(), t.testThirdChoiceStartTimeObj.getMinutes(), t.testThirdChoiceStartTimeObj.getSeconds())})
        }

        return {
          testDate: new Date(t.testFirstChoiceDateObj.getFullYear(), t.testFirstChoiceDateObj.getMonth(), t.testFirstChoiceDateObj.getDate(), t.testFirstChoiceStartTimeObj.getHours(), t.testFirstChoiceStartTimeObj.getMinutes(), t.testFirstChoiceStartTimeObj.getSeconds()),
          alternativeTimes: alternativeTimes,
          estimated10thGradeTesters: t.testStudentNum10thGrade,
          estimated11thGradeTesters: t.testStudentNum11thGrade,
          estimated12thGradeTesters: t.testStudentNum12thGrade,
          estimatedPostSecondaryTesters: t.testStudentNumOther,
          isInterestedInICat: t.testInterestInIcat,
          numberOfAccommodatedStudents: t.testNumOfAccommodatedStudents,
          location: t.testLocation,
          remarks: t.testRemarks,
          inNeedOfLapboards: t.testNeedLapboards,
          numberOfLapboads: t.testNeedLapboardsNumber,
          scoreReleaseOptionId: parseInt(t.testScoreReleaseOption),
        }
      })

      // PTI data
      var ptiReservations = this.ptiReservations.map(function(t) {
        alternativeTimes = [];

        var today = new Date();
        var ptiFirstDate = (t.ptiFirstChoiceDate || t.ptiFirstChoiceStartTime || t.ptiSecondChoiceDate || t.ptiSecondChoiceStartTime || t.ptiThirdChoiceDate || t.ptiThirdChoiceStartTime)
          ? today
          : null;

        t['ptiFirstChoiceDateObj'] = t.ptiFirstChoiceDate;
        t['ptiSecondChoiceDateObj'] = t.ptiSecondChoiceDate;
        t['ptiThirdChoiceDateObj'] = t.ptiThirdChoiceDate;
        t['ptiFirstChoiceStartTimeObj'] = '';
        t['ptiSecondChoiceStartTimeObj'] = '';
        t['ptiThirdChoiceStartTimeObj'] = '';

        if (!that.isIE && !(t.ptiFirstChoiceDate instanceof Date)) {
          if (t.ptiFirstChoiceDate) {
            const tmpPtiFirstChoiceDate = t.ptiFirstChoiceDate.split('-');
            t.ptiFirstChoiceDateObj = new Date(tmpPtiFirstChoiceDate[0], tmpPtiFirstChoiceDate[1] - 1, tmpPtiFirstChoiceDate[2]);
          }

          if (t.ptiSecondChoiceDate) {
            const tmpPtiSecondChoiceDate = t.ptiSecondChoiceDate.split('-');
            t.ptiSecondChoiceDateObj = new Date(tmpPtiSecondChoiceDate[0], tmpPtiSecondChoiceDate[1] - 1, tmpPtiSecondChoiceDate[2]);
          }

          if (t.ptiThirdChoiceDate) {
            const tmpPtiThirdChoiceDate = t.ptiThirdChoiceDate.split('-');
            t.ptiThirdChoiceDateObj = new Date(tmpPtiThirdChoiceDate[0], tmpPtiThirdChoiceDate[1] - 1, tmpPtiThirdChoiceDate[2]);
          }

          if (t.ptiFirstChoiceStartTime) {
            const tmpPtiFirstChoiceStartTime = t.ptiFirstChoiceStartTime.split(':');
            t.ptiFirstChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiFirstChoiceStartTime[0], tmpPtiFirstChoiceStartTime[1]);
          }

          if (t.ptiSecondChoiceStartTime) {
            const tmpPtiSecondChoiceStartTime = t.ptiSecondChoiceStartTime.split(':');
            t.ptiSecondChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiSecondChoiceStartTime[0], tmpPtiSecondChoiceStartTime[1]);
          }

          if (t.ptiThirdChoiceStartTime) {
            const tmpPtiThirdChoiceStartTime = t.ptiThirdChoiceStartTime.split(':');
            t.ptiThirdChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiThirdChoiceStartTime[0], tmpPtiThirdChoiceStartTime[1]);
          }
        }

        if (that.isIE && !(t.ptiFirstChoiceStartTime instanceof Date)) {
          if (t.ptiFirstChoiceStartTime) {
            const tmpPtiFirstChoiceStartTime = t.ptiFirstChoiceStartTime;
            t.ptiFirstChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiFirstChoiceStartTime.hour, tmpPtiFirstChoiceStartTime.minute);
          }

          if (t.ptiSecondChoiceStartTime) {
            const tmpPtiSecondChoiceStartTime = t.ptiSecondChoiceStartTime;
            t.ptiSecondChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiSecondChoiceStartTime.hour, tmpPtiSecondChoiceStartTime.minute);
          }

          if (t.ptiThirdChoiceStartTime) {
            const tmpPtiThirdChoiceStartTime = t.ptiThirdChoiceStartTime;
            t.ptiThirdChoiceStartTimeObj = new Date(0, 0, 0, tmpPtiThirdChoiceStartTime.hour, tmpPtiThirdChoiceStartTime.minute);
          }
        }

        if (t.ptiFirstChoiceDate) {
          ptiFirstDate = new Date(t.ptiFirstChoiceDateObj.getFullYear(), t.ptiFirstChoiceDateObj.getMonth(), t.ptiFirstChoiceDateObj.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())
        }

        if (t.ptiFirstChoiceStartTime) {
          ptiFirstDate = new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiFirstChoiceStartTimeObj.getHours(), t.ptiFirstChoiceStartTimeObj.getMinutes(), t.ptiFirstChoiceStartTimeObj.getSeconds());
        }

        if (t.ptiSecondChoiceDate && t.ptiSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.ptiSecondChoiceDateObj.getFullYear(), t.ptiSecondChoiceDateObj.getMonth(), t.ptiSecondChoiceDateObj.getDate(), t.ptiSecondChoiceStartTimeObj.getHours(), t.ptiSecondChoiceStartTimeObj.getMinutes(), t.ptiSecondChoiceStartTimeObj.getSeconds())})
        } else if (t.ptiSecondChoiceDate && !t.ptiSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.ptiSecondChoiceDateObj.getFullYear(), t.ptiSecondChoiceDateObj.getMonth(), t.ptiSecondChoiceDateObj.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())})
        } else if (!t.ptiSecondChoiceDate && t.ptiSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiSecondChoiceStartTimeObj.getHours(), t.ptiSecondChoiceStartTimeObj.getMinutes(), t.ptiSecondChoiceStartTimeObj.getSeconds())})
        }

        if (t.ptiThirdChoiceDate && t.ptiThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.ptiThirdChoiceDateObj.getFullYear(), t.ptiThirdChoiceDateObj.getMonth(), t.ptiThirdChoiceDateObj.getDate(), t.ptiThirdChoiceStartTimeObj.getHours(), t.ptiThirdChoiceStartTimeObj.getMinutes(), t.ptiThirdChoiceStartTimeObj.getSeconds())})
        } else if (t.ptiThirdChoiceDate && !t.ptiThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(t.ptiThirdChoiceDateObj.getFullYear(), t.ptiThirdChoiceDateObj.getMonth(), t.ptiThirdChoiceDateObj.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())})
        } else if (!t.ptiThirdChoiceDate && t.ptiThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiThirdChoiceStartTimeObj.getHours(), t.ptiThirdChoiceStartTimeObj.getMinutes(), t.ptiThirdChoiceStartTimeObj.getSeconds())})
        }

        return {
          testDate: ptiFirstDate,
          alternativeTimes: alternativeTimes,
          estimated10thGradeTesters: t.ptiStudentNum10thGrade,
          estimated11thGradeTesters: t.ptiStudentNum11thGrade,
          estimated12thGradeTesters: t.ptiStudentNum12thGrade,
          estimatedPostSecondaryTesters: t.ptiStudentNumOther,
          location: t.ptiLocation,
          remarks: t.ptiRemarks,
          isProjectorAvailable: t.ptiProjectorAvailable,
          isInternetAvailable: t.ptiInternetAvailable,
          willStudentHaveComputerOrPhone: t.ptiComputerOrPhone,
          inNeedOfLapboards: t.ptiNeedLapboards,
          numberOfLapboads: t.ptiNeedLapboardsNumber,
        }
      })

      // Activity data
      var classroomActivityReservation = null;
      if (this.schedFormGroup.value.classroomActivities) {
        alternativeTimes = [];

        if (!this.schedFormGroup.value.activityFirstChoiceDate || !this.schedFormGroup.value.activityFirstChoiceStartTime) {
          return {}
        }

        let activityFirstChoiceDate = this.schedFormGroup.value.activityFirstChoiceDate;
        let activitySecondChoiceDate = this.schedFormGroup.value.activitySecondChoiceDate;
        let activityThirdChoiceDate = this.schedFormGroup.value.activityThirdChoiceDate;
        let activityFirstChoiceStartTime = this.schedFormGroup.value.activityFirstChoiceStartTime;
        let activitySecondChoiceStartTime = this.schedFormGroup.value.activitySecondChoiceStartTime;
        let activityThirdChoiceStartTime = this.schedFormGroup.value.activityThirdChoiceStartTime;

        if (!that.isIE && !(activityFirstChoiceDate instanceof Date)) {
          if (activityFirstChoiceDate) {
            const tmpActivityFirstChoiceDate = activityFirstChoiceDate.split('-');
            activityFirstChoiceDate = new Date(tmpActivityFirstChoiceDate[0], tmpActivityFirstChoiceDate[1] - 1, tmpActivityFirstChoiceDate[2]);
          }

          if (activitySecondChoiceDate) {
            const tmpActivitySecondChoiceDate = activitySecondChoiceDate.split('-');
            activitySecondChoiceDate = new Date(tmpActivitySecondChoiceDate[0], tmpActivitySecondChoiceDate[1] - 1, tmpActivitySecondChoiceDate[2]);
          }

          if (activityThirdChoiceDate) {
            const tmpActivityThirdChoiceDate = activityThirdChoiceDate.split('-');
            activityThirdChoiceDate = new Date(tmpActivityThirdChoiceDate[0], tmpActivityThirdChoiceDate[1] - 1, tmpActivityThirdChoiceDate[2]);
          }

          if (activityFirstChoiceStartTime) {
            const tmpPtiFirstChoiceStartTime = activityFirstChoiceStartTime.split(':');
            activityFirstChoiceStartTime = new Date(0, 0, 0, tmpPtiFirstChoiceStartTime[0], tmpPtiFirstChoiceStartTime[1]);
          }

          if (activitySecondChoiceStartTime) {
            const tmpPtiSecondChoiceStartTime = activitySecondChoiceStartTime.split(':');
            activitySecondChoiceStartTime = new Date(0, 0, 0, tmpPtiSecondChoiceStartTime[0], tmpPtiSecondChoiceStartTime[1]);
          }

          if (activityThirdChoiceStartTime) {
            const tmpPtiThirdChoiceStartTime = activityThirdChoiceStartTime.split(':');
            activityThirdChoiceStartTime = new Date(0, 0, 0, tmpPtiThirdChoiceStartTime[0], tmpPtiThirdChoiceStartTime[1]);
          }
        }

        if (that.isIE && !(activityFirstChoiceStartTime instanceof Date)) {
          if (activityFirstChoiceStartTime) {
            const tmpActivityFirstChoiceStartTime = activityFirstChoiceStartTime;
            activityFirstChoiceStartTime = new Date(0, 0, 0, tmpActivityFirstChoiceStartTime.hour, tmpActivityFirstChoiceStartTime.minute);
          }

          if (activitySecondChoiceStartTime) {
            const tmpActivitySecondChoiceStartTime = activitySecondChoiceStartTime;
            activitySecondChoiceStartTime = new Date(0, 0, 0, tmpActivitySecondChoiceStartTime.hour, tmpActivitySecondChoiceStartTime.minute);
          }

          if (activityThirdChoiceStartTime) {
            const tmpActivityThirdChoiceStartTime = activityThirdChoiceStartTime;
            activityThirdChoiceStartTime = new Date(0, 0, 0, tmpActivityThirdChoiceStartTime.hour, tmpActivityThirdChoiceStartTime.minute);
          }
        }


        if (activitySecondChoiceDate && activitySecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activitySecondChoiceDate.getFullYear(), activitySecondChoiceDate.getMonth(), activitySecondChoiceDate.getDate(), activitySecondChoiceStartTime.getHours(), activitySecondChoiceStartTime.getMinutes(), activitySecondChoiceStartTime.getSeconds())})
        } else if (activitySecondChoiceDate && !activitySecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activitySecondChoiceDate.getFullYear(), activitySecondChoiceDate.getMonth(), activitySecondChoiceDate.getDate(), activityFirstChoiceStartTime.getHours(), activityFirstChoiceStartTime.getMinutes(), activityFirstChoiceStartTime.getSeconds())})
        } else if (!activitySecondChoiceDate && activitySecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activityFirstChoiceDate.getFullYear(), activityFirstChoiceDate.getMonth(), activityFirstChoiceDate.getDate(), activitySecondChoiceStartTime.getHours(), activitySecondChoiceStartTime.getMinutes(), activitySecondChoiceStartTime.getSeconds())})
        }

        if (activityThirdChoiceDate && activityThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activityThirdChoiceDate.getFullYear(), activityThirdChoiceDate.getMonth(), activityThirdChoiceDate.getDate(), activityThirdChoiceStartTime.getHours(), activityThirdChoiceStartTime.getMinutes(), activityThirdChoiceStartTime.getSeconds())})
        } else if (activityThirdChoiceDate && !activityThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activityThirdChoiceDate.getFullYear(), activityThirdChoiceDate.getMonth(), activityThirdChoiceDate.getDate(), activityFirstChoiceStartTime.getHours(), activityFirstChoiceStartTime.getMinutes(), activityFirstChoiceStartTime.getSeconds())})
        } else if (!activityThirdChoiceDate && activityThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(activityFirstChoiceDate.getFullYear(), activityFirstChoiceDate.getMonth(), activityFirstChoiceDate.getDate(), activityThirdChoiceStartTime.getHours(), activityThirdChoiceStartTime.getMinutes(), activityThirdChoiceStartTime.getSeconds())})
        }

        classroomActivityReservation = {
          testDate: new Date(activityFirstChoiceDate.getFullYear(), activityFirstChoiceDate.getMonth(), activityFirstChoiceDate.getDate(), activityFirstChoiceStartTime.getHours(), activityFirstChoiceStartTime.getMinutes(), activityFirstChoiceStartTime.getSeconds()),
          alternativeTimes: alternativeTimes,
          estimatedPresentationAttendees: this.schedFormGroup.value.activityAttendees,
          location: this.schedFormGroup.value.activityLocation,
          remarks: this.schedFormGroup.value.activityRemarks,
        }
      }

      // Parents data
      var parentReservation = null;
      if (this.schedFormGroup.value.presentation) {
        alternativeTimes = [];

        if (!this.schedFormGroup.value.parentFirstChoiceDate || !this.schedFormGroup.value.parentFirstChoiceStartTime) {
          return {}
        }

        let parentFirstChoiceDate = this.schedFormGroup.value.parentFirstChoiceDate;
        let parentSecondChoiceDate = this.schedFormGroup.value.parentSecondChoiceDate;
        let parentThirdChoiceDate = this.schedFormGroup.value.parentThirdChoiceDate;
        let parentFirstChoiceStartTime = this.schedFormGroup.value.parentFirstChoiceStartTime;
        let parentSecondChoiceStartTime = this.schedFormGroup.value.parentSecondChoiceStartTime;
        let parentThirdChoiceStartTime = this.schedFormGroup.value.parentThirdChoiceStartTime;

        if (!that.isIE && !(parentFirstChoiceDate instanceof Date)) {
          if (parentFirstChoiceDate) {
            const tmpparentFirstChoiceDate = parentFirstChoiceDate.split('-');
            parentFirstChoiceDate = new Date(tmpparentFirstChoiceDate[0], tmpparentFirstChoiceDate[1] - 1, tmpparentFirstChoiceDate[2]);
          }

          if (parentSecondChoiceDate) {
            const tmpparentSecondChoiceDate = parentSecondChoiceDate.split('-');
            parentSecondChoiceDate = new Date(tmpparentSecondChoiceDate[0], tmpparentSecondChoiceDate[1] - 1, tmpparentSecondChoiceDate[2]);
          }

          if (parentThirdChoiceDate) {
            const tmpparentThirdChoiceDate = parentThirdChoiceDate.split('-');
            parentThirdChoiceDate = new Date(tmpparentThirdChoiceDate[0], tmpparentThirdChoiceDate[1] - 1, tmpparentThirdChoiceDate[2]);
          }

          if (parentFirstChoiceStartTime) {
            const tmpPtiFirstChoiceStartTime = parentFirstChoiceStartTime.split(':');
            parentFirstChoiceStartTime = new Date(0, 0, 0, tmpPtiFirstChoiceStartTime[0], tmpPtiFirstChoiceStartTime[1]);
          }

          if (parentSecondChoiceStartTime) {
            const tmpPtiSecondChoiceStartTime = parentSecondChoiceStartTime.split(':');
            parentSecondChoiceStartTime = new Date(0, 0, 0, tmpPtiSecondChoiceStartTime[0], tmpPtiSecondChoiceStartTime[1]);
          }

          if (parentThirdChoiceStartTime) {
            const tmpPtiThirdChoiceStartTime = parentThirdChoiceStartTime.split(':');
            parentThirdChoiceStartTime = new Date(0, 0, 0, tmpPtiThirdChoiceStartTime[0], tmpPtiThirdChoiceStartTime[1]);
          }
        }

        if (that.isIE && !(parentFirstChoiceStartTime instanceof Date)) {
          if (parentFirstChoiceStartTime) {
            const tmpparentFirstChoiceStartTime = parentFirstChoiceStartTime;
            parentFirstChoiceStartTime = new Date(0, 0, 0, tmpparentFirstChoiceStartTime.hour, tmpparentFirstChoiceStartTime.minute);
          }

          if (parentSecondChoiceStartTime) {
            const tmpparentSecondChoiceStartTime = parentSecondChoiceStartTime;
            parentSecondChoiceStartTime = new Date(0, 0, 0, tmpparentSecondChoiceStartTime.hour, tmpparentSecondChoiceStartTime.minute);
          }

          if (parentThirdChoiceStartTime) {
            const tmpparentThirdChoiceStartTime = parentThirdChoiceStartTime;
            parentThirdChoiceStartTime = new Date(0, 0, 0, tmpparentThirdChoiceStartTime.hour, tmpparentThirdChoiceStartTime.minute);
          }
        }

        if (parentSecondChoiceDate && parentSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentSecondChoiceDate.getFullYear(), parentSecondChoiceDate.getMonth(), parentSecondChoiceDate.getDate(), parentSecondChoiceStartTime.getHours(), parentSecondChoiceStartTime.getMinutes(), parentSecondChoiceStartTime.getSeconds())})
        } else if (parentSecondChoiceDate && !parentSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentSecondChoiceDate.getFullYear(), parentSecondChoiceDate.getMonth(), parentSecondChoiceDate.getDate(), parentFirstChoiceStartTime.getHours(), parentFirstChoiceStartTime.getMinutes(), parentFirstChoiceStartTime.getSeconds())})
        } else if (!parentSecondChoiceDate && parentSecondChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentFirstChoiceDate.getFullYear(), parentFirstChoiceDate.getMonth(), parentFirstChoiceDate.getDate(), parentSecondChoiceStartTime.getHours(), parentSecondChoiceStartTime.getMinutes(), parentSecondChoiceStartTime.getSeconds())})
        }

        if (parentThirdChoiceDate && parentThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentThirdChoiceDate.getFullYear(), parentThirdChoiceDate.getMonth(), parentThirdChoiceDate.getDate(), parentThirdChoiceStartTime.getHours(), parentThirdChoiceStartTime.getMinutes(), parentThirdChoiceStartTime.getSeconds())})
        } else if (parentThirdChoiceDate && !parentThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentThirdChoiceDate.getFullYear(), parentThirdChoiceDate.getMonth(), parentThirdChoiceDate.getDate(), parentFirstChoiceStartTime.getHours(), parentFirstChoiceStartTime.getMinutes(), parentFirstChoiceStartTime.getSeconds())})
        } else if (!parentThirdChoiceDate && parentThirdChoiceStartTime) {
          alternativeTimes.push({testDate: new Date(parentFirstChoiceDate.getFullYear(), parentFirstChoiceDate.getMonth(), parentFirstChoiceDate.getDate(), parentThirdChoiceStartTime.getHours(), parentThirdChoiceStartTime.getMinutes(), parentThirdChoiceStartTime.getSeconds())})
        }

        parentReservation = {
          testDate: new Date(parentFirstChoiceDate.getFullYear(), parentFirstChoiceDate.getMonth(), parentFirstChoiceDate.getDate(), parentFirstChoiceStartTime.getHours(), parentFirstChoiceStartTime.getMinutes(), parentFirstChoiceStartTime.getSeconds()),
          alternativeTimes: alternativeTimes,
          estimatedPresentationAttendees: this.schedFormGroup.value.parentAttendees,
          location: this.schedFormGroup.value.parentLocation,
          remarks: this.schedFormGroup.value.parentRemarks,
        }
      }

      const emailObject = {
        title: this.schedFormGroup.value.title,
        recaptcha: this.schedFormGroup.value.recaptcha,
        position: this.schedFormGroup.value.position,
        firstName: this.schedFormGroup.value.firstName,
        lastName: this.schedFormGroup.value.lastName,
        phone: this.schedFormGroup.value.phone,
        phoneExt: this.schedFormGroup.value.phoneExt,
        email: this.schedFormGroup.value.email,
        heardUs: this.schedFormGroup.value.heardUs,
        heardUsOther: this.schedFormGroup.value.heardUsOther,
        schoolName: this.schedFormGroup.value.schoolName,
        schoolAddress: this.schedFormGroup.value.schoolAddress,
        schoolAddress2: this.schedFormGroup.value.schoolAddress2,
        schoolCity: this.schedFormGroup.value.schoolCity,
        // schoolCounty: this.schedFormGroup.value.schoolCounty,
        schoolState: this.schedFormGroup.value.schoolState,
        schoolZip: this.schedFormGroup.value.schoolZip,
        asvabActivities: this.getAsvabActivities(),
        asvabActivityFormats: this.getAsvabActivityFormats(),

        pop10thGrade: this.schedFormGroup.value.studentNum10thGrade,
        pop11thGrade: this.schedFormGroup.value.studentNum11thGrade,
        pop12thGrade: this.schedFormGroup.value.studentNum12thGrade,
        popPostSecondary: this.schedFormGroup.value.studentNumOther,
        notifyEmailAddresses: this.schedFormGroup.value.notifyEmailAddresses,

        asvabTestReservations: asvabTestReservations,
        ptiReservations: ptiReservations,
        classroomActivityReservation: classroomActivityReservation,
        parentReservation: parentReservation,

        timeZoneOffset: new Date().getTimezoneOffset()/60,
      };

      const self = this;
      const fullUrl = this._configService.getAwsFullUrl(`/api/contactUs/scheduleEmail`);
      return this._http.httpHelper('POST', fullUrl, null, emailObject, true, 'aws')
        .then(function (success) {

          self.ga(self.path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT');

          window.location.href = self.path + '?bringToSchool=submitted';

          return;
        })
        .catch(data => {

          self.formDisabled = false;
          
          if (data.errors) {
            var message = '';
            if (data.title) {
              // convert server response property "errors" to base obj
              var errors = data.errors;
              var error = {};
              // error object is using pascal case, need to convert to camel case
              for (var key in errors) {
                error[key.charAt(0).toLowerCase() + key.substring(1, key.length)] = errors[key];
              }
            }
            for (var key in emailObject) {
              if (error && error[key]) {
                message += '\n' + error[key][0];
              }
            }
            // if status is 400 (bad request), show validation message, else show generic error message
            if (data.status === 400 && message !== '') {
              self.errorMessage = message;
            } else {
              self.errorMessage = 'There was an error processing your request. Please try again.';
              self.schedFormGroup.value.recaptcha = '';
            }
          }
          // self.formDisabled = false;
          else {
            if (!data['title'] || data['title'] === 'null') {
              self.errorMessage = 'There was an error proccessing your request. Please try again.';
            } else {
              self.errorMessage = data['title'];
            }
          }

          // if (!error['message'] || error['message'] === 'null') {
          //   self.errorMessage = 'There was an error proccessing your request. Please try again.';
          // } else {
          //   self.errorMessage = error['message'];
          // }
          // self.schedFormGroup.value.recaptcha = '';
          // self.formDisabled = false;
        });
    }
  }

  getAsvabActivities(): any {
    return {
      asvabTest: (this.schedFormGroup.value.asvabTest === '') ? false : this.schedFormGroup.value.asvabTest,
      pti: this.schedFormGroup.value.pti,
      classroomActivities: this.schedFormGroup.value.classroomActivities,
      presentation: this.schedFormGroup.value.presentation
    }
  }

  getAsvabActivityFormats(): any {
    return {
      pti: this.getActivityFormatValue(this.schedFormGroup.value.ptiVirtual, this.schedFormGroup.value.ptiInPerson),
      classroomActivities: this.getActivityFormatValue(this.schedFormGroup.value.classroomActivitiesVirtual, this.schedFormGroup.value.classroomActivitiesInPerson),
      presentation: this.getActivityFormatValue(this.schedFormGroup.value.presentationVirtual, this.schedFormGroup.value.presentationInPerson)
    }
  }

  getActivityFormatValue(virtual: boolean, inPerson: boolean): string {
    if (virtual === true  && inPerson === true ) {
      return 'virtual,in-person';
    } else {
      if (virtual === true) {
        return 'virtual';
      }
      if (inPerson === true) {
        return 'in-person';
      }
    }
    return '';
  }

  onHeardChange() {
    if (this.schedFormGroup.value.heardUs === 'other') {
      this.heardOther = true;
    } else {
      this.heardOther = false;
    }
  }

  updateCheckboxes(groupName, value) {

    if (value === true) {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").enable();
        this.schedFormGroup.get("ptiInPerson").enable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").enable();
        this.schedFormGroup.get("classroomActivitiesInPerson").enable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").enable();
        this.schedFormGroup.get("presentationInPerson").enable();
      }
      this.markingMissingType();
    } else {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").disable();
        this.schedFormGroup.get("ptiInPerson").disable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").disable();
        this.schedFormGroup.get("classroomActivitiesInPerson").disable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").disable();
        this.schedFormGroup.get("presentationInPerson").disable();
      }
      this.markingMissingType();
    }
    this.setFormDisabled();
  }

  markingMissingType() {
    if (this.schedFormGroup.value.pti
      && !this.schedFormGroup.value.ptiVirtual
      && !this.schedFormGroup.value.ptiInPerson) {
        this.ptiMissing = true;
    } else {
      this.ptiMissing = false;
    }

    if (this.schedFormGroup.value.classroomActivities
      && !this.schedFormGroup.value.classroomActivitiesVirtual
      && !this.schedFormGroup.value.classroomActivitiesInPerson) {
        this.classroomActivitiesMissing = true;
    } else {
      this.classroomActivitiesMissing = false;
    }

    if (this.schedFormGroup.value.presentation
      && !this.schedFormGroup.value.presentationVirtual
      && !this.schedFormGroup.value.presentationInPerson) {
        this.presentationMissing = true;
    } else {
      this.presentationMissing = false;
    }
  }
}
