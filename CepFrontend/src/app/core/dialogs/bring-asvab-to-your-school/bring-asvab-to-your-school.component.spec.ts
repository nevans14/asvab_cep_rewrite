import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BringAsvabToYourSchoolComponent } from './bring-asvab-to-your-school.component';

describe('BringAsvabToYourSchoolComponent', () => {
  let component: BringAsvabToYourSchoolComponent;
  let fixture: ComponentFixture<BringAsvabToYourSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BringAsvabToYourSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BringAsvabToYourSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
