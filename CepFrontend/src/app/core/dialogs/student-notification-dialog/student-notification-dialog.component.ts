import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClassroomActivitySubmission, ClassroomActivitySubmissionComment } from 'app/core/models/classroomActivity';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { UserOccupationPlanComment } from 'app/core/models/userOccupationPlan.model';
import { PlanCalendarComponent } from 'app/plan-calendar/plan-calendar.component';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MentorService } from 'app/services/mentor.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { EmailSettingsDialogComponent } from '../email-settings-dialog/email-settings-dialog.component';

enum MessageType {
  ACTIVITY_COMMENT = 1,
  PLAN_COMMENT = 2,
  PORTFOLIO_COMMENT = 3,
  DIRECT_MESSAGE = 4
}

enum LogType {
  ACTIVITY_LOG = 1,
  PLAN_LOG = 2,
  PORTFOLIO_LOG = 3
}
@Component({
  selector: 'app-student-notification-dialog',
  templateUrl: './student-notification-dialog.component.html',
  styleUrls: ['./student-notification-dialog.component.scss']
})
export class StudentNotificationDialogComponent implements OnInit {

  public messages: any[];
  public currentUser: any;
  public isLoading: boolean;
  public unreadMsgCounter: number;
  public mentors: any[];
  public showReadItems: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
    private _classroomActivityService: ClassroomActivityService,
    private _portfolioService: PortfolioService,
    private _userService: UserService,
    public _dialogRef: MatDialogRef<StudentNotificationDialogComponent>,
    public _userOccupationPlanService: UserOccupationPlanService,
    private _utilityService: UtilityService,
    private _mentorService: MentorService,
    private _matDialog: MatDialog) {

    this.messages = [];
    this.currentUser = {};
    this.isLoading = true;
    this.unreadMsgCounter = 0;
    this.mentors = [];
    this.showReadItems = false;
  }

  ngOnInit() {

    this.getData().then(() => this.isLoading = false);

  }

  async getData() {
    let directMessages = this._userService.getDirectMessages(); //get all direct messages involving the current signed in user
    let userName = this._userService.getUsername();
    let unreadCommentsPortfolio = this._portfolioService.getPortfolioUnreadComments();
    let unreadCommentsPlan = this._userOccupationPlanService.getUnreadComments();
    let unreadActivityComments = this._classroomActivityService.getUnreadActivityComments();
    let mentors = await this._mentorService.getAll();
    //get all user occupation plans
    let userOccupationPlan = this._userOccupationPlanService.getAll();

    //get classroom activities
    let classroomActivities: any = await this._classroomActivityService.getActivities();
    await Promise.all(classroomActivities.map(async (activity: any) => {
      await Promise.all(activity.submissions.map(async (classroomActivitySubmission: ClassroomActivitySubmission) => {
        //for each submission retrieve its logs
        let activityLogs: any = await this._classroomActivityService.getMySubmissionLogs(classroomActivitySubmission.id);
        classroomActivitySubmission.logs = activityLogs;

      }))
    }))

    //get portfolio
    let portfolio = await this._portfolioService.getPortfolio();

    await Promise.all([directMessages, userName, unreadCommentsPortfolio, unreadCommentsPlan, userOccupationPlan, classroomActivities, portfolio, unreadActivityComments, mentors]).then((done: any) => {

      let directMessages = done[0];
      this.currentUser = done[1]; //current signed in user;
      let unreadCommentsPortfolio = done[2];
      let unreadCommentsPlan = done[3];
      let userOccupationPlan = done[4];
      let classroomActivities = done[5];
      let portfolio = done[6];
      let unreadActivityComments = done[7];
      this.mentors = done[8];

      this.buildMessageList(directMessages, unreadCommentsPortfolio, unreadCommentsPlan, userOccupationPlan, portfolio, classroomActivities, unreadActivityComments);

    });
  }

  buildMessageList(directMessages, unreadCommentsPortfolio, unreadCommentsPlan, userOccupationPlans, portfolio, classroomActivities, unreadActivityComments) {

    this.messages = [];
    this.unreadMsgCounter = 0;

    // ***** direct messages
    if (directMessages && directMessages.length > 0) {
      directMessages.forEach((dm: DirectMessage) => {

        if (!dm.hasBeenRead) {

          let messageItem = {
            id: dm.id,
            title: 'Direct Message',
            from: this.getMentorByEmail(dm.fromUser),
            isLog: false,
            message: dm.message,
            timestamp: this._utilityService.convertDateToLocalDate(dm.dateCreated),
            originalTimestamp: dm.dateCreated,
            hasBeenRead: dm.hasBeenRead,
            messageType: MessageType.DIRECT_MESSAGE,
            mentorLinkId: dm.mentorLinkId,
            showText: false //used for replying
          }
          this.unreadMsgCounter += 1;
          this.messages.push(messageItem);
        }

      })
    }

    // ***** portfolio comments
    if (unreadCommentsPortfolio && unreadCommentsPortfolio.length > 0) {
      unreadCommentsPortfolio.forEach((comment: any) => {

        let commentTo = comment.to.find(x => x.username == "You");

        if (commentTo) {
          let messageItem = {
            id: comment.id,
            title: 'Portfolio',
            from: this.getMentorByEmail(comment.from),
            isLog: false,
            message: comment.message,
            timestamp: this._utilityService.convertDateToLocalDate(comment.dateCreated),
            originalTimestamp: comment.dateCreated,
            hasBeenRead: false,
            messageType: MessageType.PORTFOLIO_COMMENT,
            showText: false, //used for replying
            recipientId: commentTo.id,
            mentorLinkId: this.getMentorLinkId(comment.from)
          }

          this.unreadMsgCounter += 1;
          this.messages.push(messageItem);
        }

      })
    }

    // ***** plan comments
    if (unreadCommentsPlan && unreadCommentsPlan.length > 0) {
      unreadCommentsPlan.forEach((plan: any) => {

        let userOccupationPlan = userOccupationPlans.find(x => x.id == plan.planId);
        let commentTo = plan.to.find(x => x.username == "You");

        if (commentTo) {

          let messageItem = {
            id: plan.id,
            title: (userOccupationPlan) ? userOccupationPlan.title : "Occupation Plan",
            from: this.getMentorByEmail(plan.from),
            isLog: false,
            message: plan.message,
            timestamp: this._utilityService.convertDateToLocalDate(plan.dateCreated),
            originalTimestamp: plan.dateCreated,
            hasBeenRead: false,
            messageType: MessageType.PLAN_COMMENT,
            showText: false, //used for replying
            recipientId: commentTo.id,
            planId: plan.planId,
            mentorLinkId: this.getMentorLinkId(plan.from)
          }

          this.unreadMsgCounter += 1;
          this.messages.push(messageItem);
        }

      })
    }

    // ***** activity comments
    if (unreadActivityComments && unreadActivityComments.length > 0) {
      unreadActivityComments.forEach((activity: any) => {

        let classRoomActivity = classroomActivities.find(x => x.id == activity.classroomActivityId);
        let commentTo = activity.to.find(x => x.username == "You");

        if (commentTo) {

          let messageItem = {
            id: activity.id,
            title: (classRoomActivity) ? classRoomActivity.title : "Activity",
            from: this.getMentorByEmail(activity.from),
            isLog: false,
            message: activity.message,
            timestamp: this._utilityService.convertDateToLocalDate(activity.dateCreated),
            originalTimestamp: activity.dateCreated,
            hasBeenRead: false,
            messageType: MessageType.ACTIVITY_COMMENT,
            showText: false, //used for replying
            classroomActivityId: activity.classroomActivityId,
            recipientId: commentTo.id,
            mentorLinkId: this.getMentorLinkId(activity.from)
          }

          this.unreadMsgCounter += 1;
          this.messages.push(messageItem);
        }

      })
    }

    // ***** portfolio logs
    if (portfolio && portfolio.logs && portfolio.logs.length > 0) {
      let sortedLogs = portfolio.logs.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
        return da - db;
      });

      let lastLog = sortedLogs[sortedLogs.length - 1];

      if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
        let messageItem = {
          id: lastLog.id,
          title: "Portfolio",
          from: this.getMentorByEmail(lastLog.mentorUsername),  //email of user
          isLog: true,
          message: "Reviewed",
          timestamp: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
          originalTimestamp: lastLog.dateLogged,
          hasBeenRead: false,
          logType: LogType.PORTFOLIO_LOG,
          mentorLinkId: this.getMentorLinkId(lastLog.mentorUsername)
        }
        this.unreadMsgCounter += 1;
        this.messages.push(messageItem);
      }

    }

    // ***** plan logs
    if (userOccupationPlans && userOccupationPlans.length > 0) {
      userOccupationPlans.forEach((userOccupationPlan: any) => {
        if (userOccupationPlan.logs && userOccupationPlan.logs.length > 0) {
          let sortedLogs = userOccupationPlan.logs.sort((a, b) => {
            let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
              db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
            return da - db;
          });

          let lastLog = sortedLogs[sortedLogs.length - 1];
          if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
            let messageItem = {
              id: lastLog.id,
              title: userOccupationPlan.title,
              from: this.getMentorById(lastLog.mentorLinkId),
              isLog: true,
              message: "Reviewed",
              timestamp: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
              originalTimestamp: lastLog.dateLogged,
              hasBeenRead: false,
              logType: LogType.PLAN_LOG,
              planId: lastLog.planId,
              mentorLinkId: lastLog.mentorLinkId
            }
            this.unreadMsgCounter += 1;
            this.messages.push(messageItem);
          }
        }
      })
    }

    // ***** activity logs
    if (classroomActivities && classroomActivities.length > 0) {
      classroomActivities.forEach((activity: any) => {

        if (activity.submissions && activity.submissions.length > 0) {
          activity.submissions.forEach((submission: any) => {
            //for logs
            if (submission.logs && submission.logs.length > 0) {
              let sortedLogs = submission.logs.sort((a, b) => {
                let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                  db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
                return da - db;
              });

              let lastLog = sortedLogs[sortedLogs.length - 1];
              if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
                let messageItem = {
                  id: lastLog.id,
                  title: activity.title,
                  from: this.getMentorByEmail(lastLog.user.username),
                  isLog: true,
                  message: "Reviewed: " + submission.fileName,
                  timestamp: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
                  originalTimestamp: lastLog.dateLogged,
                  hasBeenRead: false,
                  logType: LogType.ACTIVITY_LOG,
                  submissionId: submission.id,
                  classroomActivityId: submission.classroomActivityId,
                  mentorLinkId: this.getMentorLinkId(lastLog.user.username)
                }
                this.unreadMsgCounter += 1;
                this.messages.push(messageItem);
              }
            }
          })
        }

      })

    }

  }

  closeMe() {
    this._dialogRef.close();
  }

  getMentorByEmail(email) {

    let returnValue: string = "";
    if (this.mentors) {
      let mentor: any = this.mentors.find(x => x.mentorEmail == email);
      if (mentor) {
        returnValue = (mentor.firstName && mentor.lastName) ? mentor.firstName + " " + mentor.lastName : mentor.mentorEmail;
      }
    }

    return returnValue;
  }

  getMentorById(mentorId) {

    let returnValue: string = "";
    if (this.mentors) {
      let mentor: any = this.mentors.find(x => x.id == mentorId);
      if (mentor) {
        returnValue = (mentor.firstName && mentor.lastName) ? mentor.firstName + " " + mentor.lastName : mentor.mentorEmail;
      }
    }

    return returnValue;
  }

  getMentorLinkId(email) {
    let returnValue: string = "";
    if (this.mentors) {
      let mentor: any = this.mentors.find(x => x.mentorEmail == email);
      if (mentor) {
        returnValue = mentor.id;
      }
    }

    return returnValue;
  }


  getMessagesFilter(showReadItems: boolean) {

    let filterObj: any = {};

    if (!showReadItems) {
      filterObj.hasBeenRead = [false];
    } else {
      filterObj.hasBeenRead = [false, true];
    }

    return filterObj;
  }

  markAllAsRead() {

    let items = this.messages.filter(x => !x.hasBeenRead);

    if (items.length > 0) {
      items.forEach(x => {
        this.markAsRead(x, true);
      });

      this._userService.sendUserUpdate(true);

    }

  }

  markAsRead(item, markAllAsRead: Boolean = false) {

    if (item.isLog) {

      //activity log
      if (item.logType == LogType.ACTIVITY_LOG) {
        this._classroomActivityService.markSubmissionLogAsRead(item.classroomActivityId, item.submissionId, item.id, true)
          .then((rowsAffected: any) => {

            if (rowsAffected > 0) {
              var activityIndex = this.messages.findIndex(x => x.isLog && x.logType == LogType.ACTIVITY_LOG &&
                x.classroomActivityId == item.classroomActivityId && x.id == item.id);

              if (activityIndex > -1) {
                this.messages[activityIndex].hasBeenRead = true;
                this.unreadMsgCounter--;
              }

            }

            if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

          }).catch(error => console.error(error));
      }

      //plan log
      if (item.logType == LogType.PLAN_LOG) {
        this._mentorService.markStudentOccupationPlanLogAsRead(item.mentorLinkId, item.planId, item.id, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.messages.findIndex(x => x.isLog && x.logType == LogType.PLAN_LOG && x.id == item.id);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
              this.unreadMsgCounter--;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        }).catch(error => console.error(error));
      }

      //portfolio log
      if (item.logType == LogType.PORTFOLIO_LOG) {
        this._portfolioService.markPortfolioLogAsRead(item.id, true).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.messages.findIndex(x => x.isLog && x.logType == LogType.PORTFOLIO_LOG && x.id == item.id);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
              this.unreadMsgCounter--;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        }).catch(error => console.error(error));
      }

    }

    if (!item.isLog) {

      if (item.messageType == MessageType.ACTIVITY_COMMENT) {
        this._classroomActivityService.updateActivityCommentAsRead(item.classroomActivityId, item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

          if (rowsAffected > 0) {

            let index = this.messages.findIndex(x => !x.isLog && x.messageType == MessageType.ACTIVITY_COMMENT && x.classroomActivityId == item.classroomActivityId && x.id == item.id);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
              this.unreadMsgCounter--;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        });
      }

      if (item.messageType == MessageType.DIRECT_MESSAGE) {
        this._userService.updateMessage(item.id).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.messages.findIndex(x => x.id == item.id && x.messageType == MessageType.DIRECT_MESSAGE);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
              this.unreadMsgCounter--;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        });
      }

      if (item.messageType == MessageType.PLAN_COMMENT) {
        this._userOccupationPlanService.markCommentAsRead(item.planId, item.id, item.recipientId).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.messages.findIndex(x => !x.isLog && x.messageType == MessageType.PLAN_COMMENT && x.id == item.id);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
              this.unreadMsgCounter--;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        });
      }

      if (item.messageType == MessageType.PORTFOLIO_COMMENT) {
        this._portfolioService.updateCommentByHasBeenRead(item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

          if (rowsAffected > 0) {
            let index = this.messages.findIndex(x => !x.isLog && x.messageType == MessageType.PORTFOLIO_COMMENT && x.id == item.id);

            if (index > -1) {
              this.messages[index].hasBeenRead = !this.messages[index].hasBeenRead;
            }
          }

          if (!markAllAsRead) { this._userService.sendUserUpdate(true) };

        });
      }
    }

  }

  addComment(item) {

    if ((item.isLog && item.logType === LogType.PORTFOLIO_LOG) ||
      (!item.isLog && item.messageType === MessageType.PORTFOLIO_COMMENT)) {
      let formData: PortfolioComment = {
        from: null,
        id: null,
        message: item.addComment,
        dateCreated: null,
        to: null,
        commentTypeId: null,
        selectedUsers: [item.mentorLinkId]
      }

      this._portfolioService.sendComment(formData).then((response: PortfolioComment) => {
        item.addComment = undefined;
        item.showTextArea = false;
      }).catch(error => {
        console.error("ERROR", error);
      })
    }

    if ((item.isLog && item.logType === LogType.ACTIVITY_LOG) ||
      (!item.isLog && item.messageType === MessageType.ACTIVITY_COMMENT)) {

      let formData: ClassroomActivitySubmissionComment = {
        from: null,
        id: null,
        message: item.addComment,
        dateCreated: null,
        to: null,
        selectedUsers: [item.mentorLinkId],
        classroomActivityId: item.classroomActivityId
      }

      this._classroomActivityService.saveActivityComment(item.classroomActivityId, formData).then((response: ClassroomActivitySubmissionComment) => {
        item.addComment = undefined;
        item.showTextArea = false;
      }).catch(error => {
        console.error("ERROR", error);
      })

    }

    if ((item.isLog && item.logType === LogType.PLAN_LOG) ||
      (!item.isLog && item.messageType === MessageType.PLAN_COMMENT)) {
      let formData: UserOccupationPlanComment = {
        from: null,
        id: null,
        message: item.addComment,
        dateCreated: null,
        to: null,
        selectedUsers: [item.mentorLinkId],
        planId: item.planId,
        commentTypeId: LogType.PLAN_LOG
      }

      this._userOccupationPlanService.saveComment(item.planId, formData).then((response: UserOccupationPlanComment) => {
        item.addComment = undefined;
        item.showTextArea = false;
      }).catch(error => {
        console.error("ERROR", error);
      })
    }

    if (!item.isLog && item.messageType === MessageType.DIRECT_MESSAGE) {

      let formData: DirectMessage = {
        id: null, // handled on server
        messageTypeId: messageType.DIRECT_MESSAGE,
        message: item.addComment,
        dateCreated: null, // handled on server
        fromUser: null, // handled on server
        toUser: null, // handled on server
        mentorLinkId: item.mentorLinkId,
        hasBeenRead: false,
        calendarDate: null, // handled on server
        dateUpdated: null, // handled on server
      }

      this._userService.sendDirectMessage(formData).then((result: DirectMessage) => {
        item.addComment = undefined;
        item.showTextArea = false;
      }).catch((error: any) => {
        console.error("ERROR", error);
      })

    }

  }

  showNotificationSettings(){
    this._dialogRef.close();
		this._matDialog.open(EmailSettingsDialogComponent);
  }
}
