import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentNotificationDialogComponent } from './student-notification-dialog.component';

describe('StudentNotificationDialogComponent', () => {
  let component: StudentNotificationDialogComponent;
  let fixture: ComponentFixture<StudentNotificationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentNotificationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentNotificationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
