import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSettingsDialogComponent } from './email-settings-dialog.component';

describe('EmailSettingsDialogComponent', () => {
  let component: EmailSettingsDialogComponent;
  let fixture: ComponentFixture<EmailSettingsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailSettingsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSettingsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
