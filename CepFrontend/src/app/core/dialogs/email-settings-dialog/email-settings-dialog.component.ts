import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { UserEmailSettings } from 'app/core/models/emailSettings';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-email-settings-dialog',
  templateUrl: './email-settings-dialog.component.html',
  styleUrls: ['./email-settings-dialog.component.scss']
})
export class EmailSettingsDialogComponent implements OnInit {

  public isMentorSettingsDisabled: boolean;
  public isSaving: boolean;
  public isLoading: boolean;
  public wouldLikeToReceiveNotifications: any;
  public wouldLikeToReceiveMarketingComm: any;
  public sendDesignatedStudentSummaries: any;
  public sendDesignatedStudentSummariesOption: any;
  public wouldLikeToReceiveMonthlyNewsletter: any;
  public emailSettings: UserEmailSettings;
  public isDirty: boolean;
  public isStudent: boolean;

  successText;

  constructor(public _dialogRef: MatDialogRef<EmailSettingsDialogComponent>,
    private _userService: UserService) {

    this.isMentorSettingsDisabled = true;
    this.isSaving = false;
    this.isDirty = false;
    this.isLoading = true;
    this.isStudent = false;
  }

  ngOnInit() {
    this._dialogRef.updateSize("500px");

    this.getData().then(() => {

      if (this.emailSettings) {
        this.wouldLikeToReceiveNotifications = this.emailSettings.wouldLikeToReceiveNotifications;
        this.wouldLikeToReceiveMarketingComm = this.emailSettings.wouldLikeToReceiveMarketingComm;
        this.sendDesignatedStudentSummaries = this.emailSettings.sendDesignatedStudentSummaries;
        this.wouldLikeToReceiveMonthlyNewsletter = this.emailSettings.wouldLikeToReceiveMonthlyNewsletter;
        this.sendDesignatedStudentSummariesOption = null;
        if (this.emailSettings.sendDesignatedStudentSummariesMonthly) {
          this.sendDesignatedStudentSummariesOption = "monthly";
        }

        if (this.emailSettings.sendDesignatedStudentSummariesWeekly) {
          this.sendDesignatedStudentSummariesOption = "weekly";
        }

      }

      this.isLoading = false;
    });

  }

  async getData() {

    this.isStudent = (this._userService.getUser().currentUser.role == 'S') ? true : false;

    let emailSettings = this._userService.getEmailSettings();

    await Promise.all([emailSettings]).then((done: any) => {

      this.emailSettings = done[0];
    });

  }

  updateSettings() {
    this.isSaving = true;
    let sendDesignatedStudentSummariesWeekly: boolean = null;
    let sendDesignatedStudentSummariesMonthly: boolean = null;

    if (!this.isStudent) {
      if (this.sendDesignatedStudentSummaries) {
        if (this.sendDesignatedStudentSummariesOption && this.sendDesignatedStudentSummariesOption == "weekly") {
          sendDesignatedStudentSummariesWeekly = true;
        }
        if (this.sendDesignatedStudentSummariesOption && this.sendDesignatedStudentSummariesOption == "monthly") {
          sendDesignatedStudentSummariesMonthly = true;
        }
      }
    }

    let formData: UserEmailSettings = {
      sendDesignatedStudentSummaries: this.sendDesignatedStudentSummaries,
      sendDesignatedStudentSummariesWeekly: sendDesignatedStudentSummariesWeekly,
      sendDesignatedStudentSummariesMonthly: sendDesignatedStudentSummariesMonthly,
      wouldLikeToReceiveNotifications: this.wouldLikeToReceiveNotifications,
      wouldLikeToReceiveMarketingComm: this.wouldLikeToReceiveMarketingComm,
      wouldLikeToReceiveMonthlyNewsletter: this.wouldLikeToReceiveMonthlyNewsletter
    }

    this._userService.updateEmailSettings(formData).then((results: any) => {
      this.successText = "Your changes have been successfully applied.";
      setTimeout(() => { this.successText = ""; }, 5000);

      this.isSaving = false;
      this.isDirty = false;
    }).catch((error: any) => {
      this.isSaving = false;
      console.error("ERROR", error);
    })

  }

  closeMe() {
    this._dialogRef.close();
  }

}
