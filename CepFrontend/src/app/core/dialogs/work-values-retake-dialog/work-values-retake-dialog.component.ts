import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';


@Component({
  selector: 'app-work-values-retake-dialog',
  templateUrl: './work-values-retake-dialog.component.html',
  styleUrls: ['./work-values-retake-dialog.component.scss']
})
export class WorkValuesRetakeDialogComponent implements OnInit {

  constructor(public _dialogRef: MatDialogRef<WorkValuesRetakeDialogComponent>) { }

  ngOnInit() {
  }
  
  close() {
	  this._dialogRef.close();
  }
  
  retakeTest = function() {
	  this._dialogRef.close();
	  window.location.reload();
  };

}
