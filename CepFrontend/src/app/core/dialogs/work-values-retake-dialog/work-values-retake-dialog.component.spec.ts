import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkValuesRetakeDialogComponent } from './work-values-retake-dialog.component';

describe('WorkValuesRetakeDialogComponent', () => {
  let component: WorkValuesRetakeDialogComponent;
  let fixture: ComponentFixture<WorkValuesRetakeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkValuesRetakeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkValuesRetakeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
