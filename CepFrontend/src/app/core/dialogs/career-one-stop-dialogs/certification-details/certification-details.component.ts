import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component( {
    selector: 'app-certification-details',
    templateUrl: './certification-details.component.html',
    styleUrls: ['./certification-details.component.scss']
} )
export class CertificationDetailsComponent implements OnInit {

    public certId;
    public occupationTitle;
    public Name;
    public OrgName;
    public OrgAddress;
    public OrgWebPage;
    public Description;
    public CertDetailList;

    constructor( public _dialogRef: MatDialogRef<CertificationDetailsComponent>,
        @Inject( MAT_DIALOG_DATA ) public _data,
        public _http: HttpClient ) { }

    ngOnInit() {

        this.certId = this._data.certId;
        this.occupationTitle = this._data.occupationTitle;

        this.openCertificateDetail( this.certId );
    }

    openCertificateDetail = function( certificateDetail ) {

        // get detailed information ....
        this.Name = certificateDetail.Name;
        this.OrgName = certificateDetail.Organization;
        this.OrgAddress = certificateDetail.OrganizationAddress;
        this.OrgWebPage = certificateDetail.OrganizationUrl;
        if ( typeof certificateDetail.Description != 'undefined' && certificateDetail.Description ) {
            this.Description = certificateDetail.Description.replace( '&#xd;', '\n' );
        } else {
            this.Description = '';
        }

        this.CertDetailList = certificateDetail.CertDetailList;
    }

    IsJsonString( str ) {
        try {
            JSON.parse( str );
        } catch ( e ) {
            return false;
        }
        return true;
    }

    asyncCertificationDetail( certId ) {
        let promise = new Promise(( resolve, reject ) => {

            var careerOneStopUrl = '/CEP/rest/CareerOneStop/certDetail/' + certId;

            try {
                this._http.get( careerOneStopUrl ).toPromise().then(( response ) => {
                    resolve( response );
                }, ( error ) => {

                    console.error( error );
                    reject( 'Greeting ' + name + ' is not allowed.' );
                } );
            } catch ( err ) {
                alert( err.message );
            }
        } );
        return promise;
    }

}
