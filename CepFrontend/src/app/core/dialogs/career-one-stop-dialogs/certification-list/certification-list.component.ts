import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatPaginator } from '@angular/material';
import { PageEvent } from '@angular/material';
import { MatDialog } from '@angular/material';
import { CertificationDetailsComponent } from 'app/core/dialogs/career-one-stop-dialogs/certification-details/certification-details.component';


@Component({
    selector: 'app-certification-list',
    templateUrl: './certification-list.component.html',
    styleUrls: ['./certification-list.component.scss']
})
export class CertificationListComponent implements OnInit {

    public occupationCertificates;
    public occupationTitle;
    public isCertificationListVisible = true;
    public loadingCareerOneInfo = false;
    public isSelectionMode: boolean = false;
    private certificationList: string[];

    constructor(public dialogRef: MatDialogRef<CertificationListComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private _dialog: MatDialog) { }

    ngOnInit() {
        this.occupationCertificates = this.data.certs;
        this.occupationTitle = this.data.occupationTitle;

        if (this.data.selectionMode) {
            this.isSelectionMode = this.data.selectionMode;
            this.certificationList = [];

            // this.dialogRef.keydownEvents().subscribe(event => {
            //     if (event.key === "Escape") {
            //         this.onCancel();
            //     }
            // });

            // this.dialogRef.backdropClick().subscribe(event => {
            //     this.onCancel();
            // });

        }

    }

    openCertificateDetail(certificateDetail) {
        const data = {
            certId: certificateDetail,
            occupationTitle: this.occupationTitle
        };
        const dialogRef1 = this._dialog.open(CertificationDetailsComponent, {
            data: data
        });
    }

    /**
     * Angular Material Pagination
     */
    @ViewChild('paginator') vc: MatPaginator;
    careersLength: number;
    pageIndex = 0;
    pageSize = 10;
    lowValue = 0;
    highValue = 10;
    pageEvent: PageEvent;
    getPaginatorData(event) {
        if (event.pageIndex === this.pageIndex + 1) {
            this.lowValue = this.lowValue + this.pageSize;
            this.highValue = this.highValue + this.pageSize;
        } else {
            if (event.pageIndex === this.pageIndex - 1) {
                this.lowValue = this.lowValue - this.pageSize;
                this.highValue = this.highValue - this.pageSize;
            }
        }
        this.pageIndex = event.pageIndex;
    }

    // pagination next page
    nextPage() {
        this.vc.nextPage();
    }

    // pagination previous page
    previousPage() {
        this.vc.previousPage();
    }

    // pagination first page
    firstPage() {
        if (this.vc) {
            this.vc.firstPage();
        }
    }

    resetPagination() {
        this.firstPage();
    }

    selectionChange(filter, value) {

        if (value) {
            this.certificationList.push(filter);
        } else {
            this.certificationList.splice(this.certificationList.indexOf(filter), 1);
        }

    }

    //needed to keep the selection visible in the ui for user when paging is involved
    getSelectionChecked(value) {
        if (this.certificationList.indexOf(value) > -1) {
            return true;
        } else {
            return false;
        }
    }

    onCancel() {
        // if (this.data.selectionMode) {
        //     this.dialogRef.close(this.certificationList);
        // } else {
            this.dialogRef.close();
        // }
    }

    addCerts(){
        this.dialogRef.close(this.certificationList);
    }
}
