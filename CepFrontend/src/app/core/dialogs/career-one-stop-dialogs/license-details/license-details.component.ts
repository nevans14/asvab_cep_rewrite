import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component( {
    selector: 'app-license-details',
    templateUrl: './license-details.component.html',
    styleUrls: ['./license-details.component.scss']
} )
export class LicenseDetailsComponent implements OnInit {

    constructor( public _dialogRef: MatDialogRef<LicenseDetailsComponent>,
        @Inject( MAT_DIALOG_DATA ) public _data,
        public _http: HttpClient ) { }

    row;
    LICENSENAME;
    LICENSINGAGENCY;
    LICTITLE;
    DESCRIPTION;
    LICENSEAGENCY;
    ADDRESS1;
    CITY;
    STATE;
    ZIP;
    URL;
    EMAIL;
    TELEPHONE;
    ngOnInit() {
        this.row = this._data.row;

        this.LICENSENAME = this.row.Title;
        this.LICENSINGAGENCY = this.row.LicenseAgency.Name;
        this.LICTITLE = this.row.Title;
        this.DESCRIPTION = this.row.Description;
        this.LICENSEAGENCY = this.row.LicenseAgency.Name;
        this.ADDRESS1 = this.row.LicenseAgency.Address;
        this.CITY = this.row.LicenseAgency.City;
        this.STATE = this.row.LicenseAgency.State;
        this.ZIP = this.row.LicenseAgency.Zip;
        this.URL = this.row.LicenseAgency.Url
        this.EMAIL = this.row.LicenseAgency.Email;
        this.TELEPHONE = this.formatPhoneNumber( this.row.LicenseAgency.Phone );
    }

    formatPhoneNumber( number ) {

        if ( !number ) { return ''; }

        number = String( number );

        var formattedNumber = number;

        var c = ( number[0] == '1' ) ? '1 ' : '';
        number = number[0] == '1' ? number.slice( 1 ) : number;

        var area = number.substring( 0, 3 );
        var front = number.substring( 3, 6 );
        var end = number.substring( 6, 10 );

        if ( front ) {
            formattedNumber = ( c + "(" + area + ") " + front );
        }
        if ( end ) {
            formattedNumber += ( "-" + end );
        }
        return formattedNumber;
    }


}
