import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprenticeshipListComponent } from './apprenticeship-list.component';

describe('ApprenticeshipListComponent', () => {
  let component: ApprenticeshipListComponent;
  let fixture: ComponentFixture<ApprenticeshipListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprenticeshipListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprenticeshipListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
