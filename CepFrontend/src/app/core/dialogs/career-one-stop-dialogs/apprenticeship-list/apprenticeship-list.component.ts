import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatPaginator } from '@angular/material';
import { PageEvent } from '@angular/material';

@Component( {
    selector: 'app-apprenticeship-list',
    templateUrl: './apprenticeship-list.component.html',
    styleUrls: ['./apprenticeship-list.component.scss']
} )
export class ApprenticeshipListComponent implements OnInit {

    public restURL: String;
    public isApprenticeshipsVisible = true;
    public loadingCareerOneInfo = false;
    public onetOccupationCode;
    public occupationTitle;
    public apprenticeshipList: any = [];
    public apprenticeshipLabel;

    // options for form select
    public availableOptions: Array<Object> = [{
        abbreviationValue: 'AL',
        name: 'ALABAMA'
    }, {
        abbreviationValue: 'AK',
        name: 'ALASKA'
    }, {
        abbreviationValue: 'AZ',
        name: 'ARIZONA'
    }, {
        abbreviationValue: 'AR',
        name: 'ARKANSAS'
    }, {
        abbreviationValue: 'CA',
        name: 'CALIFORNIA'
    }, {
        abbreviationValue: 'CO',
        name: 'COLORADO'
    }, {
        abbreviationValue: 'CT',
        name: 'CONNECTICUT'
    }, {
        abbreviationValue: 'DE',
        name: 'DELAWARE'
    }, {
        abbreviationValue: 'FL',
        name: 'FLORIDA'
    }, {
        abbreviationValue: 'GA',
        name: 'GEORGIA'
    }, {
        abbreviationValue: 'HI',
        name: 'HAWAII'
    }, {
        abbreviationValue: 'ID',
        name: 'IDAHO'
    }, {
        abbreviationValue: 'IL',
        name: 'ILLINOIS'
    }, {
        abbreviationValue: 'IN',
        name: 'INDIANA'
    }, {
        abbreviationValue: 'IA',
        name: 'IOWA'
    }, {
        abbreviationValue: 'KS',
        name: 'KANSAS'
    }, {
        abbreviationValue: 'KY',
        name: 'KENTUCKY'
    }, {
        abbreviationValue: 'LA',
        name: 'LOUISIANA'
    }, {
        abbreviationValue: 'ME',
        name: 'MAINE'
    }, {
        abbreviationValue: 'MD',
        name: 'MARYLAND'
    }, {
        abbreviationValue: 'MA',
        name: 'MASSACHUSETTS'
    }, {
        abbreviationValue: 'MI',
        name: 'MICHIGAN'
    }, {
        abbreviationValue: 'MN',
        name: 'MINNESOTA'
    }, {
        abbreviationValue: 'MS',
        name: 'MISSISSIPPI'
    }, {
        abbreviationValue: 'MO',
        name: 'MISSOURI'
    }, {
        abbreviationValue: 'MT',
        name: 'MONTANA'
    }, {
        abbreviationValue: 'NE',
        name: 'NEBRASKA'
    }, {
        abbreviationValue: 'NV',
        name: 'NEVADA'
    }, {
        abbreviationValue: 'NH',
        name: 'NEW HAMPSHIRE'
    }, {
        abbreviationValue: 'NJ',
        name: 'NEW JERSEY'
    }, {
        abbreviationValue: 'NM',
        name: 'NEW MEXICO'
    }, {
        abbreviationValue: 'NY',
        name: 'NEW YORK'
    }, {
        abbreviationValue: 'NC',
        name: 'NORTH CAROLINA'
    }, {
        abbreviationValue: 'ND',
        name: 'NORTH DAKOTA'
    }, {
        abbreviationValue: 'OH',
        name: 'OHIO'
    }, {
        abbreviationValue: 'OK',
        name: 'OKLAHOMA'
    }, {
        abbreviationValue: 'OR',
        name: 'OREGON'
    }, {
        abbreviationValue: 'PA',
        name: 'PENNSYLVANIA'
    }, {
        abbreviationValue: 'RI',
        name: 'RHODE ISLAND'
    }, {
        abbreviationValue: 'SC',
        name: 'SOUTH CAROLINA'
    }, {
        abbreviationValue: 'SD',
        name: 'SOUTH DAKOTA'
    }, {
        abbreviationValue: 'TN',
        name: 'TENNESSEE'
    }, {
        abbreviationValue: 'TX',
        name: 'TEXAS'
    }, {
        abbreviationValue: 'UT',
        name: 'UTAH'
    }, {
        abbreviationValue: 'VT',
        name: 'VERMONT'
    }, {
        abbreviationValue: 'VA',
        name: 'VIRGINIA'
    }, {
        abbreviationValue: 'WA',
        name: 'WASHINGTON'
    }, {
        abbreviationValue: 'WV',
        name: 'WEST VIRGINIA'
    }, {
        abbreviationValue: 'WI',
        name: 'WISCONSIN'
    }, {
        abbreviationValue: 'WY',
        name: 'WYOMING'
    }, {
        abbreviationValue: 'GU',
        name: 'GUAM'
    }, {
        abbreviationValue: 'PR',
        name: 'PUERTO RICO'
    }, {
        abbreviationValue: 'VI',
        name: 'VIRGIN ISLANDS'
    }];

    // default selection for form select
    public selectedOption: any = this.availableOptions[0];

    constructor( public dialogRef: MatDialogRef<ApprenticeshipListComponent>,
        @Inject( MAT_DIALOG_DATA ) public data,
        public _http: HttpClient,
        public _config: ConfigService ) { this.restURL = _config.getRestUrl() + '/'; }

    ngOnInit() {
        this.onetOccupationCode = this.data.onetOccupationCode;
        this.occupationTitle = this.data.occupationTitle;

        this.findApprenticeship();
    }

    // Updates apprenticeship when state selection is changed.
    stateSelection() {

        this.resetPagination();

        // get licenses information ....
        let socId = this.onetOccupationCode;
        let location = this.selectedOption.abbreviationValue;
        this.loadingCareerOneInfo = true;
        Promise.all( [
            this.asynApprenticeship( socId, location )] ).then(( data: any ) => {
                this.loadingCareerOneInfo = false;

                if ( this.IsJsonString( data[0].payload ) ) {
                    this.apprenticeshipList = JSON.parse( data[0].payload );
                    // Apprenticeships ...
                    let list = this.apprenticeshipList.ApprenticeshipSponsorList;
                    this.apprenticeshipLabel = "";
                    if ( typeof list.length == 'undefined' ) {
                        this.apprenticeshipList = new Array();
                        this.apprenticeshipList.push( list );
                    } else {
                        this.apprenticeshipList = list;
                    }
                } else {
                    this.apprenticeshipList = [];
                    this.apprenticeshipLabel = "No apprenticeships for this state and occupation.";
                }
            }, ( reason ) => { // error
                this.loadingCareerOneInfo = false;
            } );
    }

    findApprenticeship() {
        this.loadingCareerOneInfo = true;
        // get licenses information ....
        let socId = this.onetOccupationCode;
        let location = this.selectedOption.abbreviationValue;
        let promise = this.asynApprenticeship( socId, location );
        promise.then(( greeting: any ) => {
            this.loadingCareerOneInfo = false;
            if ( this.IsJsonString( greeting.payload ) ) {
                this.apprenticeshipList = JSON.parse( greeting.payload );
                let list = this.apprenticeshipList;

                this.apprenticeshipLabel = "";

                if ( list.RecordCount < 2 ) {
                    this.apprenticeshipList = new Array();
                    this.apprenticeshipList.push( list );
                } else {
                    this.apprenticeshipList = list;
                }

            } else {
                this.apprenticeshipList = greeting.payload;
                this.apprenticeshipList = [];
                this.apprenticeshipLabel = "No apprenticeships for this state and occupation.";
                return;
            }
        }, ( reason ) => {
            this.loadingCareerOneInfo = true;
            alert( 'Failed: ' + reason );
        } );
    }

    IsJsonString( str ) {
        try {
            JSON.parse( str );
        } catch ( e ) {
            return false;
        }
        return true;
    }

    asynApprenticeship( onetSoc, location ) {
        let promise = new Promise(( resolve, reject ) => {

            let onetOccupationCode = onetSoc.replace( /-/g, '' ).replace( /\./g, '' ).trim();
            let careerOneStopUrl = this.restURL + 'CareerOneStop/apprenticeship/' + onetOccupationCode + '/' + location;

            try {
                this._http.get( careerOneStopUrl ).toPromise().then(( response ) => {
                    resolve( response );
                }, ( error ) => {

                    console.error( error );
                    reject( 'Greeting ' + name + ' is not allowed.' );
                } );
            } catch ( err ) {
                alert( err.message );
            }
        } );

        return promise;
    }

    /**
     * Angular Material Pagination
     */
    @ViewChild( 'paginator' ) vc: MatPaginator;
    careersLength: number;
    pageIndex = 0;
    pageSize = 10;
    lowValue = 0;
    highValue = 10;
    pageEvent: PageEvent;
    getPaginatorData( event ) {
        if ( event.pageIndex === this.pageIndex + 1 ) {
            this.lowValue = this.lowValue + this.pageSize;
            this.highValue = this.highValue + this.pageSize;
        } else {
            if ( event.pageIndex === this.pageIndex - 1 ) {
                this.lowValue = this.lowValue - this.pageSize;
                this.highValue = this.highValue - this.pageSize;
            }
        }
        this.pageIndex = event.pageIndex;
    }

    // pagination next page
    nextPage() {
        this.vc.nextPage();
    }

    // pagination previous page
    previousPage() {
        this.vc.previousPage();
    }

    // pagination first page
    firstPage() {
        if ( this.vc ) {
            this.vc.firstPage();
        }
    }

    resetPagination() {
        this.firstPage();
    }


}
