import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatPaginator } from '@angular/material';
import { MatDialog } from '@angular/material';
import { PageEvent } from '@angular/material';
import { LicenseDetailsComponent } from 'app/core/dialogs/career-one-stop-dialogs/license-details/license-details.component';
import { OccufindCareerOneFactoryService } from 'app/occufind/occupation-details/services/occufind-career-one-factory.service';


@Component({
    selector: 'app-license-list',
    templateUrl: './license-list.component.html',
    styleUrls: ['./license-list.component.scss']
})
export class LicenseListComponent implements OnInit {

    public restURL: String;
    public onetOccupationCode;
    public occupationTitle;
    public loadingCareerOneInfo = false;
    public licensesList: any = [];
    public licensesLabel;

    //options for form select
    public availableOptions: Array<Object> = [{
        abbreviationValue: 'AL',
        name: 'ALABAMA'
    }, {
        abbreviationValue: 'AK',
        name: 'ALASKA'
    }, {
        abbreviationValue: 'AZ',
        name: 'ARIZONA'
    }, {
        abbreviationValue: 'AR',
        name: 'ARKANSAS'
    }, {
        abbreviationValue: 'CA',
        name: 'CALIFORNIA'
    }, {
        abbreviationValue: 'CO',
        name: 'COLORADO'
    }, {
        abbreviationValue: 'CT',
        name: 'CONNECTICUT'
    }, {
        abbreviationValue: 'DE',
        name: 'DELAWARE'
    }, {
        abbreviationValue: 'FL',
        name: 'FLORIDA'
    }, {
        abbreviationValue: 'GA',
        name: 'GEORGIA'
    }, {
        abbreviationValue: 'HI',
        name: 'HAWAII'
    }, {
        abbreviationValue: 'ID',
        name: 'IDAHO'
    }, {
        abbreviationValue: 'IL',
        name: 'ILLINOIS'
    }, {
        abbreviationValue: 'IN',
        name: 'INDIANA'
    }, {
        abbreviationValue: 'IA',
        name: 'IOWA'
    }, {
        abbreviationValue: 'KS',
        name: 'KANSAS'
    }, {
        abbreviationValue: 'KY',
        name: 'KENTUCKY'
    }, {
        abbreviationValue: 'LA',
        name: 'LOUISIANA'
    }, {
        abbreviationValue: 'ME',
        name: 'MAINE'
    }, {
        abbreviationValue: 'MD',
        name: 'MARYLAND'
    }, {
        abbreviationValue: 'MA',
        name: 'MASSACHUSETTS'
    }, {
        abbreviationValue: 'MI',
        name: 'MICHIGAN'
    }, {
        abbreviationValue: 'MN',
        name: 'MINNESOTA'
    }, {
        abbreviationValue: 'MS',
        name: 'MISSISSIPPI'
    }, {
        abbreviationValue: 'MO',
        name: 'MISSOURI'
    }, {
        abbreviationValue: 'MT',
        name: 'MONTANA'
    }, {
        abbreviationValue: 'NE',
        name: 'NEBRASKA'
    }, {
        abbreviationValue: 'NV',
        name: 'NEVADA'
    }, {
        abbreviationValue: 'NH',
        name: 'NEW HAMPSHIRE'
    }, {
        abbreviationValue: 'NJ',
        name: 'NEW JERSEY'
    }, {
        abbreviationValue: 'NM',
        name: 'NEW MEXICO'
    }, {
        abbreviationValue: 'NY',
        name: 'NEW YORK'
    }, {
        abbreviationValue: 'NC',
        name: 'NORTH CAROLINA'
    }, {
        abbreviationValue: 'ND',
        name: 'NORTH DAKOTA'
    }, {
        abbreviationValue: 'OH',
        name: 'OHIO'
    }, {
        abbreviationValue: 'OK',
        name: 'OKLAHOMA'
    }, {
        abbreviationValue: 'OR',
        name: 'OREGON'
    }, {
        abbreviationValue: 'PA',
        name: 'PENNSYLVANIA'
    }, {
        abbreviationValue: 'RI',
        name: 'RHODE ISLAND'
    }, {
        abbreviationValue: 'SC',
        name: 'SOUTH CAROLINA'
    }, {
        abbreviationValue: 'SD',
        name: 'SOUTH DAKOTA'
    }, {
        abbreviationValue: 'TN',
        name: 'TENNESSEE'
    }, {
        abbreviationValue: 'TX',
        name: 'TEXAS'
    }, {
        abbreviationValue: 'UT',
        name: 'UTAH'
    }, {
        abbreviationValue: 'VT',
        name: 'VERMONT'
    }, {
        abbreviationValue: 'VA',
        name: 'VIRGINIA'
    }, {
        abbreviationValue: 'WA',
        name: 'WASHINGTON'
    }, {
        abbreviationValue: 'WV',
        name: 'WEST VIRGINIA'
    }, {
        abbreviationValue: 'WI',
        name: 'WISCONSIN'
    }, {
        abbreviationValue: 'WY',
        name: 'WYOMING'
    }, {
        abbreviationValue: 'GU',
        name: 'GUAM'
    }, {
        abbreviationValue: 'PR',
        name: 'PUERTO RICO'
    }, {
        abbreviationValue: 'VI',
        name: 'VIRGIN ISLANDS'
    }];

    // default selection for form select
    public selectedOption: any = this.availableOptions[0];

    public isSelectionMode: boolean = false;
    private licenseList: string[];

    constructor(public dialogRef: MatDialogRef<LicenseListComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private _dialog: MatDialog,
        public _http: HttpClient,
        public _config: ConfigService,
        private CareerOneStopFactory: OccufindCareerOneFactoryService,) { this.restURL = _config.getRestUrl() + '/'; }

    ngOnInit() {
        this.onetOccupationCode = this.data.onetOccupationCode;
        this.occupationTitle = this.data.occupationTitle;

        if (this.data.selectionMode) {
            this.isSelectionMode = this.data.selectionMode;
            this.licenseList = [];

            // this.dialogRef.keydownEvents().subscribe(event => {
            //     if (event.key === "Escape") {
            //         this.onCancel();
            //     }
            // });

            // this.dialogRef.backdropClick().subscribe(event => {
            //     this.onCancel();
            // });

        }

        this.stateSelection();
    }


    openLicenseDetail(record) {
        const data = {
            row: record
        };
        const dialogRef1 = this._dialog.open(LicenseDetailsComponent, {
            data: data,
        });
    }

    stateSelection() {

        this.resetPagination();

        // get licenses information ....
        let socId = this.onetOccupationCode;
        let location = this.selectedOption.abbreviationValue;
        this.loadingCareerOneInfo = true;

        let promise = this.asyncLicense(socId, location);
        promise.then((data: any) => {
            this.loadingCareerOneInfo = false;
            if (data) {
                if (this.IsJsonString(data.payload)) {
                    this.licensesList = JSON.parse(data.payload);
                    let list = this.licensesList;
                    this.licensesLabel = "";
                    if (typeof list.length == 'undefined' && typeof list.LICENSEID != 'undefined') {
                        this.licensesList = new Array();
                        this.licensesList.push(list);
                    } else {
                        this.licensesList = list.LicenseList;
                    }
                    this.licensesLabel = "";
                } else {
                    this.licensesList = [];
                    this.licensesLabel = "No Licenses for this state and occupation.";
                }
            }

        }, (reason) => { // error
            this.loadingCareerOneInfo = false;
        })
    }


    asyncLicense(onetSoc, location) {
        let promise = new Promise((resolve, reject) => {

            try {
                this.CareerOneStopFactory.getOccupationLicenses(onetSoc, location).toPromise().then((response) => {
                    resolve(response);
                }, (error) => {
                    console.error('Licenses fetch error' + error);
                    reject(error);
                });
            } catch (err) {
                alert(err.message);
            }
        });
        return promise;
    }

    asyncLicenseDetail(onetSoc, licenseId, location) {
        let promise = new Promise((resolve, reject) => {

            try {
                this.CareerOneStopFactory.getOccupationLicensesDetail(onetSoc, licenseId, location).toPromise().then((response) => {
                    resolve(response);
                }, (error) => {
                    console.error('Licenses fetch error' + error);
                    reject(error);
                });
            } catch (err) {
                alert(err.message);
            }
        });
        return promise;
    }


    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    /**
     * Angular Material Pagination
     */
    @ViewChild('paginator') vc: MatPaginator;
    careersLength: number;
    pageIndex = 0;
    pageSize = 10;
    lowValue = 0;
    highValue = 10;
    pageEvent: PageEvent;
    getPaginatorData(event) {
        if (event.pageIndex === this.pageIndex + 1) {
            this.lowValue = this.lowValue + this.pageSize;
            this.highValue = this.highValue + this.pageSize;
        } else {
            if (event.pageIndex === this.pageIndex - 1) {
                this.lowValue = this.lowValue - this.pageSize;
                this.highValue = this.highValue - this.pageSize;
            }
        }
        this.pageIndex = event.pageIndex;
    }

    // pagination next page
    nextPage() {
        this.vc.nextPage();
    }

    // pagination previous page
    previousPage() {
        this.vc.previousPage();
    }

    // pagination first page
    firstPage() {
        if (this.vc) {
            this.vc.firstPage();
        }
    }

    resetPagination() {
        this.firstPage();
    }

    selectionChange(filter, value) {

        if (value) {
            this.licenseList.push(filter);
        } else {
            this.licenseList.splice(this.licenseList.indexOf(filter), 1);
        }

    }

    //needed to keep the selection visible in the ui for user when paging is involved
    getSelectionChecked(value) {
        if (this.licenseList.indexOf(value) > -1) {
            return true;
        } else {
            return false;
        }
    }
    
    onCancel() {
        // if (this.data.selectionMode) {
        //     this.dialogRef.close(this.licenseList);
        // } else {
            this.dialogRef.close();
        // }
    }

    addCerts(){
        this.dialogRef.close(this.licenseList);
    }

}
