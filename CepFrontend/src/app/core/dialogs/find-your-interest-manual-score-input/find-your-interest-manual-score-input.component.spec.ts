import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindYourInterestManualScoreInputComponent } from './find-your-interest-manual-score-input.component';

describe('FindYourInterestManualScoreInputComponent', () => {
  let component: FindYourInterestManualScoreInputComponent;
  let fixture: ComponentFixture<FindYourInterestManualScoreInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindYourInterestManualScoreInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindYourInterestManualScoreInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
