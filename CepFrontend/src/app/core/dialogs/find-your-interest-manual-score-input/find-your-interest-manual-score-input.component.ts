import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { FindYourInterestService } from '../../../services/find-your-interest.service';
import { FyiTestService } from '../../../services/fyi-test.service';

@Component({
  selector: 'app-find-your-interest-manual-score-input',
  templateUrl: './find-your-interest-manual-score-input.component.html',
  styleUrls: ['./find-your-interest-manual-score-input.component.scss']
})
export class FindYourInterestManualScoreInputComponent implements OnInit {

  constructor(
    private FYIScoreService: FindYourInterestService,
    private _router: Router,
    public dialogRef: MatDialogRef<FindYourInterestManualScoreInputComponent>,
    private _fyiTestService: FyiTestService,
  ) { }

  ngOnInit() {
  }
  
  
  interestCodeRawScore = {
      rRawScore : undefined,
      iRawScore : undefined,
      aRawScore : undefined,
      sRawScore : undefined,
      eRawScore : undefined,
      cRawScore : undefined
  };
  gender = '';
  rawScoreValidate;
  genderValidate;

  submit() {

      // validate inputs
      if (this.interestCodeRawScore.rRawScore == undefined || this.interestCodeRawScore.iRawScore == undefined || this.interestCodeRawScore.aRawScore == undefined || this.interestCodeRawScore.sRawScore == undefined || this.interestCodeRawScore.eRawScore == undefined || this.interestCodeRawScore.cRawScore == undefined) {
          this.rawScoreValidate = "Enter a score for all interest codes with a range from 0 to 30";
      } else {
          this.rawScoreValidate = undefined;
      }

      if (this.gender === '') {
          this.genderValidate = "Select gender";
      } else {
          this.genderValidate = undefined;
      }

      if (this.genderValidate !== undefined || this.rawScoreValidate !== undefined) {
          return false;
      }

      var rawScore = this.interestCodeRawScore;

      // create profile
      var fyiObject = {
          testId : undefined,
          gender : this.gender,
          rawScores : undefined
      };

      fyiObject.rawScores = [ {
          interestCd : 'R',
          rawScore : rawScore.rRawScore
      }, {
          interestCd : 'I',
          rawScore : rawScore.iRawScore
      }, {
          interestCd : 'A',
          rawScore : rawScore.aRawScore
      }, {
          interestCd : 'S',
          rawScore : rawScore.sRawScore
      }, {
          interestCd : 'E',
          rawScore : rawScore.eRawScore
      }, {
          interestCd : 'C',
          rawScore : rawScore.cRawScore
      } ];


      const that = this;
      this._fyiTestService.saveTest().then(function(success) {
        fyiObject.testId = success;
        that.FYIScoreService.setFyiObject(fyiObject);
        
        if(that._router.url == '/find-your-interest-score') {
            //that._router.onSameUrlNavigation = 'reload';
            //that._router.navigate( ['/find-your-interest-reload'] );
            that._router.navigateByUrl('/find-your-interest-reload', {skipLocationChange: true}).then(()=>
            that._router.navigate(["/find-your-interest-score"]));
        } else {
            that._router.navigate( ['/find-your-interest-score'] );
        }
        
        that.dialogRef.close();
      })
  };

  close () {
      //$uibModalInstance.dismiss('cancel');
      this.dialogRef.close();
  };

}
