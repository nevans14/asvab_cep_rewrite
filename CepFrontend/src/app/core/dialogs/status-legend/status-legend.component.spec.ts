import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusLegendComponent } from './status-legend.component';

describe('StatusLegendComponent', () => {
  let component: StatusLegendComponent;
  let fixture: ComponentFixture<StatusLegendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusLegendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
