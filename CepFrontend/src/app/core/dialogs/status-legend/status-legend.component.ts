import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-status-legend',
  templateUrl: './status-legend.component.html',
  styleUrls: ['./status-legend.component.scss']
})
export class StatusLegendComponent implements OnInit {

  constructor(
    public _dialogRef: MatDialogRef<StatusLegendComponent>,
  ) { }

  ngOnInit() {
  }

  closeMe() {
    this._dialogRef.close();
  }

}
