import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClassroomActivitySubmissionComment } from 'app/core/models/classroomActivity';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { UserOccupationPlanComment } from 'app/core/models/userOccupationPlan.model';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MentorService } from 'app/services/mentor.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';

@Component({
  selector: 'app-message-to-student-dialog',
  templateUrl: './message-to-student-dialog.component.html',
  styleUrls: ['./message-to-student-dialog.component.scss']
})
export class MessageToStudentDialogComponent implements OnInit {

  public isLoading: boolean;
  public studentName: string;
  public title: string;
  public commentForm: FormGroup;
  public mentorLinkId: string;
  public id: any;

  successText;
  
  constructor(public _dialogRef: MatDialogRef<MessageToStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _formBuilder: FormBuilder,
    private _portfolioService: PortfolioService,
    private _classroomActivityService: ClassroomActivityService,
    private _userOccupationPlanService: UserOccupationPlanService) { 
    this.isLoading = true;
    this.commentForm = this._formBuilder.group({
      comment: ['', Validators.required],
    });
  }

  ngOnInit() {

    this.isLoading = false;
    this.studentName = this.data.studentName;
    this.title = this.data.title;
    this.mentorLinkId = this.data.mentorLinkId; 
    this.id = this.data.id;
    this.successText = '';
  }

  submitComment() {
    
    let selectedUsers: string[] = [];
    selectedUsers.push(this.mentorLinkId);
    
    if(this.data.title === 'Portfolio'){
      let formData: PortfolioComment = {
        from: null,
        id: null,
        message: this.commentForm.value.comment,
        dateCreated: null,
        to: null,
        commentTypeId: null,
        selectedUsers: selectedUsers
      }

      this._portfolioService.sendComment(formData).then((response:PortfolioComment) => {
        this.successText = "Comment sent successfully!";
        setTimeout(() => { this.successText = ""; }, 5000);
        this.commentForm.get("comment").reset();
      }).catch(error =>{
        console.error("ERROR", error);
      })
    }

    if(this.data.title === 'Activity'){
      let formData: ClassroomActivitySubmissionComment = {
        from: null,
        id: null, 
        message: this.commentForm.value.comment,
        dateCreated: null,
        to: null,
        selectedUsers: selectedUsers,
        classroomActivityId: this.id
      }

      this._classroomActivityService.saveActivityComment(this.id, formData).then((response:ClassroomActivitySubmissionComment) => {
        this.successText = "Comment sent successfully!";
        setTimeout(() => { this.successText = ""; }, 5000);
        this.commentForm.get("comment").reset();
      }).catch(error =>{
        console.error("ERROR", error);
      })
    }

    if(this.data.title === 'Plan'){
      let formData: UserOccupationPlanComment = {
        from: null,
        id: null, 
        message: this.commentForm.value.comment,
        dateCreated: null,
        to: null,
        selectedUsers: selectedUsers,
        planId: this.id,
        commentTypeId: 2
      }

      this._userOccupationPlanService.saveComment(this.id, formData).then((response:UserOccupationPlanComment) => {
        this.successText = "Comment sent successfully!";
        setTimeout(() => { this.successText = ""; }, 5000);
        this.commentForm.get("comment").reset();
      }).catch(error =>{
        console.error("ERROR", error);
      })
    }
  }
   
  close(){
    this._dialogRef.close();
  }
}
