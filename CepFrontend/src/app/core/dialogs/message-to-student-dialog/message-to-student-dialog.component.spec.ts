import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageToStudentDialogComponent } from './message-to-student-dialog.component';

describe('MessageToStudentDialogComponent', () => {
  let component: MessageToStudentDialogComponent;
  let fixture: ComponentFixture<MessageToStudentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageToStudentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageToStudentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
