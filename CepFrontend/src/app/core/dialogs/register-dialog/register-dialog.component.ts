import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmPwdValidator } from 'app/core/confirm-pwd-validator';
import { AuthenticationService } from 'app/services/authentication.service';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { ReCaptcha2Component } from 'ngx-captcha';
import { BringAsvabToYourSchoolComponent } from '../bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { MergeAccountDialogComponent } from '../merge-account-dialog/merge-account-dialog.component';
import { LoginService } from 'app/services/login.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit {

  isUsingPromoCode: boolean;
  isStudentCode: boolean;
  registerAccessCode: any;
  functions: any;
  conferences: any;
  registerForm: FormGroup;
  registrationLabel: any;
  // endpoint enum
  serviceEndpoint = {
    'Register': 'register',
    'TeacherRegister': 'teacherRegistration'
  };

  // for mentor registration
  isMentorRegistration: boolean;
  registerMentorForm: FormGroup;
  searchSchoolList: any = [];
  selectedSchoolList: any = [];
  guid;
  mentorEmail;
  searchSchoolError;
  
  
  isSearchSchoolButtonDisabled;
  isSubmitButtonDisabled;
  
  BASE_REGISTER_URL = this._config.getRestUrl() + '/applicationAccess/';

  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;

  constructor(public _dialogRef: MatDialogRef<RegisterDialogComponent>,
    private _config: ConfigService,
    private _dialog: MatDialog,
    private _httpHelper: HttpHelperService,
    private _formBuilder: FormBuilder,
    public _authenticationService: AuthenticationService,
    private _loginService: LoginService,
    @Inject(MAT_DIALOG_DATA) public data) {

    //set troops form controls default values and assign to form
    this.registerForm = this._formBuilder.group({
      registerAccessCode: new FormControl('', Validators.required),
      registerEmail: new FormControl('', Validators.required),
      registerPassword1: new FormControl('', [Validators.required, Validators.minLength(8)]),
      registerPassword2: new FormControl('', Validators.required),
      registerStudentId: new FormControl(''),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      phoneNumber: new FormControl(''),
      functionId: new FormControl({ value: '', disabled: false }),
      functionOther: new FormControl(''),
      //schoolName: new FormControl(''),
      //schoolZipCode: new FormControl(''),
      //schoolState: new FormControl(''),
      //essName: new FormControl(''),
      mentorShareEmails: new FormControl(''), //, Validators.pattern(/^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?)+$/)
      ///conferenceId: new FormControl({ value: '', disabled: false }),
      //eventName: new FormControl(''),
      schoolzipcode: new FormControl(''),
      schoolCity: new FormControl(''),
      schoolState: new FormControl(''),
      schoolcode: new FormControl(''),
      optedInCommunications: new FormControl(true),
      heardAboutUs: new FormControl(''),
      heardAboutUsOther: new FormControl(''),
      readPrivacyPolicy: new FormControl(false, Validators.requiredTrue),
      recaptchaResponse: new FormControl('', Validators.required)
    }, {
      validator: ConfirmPwdValidator('registerPassword1', 'registerPassword2')
    });
    
    //set troops form controls default values and assign to form
    this.registerMentorForm = this._formBuilder.group({
      registerEmail: new FormControl('', Validators.required),
      registerPassword1: new FormControl('', [Validators.required, Validators.minLength(8)]),
      registerPassword2: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      phoneNumber: new FormControl(''),
      functionId: new FormControl({ value: '', disabled: false }, Validators.required),
      functionOther: new FormControl(''),
      schoolzipcode: new FormControl(''),
      schoolCity: new FormControl(''),
      schoolState: new FormControl(''),
      schoolcode: new FormControl(''),
      optedInCommunications: new FormControl(true),
      heardAboutUs: new FormControl(''),
      heardAboutUsOther: new FormControl(''),
      readPrivacyPolicy: new FormControl(false, Validators.requiredTrue),
      recaptchaResponse: new FormControl('', Validators.required)
    }, {
      validator: ConfirmPwdValidator('registerPassword1', 'registerPassword2')
    });

  }

  ngOnInit() {

	// For mentor registration with shared link only
	if (this.data) {
		this.isMentorRegistration = this.data.isMentorRegistration ? this.data.isMentorRegistration : false;
		this.guid =  this.data.id ? this.data.id : '';
		this.mentorEmail = this.data.mentorEmail;
		if(this.mentorEmail){
			this.registerMentorForm.controls['registerEmail'].setValue(this.mentorEmail);
			this.registerMentorForm.controls['registerEmail'].disable();
		}
	}
	
    if (this.data) {
      this.registerAccessCode = this.data.accessCode;
    }

    this.registerForm.controls.registerAccessCode.setValue(this.registerAccessCode);

    this.isStudentCode = false;
    this.isUsingPromoCode = (this.registerAccessCode) ? true : false;

    //retrieve list of functions
    let url = this.BASE_REGISTER_URL + 'get-function-list';
    this._httpHelper.httpHelper('GET', url, null, null, true).then(data => {
      this.functions = data;
    });

    //retrieve list of conferences
    url = this.BASE_REGISTER_URL + 'get-conference-list';
    this._httpHelper.httpHelper('GET', url, null, null, true).then(data => {
      this.conferences = data;
    });

  }
  
  
  searchSchool (form) {
	  this.isSearchSchoolButtonDisabled = true;
	  this.searchSchoolError = undefined;
	  
	  let searchSchoolObject = {
			  schoolCode: form.value.schoolcode,
			  city: form.value.schoolCity,
			  state: form.value.schoolState,
			  zipCode: form.value.schoolzipcode
	  }
	  
	  if ((searchSchoolObject.city && searchSchoolObject.state) || searchSchoolObject.schoolCode || searchSchoolObject.zipCode) {
		  
		  this._loginService.searchSchool(searchSchoolObject).then(results => {
			  this.searchSchoolList = results;
			  
			  if (this.searchSchoolList.length == 0) {
				  this.searchSchoolError = 'Nothing found. Please try again.'
			  }
			  
			  this.isSearchSchoolButtonDisabled = false;
		  }, error => {
			  console.error(error);
			  this.searchSchoolError = 'There was an error searching for school. Please try again.' 
			  this.isSearchSchoolButtonDisabled = false;
		  });
		  
	  } else {
		 this.searchSchoolError = 'You must search by city/state or school code or zip code'; 
		 this.isSearchSchoolButtonDisabled = false;
	  }
	  
  }
  
  /**
   * This function sets user's selected schools.
   */
  setSchooltoSelectedList (schoolObject) {
	  
	  let i = this.selectedSchoolList.findIndex(o => o.schoolCode == schoolObject.schoolCode);
	  
	  if (i > -1) {
		  schoolObject.toggleSchoolSelectedView = !schoolObject.toggleSchoolSelectedView;
		  this.selectedSchoolList[i].toggleSchoolSelectedView = !this.selectedSchoolList[i].toggleSchoolSelectedView;
	  } else {
		  this.selectedSchoolList.push(schoolObject);
	  }
  }
  
  /**
   * Function to submit mentor registration. This is specific to the mentors who received the shared link from student with GUID.
   */
  submitRegisterMentorForm = function () {
	  
	    this.isSubmitButtonDisabled = true;
	  	let selectedSchoolCodes = [];
	  	
	  	// set school codes to submit
	  	for (let i = 0; i < this.selectedSchoolList.length; i++){
	  		if(this.selectedSchoolList[i].toggleSchoolSelectedView) {
	  			selectedSchoolCodes.push(this.selectedSchoolList[i].schoolCode);
	  		}
	  	}

	    //map form data to send
	    let formData = {
	      email: this.registerMentorForm.getRawValue().registerEmail,
	      accessCode: this.guid,
	      password: this.registerMentorForm.value.registerPassword2,
	      resetKey: "",
	      optedInCommunications: this.registerMentorForm.value.optedInCommunications,
	      recaptchaResponse: this.registerMentorForm.value.recaptchaResponse,
	      firstName: this.registerMentorForm.value.firstName,
	      lastName: this.registerMentorForm.value.lastName,
	      phoneNumber: this.registerMentorForm.value.phoneNumber,
	      functionOther: this.registerMentorForm.value.functionOther,
	      essName: this.registerMentorForm.value.essName,
	      eventName: this.registerMentorForm.value.eventName,
	      conferenceId: parseInt(this.registerMentorForm.value.conferenceId, 10),
	      functionId: parseInt(this.registerMentorForm.value.functionId, 10),
	      selectedSchools: selectedSchoolCodes,
	      heardAboutUs: this.registerMentorForm.value.heardAboutUs,
	      heardAboutUsOther: this.registerMentorForm.value.heardAboutUsOther,
	      agreedToPolicy: this.registerMentorForm.value.readPrivacyPolicy
	    }

	    // mentor registration uses same teacher url
	    let url = this.serviceEndpoint.TeacherRegister;

	    let self = this;

	    this._httpHelper.httpHelper('POST', '/applicationAccess/' + url, null, formData).then((response) => {

	      self.registrationLoginStatus = response;
	      let userData = response;

	      if (self.registrationLoginStatus.statusNumber === 0) {	// success

	        // Now log in the user ....
	        self._authenticationService.SetCredentials(
	          userData.email,
	          'null',
	          userData.user_id,
	          userData.role,
	          userData.accessCode,
	          userData.schoolCode,
	          userData.schoolName,
	          userData.city,
	          userData.state,
	          userData.zipCode,
	          userData.mepsId,
	          userData.mepsName,
	          false);

	        self.isSubmitButtonDisabled = false;
	        
	        // Close Dialog
	        self._dialogRef.close();

          self._loginService.redirectAfterLogin()

	        // self._router.navigate(['/dashboard']);


	      } else { // Error
	    	  self.isSubmitButtonDisabled = false;
	          //show error message
	          self.registrationLabel = self.registrationLoginStatus.status;
	          self.captchaElem.resetCaptcha();
	          self.registerForm.value.recaptchaResponse = '';
	          setTimeout(() => { self.registrationLabel = ''; }, 5000);
	      }

	    }, error => {
	    	self.isSubmitButtonDisabled = false;
	    	console.error(error);
	    })


	  }


  checkForPromoCode = function () {
    this.isUsingPromoCode = false;
    this.isStudentCode = false;
    let accessCode = this.registerForm.value.registerAccessCode;

    if (accessCode == null) {
      return;
    }

    var url = this.BASE_REGISTER_URL + 'testTeacherAccessCode';

    this._httpHelper.httpHelper('POST', url, null, { accessCode: accessCode.toUpperCase() }, true).then((response) => {
      if (response.statusNumber === 0) {
        this.isUsingPromoCode = true;
      } else if (accessCode.length === 10 && isNaN(accessCode[accessCode.length - 1])) {
        this.isStudentCode = true;
      }
    })

  }


  /**
   * Function to submit registrations.
   */
  submitRegisterForm = function () {

	this.isSubmitButtonDisabled = true;  
	let selectedSchoolCodes = [];
	  	
  	for (let i = 0; i < this.selectedSchoolList.length; i++){
  		if(this.selectedSchoolList[i].toggleSchoolSelectedView) {
  			selectedSchoolCodes.push(this.selectedSchoolList[i].schoolCode);
  		}
  	}
  
	  	
    //map form data to send
    let formData = {
      email: this.registerForm.value.registerEmail,
      accessCode: this.registerForm.value.registerAccessCode,
      password: this.registerForm.value.registerPassword2,
      resetKey: "",
      studentId: this.registerForm.value.registerStudentId,
      subject: this.registerForm.value.registerSubject,
      optedInCommunications: this.registerForm.value.optedInCommunications,
      recaptchaResponse: this.registerForm.value.recaptchaResponse,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      phoneNumber: this.registerForm.value.phoneNumber,
      functionOther: this.registerForm.value.functionOther,
      schoolName: this.registerForm.value.schoolName,
      schoolZipCode: this.registerForm.value.schoolZipCode,
      schoolState: this.registerForm.value.schoolState,
      //essName: this.registerForm.value.essName,
      //eventName: this.registerForm.value.eventName,
      mentorEmailList : this.registerForm.value.mentorShareEmails,
      //conferenceId: parseInt(this.registerForm.value.conferenceId, 10),
      functionId: parseInt(this.registerForm.value.functionId, 10),
      selectedSchools: selectedSchoolCodes,
      heardAboutUs: this.registerForm.value.heardAboutUs,
      heardAboutUsOther: this.registerForm.value.heardAboutUsOther,
      agreedToPolicy: this.registerForm.value.readPrivacyPolicy
    }

    let url = (this.isUsingPromoCode) ?
      this.serviceEndpoint.TeacherRegister :
      this.serviceEndpoint.Register;

    if (this.isUsingPromoCode) {
      formData.password = this.registerForm.value.registerPassword2;
    }

    let self = this;

    this._httpHelper.httpHelper('POST', '/applicationAccess/' + url, null, formData).then((response) => {

      self.registrationLoginStatus = response;
      let userData = response;

      if (self.registrationLoginStatus.statusNumber === 0) {	// success

        // Now log in the user ....
        self._authenticationService.SetCredentials(
          userData.email,
          'null',
          userData.user_id,
          userData.role,
          userData.accessCode,
          userData.schoolCode,
          userData.schoolName,
          userData.city,
          userData.state,
          userData.zipCode,
          userData.mepsId,
          userData.mepsName,
          false);
        
        self.isSubmitButtonDisabled = false;

        // Close Dialog
        self._dialogRef.close();

        self._loginService.redirectAfterLogin()

        // self._router.navigate(['/dashboard']);


      } else { // Error
    	  
    	self.isSubmitButtonDisabled = false;
        // if the user already exists but not using a promo account, merge accounts
        if (self.registrationLoginStatus.statusNumber === 1003 && !self.isUsingPromoCode) {
          self.showMergeModal(self.registerForm.value.registerEmail, self.registerForm.value.registerAccessCode);
        } else {
          //show error message
          self.registrationLabel = self.registrationLoginStatus.status;
          self.captchaElem.resetCaptcha();
          self.registerForm.value.recaptchaResponse = '';
          setTimeout(() => { self.registrationLabel = ''; }, 5000);
        }
      }

    }, error => {
    	self.isSubmitButtonDisabled = false;
    	console.error(error);
    })


  }


  showBringAsvabToYourSchool() {
    const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
      hasBackdrop: true,
      panelClass: 'new-modal',
      // height: '735px',
      autoFocus: false,
      disableClose: true
    });
  }

  showMergeModal(registerEmail, accessCode) {

    const modalOptions = {
      email: registerEmail,
      accessCode: accessCode
    };

    this._dialog.open(MergeAccountDialogComponent, {
      data: modalOptions,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: 500,
    }).beforeClosed().subscribe(result => {

      if (result) {

        if (result !== 'cancel') {

          //Access Code is already registered or unknown status was encountered from server
          if (result === 'errorEncountered' || result.statusNumber == 0) {

            // set credentials
            this._authenticationService.SetCredentials(
              result.email,
              'null',
              result.user_id,
              result.role,
              result.accessCode,
              result.schoolCode,
              result.schoolName,
              result.city,
              result.state,
              result.zipCode,
              result.mepsId,
              result.mepsName,
              true);

            window.location.href = this._config.getRestUrl() + '/sso/confirm_login?redirect=' + this._config.getSsoUrl() + 'login_callback/test';
            this._dialogRef.close();
          }
       
        }

        this.captchaElem.resetCaptcha();

        this.registerForm.value.recaptchaResponse = '';

        setTimeout(() => { this.registrationLabel = ''; }, 5000);

      }

    });

  }


}
