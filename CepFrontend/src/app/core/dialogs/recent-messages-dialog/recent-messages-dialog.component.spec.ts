import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentMessagesDialogComponent } from './recent-messages-dialog.component';

describe('RecentMessagesDialogComponent', () => {
  let component: RecentMessagesDialogComponent;
  let fixture: ComponentFixture<RecentMessagesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentMessagesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentMessagesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
