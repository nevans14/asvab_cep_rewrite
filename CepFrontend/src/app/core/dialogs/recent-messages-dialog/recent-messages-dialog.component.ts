import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { formatDate } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MentorService } from 'app/services/mentor.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { ClassroomActivity, ClassroomActivitySubmission, ClassroomActivitySubmissionComment, ClassroomActivitySubmissionLog } from 'app/core/models/classroomActivity';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserOccupationPlanComment } from 'app/core/models/userOccupationPlan.model';
import { Router } from '@angular/router';
import { MentorSettingComponent } from 'app/mentor-setting/mentor-setting.component';
import { EmailSettingsDialogComponent } from '../email-settings-dialog/email-settings-dialog.component';

enum CommentType {
	PORTFOLIO_COMMENT = 1,
	ACTIVITY_COMMENT = 2,
	PORTFOLIO_LOG = 3,
	ACTIVITY_LOG = 4,
	DIRECT_MESSAGE = 5
}

// View Model definitions used in the template file
export interface CommentBaseListViewItem {
	hasBeenRead: Boolean,
	typeId: Number,
	timestamp: String,
	message: String,
	from: String,
	isFromYou: Boolean,
	id: Number,
	recipientId: Number
}
export interface PortfolioListViewItem extends CommentBaseListViewItem {
	timestampReviewed: string,
	alwaysShow: boolean,
	hasBeenReviewed: boolean
}
@Component({
	selector: 'app-recent-messages-dialog',
	templateUrl: './recent-messages-dialog.component.html',
	styleUrls: ['./recent-messages-dialog.component.scss']
})
export class RecentMessagesDialogComponent implements OnInit {

	public activities: any[];
	// public studentName: string;
	public student: any;
	public portfolioComments: any;
	public portfolioLogs: any[];
	public portfolioListViewItems;
	public unreadPortfolioCount: number;
	public currentUserName: string;
	public activityListViewItems;
	public mentorLinkId: any;
	public globalIndex = 0;
	public showReadMessagesMaxNum = 5;
	public allMessagesList: any = [];
	public directMessages: DirectMessage[];
	private isDirectMessageItemLoading: boolean;
	private isItemsLoading: boolean;
	private occuPlans: any[];

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private _mentorService: MentorService,
		private _classRoomActivityService: ClassroomActivityService,
		private _portfolioService: PortfolioService,
		public _dialogRef: MatDialogRef<RecentMessagesDialogComponent>,
		private _userService: UserService,
		private _utilityService: UtilityService,
		private _userOccupationPlanService: UserOccupationPlanService,
		private _router: Router,
		private _matDialog: MatDialog) {

		this.portfolioListViewItems = [];
		this.activityListViewItems = [];
		// this.studentName = "";
		this.unreadPortfolioCount = 0;
		this.directMessages = [];
		this.isDirectMessageItemLoading = true;
		this.isItemsLoading = true;
		this.occuPlans = [];
	}

	ngOnInit() {

		this.allMessagesList = [];
		let students = this.data.students;

		let directMessages = this._userService.getDirectMessages(); //get all direct messages involving the current signed in user
		let userName = this._userService.getUsername();

		Promise.all([directMessages, userName]).then((done: any) => {
			this.directMessages = done[0];
			this.currentUserName = done[1] && done[1].username ? done[1].username : ''; //current signed in user;

			// ***** direct messages
			if (this.directMessages && this.directMessages.length > 0) {
				this.directMessages.forEach((dm: DirectMessage) => {

					// only show messages sent from student
					let isSentFromYou: boolean = (dm.fromUser == this.currentUserName) || (dm.fromUser == 'You') ? true : false;
					if (isSentFromYou) return;

					let fromUserStudent = (!isSentFromYou) ? students.find(x => x.emailAddress == dm.fromUser) : null;
					if (!fromUserStudent) return;

					let studentNameDM = "";
					if (!fromUserStudent.firstName || !fromUserStudent.lastName) {
						studentNameDM = fromUserStudent.emailAddress;
					} else {
						studentNameDM = fromUserStudent.firstName + " " + fromUserStudent.lastName;
					}

					let messageItem = {
						id: dm.id,
						from: studentNameDM,
						message: dm.message,
						timestamp: this._utilityService.convertDateToLocalDate(dm.dateCreated),
						originalTimestamp: dm.dateCreated,
						hasBeenRead: (isSentFromYou) ? true : dm.hasBeenRead,
						logType: CommentType.DIRECT_MESSAGE,
						recipientId: null,
						classroomActivityId: null,
						submissionId: null,
						isFromYou: isSentFromYou,
						mentorLinkId: dm.mentorLinkId,
					}

					this.allMessagesList.push(messageItem);
				})
			}
			this.isDirectMessageItemLoading = false

			for (let mentorLinkIndex = 0; mentorLinkIndex < students.length; mentorLinkIndex++) {

				let activities = this._mentorService.getStudentSubmittedActivities(students[mentorLinkIndex].mentorLinkId);
				let portfolioComments = this._mentorService.getStudentPortfolioComments(students[mentorLinkIndex].mentorLinkId);
				let portfolioLogs = this._mentorService.getStudentSubmittedPortfolios(students[mentorLinkIndex].mentorLinkId);
				let occuPlans = this._mentorService.getStudentCareerPlans(students[mentorLinkIndex].mentorLinkId);

				Promise.all([activities, portfolioComments, portfolioLogs, occuPlans]).then((done: any) => {

					this.activities = done[0];
					this.student = students[mentorLinkIndex];
					this.portfolioComments = done[1];
					this.portfolioLogs = done[2];
					this.occuPlans = done[3];

					let studentName = "";

					if (!this.student.firstName || !this.student.lastName) {
						studentName = this.student.emailAddress;
					} else {
						studentName = this.student.firstName + " " + this.student.lastName;
					}

					this.setPortfolioListView(this.portfolioComments, this.portfolioLogs, studentName);
					this.setActivityListView(this.activities, this.student.mentorLinkId, studentName);
					let occuPlansListItems = this.getOccuPlansListItemView(this.occuPlans, studentName);

					// Combine all messages
					for (let i = 0; i < this.activityListViewItems.length; i++) {

						this.activityListViewItems[i].mentorLinkId = this.student.mentorLinkId;
						this.activityListViewItems[i].logType = 'Activity';
						this.activityListViewItems[i].timestamp = this.activityListViewItems[i].timestampUpdated;
						this.allMessagesList.push(this.activityListViewItems[i]);
					}

					for (let i = 0; i < this.portfolioListViewItems.length; i++) {
						this.portfolioListViewItems[i].mentorLinkId = this.student.mentorLinkId;
						this.portfolioListViewItems[i].logType = 'Portfolio';
						this.allMessagesList.push(this.portfolioListViewItems[i]);
					}

					for (let i = 0; i < occuPlansListItems.length; i++) {
						occuPlansListItems[i].mentorLinkId = this.student.mentorLinkId;
						occuPlansListItems[i].logType = 'Plan';
						this.allMessagesList.push(occuPlansListItems[i]);
					}

					this.portfolioListViewItems = [];
					this.activityListViewItems = [];
					occuPlansListItems = [];

					if (mentorLinkIndex + 1 == students.length) {
						this.isItemsLoading = false;
					}

				});
			}

		});

	}

	addComment(item) {

		if (item.logType === 'Portfolio') {
			let formData: PortfolioComment = {
				from: null,
				id: null,
				message: item.addComment,
				dateCreated: null,
				to: null,
				commentTypeId: null,
				selectedUsers: [item.mentorLinkId]
			}

			this._portfolioService.sendComment(formData).then((response: PortfolioComment) => {
				item.addComment = undefined;
				item.showTextArea = false;
			}).catch(error => {
				console.error("ERROR", error);
			})
		}

		if (item.logType === 'Activity') {
			let formData: ClassroomActivitySubmissionComment = {
				from: null,
				id: null,
				message: item.addComment,
				dateCreated: null,
				to: null,
				selectedUsers: [item.mentorLinkId],
				classroomActivityId: item.classroomActivityId
			}

			this._classRoomActivityService.saveActivityComment(item.classroomActivityId, formData).then((response: ClassroomActivitySubmissionComment) => {
				item.addComment = undefined;
				item.showTextArea = false;
			}).catch(error => {
				console.error("ERROR", error);
			})
		}

		if (item.logType === 'Plan') {
			let formData: UserOccupationPlanComment = {
				from: null,
				id: null,
				message: item.addComment,
				dateCreated: null,
				to: null,
				selectedUsers: [item.mentorLinkId],
				planId: item.planId,
				commentTypeId: 2
			}

			this._userOccupationPlanService.saveComment(item.planId, formData).then((response: UserOccupationPlanComment) => {
				item.addComment = undefined;
				item.showTextArea = false;
			}).catch(error => {
				console.error("ERROR", error);
			})
		}

		if (item.logType == CommentType.DIRECT_MESSAGE) {

			let formData: DirectMessage = {
				id: null, // handled on server
				messageTypeId: messageType.DIRECT_MESSAGE,
				message: item.addComment,
				dateCreated: null, // handled on server
				fromUser: null, // handled on server
				toUser: null, // handled on server
				mentorLinkId: item.mentorLinkId,
				hasBeenRead: false,
				calendarDate: null, // handled on server
				dateUpdated: null, // handled on server
			}

			this._userService.sendDirectMessage(formData).then((result: DirectMessage) => {
				item.addComment = undefined;
				item.showTextArea = false;
			}).catch((error: any) => {
				console.error("ERROR", error);
			})

		}
	}

	filterReadMessages() {
		return this.allMessagesList.filter(a => a.hasBeenRead && !a.isFromYou);
	}

	readMessageLength() {
		return this.allMessagesList.filter(a => a.hasBeenRead && !a.isFromYou).length;
	}

	unreadMessageLength() {
		return this.allMessagesList.filter(a => !a.hasBeenRead && !a.isFromYou).length;
	}

	closeMe() {
		this._dialogRef.close();
	}

	markAsReviewed(item: any) {

		if (item.logType == 'Portfolio') {
			this._mentorService.setStudentPortfolioAsReviewed(item.mentorLinkId, true).then((rowsAffected: number) => {

				if (rowsAffected > 0) {
					// update UI
					let portfolioItemIndex = this.allMessagesList.findIndex(x => x.logType == 'Portfolio' && x.id == item.id && x.typeId == 2);
					if (portfolioItemIndex > -1) {
						this.allMessagesList[portfolioItemIndex].timestampUpdated = this._utilityService.convertDateToLocalDate(new Date(Date.now()), false);
						this.allMessagesList[portfolioItemIndex].originalTimestamp = new Date(Date.now());
						this.allMessagesList[portfolioItemIndex].hasBeenReviewed = true;
						// set the record as being read if it hasn't been
						if (!this.allMessagesList[portfolioItemIndex].hasBeenRead) {
							this.markAsRead(item);
							//updates the dashboard with new student data
							this._mentorService.sendUpdate(true);
						}
					}
				}

			}).catch(error => console.error("ERROR", error));
		}

		if (item.logType == 'Activity') {
			this._mentorService.setStudentSubmissionAsReviewed(item.mentorLinkId, item.classroomActivityId, item.id, true).then((rowsAffected: any) => {

				if (rowsAffected > 0) {

					var activityItemIndex = this.allMessagesList.findIndex(x => x.logType == 'Activity' && x.classroomActivityId == item.classroomActivityId && x.typeId == 2
						&& x.id == item.id && x.logId == item.logId);

					if (activityItemIndex > -1) {
						this.allMessagesList[activityItemIndex].timestampUpdated
							= this._utilityService.convertDateToLocalDate(new Date(Date.now()), false);
						this.allMessagesList[activityItemIndex].originalTimestamp = new Date(Date.now());
						this.allMessagesList[activityItemIndex].hasBeenReviewed = true;
						this.allMessagesList[activityItemIndex].statusId = 5;

						// set the record as being read if it hasn't been
						if (!this.allMessagesList[activityItemIndex].hasBeenRead) {
							this.markAsRead(item);
							//updates the dashboard with new student data
							this._mentorService.sendUpdate(true);
						}
					}

				}

			}).catch(error => console.error(error));
		}

		if (item.logType == 'Plan') {
			this._mentorService.markStudentOccupationPlanAsReviewed(item.mentorLinkId, item.planId, true).then((rowsAffected: any) => {

				if (rowsAffected > 0) {

					var itemIndex = this.allMessagesList.findIndex(x => x.logType == item.logType && x.planId == item.planId && x.typeId == 2
						&& x.logId == item.logId);

					if (itemIndex > -1) {
						this.allMessagesList[itemIndex].timestampUpdated
							= this._utilityService.convertDateToLocalDate(new Date(Date.now()), false);
						this.allMessagesList[itemIndex].originalTimestamp = new Date(Date.now());
						this.allMessagesList[itemIndex].hasBeenReviewed = true;
						this.allMessagesList[itemIndex].statusId = 5;

						// set the record as being read if it hasn't been
						if (!this.allMessagesList[itemIndex].hasBeenRead) {
							this.markAsRead(item);
							//updates the dashboard with new student data
							this._mentorService.sendUpdate(true);
						}
					}

				}

			}).catch(error => console.error(error));
		}

	}

	markAllAsRead() {

		let items = this.allMessagesList.filter(x => !x.hasBeenRead && !x.isFromYou);

		if (items.length > 0) {
			items.forEach(x => {
				this.markAsRead(x);
			});
		}

	}

	markAsRead(item) {

		if (item.logType == 'Activity') {
			// typeId
			// 1 = comment
			// 2 = submission
			if (item.typeId == 1) {
				this._classRoomActivityService.updateActivityCommentAsRead(item.classroomActivityId, item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

					if (rowsAffected > 0) {

						let index = this.allMessagesList.findIndex(x => x.logType == 'Activity' && x.classroomActivityId == item.classroomActivityId && x.id == item.id && x.typeId == 1);

						if (index > -1) {
							this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
							// activity.unreadMessageCount--;
						}
					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);

				});
			}

			if (item.typeId == 2) {
				this._mentorService.setStudentSubmissionLogAsRead(item.mentorLinkId, item.classroomActivityId, item.id, item.logId, !item.hasBeenRead).then((rowsAffected: any) => {

					if (rowsAffected > 0) {
						var activityIndex = this.allMessagesList.findIndex(x => x.logType == 'Activity' && x.classroomActivityId == item.classroomActivityId && x.typeId == 2
							&& x.id == item.id && x.logId == item.logId);

						if (activityIndex > -1) {
							this.allMessagesList[activityIndex].hasBeenRead = true;
						}

					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);

				}).catch(error => console.error(error));

			}

		}

		if (item.logType == 'Portfolio') {
			// typeId
			// 1 = comment
			// 2 = portfolio
			if (item.typeId == 1) {
				this._portfolioService.updateCommentByHasBeenRead(item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

					if (rowsAffected > 0) {
						let index = this.allMessagesList.findIndex(x => x.logType == 'Portfolio' && x.id == item.id && x.recipientId == item.recipientId && x.typeId == 1);

						if (index > -1) {
							this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
						}
					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);
				});

			}

			if (item.typeId == 2) {
				this._mentorService.setStudentPortfolioLogAsRead(item.mentorLinkId, item.id, !item.hasBeenRead).then((rowsAffected: any) => {

					if (rowsAffected > 0) {
						let index = this.allMessagesList.findIndex(x => x.logType == 'Portfolio' && x.id == item.id && x.typeId == 2);

						if (index > -1) {
							this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
						}
					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);

				}).catch(error => console.error(error));

			}


		}

		if (item.logType == 'Plan') {
			// typeId
			// 1 = comment
			// 2 = log
			if (item.typeId == 1) {
				this._userOccupationPlanService.markCommentAsRead(item.planId, item.id, item.recipientId,).then((rowsAffected: any) => {

					if (rowsAffected > 0) {
						let index = this.allMessagesList.findIndex(x => x.logType == 'Plan' && x.id == item.id && x.typeId == 1);

						if (index > -1) {
							this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
						}
					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);
				});

			}

			if (item.typeId == 2) {
				this._mentorService.markStudentOccupationPlanLogAsRead(item.mentorLinkId, item.planId, item.id, true).then((rowsAffected: any) => {

					if (rowsAffected > 0) {
						let index = this.allMessagesList.findIndex(x => x.logType == 'Plan' && x.id == item.id && x.typeId == 2);

						if (index > -1) {
							this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
						}
					}

					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);

				}).catch(error => console.error(error));

			}


		}

		if (item.logType == CommentType.DIRECT_MESSAGE) {
			this._userService.updateMessage(item.id).then((rowsAffected: any) => {

				//updates the dashboard with new student data
				this._mentorService.sendUpdate(true);

				if (rowsAffected > 0) {
					let index = this.allMessagesList.findIndex(x => x.id == item.id && x.logType == CommentType.DIRECT_MESSAGE);

					if (index > -1) {
						this.allMessagesList[index].hasBeenRead = !this.allMessagesList[index].hasBeenRead;
					}
				}

			});
		}

	}

	setActivityListView(activities: ClassroomActivity[], mentorLinkId, studentName) {
		if (activities.length > 0) {
			activities.forEach((activity: ClassroomActivity) => {

				let activityListViewItems = this.getActivityListViewItems(activity, mentorLinkId, studentName);

				if (activityListViewItems && activityListViewItems.length > 0) {
					activityListViewItems.forEach(x => this.activityListViewItems.push(x));
				}

			});
		}
	}

	getActivityListViewItems(activity: ClassroomActivity, mentorLinkId, studentName) {
		let returnObj: any[] = [];
		//add any unread/read comments that aren't from you
		if (activity.comments && activity.comments.length > 0) {
			activity.comments.forEach(comment => {

				let isFromYou: boolean = (comment.from == 'You' || comment.from == this.currentUserName);

				//ignore comments from you only worried about inbound.
				if (!isFromYou) {

					let hasBeenRead = comment.to[0].hasBeenRead;

					returnObj.push({
						hasBeenRead: hasBeenRead,
						typeId: 1, // comment
						timestampCreated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
						timestampUpdated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
						message: comment.message,
						from: studentName,
						isFromYou: isFromYou,
						id: comment.to[0].commentId,
						logId: null,
						recipientId: comment.to[0].id,
						file: null,
						hasBeenReviewed: null,
						alwaysShow: false,
						statusId: null,
						fileName: null,
						classroomActivityId: activity.id,
						title: activity.title,
						originalTimestamp: comment.dateCreated
					});
				}

			})
		}

		//get the latest log for each submission
		//only show in the ui if statusId == 2(submitted) and its unread
		if (activity.submissions && activity.submissions.length > 0) {

			// loop through each submission; and display its logs to the user
			activity.submissions.forEach(submission => {

				let mentor = submission.mentors.find(x => x.mentorId == mentorLinkId);
				// if not currently assigned and there is no log history skip
				if (!mentor && submission.logs.length == 0) return;

				// sort submissions
				// sort logs based on dates in descending order
				let sortedSubmissionLogs = submission.logs.sort((a, b) => {
					let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
						db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
					return da - db;
				});
				let lastSubmissionLog = sortedSubmissionLogs[sortedSubmissionLogs.length - 1];

				if (lastSubmissionLog && lastSubmissionLog.statusId == 2) {
					returnObj.push({
						hasBeenRead: lastSubmissionLog.hasBeenRead,
						typeId: 2, // log item
						timestampCreated: this._utilityService.convertDateToLocalDate(lastSubmissionLog.dateLogged),
						timestampUpdated: this._utilityService.convertDateToLocalDate(lastSubmissionLog.dateLogged),
						message: this.getSubmissionStatusDescription(lastSubmissionLog.statusId),
						from: studentName,
						isFromYou: false,  // submissions are never from a mentor
						id: submission.id, // workId
						logId: lastSubmissionLog.id,
						recipientId: null,
						file: submission.fileName,
						hasBeenReviewed: false,
						alwaysShow: false,
						statusId: lastSubmissionLog.statusId,
						fileName: submission.fileName,
						classroomActivityId: activity.id,
						title: activity.title,
						originalTimestamp: lastSubmissionLog.dateLogged
					});
				}

			});

		}

		return returnObj;
	}

	setPortfolioListView(portfolioComments: PortfolioComment[], portfolioLogs: any[], studentName) {
		let unreadCounter: number = 0;
		if (portfolioComments.length > 0) {
			portfolioComments.forEach((comment: PortfolioComment) => {

				let commentToMe = comment.to.find(x => x.username == 'You' || x.username == this.currentUserName);
				let isFromYou: boolean = (comment.from === 'You' || comment.from == this.currentUserName);

				if (commentToMe || isFromYou) {
					this.portfolioListViewItems.push(this.getPortfolioListCommentViewItem(comment, studentName, isFromYou));
					unreadCounter = (!isFromYou && !commentToMe.hasBeenRead) ? unreadCounter + 1 : unreadCounter + 0;
				}

			})
		}
		//for portfolio we only care about the status of the last log item
		if (portfolioLogs.length > 0) {
			//sort portfolio logs to determine the last log item
			let sortedPortfolioLogs = portfolioLogs.sort((a, b) => {
				let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
					db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
				return da - db;
			});
			//add the last log item to the view
			this.portfolioListViewItems.push(this.getPortfolioLogViewItem(sortedPortfolioLogs[sortedPortfolioLogs.length - 1], studentName));
		}
		this.unreadPortfolioCount = unreadCounter;
	}

	getOccuPlansListItemView(occuPlans: any[], studentName) {
		let returnObj: any[] = [];

		if (occuPlans && occuPlans.length > 0) {

			occuPlans.forEach((plan: any) => {

				//add any unread/read comments that aren't from you
				if (plan.comments && plan.comments.length > 0) {
					plan.comments.forEach(comment => {

						let isFromYou: boolean = (comment.from == 'You' || comment.from == this.currentUserName);

						if (!isFromYou) {
							let hasBeenRead: boolean;

							if (comment.to && comment.to.length > 0) {
								hasBeenRead = comment.to.find(x => x.username == 'You' || x.username == this.currentUserName).hasBeenRead;

								returnObj.push({
									hasBeenRead: hasBeenRead,
									typeId: 1, // comment
									timestampCreated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
									timestampUpdated: this._utilityService.convertDateToLocalDate(comment.dateCreated),
									message: comment.message,
									from: studentName,
									isFromYou: isFromYou,
									id: comment.to[0].commentId,
									logId: null,
									recipientId: comment.to[0].id,
									file: null,
									hasBeenReviewed: null,
									alwaysShow: false,
									statusId: null,
									fileName: null,
									classroomActivityId: null,
									planId: plan.id,
									title: plan.title,
									originalTimestamp: comment.dateCreated
								});
							}
						}
					})
				}

				//add any unread logs
				//get the latest log for each submission
				//only show in the ui if statusId == 2(submitted) and its unread
				if (plan.logs && plan.logs.length > 0) {

					//look at the latest log
					//if status of submitted and unread add it to the recent notifications.
					let sortedLogs = plan.logs.sort((a, b) => {
						let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
							db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
						return da - db;
					});
					let lastLog = sortedLogs[sortedLogs.length - 1];

					if (lastLog.statusId == 2 && !lastLog.hasBeenRead) {
						returnObj.push({
							hasBeenRead: lastLog.hasBeenRead,
							typeId: 2, // log item
							timestampCreated: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
							timestampUpdated: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
							message: this.getSubmissionStatusDescription(lastLog.statusId),
							from: studentName,
							isFromYou: false,
							id: lastLog.id,
							logId: lastLog.id,
							recipientId: null,
							file: null,
							hasBeenReviewed: false,
							alwaysShow: false,
							statusId: lastLog.statusId,
							fileName: null,
							classroomActivityId: null,
							title: plan.title,
							planId: plan.id,
							originalTimestamp: lastLog.dateLogged
						});
					}

				}
			})

		}

		return returnObj;
	}

	getPortfolioListCommentViewItem(comment: PortfolioComment, studentName, isFromYou) {

		let returnObj: any = {
			hasBeenRead: isFromYou ? true : comment.to.find(x => x.username == 'You' || x.username == this.currentUserName).hasBeenRead,
			typeId: 1, // comment
			timestamp: this._utilityService.convertDateToLocalDate(comment.dateCreated),
			message: comment.message,
			from: (isFromYou) ? 'You' : studentName,
			isFromYou: isFromYou,
			id: isFromYou ? comment.to[0].commentId : comment.to.find(x => x.username == 'You' || x.username == this.currentUserName).commentId,
			recipientId: isFromYou ? comment.to[0].id : comment.to.find(x => x.username == 'You' || x.username == this.currentUserName).id,
			timestampReviewed: null,
			hasBeenReviewed: null,
			alwaysShow: false,
			originalTimestamp: comment.dateCreated
		}
		return returnObj;
	}

	getPortfolioLogViewItem(log: any, studentName) {

		//if status of the portfolio log item is submitted; then the message is not from you
		//otherwise it is
		let isFromYou: boolean = (log.statusId == 2) ? false : true;

		//let isFromYou: boolean = false;
		let returnObj: any = {
			hasBeenRead: (isFromYou) ? true : log.hasBeenRead,
			typeId: 2, // submission
			timestamp: this._utilityService.convertDateToLocalDate(log.dateLogged),
			message: this.getSubmissionStatusDescription(log.statusId),
			from: (isFromYou) ? 'You' : studentName,
			isFromYou: isFromYou,
			id: log.id,
			recipientId: null,
			timestampReviewed: (log.dateUpdated) ? this._utilityService.convertDateToLocalDate(log.dateUpdated) : this._utilityService.convertDateToLocalDate(log.dateLogged),
			hasBeenReviewed: (log.statusId == 2) ? false : true,
			alwaysShow: false,
			originalTimestamp: log.dateLogged
		}
		return returnObj;
	}

	getSubmissionStatusDescription(statusId) {
		if (statusId == 1) {
			return "In Progress";
		}
		if (statusId == 2) {
			return "Submitted";
		}
		if (statusId == 3) {
			return "Unsubmitted";
		}
		if (statusId == 4) {
			return "Deleted";
		}
		if (statusId == 5) {
			return "Reviewed";
		}
	}

	showNotificationSettings() {

		this._dialogRef.close();
		this._matDialog.open(EmailSettingsDialogComponent);

	}

	closeGoToStudent(item) {

		this._dialogRef.close();
		this._router.navigate(['/mentor-student-detail/' + item.mentorLinkId]);
	}
}
