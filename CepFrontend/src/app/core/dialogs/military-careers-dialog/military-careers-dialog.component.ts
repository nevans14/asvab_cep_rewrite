import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';

@Component({
  selector: 'app-military-careers-dialog',
  templateUrl: './military-careers-dialog.component.html',
  styleUrls: ['./military-careers-dialog.component.scss']
})
export class MilitaryCareersDialogComponent implements OnInit {

  socId;
  militaryMoreDetails;
  ssoUrl;
  title;

  constructor(
    private _ga: GoogleAnalyticsService,
    private _config: ConfigService,
    private _dialogRef: MatDialogRef<MilitaryCareersDialogComponent>,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _user: UserService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.ssoUrl = this._config.getSsoUrl();
  }

  ngOnInit() {
    this.socId = this.data && this.data.socId ? this.data.socId : undefined;
    this.title = this.data && this.data.title ? this.data.title : '';

    if (this.socId) {
      this._occufindRestFactoryService.getMilitaryMoreDetails(this.socId).subscribe(details => {
        this.militaryMoreDetails = details;
      });
    }

    if ( this._user.isLoggedIn() ) {
      this._user.setCompletion(UserService.MILITARY_PATHWAY_VIEW_CAREERS, 'true');
    }
  }

  onNoClick(): void {
    this._dialogRef.close();
  }
}
