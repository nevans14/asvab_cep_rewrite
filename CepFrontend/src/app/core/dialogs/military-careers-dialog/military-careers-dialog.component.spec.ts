import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilitaryCareersDialogComponent } from './military-careers-dialog.component';

describe('MilitaryCareersDialogComponent', () => {
  let component: MilitaryCareersDialogComponent;
  let fixture: ComponentFixture<MilitaryCareersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilitaryCareersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilitaryCareersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
