import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpHelperService } from 'app/services/http-helper.service';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-edit-account-dialog',
  templateUrl: './edit-account-dialog.component.html',
  styleUrls: ['./edit-account-dialog.component.scss']
})
export class EditAccountDialogComponent implements OnInit {

  username: string = undefined;
  pwCurrent: string = undefined;
  pw1: string = '';
  pw2: string = '';
  errorMsg: string = undefined;
  isSaving: boolean = false;
  public isLoading: boolean;
  public pendingStudents: any[];
  public hasPendingStudents: boolean;
  public oldUsername: string;
  constructor(
    public _dialogRef: MatDialogRef<EditAccountDialogComponent>,
    private _httpHelper: HttpHelperService,
    private _mentorService: MentorService,
    @Inject(MAT_DIALOG_DATA) public data,
    private _user: UserService
  ) {
    this.isLoading = true;
    this.pendingStudents = [];
    this.hasPendingStudents = false;
    this.oldUsername = "";
  }

  ngOnInit() {
    this.username = this.data.username;
    this.oldUsername = this.username;

    this.getData().then(() => {
      this.isLoading = false;
      this.hasPendingStudents = this.pendingStudents.length > 0 ? true : false;
    })
  }

  async getData() {

    let currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    if(userRole == 'S'){
      return;
    }

    let pendingStudents = this._mentorService.getPendingStudents();

    await Promise.all([pendingStudents]).then((done: any) => {
      this.pendingStudents = done[0];
    })
  }

  responseHandling(response) {
    if (response.statusCode === 0) {
      // if (response.isUsernameUpdated) {
      //   AuthenticationService.resetUserName($scope.form.username);
      //   $rootScope.$emit("ResetUserInfo", $scope.form);
      // }

      // if (response.isPasswordUpdated) {
      //   AuthenticationService.resetPassword($scope.form.pw1);
      // }

      this.closeDialog();
    } else {
      this.errorMsg = response.statusMessage;
    }

    this.isSaving = false;
  };

  save() {
    var passCurrent = this.pwCurrent;
    var pass1 = this.pw1;
    var pass2 = this.pw2;

    let data: any = {};

    if (passCurrent === '') {
      this.errorMsg = "Current password missing!";
      return;
    }
    if (pass1.length <= 7 &&
      pass1 !== '') {
      this.errorMsg = "Password too short!";
      return;
    }
    if (pass1 !== pass2) {
      this.errorMsg = "Passwords don't match!";
      this.pw1 = '';
      this.pw2 = '';
      return;
    }

    if (this.hasPendingStudents) {
      if (this.oldUsername != this.username) {
        this.errorMsg = "Cannot change username, try again.";
        return;
      }
    }

    data['updatedUsername'] = this.username;
    data['password'] = passCurrent;
    data['updatedPassword'] = pass2;

    this.isSaving = true;
    this._httpHelper.httpHelper('post', 'user/updateUsername', null, data).then(response => {
      this.responseHandling(response);
    })
  }

  closeDialog() {
    this._dialogRef.close({ username: this.username });
  }

  cancel(){
    this._dialogRef.close();
  }
}
