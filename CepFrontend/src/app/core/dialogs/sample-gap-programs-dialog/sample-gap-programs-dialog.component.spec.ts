import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleGapProgramsDialogComponent } from './sample-gap-programs-dialog.component';

describe('SampleGapProgramsDialogComponent', () => {
  let component: SampleGapProgramsDialogComponent;
  let fixture: ComponentFixture<SampleGapProgramsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleGapProgramsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleGapProgramsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
