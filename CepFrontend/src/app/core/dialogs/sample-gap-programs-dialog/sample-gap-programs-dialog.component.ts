import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { RefGapYearPlan } from 'app/core/models/refGapYearPlan.model';
import { GapYearPlanService } from 'app/services/gap-year-plan.service';

@Component({
  selector: 'app-sample-gap-programs-dialog',
  templateUrl: './sample-gap-programs-dialog.component.html',
  styleUrls: ['./sample-gap-programs-dialog.component.scss']
})
export class SampleGapProgramsDialogComponent implements OnInit {

  public gapYearOptions: RefGapYearPlan[];
  public isLoading: boolean;
  public selectedGapYearOptions: RefGapYearPlan[];
  public checkedarray: boolean[];

  constructor(private _gapYearPlanService: GapYearPlanService,
    private _dialogRef: MatDialogRef<SampleGapProgramsDialogComponent>) {
    this.gapYearOptions = [];
    this.isLoading = true;
    this.selectedGapYearOptions = [];
    this.checkedarray = [];
  }

  ngOnInit() {

    //get gap year options
    this.getAllGapYearOptions();

  }

  async getAllGapYearOptions() {

    try {
      const gapYearOptions: any = await this._gapYearPlanService.getAllGapYearOptions();

      if (gapYearOptions) {
        for (let gapYearOption of gapYearOptions) {
          this.gapYearOptions.push(gapYearOption);
          this.checkedarray.push(false);
        };
      }

      this.isLoading = false;
    } catch (error) {
      console.error("ERROR:", error);
    }

  }

  updateSelection(value: RefGapYearPlan, checked: boolean, index: number) {

    if (checked) {
      this.selectedGapYearOptions = [];  //clear existing array
      for (var i = 0; i < this.checkedarray.length; i++) {
        if (i != index) {
          if (this.checkedarray[i] == true) {
            this.checkedarray[i] = false;
          }
        }
      }
      this.selectedGapYearOptions.push(value);

    } else {

      this.selectedGapYearOptions = [];
    }
  }

  closeAndSave() {
    //will always be one record
    this._dialogRef.close(this.selectedGapYearOptions[0]);
  }


}
