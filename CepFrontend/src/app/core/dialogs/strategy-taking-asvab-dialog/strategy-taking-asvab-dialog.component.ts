import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { ContentManagementService } from 'app/services/content-management.service';
import { AsvabSampleTestDialogComponent } from 'app/core/dialogs/asvab-sample-test-dialog/asvab-sample-test-dialog.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { ConfigService } from 'app/services/config.service';
import { AsvabIcatSampleTestDialogComponent } from '../asvab-icat-sample-test-dialog/asvab-icat-sample-test-dialog.component';

@Component({
  selector: 'app-strategy-taking-asvab-dialog',
  templateUrl: './strategy-taking-asvab-dialog.component.html',
  styleUrls: ['./strategy-taking-asvab-dialog.component.scss']
})
export class StrategyTakingAsvabDialogComponent implements OnInit {

  pageHtml = [];
  path;
  documents;

  constructor(
    public _dialogRef: MatDialogRef<StrategyTakingAsvabDialogComponent>,
    private _contentManagementService: ContentManagementService,
    private _router: Router,
    private _dialog: MatDialog,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _configService: ConfigService,
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName('student-program');
    this.documents = this._configService.getImageUrl() + 'CEP_PDF_Contents/';
  }

  showSampleTestModal() {
    let dialogRef = this._dialog.open(AsvabSampleTestDialogComponent, {
      minWidth: '80vw',
      autoFocus: false,
      disableClose: true,
      // data: modalOptions
    });

    dialogRef.afterClosed().subscribe(result => {
      this._dialog.open(StrategyTakingAsvabDialogComponent , {
        minWidth: '90vw',
        autoFocus: false,
        disableClose: true,
      });
      this.close();
    })
  }

  showSampleIcatTestModal() {
    let dialogRef = this._dialog.open(AsvabIcatSampleTestDialogComponent, {
      minWidth: '80vw',
      autoFocus: false,
      disableClose: true,
      // data: modalOptions
    });

    dialogRef.afterClosed().subscribe(result => {
      this._dialog.open(StrategyTakingAsvabDialogComponent , {
        minWidth: '90vw',
        autoFocus: false,
        disableClose: true,
      });
      this.close();
    })
  }

  reOpen() {
      this._dialog.open(StrategyTakingAsvabDialogComponent , {
        minWidth: '90vw',
        autoFocus: false,
        disableClose: true,
      });
      this.close();
  }

  closeAll() {
    this._dialog.closeAll();
  }

  close() {
    this._dialogRef.close();
  }

}
