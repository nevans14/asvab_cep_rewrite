import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyTakingAsvabDialogComponent } from './strategy-taking-asvab-dialog.component';

describe('StrategyTakingAsvabDialogComponent', () => {
  let component: StrategyTakingAsvabDialogComponent;
  let fixture: ComponentFixture<StrategyTakingAsvabDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyTakingAsvabDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyTakingAsvabDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
