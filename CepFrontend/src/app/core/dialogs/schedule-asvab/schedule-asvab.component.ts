import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';

@Component({
  selector: 'app-schedule-asvab',
  templateUrl: './schedule-asvab.component.html',
  styleUrls: ['./schedule-asvab.component.scss']
})
export class ScheduleAsvabComponent implements OnInit {
  schedFormGroup: FormGroup;
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  path;
  errorMessage: string;
  formDisabled = false;
  heardOther = false;

  constructor(
    private _config: ConfigService,
    private _dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _http: HttpHelperService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _router: Router,
    public _dialogRef: MatDialogRef<ScheduleAsvabComponent>
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.schedFormGroup = this._formBuilder.group({
      recaptcha: ['', Validators.required],
      title: ['', Validators.required],
      position: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      heardUs: [''],
      heardUsOther: [''],
      schoolName: ['', Validators.required],
      schoolAddress: ['', Validators.required],
      schoolCity: ['', Validators.required],
      schoolCounty: ['', Validators.required],
      schoolState: ['', Validators.required],
      schoolZip: ['', Validators.required],
      asvabTest: [''],
      pti: [{ value: false, disabled: false }],
      ptiVirtual: [{ value: false, disabled: true }],
      ptiInPerson: [{ value: false, disabled: true }],
      classroomActivities: [{ value: false, disabled: false }],
      classroomActivitiesVirtual: [{ value: false, disabled: true }],
      classroomActivitiesInPerson: [{ value: false, disabled: true }],
      presentation: [{ value: false, disabled: false }],
      presentationVirtual: [{ value: false, disabled: true }],
      presentationInPerson: [{ value: false, disabled: true }]
    });

  }

  onScheduleSubmit() {
    this.errorMessage = undefined;
    this.formDisabled = true;

    if (this.schedFormGroup.value.recaptcha === '') { // if string is empty
      this.errorMessage = 'Please resolve the captcha and submit!';
      this.formDisabled = false;
      return false;
    } else {

      const emailObject = {
        title: this.schedFormGroup.value.title,
        recaptcha: this.schedFormGroup.value.recaptcha,
        position: this.schedFormGroup.value.position,
        firstName: this.schedFormGroup.value.firstName,
        lastName: this.schedFormGroup.value.lastName,
        phone: this.schedFormGroup.value.phone,
        email: this.schedFormGroup.value.email,
        heardUs: this.schedFormGroup.value.heardUs,
        heardUsOther: this.schedFormGroup.value.heardUsOther,
        schoolName: this.schedFormGroup.value.schoolName,
        schoolAddress: this.schedFormGroup.value.schoolAddress,
        schoolCity: this.schedFormGroup.value.schoolCity,
        schoolCounty: this.schedFormGroup.value.schoolCounty,
        schoolState: this.schedFormGroup.value.schoolState,
        schoolZip: this.schedFormGroup.value.schoolZip
      };

      const self = this;
      return this._http.httpHelper('post', 'contactUs/scheduleEmail', null, emailObject)
        .then(function (success) {

          self.ga(self.path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT');

          const data = {
            'title': 'Schedule Notification',
            'message': 'Your information was submitted successfully.'
          };
          const dialogRef = self._dialog.open(MessageDialogComponent, {
            data: data
          });

          self.onClose();
          return;
        })
        .catch(error => {
          if (!error['message'] || error['message'] === 'null') {
            self.errorMessage = 'There was an error proccessing your request. Please try again.';
          } else {
            self.errorMessage = error['message'];
          }
          self.schedFormGroup.value.recaptcha = '';
          self.formDisabled = false;
        });
    }
  }

  ga(param) {

    this._googleAnalyticsService.trackClick(param);
  }

  onHeardChange() {
    if (this.schedFormGroup.value.heardUs === 'other') {
      this.heardOther = true;
    } else {
      this.heardOther = false;
    }
  }

  onClose() {
    this._dialogRef.close();
  }

  updateCheckboxes(groupName, value) {

    if (value === true) {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").enable();
        this.schedFormGroup.get("ptiInPerson").enable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").enable();
        this.schedFormGroup.get("classroomActivitiesInPerson").enable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").enable();
        this.schedFormGroup.get("presentationInPerson").enable();
      }
    } else {
      if (groupName === 'pti') {
        this.schedFormGroup.get("ptiVirtual").disable();
        this.schedFormGroup.get("ptiInPerson").disable();
      } else if (groupName === 'classroomActivities') {
        this.schedFormGroup.get("classroomActivitiesVirtual").disable();
        this.schedFormGroup.get("classroomActivitiesInPerson").disable();
      } else if (groupName === 'presentation') {
        this.schedFormGroup.get("presentationVirtual").disable();
        this.schedFormGroup.get("presentationInPerson").disable();
      }
    }

  }

}
