import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleAsvabComponent } from './schedule-asvab.component';

describe('ScheduleAsvabComponent', () => {
  let component: ScheduleAsvabComponent;
  let fixture: ComponentFixture<ScheduleAsvabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleAsvabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleAsvabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
