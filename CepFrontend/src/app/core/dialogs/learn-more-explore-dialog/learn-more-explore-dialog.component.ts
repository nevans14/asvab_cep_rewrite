import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-learn-more-explore-dialog',
  templateUrl: './learn-more-explore-dialog.component.html',
  styleUrls: ['./learn-more-explore-dialog.component.scss']
})
export class LearnMoreExploreDialogComponent implements OnInit {
  modalIndex = undefined;

  constructor(
    public _dialogRef: MatDialogRef<LearnMoreExploreDialogComponent>,
    public _utility: UtilityService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    switch (this.data['modalName'].toLowerCase()) {
      case 'favorite-occupation':
        this.modalIndex = 0;
        break;
      case 'favorite-college':
        this.modalIndex = 1;
        break;
      case 'favorite-cc':
        this.modalIndex = 2;
        break;
      default:
        break;
    }
  }

  onClose(): void {
    this._dialogRef.close();
  }
}
