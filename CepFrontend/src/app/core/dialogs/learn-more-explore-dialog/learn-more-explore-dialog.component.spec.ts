import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMoreExploreDialogComponent } from './learn-more-explore-dialog.component';

describe('LearnMoreExploreComponent', () => {
  let component: LearnMoreExploreDialogComponent;
  let fixture: ComponentFixture<LearnMoreExploreDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnMoreExploreDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMoreExploreDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
