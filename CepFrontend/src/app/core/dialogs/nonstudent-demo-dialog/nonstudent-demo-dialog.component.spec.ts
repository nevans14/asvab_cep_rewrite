import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonstudentDemoDialogComponent } from './nonstudent-demo-dialog.component';

describe('NonstudentDemoDialogComponent', () => {
  let component: NonstudentDemoDialogComponent;
  let fixture: ComponentFixture<NonstudentDemoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonstudentDemoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonstudentDemoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
