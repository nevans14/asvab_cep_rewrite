import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-nonstudent-demo-dialog',
  templateUrl: './nonstudent-demo-dialog.component.html',
  styleUrls: ['./nonstudent-demo-dialog.component.scss']
})
export class NonstudentDemoDialogComponent implements OnInit {

  constructor(
    public _dialogRef: MatDialogRef<NonstudentDemoDialogComponent>,
    private _user: UserService,
  ) { }

  ngOnInit() {
  }

  onClose(): void {
    this._user.setCompletion(UserService.NONSTUDENT_DEMO_CLOSED_HINT, 'true');
    this._dialogRef.close();
  }
}
