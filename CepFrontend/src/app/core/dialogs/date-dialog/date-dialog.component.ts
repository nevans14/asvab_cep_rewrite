import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-date-dialog',
  templateUrl: './date-dialog.component.html',
  styleUrls: ['./date-dialog.component.scss']
})
export class DateDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public dateFormGroup: FormGroup;
  public userOccupationPlans: UserOccupationPlan[];
  public userOccupationPlanCalendarTasks: UserOccupationPlanCalendarTask[];
  public tasks: FormArray;
  public wasOpenInPlan: boolean;
  public plan: any;

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _planCalendarService: PlanCalendarService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _matDialog: MatDialog,
    private _occufindRestFactory: OccufindRestFactoryService,
    private _userService: UserService,
    private _utilityService: UtilityService,
    @Inject(LOCALE_ID) private locale: string
    ) {

    this.isSaving = false;
    this.isLoading = true;
    this.wasOpenInPlan = false;
    this.plan = {};
    this.dateFormGroup = this._formBuilder.group({
      id: [''],
      title: ['', [Validators.required, Validators.maxLength(500)]],
      userOccupationPlan: [''],
      addTask: [''],
      date: ['', [Validators.required]],
      tasks: this._formBuilder.array([])
    });

    this.tasks = this.dateFormGroup.get('tasks') as FormArray;

  }

  ngOnInit() {

    //get all plans
    let userOccupationPlans = this.getUserOccupationPlans();

    //get all tasks
    let userOccupationPlanCalendarTasks = this.getAllUserOccupationPlanCalendarTasks();

    Promise.all([userOccupationPlans, userOccupationPlanCalendarTasks]).then((done: any) => {

      this.userOccupationPlans = done[0];
      this.userOccupationPlanCalendarTasks = done[1];

      if (this.data.userOccupationPlanCalendar) {

        if (this.data.canEditPlan) {
          this.wasOpenInPlan = false;
          //if plan id was given then set the initial drop down value
          this.dateFormGroup.get("userOccupationPlan").setValue(this.data.userOccupationPlanCalendar.planId);
          this.dateFormGroup.get("title").setValue(this.data.userOccupationPlanCalendar.title);
          if (this.data.userOccupationPlanCalendar.date) {
            this.dateFormGroup.get("date").setValue(new Date((this.data.userOccupationPlanCalendar.date)));
          }
          this.dateFormGroup.get("id").setValue(this.data.userOccupationPlanCalendar.id);

        } else {
          this.wasOpenInPlan = true;

          if (this.data.userOccupationPlanCalendar.planId) {
            this.plan = this.userOccupationPlans.find(x => x.id == this.data.userOccupationPlanCalendar.planId);
          }
          this.dateFormGroup.get("title").setValue(this.data.userOccupationPlanCalendar.title);
          this.dateFormGroup.get("date").setValue(new Date(this.data.userOccupationPlanCalendar.date));
          this.dateFormGroup.get("id").setValue(this.data.userOccupationPlanCalendar.id);

        }

      } else if (this.data.planId) {
        //opened from within a plan
        this.wasOpenInPlan = true;

        this.plan = this.userOccupationPlans.find(x => x.id == this.data.planId);
      };

      this.isLoading = false;
    });

  }

  async getUserOccupationPlans() {

    try {

      let userOccupationPlans: any = await this._userOccupationPlanService.getAll();

      return Promise.all(userOccupationPlans.map(async (userOccupationPlan: any) => {

        let occuTitle = await this._occufindRestFactory
          .getOccupationTitleDescriptionNF(userOccupationPlan.socId);

        return await { ...userOccupationPlan, occuTitle };

      }));


    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getAllUserOccupationPlanCalendarTasks() {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getTasks();

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  onPlanChange(value) {

  }

  saveDateFormGroup() {

    this.isSaving = true;

    this._userService.setCompletion(UserService.ADDED_TASK, 'true');

    //was a plan selected?
    let planId = (this.wasOpenInPlan) ? this.data.planId : (this.dateFormGroup.value.userOccupationPlan == '') ? null : this.dateFormGroup.value.userOccupationPlan;

    // CEPR81
    //if a value exists in the text field add it as a task
    if (this.dateFormGroup.value.addTask) {
      this.tasks.push(
        this._formBuilder.group({
          id: null,
          planId: null,
          calendarId: null,
          taskName: this.dateFormGroup.value.addTask,
          completed: false
        })
      );
    }


    // planId: (this.wasOpenInPlan) ? this.data.planId :
    // (this.taskFormGroup.value.userOccupationPlan == '') ? null : this.taskFormGroup.value.userOccupationPlan
    //if a plan was selected and new tasks exist; update each task to have the selected plan id
    if (planId && this.dateFormGroup.value.tasks) {
      this.dateFormGroup.value.tasks.forEach(task => {
        task.planId = planId;
      });
    }




    let formData: UserOccupationPlanCalendar = {
      id: null,
      planId: planId,
      title: this.dateFormGroup.value.title,
      date: this.dateFormGroup.value.date,
      isGraduationDate: false,
      tasks: this.dateFormGroup.value.tasks
    }

    if (this.data.userOccupationPlanCalendar) {

      //update date

      formData.id = this.data.userOccupationPlanCalendar.id;  //set calendar id for record to update

      if (this.data.userOccupationPlanCalendar.planId) {

        //updating an existing record that was assigned to a plan
        this._planCalendarService.updatePlanCalendar(formData, this.data.userOccupationPlanCalendar.planId, this.data.userOccupationPlanCalendar.id).then((rowsAffected: number) => {

          //create new task records if any exist in form
          if (this.dateFormGroup.value.tasks) {

            //save tasks to the calendar date
            const tasksUpdated = this.dateFormGroup.value.tasks.map((task) => {
              task.calendarId = formData.id;
              task.planId = formData.planId;
              return this.saveTask(task);
            });

            // wait until all promises are finished
            Promise.all(tasksUpdated).then((values: any) => {

              //if existing tasks; set all incomplete tasks in the ui to the correct plan selection
              if (this.data.userOccupationPlanCalendar.tasks) {
                let currentNotCompletedTasks = this.data.userOccupationPlanCalendar.tasks.filter(x => !x.completed);
                currentNotCompletedTasks.forEach(task => {
                  task.planId = formData.planId;
                });
                //combine new tasks with existing not completed tasks to update in the ui with the plan id if any
                formData.tasks = [...values, ...currentNotCompletedTasks];
              }else{
                formData.tasks = values;
              }

              this.isSaving = false;
              this.dialogRef.close(formData);

            });

          } else {

            //is selected plan different than previous?
            //if so update ui tasks that are not completed
            if (this.data.userOccupationPlanCalendar.planId != formData.planId) {
              let currentNotCompletedTasks = this.data.userOccupationPlanCalendar.tasks.filter(x => !x.completed);
              currentNotCompletedTasks.forEach(task => {
                task.planId = formData.planId;
              });
            }
            this.isSaving = false;
            this.dialogRef.close(formData);
          }

        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });
      } else {

        //updating an existing record that was not previously assigned to a plan

        this._planCalendarService.updateCalendar(formData, this.data.userOccupationPlanCalendar.id).then((rowsAffected: number) => {

          //create new task records if any exist in form
          if (this.dateFormGroup.value.tasks) {

            const tasksUpdated = this.dateFormGroup.value.tasks.map((task) => {
              task.calendarId = formData.id;
              task.planId = formData.planId;
              return this.saveTask(task);
            });

            // wait until all promises are finished
            Promise.all(tasksUpdated).then((values: any) => {

              //if existing tasks; set all incomplete tasks in the ui to the correct plan selection
              if (this.data.userOccupationPlanCalendar.tasks) {
                let currentNotCompletedTasks = this.data.userOccupationPlanCalendar.tasks.filter(x => !x.completed);
                currentNotCompletedTasks.forEach(task => {
                  task.planId = formData.planId;
                });
                //combine new tasks with existing not completed tasks to update in the ui with the plan id if any
                formData.tasks = [...values, ...currentNotCompletedTasks];
              }else{
                formData.tasks = values;
              }

              this.isSaving = false;
              this.dialogRef.close(formData);

            });

          } else {

            //is selected plan different than previous?
            //if so update ui tasks that are not completed
            if (this.data.userOccupationPlanCalendar.planId != formData.planId) {
              let currentNotCompletedTasks = this.data.userOccupationPlanCalendar.tasks.filter(x => !x.completed);
              currentNotCompletedTasks.forEach(task => {
                task.planId = formData.planId;
              });
            }

            this.isSaving = false;
            this.dialogRef.close(formData);
          }
        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });

      }


    } else {

      //save calendar to a plan and all of its tasks if any
      if (formData.planId) {
        this._planCalendarService.savePlanCalendar(formData.planId, formData).then((newRecord: UserOccupationPlanCalendar) => {
          this.isSaving = false;
          this.dialogRef.close(newRecord);
        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });
      } else {
        //save calendar not to a plan and all of its tasks if any
        this._planCalendarService.saveCalendar(formData).then((newRecord: UserOccupationPlanCalendar) => {
          this.isSaving = false;
          this.dialogRef.close(newRecord);
        }).catch((error: any) => {
          this.isSaving = false;
          console.error("ERROR", error);
        });
      }
    }

  }

  addTask() {

    this.tasks.push(
      this._formBuilder.group({
        id: null,
        planId: null,
        calendarId: null,
        taskName: this.dateFormGroup.value.addTask,
        completed: false
      })
    );

    //reset form control for next task entry
    this.dateFormGroup.get("addTask").setValue("");
  }

  removeTask(index) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Task, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          this.tasks.removeAt(index);
        }
      }
    });
  }

  close() {
    this.dialogRef.close();
  }

  saveTask(task) {
    //save task to a plan
    if (task.planId) {
      return this._planCalendarService.savePlanTask(task.planId, task);
    } else {
      //save task not to a plan
      return this._planCalendarService.saveTask(task);
    }
  }


}
