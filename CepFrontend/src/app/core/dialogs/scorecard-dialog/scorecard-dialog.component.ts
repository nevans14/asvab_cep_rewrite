import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { startTimeRange } from '@angular/core/src/profile/wtf_impl';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import html2canvas from "html2canvas";

declare var pdfMake: any;

@Component({
  selector: 'app-scorecard-dialog',
  templateUrl: './scorecard-dialog.component.html',
  styleUrls: ['./scorecard-dialog.component.scss']
})
export class ScorecardDialogComponent implements OnInit, AfterViewInit {

  title: any;
  message: any;
  disabledSeconds: number;
  isDownloadMode: boolean;
  isDisabled: boolean;

  constructor(public _dialogRef: MatDialogRef<ScorecardDialogComponent>,
    private _elementRef: ElementRef,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    this.disabledSeconds = 8;

    this.title = this.data['title'];
    this.message = this.data['message'];
    this.isDownloadMode = this.data['isDownloadMode'];

    //start timer to automatically close dialog
    if (!this.isDownloadMode) {
      this.startTimer();
    }

  }

  ngAfterViewInit() {

    if (this.isDownloadMode) {
      this.triggerDownload();
    } else {
      this.printPdf();
    }

  }

  startTimer() {
    let self = this;
    self.isDisabled = true;
    setTimeout(function () {
      let st = setInterval(function () {
        self.disabledSeconds--;
        if (self.disabledSeconds <= 0) {
          clearInterval(st);
          self.isDisabled = false;
          //close out of dialog
          self._dialogRef.close();
        }
      }, 1000)
    }, 1000);
  }

  getCanvas() {
    let self = this;
    let something = this._elementRef.nativeElement.querySelector('#asr');
    return new Promise((resolve, reject) => {
      html2canvas(something, {
        imageTimeout: 2000,
        removeContainer: true,
        backgroundColor: '#fff'
      }).then(function (canvas) {
        var img = canvas.toDataURL("image/jpeg");
        resolve(img);
      }, function (error) {
        reject(error);
      });
    });
  }

  triggerDownload() {
    let t = this.getCanvas();

    t.then(function (s) {
      let docDefinition = {
        pageOrientation: 'landscape',
        pageSize: 'A2',
        content: [{
          image: s,
          alignment: 'center',
          width: 1300,
        }]
      };

      pdfMake.createPdf(docDefinition).download('ASR.pdf');
    }, function (error) {
      console.error(error);
      alert(error);
    });
  }

  printPdf() {

    let t = this.getCanvas();

    // IE support
    if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
      this.printIE(t);
      return false;
    }

    t.then(function (s) {

      let docDefinition = {
        pageOrientation: 'landscape',
        pageSize: 'A2',
        content: [{
          image: s,
          alignment: 'center',
          width: 1300,
        }]
      };

      pdfMake.createPdf(docDefinition).print();
    }, function (error) {
      console.error(error);
      alert(error);
    });

  };

  /**
   * IE support for printing element.
   */
  printIE(canvas) {

    canvas.then(function (img) {
      let popup = window.open();
      popup.document.write('<img src=' + img + '>');
      popup.document.close();
      popup.focus();
      popup.print();
      popup.close();

    }, function (error) {
      alert('Printing failed.');
    });

  }


}
