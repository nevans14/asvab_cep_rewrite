import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorecardDialogComponent } from './scorecard-dialog.component';

describe('ScorecardDialogComponent', () => {
  let component: ScorecardDialogComponent;
  let fixture: ComponentFixture<ScorecardDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScorecardDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorecardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
