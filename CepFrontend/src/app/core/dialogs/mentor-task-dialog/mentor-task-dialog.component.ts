import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-mentor-task-dialog',
  templateUrl: './mentor-task-dialog.component.html',
  styleUrls: ['./mentor-task-dialog.component.scss']
})
export class MentorTaskDialogComponent implements OnInit {


  public isSaving: boolean;
  public isLoading: boolean;
  public taskFormGroup: FormGroup;
  public mentorLinkIds: string[];

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<MentorTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService
  ) {

    this.isSaving = false;
    this.isLoading = true;

    this.taskFormGroup = this._formBuilder.group({
      id: [''],
      taskName: ['', [Validators.required, Validators.maxLength(500)]]
    });
    this.mentorLinkIds = [];

  }

  ngOnInit() {
    this.isLoading = false;

    this.mentorLinkIds = (this.data.mentorLinkIds) ? this.data.mentorLinkIds : null;

  }

  async saveTaskFormGroup() {

    this.isSaving = true;

    if (this.mentorLinkIds && this.mentorLinkIds.length > 0) {

      await Promise.all(this.mentorLinkIds.map((mentorLinkId: string) => {

        let formData: DirectMessage = {
          id: null, //handled on server
          messageTypeId: messageType.TASK_MESSAGE,
          message: this.taskFormGroup.value.taskName,
          dateCreated: null, //handled on server
          fromUser: null, //handled on server
          toUser: null, //handled on server
          mentorLinkId: mentorLinkId,
          hasBeenRead: false,
          calendarDate: null,
          dateUpdated: null  //handled on server
        }

        this._userService.sendDirectMessage(formData).then((result: DirectMessage) => {
          return Promise.resolve(result);
        }).catch((error: any) => {
          return Promise.reject("error");
        })

      })).then((done: any) => {
        this.isSaving = false;
        this.dialogRef.close();
      }).catch((error: any) => {
        console.error("ERROR", error);
      });

    }

  }

  close() {
    this.dialogRef.close();
  }

}
