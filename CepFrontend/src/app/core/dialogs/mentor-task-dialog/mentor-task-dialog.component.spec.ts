import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorTaskDialogComponent } from './mentor-task-dialog.component';

describe('MentorTaskDialogComponent', () => {
  let component: MentorTaskDialogComponent;
  let fixture: ComponentFixture<MentorTaskDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorTaskDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorTaskDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
