import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsvabZoomDialogComponent } from './asvab-zoom-dialog.component';

describe('AsvabZoomDialogComponent', () => {
  let component: AsvabZoomDialogComponent;
  let fixture: ComponentFixture<AsvabZoomDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsvabZoomDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsvabZoomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
