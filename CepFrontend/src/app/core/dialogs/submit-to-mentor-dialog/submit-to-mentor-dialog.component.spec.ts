import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitToMentorDialogComponent } from './submit-to-mentor-dialog.component';

describe('SubmitToMentorDialogComponent', () => {
  let component: SubmitToMentorDialogComponent;
  let fixture: ComponentFixture<SubmitToMentorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitToMentorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitToMentorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
