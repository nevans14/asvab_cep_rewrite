import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClassroomActivitySubmissionComment } from 'app/core/models/classroomActivity';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { UserMentor } from 'app/core/models/userMentor.model';
import { UserOccupationPlan, UserOccupationPlanComment } from 'app/core/models/userOccupationPlan.model';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MentorService } from 'app/services/mentor.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';

function validateEmails(emails: string) {
  return (emails.split(',')
    .map(email => Validators.email(<AbstractControl>{ value: email }))
    .find(_ => _ !== null) === undefined);
}

function emailsValidator(control: AbstractControl): ValidationErrors | null {
  if (control.value === '' || !control.value || validateEmails(control.value)) {
    return null
  }
  return { emails: true };
}
@Component({
  selector: 'app-submit-to-mentor-dialog',
  templateUrl: './submit-to-mentor-dialog.component.html',
  styleUrls: ['./submit-to-mentor-dialog.component.scss']
})
export class SubmitToMentorDialogComponent implements OnInit {

  public title: string;
  public mentors: any[];
  public mentorInviteForm: FormGroup;
  public mentorCommentForm: FormGroup;
  public sendToMentorsForm: FormGroup;
  public errorText: string;
  public successText: string;
  public isLoading: boolean;
  public isSaving: boolean;
  public isPortfolio: boolean;
  public availableMentors: FormArray;
  public subtext: string;
  public userOccupationPlan: any;
  public mentorInviteFormSubmitted: boolean;
  public comments: any[];

  constructor(public _dialogRef: MatDialogRef<SubmitToMentorDialogComponent>,
    private _elementRef: ElementRef,
    @Inject(MAT_DIALOG_DATA) public data,
    private _formBuilder: FormBuilder,
    private _mentorService: MentorService,
    private _portfolioService: PortfolioService,
    private _userOccupationService: UserOccupationPlanService,
    private _user: UserService,
  ) {

    this.title = "";
    this.mentors = [];
    this.mentorInviteForm = this._formBuilder.group({
      inviteEmails: ['',
        Validators.compose([Validators.required, emailsValidator])
      ]
    });
    this.mentorCommentForm = this._formBuilder.group({
      comment: ['', Validators.required],
      commentMentors: ['', Validators.required]
    });
    this.sendToMentorsForm = this._formBuilder.group({
      availableMentors: this._formBuilder.array([]),
    });
    this.errorText = "";
    this.successText = "";
    this.isLoading = true;
    this.isPortfolio = false;
    this.availableMentors = this.sendToMentorsForm.get('availableMentors') as FormArray;
    this.isSaving = false;
    this.subtext = "plan";
    this.mentorInviteFormSubmitted = false;
    this.comments = [];
  }

  ngOnInit() {

    //set title
    this.title = this.data.title;
    this.userOccupationPlan = (this.data.userOccupationPlan) ? this.data.userOccupationPlan : null;
    
    if ((this.data.title).toString().toUpperCase() == "PORTFOLIO") { this.isPortfolio = true; this.subtext = "resume"; }

    //get comment history for the portfolio/class room activity
    let comments;
    if(this.isPortfolio){
      comments = this._portfolioService.getPortfolioComments();
    }else{
      comments = this._userOccupationService.getComments(this.userOccupationPlan.id);
    }

    let mentors = this._mentorService.getAll();

    Promise.all([comments, mentors]).then((done:any) => {

      this.comments = done[0];
      this.mentors = done[1];
      this.mentors.forEach(t => {
        var availableMentor = this._formBuilder.group({
          id: t.id,
          mentorEmail: t.mentorEmail.trim(),
          isSelected: false,
          show: true
        })
        this.availableMentors.push(availableMentor);
      });

      this.isLoading = false;

    })

  }

  close() {
    this._dialogRef.close();
  }

  submitMentorInvite() {

    this.mentorInviteFormSubmitted = true;

    let formData: any = {
      mentorEmail: this.mentorInviteForm.value.inviteEmails
    }

    this._mentorService.save(formData).then((response: any) => {
      this.mentorInviteFormSubmitted = false;
      this.close();
      this.mentorInviteForm.reset();
    }).catch((error: any) => {
      this.mentorInviteFormSubmitted = false;
      this.errorText = error;
      setTimeout(() => { this.errorText = ""; }, 5000);
      console.error("ERROR", error);
    })
  }

  submitMentorComment() {

    let selectedUsers: string[] = [];
    if (this.mentorCommentForm.value.commentMentors.length > 0) {
      this.mentorCommentForm.value.commentMentors.forEach(mentor => {
        selectedUsers.push(mentor);
      });
    }

    if (this.isPortfolio) {
      let formData: PortfolioComment = {
        from: null,
        id: null,
        message: this.mentorCommentForm.value.comment,
        dateCreated: null,
        to: null,
        commentTypeId: null,
        selectedUsers: selectedUsers
      }

      this._portfolioService.sendComment(formData).then((response: PortfolioComment) => {
        this.comments.push(response);
        this.mentorCommentForm.get("comment").setValue("");
      }).catch((error: any) => {
        this.errorText = error;
        setTimeout(() => { this.errorText = ""; }, 5000);
        console.error("ERROR", error);
      })
    } else {
      let formData: UserOccupationPlanComment = {
        from: null,
        id: null,
        message: this.mentorCommentForm.value.comment,
        dateCreated: null,
        to: null,
        selectedUsers: selectedUsers,
        planId: this.userOccupationPlan.id,
        commentTypeId: 2
      }

      this._userOccupationService.saveComment(this.userOccupationPlan.id, formData).then((response: UserOccupationPlanComment) => {
        this.comments.push(response);
        this.mentorCommentForm.get("comment").setValue("");
      }).catch((error: any) => {
        this.errorText = error;
        setTimeout(() => { this.errorText = ""; }, 5000);
        console.error("ERROR", error);
      })
    }


  }

  submitSendToMentorsForm() {

    this.isSaving = true;

    //format data to send to server
    let selectedMentors: UserMentor[] = this.getSelectedAvailableMentors();
    let toMentors: any[] = [];
    if (selectedMentors.length > 0) {
      selectedMentors.forEach(mentor => {
        toMentors.push({
          mentorId: mentor.id,
          didReview: false,
          dateReviewed: null
        });

      });
    }

    if (this.isPortfolio) {
      this._portfolioService.submit(toMentors).then((response: any) => {

        this.isSaving = false;
        this.successText = "Portfolio successfully submitted.";
        setTimeout(() => { this.successText = ""; }, 5000);
        if (Number.isInteger(response) && response > 0) {
          //remove mentor from available mentors
          selectedMentors.forEach((selectedMentor: UserMentor) => {
            this.removeMentor(selectedMentor.mentorEmail);
          });

        } else {
          this.errorText = response;
          setTimeout(() => { this.errorText = ""; }, 5000);
          console.error("ERROR", response);
        }
      }).catch((error: any) => {
        this.isSaving = false;
        this.errorText = error;
        setTimeout(() => { this.errorText = ""; }, 5000);
        console.error("ERROR", error);
      })
    } else {

      let formData: any = this.userOccupationPlan;

      formData.statusId = this.userOccupationPlan.statusId;
      formData.socId = this.userOccupationPlan.socId;
      formData.pathway = this.userOccupationPlan.pathway;
      formData.additionalNotes = this.userOccupationPlan.additionalNotes;
      formData.useNationalAverage = this.userOccupationPlan.useNationalAverage;
      formData.mentors = toMentors;
      formData.logs = this.userOccupationPlan.logs;
      formData.calendarDates = this.userOccupationPlan.calendarDates;

      this._user.setCompletion(UserService.SUBMIT_PLAN_TO_MENTOR, 'true');

      this._userOccupationService.submit(this.userOccupationPlan.id, formData).then((response: any) => {
        this.isSaving = false;
        this.successText = "Plan successfully submitted.";
        setTimeout(() => { this.successText = ""; }, 5000);
        if (Number.isInteger(response) && response > 0) {
          //remove mentor from available mentors
          selectedMentors.forEach((selectedMentor: UserMentor) => {
            this.removeMentor(selectedMentor.mentorEmail);
          });

        } else {
          this.errorText = response;
          setTimeout(() => { this.errorText = ""; }, 5000);
          console.error("ERROR", response);
        }
      }).catch((error: any) => {
        this.isSaving = false;
        this.errorText = error;
        setTimeout(() => { this.errorText = ""; }, 5000);
        console.error("ERROR", error);
      })
    }

  }

  getSelectedAvailableMentors(): UserMentor[] {

    let returnValue: UserMentor[] = [];

    const availableMentors = <FormArray>this.sendToMentorsForm.controls['availableMentors'];
    for (let i = availableMentors.length - 1; i >= 0; i--) {
      if (availableMentors.value[i].isSelected && availableMentors.value[i].show) {
        let userMentor: UserMentor = {
          id: availableMentors.value[i].id,
          mentorEmail: availableMentors.value[i].mentorEmail,
        };
        returnValue.push(userMentor);
      }
    }

    return returnValue;
  }

  getMentorDisplayName(mentorId): string {
    let returnString = "";
    let mentor = this.mentors.find(x => x.id == mentorId);

    if (mentor) {
      returnString = mentor.mentorEmail;
    }

    return returnString;
  }

  removeMentor(mentorEmail){

    const availableMentors = <FormArray>this.sendToMentorsForm.controls['availableMentors'];
    for (let i = availableMentors.length - 1; i >= 0; i--) {
      if (availableMentors.value[i].mentorEmail == mentorEmail) {
        availableMentors.value[i].show = false;
      }
    }

  }
}
