// *** component moved to register-dialog ****
// *******************************************


// import { Component, OnInit } from '@angular/core';
// import { ConfigService } from 'app/services/config.service';
// import { HttpHelperService } from 'app/services/http-helper.service';
// import { AuthenticationService } from 'app/services/authentication.service';
// import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
// import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
// declare var bcrypt: any;

// @Component( {
//     selector: 'app-account-registration',
//     templateUrl: './account-registration.component.html',
//     styleUrls: ['./account-registration.component.scss']
// } )
// export class AccountRegistrationComponent implements OnInit {

//     accessCode: any;
//     //baseUrl = this._config.getBaseUrl();
//     //baseUrl = 'localhost:8080';
//     BASE_REGISTER_URL = 'applicationAccess/';
//     serviceEndpoint = {
//         'Register': 'register',
//         'TeacherRegister': 'teacherRegistration'
//     };
//     hash: any;
//     constructor( private _config: ConfigService,
//         private _httpHelper: HttpHelperService,
//         public dialogRef: MatDialogRef<AccountRegistrationComponent>,
//         private _dialog: MatDialog,
//         private _authorizationService: AuthenticationService
//         ) { }

//     ngOnInit() {
//     }

//     close() {
//         this.dialogRef.close();
//     }

//     checkForPromoCode() {
//         this.isUsingPromoCode = false;
//         this.isStudentCode = false;
//         let accessCode = this.registerAccessCode;
//         if ( accessCode == null ) {
//             return;
//         }
//         // set the access to all upper case
//         accessCode = accessCode.toUpperCase();
//         if ( accessCode === "CEP123" ) {
//             this.isUsingPromoCode = true;
//         } else if ( accessCode.length === 10 && isNaN( accessCode[accessCode.length - 1] ) ) {
//             this.isStudentCode = true;
//         }
//     }

//     mergeAccounts = false;
//     isUsingPromoCode = ( this.accessCode ) ? true : false;
//     registerAccessCode = ( this.isUsingPromoCode ) ? this.accessCode : undefined;
//     registerEmail = undefined;
//     registerStudentId = undefined;
//     registerSubject = undefined;
//     registerPassword1 = undefined;
//     registerPassword2 = undefined;
//     optedInCommunications = true;
//     isStudentCode = false;
//     disableForm = false;
//     registrationLabel = '';

//     RegisterStudent() {
//         // disable the form
//         this.disableForm = true;
//         // Register Modal
//         var data = {
//             email: this.registerEmail,
//             accessCode: this.registerAccessCode,
//             password: this.registerPassword2,
//             resetKey: "",
//             studentId: this.registerStudentId,
//             subject: this.registerSubject,
//             optedInCommunications: this.optedInCommunications
//         };

//         var pass1 = this.registerPassword1;
//         var pass2 = this.registerPassword2;


//         if ( pass1.length <= 7 ) {
//             this.registrationLabel = "Password too short! Please have a password that is 8 characters or more.";
//             this.disableForm = false;
//             return;
//         }

//         if ( pass1 != pass2 ) {
//             this.registrationLabel = "Passwords do not match!";
//             this.disableForm = false;
//             return;
//         }
//         console.log( this.BASE_REGISTER_URL );
//         let url = this.BASE_REGISTER_URL + ( this.isUsingPromoCode ?
//             this.serviceEndpoint.TeacherRegister :
//             this.serviceEndpoint.Register );
//         if ( this.isUsingPromoCode ) {
//             let hash = this.crypt( pass1, function( res ) {
//                 data.password = res;
//                 this.post( url, data );
//             } );
//         } else {
//             this.post( url, data );
//         }
//     }

//     /**
//      * Posts to service
//      */
//     post = function( url, data ) {

//         return this._httpHelper.httpHelper( 'POST', url, null, data )
//             .then(( res ) => {
//                 this.registrationLoginStatus = res;
//                 let userData = res;
//                 if ( this.registrationLoginStatus.statusNumber == 0 ) { // success
//                     // Clear all the data
//                     this.registerAccessCode = '';
//                     this.registerEmail = '';
//                     this.registerPassword1 = '';
//                     this.registerPassword2 = '';
//                     this.registerStudentId = '';
//                     this.registerSubject = '';

//                     //TODO: Not sure if we need this. Leaving this here for now when SSO is worked on and if these other user data is referenced.
//                     //  Now log in the user ....
//                     this._authorizationService.SetCredentials(this.registerEmail , 'null', userData.user_id, userData.role,
//                             userData.accessCode,
//                             userData.schoolCode,
//                             userData.schoolName,
//                             userData.city,
//                             userData.state,
//                             userData.zipCode,
//                             userData.mepsId,
//                             userData.mepsName);
//                     // Close Dialog
//                     /*$uibModalInstance.dismiss('cancel');
//                     // SSO: login to the CTM website.
//                     $window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';*/

//                     this.close();
//                 } else { // Error
//                     // if the user already exists but not using a promo account, merge accounts
//                     if ( this.registrationLoginStatus.statusNumber == 1003 && !this.isUsingPromoCode ) {
//                         this.email = this.registerEmail;
//                         //TODO: dependent on merge account function
//                         this.showMergeModal();
//                     } else {
//                         this.registrationLabel = this.registrationLoginStatus.status;
//                         this.disableForm = false;
//                         setTimeout(() => { this.registrationLabel = ''; }, 5000 );
//                     }
//                 }
//             } );
//     };
//     /**
//      * Show Merge Account Modal
//      */

//     //TODO:
//     /*showMergeModal = function() {
//         var modalInstance = mergeAccountModalService.show({
//             email : this.registerEmail,
//             accessCode : this.registerAccessCode
//         }, {});

//         modalInstance.then(function (response) {
//             if(response.statusNumber == 0) {
//                 this.modalOptions.close();
//             }
//         });
//     } */


//     /**
//      * Begin hashing passwords
//      */
//     //id;
//     //bcrypt = new bCrypt();

//     //begin = '';
//     result( hashRet ) {
//         console.log( hashRet );
//         this.hash = hashRet;
//     }

//     ckpassword = function( password, hash, callbackFunction ) {
//         try {

//             bcrypt.checkpw( password, hash, callbackFunction, null );
//         } catch ( err ) {
//             alert( err );
//             return;
//         }
//     };

//     crypt = function( password, callbackFunction ) {
//         let salt = '';
//         let rounds = 10;

//         // Auto generate the salt
//         try {
//             salt = bcrypt.gensalt( rounds );
//         } catch ( err ) {
//             alert( err );
//             return;
//         }

//         try {
//             // bcrypt.hashpw( password , salt, result , null );
//             bcrypt.hashpw( password, salt, callbackFunction, null );
//         } catch ( err ) {
//             alert( err );
//             return;
//         }
//     };

//     onBringAsvabToYourSchool() {
//         const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent , {
//             hasBackdrop: true,
//             panelClass: 'new-modal',
//             height: '95%',
//             autoFocus: true,
//             disableClose: true
//         });
//         this.close();
//     }

// }
