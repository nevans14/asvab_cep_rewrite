import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-mentor-group-dialog',
  templateUrl: './mentor-group-dialog.component.html',
  styleUrls: ['./mentor-group-dialog.component.scss']
})
export class MentorGroupDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public mentorGroupFormGroup: FormGroup;
  public mentorLinkIds: string[];
  public studentGroups: any[];

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<MentorGroupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService,
    private _mentorService: MentorService
  ) {

    this.isSaving = false;
    this.isLoading = true;

    this.mentorGroupFormGroup = this._formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(255)]]
    });
    this.mentorLinkIds = [];
    this.studentGroups = [];

  }

  ngOnInit() {
    this.isLoading = false;

    this.mentorLinkIds = (this.data.mentorLinkIds) ? this.data.mentorLinkIds : null;
    this.studentGroups = (this.data.groups) ? this.data.groups : null;

  }

  async saveForm() {

    this.isSaving = true;

    if (this.mentorLinkIds && this.mentorLinkIds.length > 0) {



      let existingGrp: any = null;

      if (this.studentGroups && this.studentGroups.length > 0) {
        existingGrp = this.studentGroups.find(x => x.groupName == this.mentorGroupFormGroup.value.name);
      }

      if (existingGrp) {

        let formData: any = this.mentorLinkIds

        this._mentorService.saveStudentsToExistingGroup(formData, existingGrp.groupId).then((result: any) => {
          this.isSaving = false;
          //updates the dashboard with new student data
          this._mentorService.sendUpdate(true);
          this.close();
        }).catch((error: any) => {
          console.error("error");
        })
      } else {

        let formData: any = {
          groupName: this.mentorGroupFormGroup.value.name,
          students: this.mentorLinkIds
        }

        this._mentorService.saveStudentsToNewGroup(formData).then((result: any) => {
          this.isSaving = false;
          //updates the dashboard with new student data
          this._mentorService.sendUpdate(true);
          this.close();
        }).catch((error: any) => {
          console.error("error");
        })
      }


    }

  }

  close() {
    this.dialogRef.close();
  }


}
