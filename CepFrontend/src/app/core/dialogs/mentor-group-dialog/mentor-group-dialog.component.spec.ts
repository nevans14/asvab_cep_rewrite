import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorGroupDialogComponent } from './mentor-group-dialog.component';

describe('MentorGroupDialogComponent', () => {
  let component: MentorGroupDialogComponent;
  let fixture: ComponentFixture<MentorGroupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorGroupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorGroupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
