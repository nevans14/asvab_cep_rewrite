import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { PortfolioService } from 'app/services/portfolio.service';

@Component({
  selector: 'app-retrieve-asvab-scores-dialog',
  templateUrl: './retrieve-asvab-scores-dialog.component.html',
  styleUrls: ['./retrieve-asvab-scores-dialog.component.scss']
})
export class RetrieveAsvabScoresDialogComponent implements OnInit {

  retrieve: any;
  enter: any;

  accessCodeForm = new FormGroup({
    accessCode: new FormControl('', Validators.required)
  });

  manualForm = new FormGroup({
    verbalScore: new FormControl('', Validators.required),
    mathScore: new FormControl('', Validators.required),
    scienceScore: new FormControl('', Validators.required),
    grade: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required),
    testLocation: new FormControl(''),
    testMonth: new FormControl(''),
    testDay: new FormControl(''),
    testYear: new FormControl('')
  });

  constructor(public dialogRef: MatDialogRef<RetrieveAsvabScoresDialogComponent>,
    private _findYourInterestService: FindYourInterestService,
    private _router: Router,
    private _portfolioService: PortfolioService,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {

    //set mode
    this.enter = this.data.enter;
    this.retrieve = this.data.retrieve;
  }

  close() {
    this.dialogRef.close('cancel');
  };


  submitAccessCodeForm() {

    this._findYourInterestService.getTestScoreByAccessCode(this.accessCodeForm.value.accessCode).then((success) => {
      //close modal and refresh
      this.dialogRef.close(success);
    }, function (error) {
      console.debug('error', error);
    });

  }

  submitManualForm() {

    //map values
    let formData = {
      verbalScore: this.manualForm.value.verbalScore,
      mathScore: this.manualForm.value.mathScore,
      scienceScore: this.manualForm.value.scienceScore,
      grade: this.manualForm.value.grade,
      gender: this.manualForm.value.gender,
      testLocation: this.manualForm.value.testLocation,
      testDay: this.manualForm.value.testDay,
      testYear: this.manualForm.value.testYear,
      testMonth: this.manualForm.value.testMonth
    }

    this._portfolioService.insertAsvabScore(formData).then((success) => {
      //close modal and refresh
      this.dialogRef.close(success);
    }, function (error) {
      alert("Failed to save ASVAB score. Try again. If error persist, please contact site administrator.");
      console.debug('error', error);
    });

  }

  displayOptions = function (viewFor) {
    switch (viewFor) {
      case 'enter':
        this.enter = true;
        this.retrieve = false;
        break;
      case 'retrieve':
        this.retrieve = true;
        this.enter = false;
        break;
      case 'initial':
        this.enter = false;
        this.retrieve = false;
        break;
    }
  }


}
