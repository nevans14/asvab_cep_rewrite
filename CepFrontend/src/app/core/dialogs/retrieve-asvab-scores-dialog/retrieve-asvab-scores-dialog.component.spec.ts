import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetrieveAsvabScoresDialogComponent } from './retrieve-asvab-scores-dialog.component';

describe('RetrieveAsvabScoresDialogComponent', () => {
  let component: RetrieveAsvabScoresDialogComponent;
  let fixture: ComponentFixture<RetrieveAsvabScoresDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetrieveAsvabScoresDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetrieveAsvabScoresDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
