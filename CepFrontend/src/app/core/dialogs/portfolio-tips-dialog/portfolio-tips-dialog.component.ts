import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component( {
    selector: 'app-portfolio-tips-dialog',
    templateUrl: './portfolio-tips-dialog.component.html',
    styleUrls: ['./portfolio-tips-dialog.component.scss']
} )
export class PortfolioTipsDialogComponent implements OnInit {

    constructor( public _dialogRef: MatDialogRef<PortfolioTipsDialogComponent> ) { }

    ngOnInit() {
    }

}
