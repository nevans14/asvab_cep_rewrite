import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioTipsDialogComponent } from './portfolio-tips-dialog.component';

describe('PortfolioTipsDialogComponent', () => {
  let component: PortfolioTipsDialogComponent;
  let fixture: ComponentFixture<PortfolioTipsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioTipsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioTipsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
