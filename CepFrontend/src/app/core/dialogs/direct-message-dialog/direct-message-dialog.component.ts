import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { MentorService } from 'app/services/mentor.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-direct-message-dialog',
  templateUrl: './direct-message-dialog.component.html',
  styleUrls: ['./direct-message-dialog.component.scss']
})
export class DirectMessageDialogComponent implements OnInit {

  public recipients: any[];
  public messageForm: FormGroup;
  public errorText: string;
  public isLoading: boolean;
  public isSaving: boolean;
  public isPortfolio: boolean;
  public isStudent: boolean;
  public isReplyMessage: boolean;
  public directMessageMentorLinkId: any;
  public directMessageUsername: any;
  public mentorLinkIds: string[];
  
  successText;

  constructor(public _dialogRef: MatDialogRef<DirectMessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _formBuilder: FormBuilder,
    private _mentorService: MentorService,
    private _userService: UserService) {

    this.recipients = [];

    this.messageForm = this._formBuilder.group({
      message: ['', Validators.required],
      selectedRecipients: ['', Validators.required]
    });

    this.errorText = "";
    this.isLoading = true;
    this.isPortfolio = false;
    this.isSaving = false;
    this.mentorLinkIds = [];
  }

  ngOnInit() {

    this.successText = '';

    this.isStudent = this.data.isStudent;
    this.isReplyMessage = (this.data.isReplyMessage) ? true : false;
    this.directMessageMentorLinkId = (this.data.directMessageMentorLinkId) ? this.data.directMessageMentorLinkId : null;
    this.directMessageUsername = (this.data.directMessageUsername) ? this.data.directMessageUsername : null;
    this.mentorLinkIds = (this.data.mentorLinkIds) ? this.data.mentorLinkIds : null;
    //set available recipients of message
    this.setRecipients(this.isStudent);

  }

  /**
   * if current logged in user is a student; retrieve there mentors to send a message to;
   * otherwise retrieve all students for the mentor
   * @param isStudent 
   */
  setRecipients(isStudent: boolean) {

    if (isStudent) {
      this._mentorService.getAll().then((results: any) => {
        let availableRecipients = results;

        if (availableRecipients && availableRecipients.length > 0) {
          availableRecipients.forEach(t => {

            if (t.didAccept) {
              this.recipients.push({
                id: t.id,
                username: t.mentorEmail
              });
            }
          });
        }

        //set the selected recipient
        if (this.directMessageMentorLinkId) {
          let linkId = [];
          linkId.push(this.directMessageMentorLinkId);
          this.messageForm.get("selectedRecipients").patchValue(linkId);
        }
        this.isLoading = false;

      })
    } else {
      this._mentorService.getStudents().then((results: any) => {
        let availableRecipients = results;

        if (availableRecipients && availableRecipients.length > 0) {
          availableRecipients.forEach(t => {

            this.recipients.push({
              id: t.mentorLinkId,
              username: t.emailAddress
            });
          });
        }

        //set the selected recipient
        if (this.directMessageMentorLinkId) {
          let linkId = [];
          linkId.push(this.directMessageMentorLinkId);
          this.messageForm.get("selectedRecipients").patchValue(linkId);
        }

        if (this.mentorLinkIds && this.mentorLinkIds.length > 0) {
          let linkId = [];
          this.mentorLinkIds.forEach((x: any) => {
            linkId.push(x);
          })
          this.messageForm.get("selectedRecipients").patchValue(linkId);
        }

        this.isLoading = false;
      })
    }

  }

  getDisplayName(id): string {
    let returnString = "";
    let recipient = this.recipients.find(x => x.id == id);

    if (recipient) {
      returnString = recipient.username;
    }
    return returnString;
  }

  close() {
    this._dialogRef.close();
  }

  submitMessageForm() {
    let selectedRecipients: string[] = [];
    if (this.messageForm.value.selectedRecipients.length > 0) {
      this.messageForm.value.selectedRecipients.forEach(selectedRecipient => {
        selectedRecipients.push(selectedRecipient);
      });
    }

    let formData: DirectMessage = {
      id: null, //handled on server
      messageTypeId: messageType.DIRECT_MESSAGE,
      message: this.messageForm.value.message,
      dateCreated: null, //handled on server
      fromUser: null, //handled on server
      toUser: null, //handled on server
      mentorLinkId: selectedRecipients[0], //TODO update for multiple?
      hasBeenRead: false,
      calendarDate: null, //handled on server
      dateUpdated: null, //handled on server
    }

    this._userService.sendDirectMessage(formData).then((result: DirectMessage) => {
      this.successText = "Message sent successfully!";
      setTimeout(() => { this.successText = ""; }, 5000);
      this.messageForm.get("message").reset();
    }).catch((error: any) => {
      this.errorText = error;
      setTimeout(() => { this.errorText = ""; }, 5000);
      console.error("ERROR", error);
    })
  }

}
