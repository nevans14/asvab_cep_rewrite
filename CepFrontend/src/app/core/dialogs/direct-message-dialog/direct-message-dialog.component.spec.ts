import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectMessageDialogComponent } from './direct-message-dialog.component';

describe('DirectMessageDialogComponent', () => {
  let component: DirectMessageDialogComponent;
  let fixture: ComponentFixture<DirectMessageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectMessageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectMessageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
