import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes, UrlSegment } from '@angular/router';
import { FyiInterestCodesService } from 'app/dashboard/resolves/fyi-interest-codes.service';
import { NumberTestTakenService } from 'app/dashboard/resolves/number-test-taken.service';
import { TiedLinkResolveService } from 'app/dashboard/resolves/tied-link-resolve.service';
import { AccessCodeDetailService } from 'app/resolves/access-code-detail.service';
import { AsvabScoreResolveService } from 'app/resolves/asvab-score-resolve.service';
import { CompareOccupationResolveService } from 'app/resolves/compare-occupation-resolve.service';
import { EmploymentDetailsResolveService } from 'app/resolves/employment-details-resolve.service';
import { SchoolDetailsResolveService } from 'app/resolves/school-details-resolve.service';
import { ScoreSummaryResolveService } from 'app/resolves/score-summary-resolve.service';
import { SiteSearchResultsComponent } from 'app/site-search/site-search-results/site-search-results.component';
import { CompareOccupationComponent } from './compare-occupation/compare-occupation.component';
import { EmploymentDetailsComponent } from './occufind/employment-details/employment-details.component';
import { SchoolDetailsComponent } from './occufind/school-details/school-details.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EducatorsComponent } from './educators/educators.component';
import { FyiInstructionComponent } from './find-your-interest/fyi-instruction/fyi-instruction.component';
import { FyiResultsComponent } from './find-your-interest/fyi-results/fyi-results.component';
import { FyiTestComponent } from './find-your-interest/fyi-test/fyi-test.component';
import { FyiTiedComponent } from './find-your-interest/fyi-tied/fyi-tied.component';
import { FyiScoreSharedComponent } from './find-your-interest/fyi-score-shared/fyi-score-shared.component';
import { GeneralResourcesComponent } from './general-resources/general-resources.component';
import { HomeComponent } from './home/home.component';
import { MyAsvabSummaryResultsComponent } from './my-asvab-summary-results/my-asvab-summary-results.component';
import { ParentsComponent } from './parents/parents.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PortfolioDirectionsComponent } from './portfolio/portfolio-directions.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AsvabCepAtYourSchoolComponent } from './asvab-cep-at-your-school/asvab-cep-at-your-school.component';
import { CareerClusterResolveService } from './resolves/career-cluster-resolve.service';
import { CareerClusterComponent } from './career-cluster/career-cluster.component';
import { PathwayComponent } from './career-cluster/pathway/pathway.component';
import { OccupationResultsResolveService } from './resolves/occupation-results-resolve.service';
import { OccupationResultsComponent } from './career-cluster/occupation-results/occupation-results.component';
import { FindYourInterestResultResolveService } from './services/find-your-interest-result-resolve.service';
import { FyiTestService } from './services/fyi-test.service';
import { StudentComponent } from './student/student.component';
import { GeneralHelpComponent } from './general-help/general-help.component';
import { GeneralPrivacySecurityComponent } from './general-privacy-security/general-privacy-security.component';
import { GeneralLinkComponent } from './general-link/general-link.component';
import { PortfolioResolveService } from './resolves/portfolio-resolve.service';
import { OccupationDetailsComponent } from './occufind/occupation-details/occupation-details.component';
import { OccupationDetailResolveService } from './resolves/occupation-detail-resolve.service';
import { SsoLoginComponent } from 'app/sso-login/sso-login.component'
import { OccupationSearchResultsResolveService } from './resolves/occupation-search-results-resolve.service';
import { FavoritesComponent } from './favorites/favorites.component';
import { FavoritesResolveService } from './resolves/favorites-resolve.service';
import { OccupationSearchComponent } from './occufind/occupation-search/occupation-search.component';
import { OccupationSearchResultsComponent } from './occufind/occupation-search-results/occupation-search-results.component';
import { LearnAboutYourselfComponent } from './learn-about-yourself/learn-about-yourself.component';
import { ExploreCareersComponent } from './explore-careers/explore-careers.component';
import { PlanYourFutureComponent } from './plan-your-future/plan-your-future.component';
import { CreatePlanComponent } from './create-plan/create-plan.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { ViewPlanComponent } from './view-plan/view-plan.component';
import { PlanResumeComponent } from './plan-resume/plan-resume.component';
import { PlanCalendarComponent } from './plan-calendar/plan-calendar.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { EducationDetailsComponent } from './occufind/education-details/education-details.component';
import { MilitaryDetailsComponent } from './occufind/military-details/military-details.component';
import { EssTrainingComponent } from './ess-training/ess-training.component';
import { MediaCenterResolveService } from 'app/resolves/media-center-resolve.service';
import { OccupationTitleDescriptionResolveService } from 'app/resolves/occupation-title-description-resolve.service';
import { OccupationSchoolMajorsResolveService } from 'app/resolves/occupation-school-majors-resolve.service';
import { OccupationSchoolDetailsResolveService } from 'app/resolves/occupation-school-details-resolve.service';
import { FavoritesOccupationResolveService } from 'app/resolves/favorites-occupation-resolve.service';
import { DbInfoResolveService } from 'app/resolves/db-info-resolve.service';
import { EssTrainingResolveService } from 'app/resolves/ess-training-resolve.service';
import { MediaCenterComponent } from './media-center/media-center.component';
import { MediaCenterArticleComponent } from 'app/media-center/media-center-article/media-center-article.component';
import { MediaCenterArticleResolveService } from './resolves/media-center-article-resolve.service';
import { MediaCenterArticleListComponent } from 'app/media-center/media-center-article-list/media-center-article-list.component';
import { OccupationMilitaryMoreDetailsResolveService } from 'app/resolves/occupation-military-more-details-resolve.service';
import { OccupationMilitaryResourcesResolveService } from 'app/resolves/occupation-military-resources-resolve.service';
import { MyAsvabSummaryResultsManualComponent } from 'app/my-asvab-summary-results/my-asvab-summary-results-manual/my-asvab-summary-results-manual.component';
import { PortfolioAsvabScoreResolveService } from './resolves/portfolio-asvab-score-resolve.service';
import { FyiTiedResolveService } from './resolves/fyi-tied-resolve.service';
import { LoginRedirectService } from './core/login-redirect.service';
import { ContinuingEducationComponent } from './educators/continuing-education/continuing-education/continuing-education.component';
import { CountdownComponent } from './countdown/countdown.component';
import { CreatePlanResolveService } from './resolves/create-plan-resolve.service';
import { EditPlanResolveService } from './resolves/edit-plan-resolve.service';
import { PlanYourFutureResolveService } from './resolves/plan-your-future-resolve.service';
import { LegislationComponent } from './legislation/legislation.component';
import { OptionReadyComponent } from './option-ready/option-ready.component';
import { WorkValuesIntroComponent } from './work-values/work-values-intro/work-values-intro.component';
import { WorkValuesTestComponent } from './work-values/work-values-test/work-values-test.component';
import { WorkValuesTiedScoresComponent } from './work-values/work-values-tied-scores/work-values-tied-scores.component';
import { WorkValuesTestResultsComponent } from './work-values/work-values-test-results/work-values-test-results.component';
import { WorkValuesTestDataResolveService } from './resolves/work-values-test-data-resolve.service';
import { WorkValuesTestResultsResolveService } from './resolves/work-values-test-results-resolve.service';
import { ResumeBuilderPrintComponent } from './resume-builder-print/resume-builder-print.component';
import { ViewPlanResolveService } from './resolves/view-plan-resolve.service';
import { PlanCalendarResolveService } from './resolves/plan-calendar-resolve.service';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
import { MentorStudentDetailComponent } from './mentor-student-detail/mentor-student-detail.component';
import { MentorStudentViewResolveService } from './resolves/mentor-student-view-resolve.service';
import { MentorDashboardResolveService } from './resolves/mentor-dashboard-resolve.service';
import { MentorSettingComponent } from './mentor-setting/mentor-setting.component';
import { StudentProfileResolveService } from './resolves/student-profile-resolve.service';
import { MentorRegistrationComponent } from './mentor-registration/mentor-registration.component';
import { MentorSettingResolveService } from './resolves/mentor-setting-resolve.service';
import { StaticPageServiceService } from './services/static-page-service.service';
import { GeneralSiteComponent } from './general-site/general-site.component';
import { GeneralSiteResolveService } from './resolves/general-site-resolve.service';
import { GeneralOccufindViewAllComponent } from './occufind/general-occufind-view-all/general-occufind-view-all.component';




// *** NOTE if a route requires a login please use , canActivate: [LoginRedirectService]

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'score-request', component: HomeComponent },
  { path: 'home', redirectTo: '', pathMatch: 'full' },
  { path: 'score-request', component: HomeComponent },
  { path: 'faq', component: ContactUsComponent },
  { path: 'general-resources', component: GeneralResourcesComponent },
  // { path: 'general-resources:anchor', component: GeneralResourcesComponent },
  { path: 'general-link', component: GeneralLinkComponent },
  { path: 'general-privacy-security', component: GeneralPrivacySecurityComponent },
  { path: 'general-site', component: GeneralSiteComponent, resolve: {page: GeneralSiteResolveService} },
  { path: 'general-occufind-view-all', component: GeneralOccufindViewAllComponent },
  // {
  //   matcher: matcherStudent,
  //   component: StudentComponent
  // },
  // {
  //   matcher: matcherEducators,
  //   component: EducatorsComponent
  // },
  // {
  //   matcher: matcherParents,
  //   component: ParentsComponent
  // },
  {
    matcher: matcherPathway,
    component: PathwayComponent
  },
  { path: 'student', component: StudentComponent },
  { path: 'student-learn', component: StudentComponent },
  { path: 'student-explore', component: StudentComponent },
  { path: 'student-plan', component: StudentComponent },
  { path: 'educators', component: EducatorsComponent },
  { path: 'educators-test', component: EducatorsComponent },
  { path: 'educators-post-test', component: EducatorsComponent },
  { path: 'federally-funded-sources', component: EducatorsComponent },
  { path: 'teachers', component: EducatorsComponent },
  { path: 'parents', component: ParentsComponent },
  { path: 'parents-test', component: ParentsComponent },
  { path: 'parents-post-test', component: ParentsComponent },
  { path: 'parents-information', component: ParentsComponent },
  { path: 'portfolio', component: PortfolioComponent, resolve: { portfolioResolver: PortfolioResolveService }, canActivate: [LoginRedirectService] },
  { path: 'portfolio-directions', component: PortfolioDirectionsComponent, canActivate: [LoginRedirectService] },
  // { path: 'dashboard', component: DashboardComponent, resolve: { numberTestTaken: NumberTestTakenService, accessCodeDetail: AccessCodeDetailService, userInterestCodes: FyiInterestCodesService, scoreSummary: ScoreSummaryResolveService, asvabScore: AsvabScoreResolveService, tiedLink: TiedLinkResolveService }, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest', component: FyiInstructionComponent, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest-test', component: FyiTestComponent, resolve: { data: FyiTestService }, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest-score', component: FyiResultsComponent, resolve: { CombinedAndGenderScores: FindYourInterestResultResolveService, accessCodeDetail: AccessCodeDetailService }, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest-tied', component: FyiTiedComponent, resolve: { data: FyiTiedResolveService }, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest-reload', component: FyiResultsComponent, resolve: { CombinedAndGenderScores: FindYourInterestResultResolveService }, canActivate: [LoginRedirectService] },
  { path: 'find-your-interest-score-shared/:iCodeOne/:iCodeTwo', component: FyiScoreSharedComponent },
  {
    path: 'my-asvab-summary-results', component: MyAsvabSummaryResultsComponent,
    resolve: {
      scoreSummary: ScoreSummaryResolveService,
      mediaCenter: MediaCenterResolveService
    }, canActivate: [LoginRedirectService]
  },
  { path: 'passwordReset', component: PasswordResetComponent },
  { path: 'search-result', component: SiteSearchResultsComponent },
  { path: 'compare-occupation/:socIdOne/:socIdTwo/:socIdThree/:socIdFour/:socIdFive/:socIdSix/:socIdSeven/:socIdEight/:socIdNine/:socIdTen', component: CompareOccupationComponent, resolve: { occupationSelectionDetails: CompareOccupationResolveService }, canActivate: [LoginRedirectService] },
  { path: 'occufind-occupation-details/:socId', component: OccupationDetailsComponent, resolve: { occupationDetails: OccupationDetailResolveService }, runGuardsAndResolvers: 'always' },
  { path: 'occufind-employment-details/:socId', component: EmploymentDetailsComponent, resolve: { employmentDetails: EmploymentDetailsResolveService } },
  {
    path: 'occufind-education-details/:socId', component: EducationDetailsComponent,
    resolve: {
      occupationTitleDescription: OccupationTitleDescriptionResolveService,
      occuSchoolMajors: OccupationSchoolMajorsResolveService,
      occuSchoolDetails: OccupationSchoolDetailsResolveService,
      favoritesOccupation: FavoritesOccupationResolveService,
      dbInfoResolveService: DbInfoResolveService
    }
  },
  {
    path: 'occufind-military-details/:socId', component: MilitaryDetailsComponent,
    resolve: {
      occupationTitleDescription: OccupationTitleDescriptionResolveService,
      militaryMoreDetails: OccupationMilitaryMoreDetailsResolveService,
      militaryResources: OccupationMilitaryResourcesResolveService,
      favoritesOccupation: FavoritesOccupationResolveService,
    }
  },
  { path: 'occufind-school-details/:socId/:unitId', component: SchoolDetailsComponent, resolve: { schoolDetails: SchoolDetailsResolveService } },
  { path: 'occufind-occupation-search', component: OccupationSearchComponent, canActivate: [LoginRedirectService] },
  { path: 'occufind-occupation-search-results', component: OccupationSearchResultsComponent, resolve: { searchresultDetails: OccupationSearchResultsResolveService }, canActivate: [LoginRedirectService] },
  { path: 'career-cluster-public', redirectTo: 'career-cluster', pathMatch: 'full' },
  { path: 'career-cluster', component: CareerClusterComponent, resolve: { careerclusterDetails: CareerClusterResolveService } },
  { path: 'career-cluster-occupation-results/:ccId', component: OccupationResultsComponent, resolve: { occupationResults: OccupationResultsResolveService } },
  { path: 'sso-login/:redirect/:data', component: SsoLoginComponent },
  { path: 'sso-logoff', component: SsoLoginComponent },
  { path: 'asvab-cep-at-your-school', component: AsvabCepAtYourSchoolComponent },
  { path: 'favorites', component: FavoritesComponent, resolve: { favoritesResolver: FavoritesResolveService }, canActivate: [LoginRedirectService] },
  // { path: 'learn-about-yourself', component: LearnAboutYourselfComponent, resolve: {numberTestTaken: NumberTestTakenService, accessCodeDetail: AccessCodeDetailService, userInterestCodes: FyiInterestCodesService, scoreSummary: ScoreSummaryResolveService, asvabScore: AsvabScoreResolveService, tiedLink: TiedLinkResolveService} }, // moved resolves to top-result component
  { path: 'learn-about-yourself', component: LearnAboutYourselfComponent, resolve: { testResults: WorkValuesTestResultsResolveService }, canActivate: [LoginRedirectService], runGuardsAndResolvers: 'always' },
  { path: 'explore-careers', component: ExploreCareersComponent, resolve: { favoritesResolver: FavoritesResolveService }, canActivate: [LoginRedirectService] },
  { path: 'plan-your-future', component: PlanYourFutureComponent, resolve: { planYourFutureResolver: PlanYourFutureResolveService }, canActivate: [LoginRedirectService] },
  { path: 'create-plan', component: CreatePlanComponent, resolve: { createPlanResolver: CreatePlanResolveService }, canActivate: [LoginRedirectService] },
  { path: 'edit-plan/:id', component: EditPlanComponent, resolve: { editPlanResolver: EditPlanResolveService }, canActivate: [LoginRedirectService] },
  { path: 'view-plan/:id', component: ViewPlanComponent, resolve: { viewPlanResolver: ViewPlanResolveService }, canActivate: [LoginRedirectService] },
  { path: 'plan-resume', component: PlanResumeComponent, canActivate: [LoginRedirectService] },
  { path: 'resume-builder-print', component: ResumeBuilderPrintComponent, canActivate: [LoginRedirectService] },
  { path: 'plan-calendar', component: PlanCalendarComponent, resolve: { planCalendarResolver: PlanCalendarResolveService }, canActivate: [LoginRedirectService] },
  {
    path: 'training-registration-form/:id', component: EssTrainingComponent,
    resolve: {
      essTrainingResolveService: EssTrainingResolveService
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'media-center', component: MediaCenterComponent,
    resolve: {
      mediaCenter: MediaCenterResolveService
    }
  },
  {
    path: 'media-center-article/:id', component: MediaCenterArticleComponent,
    resolve: {
      mediaCenter: MediaCenterResolveService,
      article: MediaCenterArticleResolveService
    }
  },
  {
    path: 'media-center-article/:categoryName/:slug', component: MediaCenterArticleComponent,
    resolve: {
      mediaCenter: MediaCenterResolveService,
      article: MediaCenterArticleResolveService
    }
  },
  {
    path: 'media-center-article-list/:id', component: MediaCenterArticleListComponent,
    resolve: {
      mediaCenter: MediaCenterResolveService
    }
  },
  {
    path: 'my-asvab-summary-results-manual', component: MyAsvabSummaryResultsManualComponent,
    resolve: {
      asvabScore: PortfolioAsvabScoreResolveService,
      mediaCenter: MediaCenterResolveService
    }, canActivate: [LoginRedirectService]
  },
  {
    path: 'continuing-education', component: ContinuingEducationComponent
  },
  { path: 'general-help', component: GeneralHelpComponent },
  { path: 'countdown', component: CountdownComponent },
  { path: 'legislation', component: LegislationComponent },
  { path: 'option-ready', component: OptionReadyComponent },
  { path: 'work-values-intro', component: WorkValuesIntroComponent, canActivate: [LoginRedirectService] },
  { path: 'work-values-test', component: WorkValuesTestComponent, resolve: { testData: WorkValuesTestDataResolveService }, canActivate: [LoginRedirectService] },
  { path: 'work-values-tied-scores', component: WorkValuesTiedScoresComponent, canActivate: [LoginRedirectService] },
  { path: 'work-values-test-results', component: WorkValuesTestResultsComponent, canActivate: [LoginRedirectService] },
  {
    path: 'student-profile', component: StudentProfileComponent, resolve: {
      studentProfileResolver: StudentProfileResolveService
    },
    canActivate: [LoginRedirectService]
  },
  {
    path: 'mentor-student-detail/:id',
    component: MentorStudentDetailComponent,
    canActivate: [LoginRedirectService],
    resolve: {
      mentorStudentViewResolver: MentorStudentViewResolveService
    }
  },
  {
    path: 'mentor-setting', component: MentorSettingComponent, canActivate: [LoginRedirectService], resolve: {
      mentorSettingResolver: MentorSettingResolveService
    }
  },
  {
    path: 'mentor-dashboard', component: MentorDashboardComponent, canActivate: [LoginRedirectService], resolve: {
      mentorDashboardResolver: MentorDashboardResolveService
    }
  },
  { path: 'mentor-student-detail', component: MentorStudentDetailComponent },
  {
    path: ':guid/mentor-registration',
    component: MentorRegistrationComponent
  }
];

const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  onSameUrlNavigation: 'reload'
  // ...any other options you'd like to use
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export function matcherStudent(url: UrlSegment[]) {
  if (url.length === 1) {
    const path = url[0].path;
    if (path.startsWith('student')
    ) {
      return { consumed: url };
    }
  }
  return null;
}

export function matcherEducators(url: UrlSegment[]) {
  if (url.length === 1) {
    const path = url[0].path;
    if (path.startsWith('educators') ||
      path.startsWith('teachers')
    ) {
      return { consumed: url };
    }
  }
  return null;
}

export function matcherParents(url: UrlSegment[]) {
  if (url.length === 1) {
    const path = url[0].path;
    if (path.startsWith('parents')
    ) {
      return { consumed: url };
    }
  }
  return null;
}

export function matcherPathway(url: UrlSegment[]) {
  if (url.length === 1) {
    const path = url[0].path;
    if (path.startsWith('career-cluster-pathway')
    ) {
      return { consumed: url };
    }
  }
  return null;
}
