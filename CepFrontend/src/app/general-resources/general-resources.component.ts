import { Component, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { ConfigService } from '../services/config.service';
import { UserService } from 'app/services/user.service';
import { ResourceService } from '../services/resource.service';
import { MatDialog } from '@angular/material';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { PopupPlayerComponent } from '../core/dialogs/popup-player/popup-player.component';
import { AsvabSampleTestDialogComponent } from '../core/dialogs/asvab-sample-test-dialog/asvab-sample-test-dialog.component';
import { AsvabIcatSampleTestDialogComponent } from '../core/dialogs/asvab-icat-sample-test-dialog/asvab-icat-sample-test-dialog.component';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import $ from 'jquery';
declare var $: $

@Component( {
    selector: 'app-general-resources',
    templateUrl: './general-resources.component.html',
    styleUrls: ['./general-resources.component.scss']
} )
export class GeneralResourcesComponent implements OnInit {
    resource = this._configService.getImageUrl();
    
    resourceTopics = undefined;
    quickLinkTopics = undefined;
    resources = this.resource + 'static/';
    documents = this.resource + this._configService.getDocumentFolder();

    seoTitle = 'Career Planning Resources | ASVAB Career Exploration Program';
    metaDescription = 'Find guides, templates, activities, and other career planning resources for students to assist in career planning from the ASVAB Career Exploration Program.';

    /***
     * Resource Types enums
     */
    resourceType = {
        'Document': 1,
        'Video': 2,
        'Link': 3,
        'ExtLink': 4,
        'PopupVideo': 5
    };
    quickLinkType = {
        'Document': 1,
        'Link': 2,
        'JavaScript': 3
    };

    /* ngx slick carousel config */
    slideConfig = {
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            }, {
                breakpoint: 639,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    constructor(
        private _configService: ConfigService,
        private _userService: UserService,
        private _resourceService: ResourceService,
        private _dialog: MatDialog,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _router: Router,
        private _meta: Meta,
        private _titleTag: Title,
    ) { }
    
    ngOnInit() {
        // this.resource = this._config.getResource();

        /***
         * Get resources from DB
         */
        this._resourceService.getResources().subscribe(
            data => {
                this.resourceTopics = data;
            } );

        this._resourceService.getQuickLinks().subscribe(
            data => {
                this.quickLinkTopics = data;
            } );

        if( this._router.url == '/general-resources#box-row4'){
            var BoxRowInterval = setInterval(function () {
                if ($("#box-row4").length > 0) {
                    clearInterval(BoxRowInterval);
                    setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#box-row4").offset().top - $('#header').height() - $('#banner').height() - 50
                    }, 1000); 
                }, 1000);
                }
            }, 200); 
        }

        this._titleTag.setTitle(this.seoTitle);
        this._meta.updateTag({name: 'description', content: this.metaDescription});

        this._meta.updateTag({property: 'og:title', content: this.seoTitle});
        this._meta.updateTag({property: 'og:description', content: this.metaDescription});

        this._meta.updateTag({ name: 'twitter:title', content:  this.seoTitle});
        this._meta.updateTag({ name: 'twitter:description', content: this.metaDescription});
    }

    slickInit( e ) {
        // console.log( 'slick initialized' );
    }

    breakpoint( e ) {
        // console.log( 'breakpoint' );
    }

    afterChange( e ) {
        // console.log( 'afterChange' );
    }

    beforeChange( e ) {
        // console.log( 'beforeChange' );
    }

    toggleViewClass = [];
    toggleViewAll( index ) {
        switch ( index ) {
            case 0:
                this.toggleViewClass[0] = true;
                $( '.carousel' + index ).slick( 'unslick' );
                break;
            case 1:
                this.toggleViewClass[1] = true;
                $( '.carousel' + index ).slick( 'unslick' );
                break;
            case 2:
                this.toggleViewClass[2] = true;
                $( '.carousel' + index ).slick( 'unslick' );
                break;
            case 3:
                this.toggleViewClass[3] = true;
                $( '.carousel' + index ).slick( 'unslick' );
                break;
        }
    }

    toggleViewLess( index ) {
        switch ( index ) {
            case 0:
                this.toggleViewClass[0] = false;
                $( '.carousel' + index ).slick( this.slideConfig );
                break;
            case 1:
                this.toggleViewClass[1] = false;
                $( '.carousel' + index ).slick( this.slideConfig );
                break;
            case 2:
                this.toggleViewClass[2] = false;
                $( '.carousel' + index ).slick( this.slideConfig );
                break;
            case 3:
                this.toggleViewClass[3] = false;
                $( '.carousel' + index ).slick( this.slideConfig );
                break;
        }
    }


    ga_trackClick( param ) {
        this._googleAnalyticsService.trackClick( param );
    }

    gaSocialShare( socialPlug, value ) {
        this._googleAnalyticsService.trackSocialShare( socialPlug, value );
    }

    ga_trackSampleTestClick( param ) {
        this._googleAnalyticsService.trackSampleTestClick( param );
    }


    // TODO:
    isLoggedIn() {
        return this._userService.isLoggedIn();
    }


    /**
     * Open video player dialog.
     */
    showVideoDialog( videoId ) {
        // const videoUrl =  this.resource + 'CEP_VIDEOS/' + videoId;
        const videoUrl =  this._configService.getImageUrl() + 'CEP_VIDEOS/' + videoId;

        const dialogRef = this._dialog.open( PopupPlayerComponent, {
            data: { videoId: videoUrl } // set videoUrl for CEP hosted videos
        } );
    }

    /**
     * 
     * @param url quicklink url that calls a modal function ex: showSampleTestModal()
     */
    showModal(url){
        if(this[url]){
            this[url]();
        }
    }

    /**
     * called from showModal; opens the sample test dialog
     */
    showSampleTestModal() {
        const dialogRef = this._dialog.open(AsvabSampleTestDialogComponent , {
            hasBackdrop: true,
            panelClass: 'custom-dialog-container',
            height: '95%',
            autoFocus: true
        });
    }

    /**
     * called from showModal; opens the icat sample test dialog
     */
    showSampleIcatTestModal() {
        const dialogRef = this._dialog.open(AsvabIcatSampleTestDialogComponent , {
            hasBackdrop: true,
            panelClass: 'custom-dialog-container',
            height: '95%',
            autoFocus: true
        });
    }

}
