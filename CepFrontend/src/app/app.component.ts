import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd, NavigationError, Event } from '@angular/router';
// import { SessionService } from './services/session.service';
import { UserService } from './services/user.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'app';

    //Loading indicator implementation
    loading;
    constructor(
        private router: Router,
        // private _sessionService: SessionService,
        private _userService: UserService
    ) {
        this.router.events.subscribe((event: Event) => {

            if (event instanceof NavigationStart) {
                this.loading = true;

                // Setup keep alive modal for active session
                if (this._userService.isLoggedIn()) {
                    this._userService.startSession();
                }

            }

        });
    }

    ngAfterViewInit() {
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    this.loading = true;
                }
                else if (
                    window.scrollTo(0, 0),
                    event instanceof NavigationEnd ||
                    event instanceof NavigationCancel ||
                    event instanceof NavigationError
                ) {
                    this.loading = false;
                }
            });
    }
}
