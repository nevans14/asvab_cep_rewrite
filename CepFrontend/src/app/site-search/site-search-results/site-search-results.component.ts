import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-site-search-results',
  templateUrl: './site-search-results.component.html',
  styleUrls: ['./site-search-results.component.scss']
})
export class SiteSearchResultsComponent implements OnInit {

  gcseresults;
  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.gcseresults = this._sanitizer.bypassSecurityTrustHtml(
      '<span><gcse:searchresults-only></gcse:searchresults-only></span>'
    );
  }

}
