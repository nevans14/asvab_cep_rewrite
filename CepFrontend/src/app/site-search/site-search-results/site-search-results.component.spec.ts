import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteSearchResultsComponent } from './site-search-results.component';

describe('SiteSearchResultsComponent', () => {
  let component: SiteSearchResultsComponent;
  let fixture: ComponentFixture<SiteSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
