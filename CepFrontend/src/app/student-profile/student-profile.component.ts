import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { EditAccountDialogComponent } from 'app/core/dialogs/edit-account-dialog/edit-account-dialog.component';
import { MatDialog } from '@angular/material';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MentorService } from 'app/services/mentor.service';
import { ManageMentorsDialogComponent } from 'app/core/dialogs/manage-mentors-dialog/manage-mentors-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { PortfolioComment } from 'app/core/models/portfolioComment.model';
import { ClassroomActivity, ClassroomActivitySubmission, ClassroomActivitySubmissionComment, ClassroomActivitySubmissionLog } from 'app/core/models/classroomActivity';
import { formatDate } from '@angular/common';
import { PortfolioService } from 'app/services/portfolio.service';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MessageToStudentDialogComponent } from 'app/core/dialogs/message-to-student-dialog/message-to-student-dialog.component';
import { DirectMessageDialogComponent } from 'app/core/dialogs/direct-message-dialog/direct-message-dialog.component';
import { DirectMessage, messageType } from 'app/core/models/directMessage';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { UserOccupationPlan, UserOccupationPlanComment, UserOccupationPlanLog } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UtilityService } from 'app/services/utility.service';
import { EmailSettingsDialogComponent } from 'app/core/dialogs/email-settings-dialog/email-settings-dialog.component';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { RegisterDialogComponent } from 'app/core/dialogs/register-dialog/register-dialog.component';


function validateEmails(emails: string) {
  return (emails.split(',')
    .map(email => Validators.email(<AbstractControl>{ value: email }))
    .find(_ => _ !== null) === undefined);
}

function emailsValidator(control: AbstractControl): ValidationErrors | null {
  if (control.value === '' || !control.value || validateEmails(control.value)) {
    return null
  }
  return { emails: true };
}

enum CommentType {
  PORTFOLIO_COMMENT = 1,
  ACTIVITY_COMMENT = 2,
  PORTFOLIO_LOG = 3,
  ACTIVITY_LOG = 4,
  DIRECT_MESSAGE = 5,
  PLAN_COMMENT = 6,
  PLAN_LOG = 7
}

enum PortfolioLogType {
  LOG = 1,
  COMMENT = 2
}

const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.scss']
})
export class StudentProfileComponent implements OnInit {

  mentorInviteForm: FormGroup;
  table_data_group = false
  table_data_group1 = true

  leftNavCollapsed: boolean = false;
  userName: any = undefined;
  errorText: string;
  successText: string;
  userFirstLast: string;

  private mentors: any[];
  private portfolio: any;
  private activityComments: ClassroomActivitySubmissionComment[];
  private activities: ClassroomActivity[];
  public messageList: any[]; //for display purposes
  public activityList: any[]; //for display purposes
  public portfolioView: any;
  public numUnreadMessages: number;
  public showReadItems: boolean;
  public showPortfolioHistory: boolean;
  public directMessages: DirectMessage[];
  public userOccupationPlans: any[];
  public planList: any[]; //for display purposes
  public mentorInviteFormSubmitted: boolean;
  public needsToRegister: boolean;

  constructor(
    private _userService: UserService,
    private _dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _mentorService: MentorService,
    private _activatedRoute: ActivatedRoute,
    private _portfolioService: PortfolioService,
    private _classroomActivityService: ClassroomActivityService,
    private _planCalendarService: PlanCalendarService,
    private _userOccupationPlanService: UserOccupationPlanService,
    @Inject(LOCALE_ID) private locale: string,
    private _utilityService: UtilityService
  ) {

    this.mentorInviteForm = this._formBuilder.group({
      inviteEmails: ['',
        Validators.compose([Validators.required, emailsValidator])
      ]
    });
    this.errorText = "";
    this.successText = "";
    this.mentors = [];
    this.activityComments = [];
    this.portfolio = {};
    this.messageList = [];
    this.numUnreadMessages = 0;
    this.showReadItems = false;
    this.activities = [];
    this.activityList = [];
    this.portfolioView = {};
    this.showPortfolioHistory = false;
    this.directMessages = [];
    this.planList = [];
    this.userOccupationPlans = [];
    this.mentorInviteFormSubmitted = false;
    this.userFirstLast = "";
    this.needsToRegister = false;
  }

  ngOnInit() {

    this.mentors = this._activatedRoute.snapshot.data.studentProfileResolver[0];
    this.portfolio = this._activatedRoute.snapshot.data.studentProfileResolver[1];
    this.activityComments = this._activatedRoute.snapshot.data.studentProfileResolver[2];
    this.activities = this._activatedRoute.snapshot.data.studentProfileResolver[3];
    this.directMessages = this._activatedRoute.snapshot.data.studentProfileResolver[4];
    this.userName = this._activatedRoute.snapshot.data.studentProfileResolver[5];
    this.userOccupationPlans = this._activatedRoute.snapshot.data.studentProfileResolver[6];


    //Loop through data returned and normalize it to display in the UI
    this.setNewMessages(this.portfolio, this.activityComments, this.directMessages, this.userOccupationPlans);

    //sets bottom section
    this.setLogsSection(this.portfolio, this.activities, this.activityComments, this.userOccupationPlans);

    if (this.userName && this.userName.firstName && this.userName.lastName) {
      this.userFirstLast = this._utilityService.capitalizeFirstLetter2(this.userName.firstName) + " " +
        this._utilityService.capitalizeFirstLetter2(this.userName.lastName);
    }
  }

  openRegistrationModal() {
    this._dialog.open(RegisterDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      height: '95%',
      autoFocus: true,
      disableClose: true
    });
  }
  setNewMessages(portfolio, activityComments, directMessages, userOccupationPlans) {
    //***** Portfolio Comments
    if (portfolio && portfolio.comments && portfolio.comments.length > 0) {
      portfolio.comments.forEach((pc: PortfolioComment) => {
        let portfolioComment = this.getCommentViewItem(pc, CommentType.PORTFOLIO_COMMENT);
        this.messageList.push(portfolioComment);
        if (!portfolioComment.hasBeenRead) this.numUnreadMessages++;
      })
    }

    //***** Activity comments
    if (activityComments && activityComments.length > 0) {
      activityComments.forEach((comment: ClassroomActivitySubmissionComment) => {

        let classroomActivitySubmissionComment = this.getCommentViewItem(comment, CommentType.ACTIVITY_COMMENT);
        this.messageList.push(classroomActivitySubmissionComment);
        if (!classroomActivitySubmissionComment.hasBeenRead) this.numUnreadMessages++;

      })
    }

    //***** Plan comments + Plan Logs
    if (userOccupationPlans && userOccupationPlans.length > 0) {
      userOccupationPlans.forEach((plan: any) => {
        if (plan.comments && plan.comments.length > 0) {
          plan.comments.forEach((comment: UserOccupationPlanComment) => {

            let commentViewItem = this.getCommentViewItem(comment, CommentType.PLAN_COMMENT);
            this.messageList.push(commentViewItem);
            if (!commentViewItem.hasBeenRead) this.numUnreadMessages++;

          })
        }

        //only show submitted messages for user
        if (plan.logs && plan.logs.length > 0) {
          plan.logs.forEach((log: any) => {
            if (log.statusId == 5 && !log.hasBeenRead) {
              let messageItem = {
                id: log.id,
                from: this.getMentorName(log.mentorLinkId),
                fromId: log.mentorLinkId,
                message: plan.title + " reviewed",
                timeStamp: this._utilityService.convertDateToLocalDate(log.dateLogged),
                hasBeenRead: log.hasBeenRead,
                typeId: CommentType.PLAN_LOG,
                recipientId: null,
                classroomActivityId: null,
                planId: plan.id,
                submissionId: null,
                mentorLinkId: log.mentorLinkId,
                originalTimeStamp: log.dateLogged
              }

              if (!messageItem.hasBeenRead) this.numUnreadMessages++;

              this.messageList.push(messageItem);

            }
          })

        }
      });
    }


    //***** Activity logs are added to the new message list within setLogsSection() */

    //***** Portfolio logs
    if (portfolio && portfolio.logs && portfolio.logs.length > 0) {
      portfolio.logs.forEach((log: any) => {

        if (log.statusId == 5) {
          let messageItem = {
            id: log.id,
            from: this.getMentorNameByEmail(log.mentorUsername),
            fromId: log.mentorUserName,
            message: "Portfolio reviewed",
            timeStamp: this._utilityService.convertDateToLocalDate(log.dateLogged),
            hasBeenRead: log.hasBeenRead,
            typeId: CommentType.PORTFOLIO_LOG,
            recipientId: null,
            classroomActivityId: null,
            submissionId: null,
            originalTimeStamp: log.dateLogged
          }

          if (!messageItem.hasBeenRead) this.numUnreadMessages++;

          this.messageList.push(messageItem);

        }

      })
    }

    //***** Direct Messages
    if (directMessages && directMessages.length > 0) {
      directMessages.forEach((dm: DirectMessage) => {

        //was the message sent to you?
        //if the message was from you then you cannot send yourself the same message
        let isSentFromYou: boolean = (dm.fromUser == this.userName.username) ? true : false;
        let messageItem = {
          id: dm.id,
          from: (isSentFromYou) ? 'You' : this.getMentorNameByEmail(dm.fromUser),
          fromId: dm.fromUser,
          message: (dm.messageTypeId == 5) ? "recommended an event for your calendar <br/> Date: " + formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(dm.calendarDate)), 'MM/dd/yyyy', this.locale) + "<br /> " + dm.message
            : (dm.messageTypeId == 6) ? "recommended a task for you. <br/> Task: <br />" + dm.message : dm.message,
          timeStamp: this._utilityService.convertDateToLocalDate(dm.dateCreated),
          hasBeenRead: (isSentFromYou) ? true : dm.hasBeenRead,
          typeId: CommentType.DIRECT_MESSAGE,
          recipientId: null,
          classroomActivityId: null,
          submissionId: null,
          calendarDate: dm.calendarDate,
          messageTypeId: dm.messageTypeId,
          originalMessage: dm.message,
          originalTimeStamp: dm.dateCreated
        }

        this.messageList.push(messageItem);
        if (!messageItem.hasBeenRead) this.numUnreadMessages++;
      })
    }

  }

  setLogsSection(portfolio, activities, _activityComments, userOccupationPlans: UserOccupationPlan[]) {
    //***** portfolio + portfolio logs + portfolio comments for bottom section
    if (portfolio) {
      this.portfolioView.lastUpdate = (portfolio.dateUpdated) ? formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(portfolio.dateUpdated)), 'MM/dd/yyyy', this.locale)
        : formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(portfolio.dateCreated)), 'MM/dd/yyyy', this.locale);
      this.portfolioView.cssStatusClass = this.getPortfolioCssClass(portfolio);
      this.portfolioView.history = [];

      //show a log item for each mentor
      if (portfolio.mentors && portfolio.mentors.length > 0) {
        portfolio.mentors.forEach((mentor: any) => {

          // let mentorEmail = this.mentors.find(x => x.id == mentor.mentorId).mentorEmail
          let userName = this.getMentorName(mentor.mentorId);
          this.portfolioView.history.push({
            lastUpdate: (mentor.didReview) ? this._utilityService.convertDateToLocalDate(mentor.dateReviewed) :
              (portfolio.dateUpdated) ? this._utilityService.convertDateToLocalDate(portfolio.dateUpdated) : this._utilityService.convertDateToLocalDate(portfolio.dateCreated),
            type: PortfolioLogType.LOG,
            message: (mentor.didReview) ? "Reviewed " : "Submitted for review ",
            userName: userName
          })
        })
      }

      if (portfolio.logs && portfolio.logs.length > 0) {
        let sortedLogs = portfolio.logs.sort((a, b) => {
          let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
            db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
          return da - db;
        });

        let lastLog = sortedLogs[sortedLogs.length - 1];
        let lastLogDate = (lastLog.dateUpdated) ? lastLog.dateUpdated : lastLog.dateCreated;
        if (lastLogDate > this.portfolioView.lastUpdate) {
          this.portfolioView.lastUpdate = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastLogDate)), 'MM/dd/yyyy', this.locale);
        }

      }

      if (portfolio.comments && portfolio.comments.length > 0) {
        portfolio.comments.forEach((portfolioComment: any) => {
          this.portfolioView.history.push({
            lastUpdate: this._utilityService.convertDateToLocalDate(portfolioComment.dateCreated),
            type: PortfolioLogType.COMMENT,
            message: portfolioComment.message,
            userName: this.getMentorNameByEmail(portfolioComment.from)
          })
        })

        let sortedComments = portfolio.comments.sort((a, b) => {
          let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
            db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
          return da - db;
        });

        let lastComment = sortedComments[sortedComments.length - 1];

        if (lastComment.dateCreated > this.portfolioView.lastUpdate) {
          this.portfolioView.lastUpdate = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastComment.dateCreated)), 'MM/dd/yyyy', this.locale);
        }

      }


    }

    //loop through each activity and show any activities you are assigned
    //also if a log item is unread and typeId == 5 //reviewed add to top message list
    if (activities && activities.length > 0) {
      activities.forEach((activity: any) => {

        let activityComments = _activityComments.filter(x => x.classroomActivityId == activity.id);
        activity.comments = activityComments;
        if (!activity.submissions || activity.submissions.length == 0) {
          if (!activity.comments || activity.comments.length == 0) {
            return;
          }
        }

        let displayActivityItem: any = {};
        displayActivityItem.title = activity.title;
        displayActivityItem.id = activity.id;
        displayActivityItem.isLoading = false; //used for loading screen when retrieving submission logs
        displayActivityItem.expanded = false; //used for expand/collapse
        displayActivityItem.submissions = [];
        displayActivityItem.timeStamp = "";
        //flag used to retrieve records log records from server; false = fetch; true = ignore
        //this is needed due to the logs of each submission are not being returned
        displayActivityItem.hasRetrievedSubmissions = false;

        if (activity.submissions && activity.submissions.length > 0) {
          //order submissions for the activity
          let sortedSubmission = activity.submissions.sort((a, b) => {
            let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
              db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
            return da - db;
          });

          let lastSubmission = sortedSubmission[sortedSubmission.length - 1];
          // displayActivityItem.submissions = sortedSubmission;
          //set last update date for activity
          displayActivityItem.timeStamp = (lastSubmission.dateUpdated) ?
            formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastSubmission.dateUpdated)), 'MM/dd/yyyy', this.locale) :
            formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastSubmission.dateCreated)), 'MM/dd/yyyy', this.locale);

          sortedSubmission.forEach((x: any) => {
            if (x.logs && x.logs.length > 0) {

              //order logs and get the latest.
              let sortedLogs = x.logs.sort((a, b) => {
                let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
                  db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
                return da - db;
              });

              let lastLog = sortedLogs[sortedLogs.length - 1];

              //only show if 'reviewed' in unread message list 
              if (lastLog.statusId == 5) {
                let messageItem = {
                  id: lastLog.id,
                  from: this.getMentorNameByEmail(lastLog.user.username),
                  fromId: lastLog.user.username,
                  message: x.fileName + " reviewed for " + activity.title,
                  timeStamp: this._utilityService.convertDateToLocalDate(lastLog.dateLogged),
                  hasBeenRead: lastLog.hasBeenRead,
                  typeId: CommentType.ACTIVITY_LOG,
                  recipientId: null,
                  classroomActivityId: x.classroomActivityId,
                  submissionId: x.id, //workId
                  originalTimeStamp: lastLog.dateLogged
                }

                this.messageList.push(messageItem);
                if (!messageItem.hasBeenRead) this.numUnreadMessages++;
              }

            }

          })
        } else {
          //use the latest date from comments
          if (activity.comments && activity.comments.length > 0) {
            let sortedComments = activity.comments.sort((a, b) => {
              let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
                db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
              return da - db;
            });

            let lastComment = sortedComments[sortedComments.length - 1];
            displayActivityItem.timeStamp = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastComment.dateCreated)), 'MM/dd/yyyy', this.locale);

          }
        }

        this.activityList.push(displayActivityItem);
      })
    }

    //***** Plan Logs
    if (userOccupationPlans && userOccupationPlans.length > 0) {
      userOccupationPlans.forEach((plan: any) => {

        let planListViewItem: any = {};
        planListViewItem.title = plan.title;
        planListViewItem.id = plan.id;
        planListViewItem.expanded = false; //used for expand/collapse
        planListViewItem.messages = [];
        planListViewItem.statusId = plan.statusId;

        let timeStamp = (plan.dateUpdated) ? plan.dateUpdated : plan.dateCreated;
        planListViewItem.timeStamp = formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(timeStamp)), 'MM/dd/yyyy', this.locale);

        //add comments and logs 
        if (plan.logs && plan.logs.length > 0) {
          plan.logs.forEach((log: any) => {
            let mentor = this.mentors.find(x => x.id == log.mentorLinkId);

            let mentorUserName = (mentor) ? (mentor.firstName && mentor.lastName) ? mentor.firstName + ' ' + mentor.lastName : mentor.mentorEmail : "";

            let messageItem = {
              id: log.id,
              user: mentorUserName,
              message: (log.statusId == 2) ? "Submitted" : "Reviewed",
              timeStamp: this._utilityService.convertDateToLocalDate(log.dateLogged),
              hasBeenRead: log.hasBeenRead,
              typeId: 2,
              originalTimeStamp: log.dateLogged
            }

            planListViewItem.messages.push(messageItem);

          })
        }

        if (plan.comments && plan.comments.length > 0) {
          plan.comments.forEach((comment: any) => {

            let messageItem = {
              id: comment.id,
              user: this.getMentorNameByEmail(comment.from),
              message: comment.message,
              timeStamp: this._utilityService.convertDateToLocalDate(comment.dateCreated),
              hasBeenRead: comment.hasBeenRead,
              typeId: 1,
              originalTimeStamp: comment.dateCreated
            }
            planListViewItem.messages.push(messageItem);

          })
        }


        this.planList.push(planListViewItem);
      });
    }

  }

  getCommentViewItem(comment: any, commentType: CommentType) {

    //was the message sent to the student?
    let toYou: boolean = (comment.to.find(x => x.username == 'You')) ? true : false;
    let recipientId: any = null;

    //determine read status;
    let hasBeenRead: boolean = true;
    if (toYou) {
      let you = comment.to.find(x => x.username == 'You');
      hasBeenRead = you.hasBeenRead;
      recipientId = you.id;
    } else {
      hasBeenRead = (comment.to.filter(x => !x.hasBeenRead).length > 0) ? false : true;
    }

    if (comment.from == 'You' || comment.from == this.userName.username) {
      hasBeenRead = true;
    }

    let messageItem = {
      id: comment.id,
      from: (comment.from == 'You') ? "You" : this.getMentorNameByEmail(comment.from),
      fromId: comment.from,
      message: comment.message,
      timeStamp: this._utilityService.convertDateToLocalDate(comment.dateCreated),
      hasBeenRead: hasBeenRead,
      typeId: commentType,
      recipientId: recipientId,
      classroomActivityId: (commentType == CommentType.ACTIVITY_COMMENT) ? comment.classroomActivityId : null,
      planId: (commentType == CommentType.PLAN_COMMENT) ? comment.planId : null,
      submissionId: null,
      originalTimeStamp: comment.dateCreated
    }

    return messageItem;
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  editAccountModal() {
    let dialogRef = this._dialog.open(EditAccountDialogComponent, {
      data: this.userName,
      maxWidth: '340px',
    });

    dialogRef.afterClosed().subscribe(res => {
      this.userName.username = res.username;
    })
  }

  submitEmail() {

    //do not allow a student to send mentor invites if they have not registered.
    if (!this.userName) {
      this.needsToRegister = true;
      return;
    }

    this.mentorInviteFormSubmitted = true;
    this.mentorInviteForm.controls['inviteEmails'].disable();
    let formData: any = {
      mentorEmail: this.mentorInviteForm.value.inviteEmails
    }

    this._mentorService.save(formData).then((response: any) => {
      this.mentorInviteFormSubmitted = false;
      this.mentorInviteForm.controls['inviteEmails'].enable();
      this.successText = "Mentor(s) successfully invited!";
      setTimeout(() => { this.successText = ""; }, 5000);
      this.mentorInviteForm.reset();
    }).catch((error: any) => {
      this.mentorInviteFormSubmitted = false;
      this.mentorInviteForm.controls['inviteEmails'].enable();
      this.errorText = error;
      setTimeout(() => { this.errorText = ""; }, 5000);
      console.error("ERROR", error);
    })
  }

  showManageMentorList() {
    let dialogRef = this._dialog.open(ManageMentorsDialogComponent, {
      data: this.userName,
      maxWidth: '340px',
    });
  }

  showDirectMessage(replyObj: any = null) {
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    let data: any = {};

    if (!replyObj) {
      data = {
        isStudent: (userRole) ? (userRole != 'S') ? false : true : false
      }

    } else {
      data = {
        isStudent: (userRole) ? (userRole != 'S') ? false : true : false,
        directMessageMentorLinkId: replyObj.directMessageMentorLinkId,
        directMessageUsername: replyObj.directMessageUsername
      }
    }

    let dialogRef = this._dialog.open(DirectMessageDialogComponent, {
      data: data,
      maxWidth: '450px',
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {

        let newDirectMessage: DirectMessage = result;

        //show message in the new messages but as read
        let messageItem = {
          id: newDirectMessage.id,
          from: 'You',
          message: newDirectMessage.message,
          timeStamp: this._utilityService.convertDateToLocalDate(newDirectMessage.dateCreated),
          hasBeenRead: true,
          typeId: CommentType.DIRECT_MESSAGE,
          recipientId: null,
          classroomActivityId: null,
          submissionId: null,
          originalTimeStamp: newDirectMessage.dateCreated
        }

        this.messageList.push(messageItem);

        //was this a reply item?
        //if so check the previous message as read
        if (replyObj) {
          this.markMessageAsRead(replyObj.messageItem);
        }
      }
    })
  }

  markMessageAsRead(item) {

    if (item.typeId == CommentType.PORTFOLIO_COMMENT) {
      this._portfolioService.updateCommentByHasBeenRead(item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.recipientId == item.recipientId && x.typeId == CommentType.PORTFOLIO_COMMENT);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);
          }
        }

      });

    }

    if (item.typeId == CommentType.ACTIVITY_COMMENT) {
      this._classroomActivityService.updateActivityCommentAsRead(item.classroomActivityId, item.id, item.recipientId, !item.hasBeenRead).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.recipientId == item.recipientId && x.typeId == CommentType.ACTIVITY_COMMENT);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });

    }

    if (item.typeId == CommentType.ACTIVITY_LOG) {
      this._classroomActivityService.markSubmissionLogAsRead(item.classroomActivityId, item.submissionId, item.id, !item.hasBeenRead).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.submissionId == item.submissionId && x.typeId == CommentType.ACTIVITY_LOG);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });

    }

    if (item.typeId == CommentType.PORTFOLIO_LOG) {
      this._portfolioService.markPortfolioLogAsRead(item.id, !item.hasBeenRead).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.typeId == CommentType.PORTFOLIO_LOG);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });

    }

    if (item.typeId == CommentType.PLAN_LOG) {

      this._mentorService.markStudentOccupationPlanLogAsRead(item.mentorLinkId, item.planId, item.id, true).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.typeId == CommentType.PLAN_LOG);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });

    }

    if (item.typeId == CommentType.PLAN_COMMENT) {
      this._userOccupationPlanService.markCommentAsRead(item.planId, item.id, item.recipientId).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.typeId == CommentType.PLAN_COMMENT);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });

    }

    if (item.typeId == CommentType.DIRECT_MESSAGE) {
      this._userService.updateMessage(item.id).then((rowsAffected: any) => {

        if (rowsAffected > 0) {
          let index = this.messageList.findIndex(x => x.id == item.id && x.typeId == CommentType.DIRECT_MESSAGE);

          if (index > -1) {
            this.messageList[index].hasBeenRead = !this.messageList[index].hasBeenRead;
            this.numUnreadMessages--;
            this._userService.sendUserUpdate(true);

          }
        }

      });
    }

  }

  replyMessage(item) {

    let data: any = {};

    if (item.typeId == CommentType.PORTFOLIO_COMMENT) {

      //should always be found; throw an error if its not
      let mentor = this.mentors.find(x => x.mentorEmail == item.fromId);

      if (!mentor) {
        console.error("Cannot send a reply to an unknown recipient!");
        return;
      }

      data = {
        title: "Portfolio",
        studentName: item.from,
        mentorLinkId: mentor.id,
        id: null
      }

    }

    if (item.typeId == CommentType.ACTIVITY_COMMENT) {

      //should always be found; throw an error if its not
      let mentor = this.mentors.find(x => x.mentorEmail == item.fromId);

      if (!mentor) {
        console.error("Cannot send a reply to an unknown recipient!");
        return;
      }

      data = {
        title: "Activity",
        studentName: item.from,
        mentorLinkId: mentor.id,
        id: item.classroomActivityId
      }

    }

    if (item.typeId == CommentType.PLAN_COMMENT) {

      //should always be found; throw an error if its not
      let mentor = this.mentors.find(x => x.mentorEmail == item.fromId);

      if (!mentor) {
        console.error("Cannot send a reply to an unknown recipient!");
        return;
      }

      data = {
        title: "Plan",
        studentName: item.from,
        mentorLinkId: mentor.id,
        id: item.planId
      }

    }

    if (item.typeId == CommentType.DIRECT_MESSAGE) {

      //should always be found; throw an error if its not
      let mentor = this.mentors.find(x => x.mentorEmail == item.fromId);

      if (!mentor) {
        console.error("Cannot send a reply to an unknown recipient!");
        return;
      }

      this.showDirectMessage({
        directMessageMentorLinkId: mentor.id,
        directMessageUsername: mentor.mentorEmail,
        messageItem: item
      })

      return;

    }

    let dialogRef = this._dialog.open(MessageToStudentDialogComponent, {
      data: data,
      maxWidth: '340px',
    });

    //after dialog closes; update UI with new comments and mark the item as 
    //unread
    dialogRef.afterClosed().subscribe((response: any) => {

      if (response && response != 'cancel') {

        //if portfolio comment
        //update the portfolio history with a new comment that was sent
        if (item.typeId == CommentType.PORTFOLIO_COMMENT) {

          if (!item.hasBeenRead) {
            this.markMessageAsRead(item);
          }
          this.portfolioView.history.push({
            lastUpdate: this._utilityService.convertDateToLocalDate(response.dateCreated),
            type: 2,
            message: response.message,
            userName: 'You'
          })

          //add new comment to the top section
          let classroomActivitySubmissionComment = this.getCommentViewItem(response, CommentType.PORTFOLIO_COMMENT);
          this.messageList.push(classroomActivitySubmissionComment);

        }

        //if activity comment
        //update the activity history with a new comment that was sent
        if (item.typeId == CommentType.ACTIVITY_COMMENT) {
          //add new comment to log section at the bottom of the page
          let activity = this.activityList.find(x => x.id == item.classroomActivityId);

          if (activity) {

            if (!item.hasBeenRead) {
              this.markMessageAsRead(item);
            }

            activity.submissions.push({
              dateLogged: this._utilityService.convertDateToLocalDate(response.dateCreated),
              typeId: 1,
              message: response.message,
              userName: 'You'
            })
          }

          //add new comment to the top section
          let portfolioComment = this.getCommentViewItem(response, CommentType.ACTIVITY_COMMENT);
          this.messageList.push(portfolioComment);
        }

        if (item.typeId == CommentType.PLAN_COMMENT) {

          let plan = this.planList.find(x => x.id == item.planId);


          let messageItem = {
            id: response.id,
            user: 'You',
            message: response.message,
            timeStamp: this._utilityService.convertDateToLocalDate(response.dateCreated),
            hasBeenRead: true,
            typeId: 1,
            originalTimeStamp: response.dateCreated
          }
          plan.messages.push(messageItem);

          //add new comment to the top section
          response.from = 'You';
          let commentViewItem = this.getCommentViewItem(response, CommentType.PLAN_COMMENT);
          this.messageList.push(commentViewItem);

        }

      }

    });
  }

  getListItemsFilter(showReadItems: boolean) {

    let filterObj: any = {};

    if (!showReadItems) {
      filterObj.hasBeenRead = [false];
      // filterObj.hasBeenReviewed = [false];
    } else {
      filterObj.hasBeenRead = [false, true];
    }

    return filterObj;
  }

  getSubmissionStatusDescription(statusId, fileName) {

    if (statusId == 1) {
      return "In Progress";
    }

    if (statusId == 2) {
      return "Submitted " + fileName + " for Review: ";
    }

    if (statusId == 3) {
      return "Unsubmitted " + fileName;
    }

    if (statusId == 4) {
      return "Deleted";
    }

    if (statusId == 5) {
      return "Reviewed " + fileName + ": ";
    }
  }

  getActivitySubmissionLogs(activity: any) {

    if (!activity.hasRetrievedSubmissions) {

      if (this.activities && this.activities.length > 0) {

        let classroomActivity = this.activities.find(x => x.id == activity.id);

        if (!classroomActivity) return;

        //order submissions for the activity
        let sortedSubmission = classroomActivity.submissions.sort((a, b) => {
          let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
            db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
          return da - db;
        }).reverse();

        sortedSubmission.forEach((classroomActivitySubmission: ClassroomActivitySubmission) => {

          if (classroomActivitySubmission.logs && classroomActivitySubmission.logs.length > 0) {
            classroomActivitySubmission.logs.forEach((x: any) => {
              let submissionDisplayItem: any = {};
              submissionDisplayItem.typeId = 2;//submission
              submissionDisplayItem.message = this.getSubmissionStatusDescription(x.statusId, classroomActivitySubmission.fileName);
              submissionDisplayItem.dateLogged = this._utilityService.convertDateToLocalDate(x.dateLogged);
              submissionDisplayItem.userName = (x.statusId == 3) ? "" : this.getMentorNameByEmail(x.user.username); //don't show username if unsubmitted
              activity.submissions.push(submissionDisplayItem);
            })

          }
        })
      }

      //add messages/comments to the activity when its expanded
      if (this.activityComments && this.activityComments.length > 0) {
        let activityCommentsForActivity = this.activityComments.filter(x => x.classroomActivityId == activity.id);
        if (activityCommentsForActivity && activityCommentsForActivity.length > 0) {
          activityCommentsForActivity.forEach((activityComment: ClassroomActivitySubmissionComment) => {
            let submissionDisplayItem: any = {};
            submissionDisplayItem.typeId = 1;//comment
            submissionDisplayItem.message = activityComment.message;
            submissionDisplayItem.dateLogged = this._utilityService.convertDateToLocalDate(activityComment.dateCreated);

            if (activityComment.from.toLowerCase() == "you") {
              submissionDisplayItem.userName = "You";
            } else {
              submissionDisplayItem.userName = this.getMentorNameByEmail(activityComment.from);
            }

            activity.submissions.push(submissionDisplayItem)

          })
        }
      }

      activity.hasRetrievedSubmissions = !activity.hasRetrievedSubmissions;

    }
  }

  getActivityCssClass(activity: any) {
    let classroomActivity: ClassroomActivity = this.activities.find(x => x.id == activity.id);

    return this._utilityService.getActivityStatusCssClass(classroomActivity);

  }

  getPortfolioCssClass(portfolio: any) {

    if (portfolio.statusId == 5) {
      return "checkmark";
    }

    if (portfolio.statusId == 2) {
      return "ready_for_review";
    }

    if (portfolio.comments && portfolio.comments.length > 0) {
      return "in_progress";
    } else {
      return "not_started";
    }
  }

  getPlanCssClass(plan: any) {

    if (plan.statusId == 5) {
      return "checkmark";
    } else {
      if (plan.statusId == 2) {
        return "ready_for_review";
      } else {
        return "in_progress";
      }
    }
  }

  addToCalendar(item: any) {

    //save calendar item
    if (item.messageTypeId == 5) {

      let formData: UserOccupationPlanCalendar = {
        id: null,
        planId: null,
        title: item.originalMessage,
        date: (item.calendarDate) ? item.calendarDate : null,
        isGraduationDate: false,
        tasks: null
      }

      //save calendar not to a plan and all of its tasks if any
      this._planCalendarService.saveCalendar(formData).then((newRecord: UserOccupationPlanCalendar) => {
        var data = {
          title: 'Calendar Updated Success',
          message: '<p>Calendar successfully updated with the new date!</p>'
        }

        const dialogRef1 = this._dialog.open(MessageDialogComponent, {
          data: data,
          autoFocus: false,
          maxWidth: '300px'
        }).beforeClosed().subscribe((response: any) => {

          if (!item.hasBeenRead) {
            this.markMessageAsRead(item)
          }

        })

      }).catch((error: any) => {
        console.error("ERROR", error);
      });

    }

    //save task item
    if (item.messageTypeId == 6) {

      let formData: UserOccupationPlanCalendarTask = {
        id: null,
        planId: null,
        calendarId: null,
        taskName: item.originalMessage,
        completed: false
      }

      //save calendar not to a plan and all of its tasks if any
      this._planCalendarService.saveTask(formData).then((newRecord: UserOccupationPlanCalendarTask) => {
        var data = {
          title: 'Task Added Success',
          message: '<p>Task successfully added to your calendar!</p>'
        }

        const dialogRef1 = this._dialog.open(MessageDialogComponent, {
          data: data,
          autoFocus: false,
          maxWidth: '300px'
        }).beforeClosed().subscribe((response: any) => {

          if (!item.hasBeenRead) {
            this.markMessageAsRead(item)
          }

        })
      }).catch((error: any) => {
        console.error("ERROR", error);
      });

    }

  }

  getMentorName(mentorLinkId) {
    let mentor = this.mentors.find(x => x.id == mentorLinkId);
    if (mentor) {
      return (mentor.firstName && mentor.lastName) ? mentor.firstName + ' ' + mentor.lastName : mentor.mentorEmail;
    }
    return "";
  }

  getMentorNameByEmail(mentorEmail) {
    let mentor = this.mentors.find(x => x.mentorEmail == mentorEmail);
    if (mentor) {
      return (mentor.firstName && mentor.lastName) ? mentor.firstName + ' ' + mentor.lastName : mentor.mentorEmail;
    }
    return mentorEmail;
  }

  showNotificationSettings() {

    this._dialog.open(EmailSettingsDialogComponent);

  }
}
