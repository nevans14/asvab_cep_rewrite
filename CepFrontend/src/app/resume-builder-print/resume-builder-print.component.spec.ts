import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeBuilderPrintComponent } from './resume-builder-print.component';

describe('ResumeBuilderPrintComponent', () => {
  let component: ResumeBuilderPrintComponent;
  let fixture: ComponentFixture<ResumeBuilderPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeBuilderPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeBuilderPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
