import { Component, OnInit } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpHelperService } from '../services/http-helper.service'; 
import { Router } from '@angular/router';
import { ConfigService } from '../services/config.service';
declare var bCrypt: any;

@Component( {
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.scss']
} )
export class PasswordResetComponent implements OnInit {

    constructor( private location: LocationStrategy,
        private http: HttpClient,
        private _router: Router,
        private _config: ConfigService,
        private _httpHelper: HttpHelperService,
    ) {
        this.baseUrl = this._config.getBaseUrl() + 'CEP/';
    }

    ngOnInit() {
        this.restCredentials.resetId = this.getParameterByName('resetId');
        this.restCredentials.username = this.getParameterByName('username');
    }

    baseUrl;
    restCredentials = { resetId: '', username: '', Password1: undefined, Password2: undefined };
    registrationLabel = '';

    absoluteUrl: any;
    questionMarkPosition: any;
    urlParametersString: any;
    urlParameters: any;
    accForInsert: any;
    registrationLoginStatus: any;
    resetParameters = {};

    getParameterByName (name) {
		var absoluteUrl = ( <any>this.location )._platformLocation.location.href;
		absoluteUrl = absoluteUrl = absoluteUrl.replace("%20","+")
			.replace("%3D","=");
		absoluteUrl = absoluteUrl.substr(absoluteUrl.indexOf('?'));
		absoluteUrl = absoluteUrl.replace('?', ',')
			.replace('&', ',');
		var parts = absoluteUrl.split(',');
		for (var i = 0; i < parts.length; i++) {
			var subParts = parts[i].split('=');
			if (subParts[0] === name) {
				return decodeURIComponent(subParts[1]);
			}
		}
	}


    ResetPassword() {
        var data = {
            email: this.restCredentials.username,
            accessCode: "",
            password: "",
            resetKey: this.restCredentials.resetId
        };

        var pass1 = this.restCredentials.Password1;
        var pass2 = this.restCredentials.Password2;

        if ( pass1.length <= 7 ) {
            this.registrationLabel = "Password too short!";
            return;
        }
        if ( pass1 != pass2 ) {
            this.registrationLabel = "Passwords don't match!";
            this.restCredentials.Password1 = '';
            this.restCredentials.Password2 = '';
            return;
        }

        data.password = this.restCredentials.Password2;
        this._httpHelper.httpHelper('post', 'applicationAccess/passwordReset', null, data)
            .then(
            ( response ) => {
                this.registrationLoginStatus = response;
                // Success ..
                if ( this.registrationLoginStatus.statusNumber == 0 ) {
                    // Redirect to homepage
                    this._router.navigate( ['/'] );
                } else { // Error
                    this.registrationLabel = this.registrationLoginStatus.status
                }
            });
    };
}
