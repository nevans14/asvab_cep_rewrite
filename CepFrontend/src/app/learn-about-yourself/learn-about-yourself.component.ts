import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { MoreAboutMeComponent } from 'app/core/dialogs/more-about-me/more-about-me.component';
import { LearnAboutYourselfService } from 'app/services/learn-about-yourself.service';
import { UtilityService } from 'app/services/utility.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FindYourInterestService } from 'app/services/find-your-interest.service';
import { FindYourInterestManualScoreInputComponent } from 'app/core/dialogs/find-your-interest-manual-score-input/find-your-interest-manual-score-input.component';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { ConfigService } from 'app/services/config.service';
import { NonstudentDemoDialogComponent } from 'app/core/dialogs/nonstudent-demo-dialog/nonstudent-demo-dialog.component';
@Component({
  selector: 'app-learn-about-yourself',
  templateUrl: './learn-about-yourself.component.html',
  styleUrls: ['./learn-about-yourself.component.scss']
})
export class LearnAboutYourselfComponent implements OnInit, DoCheck, OnDestroy {

  haveAsvabTestScores = false;
  haveInterestCodes = false;
  occufindStarted = false;
  userCompletion;
  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  moreAboutMe : {
    likes: any[],
    dislikes: any[]
  };
  workValueScores;
  isWorkValueResults;
  workValueTopResults;
  navigationSubscription;
  viewedASVABScore = false;
  viewedASVABScoreClosedHint = false;
  completedFYIClosedHint = false;
  completedOccupationSearch = false;
  completedOccupationSearchClosedHint = false;
  viewedLineScore = false;
  viewedLineScoreClosedHint = false;
  completedWorkValues = false;
  completedWorkValuesClosedHint = false;
  completedMoreAboutMe = false;
  completedMoreAboutMeClosedHint = false;
  numberTestTaken:any;
  hasMilitaryPlanEnlisted = false;
  userOccupationPlans: UserOccupationPlan[];
  ssoUrl: any;
  currentUser: any;
  userRole: any;
  nonStudentDemoClosedHint: boolean = false;
  nonStudentDemoModalShow: boolean = true;

  //Guided Tour Tips
  guidedTipConfig;

  constructor(
    private _user: UserService,
    private _dialog: MatDialog,
    private _learnAboutYourselfService: LearnAboutYourselfService,
    private _utility: UtilityService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _FYIScoreService: FindYourInterestService,
    private _matDialog: MatDialog,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _configService: ConfigService,
  ) {
    this.navigationSubscription = this._router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });

    this.ssoUrl = this._configService.getSsoUrl();

    this.currentUser = this._user.getUser() ? this._user.getUser().currentUser : null;
    this.userRole = this.currentUser ? this.currentUser.role : null;
  }

  ngOnInit() {
    this._user.getCompletion().subscribe(data => {
      this.userCompletion = data;

      if (this.userRole != 'S' && !this.nonStudentDemoClosedHint && this.nonStudentDemoModalShow) {
        this.nonStudentDemoModalShow = false;
        if (this._user.checkUserCompletion(this.userCompletion, UserService.NONSTUDENT_DEMO_CLOSED_HINT)) {
          this.nonStudentDemoClosedHint = true;
        } else {
          this._dialog.open(NonstudentDemoDialogComponent, {
            maxWidth: '600px',
          })
        }
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_ASVAB_SCORE)) {
        this.viewedASVABScore = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_ASVAB_SCORE_CLOSED_HINT)) {
        this.viewedASVABScoreClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_FYI_CLOSED_HINT)) {
        this.completedFYIClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_OCCUPATION_SEARCH)) {
        this.completedOccupationSearch = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT)) {
        this.completedOccupationSearchClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_LINE_SCORE)) {
        this.viewedLineScore = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_LINE_SCORE_CLOSED_HINT)) {
        this.viewedLineScoreClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_WORK_VALUE)) {
        this.completedWorkValues = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_WORK_VALUE_CLOSED_HINT)) {
        this.completedWorkValuesClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.LIKE_DISLIKE_MORE_ABOUT_ME)) {
        this.completedMoreAboutMe = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT)) {
        this.completedMoreAboutMeClosedHint = true;
      }

      const guidedTipConfigs = this._user.getCompletionByType(this.userCompletion, UserService.GUIDED_TIP_CONFIG);
      this.guidedTipConfig = (guidedTipConfigs) ? JSON.parse(guidedTipConfigs.value) : null;
    });

    this._userOccupationPlanService.getAll().then((plans: UserOccupationPlan[]) => {
      this.userOccupationPlans = plans;
      this.hasMilitaryPlanEnlisted = this.userOccupationPlans.some((p: any) => 
        p.militaryPlans && p.militaryPlans.length > 0 && p.militaryPlans.some(milP => milP.didSelectEnlisted)
      )
    });

    //get more about me details
    this._learnAboutYourselfService.getMoreAboutMe().then((response: any) => {
      this.moreAboutMe = this.formatMoreAboutMeForView(response);
    });

    if (window.innerWidth < 640) {
      this.leftNavCollapsed = this._user.getLeftNavCollapsed();;
    }
    
    this.workValueScores = this._activatedRoute.snapshot.data.testResults;
	if (this.workValueScores) {
		this.workValueTopResults = [
				{
					"TypeId" : this.workValueScores.workValueOne
				},
				{
					"TypeId" : this.workValueScores.workValueTwo
				},
				{
					"TypeId" : this.workValueScores.workValueThree
				} ]
		window.sessionStorage
				.setItem(
						'workValueTopResults',
						JSON
								.stringify(this.workValueTopResults));
	}
	this.isWorkValueResults = JSON.parse(window.sessionStorage.getItem('workValueTopResults')) ? true : false;

  this._FYIScoreService.getNumberOfTestTaken().then( response => {
    this.numberTestTaken = response;
  } );

  }


  ngDoCheck() { 
    this.haveAsvabTestScores = !!window.sessionStorage.getItem( 'sV' )
      && !!window.sessionStorage.getItem( 'sS' )
      && !!window.sessionStorage.getItem( 'sM' );

    this.haveInterestCodes = !!window.sessionStorage.getItem( 'interestCodeOne' )
      && !!window.sessionStorage.getItem( 'interestCodeTwo' )
      && !!window.sessionStorage.getItem( 'interestCodeThree' );
  }

  moreAboutMeModal() {

    //retrieve list of experiences to populate inside modal
    this._learnAboutYourselfService.getMoreAboutMe().then((response: any) => {

      const dialogRef = this._dialog.open(MoreAboutMeComponent, {
            hasBackdrop: true,
            panelClass: 'new-modal',
            height: '95%',
            autoFocus: true,
            disableClose: true,
            data: response
        });

      dialogRef.beforeClosed().subscribe(result => {
        //refresh data on page
        if(result){
          this.moreAboutMe = {likes: [], dislikes: []};
          
            if(result.likes){
              this.moreAboutMe.likes =  Array.from(new Set(this._utility.getValuesFromMoreAboutMe(result.likes)));
            }
            if(result.dislikes){
              this.moreAboutMe.dislikes =  Array.from(new Set(this._utility.getValuesFromMoreAboutMe(result.dislikes)));
            }
        }
      });

    });

  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }
 
  /**
   * format the json returned from the server
   * @param experiences 
   */
  formatMoreAboutMeForView(experiences: any[]){
    let returnMoreAboutMe : {
      likes: any[],
      dislikes: any[]
    } = {
      likes: [],
      dislikes: []
    };

    let likes: string[] = [];
    let dislikes: string[] = [];

    for (let experience of experiences) { 
      let parsedMoreAboutMe = JSON.parse(experience.moreAboutMe).values;
      if(parsedMoreAboutMe){

        for (let like of this._utility.getValuesFromMoreAboutMe(parsedMoreAboutMe[1].likes)) { 
          likes.push(like);
        }

        for (let dislike of this._utility.getValuesFromMoreAboutMe(parsedMoreAboutMe[2].dislikes)) { 
          dislikes.push(dislike);
        }
      }
    }

    let uniqueLikes = Array.from(new Set(likes));
    returnMoreAboutMe.likes = uniqueLikes;
    
    let uniqueDislikes = Array.from(new Set(dislikes));
    returnMoreAboutMe.dislikes = uniqueDislikes;

    return returnMoreAboutMe;
  }

  completionViewScore() {
    this.viewedASVABScore = true;
    this._user.setCompletion(UserService.VIEWED_ASVAB_SCORE, 'true');
  }

  closedViewScoreHint() {
    this.viewedASVABScoreClosedHint = true;
    this._user.setCompletion(UserService.VIEWED_ASVAB_SCORE_CLOSED_HINT, 'true');
  }

  closedFYIHint() {
    this.completedFYIClosedHint = true;
    this._user.setCompletion(UserService.COMPLETED_FYI_CLOSED_HINT, 'true');
  }

  closedOccupationSearchHint() {
    this.completedOccupationSearchClosedHint = true;
    this._user.setCompletion(UserService.COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT, 'true');
  }

  completionViewLineScore() {
    this.viewedLineScore = true;
    this._user.setCompletion(UserService.VISITED_MILITARY_LINE_SCORE, 'true');
  }

  closedLineScoreHint() {
    this.viewedLineScoreClosedHint = true;
    this._user.setCompletion(UserService.VISITED_MILITARY_LINE_SCORE_CLOSED_HINT, 'true');
  }

  closedWorkValueHint() {
    this.completedWorkValuesClosedHint = true;
    this._user.setCompletion(UserService.COMPLETED_WORK_VALUE_CLOSED_HINT, 'true');
  }

  closedMoreAboutMeHint() {
    this.completedMoreAboutMeClosedHint = true;
    this._user.setCompletion(UserService.LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT, 'true');
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
        this.navigationSubscription.unsubscribe();
    }
  }

  openManualInput(){
    this._matDialog.open(FindYourInterestManualScoreInputComponent, {
      maxWidth: '800px'
    });
  }
}
