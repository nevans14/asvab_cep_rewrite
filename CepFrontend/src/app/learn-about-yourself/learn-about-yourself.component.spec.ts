import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnAboutYourselfComponent } from './learn-about-yourself.component';

describe('LearnAboutYourselfComponent', () => {
  let component: LearnAboutYourselfComponent;
  let fixture: ComponentFixture<LearnAboutYourselfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnAboutYourselfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnAboutYourselfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
