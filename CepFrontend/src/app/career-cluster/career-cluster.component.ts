import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Title, Meta } from '@angular/platform-browser';

import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { CareerClusterService } from 'app/career-cluster/services/career-cluster.service';
import { OccufindOccupationSearchService } from 'app/services/occufind-occupation-search.service';
import { StaticPageServiceService } from '../services/static-page-service.service';
import { ContentManagementService } from 'app/services/content-management.service';
import { ConfigService } from 'app/services/config.service';
@Component({
  selector: 'app-career-cluster',
  templateUrl: './career-cluster.component.html',
  styleUrls: ['./career-cluster.component.scss']
})
export class CareerClusterComponent implements OnInit {

	career;
	isLoggedIn;

	occupationResults;
  careerCluster: any = {};
  favorites;
  currentUser;
  occuFindUserLoggedIn;
  onetVersion: any = '';
  pageIndex = 1;
  pageSize = 20;

  selectedInterestCodeOne;
  selectedInterestCodeTwo;
  selectedTitleOne;
  selectedTitleTwo;
  brightOccupationFlagDisplay;
  greenOccupationFlagDisplay;
  stemOccupationFlagDisplay;
  hotOccupationFlagDisplay;
  userSearchStringDisplay;

  // OCCUFIND MENU PARAMS (KEYWORD, INTERESTS, SKILL, CATEGORIES)
  keyword = '';
  // INTEREST
  // Using (ngModelChange) since when (click)=limitTwo,
  // limitTwo gets filterItems before ngModel updates filterItems with the input selection
	pageHtml: any = [];
	awsMediaUrl;
	thumbnailBaseUrl;

  checkboxInterest = [{
		name : 'Realistic',
		code : 'R'
	}, {
		name : 'Investigative',
		code : 'I'
	}, {
		name : 'Artistic',
		code : 'A'
	}, {
		name : 'Social',
		code : 'S'
	}, {
		name : 'Enterprising',
		code : 'E'
	}, {
		name : 'Conventional',
		code : 'C'
	}];

	filterItems = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
	};
	filterItemsTwo = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };
	interestLimit = 2;
	interestOneChecked = 0;
	interestTwoChecked = 0;
  
  countInterestOneChecked(item) { 
		if (this.filterItems[item.code]) {
      this.interestOneChecked++;
    }
    else {
      this.interestOneChecked--;
    }
  }
	
	countInterestTwoChecked(item) { 
		if (this.filterItemsTwo[item.code]) {
			this.interestTwoChecked++;
    }
    else {
      this.interestTwoChecked--;
    }
  }

  getSelectedInterestCodes() {
		this.selectedTitleOne = undefined;
		this.selectedTitleTwo = undefined;
		var isFirstTitleFilled = false;
		for (var i = 0; i < this.checkboxInterest.length; i++) {
			if (this.filterItems[this.checkboxInterest[i].code]
				|| this.filterItemsTwo[this.checkboxInterest[i].code]) {
				if (!isFirstTitleFilled) {
					this.selectedTitleOne = this.checkboxInterest[i].name;
					isFirstTitleFilled = true;
				} else {
					this.selectedTitleTwo = this.checkboxInterest[i].name;
				}
			}
		}
  }
  
  displaySelectedInterestCodes() {
		var message = '';
		for (var i = 0; i < this.checkboxInterest.length; i++) {
			if (this.filterItems[this.checkboxInterest[i].code]
				|| this.filterItemsTwo[this.checkboxInterest[i].code]) {
				switch (this.checkboxInterest[i].code) {
					case 'R' : message += (message == '' ? 'Realistic' : ' and Realistic'); break;
					case 'I' : message += (message == '' ? 'Investigative' : ' and Investigative'); break;
					case 'A' : message += (message == '' ? 'Artistic' : ' and Artistic'); break;
					case 'S' : message += (message == '' ? 'Social' : ' and Social'); break;
					case 'C' : message += (message == '' ? 'Conventional' : ' and Conventional'); break;
					case 'E' : message += (message == '' ? 'Enterprising' : ' and Enterprising'); break;
					default:
						break;
				}
			}
		}
		// if no interest codes have been selected, then the filter returns 'All' jobs
		if (message == '') message += 'All';
		return message += ' Occupations';
  }

  // SKILL
  skillImportance = '';
  skillView = 'hi';
  skillLimit = 1;
  skillChecked = 0;
  currentPage = 1;
  itemsPerPage = 20;
  tab = {index: 0};

  checkboxSkill = [{
		name: 'Verbal',
		code: 'V'
	}, {
		name: 'Math',
		code: 'M'
	}, {
		name: 'Science/Technical',
		code: 'S'
  }];
  
	skillItems = {
		'V': false,
		'M': false,
		'S': false
	};
	
	limitOne(item) {
		if (this.skillItems[item.code])
			this.skillChecked++;
		else 
			this.skillChecked--;
	};
	
	onSkillChange(item) {
		this.skillImportance = (this.skillItems[item.code] ? item.code : '');
		if (this.skillImportance == '') {
      this.resetTabs();
    }
		this.saveSearch();
  };
  
  resetTabs() {
		this.skillView = 'hi';
		this.tab.index = 0;
		this.currentPage = 1;
		this.itemsPerPage = 20;
  }
  
  skillViewSelected(e) {
    this.skillView = 
      e.index == 0 ? 'hi' :
      e.index == 1 ? 'md' :
      'lw';
		this.saveSearch();
		this.currentPage = 1;
		this.tab.index = e.index;
	}

  // CATEGORY
  checkboxCategory = [{
		name: 'Bright Outlook',
		code: 'B',
		iconClass: 'icon hot',
		rel: 'BRIGHT'
	},  {
		name: 'STEM Careers',
		code: 'S',
		iconClass: 'icon stem',
		rel: 'STEM'
	}, {
		name: 'Hot Military Careers',
		code: 'H',
		iconClass: 'icon-hotjob',
		rel: 'HOT'
	}, {
		name: 'Available in the Military',
		code: 'M'
  }];
  
	categoryItems = {
		'B': false,
		'G': false,
		'S': false,
		'H': false,
		'M': false
  };

  displaySelectedCategories() {
		var message = '';
		for (var i = 0; i < this.checkboxCategory.length; i++) {
			if (this.categoryItems[this.checkboxCategory[i].code]) {
				switch (this.checkboxCategory[i].code) {
					case 'H': message += (message == '' ? 'Hot' 	: ' | Hot'); break;
					case 'B': message += (message == '' ? 'Bright' 	: ' | Bright'); break;
					case 'G': message += (message == '' ? 'Green' 	: ' | Green'); break;
					case 'S': message += (message == '' ? 'STEM' 	: ' | STEM'); break;
					case 'M': message += (message == '' ? 'Available in the Military' : ' | Available in the Military'); break;
				default:
					break;
				}
			}
		}
		return message;
	}	
  
  clearSelections() {
		this.keyword = '';		
		this.skillImportance = '';		
		this.resetTabs();
		this.categoryItems = {
			'B': false,
			'G': false,
			'S': false,
			'H': false,
			'M': false,
		};
		this.filterItems = {
			'R': false,
			'I': false,
			'A': false,
			'S': false,
			'E': false,
			'C': false
		};
		this.filterItemsTwo = {
			'R': false,
			'I': false,
			'A': false,
			'S': false,
			'E': false,
			'C': false
		};
		this.skillItems = {
			'V': false,
			'M': false,
			'S': false
		};
		this.interestOneChecked = 0;
		this.interestTwoChecked = 0;
		this.skillChecked = 0;	
		this.saveSearch();
  }

  // GENERIC OCCUFIND FUNCTIONS
  
  saveSearch() {
		var currentSearchSelections = {
			filterItems   : this.filterItems,
			filterItemsTwo : this.filterItemsTwo,
			categoryItems : this.categoryItems,
			skillItems    : this.skillItems,
			keyword       : this.keyword,
			currentPage   : this.currentPage,
			skillImportance : this.skillImportance,
			skillView     : this.skillView			
		};
    window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
    
    this.getSelectedInterestCodes();
  };

  bodyText = "<p><b>Available in the Military</b> indicates occupations that are offered in one or more of the Military Services, each with its own unique requirements.</p>" +
  "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" + 
  "<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
  "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
  //"<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" + 
  "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
  "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>";

  careerDefinitions() {
    this._utility.modal('', this.bodyText)
  }

  filter(items, checkbox) {
		var size = 0;
		for (var i = 0; i < checkbox.length; i++) {
			if (items[checkbox[i].code]) {
				size++;
			}
		}
		return size;
  }
  
  scrollTo(id) {
		var elmnt = document.getElementById("search-sidebar");
	    elmnt.scrollIntoView();
	}

	showSortByStrength() {
    var data = {
        title: 'Career Exploration Scores',
        message: '<p>You can explore occupations that fit your ASVAB strengths. Choose the skill you want to investigate. Consider if it is one of your strengths.</p>' +
        '<p>You\'ll see the occupations organized in three levels of importance: Most Important, Moderately Important, and Less Important. This has to do with how often the skills are used on the job. Explore the importance level that relates to your relative strength in that skill.</p>' + 
        '<p>(For example, is math your top ASVAB strength? Sort by math and explore occupations in the Most Important category. If verbal skills are your lowest strength, sort by verbal and explore the occupations in the less important category).</p>'
    };

    const dialogRef1 = this.dialog.open(MessageDialogComponent, {
        data: data
        //maxWidth: '400px'
    });
  }

  careerDefinitionsNoCareerCluster() {
    var data = {
        title: 'Categories',
        message: "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
        "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
        //"<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
        "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
        "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
    };

    const dialogRef1 = this.dialog.open(MessageDialogComponent, {
        data: data
        //maxWidth: '400px'
    });
  }
  
  occufindSearch() {
		this.router.navigate(['/occufind-occupation-search-results']);
	};

  constructor(
    private _user: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private _utility: UtilityService,
		private _careerClusterService: CareerClusterService,
		private _occupationSearchService : OccufindOccupationSearchService,
		private dialog: MatDialog,
		private _meta: Meta,
    private _titleTag: Title,
		private staticPageService: StaticPageServiceService,
		private _cmsService: ContentManagementService,
		private _config: ConfigService,
  ) {
		// Initialize occufind search params from session
    const currentSearchSelections = JSON.parse(window.sessionStorage.getItem('currentSearchSelections'));
    this.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : this.skillImportance;
    this.skillView = currentSearchSelections ? currentSearchSelections.skillView : this.skillView;
    this.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : this.skillItems;
    this.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : this.filterItems;
    this.filterItemsTwo = currentSearchSelections ? currentSearchSelections.filterItemsTwo : this.filterItemsTwo;
    this.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : this.categoryItems;	
    this.keyword = currentSearchSelections ? currentSearchSelections.keyword : this.keyword;
    this.currentPage = currentSearchSelections ? currentSearchSelections.currentPage : this.currentPage;
    this.tab.index = 
      this.skillView == 'hi' ? 0 : 
      this.skillView == 'md' ? 1 : 
      this.skillView == 'lw' ? 2 : 
      0;
    this.interestOneChecked = this.filter(this.filterItems, this.checkboxInterest);
    this.interestTwoChecked = this.filter(this.filterItemsTwo, this.checkboxInterest);
    this.skillChecked = this.filter(this.skillItems, this.checkboxSkill);

    this.selectedInterestCodeOne = this._occupationSearchService.userSearch.interestCodeOne;
    this.selectedInterestCodeTwo = this._occupationSearchService.userSearch.interestCodeTwo;
    this.selectedTitleOne = this._occupationSearchService.getSelectedTitleOne(this._occupationSearchService.userSearch.interestCodeOne);
    this.selectedTitleTwo = this._occupationSearchService.getSelectedTitleTwo(this._occupationSearchService.userSearch.interestCodeTwo);
    this.brightOccupationFlagDisplay = this._occupationSearchService.userSearch.brightOccupationFlag;
    this.greenOccupationFlagDisplay = this._occupationSearchService.userSearch.greenOccupationFlag;
    this.stemOccupationFlagDisplay = this._occupationSearchService.userSearch.stemOccupationFlag;
    this.hotOccupationFlagDisplay = this._occupationSearchService.userSearch.hotOccupationFlag;
    this.userSearchStringDisplay = this._occupationSearchService.userSearch.userSearchString;

    this.getSelectedInterestCodes();
	}

  ngOnInit() {
    this.career = this.route.snapshot.data.careerclusterDetails[0];
    
    const path = this.router.url;
    this.pageHtml = this._cmsService.getPageByName(path);
		
		this.awsMediaUrl = this._config.getImageUrl();
		this.thumbnailBaseUrl = this.awsMediaUrl + 'MEDIA_CENTER/thumbnails/'

    this._careerClusterService.careerClusters = this.career;
    this.currentUser = this._user.getUser();
    this.isLoggedIn = this.currentUser !== undefined;
    this._utility.getByName('onet_version').subscribe((ver: any) => {
      this.onetVersion = ver;
		});
		
    if ( this._user.isLoggedIn() ) {
      this.occuFindUserLoggedIn = true;
			this._user.setCompletion(UserService.VISITED_CAREER_CLUSTER, 'true');
    } else {
      this.occuFindUserLoggedIn = false;
		}

		this.staticPageService.getPageByPageName("career-cluster").subscribe(data => {
			const seoTitle = data.pageTitle;
			const metaDescription = data.pageDescription;

			this._titleTag.setTitle(seoTitle);
			this._meta.updateTag({name: 'description', content: metaDescription});

			this._meta.updateTag({property: 'og:title', content: seoTitle});
			this._meta.updateTag({property: 'og:description', content: metaDescription});

			this._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
			this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
		})
  }
  
  selectedCareerCluster(ccId) {
		this._careerClusterService.selectedCareerCluster = ccId;
    this.router.navigate(['/career-cluster-occupation-results/' + ccId]);
  };
  
  addFavorite(ccId, title) {
    
    let that = this;
		// get the current favorites list
		var favoritePromise = this._user.getCareerClusterFavoritesList();
		favoritePromise.then(function(response: any) {
			
			var len = response.length;
			
			// check to see if career cluster was already added because we don't want duplicates
			for (var i = 0; i < len; i++) {
				if (response[i].ccId == ccId) {

					// inform user that the career cluster was already added to the list
					var modalOptions = {
						headerText : 'Career Cluster Favorites',
						bodyText : '<b>' + title + '</b>' + ' was already added to the list. Select another career cluster to add.'
					};

          that._utility.modal(modalOptions.headerText, modalOptions.bodyText)
					return false;	
				}
			}
			
			// limit 10 career clusters can be added
			if(len >= 2){
				
				// alert user that their favorites is full
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : 'Your favorite career cluster list is full. This career cluster was not added.'
				};

        that._utility.modal(modalOptions.headerText, modalOptions.bodyText)
				
				return false;
			}

			// save the career cluster id to the database if not yet saved
			var promise = that._user.insertFavoriteCareerCluster(ccId, title);
			promise.then(function(response) {

				//inform user that the career cluster is added to their favorites
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : '<b>' + title + '</b> is added to your favorites.',
				};

        that._utility.modal(modalOptions.headerText, modalOptions.bodyText)

			}, function(reason) {
				
				//inform user that there was an error and to try again.
				console.error(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.',
				};

        that._utility.modal(modalOptions.headerText, modalOptions.bodyText)
				return false;
			});
		}, function(reason) {
			console.error(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.',
			};

      that._utility.modal(modalOptions.headerText, modalOptions.bodyText)
			return false;
		});
	}
}
