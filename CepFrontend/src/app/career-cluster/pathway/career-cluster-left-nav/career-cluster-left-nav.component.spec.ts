import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterLeftNavComponent } from './career-cluster-left-nav.component';

describe('CareerClusterLeftNavComponent', () => {
  let component: CareerClusterLeftNavComponent;
  let fixture: ComponentFixture<CareerClusterLeftNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterLeftNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterLeftNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
