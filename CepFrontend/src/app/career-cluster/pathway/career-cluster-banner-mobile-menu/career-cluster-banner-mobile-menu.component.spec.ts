import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterBannerMobileMenuComponent } from './career-cluster-banner-mobile-menu.component';

describe('CareerClusterBannerMobileMenuComponent', () => {
  let component: CareerClusterBannerMobileMenuComponent;
  let fixture: ComponentFixture<CareerClusterBannerMobileMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterBannerMobileMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterBannerMobileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
