import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'app/services/config.service';
import { Title, Meta } from '@angular/platform-browser';

import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { CareerClusterRestFactoryService } from '../services/career-cluster-rest-factory.service';


@Component({
  selector: 'app-pathway',
  templateUrl: './pathway.component.html',
  styleUrls: ['./pathway.component.scss']
})
export class PathwayComponent implements OnInit {
  pageHtml = '';
  studentCep = true;
  studentExplore = false;
  studentLearn = false;
  studentPlan = false;
  path;
  currentUser;
  isLoggedIn = false;
  showMore = [];
  ccPathways = {pathways: []};
  awsMediaUrl;
  url = '/occufind-occupation-details/';

  constructor(
    private _router: Router,
    private _user: UserService,
    private _careerClusterRestFactory: CareerClusterRestFactoryService,
    private _config: ConfigService,
    private _utiliy: UtilityService,
    private _meta: Meta,
    private _titleTag: Title,
  ) { }

  ngOnInit() {
    this.currentUser = this._user.getUser();
    this.isLoggedIn = this.currentUser !== undefined;

    this.path = this._router.url;
    console.debug('url:', this.path);

    this.setSelectedTab('explore');
    this.getPageByName(this.path);

    this.awsMediaUrl = this._config.getImageUrl();
  }

  getPageByName(pageName) {
    let pageNum = undefined;
    let pathwayFilename = '';

    const pageNameArray = pageName.split('-');
    pageNum = parseInt(pageNameArray[pageNameArray.length - 1]);
    if (isNaN(pageNum)) {
      return;
    }

    let that = this;
    this._careerClusterRestFactory.getCareerClusterById(pageNum).subscribe(data => {
      that.showMore = [];
      that.ccPathways = data;
      that.ccPathways.pathways.forEach(function(pathway, i) {
        that.showMore[i] = false;
      })

      const seoTitle = that.ccPathways['pathwaySeoTitle'];
			const seoDescription = that.ccPathways['pathwaySeoDescription'];

			that._titleTag.setTitle(seoTitle);
			that._meta.updateTag({name: 'description', content: seoDescription});

			that._meta.updateTag({property: 'og:title', content: seoTitle});
			that._meta.updateTag({property: 'og:description', content: seoDescription});

			that._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
			that._meta.updateTag({ name: 'twitter:description', content: seoDescription});
    });
  }

  setSelectedTab(path) {
    if (path.indexOf('explore') > -1) {
      this.studentCep = false;
      this.studentLearn = false;
      this.studentExplore = true;
      this.studentPlan = false;
    } else if (path.indexOf('learn') > -1) {
      this.studentCep = false;
      this.studentLearn = true;
      this.studentExplore = false;
      this.studentPlan = false;
    } else if (path.indexOf('plan') > -1) {
      this.studentCep = false;
      this.studentLearn = false;
      this.studentExplore = false;
      this.studentPlan = true;
    } else if (this.path.indexOf('student') > -1) {
      this.studentCep = true;
      this.studentLearn = false;
      this.studentExplore = false;
      this.studentPlan = false;
    }
  }

  toggleShowOccupations(index, showMore) {
		this.showMore[index] = showMore;
	}
}

