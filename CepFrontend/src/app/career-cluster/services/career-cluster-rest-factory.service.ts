import { Injectable } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CareerClusterRestFactoryService {

    restURL: any;

    constructor(private _http: HttpClient,
        private _config: ConfigService) {

        this.restURL = this._config.getRestUrl() + '/';
    }

    getCareerCluster() {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/careerClusterList')).toPromise();
    }

    getCareerClusterOccupation = function (ccId) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/careerClusterOccupationList/' + ccId));
    }

    getCareerClusterOccupations = function (ccId) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/CareerClusterOccupations/' + ccId + '/'));
    }

    getCareerClusterById = function (ccId) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/CareerClusterById/' + ccId + '/'));
    }

    getStateSalary = function (onetSocTrimmed) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/StateSalary/${onetSocTrimmed}/`));
    }

    getStateEntrySalary = function (onetSocTrimmed) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/StateEntrySalary/${onetSocTrimmed}/`));
    }

    getSkillImportance = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/occupationSkillImportance/' + onetSoc + '/'));
    }

    getTasks = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/careerClusterOccupationTaskList/' + onetSoc + '/'));
    }

    getOccupationInterestCodes = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl('/api/careerCluster/occupationInterestCodes/' + onetSoc + '/'));
    }

    getEducationLevel = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/careerCluster/occupationEducationLevel/${onetSoc}/`));
    }

    getEducationLevels = function (onetSoc) {
        // onetSoc: comma separated string
        return this._http.get(this._config.getAwsFullUrl(`/api/careerCluster/occupationEducationLevels/${onetSoc}/`));
    }

    getRelatedOccupations = function (onetSoc) {
        // return this._http.get(this._config.getAwsFullUrl(`/api/careerCluster/relatedOccupation/${onetSoc}/`));
        return this._http.get( this.restURL + 'careerCluster/relatedOccupation/' + onetSoc + '/' );
    }

    getMilitaryResources = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/militaryCareerResource/${onetSoc}/`));
    }

    getOOHResources = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/OOHResource/${onetSoc}/`));
    }
}
