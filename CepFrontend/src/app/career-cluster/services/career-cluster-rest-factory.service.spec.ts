import { TestBed } from '@angular/core/testing';

import { CareerClusterRestFactoryService } from './career-cluster-rest-factory.service';

describe('CareerClusterRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerClusterRestFactoryService = TestBed.get(CareerClusterRestFactoryService);
    expect(service).toBeTruthy();
  });
});
