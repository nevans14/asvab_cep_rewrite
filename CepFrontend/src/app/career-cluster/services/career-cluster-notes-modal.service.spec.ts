import { TestBed } from '@angular/core/testing';

import { CareerClusterNotesModalService } from './career-cluster-notes-modal.service';

describe('CareerClusterNotesModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerClusterNotesModalService = TestBed.get(CareerClusterNotesModalService);
    expect(service).toBeTruthy();
  });
});
