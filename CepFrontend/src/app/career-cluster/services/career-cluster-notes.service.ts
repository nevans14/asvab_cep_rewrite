import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { CareerClusterNotesDialogComponent } from 'app/core/dialogs/career-cluster-notes-dialog/career-cluster-notes-dialog.component';
import { NotesRestFactoryService } from 'app/services/notes-rest-factory.service';


@Injectable({
  providedIn: 'root'
})
export class CareerClusterNotesService {

  public careerClusterNoteList = undefined;

  constructor(
    private _dialog: MatDialog,
    private _notesRestFactoryService: NotesRestFactoryService,
  ) { }

  getCareerClusterNotes() {
    let promise = new Promise((resolve, reject) => {

      // if careerClusterNoteList is undefined then recover data from database
      if (this.careerClusterNoteList == undefined) {

        if (typeof (Storage) !== "undefined") {

          if (window.sessionStorage.getItem("careerClusterNoteList") === null || window.sessionStorage.getItem("careerClusterNoteList") == 'undefined') {

            let that = this;
            // recover data using database
            this._notesRestFactoryService.getCareerClusterNotes().then(data => {

              // success now store in session object
              window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(data));

              // sync with in memory property
              that.careerClusterNoteList = JSON.parse(window.sessionStorage.getItem('careerClusterNoteList'));

              resolve(that.careerClusterNoteList);

            }, error => {

              console.error('Error' + error);
              reject("Error: request returned status" + error);
            });
          } else {

            // recover data using session storage
            this.careerClusterNoteList = JSON.parse(window.sessionStorage.getItem('careerClusterNoteList'));
            resolve(this.careerClusterNoteList);
          }

        } else {

          // no session storage support so get data using database
          this._notesRestFactoryService.getCareerClusterNotes().then(data => {
            this.careerClusterNoteList = data;
            resolve(this.careerClusterNoteList);

          }, error => {

            console.error('Error' + error);
            reject("Error: request returned status" + error);
          });
        }

      } else {

        // data is available so dont need to use session storage or database
        // to recover
        resolve(this.careerClusterNoteList);
      }
    });

    return promise;
  }

  insertCareerClusterNote(selectedNotesObject) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.insertCareerClusterNote(selectedNotesObject).then((data) => {

        // push newly added occupation to property
        that.careerClusterNoteList.push(data);

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(that.careerClusterNoteList));

          // sync with in memory property
          that.careerClusterNoteList = JSON.parse(window.sessionStorage.getItem('careerClusterNoteList'));
        }

        resolve("Insert successful.");
      }, (error) => {
        console.error(error);
        reject("Error: request returned status " + error);
      });

    });
    return promise;
  }

  deleteNote(noteId) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.deleteCareerClusterNote(noteId).then((data) => {

        // sync with in memory property
        let length = that.careerClusterNoteList.length;
        for (let i = 0; i < length; i++) {
          if (that.careerClusterNoteList[i].noteId == noteId) {
            that.careerClusterNoteList.splice(i, 1);
            break;
          }
        }

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(that.careerClusterNoteList));
          // that.careerClusterNoteList = JSON.parse(window.sessionStorage.getItem('careerClusterNoteList'));
        }

        resolve(that.careerClusterNoteList);

      }, (error) => {

        console.error('Error' + error);
        reject("Error: request returned status" + error);
      });
    });

    return promise;
  }

  updateCareerClusterNote(selectedNotesObject) {
    let promise = new Promise((resolve, reject) => {

      let that = this;
      this._notesRestFactoryService.updateCareerClusterNote(selectedNotesObject).then((data) => {

        // sync with in memory property
        let length = that.careerClusterNoteList.length;
        for (let i = 0; i < length; i++) {
          if (that.careerClusterNoteList[i].noteId == selectedNotesObject.noteId) {
            that.careerClusterNoteList[i] = selectedNotesObject;
          }
        }

        // sync with session storage
        if (typeof (Storage) !== "undefined") {
          window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(that.careerClusterNoteList));
          that.careerClusterNoteList = JSON.parse(window.sessionStorage.getItem('careerClusterNoteList'));
        }

        resolve(that.careerClusterNoteList);

      }, (error) => {
        console.error('Error' + error);
        reject("Error: request returned status" + error);
      });
    });

    return promise;
  }
}
