import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { CareerClusterNotesDialogComponent } from 'app/core/dialogs/career-cluster-notes-dialog/career-cluster-notes-dialog.component';
import { NotesRestFactoryService } from 'app/services/notes-rest-factory.service';


@Injectable({
  providedIn: 'root'
})
export class CareerClusterNotesModalService {

  public careerClusterNoteList = undefined;

  constructor(
    private _dialog: MatDialog,
    private _notesRestFactoryService: NotesRestFactoryService,
  ) { }

  show(data) {
    const dialogRef = this._dialog.open(CareerClusterNotesDialogComponent, {
      data: data,
    });
  }
}
