import { TestBed } from '@angular/core/testing';

import { CareerClusterService } from './career-cluster.service';

describe('CareerClusterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerClusterService = TestBed.get(CareerClusterService);
    expect(service).toBeTruthy();
  });
});
