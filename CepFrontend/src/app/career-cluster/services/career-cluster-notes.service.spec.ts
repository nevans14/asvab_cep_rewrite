import { TestBed } from '@angular/core/testing';

import { CareerClusterNotesService } from './career-cluster-notes.service';

describe('CareerClusterNotesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerClusterNotesService = TestBed.get(CareerClusterNotesService);
    expect(service).toBeTruthy();
  });
});
