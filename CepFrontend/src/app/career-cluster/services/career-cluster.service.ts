import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CareerClusterService {

  constructor() { }

  selectedCareerCluster: any;

  getSelectedCareerCluster = function() {
    return this.selectedCareerCluster;
  }

  careerClusters: any;

  getCareerClusters = function() {
    return this.careerClusters;
  }

  selectedOnetSoc: any;

  getSelectedOnetSoc = function() {
    return this.selectedOnetSoc;
  }

  selectedOcupationTitle: any;

  getSelectedOcupationTitle = function(){
    return this.selectedOcupationTitle;
  }
}
