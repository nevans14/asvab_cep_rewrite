import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { OccufindOccupationSearchService } from 'app/services/occufind-occupation-search.service';
import { CareerClusterNotesService } from 'app/career-cluster/services/career-cluster-notes.service';
import { CareerClusterNotesModalService } from 'app/career-cluster/services/career-cluster-notes-modal.service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-occupation-results',
  templateUrl: './occupation-results.component.html',
  styleUrls: ['./occupation-results.component.scss']
})
export class OccupationResultsComponent implements OnInit {

  occupationResults;
  careerCluster: any = {};
  favorites;
  currentUser;
  occuFindUserLoggedIn;
  onetVersion: any = '';
  pageIndex = 1;
  pageSize = 20;
  ccId;

  selectedInterestCodeOne;
  selectedInterestCodeTwo;
  selectedInterestCodeThree;
  selectedTitleOne;
  selectedTitleTwo
  selectedTitleThree;

  workValuesExpanded: boolean;
	categoryExpanded: boolean;

  // OCCUFIND MENU PARAMS (KEYWORD, INTERESTS, SKILL, CATEGORIES)
  keyword = '';
  // INTEREST
  // Using (ngModelChange) since when (click)=limitTwo,
  // limitTwo gets filterItems before ngModel updates filterItems with the input selection

  checkboxInterest = [{
    name: 'Realistic',
    code: 'R'
  }, {
    name: 'Investigative',
    code: 'I'
  }, {
    name: 'Artistic',
    code: 'A'
  }, {
    name: 'Social',
    code: 'S'
  }, {
    name: 'Enterprising',
    code: 'E'
  }, {
    name: 'Conventional',
    code: 'C'
  }];

  filterItems = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };
  filterItemsTwo = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };
  filterItemsThree = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };
  interestLimit = 3;
  interestOneChecked = 0;
  interestTwoChecked = 0;
  interestThreeChecked = 0;

  countInterestOneChecked(item) {
    if (this.filterItems[item.code]) {
      this.selectedInterestCodeOne = item.code;
      this.interestOneChecked++;
    }
    else {
      this.selectedInterestCodeOne = undefined;
      this.interestOneChecked--;
    }
  }

  countInterestTwoChecked(item) {
    if (this.filterItemsTwo[item.code]) {
      this.selectedInterestCodeTwo = item.code;
      this.interestTwoChecked++;
    }
    else {
      this.selectedInterestCodeTwo = undefined;
      this.interestTwoChecked--;
    }
  }

  countInterestThreeChecked(item) {
    if (this.filterItemsThree[item.code]) {
      this.selectedInterestCodeThree = item.code;
      this.interestThreeChecked++;
    }
    else {
      this.selectedInterestCodeThree = undefined;
      this.interestThreeChecked--;
    }
  }

  //sort
  interestCodeSort() {
    var self = this;
    this.occupationResults.forEach(function (element) {

      // one interest code selected
      if (self.interestOneChecked >= 1 && self.interestTwoChecked == 0 && self.interestThreeChecked == 0) {
        if (element.interestCdOne == self.selectedInterestCodeOne) {
          element.sortValue = 0;
        } else if (element.interestCdTwo == self.selectedInterestCodeOne) {
          element.sortValue = 1;
        } else if (element.interestCdThree == self.selectedInterestCodeOne) {
          element.sortValue = 3;
        }
      }

      // two interest code selected
      else if (self.interestOneChecked >= 1 && self.interestTwoChecked >= 1 && self.interestThreeChecked == 0) {
        if (element.interestCdOne == self.selectedInterestCodeOne && element.interestCdTwo == self.selectedInterestCodeTwo) {
          element.sortValue = 0;
        } else if (element.interestCdTwo == self.selectedInterestCodeOne && element.interestCdOne == self.selectedInterestCodeTwo) {
          element.sortValue = 1;
        } else if (element.interestCdOne == self.selectedInterestCodeOne && element.interestCdThree == self.selectedInterestCodeTwo) {
          element.sortValue = 3;
        } else if (element.interestCdThree == self.selectedInterestCodeOne && element.interestCdOne == self.selectedInterestCodeTwo) {
          element.sortValue = 4;
        } else if (element.interestCdTwo == self.selectedInterestCodeOne && element.interestCdThree == self.selectedInterestCodeTwo) {
          element.sortValue = 5;
        } else if (element.interestCdThree == self.selectedInterestCodeOne && element.interestCdTwo == self.selectedInterestCodeTwo) {
          element.sortValue = 6;
        }
      }

      // three interest code selected
      else if (self.interestOneChecked >= 1 && self.interestTwoChecked >= 1 && self.interestThreeChecked >= 1) {
        if (element.interestCdOne == self.selectedInterestCodeOne && element.interestCdTwo == self.selectedInterestCodeTwo && element.interestCdThree == self.selectedInterestCodeThree) {
          element.sortValue = 0;
        } else if (element.interestCdOne == self.selectedInterestCodeOne && element.interestCdTwo == self.selectedInterestCodeThree && element.interestCdThree == self.selectedInterestCodeTwo) {
          element.sortValue = 1;
        } else if (element.interestCdOne == self.selectedInterestCodeTwo && element.interestCdTwo == self.selectedInterestCodeOne && element.interestCdThree == self.selectedInterestCodeThree) {
          element.sortValue = 3;
        } else if (element.interestCdOne == self.selectedInterestCodeTwo && element.interestCdTwo == self.selectedInterestCodeThree && element.interestCdThree == self.selectedInterestCodeOne) {
          element.sortValue = 4;
        } else if (element.interestCdOne == self.selectedInterestCodeThree && element.interestCdTwo == self.selectedInterestCodeOne && element.interestCdThree == self.selectedInterestCodeTwo) {
          element.sortValue = 5;
        } else if (element.interestCdOne == self.selectedInterestCodeThree && element.interestCdTwo == self.selectedInterestCodeTwo && element.interestCdThree == self.selectedInterestCodeOne) {
          element.sortValue = 6;
        }

      }

      else {
        element.sortValue = undefined;
      }

    });
  }

  getSelectedInterestCodes() {
    this.selectedTitleOne = undefined;
    this.selectedTitleTwo = undefined;
    this.selectedTitleThree = undefined;
    var isFirstTitleFilled = false;
    var isSecondTitleFilled = false;
    for (var i = 0; i < this.checkboxInterest.length; i++) {
      if (this.filterItems[this.checkboxInterest[i].code]
        || this.filterItemsTwo[this.checkboxInterest[i].code]
        || this.filterItemsThree[this.checkboxInterest[i].code]) {
        if (!isFirstTitleFilled) {
          this.selectedTitleOne = this.checkboxInterest[i].name;
          isFirstTitleFilled = true;
        } else if (!isSecondTitleFilled) {
          this.selectedTitleTwo = this.checkboxInterest[i].name;
          isSecondTitleFilled = true;
        } else {
          this.selectedTitleThree = this.checkboxInterest[i].name;
        }
      }
    }
  }

  displaySelectedInterestCodes() {
    var message = '';
    for (var i = 0; i < this.checkboxInterest.length; i++) {
      if (this.filterItems[this.checkboxInterest[i].code]
        || this.filterItemsTwo[this.checkboxInterest[i].code]
        || this.filterItemsThree[this.checkboxInterest[i].code]) {
        switch (this.checkboxInterest[i].code) {
          case 'R': message += (message == '' ? 'Realistic' : ' and Realistic'); break;
          case 'I': message += (message == '' ? 'Investigative' : ' and Investigative'); break;
          case 'A': message += (message == '' ? 'Artistic' : ' and Artistic'); break;
          case 'S': message += (message == '' ? 'Social' : ' and Social'); break;
          case 'C': message += (message == '' ? 'Conventional' : ' and Conventional'); break;
          case 'E': message += (message == '' ? 'Enterprising' : ' and Enterprising'); break;
          default:
            break;
        }
      }
    }
    // if no interest codes have been selected, then the filter returns 'All' jobs
    if (message == '') message += 'All';
    return message += ' Occupations';
  }


  // SKILL
  skillImportance = '';
  skillView = 'hi';
  skillLimit = 1;
  skillChecked = 0;
  currentPage = 1;
  itemsPerPage = 20;
  tab = { index: 0 };

  checkboxSkill = [{
    name: 'Verbal',
    code: 'V'
  }, {
    name: 'Math',
    code: 'M'
  }, {
    name: 'Science/Technical',
    code: 'S'
  }];

  skillItems = {
    'V': false,
    'M': false,
    'S': false
  };

  limitOne(item) {
    if (this.skillItems[item.code])
      this.skillChecked++;
    else
      this.skillChecked--;
  };

  onSkillChange(item) {
    this.skillImportance = (this.skillItems[item.code] ? item.code : '');
    if (this.skillImportance == '') {
      this.resetTabs();
    }
    this.saveSearch();
  };

  resetTabs() {
    this.skillView = 'hi';
    this.tab.index = 0;
    this.currentPage = 1;
    this.itemsPerPage = 20;
  }

  skillViewSelected(e) {
    this.skillView =
      e.index == 0 ? 'hi' :
        e.index == 1 ? 'md' :
          'lw';
    this.saveSearch();
    this.currentPage = 1;
    this.tab.index = e.index;
  }

  // CATEGORY
  checkboxCategory = [{
    name: 'Bright Outlook',
    code: 'B',
    iconClass: 'icon hot',
    rel: 'BRIGHT'
  }, {
    name: 'STEM Careers',
    code: 'S',
    iconClass: 'icon stem',
    rel: 'STEM'
  }, {
    name: 'Hot Military Careers',
    code: 'H',
    iconClass: 'icon-hotjob',
    rel: 'HOT'
  }, {
    name: 'Available in the Military',
    code: 'M'
  }];

  categoryItems = {
    'B': false,
    'G': false,
    'S': false,
    'H': false,
    'M': false
  };

  // WORK VALUES
  checkboxWorkValues = [{
    name: 'Achievement',
    code: 'A',
    iconClass: 'iconWV achievement',
    rel: 'Achievement'
  }, {
    name: 'Independence',
    code: 'I',
    iconClass: 'iconWV independence',
    rel: 'Independence'
  }, {
    name: 'Recognition',
    code: 'R',
    iconClass: 'iconWV recognition',
    rel: 'Recognition'
  }, {
    name: 'Relationships',
    code: 'E',
    iconClass: 'iconWV relationships',
    rel: 'Relationships'
  }, {
    name: 'Support',
    iconClass: 'iconWV support',
    code: 'S',
    rel: 'Support'
  }, {
    name: 'Working Conditions',
    iconClass: 'iconWV workingConditions',
    code: 'W',
    rel: 'Working Conditions'
  }];

  workValueItems = {
		'A': false,
		'I': false,
		'R': false,
		'E': false,
		'S': false,
		'W': false
	};

  clearSelections() {
    this.keyword = '';
    this.skillImportance = '';
    this.resetTabs();
    this.categoryItems = {
      'B': false,
      'G': false,
      'S': false,
      'H': false,
      'M': false,
    };
    this.filterItems = {
      'R': false,
      'I': false,
      'A': false,
      'S': false,
      'E': false,
      'C': false
    };
    this.filterItemsTwo = {
      'R': false,
      'I': false,
      'A': false,
      'S': false,
      'E': false,
      'C': false
    };
    this.filterItemsThree = {
      'R': false,
      'I': false,
      'A': false,
      'S': false,
      'E': false,
      'C': false
    };
    this.skillItems = {
      'V': false,
      'M': false,
      'S': false
    };
    this.workValueItems = {
      'A': false,
      'I': false,
      'R': false,
      'E': false,
      'S': false,
      'W': false
    };
    this.interestOneChecked = 0;
    this.interestTwoChecked = 0;
    this.interestThreeChecked = 0;
    this.skillChecked = 0;
    this.saveSearch();
  }

  // GENERIC OCCUFIND FUNCTIONS

  saveSearch() {
    var currentSearchSelections = {
      filterItems: this.filterItems,
      filterItemsTwo: this.filterItemsTwo,
      filterItemsThree: this.filterItemsThree,
      selectedInterestCodeOne: this.selectedInterestCodeOne,
      selectedInterestCodeTwo: this.selectedInterestCodeTwo,
      selectedInterestCodeThree: this.selectedInterestCodeThree,
      categoryItems: this.categoryItems,
			workValueItems: this.workValueItems,
      skillItems: this.skillItems,
      keyword: this.keyword,
      currentPage: this.currentPage,
      skillImportance: this.skillImportance,
      skillView: this.skillView
    };
    window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));

    this.getSelectedInterestCodes();
    this.interestCodeSort();
  };


  careerDefinitions() {
    var data = {
      title: 'Career Exploration Scores',
      message: "<p><b>Available in the Military</b> indicates occupations that are offered in one of more of the Military Services, each with its own unique requirements.</p>" +
        "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
        "<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
        "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
        "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
        "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"

    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data
    });

  }


  filter(items, checkbox) {
    var size = 0;
    for (var i = 0; i < checkbox.length; i++) {
      if (items[checkbox[i].code]) {
        size++;
      }
    }
    return size;
  }

  // ------------------------------------------------------------------

  constructor(
    private _user: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private _utility: UtilityService,
    private _occupationSearchService: OccufindOccupationSearchService,
    private _careerClusterNotesService: CareerClusterNotesService,
    private _careerClusterNotesModalService: CareerClusterNotesModalService,
    private _meta: Meta,
    private _titleTag: Title,
    private _matDialog: MatDialog
  ) {
    // Initialize occufind search params from session
    const currentSearchSelections = JSON.parse(window.sessionStorage.getItem('currentSearchSelections'));
    this.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : this.skillImportance;
    this.skillView = currentSearchSelections ? currentSearchSelections.skillView : this.skillView;
    this.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : this.skillItems;
    this.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : this.filterItems;
    this.filterItemsTwo = currentSearchSelections ? currentSearchSelections.filterItemsTwo : this.filterItemsTwo;
    this.filterItemsThree = currentSearchSelections ? currentSearchSelections.filterItemsThree : this.filterItemsThree;
    this.selectedInterestCodeOne = currentSearchSelections ? currentSearchSelections.selectedInterestCodeOne : this.selectedInterestCodeOne;
    this.selectedInterestCodeTwo = currentSearchSelections ? currentSearchSelections.selectedInterestCodeTwo : this.selectedInterestCodeTwo;
    this.selectedInterestCodeThree = currentSearchSelections ? currentSearchSelections.selectedInterestCodeThree : this.selectedInterestCodeThree;
    this.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : this.categoryItems;
    this.workValueItems = currentSearchSelections ? currentSearchSelections.workValueItems : this.workValueItems;
    this.keyword = currentSearchSelections ? currentSearchSelections.keyword : this.keyword;
    this.currentPage = currentSearchSelections ? currentSearchSelections.currentPage : this.currentPage;
    this.tab.index =
      this.skillView == 'hi' ? 0 :
        this.skillView == 'md' ? 1 :
          this.skillView == 'lw' ? 2 :
            0;
    this.interestOneChecked = this.filter(this.filterItems, this.checkboxInterest);
    this.interestTwoChecked = this.filter(this.filterItemsTwo, this.checkboxInterest);
    this.interestThreeChecked = this.filter(this.filterItemsThree, this.checkboxInterest);
    this.skillChecked = this.filter(this.skillItems, this.checkboxSkill);

    this.workValuesExpanded = false;
		this.categoryExpanded = false;
  }

  ngOnInit() {
    this.ccId = this.route.snapshot.params.ccId;
    this.occupationResults = this.route.snapshot.data.occupationResults[0];
    this.interestCodeSort();
    this.careerCluster = this.route.snapshot.data.occupationResults[1];
    this.favorites = this.route.snapshot.data.occupationResults[2];
    this._utility.getByName('onet_version').subscribe((ver: any) => {
      this.onetVersion = ver;
    });

    if (this._user.isLoggedIn()) {
      this.occuFindUserLoggedIn = true;
    } else {
      this.occuFindUserLoggedIn = false;
    }

    const metaTitle = this.careerCluster.data && this.careerCluster.data.seoTitle
		  ? this.careerCluster.data.seoTitle
		  : this.careerCluster.data.ccTitle;
    const metaDescription = this.careerCluster.data && this.careerCluster.data.seoDescription
	    ? this.careerCluster.data.seoDescription
		  : '';

    this._titleTag.setTitle(metaTitle);
    this._meta.updateTag({name: 'description', content: metaDescription});

    this._meta.updateTag({property: 'og:title', content: metaTitle});
    this._meta.updateTag({property: 'og:description', content: metaDescription});

    this._meta.updateTag({ name: 'twitter:title', content:  metaTitle});
    this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
  }

  selectedOccupation(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
    this._occupationSearchService.selectedOnetSoc.push(onetSoc);
    this._occupationSearchService.selectedOcupationTitle = title;
    this._occupationSearchService.occupationGreen = green;
    this._occupationSearchService.occupationStem = stem;
    this._occupationSearchService.occupationBright = bright;
    this._occupationSearchService.occupationHot = hot;
    this._occupationSearchService.occupationInterestOne = interestCdOne;
    this._occupationSearchService.occupationInterestTwo = interestCdTwo;
    this._occupationSearchService.occupationInterestThree = interestCdThree;
    this.router.navigate(['/occufind-occupation-details/' + onetSoc]);
  };

  showOccufindNotes() {
    this._careerClusterNotesModalService.show({
      title: this.careerCluster.ccTitle,
      ccId: this.ccId
    });
  }

  showSortByStrength() {
    var data = {
      title: 'Career Exploration Scores',
      message: '<p>You can explore occupations that fit your ASVAB strengths. Choose the skill you want to investigate. Consider if it is one of your strengths.</p>' +
        '<p>You\'ll see the occupations organized in three levels of importance: Most Important, Moderately Important, and Less Important. This has to do with how often the skills are used on the job. Explore the importance level that relates to your relative strength in that skill.</p>' +
        '<p>(For example, is math your top ASVAB strength? Sort by math and explore occupations in the Most Important category. If verbal skills are your lowest strength, sort by verbal and explore the occupations in the less important category).</p>'
    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data,
      autoFocus: false
    });
  }

  careerDefinitionsNoCareerCluster() {
    var data = {
      title: 'Categories',
      message: "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
        "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
        "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
        "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data,
      autoFocus: false
    });
  }

  getWorkValueHelp() {
    var data = {
      title: 'WORK VALUES',
      message: "<p>Consider your top work values when exploring occupations and creating your action plan for the future. You can take the work values assessment <a href='work-values-test' title='Work Values test'>here</a>.</p>"
    };

    this._matDialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '400px',
      autoFocus: false
    });
  }

  getFilterCount(type): number {
    let totalCount = 0;

    const currentSearchSelections = JSON.parse(window.sessionStorage.getItem('currentSearchSelections'));

    if (!currentSearchSelections) {
      return totalCount;
    }

    if (type == 'category') {
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.B == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.G == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.S == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.H == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.M == true) ? totalCount + 1 : totalCount : totalCount;
      return totalCount;
    }

    if (type == 'workValues') {
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.A == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.I == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.R == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.E == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.S == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.W == true) ? totalCount + 1 : totalCount : totalCount;
      return totalCount;
    }

    return totalCount;
  }

	expandCategory(value) {
		this.categoryExpanded = value;

		//close the other drop down if its expanded
		if (this.workValuesExpanded) {
			this.workValuesExpanded = false;
		}
	}

	expandWorkValues(value) {
		this.workValuesExpanded = value;

		//close the other drop down if its expanded
		if (this.categoryExpanded) {
			this.categoryExpanded = false;
		}
	}

	@ViewChild('workValues') workValues: ElementRef;
	@ViewChild('categories') categories: ElementRef;

	@HostListener('document:mousedown', ['$event'])
	onGlobalClick(event): void {
		if (!this.workValues.nativeElement.contains(event.target)) {
			// clicked outside => close dropdown list
			if(this.workValuesExpanded){
				this.workValuesExpanded = false;
			}
		}

		if (!this.categories.nativeElement.contains(event.target)) {
			// clicked outside => close dropdown list
			if(this.categoryExpanded){
				this.categoryExpanded = false;
			}
		}
	}
}
