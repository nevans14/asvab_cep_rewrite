import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationResultsComponent } from './occupation-results.component';

describe('OccupationResultsComponent', () => {
  let component: OccupationResultsComponent;
  let fixture: ComponentFixture<OccupationResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
