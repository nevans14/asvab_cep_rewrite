import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkValuesTiedScoresComponent } from './work-values-tied-scores.component';

describe('WorkValuesTiedScoresComponent', () => {
  let component: WorkValuesTiedScoresComponent;
  let fixture: ComponentFixture<WorkValuesTiedScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkValuesTiedScoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkValuesTiedScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
