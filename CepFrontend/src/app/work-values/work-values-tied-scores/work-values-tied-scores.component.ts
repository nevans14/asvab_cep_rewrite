import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WorkValuesApiService } from 'app/services/work-values-api.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
@Component({
  selector: 'app-work-values-tied-scores',
  templateUrl: './work-values-tied-scores.component.html',
  styleUrls: ['./work-values-tied-scores.component.scss']
})
export class WorkValuesTiedScoresComponent implements OnInit {

	public workValueTiedResults;
	public tiedScores;
	public tieSelectionIndex;
	public availableSlots;
	public topThreeWorkValues;
	public tieScoresQueue;
	public postError;
	public isDisabled;
	public isRouteLoading;

	seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';

  constructor(
		  private _router: Router,
		  private workValueApi: WorkValuesApiService,
			private _userService: UserService,
			private _utility: UtilityService,
		) { }

  ngOnInit() {
	  this.workValueTiedResults = JSON.parse(window.sessionStorage.getItem('workValueTiedResults'));
		this.tiedScores = [];
		this.tieSelectionIndex = undefined;
		this.availableSlots = 3
		this.topThreeWorkValues = [];
		this.tieScoresQueue = [];
		
		this.setupTiedScores();

		this._utility.setSocialMeta(this.seoTitle, this.metaDescription);
  }
  
	

	// only keep TypeId and Score keys
	filterProperties = function(obj) {
		return {
			TypeId: obj.TypeId,
			Score: obj.Score,
		}
	}

	public testObject = {
			TestId: undefined, 
			Responses: undefined, 
			Scores: undefined, 
			TopThree: {WorkValueOne: undefined, 
				WorkValueTwo: undefined, 
				WorkValueThree: undefined}}

	
	/**
	 * Setup this.tiedScores to use to display tied selections.
	 */
	setupTiedScores = function() {

		var isNextScoreTied = false;
		var tiedScoreSet = [];
		for (var i = 0; i < this.workValueTiedResults.length; i++) {
			
			if ((i != this.workValueTiedResults.length - 1) && this.workValueTiedResults[i].Score == this.workValueTiedResults[i + 1].Score) {
				tiedScoreSet.push(this.workValueTiedResults[i]);
				this.workValueTiedResults[i].tied = true;
				isNextScoreTied = true;
			}else if (isNextScoreTied) {
				isNextScoreTied = false
				tiedScoreSet.push(this.workValueTiedResults[i]);
				this.workValueTiedResults[i].tied = true;
				this.tiedScores.push(tiedScoreSet);
				tiedScoreSet = [];
			} else {
				this.workValueTiedResults[i].tied = false;
			}
			
		}
		
		for (var i = 0; i < this.workValueTiedResults.length; i++) {
			if(!this.workValueTiedResults[i].tied){
				this.availableSlots--;
			} else {
				break;
			}
		}
	}
	
	
	selectTieScore = function() {
		this.postError = false;
		if(this.tiedScores[0].length == 2) {
				this.tieScoresQueue.push(this.tiedScores[0].splice([this.tieSelectionIndex], 1)[0]);
				this.tieScoresQueue.push(this.tiedScores[0].splice(0, 1)[0]); //push last in list
				this.tiedScores.splice(0, 1);
				this.availableSlots = this.availableSlots - 2;
		}else{
			this.tieScoresQueue.push(this.tiedScores[0].splice([this.tieSelectionIndex], 1)[0]);
			this.availableSlots--;
		}
		
		
		// last tie break slot
		if(this.availableSlots<1 || (this.availableSlots == 1 && !this.workValueTiedResults[2].tied)) {
			this.isRouteLoading = true;
			this.tiedSelectionCompleted = true;
			
			
			var tiedScoresSelected = {
						FirstTypeSelected: undefined,
						SecondTypeSelected: undefined,
						ThirdTypeSelected: undefined,
			}
			
			// sync tie selections into results
			for (var i = 0; i < this.workValueTiedResults.length - 3; i++) {
				if(this.workValueTiedResults[i].tied) {
					this.workValueTiedResults[i] = this.tieScoresQueue.shift();
					
					// setup user tied selected
					if(tiedScoresSelected.FirstTypeSelected == undefined) {
						tiedScoresSelected.FirstTypeSelected = this.workValueTiedResults[i].TypeId;
					} else if(tiedScoresSelected.SecondTypeSelected == undefined) {
						tiedScoresSelected.SecondTypeSelected = this.workValueTiedResults[i].TypeId;
					} else if(tiedScoresSelected.ThirdTypeSelected == undefined) {
						tiedScoresSelected.ThirdTypeSelected = this.workValueTiedResults[i].TypeId;
					}
				}
			}
			
			var index = 0;
			while (index < this.workValueTiedResults.length) { 
				this.workValueTiedResults[index] = this.filterProperties(this.workValueTiedResults[index]);
			    index++; 
			}
			window.sessionStorage.setItem('workValueTopResults', JSON.stringify(this.workValueTiedResults.slice(0,3)));
			
			
			var index = 0;
			var workValueTiedScoresUnsorted = JSON.parse(window.sessionStorage.getItem('workValueTiedResults'));
			while (index < workValueTiedScoresUnsorted.length) { 
				workValueTiedScoresUnsorted[index] = this.filterProperties(workValueTiedScoresUnsorted[index]);
			    index++; 
			}
			
			// setup testObject used for post request body			
			this.testObject.Scores = workValueTiedScoresUnsorted;
			this.testObject.TopThree = {WorkValueOne: this.workValueTiedResults[0].TypeId, WorkValueTwo: this.workValueTiedResults[1].TypeId, WorkValueThree: this.workValueTiedResults[2].TypeId}
			this.testObject.Responses = window.sessionStorage.getItem('workValueTestResponses');
			this.testObject.TiedScores = tiedScoresSelected;
			
			this.postWorkValues();
			
		}
		
		this.tieSelectionIndex = undefined;
	}
	
	/*
	 * Post work value testObject.
	 */
	postWorkValues = function(){
		
		this.isDisabled = true
		let self = this;
		this.workValueApi.getTestId().then((result) => {
		    self.testObject.TestId = result;
		    return self.workValueApi.postResults(self.testObject).toPromise();
		  }, (e) => {
			  console.error(e);
			  self.isRouteLoading = false;
			  self.isDisabled = false;
			  self.postError = true;
		  }).then(function(result) {
				self._userService.setCompletion(UserService.COMPLETED_WORK_VALUE, 'true');
			  window.sessionStorage.removeItem('workValueTestResponses');
			  window.sessionStorage.removeItem('workValueTiedResults');
	      self._router.navigateByUrl('/work-values-test-results');
		  }, function(e){
			  console.error(e);
			  self.isRouteLoading = false;
			  self.isDisabled = false;
			  self.postError = true;
		  });
	}
	
	

	tiedNumText = function() {
		var number = this.tiedScores[0].length > 1 
			? this.tiedScores[0].length 
			: this.tiedScores[1].length;
		switch(number) {
			case 2:
				return 'two';
			case 3:
				return 'three';
			case 4:
				return 'four';
			default:
				return '';
		}
	}

}
