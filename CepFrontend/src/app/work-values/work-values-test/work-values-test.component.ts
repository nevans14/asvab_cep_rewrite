import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { WorkValuesApiService } from 'app/services/work-values-api.service';
import { WorkValuesRetakeDialogComponent } from 'app/core/dialogs/work-values-retake-dialog/work-values-retake-dialog.component';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
@Component({
  selector: 'app-work-values-test',
  templateUrl: './work-values-test.component.html',
  styleUrls: ['./work-values-test.component.scss']
})
export class WorkValuesTestComponent implements OnInit {

	public isTestInProgress;	
	public testData;
	public progressPercentage;
	public activeQuestion;
	public lastQuestion;
	public workValueSortedScores;
	public rawScores;
	
	public isDisabled;

	seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';
	
  constructor(
  private _activatedRoute: ActivatedRoute,
  private _dialog: MatDialog,
  private workValueApi: WorkValuesApiService,
  private _router: Router,
	private _userService: UserService,
	private _utility: UtilityService,
	) { }

  ngOnInit() {
     this.isTestInProgress = window.sessionStorage.getItem('workValueUserSelections') ? true : false;	
	 this.testData = this.isTestInProgress ? JSON.parse(window.sessionStorage.getItem('workValueUserSelections')) : this._activatedRoute.snapshot.data.testData;
	 this.progressPercentage = 0;
	 this.activeQuestion = this.isTestInProgress ? window.sessionStorage.getItem('workValueActiveQuestion') : 0;
	 this.lastQuestion = this.testData.length - 1;
	 this.workValueSortedScores = [];
	 
	 /*
	 * Type ID definitions
	 * 1: Achievement 
	 * 2: Independence 
	 * 3: Recognition 
	 * 4: Relationships 
	 * 5: Support 
	 * 6: Working Conditions
	 */
	 this.rawScores = {
			"1": 0,
			"2": 0,
			"3": 0,
			"4": 0,
			"5": 0,
			"6": 0
	}
	 
	 this.updateProgressBar();

	 this._utility.setSocialMeta(this.seoTitle, this.metaDescription);
  }
  
  
	
	
	
	/**
	 * Setup next question.
	 */
	nextQuestion = function () {
		
		if(!this.testData[this.activeQuestion].answer) {
			/*var customModalOptions = {
					headerText : 'Work Values',
					bodyText : '<p>Make a selection.</p>'
				};
				var customModalDefaults = {size : 'sm'};
				modalService.show(customModalDefaults, customModalOptions);
				*/
				const data = {
			            'title': 'Work Values',
			            'message': '<p>Make a selection.</p>'
			          };
			          const dialogRef = this._dialog.open(MessageDialogComponent, {
			            data: data,
			            width: '400px',
			          });
			return;
		}

		if(this.activeQuestion < this.lastQuestion) {
			this.activeQuestion++;
			this.updateProgressBar();
			
			window.sessionStorage.setItem('workValueUserSelections', JSON.stringify(this.testData));
			window.sessionStorage.setItem('workValueActiveQuestion', this.activeQuestion);
			
		} else {
			this.submit(); // last question submit answers
		}
	}
	
	/**
	 * Setup previous question.
	 */
	previousQuestion = function () {

		if(this.activeQuestion > 0) {
			this.activeQuestion--;
			
			this.updateProgressBar();
			window.sessionStorage.setItem('workValueUserSelections', JSON.stringify(this.testData));
			window.sessionStorage.setItem('workValueActiveQuestion', this.activeQuestion);
		}
	}
	
	updateProgressBar = function () {
		this.progressPercentage = this.activeQuestion / this.lastQuestion * 100;
	}
	
	
	// only keep TypeId and Score keys
	filterProperties = function(obj) {
		return {
			TypeId: obj.TypeId, 
			Score: obj.Score,
		}
	} 

	public testObject = {
			TestId: undefined, 
			Responses: undefined, 
			Scores: undefined, 
			TopThree: {WorkValueOne: undefined, 
				WorkValueTwo: undefined, 
				WorkValueThree: undefined}}
	
	submit = function () {
		
		this.isDisabled = true;
		
		// reset values
		this.rawScores = {
				"1": 0,
				"2": 0,
				"3": 0,
				"4": 0,
				"5": 0,
				"6": 0
		}
		this.workValueSortedScores = []
		
		// tally scores
		for(var i = 0; i < this.testData.length; i++){
			var typeId = this.testData[i].answer.typeId;
			this.rawScores[typeId]++;
		}
		
		var typeId: any;
		for ( typeId in this.rawScores) {
		    this.workValueSortedScores.push({TypeId: typeId, Score: this.rawScores[typeId]});
		}
		
		//For testing purpose only
		/*this.workValueSortedScores = [{TypeId: "1", Score: 5},
			{TypeId: "2", Score: 4},
			{TypeId: "3", Score: 3},
			{TypeId: "4", Score: 2},
			{TypeId: "5", Score: 1},
			{TypeId: "6", Score: 0}];*/

		// create score list and sort scores from highest to lowest
		this.workValueSortedScores.sort(function(a, b) {
		    return b.Score - a.Score;
		});
		
		this.setupTiedScores(this.workValueSortedScores);
		var isTiedScore = this.isTiedScore();
		
		// setup test response string
		var testResponses = '';
		for(var i = 0; i < this.testData.length; i++){
			var typeId = this.testData[i].answer.typeId;
			var optionId = this.testData[i].answer.optionId;
			this.rawScores[typeId]++;
			testResponses = testResponses + optionId + '|';
		}
		
		
		if(isTiedScore){
			
			window.sessionStorage.setItem('workValueTestResponses', testResponses);
			
			window.sessionStorage.removeItem('workValueUserSelections');
			 window.sessionStorage.removeItem('workValueActiveQuestion');
			
			// Show retake test alert if there are more that 4 way ties and if not redirect user to tie break page.
			if(!this.maxAllowedTiesAlert()){
				window.sessionStorage.setItem('workValueTiedResults', JSON.stringify(this.workValueSortedScores));
				this._router.navigateByUrl('/work-values-tied-scores');
			}
			
			
		} else {
			
			var index = 0;
			
			// keep only TypeId and Scores properties
			while (index < this.workValueSortedScores.length) { 
				this.workValueSortedScores[index] = this.filterProperties(this.workValueSortedScores[index]);
			    index++; 
			}
			
			// setup testObject used for post request body
			this.testObject.Scores = this.workValueSortedScores
			this.testObject.TopThree = {WorkValueOne: this.workValueSortedScores[0].TypeId, WorkValueTwo: this.workValueSortedScores[1].TypeId, WorkValueThree: this.workValueSortedScores[2].TypeId}
			this.testObject.Responses = testResponses;
			
			
			this.postWorkValues();
		}
	}
	
	postWorkValues = function(){
		this.isDisabled = true;
		let self = this;
		this.workValueApi.getTestId().then((result) => {
			self.testObject.TestId = result;
		    return self.workValueApi.postResults(self.testObject).toPromise();
		  }, (e) => {
			  console.error(e);
			  self.isDisabled = false;
		  }).then(function(result) {
				self._userService.setCompletion(UserService.COMPLETED_WORK_VALUE, 'true');
			  window.sessionStorage.setItem('workValueTopResults', JSON.stringify(self.workValueSortedScores.slice(0,3)));
			  window.sessionStorage.removeItem('workValueUserSelections');
			  window.sessionStorage.removeItem('workValueActiveQuestion');
			  self._router.navigateByUrl('/work-values-test-results');
		  }, function(e){
			  console.error(e);
			  self.isDisabled = false;
			  /*var customModalOptions = {
						headerText : 'Error',
						bodyText : 'An error has occured, please try again.'
					};
					var customModalDefaults = {size : 'sm'};
					modalService.show(customModalDefaults, customModalOptions);*/
					
					const data = {
				            'title': 'Error',
				            'message': 'An error has occurred, please try again.'
				          };
				          const dialogRef = self._dialog.open(MessageDialogComponent, {
				            data: data
				          });
		  });
	}
	

	isTiedScore = function() {
		let index = 0;
		while(index < 3){
			if(this.workValueSortedScores[index].tied == true) {
				return true
			}
			index++;
		}
		return false;
		
	}
	
	
	public tiedScores = [];
	
	/**
	 * Setup tiedScores to use to display tied selections.
	 */
	setupTiedScores = function(workValueTiedResults) {

		var isNextScoreTied = false;
		var tiedScoreSet = [];
		for (var i = 0; i < workValueTiedResults.length; i++) {
			
			if ((i != workValueTiedResults.length - 1) && workValueTiedResults[i].Score == workValueTiedResults[i + 1].Score) {

				tiedScoreSet.push(workValueTiedResults[i]);
				workValueTiedResults[i].tied = true;
				isNextScoreTied = true;
				
			}else if (isNextScoreTied) {
				isNextScoreTied = false
				tiedScoreSet.push(workValueTiedResults[i]);
				workValueTiedResults[i].tied = true;
				this.tiedScores.push(tiedScoreSet);
				tiedScoreSet = [];
			} else {
				workValueTiedResults[i].tied = false;
			}
			
		}
	}
	
	/**
	 * If there are 5 or more ties for one particular score, alert user to give
	 * them option to retake test or proceed with selecting from their tied
	 * scores.
	 */
	maxAllowedTiesAlert = function() {
		for (var i = 0; i < this.tiedScores.length; i++) {
			if(this.tiedScores[i].length >= 5){
				var customModalOptions = {
						headerText : '',
						bodyText : ''
					};
					/*var customModalDefaults = {size : 'md', backdrop: 'static'};
					workValuesRetakeModal.show(customModalDefaults, customModalOptions);*/
				    this._dialog.open(WorkValuesRetakeDialogComponent);
					
					return true;
			}
		}
		return false;
	}

}
