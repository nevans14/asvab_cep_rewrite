import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkValuesTestComponent } from './work-values-test.component';

describe('WorkValuesTestComponent', () => {
  let component: WorkValuesTestComponent;
  let fixture: ComponentFixture<WorkValuesTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkValuesTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkValuesTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
