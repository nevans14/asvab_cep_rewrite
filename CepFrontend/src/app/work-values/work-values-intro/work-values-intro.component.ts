import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/services/utility.service';
@Component({
  selector: 'app-work-values-intro',
  templateUrl: './work-values-intro.component.html',
  styleUrls: ['./work-values-intro.component.scss']
})
export class WorkValuesIntroComponent implements OnInit {

  seoTitle = 'What are Work Values | ASVAB Career Exploration Program';
	metaDescription = 'Discover what are work values with this situational judgement activity. 16 scenarios help you determine work values that are important to you.';

  constructor(
    private _utility: UtilityService,
  ) { }

  ngOnInit() {
    this._utility.setSocialMeta(this.seoTitle, this.metaDescription);
  }

}
