import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkValuesIntroComponent } from './work-values-intro.component';

describe('WorkValuesIntroComponent', () => {
  let component: WorkValuesIntroComponent;
  let fixture: ComponentFixture<WorkValuesIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkValuesIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkValuesIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
