import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkValuesTestResultsComponent } from './work-values-test-results.component';

describe('WorkValuesTestResultsComponent', () => {
  let component: WorkValuesTestResultsComponent;
  let fixture: ComponentFixture<WorkValuesTestResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkValuesTestResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkValuesTestResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
