import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilityService } from 'app/services/utility.service';
@Component({
  selector: 'app-work-values-test-results',
  templateUrl: './work-values-test-results.component.html',
  styleUrls: ['./work-values-test-results.component.scss']
})
export class WorkValuesTestResultsComponent implements OnInit {

  public workValueTopResults;

  seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';
  
  constructor(
    private _router: Router,
    private _utility: UtilityService,
  ) { }

  ngOnInit() {
	  this.workValueTopResults = JSON.parse(window.sessionStorage.getItem('workValueTopResults'));
    this._utility.setSocialMeta(this.seoTitle, this.metaDescription);
  }
  
	
	retakeWorkValues = function(){
		this._router.navigateByUrl('/work-values-test');
	}
	

}
