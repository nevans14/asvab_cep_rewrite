import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Title, Meta } from '@angular/platform-browser';
import { UserService } from 'app/services/user.service';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { LearnMoreDialogComponent } from 'app/core/dialogs/learn-more-dialog/learn-more-dialog.component';
import { MediaCenterService } from 'app/services/media-center.service';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { LoginService } from 'app/services/login.service';
import { ConfigService } from 'app/services/config.service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import $ from 'jquery'
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { ScoreRequestComponent } from 'app/contact-us/score-request.component';

declare var $: $

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
    isAtHomepage = this.router.url === '/';
    status = {
        isopen: this.isAtHomepage ? true : false
    };
    // if the status is open and if the user is not logged in, open the login panel, else hide it
    toggleLogin = this.status.isopen && this._userService.getUser() ? "block" : "none";

    redirect: string = '';
 
    resources; // TODO:
    mediaCenterList;
    resourceBaseUrl;
    pdfBaseUrl;
    path;

    constructor(
        private _userService: UserService,
        private _dialog: MatDialog,
        private mediaCenterService: MediaCenterService,
        private router: Router,
        private route: ActivatedRoute,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _loginService: LoginService,
        private _config: ConfigService,
        private _meta: Meta,
        private _titleTag: Title,
    ) {
        // Used for testing new bring to school form
        // this.onBringAsvabToYourSchool();

        this.resourceBaseUrl = this._config.getImageUrl();
        this.pdfBaseUrl = this.resourceBaseUrl + 'CEP_PDF_Contents/';
        this.path = this.router.url;
    }

    ngOnInit() {
        this._loginService.getXSRFTokens();

        if (this.router.url.includes('score-request')) {
            this.openScoreRequestModal();
        }

        this.route.queryParams
            .subscribe(params => {
                this.redirect = params['redirect'];
                if (this.redirect) {
                    // had to open in setTimeout due to https://github.com/angular/components/issues/5268
                    setTimeout(() => this.showLoginModal());
                }
            });

        if (this.path === '/score-request') {
            this._dialog.open(ScoreRequestComponent , {
            });
        }

        if (this.path.includes('requestScore=submitted')) {
            this._dialog.open(ScoreRequestComponent , {
              data: {
                requestScoreResponse: window.sessionStorage.getItem("requestScoreResponse"),
              }
            });
      
            window.sessionStorage.removeItem("requestScoreResponse");
          }

        this.mediaCenterService.getMediaCenterList().then(
            data => {
                this.mediaCenterList = data;
            });

        $(window).scroll(function () {
            var HeaderHeight = $('#header').height();
            var ScrolledHeaderHeight = $('#header.scrolled').height();
            var HomePageSlides = $('#home #slides').height();

            if ($(window).scrollTop() >= HomePageSlides) {
                $('#mega-menu-buttons').addClass('mega-menu-sticky').css('top', HeaderHeight);
                if ($('#mega-menu-buttons').length > 0 && $('#mega-menu-buttons+.mega-menu-buttons-spacing').length == 0) {
                    $('#mega-menu-buttons').after('<div class="mega-menu-buttons-spacing"></div>');
                    $('#mega-menu-buttons+.mega-menu-buttons-spacing').css('height', $('#mega-menu-buttons').outerHeight());
                }
            } else {
                $('#mega-menu-buttons').removeClass('mega-menu-sticky').css('top', '');
                $('#mega-menu-buttons+.mega-menu-buttons-spacing').remove();
            }

            var headerLoginInterval = setInterval(function () {
                if ($("#home header nav .menu-right #header-login-container #header-login.button").length > 0) {
                    clearInterval(headerLoginInterval);
                    if ($(window).scrollTop() === 0) {
                        if ($('#header:not(.login) .dropdown-menu').css('display')
                            && $('#header:not(.login) .dropdown-menu').css('display').indexOf('none') > -1) {
                            $('header nav #header-login.dropdown-toggle').trigger('click')
                        }
                    } else {
                        if ($('#header:not(.login) .dropdown-menu').css('display')
                            && $('#header:not(.login) .dropdown-menu').css('display').indexOf('none') == -1) {
                            $('header nav #header-login.dropdown-toggle').trigger('click')
                        }
                    }
                }
            }, 10);
        });
        $(window).on("resize", function () {
            $(window).trigger('scroll');
        });

        const seoTitle = 'What career is right for me? | ASVAB Career Exploration Program';
        const metaDescription = 'Find what career is right for you with the ASVAB CEP career exploration & planning tools. Find career satisfaction & your dream job.';

        this._titleTag.setTitle(seoTitle);
        this._meta.updateTag({name: 'description', content: metaDescription});

        this._meta.updateTag({property: 'og:title', content: seoTitle});
        this._meta.updateTag({property: 'og:description', content: metaDescription});

        this._meta.updateTag({ name: 'twitter:title', content:  seoTitle});
        this._meta.updateTag({ name: 'twitter:description', content: metaDescription});
    }

    ngAfterViewInit() {
        let slideShowTimer;
        if (typeof slideShowTimer !== 'undefined') {
            /*$interval.cancel( slideShowTimer );*/
            clearInterval(slideShowTimer);
        }

        $('#slides .career').hide().eq(0).addClass('active').show();

        /*slideShowTimer = $interval( function() {
            rotateCareers();
        }, 5000 );*/

        slideShowTimer = setInterval(function () {
            rotateCareers();
        }, 5000);

        function rotateCareers() {
            var $current = $('#slides .career.active');
            var $next = $current.next();

            if (!$next.length)
                $next = $('#slides .career').eq(0);

            $current.removeClass('active').fadeOut();
            $next.addClass('active').fadeIn();
        };
    }

    onBringAsvabToYourSchool() {
        const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
            hasBackdrop: true,
            panelClass: 'new-modal',
            // maxHeight: '90vh',
            autoFocus: false,
            disableClose: true
        });
    }

    /**
     * Show video modal
     */
    showVideoDialog(videoName) {
        this._dialog.open(PopupPlayerComponent, {
            data: { videoId: videoName }
        });
    }

    onLearnMore() {
        const dialogRef2 = this._dialog.open(LearnMoreDialogComponent, {
        });
    }

    snapChatModal() {
        this._googleAnalyticsService.trackClick('#CONNECT_WITH_US_SNAPCHAT')

        var data = {
            title: 'Connect with us on SnapChat!',
            message: '<p class="text-center"><img style="width: 400px;" src="assets/images/option-ready-snap.png" alt=""/></p>'
        };

        const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: data,
            maxWidth: '500px'
        });
    }

    trackClick(path) {
        this._googleAnalyticsService.trackClick(`#${path}`);
    }

    showLoginModal() {
        this._dialog.open(LoginDialogComponent, {
            hasBackdrop: true,
            panelClass: 'new-modal',
            width: '300px',
            autoFocus: true,
            disableClose: true
        });
    }

    openScoreRequestModal() {
        const dialogRef2 = this._dialog.open(ScoreRequestComponent , {
        });
    }
    
}
