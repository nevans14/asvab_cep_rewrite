import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RegisterDialogComponent } from 'app/core/dialogs/register-dialog/register-dialog.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mentor-registration',
  templateUrl: '../home/home.component.html',
  styleUrls: ['./mentor-registration.component.scss']
})
export class MentorRegistrationComponent implements OnInit {

  constructor(private _dialog: MatDialog,
		  private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
	  let guid = this._activatedRoute.snapshot.paramMap.get('guid');
	  let mentorEmail = this._activatedRoute.snapshot.queryParamMap.get('email_address');
	  this._dialog.open(RegisterDialogComponent, {data: {isMentorRegistration : true, id: guid, mentorEmail : mentorEmail}});
  }

}
