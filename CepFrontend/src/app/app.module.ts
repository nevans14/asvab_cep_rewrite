import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { ShareButtonsConfig } from '@ngx-share/core';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { CompareOccupationComponent } from './compare-occupation/compare-occupation.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ScoreRequestComponent } from './contact-us/score-request.component';
import { CoreModule } from './core/core.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EducatorsComponent } from './educators/educators.component';
import { FyiInstructionComponent } from './find-your-interest/fyi-instruction/fyi-instruction.component';
import { FyiResultsComponent } from './find-your-interest/fyi-results/fyi-results.component';
import { FyiTestComponent } from './find-your-interest/fyi-test/fyi-test.component';
import { FyiTiedComponent } from './find-your-interest/fyi-tied/fyi-tied.component';
import { GeneralResourcesComponent } from './general-resources/general-resources.component';
import { GreenStemBrightComponent } from './green-stem-bright/green-stem-bright.component';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material.module';
import { MyAsvabSummaryResultsComponent } from './my-asvab-summary-results/my-asvab-summary-results.component';
import { EmploymentDetailsComponent } from './occufind/employment-details/employment-details.component';
import { EducationDetailsComponent } from './occufind/education-details/education-details.component';
import { MilitaryDetailsComponent } from './occufind/military-details/military-details.component';
import { OccufindOccupationDetailsNotLoggedInComponent } from './occufind/occupation/occufind-occupation-details-not-logged-in/occufind-occupation-details-not-logged-in.component';
import { SearchResultsComponent } from './occufind/occupation/search-results/search-results.component';
import { SchoolDetailsComponent } from './occufind/school-details/school-details.component';
import { ParentsComponent } from './parents/parents.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PortfolioDirectionsComponent } from './portfolio/portfolio-directions.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ConfigService } from './services/config.service';
import { ContactUsService } from './services/contact-us.service';
import { LoginService } from './services/login.service';
import { PortfolioService } from './services/portfolio.service';
import { ResourceService } from './services/resource.service';
import { UserService } from './services/user.service';
import { UtilityService }  from './services/utility.service';
import { EssTrainingService } from './services/ess-training.service';
import { SiteSearchResultsComponent } from './site-search/site-search-results/site-search-results.component';
import { StudentComponent } from './student/student.component';
import { OccupationDetailsComponent } from './occufind/occupation-details/occupation-details.component';
import { CareerClusterComponent } from './career-cluster/career-cluster.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { OccupationResultsComponent } from './career-cluster/occupation-results/occupation-results.component';
import { SsoLoginComponent } from './sso-login/sso-login.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { AsvabCepAtYourSchoolComponent } from './asvab-cep-at-your-school/asvab-cep-at-your-school.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { OccupationSearchResultsComponent } from './occufind/occupation-search-results/occupation-search-results.component';
import { PathwayComponent } from './career-cluster/pathway/pathway.component';
import { CareerClusterLeftNavComponent } from './career-cluster/pathway/career-cluster-left-nav/career-cluster-left-nav.component';
import { CareerClusterBannerMobileMenuComponent } from './career-cluster/pathway/career-cluster-banner-mobile-menu/career-cluster-banner-mobile-menu.component';
import { LearnAboutYourselfComponent } from './learn-about-yourself/learn-about-yourself.component';
import { ExploreCareersComponent } from './explore-careers/explore-careers.component';
import { PlanYourFutureComponent } from './plan-your-future/plan-your-future.component';
import { EssTrainingComponent } from './ess-training/ess-training.component';
import { OccupationSearchComponent } from './occufind/occupation-search/occupation-search.component';
import { ChartsModule } from 'ng2-charts';
import { MediaCenterComponent } from './media-center/media-center.component';
import { MediaCenterArticleComponent } from './media-center/media-center-article/media-center-article.component';
import { MediaCenterArticleListComponent } from './media-center/media-center-article-list/media-center-article-list.component';
import { MyAsvabSummaryResultsManualComponent } from './my-asvab-summary-results/my-asvab-summary-results-manual/my-asvab-summary-results-manual.component';
import { FavoritesRestFactoryService } from './services/favorites-rest-factory.service';
import { RatingModule  } from 'ng-starrating';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { CreatePlanComponent } from './create-plan/create-plan.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { ViewPlanComponent } from './view-plan/view-plan.component';
import { PlanResumeComponent } from './plan-resume/plan-resume.component';
import { PlanCalendarComponent } from './plan-calendar/plan-calendar.component';
import { ContinuingEducationComponent } from './educators/continuing-education/continuing-education/continuing-education.component';
import { GeneralHelpComponent } from './general-help/general-help.component';
import { CountdownComponent } from './countdown/countdown.component';
import { FyiScoreSharedComponent } from './find-your-interest/fyi-score-shared/fyi-score-shared.component';
import { OccufindSalaryDialogComponent } from './core/dialogs/occufind-salary-dialog/occufind-salary-dialog.component';
import { CollegePlanComponent } from './edit-plan/college-plan/college-plan.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { HighSchoolPlanComponent } from './edit-plan/high-school-plan/high-school-plan.component';
import { PlanItemCardComponent } from './plan-your-future/plan-item-card/plan-item-card.component';
import { LegislationComponent } from './legislation/legislation.component';
import { MilitaryPlanComponent } from './edit-plan/military-plan/military-plan.component';
import { UserMilitaryPlanComponent } from './edit-plan/military-plan/user-military-plan/user-military-plan.component';
import { WorkValuesIntroComponent } from './work-values/work-values-intro/work-values-intro.component';
import { WorkValuesTestComponent } from './work-values/work-values-test/work-values-test.component';
import { WorkValuesTestResultsComponent } from './work-values/work-values-test-results/work-values-test-results.component';
import { WorkValuesTiedScoresComponent } from './work-values/work-values-tied-scores/work-values-tied-scores.component';


import { WorkValuesApiService } from './services/work-values-api.service';
import { GapYearPlanComponent } from './edit-plan/gap-year-plan/gap-year-plan.component';
import { UserGapYearPlanComponent } from './edit-plan/gap-year-plan/user-gap-year-plan/user-gap-year-plan.component';
import { WorkPlanComponent } from './edit-plan/work-plan/work-plan.component';
import { ResumeBuilderPrintComponent } from './resume-builder-print/resume-builder-print.component';
import { GeneralLinkComponent } from './general-link/general-link.component';
import { GeneralPrivacySecurityComponent } from './general-privacy-security/general-privacy-security.component';
import { OptionReadyComponent } from './option-ready/option-ready.component';
import { CalendarPlanComponent } from './edit-plan/calendar-plan/calendar-plan.component';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
import { MentorStudentDetailComponent } from './mentor-student-detail/mentor-student-detail.component';
import { QuillModule } from 'ngx-quill';
import { PrintCareerPlanComponent } from './mentor-student-detail/print-career-plan/print-career-plan.component';
import { MentorSettingComponent } from './mentor-setting/mentor-setting.component';
import { PrintPortfolioComponent } from './mentor-student-detail/print-portfolio/print-portfolio.component';
import { PrintAsvabScoreComponent } from './mentor-student-detail/print-asvab-score/print-asvab-score.component';
import { MentorDashboardFiltersDialogComponent } from './mentor-dashboard/mentor-dashboard-filters-dialog/mentor-dashboard-filters-dialog.component';
import { MentorRegistrationComponent } from './mentor-registration/mentor-registration.component';
import { GeneralSiteComponent } from './general-site/general-site.component';
import { GeneralOccufindViewAllComponent } from './occufind/general-occufind-view-all/general-occufind-view-all.component';

// Moved to export function as lambda's are not supported inside useFactory ref: https://gist.github.com/fernandohu/122e88c3bcd210bbe41c608c36306db9
export function initConfig(config: ConfigService){
  return () => config.loadConfigurationData()
}

const customConfig: ShareButtonsConfig = {
  twitterAccount: 'CareersintheMil'
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactUsComponent,
    ScoreRequestComponent,
    ParentsComponent,
    EducatorsComponent,
    StudentComponent,
    GeneralResourcesComponent,
    DashboardComponent,
    PortfolioComponent, PortfolioDirectionsComponent,
    OccufindOccupationDetailsNotLoggedInComponent,
    SearchResultsComponent,
    SchoolDetailsComponent,
    EmploymentDetailsComponent,
    EducationDetailsComponent,
    MilitaryDetailsComponent,
    GreenStemBrightComponent,
    FyiTestComponent,
    FyiResultsComponent,
    FyiInstructionComponent,
    FyiTiedComponent,
    MyAsvabSummaryResultsComponent,
    PasswordResetComponent,
    CompareOccupationComponent,
    SiteSearchResultsComponent,
    OccupationDetailsComponent,
    CareerClusterComponent,
    OccupationResultsComponent,
    SsoLoginComponent,
    AsvabCepAtYourSchoolComponent,
    FavoritesComponent,
    OccupationSearchResultsComponent,
    PathwayComponent,
    CareerClusterLeftNavComponent,
    CareerClusterBannerMobileMenuComponent,
    LearnAboutYourselfComponent,
    ExploreCareersComponent,
    PlanYourFutureComponent,
    EssTrainingComponent,
    OccupationSearchComponent,
    MediaCenterComponent,
    MediaCenterArticleComponent,
    MediaCenterArticleListComponent,
    MyAsvabSummaryResultsManualComponent,
    StudentProfileComponent,
    CreatePlanComponent,
    EditPlanComponent,
    ViewPlanComponent,
    PlanResumeComponent,
    PlanCalendarComponent,
    ContinuingEducationComponent,
    GeneralHelpComponent,
    CountdownComponent,
    FyiScoreSharedComponent,
    OccufindSalaryDialogComponent,
    CollegePlanComponent,
    PlanItemCardComponent,
    MilitaryPlanComponent,
    HighSchoolPlanComponent,
    PlanItemCardComponent,
    UserMilitaryPlanComponent,
    LegislationComponent,
    PlanItemCardComponent,
    WorkValuesIntroComponent,
    WorkValuesTestComponent,
    WorkValuesTestResultsComponent,
    WorkValuesTiedScoresComponent,
    GapYearPlanComponent,
    UserGapYearPlanComponent,
    WorkPlanComponent,
    ResumeBuilderPrintComponent,
    GeneralLinkComponent,
    GeneralPrivacySecurityComponent,
    OptionReadyComponent,
    CalendarPlanComponent,
    MentorDashboardComponent,
    MentorStudentDetailComponent,
    PrintCareerPlanComponent,
    MentorSettingComponent,
    PrintPortfolioComponent,
    PrintAsvabScoreComponent,
    MentorDashboardFiltersDialogComponent,
    MentorRegistrationComponent,
    GeneralSiteComponent,
    GeneralOccufindViewAllComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxCaptchaModule,
    NgxSpinnerModule,
    SlickCarouselModule,
    CarouselModule,
    ShareButtonsModule.withConfig(customConfig),
    FormsModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule,
    MatFormFieldModule,
    ChartsModule,
    RatingModule,
    CurrencyMaskModule,
    QuillModule.forRoot({
      modules: {
        toolbar: [['bold', 'italic', 'underline'], [{ 'list': 'ordered'}, { 'list': 'bullet' }], ['clean']]
      }
    })
  ],
  exports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule
  ],
  providers: [
    DatePipe,
    CookieService,
    UserService,
    UtilityService,
    EssTrainingService,
    LoginService,
    ResourceService,
    ContactUsService,
    PortfolioService,
    FavoritesRestFactoryService,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [ConfigService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    { provide: Window, useValue: window },
    WorkValuesApiService
  ],
  entryComponents: [
    ContactUsComponent,
    ScoreRequestComponent,
    StudentComponent,
    OccufindSalaryDialogComponent,
    PrintCareerPlanComponent,
    PrintPortfolioComponent,
    PrintAsvabScoreComponent,
    MentorDashboardFiltersDialogComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
