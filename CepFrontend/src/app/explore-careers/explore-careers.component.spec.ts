import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreCareersComponent } from './explore-careers.component';

describe('ExploreCareersComponent', () => {
  let component: ExploreCareersComponent;
  let fixture: ComponentFixture<ExploreCareersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreCareersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreCareersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
