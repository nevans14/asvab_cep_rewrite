import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { UtilityService }  from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';
import { LearnMoreExploreDialogComponent } from 'app/core/dialogs/learn-more-explore-dialog/learn-more-explore-dialog.component';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { CareerClusterRestFactoryService } from 'app/career-cluster/services/career-cluster-rest-factory.service';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-explore-careers',
  templateUrl: './explore-careers.component.html',
  styleUrls: ['./explore-careers.component.scss']
})
export class ExploreCareersComponent implements OnInit {

  userCompletion;
  favoriteList = [];
  careerClusterFavoriteList = [];
  schoolFavoriteList = [];
  favoriteLength;
  myStyle = { 'cursor': 'pointer' };
  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  favoriteOccupation1 = true;
  favoriteOccupation2 = true;
  favoriteOccupation3 = true;
  favoriteOccupation4 = true;
  favoriteOccupation5 = true;
  favoriteOccupation6 = true;
  favoriteOccupation7 = true;
  completedOccupationSearch = false;  
  viewedCareerDetail = false;
  completedOccupationSearchClosedHint = false;
  viewedCareerDetailClosedHint = false;
  favoritedOccupationClosedHint = false;
  createdPlanClosedHint = false;
  completedWorkValues = false;
  completedWorkValuesClosedHint = false;
  userOccupationPlans: UserOccupationPlan[];

  // Guided Tour Tips
  ccOccupationsAll: any;
  showFavCCTip: boolean = false;
  showFavCCTipClosedHint: boolean = false;
  guidedTipConfig;
  completedMoreAboutMe = false;
  completedMoreAboutMeClosedHint = false;
  favoritedCollegeClosedHint = false;
  compareMoreThanTwoFavCareersClosedHint = false;
  visitedNotes = false;
  visitedNotesClosedHint = false;

  // Comment out plus item since don't need to show hint now
  // plusItemIndex: number = 0;
  // visitedResourceClosedHint = false;
  // favoritedOccupationAdditional = false;
  // visitedOccupationDetailLink = false;
  // favoritedCollege = false;
  // toCTMFromOccuDetail = false;
  // comparedOccupations = false;
  // visitedCC = false;
  // visitedNotes = false;
  // refinedOccufindSearch = false;
  // favoritedCC = false;

  constructor(
    private _user: UserService,
    private _utility: UtilityService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _favoriteService: FavoritesRestFactoryService,
    private _configService: ConfigService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _ccRestFactoryService: CareerClusterRestFactoryService,
  ) { }

  ngOnInit() {
     // favorites data resolver
     let favoritesResolveArray = this._activatedRoute.snapshot.data.favoritesResolver;
     this.favoriteList = favoritesResolveArray[0];
     this.careerClusterFavoriteList = favoritesResolveArray[1];
     this.schoolFavoriteList = favoritesResolveArray[2];
     this.schoolFavoriteList = this.schoolFavoriteList.filter(s => !!s.schoolName)
     this.favoriteLength = this.favoriteList.length

     if (window.innerWidth < 640) {
        this.leftNavCollapsed = this._user.getLeftNavCollapsed();
      }

     // reset occupations selected
     for ( let i = 0; i < this.favoriteLength; i++ ) {
         this.favoriteList[i].selected = false;
     }

    this._user.getCompletion().subscribe(data => {
      this.userCompletion = data;

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_OCCUPATION_SEARCH)) {
        this.completedOccupationSearch = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT)) {
        this.completedOccupationSearchClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_CAREER_DETAIL)) {
        this.viewedCareerDetail = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VIEWED_CAREER_DETAIL_CLOSED_HINT)) {
        this.viewedCareerDetailClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION_CLOSED_HINT)) {
        this.favoritedOccupationClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CREATED_PLAN_CLOSED_HINT)) {
        this.createdPlanClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_WORK_VALUE)) {
        this.completedWorkValues = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPLETED_WORK_VALUE_CLOSED_HINT)) {
        this.completedWorkValuesClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAV_CAREERS_SAME_CC_CLOSED_HINT)) {
        this.showFavCCTipClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.LIKE_DISLIKE_MORE_ABOUT_ME)) {
        this.completedMoreAboutMe = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT)) {
        this.completedMoreAboutMeClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAV_COLLEGE_CLOSED_HINT)) {
        this.favoritedCollegeClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.COMPARE_MORE_THAN_TWO_FAV_CAREERS_CLOSED_HINT)) {
        this.compareMoreThanTwoFavCareersClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_NOTES)) {
        this.visitedNotes = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_NOTES_CLOSED_HINT)) {
        this.visitedNotesClosedHint = true;
      }

      const guidedTipConfigs = this._user.getCompletionByType(this.userCompletion, UserService.GUIDED_TIP_CONFIG);
      this.guidedTipConfig = JSON.parse(guidedTipConfigs.value);
      this.setShowFavCCTip();

      // this.checkPlusItemToShow();
    });

    this._userOccupationPlanService.getAll().then((plans: UserOccupationPlan[]) => {
      this.userOccupationPlans = plans;
    });  
  }

  setShowFavCCTip() {
    if (this.favoriteList.length >= 2) {

      let ccOccupationsList = {};
      for(let i = 0; i< 16; ++i) {
        ccOccupationsList[i] = this._ccRestFactoryService.getCareerClusterOccupations(i+1);
      }

      forkJoin(ccOccupationsList).subscribe(ccOccupations => {
        this.ccOccupationsAll = ccOccupations;

        let occupationsFound = 0;
        Object.values(this.ccOccupationsAll).forEach((ccOccupSet: any) => {
          if (occupationsFound < 2) {
            occupationsFound = 0;
            ccOccupSet.forEach(ccOccup => {
              let foundFav = this.favoriteList.some(
                favOccup => favOccup.onetSocCd === ccOccup.onetSoc
              )
              if (foundFav) {
                ++occupationsFound;
              }
            })
          }
        })

        if (occupationsFound >= 2
          && this.guidedTipConfig.helpLearn
          && !this.showFavCCTipClosedHint
          ) {
          this.showFavCCTip = true;
        }
      })
    }
  }

  closedMoreAboutMeHint() {
    this.completedMoreAboutMeClosedHint = true;
    this._user.setCompletion(UserService.LIKE_DISLIKE_MORE_ABOUT_ME_CLOSED_HINT, 'true');
  }

  closedFavCollegeHint() {
    this.favoritedCollegeClosedHint = true;
    this._user.setCompletion(UserService.FAV_COLLEGE_CLOSED_HINT, 'true');
  }

  closedCompareMoreThanTwoFavCareersHint() {
    this.compareMoreThanTwoFavCareersClosedHint = true;
    this._user.setCompletion(UserService.COMPARE_MORE_THAN_TWO_FAV_CAREERS_CLOSED_HINT, 'true');
  }

  closedVisitNotesHint() {
    this.visitedNotesClosedHint = true;
    this._user.setCompletion(UserService.VISITED_NOTES_CLOSED_HINT, 'true');
  }
  

  // checkPlusItemToShow() {
  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION_ADDITIONAL)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION_ADDITIONAL_CLOSED_HINT)) { 
  //       this.plusItemIndex = 1;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_OCCUPATION_DETAIL_LINK)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT)) { 
  //       this.plusItemIndex = 2;
  //       return;
  //     }
    
  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_COLLEGE)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_COLLEGE_CLOSED_HINT)) { 
  //       this.plusItemIndex = 3;
  //       return;
  //     }
    
  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.TO_CTM_FROM_OCCU_DETAIL)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.TO_CTM_FROM_OCCU_DETAIL_CLOSED_HINT)) { 
  //       this.plusItemIndex = 4;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.COMPARED_OCCUPATIONS)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.COMPARED_OCCUPATIONS_CLOSED_HINT)) { 
  //       this.plusItemIndex = 5;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_CAREER_CLUSTER)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_CAREER_CLUSTER_CLOSED_HINT)) { 
  //       this.plusItemIndex = 6;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_NOTES)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_NOTES_CLOSED_HINT)) { 
  //       this.plusItemIndex = 7;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.REFINED_OCCUFIND_SEARCH)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.REFINED_OCCUFIND_SEARCH_CLOSED_HINT)) { 
  //       this.plusItemIndex = 8;
  //       return;
  //     }

  //   if (!this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_CC)
  //     && !this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_CC_CLOSED_HINT)) { 
  //       this.plusItemIndex = 9;
  //       return;
  //     }
  // }

  // showPlusItemsNoFavorites() {
  //   return (this.viewedCareerDetail || this.viewedCareerDetailClosedHint)
  //     && this.favoriteLength === 0 && this.favoritedOccupationClosedHint && (this.completedWorkValues || this.completedWorkValuesClosedHint)
  //     && this.plusItemIndex <= 9
  // }

  // showPlusItemsWithFavorites() {
  //   return (this.viewedCareerDetail || this.viewedCareerDetailClosedHint)
  //     && this.favoriteLength > 0 && this.userOccupationPlans && this.userOccupationPlans.length > 0 && (this.completedWorkValues || this.completedWorkValuesClosedHint)
  //     && this.plusItemIndex <= 9
  // }

  // closedPlusItem() {
  //   switch (this.plusItemIndex) {
  //     case 0:
  //       this.visitedResourceClosedHint = true;
  //       this._user.setCompletion(UserService.VISITED_RESOURCE_CLOSED_HINT, 'true');
  //       break;
  //     case 1:
  //       this.favoritedOccupationAdditional = true;
  //       this._user.setCompletion(UserService.FAVORITED_OCCUPATION_ADDITIONAL_CLOSED_HINT, 'true');
  //       break;
  //     case 2:
  //       this.visitedOccupationDetailLink = true;
  //       this._user.setCompletion(UserService.VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT, 'true');
  //       break;
  //     case 3:
  //       this.favoritedCollege = true;
  //       this._user.setCompletion(UserService.FAVORITED_COLLEGE_CLOSED_HINT, 'true');
  //       break;
  //     case 4:
  //       this.toCTMFromOccuDetail = true;
  //       this._user.setCompletion(UserService.TO_CTM_FROM_OCCU_DETAIL_CLOSED_HINT, 'true');
  //       break;
  //     case 5:
  //       this.comparedOccupations = true;
  //       this._user.setCompletion(UserService.COMPARED_OCCUPATIONS_CLOSED_HINT, 'true');
  //       break;
  //     case 6:
  //       this.visitedCC = true;
  //       this._user.setCompletion(UserService.VISITED_CAREER_CLUSTER_CLOSED_HINT, 'true');
  //       break;
  //     case 7:
  //       this.visitedNotes = true;
  //       this._user.setCompletion(UserService.VISITED_NOTES_CLOSED_HINT, 'true');
  //       break;
  //     case 8:
  //       this.refinedOccufindSearch = true;
  //       this._user.setCompletion(UserService.VISITED_NOTES_CLOSED_HINT, 'true');
  //       break;
  //     case 9:
  //       this.favoritedCC = true;
  //       this._user.setCompletion(UserService.FAVORITED_CC_CLOSED_HINT, 'true');
  //       break;
  //   }
  //   this.plusItemIndex++;
  // }

  closedOccupationSearchHint() {
    this.completedOccupationSearchClosedHint = true;
    this._user.setCompletion(UserService.COMPLETED_OCCUPATION_SEARCH_CLOSED_HINT, 'true');
  }

  closedViewDetailHint() {
    this.viewedCareerDetailClosedHint = true;
    this._user.setCompletion(UserService.VIEWED_CAREER_DETAIL_CLOSED_HINT, 'true');
  }

  closedFavoritedOccupationHint() {
    this.favoritedOccupationClosedHint = true;
    this._user.setCompletion(UserService.FAVORITED_OCCUPATION_CLOSED_HINT, 'true');
  }

  closedCreatePlanHint() {
    this.createdPlanClosedHint = true;
    this._user.setCompletion(UserService.CREATED_PLAN_CLOSED_HINT, 'true');
  }

  closedWorkValueHint() {
    this.completedWorkValuesClosedHint = true;
    this._user.setCompletion(UserService.COMPLETED_WORK_VALUE_CLOSED_HINT, 'true');
  }

  closedShowFavCCHint() {
    this.showFavCCTip = false;
    this.showFavCCTipClosedHint = true;
    this._user.setCompletion(UserService.FAV_CAREERS_SAME_CC_CLOSED_HINT, 'true');
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  setOccupationHeart(numHearts, favId) {
    this.favoriteList.filter(f => {
      if (f.id === favId) {
        f['hearts'] = numHearts;
        this._favoriteService.updateFavoriteOccupationHearts({id: favId, hearts: numHearts});
      }
    });
  }

  // remove favorites using user factory service
  removeFavorites( id ) {
    this.myStyle = { 'cursor': 'wait' };

    let promise = this._user.deleteFavorite( id );
    promise.then(( response: any ) => {
        this.favoriteList = response;
        this.myStyle = { 'cursor': 'pointer' };
    }, ( reason ) => {
        this.myStyle = { 'cursor': 'pointer' };

        for ( let i = 0; i < this.favoriteList.length; i++ ) {
            if ( this.favoriteList[i].id = id ) {
                this.favoriteList[i].disabled = false;
            }
        }

        // inform user that there was an error and to try again.
        console.error( reason );
        const data = {
            title: 'Error',
            message: 'There was an error proccessing your request. Please try again.'
        };

        this._dialog.open( MessageDialogComponent, {
            data: data,
            maxWidth: '600px'
        });
    })
  }

  /**
   * Tracks how many selections are made to limit user selection to two
   */
  checked = 0;
  checkChanged = function( item ) {
      if ( item.selected )
          this.checked++;
      else
          this.checked--;
  }

  compareSelection = function() {
      let occupationSelected = [];

      for ( let i = 0; i < this.favoriteLength; i++ ) {

          if ( this.favoriteList[i].selected == true ) {
              occupationSelected.push( this.favoriteList[i].onetSocCd );
          }
      }

      if ( occupationSelected.length < 2 ) {
          const data = {
              title: 'Favorites',
              message: 'Select at least 2 occupations.'
          };

          this._dialog.open( MessageDialogComponent, {
              data: data,
              maxWidth: '600px'
          } );
          return false;
      }

      const selectedLen = occupationSelected.length;
      if (selectedLen < 10) {
        for(let i = 0; i < 10 - selectedLen; ++i) {
          occupationSelected.push('null')
        }
      }
      
      // this._configService.setCompareOccupationRoutes(occupationSelected);
      this._router.navigate(['/compare-occupation/' +  occupationSelected.join('/')]);
  }

  // remove career cluster favorites using user factory service
  removeCareerClusterFavorites( id ) {
      this.myStyle = { 'cursor': 'wait' };

      let promise = this._user.deleteCareerClusterFavorite( id );
      promise.then(( response: any ) => {
          this.careerClusterFavoriteList = response;
          this.myStyle = { 'cursor': 'pointer' };
      }, ( reason ) => {
          this.myStyle = { 'cursor': 'pointer' };

          for ( var i = 0; i < this.careerClusterFavoriteList.length; i++ ) {
              if ( this.careerClusterFavoriteList[i].id = id ) {
                  this.careerClusterFavoriteList[i].disabled = false;
              }
          }

          // inform user that there was an error and to try again.
          console.error( reason );
          const data = {
              title: 'Error',
              message: 'There was an error proccessing your request. Please try again.'
          };

          this._dialog.open( MessageDialogComponent, {
              data: data,
              maxWidth: '600px'
          });
      })
  }

  // remove school favorites using user factory service
  removeSchoolFavorites( id ) {
      this.myStyle = { 'cursor': 'wait' };

      let promise = this._user.deleteSchoolFavorite( id );
      promise.then(( response: any ) => {
          this.schoolFavoriteList = response;
          this.myStyle = { 'cursor': 'pointer' };
      }, ( reason ) => {
          this.myStyle = { 'cursor': 'pointer' };

          for ( let i = 0; i < this.schoolFavoriteList.length; i++ ) {
              if ( this.schoolFavoriteList[i].id = id ) {
                  this.schoolFavoriteList[i].disabled = false;
              }
          }

          // inform user that there was an error and to try again.
          console.error( reason );
          const data = {
              title: 'Error',
              message: 'There was an error proccessing your request. Please try again.'
          };

          this._dialog.open( MessageDialogComponent, {
              data: data,
              maxWidth: '600px'
          });
      })
  }

  showNotes(title, socId) {
    this._dialog.open(NotesDialogComponent, {
      data: {
        title: title,
        socId: socId
      },
      maxWidth: '900px',
    });
  }
  
  showNotesSchool(title, unitId) {
    this._dialog.open(NotesDialogComponent, {
      data: {
        schoolName: title,
        unitId: unitId
      },
      maxWidth: '900px',
    });
  }
  
  showNotesCC(title, ccId) {
    this._dialog.open(NotesDialogComponent, {
      data: {
        ccTitle: title,
        ccId: ccId
      },
      maxWidth: '900px',
    });
	}

  learnHow(modalName) {
    const data = {
      'modalName': modalName,
    };
    const dialogRef = this._dialog.open(LearnMoreExploreDialogComponent, {
      data: data,
      hasBackdrop: true,
      maxWidth: modalName.toLowerCase() === 'favorite-college' ? '600px' : '750px'
    });
  }
}
