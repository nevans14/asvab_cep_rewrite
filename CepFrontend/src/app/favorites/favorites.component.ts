import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UserService } from 'app/services/user.service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';

@Component( {
    selector: 'app-favorites',
    templateUrl: './favorites.component.html',
    styleUrls: ['./favorites.component.scss']
} )
export class FavoritesComponent implements OnInit {

    public favoriteList = [];
    public careerClusterFavoriteList = [];
    public schoolFavoriteList = [];
    public favoriteLength;
    public myStyle = { 'cursor': 'pointer' };
    loggedIn = false;

    constructor( private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _user: UserService,
        private _dialog: MatDialog ) { }

    ngOnInit() {

        // favorites data resolver
        let favoritesResolveArray = this._activatedRoute.snapshot.data.favoritesResolver;
        this.favoriteList = favoritesResolveArray[0];
        this.careerClusterFavoriteList = favoritesResolveArray[1];
        this.schoolFavoriteList = favoritesResolveArray[2];
        this.favoriteLength = this.favoriteList.length

        // reset occupations selected
        for ( let i = 0; i < this.favoriteLength; i++ ) {
            this.favoriteList[i].selected = false;
        }

        this.loggedIn = this._user.isLoggedIn();

        //scoreSummary = scoreSummary[0].data; 

        //haveAsvabTestScores = (angular.isObject(this.scoreSummary) && this.scoreSummary.hasOwnProperty('verbalAbility'));
    }



    // remove favorites using user factory service
    removeFavorites( id ) {
        this.myStyle = { 'cursor': 'wait' };

        let promise = this._user.deleteFavorite( id );
        promise.then(( response: any ) => {
            this.favoriteList = response;
            this.myStyle = { 'cursor': 'pointer' };
        }, ( reason ) => {
            this.myStyle = { 'cursor': 'pointer' };

            for ( let i = 0; i < this.favoriteList.length; i++ ) {
                if ( this.favoriteList[i].id = id ) {
                    this.favoriteList[i].disabled = false;
                }
            }

            // inform user that there was an error and to try again.
            console.error( reason );
            const data = {
                title: 'Error',
                message: 'There was an error proccessing your request. Please try again.'
            };

            this._dialog.open( MessageDialogComponent, {
                data: data,
                maxWidth: '600px'
            } );
        } )
    }

    /**
     * Tracks how many selections are made to limit user selection to two
     */
    limit = 3;
    checked = 0;
    checkChanged = function( item ) {
        if ( item.selected )
            this.checked++;
        else
            this.checked--;
    }

    compareSelection = function() {
        let occupationSelected = [];

        for ( let i = 0; i < this.favoriteLength; i++ ) {

            if ( this.favoriteList[i].selected == true ) {
                occupationSelected.push( this.favoriteList[i].onetSocCd );
            }
        }

        if ( occupationSelected.length < 2 ) {
            const data = {
                title: 'Favorites',
                message: 'Select at least 2 occupations.'
            };

            this._dialog.open( MessageDialogComponent, {
                data: data,
                maxWidth: '600px'
            } );
            return false;
        }

        this._router.navigate( ['/compare-occupation/' + occupationSelected[0] + '/' + occupationSelected[1] + '/' + occupationSelected[2]] );
    }

    // remove career cluster favorites using user factory service
    removeCareerClusterFavorites( id ) {
        this.myStyle = { 'cursor': 'wait' };

        let promise = this._user.deleteCareerClusterFavorite( id );
        promise.then(( response: any ) => {
            this.careerClusterFavoriteList = response;
            this.myStyle = { 'cursor': 'pointer' };
        }, ( reason ) => {
            this.myStyle = { 'cursor': 'pointer' };

            for ( var i = 0; i < this.careerClusterFavoriteList.length; i++ ) {
                if ( this.careerClusterFavoriteList[i].id = id ) {
                    this.careerClusterFavoriteList[i].disabled = false;
                }
            }

            // inform user that there was an error and to try again.
            console.error( reason );
            const data = {
                title: 'Error',
                message: 'There was an error proccessing your request. Please try again.'
            };

            this._dialog.open( MessageDialogComponent, {
                data: data,
                maxWidth: '600px'
            } );
        } )
    }

    // remove school favorites using user factory service
    removeSchoolFavorites( id ) {
        this.myStyle = { 'cursor': 'wait' };

        let promise = this._user.deleteSchoolFavorite( id );
        promise.then(( response: any ) => {
            this.schoolFavoriteList = response;
            this.myStyle = { 'cursor': 'pointer' };
        }, ( reason ) => {
            this.myStyle = { 'cursor': 'pointer' };

            for ( let i = 0; i < this.schoolFavoriteList.length; i++ ) {
                if ( this.schoolFavoriteList[i].id = id ) {
                    this.schoolFavoriteList[i].disabled = false;
                }
            }

            // inform user that there was an error and to try again.
            console.error( reason );
            const data = {
                title: 'Error',
                message: 'There was an error proccessing your request. Please try again.'
            };

            this._dialog.open( MessageDialogComponent, {
                data: data,
                maxWidth: '600px'
            } );
        } )
    }


}
