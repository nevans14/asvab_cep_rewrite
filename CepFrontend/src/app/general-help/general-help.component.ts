import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentManagementService } from '../services/content-management.service';
import $ from 'jquery'
declare var $: $

@Component({
  selector: 'app-general-help',
  templateUrl: './general-help.component.html',
  styleUrls: ['./general-help.component.scss']
})
export class GeneralHelpComponent implements OnInit {

  pageHtml = [];
  path;

  constructor(
    private _contentManagementService: ContentManagementService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  redirect(path) {
    this._router.navigate([path]);
  }
}
