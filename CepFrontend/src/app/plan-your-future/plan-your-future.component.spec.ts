import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanYourFutureComponent } from './plan-your-future.component';

describe('PlanYourFutureComponent', () => {
  let component: PlanYourFutureComponent;
  let fixture: ComponentFixture<PlanYourFutureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanYourFutureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanYourFutureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
