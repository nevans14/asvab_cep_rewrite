import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { StudentNotificationDialogComponent } from 'app/core/dialogs/student-notification-dialog/student-notification-dialog.component';
import { SubmitToMentorDialogComponent } from 'app/core/dialogs/submit-to-mentor-dialog/submit-to-mentor-dialog.component';
import { TaskDialogComponent } from 'app/core/dialogs/task-dialog/task-dialog.component';
import { UserOccupationPlan, UserOccupationPlanComment, UserOccupationPlanLog } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-plan-item-card',
  templateUrl: './plan-item-card.component.html',
  styleUrls: ['./plan-item-card.component.scss']
})
export class PlanItemCardComponent implements OnInit {

  @Input() userOccupationPlan: any;
  @Output() onRemoval: EventEmitter<number>;
  @Output() onAddTask: EventEmitter<UserOccupationPlanCalendarTask>;

  private isLoading: boolean;
  private occupationDescription: string;
  public occupationPlanCalendars: UserOccupationPlanCalendar[];
  public unassignedPlanTasks: UserOccupationPlanCalendarTask[];
  public closestUpcomingDate: UserOccupationPlanCalendar;
  public logs: UserOccupationPlanLog[];
  public comments: UserOccupationPlanComment[];
  public unreadMsgCount: number;
  public userRole;
  public occupationPathwayIcons: any[];

  constructor(private _occufindRestFactory: OccufindRestFactoryService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _planCalendarService: PlanCalendarService,
    private _matDialog: MatDialog,
    private _utilityService: UtilityService,
    private _userService: UserService) {
    this.isLoading = true;
    this.onRemoval = new EventEmitter();
    this.onAddTask = new EventEmitter();
    this.unreadMsgCount = 0;
    this.occupationPathwayIcons = [];
  }

  ngOnInit() {

    //retrieve current user and user role
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    this.userRole = currentUser ? currentUser.role : null;

    this.occupationDescription = this.userOccupationPlan.title;
    this.occupationPlanCalendars = this.userOccupationPlan.calendarDates;
    this.logs = this.userOccupationPlan.logs;

    //get unassigned tasks for plan
    let userOccupationPlanCalendarTasks = this.getUserOccupationPlanCalendarTasks(this.userOccupationPlan.id);

    //get comments for plan
    let comments = this._userOccupationPlanService.getComments(this.userOccupationPlan.id);

    Promise.all([comments, userOccupationPlanCalendarTasks]).then((done: any) => {

      this.comments = done[0];
      this.unassignedPlanTasks = done[1];

      if (this.occupationPlanCalendars && this.occupationPlanCalendars.length > 0) {
        this.closestUpcomingDate = this._utilityService.getClosestDate(this.occupationPlanCalendars, new Date());
      }

      //get unread count
      if (this.userRole == 'S') {
        this.unreadMsgCount = this.getUnreadCount(this.logs, this.comments);
      } else {
        this.unreadMsgCount = 2;
      }

      this.occupationPathwayIcons = this.getPathwayIcons(this.userOccupationPlan);

      this.isLoading = false;

    });

  }

  getPathwayIcons(userOccupationPlan: any) {
    let pathwaysArr = [];
    if (userOccupationPlan.pathway) {
      let pathways = JSON.parse(userOccupationPlan.pathway);

      // Cert: certificate, technical school, or associated degree, Vocational training (that show 2 year card, right)
      if (pathways.certificate || pathways.technicalSchool || pathways.associatesDegree || pathways.vocationalTraining) {
        pathwaysArr.push("assets/images/pathway/Plan_icon_cert.png");
      }

      // College: bachelor, or advanced degree, career and technical education ( this show both 2 year and 4 year colleges, right)
      if (pathways.bachelorsDegree || pathways.advanceDegree || pathways.careerTechnicalEducation) {
        pathwaysArr.push("assets/images/pathway/Plan_icon_college.png");
      }

      // Military: military
      if (pathways.military) {
        pathwaysArr.push("assets/images/pathway/Plan_icon_military.png");
      }

      // Gap year: gap year
      if (pathways.gapYear) {
        pathwaysArr.push("assets/images/pathway/Plan_icon_gapyear.png");
      }

      // Work: work based learning, vocational training, or career and technical education
      if (pathways.workBasedLearning || pathways.vocationalTraining || pathways.careerTechnicalEducation) {
        pathwaysArr.push("assets/images/pathway/Plan_icon_work.png");
      }

      return pathwaysArr;

    } else {
      return [];
    }

  }
  getUnreadCount(logs: UserOccupationPlanLog[], comments: UserOccupationPlanComment[]) {

    let unreadCount = 0;

    if (logs && logs.length > 0) {

      let sortedLogs = logs.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
        return da - db;
      });

      let lastLog = sortedLogs[sortedLogs.length - 1];
      if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
        unreadCount += 1;
      }
    }

    if (comments && comments.length > 0) {
      comments.forEach((comment: UserOccupationPlanComment) => {
        if (comment.from != 'You') {
          unreadCount = (comment.to.filter(x => !x.hasBeenRead).length > 0) ? unreadCount + 1 : unreadCount;
        }
      })
    }

    return unreadCount;
  }

  removeCard(planId, occupationDescription) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Plan for ' + occupationDescription + ', Are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '400px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._userOccupationPlanService.delete(planId).then((rowsAffected: number) => {
            //remove record from ui
            this.onRemoval.emit(planId);
          }).catch(error => {
            console.error("Error:", error);
          });

        }
      }
    });

  }

  addTask() {

    let data = {
      planId: this.userOccupationPlan.id,
      canEditPlan: false,
      calendarId: (this.closestUpcomingDate) ? this.closestUpcomingDate.id : null
    }

    const dialogRef = this._matDialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false })

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        if (userOccupationPlanCalendarTask.calendarId) {

          let index = this.occupationPlanCalendars.findIndex(x => x.id == userOccupationPlanCalendarTask.calendarId);

          if (this.occupationPlanCalendars[index]) {
            this.occupationPlanCalendars[index].tasks.push(userOccupationPlanCalendarTask);
          }

        } else {
          this.unassignedPlanTasks.push(userOccupationPlanCalendarTask);
        }

        //emit to update calendar on plan-your-future
        this.onAddTask.emit(userOccupationPlanCalendarTask);

      }
    });

  }

  async getUserOccupationPlanCalendar(planId) {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getPlanCalendars(planId);

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  /** 
 * Compares two Date objects and returns e number value that represents 
 * the result:
 * 0 if the two dates are equal.
 * 1 if the first date is greater than second.
 * -1 if the first date is less than second.
 * @param date1 First date object to compare.
 * @param date2 Second date object to compare.
 */
  public compareDate(date1: Date, date2: Date): number {
    // With Date object we can compare dates them using the >, <, <= or >=.
    // The ==, !=, ===, and !== operators require to use date.getTime(),
    // so we need to create a new instance of Date with 'new Date()'
    let d1 = new Date(date1); let d2 = new Date(date2);

    // Check if the dates are equal
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;

    // Check if the first is greater than second
    if (d1 > d2) return 1;

    // Check if the first is less than second
    if (d1 < d2) return -1;
  }

  getProgressBarValueForCalendar(calendar: UserOccupationPlanCalendar) {
    if (calendar.tasks) {
      //get tasks
      let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);
      return (this.getProgressBarValue(calendar.tasks.length, notCompletedTasks.length));
    }
    return 0;
  }

  getProgressBarValueForUnAssignedTasks() {

    //get tasks
    let notCompletedTasks = this.unassignedPlanTasks.filter(x => x.completed == false);

    return (this.getProgressBarValue(this.unassignedPlanTasks.length, notCompletedTasks.length));
  }

  getProgressBarValue(max: number, current: number) {
    return (max - current) / max * 100;
  }

  getRemainingTasksForCalendar(calendar: UserOccupationPlanCalendar) {

    //set tasks
    if (calendar.tasks) {
      let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

      if (notCompletedTasks) {
        if (notCompletedTasks.length > 0) {
          return notCompletedTasks.length;
        }
      }
    }
    return 0;
  }

  getUnassignedPlanTasks() {
    //check to see if there are remaining tasks that are not assigned to a date
    //if so return this count instead;
    let notCompletedTasksNotAssignedToDate = this.unassignedPlanTasks.filter(x => x.completed == false);

    if (notCompletedTasksNotAssignedToDate) {
      if (notCompletedTasksNotAssignedToDate.length > 0) {
        return notCompletedTasksNotAssignedToDate.length;
      }
    }

    return 0;
  }

  async getUserOccupationPlanCalendarTasks(planId) {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getPlanTasks(planId);

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  showSubmitToMentor(userOccupationPlan) {

    if (this.userRole != 'S') {
      const data = {
        'title': 'Plan Submission',
        'message': 'Students will be able to submit their Plans to their mentors, as well as send comments.'
      };

      const dialogRef = this._matDialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '400px',
      });
    } else {
      let data = {
        title: "Plan",
        userOccupationPlan: userOccupationPlan
      }

      this._matDialog.open(SubmitToMentorDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });
    }

  };

  showNotification(userOccupationPlan) {

    //retrieve current user and user role
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    if (userRole != 'S') {
      const data = {
        'title': 'Unread Notifications',
        'message': 'This dialog will show unread messages for the student.'
      };

      const dialogRef = this._matDialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '400px',
      });
    } else {
      this._matDialog.open(StudentNotificationDialogComponent, {
        maxWidth: "800px",
        autoFocus: false
      });
    }

  }

}
