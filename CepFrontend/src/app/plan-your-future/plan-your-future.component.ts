import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserService } from 'app/services/user.service';
import { ClassroomActivitiesDialogComponent } from 'app/core/dialogs/classroom-activities-dialog/classroom-activities-dialog.component';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { MatCalendar, MatCalendarCellCssClasses } from '@angular/material';
import { DateDialogComponent } from 'app/core/dialogs/date-dialog/date-dialog.component';
import { MilitaryCareersDialogComponent } from 'app/core/dialogs/military-careers-dialog/military-careers-dialog.component';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UtilityService } from 'app/services/utility.service';
import { SubmitToMentorDialogComponent } from 'app/core/dialogs/submit-to-mentor-dialog/submit-to-mentor-dialog.component';
import { ClassroomActivityService } from 'app/services/classroom-activity.service';
import { MentorService } from 'app/services/mentor.service';
import { formatDate } from '@angular/common';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import { ConfigService } from 'app/services/config.service';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { StudentNotificationDialogComponent } from 'app/core/dialogs/student-notification-dialog/student-notification-dialog.component';
import { Subscription } from 'rxjs';
import { PortfolioService } from 'app/services/portfolio.service';
@Component({
  selector: 'app-plan-your-future',
  templateUrl: './plan-your-future.component.html',
  styleUrls: ['./plan-your-future.component.scss']
})
export class PlanYourFutureComponent implements OnInit {

  tips: any;
  tipIndex: number;
  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  private portfolio: any;
  private userOccupationPlans: UserOccupationPlan[];
  private userOccupationPlanCalendars: UserOccupationPlanCalendar[];
  public selectedDate: Date;
  public isCalendarLoading = true;
  public selectedUserOccupationPlanCalendar: UserOccupationPlanCalendar[];
  public favoriteList: any[];
  public currentPage: number;
  private classroomActivities;
  private classroomActivitiesFiltered;
  private activeCategory = {
    categoryId: '',
  }
  private activityCategories;
  private isNotStudent;
  public portfolioUnreadMsgCount: number = 0;
  favoriteLength: number = 0;
  favoritedOccupationClosedHint: boolean = false;
  createdPlanClosedHint: boolean = false;
  viewedLineScore: boolean = false;
  viewedLineScoreClosedHint: boolean = false;
  startedPortfolio: boolean = false;
  startedPortfolioClosedHint: boolean = false;
  connectedToMentor: boolean = false;
  connectedToMentorClosedHint: boolean = false;
  userCompletion: any;
  hasMilitaryPlanEnlisted = false;
  ssoUrl: any;
  mentors: any = [];
  subscription: Subscription;

  // Guided Tour
  createdPlanShowCalendarClosedHint: boolean = false;
  guidedTipConfig;
  hasMilitaryPlanOfficer = false;
  militaryPlanOfficerClosedHint: boolean = false;
  lessThanTwoPlansClosedHint: boolean = false;
  hasMilitaryPathway: boolean = false;
  hasMilitaryPathwayClosedHint: boolean = false;
  militaryPathwayPlanSocId;
  viewedMilitaryCareers: boolean = false;
  downloadActivity: boolean = false;
  downloadActivityClosedHint: boolean = false;
  submitPlan: boolean = false;
  submitPlanClosedHint: boolean = false;
  userRole: any;

  @ViewChild(MatCalendar) calendar: MatCalendar<Date>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _user: UserService,
    private _dialog: MatDialog,
    private _planCalendarService: PlanCalendarService,
    private _utilityService: UtilityService,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _configService: ConfigService,
    private _mentorService: MentorService,
    private _portfolioRestFactory: PortfolioRestFactoryService,
    private _router: Router,
    private _classroomActivityService: ClassroomActivityService,
    @Inject(LOCALE_ID) private locale: string,
    private _userService: UserService,
    private _portfolioService: PortfolioService
  ) {
    this.userOccupationPlans = [];
    this.selectedDate = new Date();
    this.currentPage = 1;
    this.ssoUrl = this._configService.getSsoUrl();
  }

  ngOnInit() {
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = this._user.getLeftNavCollapsed();
    }
    //retrieve current user and user role
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    this.userRole = currentUser ? currentUser.role : null;

    this.userOccupationPlanCalendars = this._activatedRoute.snapshot.data.planYourFutureResolver[0];
    this.userOccupationPlans = this._activatedRoute.snapshot.data.planYourFutureResolver[1];
    this.favoriteList = this._activatedRoute.snapshot.data.planYourFutureResolver[2];
    this.favoriteLength = this.favoriteList.length;
    this.classroomActivities = this._activatedRoute.snapshot.data.planYourFutureResolver[3];
    this.classroomActivitiesFiltered = this.classroomActivities;
    this.activityCategories = this._activatedRoute.snapshot.data.planYourFutureResolver[4].filter(function (d) {
      return !d.title.includes("All Categories")
    });

    this.portfolio = this._activatedRoute.snapshot.data.planYourFutureResolver[5];
    //get unread count
    if (this.userRole.toUpperCase() == 'S') {
      this.portfolioUnreadMsgCount = this.getUnreadCount(this.portfolio);
    } else {
      this.portfolioUnreadMsgCount = 2;
    }

    if (this.userOccupationPlanCalendars && this.userOccupationPlanCalendars.length > 0) {
      let closestUpcomingEventDate = this.convertToDateObject(this._utilityService.getClosestDate(this.userOccupationPlanCalendars, this.selectedDate).date);

      if (this.selectedDate < closestUpcomingEventDate) {
        this.selectedDate = closestUpcomingEventDate;
      }

      this.onSelect(this.selectedDate);
    } else {
      //CEPR147
      //if no calendar dates exist; set the selection to null and turn off the loading spinner.
      this.isCalendarLoading = false;
      this.selectedUserOccupationPlanCalendar = null;
    }


    this.tips = [
      'Did you know that volunteer work is a great way to explore your interests.',
      'Share your portfolio with counselors, teachers, and parents to help guide meaningful discussions about your plans after high school.',
      'Narrow your focus and set realistic goals! Declare which path(s) you\'re most strongly considering in your first year after graduating high school.',
      'It gets harder to remember your likes/dislikes and responsibilities at each job as time goes by. Document your work experiences along the way.',
      'Compiling your volunteer and extracurricular activities will help you reflect on your experience when writing first resume and application essays.',
      'Map out the needed coursework relevant to your career choice or notable courses you\'re already completed.',
      'Track your test scores and be mindful of the scores you need for acceptance or entry into your chosen post-secondary path.',
      'Career satisfaction comes from having a job that meets your needs. Document your skills, interests, and work values.',
      'Save your favorite occupations, colleges, and career clusters and start planning.',
    ]

    this.tipIndex = Math.floor(Math.random() * this.tips.length);

    this._user.getCompletion().subscribe(data => {
      this.userCompletion = data;

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAVORITED_OCCUPATION_CLOSED_HINT)) {
        this.favoritedOccupationClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CREATED_PLAN_CLOSED_HINT)) {
        this.createdPlanClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_LINE_SCORE)) {
        this.viewedLineScore = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_LINE_SCORE_CLOSED_HINT)) {
        this.viewedLineScoreClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.STARTED_PORTFOLIO)) {
        this.startedPortfolio = true;
      } else {
        this._portfolioRestFactory.isPortfolioStarted().toPromise().then(success => {
          if (success > 0) {
            this.completionStartPortfolio();
          }
        }, error => {
          console.error(error);
        });
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.STARTED_PORTFOLIO_CLOSED_HINT)) {
        this.startedPortfolioClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CONNECTED_TO_MENTOR)) {
        this.connectedToMentor = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CONNECTED_TO_MENTOR_CLOSED_HINT)) {
        this.connectedToMentorClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.CREATED_PLAN_SHOW_CALENDAR_CLOSED_HINT)) {
        this.createdPlanShowCalendarClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.MILITARY_PLAN_OFFICER_CLOSED_HINT)) {
        this.militaryPlanOfficerClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.LESS_THAN_TWO_PLANS_CLOSED_HINT)) {
        this.lessThanTwoPlansClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.MILITARY_PATHWAY_VIEW_CAREERS)) {
        this.viewedMilitaryCareers = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.MILITARY_PATHWAY_VIEW_CAREERS_CLOSED_HINT)) {
        this.hasMilitaryPathwayClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.DOWNLOAD_ACTIVITY)) {
        this.downloadActivity = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.DOWNLOAD_ACTIVITY_CLOSED_HINT)) {
        this.downloadActivityClosedHint = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.SUBMIT_PLAN_TO_MENTOR)) {
        this.submitPlan = true;
      }

      if (this._user.checkUserCompletion(this.userCompletion, UserService.SUBMIT_PLAN_TO_MENTOR_CLOSED_HINT)) {
        this.submitPlanClosedHint = true;
      }

      const guidedTipConfigs = this._user.getCompletionByType(this.userCompletion, UserService.GUIDED_TIP_CONFIG);
      this.guidedTipConfig = JSON.parse(guidedTipConfigs.value);

      // this.checkPlusItemToShow();
    });

    this._userOccupationPlanService.getAll().then((plans: UserOccupationPlan[]) => {
      this.userOccupationPlans = plans;
      this.hasMilitaryPlanEnlisted = this.userOccupationPlans.some((p: any) =>
        p.militaryPlans && p.militaryPlans.length > 0 && p.militaryPlans.some(milP => milP.didSelectEnlisted)
      )

      this.hasMilitaryPlanOfficer = this.userOccupationPlans.some((p: any) =>
        p.militaryPlans && p.militaryPlans.length > 0 && p.militaryPlans.some(milP => milP.didSelectOfficer)
      )

      let militaryPlanTemp = this.userOccupationPlans.find((p: any) =>
        p.militaryPlans && p.militaryPlans.length > 0 && !!JSON.parse(p.pathway).military);
      this.militaryPathwayPlanSocId = militaryPlanTemp ? militaryPlanTemp.socId : undefined;

      this.hasMilitaryPathway = !!this.militaryPathwayPlanSocId;
    });

    this._mentorService.getAll().then((mentors: any) => {
      this.mentors = mentors;
      this.connectedToMentor = !!this.mentors.length;

      if (this.connectedToMentor) {
        this.completionConnectToMentor();
      }
    })

    this.setUserSubscription();

  }

  setUserSubscription() {
    this.subscription = this._user.getUserUpdates().subscribe(hasUpdate => {
      this.updateMessageCount();


    })
  }

  updateMessageCount() {

    this._portfolioService.getPortfolio().then((results: any) => {
      this.portfolioUnreadMsgCount = this.getUnreadCount(results);
    })


    this._classroomActivityService.getActivities().then((results: any) => {
      this.classroomActivities = results;
      this.classroomActivitiesFiltered = this.classroomActivities;
    })

    this._userOccupationPlanService.getAll().then((results: any) => {
      this.userOccupationPlans = results;
    })

  }

  showMilitaryCareers() {
    if (this.hasMilitaryPathway) {
      this._dialog.open(MilitaryCareersDialogComponent, {
        data: {
          socId: this.militaryPathwayPlanSocId,
          title: 'Military Careers',
        },
      })
    }
  }

  closedFavoritedOccupationHint() {
    this.favoritedOccupationClosedHint = true;
    this._user.setCompletion(UserService.FAVORITED_OCCUPATION_CLOSED_HINT, 'true');
  }

  closedCreatePlanHint() {
    this.createdPlanClosedHint = true;
    this._user.setCompletion(UserService.CREATED_PLAN_CLOSED_HINT, 'true');
  }

  completionViewLineScore() {
    this.viewedLineScore = true;
    this._user.setCompletion(UserService.VISITED_MILITARY_LINE_SCORE, 'true');
  }

  closedLineScoreHint() {
    this.viewedLineScoreClosedHint = true;
    this._user.setCompletion(UserService.VISITED_MILITARY_LINE_SCORE_CLOSED_HINT, 'true');
  }

  completionStartPortfolio() {
    this.startedPortfolio = true;
    this._user.setCompletion(UserService.STARTED_PORTFOLIO, 'true');
  }

  closedStartPortfolioHint() {
    this.startedPortfolioClosedHint = true;
    this._user.setCompletion(UserService.STARTED_PORTFOLIO_CLOSED_HINT, 'true');
  }

  completionConnectToMentor() {
    this.connectedToMentor = true;
    this._user.setCompletion(UserService.CONNECTED_TO_MENTOR, 'true');
  }

  closedConnectToMentorHint() {
    this.connectedToMentorClosedHint = true;
    this._user.setCompletion(UserService.CONNECTED_TO_MENTOR_CLOSED_HINT, 'true');
  }

  closedCreatedPlanShowCalendarHint() {
    this.createdPlanShowCalendarClosedHint = true;
    this._user.setCompletion(UserService.CREATED_PLAN_SHOW_CALENDAR_CLOSED_HINT, 'true');
  }

  closedMilitaryPlanOfficerHint() {
    this.militaryPlanOfficerClosedHint = true;
    this._user.setCompletion(UserService.MILITARY_PLAN_OFFICER_CLOSED_HINT, 'true');
  }

  closedLessThanTwoPlansHint() {
    this.lessThanTwoPlansClosedHint = true;
    this._user.setCompletion(UserService.LESS_THAN_TWO_PLANS_CLOSED_HINT, 'true');
  }

  closedMilitaryPathwayHint() {
    this.hasMilitaryPathwayClosedHint = true;
    this._user.setCompletion(UserService.MILITARY_PATHWAY_VIEW_CAREERS_CLOSED_HINT, 'true');
  }

  closedDownloadActivityHint() {
    this.downloadActivityClosedHint = true;
    this._user.setCompletion(UserService.DOWNLOAD_ACTIVITY_CLOSED_HINT, 'true');
  }

  closedSubmitPlanHint() {
    this.submitPlanClosedHint = true;
    this._user.setCompletion(UserService.SUBMIT_PLAN_TO_MENTOR_CLOSED_HINT, 'true');
  }

  /**
   * Filter classroom category
   */
  onCategoryChange = function (category) {
    this.activeCategory.categoryId = category;
    this.classroomActivitiesFiltered = this.classroomActivities.filter(function (d) {
      if (d.categoryIds.some(function (c) {
        return c.toString() === category.toString()
      }) || category.toString() === "") {
        return true;
      }
    })
  }

  /**
   * Returns date object of string format mm/dd/yyyy.
   */
  convertToDateObject(date: String) {
    let dateFormat = "mm/dd/yyyy";
    let dateFormatDelimiter = dateFormat.split("/");
    let dateItems = date.split("/");
    let monthIndex = dateFormatDelimiter.indexOf("mm");
    let dayIndex = dateFormatDelimiter.indexOf("dd");
    let yearIndex = dateFormatDelimiter.indexOf("yyyy");
    let month = parseInt(dateItems[monthIndex]);
    month -= 1;

    return new Date(parseInt(dateItems[yearIndex]), month, parseInt(dateItems[dayIndex]));
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  /* ngx slick carousel config */
  slideConfig = {
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: false,
          dots: true
        }
      }, {
        breakpoint: 639,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };


  cardCarouselConfig = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: false,
          dots: false
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  removeUserOccupationPlan(planId) {
    this.userOccupationPlans.forEach((value: UserOccupationPlan, index) => {
      if (value.id == planId) this.userOccupationPlans.splice(index, 1);
    });
  }

  getDateClass() {
    return (date: Date): MatCalendarCellCssClasses => {
      const highlightDate = this.userOccupationPlanCalendars
        // .map(userOccupationPlanCalendar => new Date(this._utilityService.convertServerUTCToUniversalUTC(userOccupationPlanCalendar.date)))
        .map(userOccupationPlanCalendar => this.adjustDate(userOccupationPlanCalendar.date))
        .some(d => d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear());
      return highlightDate ? 'selected-date' : '';
    };
  }

  /**
   * 
   * @param date 
   * @returns returns an adjusted date due to the server storing a date as a midnight; with utc conversion it was removing a day from 
   * timestamp.
   */
  adjustDate(date) {
    let returnDate: Date = new Date(this._utilityService.convertServerUTCToUniversalUTC(date));
    returnDate.setDate(returnDate.getDate() + 1);
    return returnDate;
  }

  /**
   * updates the ui if a date selected as a calendar date and its tasks.
   * @param event 
   */
  onSelect(event) {
    this.selectedDate = event;

    this.isCalendarLoading = true;

    if (this.userOccupationPlanCalendars && this.userOccupationPlanCalendars.length > 0) {
      const userOccupationPlanCalendar = this.userOccupationPlanCalendars
        .filter(d => new Date(this.adjustDate(d.date)).getDate() === this.selectedDate.getDate()
          && new Date(this._utilityService.convertServerUTCToUniversalUTC(d.date)).getMonth() === this.selectedDate.getMonth()
          && new Date(this._utilityService.convertServerUTCToUniversalUTC(d.date)).getFullYear() === this.selectedDate.getFullYear());

      if (userOccupationPlanCalendar) {
        this.isCalendarLoading = false;
        this.selectedUserOccupationPlanCalendar = userOccupationPlanCalendar;
      } else {
        this.isCalendarLoading = false;
        this.selectedUserOccupationPlanCalendar = null;
      }
    } else {
      //CEPR147
      //if no calendar dates exist; set the selection to null and turn off the loading spinner.
      this.isCalendarLoading = false;
      this.selectedUserOccupationPlanCalendar = null;
    }
  }

  classroomActivitiesDialog(activity: any) {

    let classroomSubmission = this._classroomActivityService.getMyActivitySubmissions(activity.id);

    let submissionResults;
    classroomSubmission.then((results) => {
      submissionResults = results;
      return this._mentorService.getAll();
    }, (error) => {
      console.error(error);
    }).then((mentorResults) => {
      const dialogRef = this._dialog.open(ClassroomActivitiesDialogComponent, {
        disableClose: true,
        data: { activity: submissionResults, isNotStudent: (this.userRole.toUpperCase() == "S" ? false : true), mentors: mentorResults },
      }).afterClosed()
        .subscribe(response => {
          var index = this.classroomActivitiesFiltered.map(x => {
            return x.id;
          }).indexOf(response.activity.id);
          this.classroomActivitiesFiltered[index].submissions = response.activity.submissions;
        });


    }, (error) => {
      console.error(error);
    });

  }

  getRemainingTasksForCalendar(calendar: UserOccupationPlanCalendar) {

    let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

    if (notCompletedTasks) {
      return notCompletedTasks.length;
    }

    return 0;
  }

  getProgressBarValueForCalendar(calendar: UserOccupationPlanCalendar) {

    let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

    return (this.getProgressBarValue(calendar.tasks.length, notCompletedTasks.length));

  }

  getProgressBarValue(max: number, current: number) {
    return (max - current) / max * 100;
  }

  editDate(date: UserOccupationPlanCalendar) {

    date.date = this.adjustDate(date.date).toLocaleDateString();

    let data = {
      userOccupationPlanCalendar: date,
      canEditPlan: true
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendar: UserOccupationPlanCalendar = result;

        let existingIndex = this.userOccupationPlanCalendars.findIndex(x => x.id == userOccupationPlanCalendar.id);

        //update tasks
        if (userOccupationPlanCalendar.tasks) {
          userOccupationPlanCalendar.tasks.forEach((task: UserOccupationPlanCalendarTask) => {

            let index = this.userOccupationPlanCalendars[existingIndex].tasks.findIndex(x => x.id == task.id);

            if (index > -1) {
              //record was found; update
              this.userOccupationPlanCalendars[existingIndex].tasks[index] = task;
            } else {
              this.userOccupationPlanCalendars[existingIndex].tasks.push(task);
            }

          });
        }

        this.userOccupationPlanCalendars[existingIndex].planId = userOccupationPlanCalendar.planId;
        this.userOccupationPlanCalendars[existingIndex].title = userOccupationPlanCalendar.title;

        this.userOccupationPlanCalendars[existingIndex].date = 
          formatDate(new Date(userOccupationPlanCalendar.date).toLocaleDateString(),'MM/dd/yyyy', this.locale);

        this.calendar.updateTodaysDate(); // update calendar state

      }
    });

  }

  setTaskComplete(task: UserOccupationPlanCalendarTask, event) {

    let dialogData = {
      title: 'Confirm Completion',
      message: 'Complete task <strong>' + this.convertTaskName(task.taskName) + '</strong>, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          task.completed = true;

          //updating an existing record
          this._planCalendarService.updateTask(task, task.id).then((rowsAffected: number) => {

            this.userOccupationPlanCalendars.find(x => x.id == task.calendarId).tasks.find(y => y.id == task.id).completed = true;

          }).catch((error: any) => {
            console.error("ERROR", error);
          });

        } else {
          event.target.checked = false;
        }
      } else {
        event.target.checked = false;
      }
    });

  }

  //adds task to list that was added from a plan card add-task dialog
  addTaskToList(event) {

    if (event.calendarId) {
      let index = this.userOccupationPlanCalendars.findIndex(x => x.id == event.calendarId);

      if (this.userOccupationPlanCalendars[index]) {
        this.userOccupationPlanCalendars[index].tasks.push(event);
      }
    }

  }


  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

  tipsModal() {
    const data = {
      'title': 'Portfolio Tips',
      'message': `<ul><li>${this.tips[0]}</li><li>${this.tips[1]}</li><li>${this.tips[2]}</li><li>${this.tips[3]}</li><li>${this.tips[4]}</li><li>${this.tips[5]}</li><li>${this.tips[6]}</li><li>${this.tips[7]}</li><li>${this.tips[8]}</li></ul>`,
    };

    const dialogRef = this._dialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '600px',
    });
  }

  showSubmitToMentor() {

    //retrieve current user and user role
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    if (userRole != 'S') {
      const data = {
        'title': 'Portfolio Submission',
        'message': 'Students will be able to submit their Portfolio to their mentors, as well as send comments.'
      };

      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '400px',
      });
    } else {
      let data = {
        title: "Portfolio"
      }
      this._dialog.open(SubmitToMentorDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });
    }
  }

  showNotification() {

    if (this.userRole != 'S') {
      const data = {
        'title': 'Unread Notifications',
        'message': 'This dialog will show unread messages for the student.'
      };

      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '400px',
      });
    } else {
      this._dialog.open(StudentNotificationDialogComponent, {
        maxWidth: "800px",
        autoFocus: false
      });
    }
  }

  /** returns true/false based on whether all mentors have reviewed all submissions */
  getIsActivityCompleted(submissions: any[]): boolean {

    if (submissions.length > 0) {

      let numSubmissions = submissions.length;
      let numSubmissionsReviewed = 0;
      let lastReviewDate;
      submissions.forEach(submission => {

        //loop through and see if all mentors assigned to the submission have reviewed
        if (Array.isArray(submission.mentors) && submission.mentors.length > 0) {
          let mentors: any[] = submission.mentors;
          let numMentors = mentors.length;
          let numReviewed = mentors.filter(x => x.didReview == true).length;
          if (numMentors == numReviewed) {
            numSubmissionsReviewed++;
          }
        }

      })

      if (numSubmissions == numSubmissionsReviewed) {
        return true;
      } else {
        return false;
      }

    }
  }

  getSubmissionStatus(submissions: any[]): string {

    if (submissions && submissions.length > 0) {

      let numSubmissions = submissions.length;
      let numSubmissionsSubmitted = 0;
      let numSubmissionsCompleted = 0;
      let lastUpdatedDate: Date;
      submissions.forEach(submission => {
        //foreach submission
        //if statusid == 2; then mark as submitted        
        //loop through and see if all mentors assigned to the submission have reviewed
        if (submission.statusId == 2) {
          numSubmissionsSubmitted++;

          if (!lastUpdatedDate) {
            lastUpdatedDate = new Date(this._utilityService.convertServerUTCToUniversalUTC(submission.dateUpdated));
          } else {
            lastUpdatedDate = (submission.dateUpdated > lastUpdatedDate) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(submission.dateUpdated)) : lastUpdatedDate;
          }

        }

        if (submission.statusId == 5) {
          numSubmissionsCompleted++;
        }
      })

      if ((numSubmissions == numSubmissionsSubmitted + numSubmissionsCompleted) && lastUpdatedDate) {
        return "Submitted " + formatDate(lastUpdatedDate, 'MM/dd/yyyy', this.locale);
      }

    }
  }

  routeToPortfolio() {
    if (this.startedPortfolio) {
      this._router.navigate(['/portfolio']);
    } else {
      this._router.navigate(['/portfolio-directions']);
    }
  }

  getUnreadCount(portfolio: any) {
    let unreadCount = 0;

    if (!portfolio) {
      return unreadCount;
    }

    if (portfolio.logs && portfolio.logs.length > 0) {

      let sortedLogs = portfolio.logs.sort((a, b) => {
        let da: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateLogged)),
          db: any = new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateLogged));
        return da - db;
      });

      let lastLog = sortedLogs[sortedLogs.length - 1];
      if (lastLog.statusId == 5 && !lastLog.hasBeenRead) {
        unreadCount += 1;
      }
    }

    if (portfolio.comments && portfolio.comments.length > 0) {
      portfolio.comments.forEach((comment: any) => {
        if (comment.from != 'You') {
          unreadCount = (comment.to.filter(x => !x.hasBeenRead).length > 0) ? unreadCount + 1 : unreadCount;
        }
      })
    }

    return unreadCount;
  }

}
