import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralSiteComponent } from './general-site.component';

describe('GeneralSiteComponent', () => {
  let component: GeneralSiteComponent;
  let fixture: ComponentFixture<GeneralSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
