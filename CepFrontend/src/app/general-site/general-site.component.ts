import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-general-site',
  templateUrl: './general-site.component.html',
  styleUrls: ['./general-site.component.scss']
})
export class GeneralSiteComponent implements OnInit {

	
  page;
  pageHtml;
  
  constructor(private _activatedRoute: ActivatedRoute,
		  private sanitized: DomSanitizer,
		  private _meta: Meta,
		  private _titleTag: Title) { }

  ngOnInit() {
	  
	  this.page = this._activatedRoute.snapshot.data.page;
	  
	  // This is needed because the "id" attribute is being ignored in InnerHtml
	  this.pageHtml = this.sanitized.bypassSecurityTrustHtml(this.page.pageHtml);

	  this.updateMeta();
  }
  
  updateMeta() {

	    this._titleTag.setTitle(this.page.pageTitle);
	    this._meta.updateTag({name: 'description', content: this.page.pageDescription});

	    this._meta.updateTag({property: 'og:title', content: this.page.pageTitle});
	    this._meta.updateTag({property: 'og:description', content: this.page.pageDescription});
	    this._meta.updateTag({property: 'og:type', content: 'website'});

	    this._meta.updateTag({ name: 'twitter:title', content:  this.page.pageTitle});
	    this._meta.updateTag({ name: 'twitter:description', content: this.page.pageDescription});
	  }

}
