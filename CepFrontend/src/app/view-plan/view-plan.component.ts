import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ContactRecruiterDialogComponent } from 'app/core/dialogs/contact-recruiter-dialog/contact-recruiter-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { SubmitToMentorDialogComponent } from 'app/core/dialogs/submit-to-mentor-dialog/submit-to-mentor-dialog.component';
import { ServiceCollege } from 'app/core/models/serviceCollege.model';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserGapYearPlan } from 'app/core/models/userGapYearPlan.model';
import { UserGapYearPlanType } from 'app/core/models/userGapYearPlanType.model';
import { UserMilitaryPlan } from 'app/core/models/userMilitaryPlan.model';
import { UserMilitaryPlanRotc } from 'app/core/models/userMilitaryPlanRotc.model';
import { UserMilitaryPlanServiceCollege } from 'app/core/models/userMilitaryPlanServiceCollege.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { UserSchoolPlan } from 'app/core/models/userSchoolPlan.model';
import { UserWorkPlan } from 'app/core/models/userWorkPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { CollegePlanService } from 'app/services/college-plan.service';
import { ConfigService } from 'app/services/config.service';
import { GapYearPlanService } from 'app/services/gap-year-plan.service';
import { IpedsService } from 'app/services/ipeds.service';
import { MilitaryPlanService } from 'app/services/military-plan.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { SchoolPlanService } from 'app/services/school-plan.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { WorkPlanService } from 'app/services/work-plan.service';
import html2canvas from "html2canvas";

declare var pdfMake: any;

@Component({
  selector: 'app-view-plan',
  templateUrl: './view-plan.component.html',
  styleUrls: ['./view-plan.component.scss']
})
export class ViewPlanComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs
  public userOccupationPlan: UserOccupationPlan;
  public occupationDescription: string;
  public salary: string;
  public salaryText: string;
  public userSchoolPlan: UserSchoolPlan;
  public userCollegePlans: any[];
  public userMilitaryPlans: any[];
  public userGapYearPlans: any[];
  public militaryOccupationTitles: any[];
  public documentLocation: string;
  public serviceColleges: ServiceCollege[];
  public userWorkPlan: UserWorkPlan;
  public selectedPathways;

  serviceContacts: any;

  public hsPlanIsLoading: boolean;
  public gapYearIsLoading: boolean;
  public collegePlanIsLoading: boolean;
  public workPlanIsLoading: boolean;
  public militaryPlanIsLoading: boolean;
  public serviceCollegesIsLoading: boolean;
  public userPlanTasks: UserOccupationPlanCalendarTask[];
  public userPlanCalendarTasks: any[];
  public favorites: any;

  public student:any;
  public studentName:string;
  
  mobileOrTablet: boolean = false;

  @ViewChild('printMe') printSectionElem: ElementRef;

  constructor(private _activatedRoute: ActivatedRoute,
    private _occufindRestFactory: OccufindRestFactoryService,
    private _schoolPlanService: SchoolPlanService,
    private _collegePlanService: CollegePlanService,
    private _militaryPlanService: MilitaryPlanService,
    private _gapYearPlanService: GapYearPlanService,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _configService: ConfigService,
    public _utilityService: UtilityService,
    private _ipedsService: IpedsService,
    private _workPlanService: WorkPlanService,
    private _matDialog: MatDialog,
    private _planCalendarService: PlanCalendarService,
    private _userService: UserService) {

    this.hsPlanIsLoading = true;
    this.gapYearIsLoading = true;
    this.collegePlanIsLoading = true;
    this.workPlanIsLoading = true;
    this.militaryPlanIsLoading = true;
    this.serviceCollegesIsLoading = true;

    this.mobileOrTablet = this._utilityService.mobileOrTablet();
  }

  ngOnInit() {
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = false;
    }

    this.documentLocation = this._configService.getImageUrl() + this._configService.getDocumentFolder();

    this.userOccupationPlan = this._activatedRoute.snapshot.data.viewPlanResolver[0];
    this.occupationDescription = this._activatedRoute.snapshot.data.viewPlanResolver[1];
    this.militaryOccupationTitles = this._activatedRoute.snapshot.data.viewPlanResolver[2];
    this.favorites = this._activatedRoute.snapshot.data.viewPlanResolver[3];
    this.student = this._activatedRoute.snapshot.data.viewPlanResolver[4];

    //set student name
    if (!this.student) {
      this.studentName = '';
    } else if (this.student && (!this.student.firstName || !this.student.lastName)) {
      this.studentName = this.student.emailAddress;
    } else {
      this.studentName = this.student.firstName + " " + this.student.lastName;
    }

    let onetSocTrimmed = this.userOccupationPlan.socId.slice(0, -3);

    this.selectedPathways = this._utilityService.parsePathway(this.userOccupationPlan.pathway);

    if (this.userOccupationPlan.useNationalAverage) {
      this._occufindRestFactory.getNationalSalary(onetSocTrimmed).toPromise().then(response => this.salary = this._utilityService.convertToUSDCurrency(response));
      this.salaryText = "National Average Starting Salary";
    } else {
      this._occufindRestFactory.getNationalEntrySalary(onetSocTrimmed).toPromise().then(response => this.salary = this._utilityService.convertToUSDCurrency(response));

      this.salaryText = "Starting Average Salary";
    }

    this.getServiceColleges();

    this.getHighSchoolPlans();

    this.getAllCollegePlans();

    this.getAllMilitaryPlans();

    this.getAllGapYearPlans();

    this.getUserWorkPlan();

    this._workPlanService.getServiceContacts().subscribe(
      data => {
        this.serviceContacts = data;
      });

    let planCalendarTasks = this.getAllUserOccupationPlanCalendarTasks();
    let planCalendars = this.getAllUserOccupationPlanCalendars();

    Promise.all([planCalendarTasks, planCalendars]).then((done: any) => {
      this.userPlanTasks = done[0];
      this.userPlanCalendarTasks = done[1];

      //add new property 'expanded' to calendar class
      //this will control whether a date can show its tasks to the user
      //default to true
      if (this.userPlanCalendarTasks) {
        this.userPlanCalendarTasks.forEach(element => {
          element.expanded = true;
        });
      }



    });

  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  getHighSchoolPlans() {
    this._schoolPlanService.get(this.userOccupationPlan.id).then((userSchoolPlan: UserSchoolPlan) => {
      if (userSchoolPlan.graduationDate) {
        userSchoolPlan.graduationDate = new Date(this._utilityService.convertServerUTCToUniversalUTC(userSchoolPlan.graduationDate)).toLocaleDateString();
      }
      this.userSchoolPlan = userSchoolPlan;
      this.hsPlanIsLoading = false;
    }).catch(error => {
      this.hsPlanIsLoading = false;
      console.error("Error:", error);
    });
  }

  getAllCollegePlans() {
    this._collegePlanService.getAll(this.userOccupationPlan.id).then((_userCollegePlans: UserCollegePlan[]) => {
      if (_userCollegePlans && _userCollegePlans.length > 0) {
        this.userCollegePlans = [];
        _userCollegePlans.forEach((collegePlan: any) => {
          this.userCollegePlans.push({ ...collegePlan, ...this.setCollegePlanSources(collegePlan) });
        });
      }
      this.collegePlanIsLoading = false;
    }).catch(error => {
      this.collegePlanIsLoading = false;
      console.error("Error", error);
    });
  }

  getAllMilitaryPlans() {

    let numMilitaryPlans = 0;
    this._militaryPlanService.get(this.userOccupationPlan.id).then(async (_userMilitaryPlans: UserMilitaryPlan[]) => {

      if (_userMilitaryPlans && _userMilitaryPlans.length > 0) {
        this.userMilitaryPlans = [];
        for (let userMilitaryPlan of _userMilitaryPlans) {
          let militaryPlanInfo = await this.setMilitaryPlanInfo(userMilitaryPlan).then((response: any) => {
            this.userMilitaryPlans.push({ ...userMilitaryPlan, ...response })
            numMilitaryPlans++;
          }).catch((error) => {
            this.militaryPlanIsLoading = false;
            console.error("ERROR", error);
          });

          //are we done loading them all?
          if (numMilitaryPlans >= _userMilitaryPlans.length) {
            this.militaryPlanIsLoading = false;
          }

        }
      } else {
        this.militaryPlanIsLoading = false;
      }
    }).catch(error => {
      this.militaryPlanIsLoading = false;
      console.error("ERROR", error);
    });
  }

  getAllGapYearPlans() {
    this._gapYearPlanService.getAll(this.userOccupationPlan.id).then(async (_userGapYearPlans: UserGapYearPlan[]) => {

      if (_userGapYearPlans && _userGapYearPlans.length > 0) {
        this.userGapYearPlans = [];

        for (let userGapYearPlan of _userGapYearPlans) {
          let userGapYearPlanInfo = await this.setGapYearInfo(userGapYearPlan);
          this.userGapYearPlans.push({ ...userGapYearPlan, ...userGapYearPlanInfo })
        }
      }
      this.gapYearIsLoading = false;

    }).catch(error => {
      this.gapYearIsLoading = false;
      console.error("ERROR", error);
    });
  }

  async getUserWorkPlan() {

    try {
      this.userWorkPlan = await this._workPlanService.get(this.userOccupationPlan.id).then((response: UserWorkPlan) => { return response; });

      if (!this.userWorkPlan) {
        this.workPlanIsLoading = false;
        this.userWorkPlan = null;
      } else {
        this.userWorkPlan = {
          planId: this.userWorkPlan.planId,
          workPlan: this.userWorkPlan.workPlan
        };
        this.workPlanIsLoading = false;
      }
    } catch (error) {
      this.workPlanIsLoading = false;
      this.userWorkPlan = null;
    }

  }

  async getServiceColleges() {
    let services: string[] = ['A', 'F', 'C', 'M', 'N', 'S']
    //retrieve all service colleges
    //filter out within the specific user plan
    try {
      let serviceColleges: any = await this._ipedsService.getServiceColleges(services);

      if (serviceColleges) {
        this.serviceColleges = [];
        for (let serviceCollege of serviceColleges) {
          this.serviceColleges.push(serviceCollege);
        };
        this.serviceCollegesIsLoading = false;
      }
    } catch (error) {
      this.serviceCollegesIsLoading = false;
      console.error("ERROR:", error);
    }
  }

  setCollegePlanSources(userCollegePlan: UserCollegePlan) {
    //tuition sources
    let sources = [];
    let sumTotalSource = 0;

    if (userCollegePlan.familyContribution) {
      sources.push({
        value: userCollegePlan.familyContribution,
        label: 'Family'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.familyContribution;
    }

    if (userCollegePlan.scholarshipContribution) {
      sources.push({
        value: userCollegePlan.scholarshipContribution,
        label: 'Scholarship'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.scholarshipContribution;
    }

    if (userCollegePlan.savings) {
      sources.push({
        value: userCollegePlan.savings,
        label: 'Savings/529'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.savings;
    }

    if (userCollegePlan.loan) {
      sources.push({
        value: userCollegePlan.loan,
        label: 'Loan'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.loan;
    }

    if (userCollegePlan.other) {
      sources.push({
        value: userCollegePlan.other,
        label: 'Other'
      });
      sumTotalSource = sumTotalSource + userCollegePlan.other;
    };

    return ({ sources: sources, sumTotalSource: sumTotalSource })
  }

  async setMilitaryPlanInfo(userMilitaryPlan: UserMilitaryPlan) {

    let services = [];
    let rotc = [];
    let academies = [];
    let careers = [];

    if (userMilitaryPlan.didSelectArmy) services.push("Army");
    if (userMilitaryPlan.didSelectMarineCorps) services.push("Marine Corps");
    if (userMilitaryPlan.didSelectNavy) services.push("Navy");
    if (userMilitaryPlan.didSelectAirForce) services.push("Air Force");
    if (userMilitaryPlan.didSelectSpaceForce) services.push("Space Force");
    if (userMilitaryPlan.didSelectCoastGuard) services.push("Coast Guard");

    if (userMilitaryPlan.didSelectOfficer) {

      if (userMilitaryPlan.rotcPrograms) {
        userMilitaryPlan.rotcPrograms.forEach(async (userMilitaryPlanRotc: UserMilitaryPlanRotc) => {
          let printRotc: any = {};

          if (userMilitaryPlanRotc.wantsToApply == 1) {
            let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(userMilitaryPlanRotc.unitId).toPromise();

            if (schoolProfile) {
              printRotc.value = schoolProfile.institutionName;
              printRotc.label = '';
              if (schoolProfile.rotcAirForce) printRotc.label = 'AFROTC';
              if (schoolProfile.rotcNavy) printRotc.label = 'NROTC';
              if (schoolProfile.rotcArmy) printRotc.label = 'AROTC';

              if (printRotc.label == '') {
                printRotc.label = 'School'
              }

              rotc.push(printRotc);
            }

          }

        });
      }

      if (userMilitaryPlan.serviceColleges) {
        userMilitaryPlan.serviceColleges.forEach(async (serviceCollege: UserMilitaryPlanServiceCollege) => {

          let printServiceCollege: any = {};
          // let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(serviceCollege.unitId).toPromise();


          printServiceCollege.label = (serviceCollege.collegeTypeId == 1) ? 'Military Academy' :
            ((serviceCollege.collegeTypeId == 2) ? 'Senior Military College' : 'Maritime Academy');

          printServiceCollege.unitId = serviceCollege.unitId;
          printServiceCollege.collegeTypeId = serviceCollege.collegeTypeId;

          academies.push(printServiceCollege);


        })
      }

    }

    for (let userMilitaryPlanCareer of userMilitaryPlan.selectedCareers) {
      let jobTitle: string;
      let occuTitle: string;
      let service: string = "";
      if (userMilitaryPlanCareer.mcId) {

        let militaryOccuTitle: any = this.militaryOccupationTitles.find(x => x.mcId == userMilitaryPlanCareer.mcId);

        if (militaryOccuTitle) {

          let favoriteExists = this.favorites.find(x => x.mcId == militaryOccuTitle.mcId);
          if (favoriteExists) {
            occuTitle = militaryOccuTitle.title + "<i style='margin-left:5px' class='fa fa-heart'></i>";
          } else {
            occuTitle = militaryOccuTitle.title;
          }

        } else {
          occuTitle = "";
        }

        // army details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'A') {
          try {
            let career: any = await this._militaryPlanService.getArmyDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Army: ' + career.title;
              service = 'Army';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        //navy details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'N') {
          try {
            let career: any = await this._militaryPlanService.getNavyDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Navy: ' + career.title;
              service = 'Navy';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // air force details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'F') {
          try {
            let career: any = await this._militaryPlanService.getAirForceDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = 'Air Force: ' + career.title;
              service = 'Air Force';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // marine corps details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'M') {
          try {
            let career: any = await this._militaryPlanService.getMarineDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Marine Corps: " + career.title;
              service = 'Marine Corps';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        //space force details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'S') {
          try {
            let career: any = await this._militaryPlanService.getSpaceForceDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Space Force: " + career.title;
              service = 'Air Force'; //No Space Force recruiter exists; TODO update when available
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

        // coast guard details
        if (userMilitaryPlanCareer.svc.toUpperCase() == 'C') {
          try {
            let career: any = await this._militaryPlanService.getCoastGuardDetail(userMilitaryPlanCareer.mcId, userMilitaryPlanCareer.moc)
              .then((response: any) => {
                return response;
              });
            if (career) {
              jobTitle = "Coast Guard: " + career.title;
              service = 'Coast Guard';
            } else {
              jobTitle = "";
            }
          } catch (error) {
            console.error("ERROR", error);
          }

        }

      }

      careers.push({ occuTitle, jobTitle, service });

    }

    return ({ services: services.join(', '), rotc: rotc, academies: academies, careers: careers });

  }

  getAcademyApplicationTimelinePdfKey(unitId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId);
    if (academy) {
      return academy.applicationTimelinePdfKey;
    } else {
      return null;
    }
  }

  getAcademyChecklistPdfKey(unitId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId);
    if (academy) {
      return academy.checklistPdfKey;
    } else {
      return null;
    }
  }

  async setGapYearInfo(userGapYearPlan: UserGapYearPlan) {

    let types: string[] = [];
    if (userGapYearPlan.types) {
      for (let x of userGapYearPlan.types) {
        if (x.isSelected) {
          types.push(x.name);
        }
      }
    }

    return ({ selectedTypes: types.join(', ') });

  }

  printPdf() {

    setTimeout(() => this.triggerPrint());
  }

  downloadPdf() {

    setTimeout(() => this.triggerDownload());

  }

  triggerDownload() {

    this.getCanvas().then((s: any) => {

      let docDefinition = {
        pageOrientation: 'portrait',
        pageSize: 'LETTER',
        content: s
      };

      pdfMake.createPdf(docDefinition).download(this.studentName.replace(' ', '_') + '_' + this.occupationDescription.replace(" ", "_") + '_plan.pdf');

    }).catch(error => {
      console.error("ERROR", error);
      alert(error);
    });
  }

  triggerPrint() {
    this.getCanvas().then((s: any) => {

      let docDefinition = {
        pageOrientation: 'portrait',
        pageSize: 'LETTER',
        content: s
      };

      pdfMake.createPdf(docDefinition).print();

    }).catch(error => {
      console.error("ERROR", error);
      alert(error);
    });
  }

  getCanvas() {
    let self = this;

    return new Promise((resolve, reject) => {
      html2canvas(self.printSectionElem.nativeElement, {
        imageTimeout: 2000,
        removeContainer: true,
        backgroundColor: '#fff',
        allowTaint: true,
        logging: false,
        scale: 1,
        ignoreElements: (node) => { //needed due to https://github.com/niklasvh/html2canvas/issues/1593#issuecomment-489059452
          return node.nodeName === 'IFRAME';
        },
        onclone: function (cloneDoc) {
          try {

            let printSection: HTMLElement = cloneDoc.getElementById("printMe");
            printSection.style.display = 'block';

            let elements = cloneDoc.querySelectorAll('.task_list_item_expanded');

            if (elements) {
              elements.forEach(x => x.className = "task_list_item_expanded show-expand")
            }

          } catch (error) {
            reject(error);
          }

        }
      }).then(function (canvas) {

        let splitAt = self.mobileOrTablet ? 520 : 1050;
        // let splitAt = 1000;

        let images = [];
        let y = 0;
        while (canvas.height > y) {
          const region = self.getClippedRegion(canvas, 0, y, canvas.width, splitAt)

          // Stop generate image if blank
          if (!region.image) {
            break;
          }

          images.push(region);
          y += splitAt;
        }
        resolve(images);

      }).catch(function (error) {
        reject(error);
      });
    });

  }

  getClippedRegion(image, x, y, width, height) {
    var canvas = document.createElement("canvas"),
      ctx = canvas.getContext("2d");

    canvas.width = width;
    canvas.height = height;

    ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

    let contentLocation = ctx.getImageData(0, 0, canvas.width, canvas.height).data.find(x => x !== 255);

    if (!contentLocation) {
        return { image: undefined };
    }

    return {
      image: canvas.toDataURL(),
      width: 520,
      alignment: 'center'
    };
  }

  hasAnticipatedWageType(types: UserGapYearPlanType[]): boolean {

    if (!types || types.length == 0) {
      return false;
    }

    // 1	Study Abroad
    // 2	Work Abroad
    // 3	College Sponsored
    // 4	Intership/Work
    // 5	Volunteer
    // 6	Travel
    for (let x of types) {
      if (x.isSelected) {
        if (x.id == 2 || x.id == 4) {
          return true;
        }
      }
    }

    return false;
  }

  getAcademyName(unitId: number, collegeTypeId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId && x.collegeTypeId == collegeTypeId);
    if (academy) {
      if (academy.alternativeName) {
        return academy.alternativeName;
      } else {
        return academy.schoolProfile.institutionName;
      }
    } else {
      return unitId;
    }
  }

  getAcademy(unitId: number, collegeTypeId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId && x.collegeTypeId == collegeTypeId);
    if (academy) {
      return academy;
    } else {
      return null;
    }
  }

  openServiceContactUsDialog(service) {

    var item;

    for (var i = 0; i < this.serviceContacts.length; i++) {
      if (service == this.serviceContacts[i].name) {
        item = this.serviceContacts[i];
        break;
      }
    }

    this._matDialog.open(ContactRecruiterDialogComponent, {
      data: item,
      panelClass: 'custom-dialog',
      height: '95%',
    });
  }

  async getAllUserOccupationPlanCalendarTasks() {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getPlanTasks(this.userOccupationPlan.id);

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getAllUserOccupationPlanCalendars() {

    try {

      let userOccupationPlanCalendars = await this._planCalendarService.getPlanCalendars(this.userOccupationPlan.id);

      return await userOccupationPlanCalendars;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

  hasUrl(taskName): boolean {
    if (this._utilityService.isJson(taskName)) {
      return true;
    }

    return false;
  }

  getUrl(taskName) {
    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).url;
    }

    return taskName;
  }

  getResourceLinks(userMilitaryPlans: any[]) {

    let returnResourceLinks = [];
    let didSelectAirForce: boolean = false;
    let didSelectArmy: boolean = false;
    let didSelectCoastGuard: boolean = false;
    let didSelectMarineCorps: boolean = false;
    let didSelectNavy: boolean = false;
    let didSelectSpaceForce: boolean = false;

    let didSelectReserve: boolean = false;
    let didSelectNationalGuard: boolean = false;
    let didSelectROTC: boolean = false;
    let academies = [];

    userMilitaryPlans.forEach((userMilitaryPlan: UserMilitaryPlan) => {

      if (!didSelectAirForce) {
        //only need to set once
        if (userMilitaryPlan.didSelectAirForce) {
          didSelectAirForce = true;
        }
      }

      if (!didSelectArmy) {
        //only need to set once
        if (userMilitaryPlan.didSelectArmy) {
          didSelectArmy = true;
        }
      }

      if (!didSelectCoastGuard) {
        //only need to set once
        if (userMilitaryPlan.didSelectCoastGuard) {
          didSelectCoastGuard = true;
        }
      }

      if (!didSelectMarineCorps) {
        //only need to set once
        if (userMilitaryPlan.didSelectMarineCorps) {
          didSelectMarineCorps = true;
        }
      }

      if (!didSelectNavy) {
        //only need to set once
        if (userMilitaryPlan.didSelectNavy) {
          didSelectNavy = true;
        }
      }

      if (!didSelectSpaceForce) {
        //only need to set once
        if (userMilitaryPlan.didSelectSpaceForce) {
          didSelectSpaceForce = true;
        }
      }

      if (!didSelectReserve) {
        //only need to set once
        if (userMilitaryPlan.isInterestedInReserves) {
          didSelectReserve = true;
        }
      }

      if (!didSelectNationalGuard) {
        //only need to set once
        if (userMilitaryPlan.isInterestedInNationalGuard) {
          didSelectNationalGuard = true;
        }
      }

      if (!didSelectROTC) {
        //only need to set once
        if (userMilitaryPlan.rotcPrograms.length > 0) {
          didSelectROTC = true;
        }
      }

      if (userMilitaryPlan.serviceColleges.length > 0) {
        userMilitaryPlan.serviceColleges.forEach(x => academies.push(x));
      }

    })

    if (didSelectArmy) {
      returnResourceLinks.push({ url: "https://www.goarmy.com/", title: "https://www.goarmy.com/" });
    }
    if (didSelectMarineCorps) {
      returnResourceLinks.push({ url: "https://www.marines.com/", title: "https://www.marines.com/" });
    }
    if (didSelectNavy) {
      returnResourceLinks.push({ url: "https://www.navy.com/", title: "https://www.navy.com/" });
    }
    if (didSelectAirForce) {
      returnResourceLinks.push({ url: "https://www.airforce.com/", title: "https://www.airforce.com/" });
    }
    if (didSelectSpaceForce) {
      returnResourceLinks.push({ url: "https://www.spaceforce.mil/", title: "https://www.spaceforce.mil/" });
    }
    if (didSelectCoastGuard) {
      returnResourceLinks.push({ url: "https://www.gocoastguard.com/", title: "https://www.gocoastguard.com/" });
    }
    if (didSelectROTC) {
      returnResourceLinks.push({ url: "https://www.careersinthemilitary.com/options-becoming-an-officer/rotc", title: "https://www.careersinthemilitary.com/options-becoming-an-officer" });
    }
    if (didSelectNationalGuard) {
      if (didSelectArmy) {
        returnResourceLinks.push({ url: "https://www.nationalguard.com/", title: "Army National Guard" });
      }

      if (didSelectAirForce) {
        returnResourceLinks.push({ url: "https://www.goang.com/", title: "Air National Guard" });
      }

    }
    if (didSelectReserve) {
      if (didSelectArmy) {
        returnResourceLinks.push({ url: "https://www.goarmy.com/about/serving-in-the-army/serve-your-way/army-reserve.html", title: "Army National Guard" });
      }
      if (didSelectMarineCorps) {
        returnResourceLinks.push({ url: "https://www.marines.com/", title: "Marine Corps Reserve" });
      }
      if (didSelectNavy) {
        returnResourceLinks.push({ url: "https://www.navy.com/", title: "Navy Reserve" });
      }
      if (didSelectAirForce) {
        returnResourceLinks.push({ url: "https://afreserve.com/", title: "Air Force Reserve" });
      }
      if (didSelectCoastGuard) {
        returnResourceLinks.push({ url: "https://www.gocoastguard.com/", title: "Coast Guard Reserve" });
      }
    }

    if (academies.length > 0) {
      academies.forEach((academy: any) => {


        let academyInfo = this.getAcademy(academy.unitId, academy.collegeTypeId);

        if (academyInfo) {

          let academyName = "";
          if (academyInfo.alternativeName) {
            academyName = academyInfo.alternativeName;
          } else {
            academyName = academyInfo.schoolProfile.institutionName;
          }

          returnResourceLinks.push({ url: academyInfo.schoolProfile.url, title: academyName });

        }

      });
    }

    return returnResourceLinks;
  }

  showSubmitToMentor() {

    //retrieve current user and user role
    let currentUser = this._userService.getUser() ? this._userService.getUser().currentUser : null;
    let userRole = currentUser ? currentUser.role : null;

    if (userRole != 'S') {
      const data = {
        'title': 'Plan Submission',
        'message': 'Students will be able to submit their Plan to their mentors, as well as send comments.'
      };

      const dialogRef = this._matDialog.open(MessageDialogComponent, {
        data: data,
        maxWidth: '400px',
      });
    } else {
      let data = {
        title: "Plan",
        userOccupationPlan: this.userOccupationPlan
      }

      this._matDialog.open(SubmitToMentorDialogComponent, { data: data, maxWidth: '500px', autoFocus: false }).afterClosed().subscribe(result => {

        if (result) {

          //do something
        }
      })
    }

  };

}
