import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { forEach } from '@angular/router/src/utils/collection';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { SearchSchoolsDialogComponent } from 'app/core/dialogs/search-schools-dialog/search-schools-dialog.component';
import { FavoriteSchool } from 'app/core/models/favoriteSchool.model';
import { SchoolProfile } from 'app/core/models/schoolProfile.model';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { UserSchoolPlan } from 'app/core/models/userSchoolPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { CollegePlanObservableService } from 'app/services/college-plan-observable.service';
import { CollegePlanService } from 'app/services/college-plan.service';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { SchoolPlanService } from 'app/services/school-plan.service';
import { UtilityService } from 'app/services/utility.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { resolve } from 'url';

@Component({
  selector: 'app-college-plan',
  templateUrl: './college-plan.component.html',
  styleUrls: ['./college-plan.component.scss']
})
export class CollegePlanComponent implements OnInit, OnDestroy {
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() occupationDescription: string;
  @Input() selectedPathways: any;

  @Output() onAutoCalendarDatesCreated: EventEmitter<UserOccupationPlanCalendar[]>;
  @Output() onRemoval: EventEmitter<{ id: number, autoCalendars: any[] }>;


  public favoriteColleges: FavoriteSchool[];  //filtered list that is displayed
  private allFavoriteColleges: FavoriteSchool[]; //list of all favorite colleges unfiltered
  private favoriteSchoolsSelected: any[];
  public selectedSchools: UserCollegePlan[];  //used for edit/view
  public addAnotherSchool: boolean;
  private updateCollegePlanSubmitted: boolean[];
  private readonly onDestroy = new Subject<void>();
  public occuMajors: string[];
  public wasSaved: boolean;

  constructor(
    private _collegePlanService: CollegePlanService,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _matDialog: MatDialog,
    private _favoritesRestFactory: FavoritesRestFactoryService,
    private _collegePlanObservableService: CollegePlanObservableService,
    private _utilityService: UtilityService,
    private _planCalendarService: PlanCalendarService,
    private _schoolPlanService: SchoolPlanService
  ) {
    this.selectedSchools = [];
    this.addAnotherSchool = true;
    this.updateCollegePlanSubmitted = [];
    this.favoriteSchoolsSelected = [];
    this.occuMajors = [];
    this.onAutoCalendarDatesCreated = new EventEmitter();
    this.onRemoval = new EventEmitter();
    this.wasSaved = false;

    this._collegePlanObservableService.collegePlanChanged$.pipe(takeUntil(this.onDestroy)).subscribe((userCollegePlans: UserCollegePlan[]) => {

      this.updateCollegePlanSubmitted = [];

      if (userCollegePlans.length > 0) {
        this.addAnotherSchool = false;
      }

      for (let collegePlan of userCollegePlans) {

        //does this college plan already exist in the ui?
        let selectedSchool = this.selectedSchools.find(x => x.id == collegePlan.id);

        if (selectedSchool) {
          selectedSchool = collegePlan;
          this.updateCollegePlanSubmitted.push(false);
        } else {

          //get school profile major info as it is excluded from the getAll call
          this._occufindRestFactoryService.getSchoolProfile(collegePlan.unitId).toPromise()
            .then((schoolProfile: SchoolProfile) => {
              //combine objects to display to user and allow entry
              collegePlan.schoolProfile = schoolProfile;
              collegePlan.schoolProfile = this.getSchoolMajors(collegePlan.schoolProfile);
              this.selectedSchools.push(collegePlan);
              this.updateCollegePlanSubmitted.push(false);
            });
        }
      };

    });

  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
    this._collegePlanObservableService.unsubscribe();
  }

  ngOnInit() {

    //get all possible majors for this occupation
    this.getOccuMajors(this.userOccupationPlan.socId).then((returnValues: any) => {
      for (var occuTitle of returnValues) {
        this.occuMajors.push(occuTitle)
      }
      //get/set all college plans associated to the user if any
      this.getAllCollegePlans();
    }).catch((error) => console.error("ERROR", error));

  }

  getAllCollegePlans() {

    this._collegePlanService.getAll(this.userOccupationPlan.id).then((values: UserCollegePlan[]) => {
      let userCollegePlans = values;

      this._collegePlanObservableService.set(userCollegePlans); //send values to the observable service;

      this.getAllFavoriteSchools();
    }).catch(error => {
      console.error("Error", error);
    });
  }

  getSchoolMajors(schoolProfile: SchoolProfile) {
    var returnSchoolProfile = schoolProfile;
    var cepTitles: any = [];
    for (var collegeMajor of Object.keys(schoolProfile.collegeMajors)) {
      for (var collegeDegree of schoolProfile.collegeMajors[collegeMajor]) {
        if (this.occuMajors.find(x => x.toUpperCase() == collegeDegree.cipTitle.toUpperCase())) {
          cepTitles.push(collegeDegree);
        }
      }
    }
    returnSchoolProfile.collegeMajors = undefined;
    returnSchoolProfile.collegeMajors = cepTitles;
    return (returnSchoolProfile);
  }

  async getOccuMajors(socId) {
    let x = await this._occufindRestFactoryService.getSchoolMajorsForCP(socId);
    return x;
  }

  /**
   * Retrieve all favorite colleges related to the user and only for the selected occupation for the plan
   */
  async getAllFavoriteSchools() {
    let self = this;
    this._favoritesRestFactory.getFavoriteSchoolBySocId(this.userOccupationPlan.socId).then(async (favoriteSchools: FavoriteSchool[]) => {
      //only allow selection of colleges that have not already been selected
      let favoriteSchoolsToShow: FavoriteSchool[] = [];

      //filter through and only show favorite schools based on the pathway selected
      if (favoriteSchools) {

        await Promise.all(favoriteSchools.map(async (favoriteSchool: FavoriteSchool) => {
          let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(favoriteSchool.unitId).toPromise();
          if (schoolProfile) {
            if (this._utilityService.containsPathways(this.selectedPathways, 'certificate', 'technicalSchool', 'vocationalTraining', 'careerTechnicalEducation')) {
              if (schoolProfile.certificate) {
                favoriteSchoolsToShow.push(favoriteSchool);
                return;
              }
            }
            if (this._utilityService.containsPathways(this.selectedPathways, 'associatesDegree', 'technicalSchool', 'vocationalTraining', 'careerTechnicalEducation')) {
              if (schoolProfile.associates) {
                favoriteSchoolsToShow.push(favoriteSchool);
                return;
              }
            }
            //only include military when they are interested in becoming an officer
            if (this._utilityService.containsPathways(this.selectedPathways, 'bachelorsDegree', 'military')) {
              if (schoolProfile.bachelors) {
                favoriteSchoolsToShow.push(favoriteSchool);
                return;
              }
            }
            if (this._utilityService.containsPathways(this.selectedPathways, 'advanceDegree')) {
              if (schoolProfile.doctoral || schoolProfile.masters) {
                favoriteSchoolsToShow.push(favoriteSchool);
                return;
              }
            }
          }

        }));


      }


      this.allFavoriteColleges = favoriteSchoolsToShow;
      this.favoriteColleges = favoriteSchoolsToShow.filter(function (favoriteSchool: FavoriteSchool) {
        if (!self.selectedSchools.find(x => x.unitId == favoriteSchool.unitId)) {
          return favoriteSchool;
        }
      });

    }).catch(error => {
      console.error("Error", error);
    });

  }

  addSchoolSelected(school, event) {
    if (event.target.checked) {
      this.favoriteSchoolsSelected.push(school);
    } else {
      this.favoriteSchoolsSelected.splice(this.favoriteSchoolsSelected.findIndex(e => e.unitId === school.unitId), 1);
    }

  }


  /**
   * Creates the initial UserCollegePlan record based on the unit id passed
   * @param unitId 
   */
  saveFavoriteCollege(unitId: number) {

    let formData: UserCollegePlan = {
      id: null,
      planId: this.userOccupationPlan.id,
      unitId: unitId,
      isSeekingInStateTuition: null,
      degree: null,
      major: null,
      familyContribution: null,
      scholarshipContribution: null,
      savings: null,
      loan: null,
      other: null,
      schoolProfile: null
    };

    this.saveCollegePlan(formData);

  }

  updateCollegePlan(data: UserCollegePlan, index, familyContribution, other, loan, savings, scholarshipContribution, isSeekingInStateTuition, major, degree) {

    this.updateCollegePlanSubmitted[index] = true;
    let formData: UserCollegePlan = {
      id: data.id,
      planId: data.planId,
      unitId: data.unitId,
      isSeekingInStateTuition: data.isSeekingInStateTuition,
      degree: data.degree,
      major: data.major,
      familyContribution: data.familyContribution,
      scholarshipContribution: data.scholarshipContribution,
      savings: data.savings,
      loan: data.loan,
      other: data.other,
      schoolProfile: null
    };

    //save to database
    this._collegePlanService.update(formData.planId, formData.id, formData).then((rowsAffected: any) => {
      if (rowsAffected > 0) {
        //record was successfully saved

        //was degree that was selected a 4 year degree?

        this.updateCollegePlanSubmitted[index] = false;
        familyContribution.control.markAsPristine();
        other.control.markAsPristine();
        loan.control.markAsPristine();
        savings.control.markAsPristine();
        scholarshipContribution.control.markAsPristine();
        isSeekingInStateTuition.control.markAsPristine();
        major.control.markAsPristine();
        degree.control.markAsPristine();
        formData.schoolProfile = data.schoolProfile;
        this._collegePlanObservableService.update(formData);

        this.startTimer();
      } else {
        window.alert("Error: record was not updated.  Please try again.");
      }
    }).catch(error => {
      console.error("Error: ", error);
      this.updateCollegePlanSubmitted[index] = false;
    });

  }

  /**
 * used when the 'Next'button is clicked
 * @param unitId 
 */
  saveFavoriteColleges() {

    for (let school of this.favoriteSchoolsSelected) {
      this.saveFavoriteCollege(school.unitId);
    }

    this.favoriteSchoolsSelected = [];  //reset list

  }

  /**
   * Creates a new user college plan in the database
   * based on the plan and unit id's.
   * @param formData 
   */
  saveCollegePlan(formData: UserCollegePlan) {
    //save to database
    this._collegePlanService.save(this.userOccupationPlan.id, formData).then((newRecord: UserCollegePlan) => {
      this._occufindRestFactoryService.getSchoolProfile(newRecord.unitId).toPromise()
        .then((schoolProfile: SchoolProfile) => {
          //combine objects to display to user and allow entry
          newRecord.schoolProfile = schoolProfile;

          newRecord.schoolProfile = this.getSchoolMajors(newRecord.schoolProfile);
          this.selectedSchools.push(newRecord);
          this.refreshFavoriteColleges();
          this.addAnotherSchool = false;
          this.updateCollegePlanSubmitted.push(false);

          this._collegePlanObservableService.add(newRecord);

          //do automatic tasks already exist?
          //assumption that both FAFSA dates are always created
          //therefore only need to check for one task title 
          this._planCalendarService.checkAutoTaskExistsForPlan('UserCollegePlan', this.userOccupationPlan.id, formData.id, schoolProfile.institutionName + ': FAFSA open date').then(
            (value: boolean) => {

              if (!value) {
                //auto tasks do not exist for the college plan;
                //create only if graduation date exists

                //get graduation date
                this._schoolPlanService.get(this.userOccupationPlan.id).then((userSchoolPlan: UserSchoolPlan) => {

                  //create tasks only if graduation date exists;
                  if (userSchoolPlan.graduationDate) {
                    //create a FAFSA tasks assigned to a calendar date
                    let graduationYear = new Date(this._utilityService.convertServerUTCToUniversalUTC(userSchoolPlan.graduationDate)).getFullYear();

                    
                    // Oct 1 , same year as graduation
                    let x = this._planCalendarService.saveAutoDateForCollegePlan(formData.planId,
                      schoolProfile.institutionName + ': FAFSA open date', new Date(graduationYear, 9, 1));

                    // June 30, same year as graduation
                    let y = this._planCalendarService.saveAutoDateForCollegePlan(formData.planId,
                      schoolProfile.institutionName + ': FAFSA close date', new Date(graduationYear, 5, 30));

                    Promise.all([x, y]).then((done: any) => {

                      let emitDates = [];
                      emitDates.push(done[0]);
                      emitDates.push(done[1]);
                      this.onAutoCalendarDatesCreated.emit(emitDates);
                      this.startTimer();

                    });
                  }
                });
              }

            }
          )

          // this.selectedSchools.push({ ...newRecord, ...this.getSchoolMajors(schoolProfile) });
          // this.refreshFavoriteColleges();
          // this.addAnotherSchool = false;
        });
    }).catch(error => {
      window.alert("Error: " + error);
    });
  }

  removeCollegePlan(collegePlan: UserCollegePlan, index) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove College Plan for ' + collegePlan.schoolProfile.institutionName + ', Are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '400px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          //remove record from db
          this._collegePlanService.delete(collegePlan.planId, collegePlan.id).then((rowsAffected: any) => {

            let schoolRemoved = this.selectedSchools[index].schoolProfile.institutionName;

            //remove record
            this.selectedSchools.splice(index, 1);
            this._collegePlanObservableService.remove(collegePlan.id);

            this.refreshFavoriteColleges();
            if (this.selectedSchools.length <= 0) {
              this.addAnotherSchool = true;
            }

            //remove associated tasks if any
            this._planCalendarService.getPlanCalendars(collegePlan.planId).then((response:UserOccupationPlanCalendar[]) =>{


              let autoCalendars = response.filter(x => (x.title.toUpperCase() == (schoolRemoved + ": FAFSA OPEN DATE").toUpperCase() 
                || x.title.toUpperCase() == (schoolRemoved + ": FAFSA CLOSE DATE").toUpperCase()));
              let calendarsToRemove:any = [];

              if (autoCalendars) {
        
                autoCalendars.forEach((calendar: UserOccupationPlanCalendar) => {
                  calendarsToRemove.push(calendar);
                  // let hasAutoTasks = calendar.tasks.filter(x => this._utilityService.isJson(x.taskName));

                  // //if the calendar has auto tasks for the college plan
                  // //then delete
                  // if(hasAutoTasks){
                  //   hasAutoTasks.forEach((task: UserOccupationPlanCalendarTask) => {
                  //     if(task.planId == collegePlan.planId){
                  //       calendarsToRemove.push(calendar);
                  //     }

                  //   });

                  // }

                });
        
                Promise.all(calendarsToRemove.map((x: UserOccupationPlanCalendar) => {
                  return this._planCalendarService.deleteCalendar(x.id);
                }));
        
              }

              //update ui
              this.onRemoval.emit({ id: collegePlan.id, autoCalendars: calendarsToRemove });

            })

          }).catch(error => {
            console.error("Error!!!!: ", error);
          });

        }
      }
    });


  }


  /**
   * refreshes the favorite college list;
   */
  refreshFavoriteColleges() {
    let self = this;
    this.favoriteColleges = this.allFavoriteColleges.filter(function (favoriteSchool: FavoriteSchool) {
      if (!self.selectedSchools.find(x => x.unitId == favoriteSchool.unitId)) {
        return favoriteSchool;
      }
    });
  }

  /**
   * calculates and then returns the tuition source; Takes the in-state or out of state tuition
   * and subtracts based on source values in the text boxes below
   * @param collegePlan 
   */
  calculateTuitionSrc(collegePlan: UserCollegePlan): string {
    return ((collegePlan.familyContribution || 0) +
      (collegePlan.scholarshipContribution || 0) +
      (collegePlan.savings || 0) +
      (collegePlan.loan || 0) +
      (collegePlan.other || 0)).toString();
  }

  showSearchDialog() {

    let dialogData = {
      userOccupationPlan: this.userOccupationPlan,
      selectedSchools: this.selectedSchools,
      occupationDescription: this.occupationDescription,
      selectedPathways: this.selectedPathways
    };

    this._matDialog.open(SearchSchoolsDialogComponent, {
      data: dialogData,
      maxWidth: '800px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response != 'cancel') {
          this.saveFavoriteCollege(response);
        }
      }
    });

  }

  startTimer() {
    this.wasSaved = true;
    let self = this;
    setTimeout(function () {
      self.wasSaved = false;
    }, 2000);
  }

}
