import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegePlanComponent } from './college-plan.component';

describe('CollegePlanComponent', () => {
  let component: CollegePlanComponent;
  let fixture: ComponentFixture<CollegePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollegePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
