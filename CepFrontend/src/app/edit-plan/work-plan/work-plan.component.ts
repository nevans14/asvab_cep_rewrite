import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CertificationListComponent } from 'app/core/dialogs/career-one-stop-dialogs/certification-list/certification-list.component';
import { LicenseListComponent } from 'app/core/dialogs/career-one-stop-dialogs/license-list/license-list.component';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserWorkPlan } from 'app/core/models/userWorkPlan.model';
import { OccufindCareerOneFactoryService } from 'app/occufind/occupation-details/services/occufind-career-one-factory.service';
import { UtilityService } from 'app/services/utility.service';
import { WorkPlanService } from 'app/services/work-plan.service';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-work-plan',
  templateUrl: './work-plan.component.html',
  styleUrls: ['./work-plan.component.scss']
})
export class WorkPlanComponent implements OnInit {
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() occupationDescription: string;
  @Input() selectedPathways: any;

  occupationCertificates: any;
  workPlans: any[];
  workPlan: string;
  public userWorkPlan: UserWorkPlan;
  public socIdToUseForLookups: number;
  public isSaving: boolean;
  public wasSaved: boolean;

  constructor(private _dialog: MatDialog,
    private _occufindCareerOneFactoryService: OccufindCareerOneFactoryService,
    private _workPlanService: WorkPlanService,
    private _utilityService: UtilityService) {

    this.workPlans = [];
    this.workPlan = "";
    this.socIdToUseForLookups = 0;
    this.isSaving = false;
    this.wasSaved = false;
  }

  ngOnInit() {

    this.setOccupationCertificates();

    //get work plan if it exists
    this.getUserWorkPlan();

  }

  async setOccupationCertificates() {

    try {
      let crosswalk: any = await this._occufindCareerOneFactoryService.getOccupationSocCrosswalk(this.userOccupationPlan.socId);
      this.socIdToUseForLookups = (crosswalk) ? ((crosswalk.onetSoc) ? crosswalk.onetSoc : this.userOccupationPlan.socId) : this.userOccupationPlan.socId;

      this.occupationCertificates = await this._occufindCareerOneFactoryService
        .getOccupationCertificates(this.socIdToUseForLookups).toPromise();

    } catch (error) {
      this.occupationCertificates = null;
    }

    try {
      this.occupationCertificates = JSON.parse(this.occupationCertificates.payload);
    } catch (error) {
      if (this.occupationCertificates) {
        this.occupationCertificates = this.occupationCertificates.payload;
      }
    }

  }

  openCertificationList() {

    const data = {
      certs: this.occupationCertificates,
      occupationTitle: this.occupationDescription,
      selectionMode: true,
      hasBackdrop: true
    };

    const dialogRef1 = this._dialog.open(CertificationListComponent, {
      data: data,
      autoFocus: false,
      maxHeight: 700,
      maxWidth: '800px'
    })

    dialogRef1.afterClosed().subscribe(response => {
      if (response) {
        if (Array.isArray(response) && response.length > 0) {
          this.workPlans.push(response);
          this.workPlan = this.workPlan + ' ' + this.workPlans.join(", ");
        }
      }
    });

  }

  openLicenseList() {

    const data = {
      onetOccupationCode: this.socIdToUseForLookups,
      occupationTitle: this.occupationDescription,
      selectionMode: true,
      hasBackdrop: true
    };

    const dialogRef1 = this._dialog.open(LicenseListComponent, {
      data: data,
    }).afterClosed().subscribe(response => {
      if (response) {
        if (Array.isArray(response) && response.length > 0) {
          this.workPlans.push(response);
          this.workPlan = this.workPlan + ' ' + this.workPlans.join(", ");
        }
      }
    });

  }

  isDisabled() {

    if (this.userWorkPlan) {
      if (this.userWorkPlan.workPlan == this.workPlan) {
        return true;
      }

      if (this.userWorkPlan.workPlan == '' && this.workPlan == '') {
        return true;
      }

      return false;
    } else {
      return true;
    }

  }

  saveWorkPlan() {

    this.isSaving = true;

    let formData: UserWorkPlan = {
      planId: this.userOccupationPlan.id,
      workPlan: this.workPlan
    }

    if (this.userWorkPlan.planId) {
      this._workPlanService.update(this.userOccupationPlan.id, formData).then((rowsAffected) => {
        this.userWorkPlan = {
          planId: formData.planId,
          workPlan: formData.workPlan
        }
        this.isSaving = false;
        // this.wasSaved = true;

        this.startTimer();

      }).catch(error => {
        this.isSaving = false;
        console.error("ERROR", error);
      });
    } else {
      this._workPlanService.save(this.userOccupationPlan.id, formData).then((rowsAffected) => {
        this.userWorkPlan = {
          planId: formData.planId,
          workPlan: formData.workPlan
        }

        // this.wasSaved = true;
        this.startTimer();

      }).catch(error => {
        this.isSaving = false;
        console.error("ERROR", error);
      });
    }

  }

  startTimer() {
    let self = this;
    this.wasSaved = true;
    setTimeout(function () {
      self.wasSaved = false;
    }, 2000);
  }

  async getUserWorkPlan() {

    try {
      this.userWorkPlan = await this._workPlanService.get(this.userOccupationPlan.id).then((response: UserWorkPlan) => { return response; });

      if (!this.userWorkPlan) {
        this.userWorkPlan = {
          planId: null,
          workPlan: ''
        };
      } else {
        this.userWorkPlan = {
          planId: this.userWorkPlan.planId,
          workPlan: this.userWorkPlan.workPlan
        };
        this.workPlan = this.userWorkPlan.workPlan
      }
    } catch (error) {
      this.userWorkPlan = {
        planId: null,
        workPlan: ''
      };
    }

  }

}
