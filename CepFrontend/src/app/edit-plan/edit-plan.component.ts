import { Component, OnInit, ViewChild} from '@angular/core';
import { MatDialog} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { UserOccupationPlanService } from 'app/services/user-occupation-plan.service';
import html2canvas from "html2canvas";
import { UserGapYearPlanType } from 'app/core/models/userGapYearPlanType.model';
import { UtilityService } from 'app/services/utility.service';
import { Subject } from 'rxjs';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { CalendarPlanComponent } from './calendar-plan/calendar-plan.component';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-edit-plan',
  templateUrl: './edit-plan.component.html',
  styleUrls: ['./edit-plan.component.scss']
})
export class EditPlanComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs

  public userOccupationPlan: UserOccupationPlan;
  public occupationDescription: string;
  public userCollegePlans: UserCollegePlan[];
  public nationalSalary: any;
  public nationalEntrySalary: any;
  public salaryType: any;
  public printObj: any;
  public notes: string;
  public selectedPathways;
  public availableSchools: any[];
  public newCalendarTasks: UserOccupationPlanCalendar[];

  @ViewChild(CalendarPlanComponent) child: CalendarPlanComponent;

  private readonly onDestroy = new Subject<void>();

  constructor(private _activatedRoute: ActivatedRoute,
    private _userOccupationPlanService: UserOccupationPlanService,
    private _matDialog: MatDialog,
    private _router: Router,
    public _utilityService: UtilityService,
    private _userService: UserService,
  ) {

    this.printObj = {};
    this.notes = "";
    this.newCalendarTasks = [];
  }

  ngOnInit() {
    if (window.innerWidth < 640) {
      this.leftNavCollapsed = false;
    }

    this.userOccupationPlan = this._activatedRoute.snapshot.data.editPlanResolver[0];
    this.selectedPathways = this._utilityService.parsePathway(this.userOccupationPlan.pathway);
    this.occupationDescription = this._activatedRoute.snapshot.data.editPlanResolver[1];
    this.nationalSalary = this._activatedRoute.snapshot.data.editPlanResolver[2];
    this.nationalEntrySalary = this._activatedRoute.snapshot.data.editPlanResolver[3];

    this.salaryType = (this.userOccupationPlan.useNationalAverage) ? "national" : "entry";

    this.notes = this.userOccupationPlan.additionalNotes;

    this._userService.setCompletion(UserService.CREATED_PLAN, 'true');
  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  removePlan(planId, occupationDescription) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Plan for ' + occupationDescription + ', Are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '400px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._userOccupationPlanService.delete(planId).then((rowsAffected: number) => {
            //redirect
            this._router.navigate(['/plan-your-future']);

          }).catch(error => {
            console.error("Error:", error);
          });

        }
      }
    });
  }

  formatTypes(types: UserGapYearPlanType[]): string {

    if (!types || types.length == 0) {
      return "";
    }

    let names: string[] = [];

    for (let x of types) {
      if (x.isSelected) {
        names.push(x.name);
      }
    }

    return names.join(", ");

  }

  saveAndClosePlan() {

    let formData: UserOccupationPlan = {
      id: this.userOccupationPlan.id,
      socId: this.userOccupationPlan.socId,
      pathway: this.userOccupationPlan.pathway,
      additionalNotes: this.notes,
      useNationalAverage: (this.salaryType=='national') ? true : false,
      statusId: this.userOccupationPlan.statusId
    }

    this._userOccupationPlanService.update(formData, formData.id).then((rowsAffected: number) => {
      if (rowsAffected > 0) {
        this._router.navigateByUrl('/view-plan/' + formData.id);
      } else {
        console.error("Error updating record.");
      }
    }).catch(error => {
      console.error("Error: ", error);
    });

  }

  hasAnticipatedWageType(types: UserGapYearPlanType[]): boolean {

    if (!types || types.length == 0) {
      return false;
    }

    // 1	Study Abroad
    // 2	Work Abroad
    // 3	College Sponsored
    // 4	Intership/Work
    // 5	Volunteer
    // 6	Travel
    for (let x of types) {
      if (x.isSelected) {
        if (x.id == 2 || x.id == 4) {
          return true;
        }
      }
    }

    return false;
  }

  onAutoCalendarTasksCreated(event) {

    if (Array.isArray(event)) {

      //create initial filter option for dates with tasks
      //create initial option for expanding list to true
      event.map((userOccupationPlanCalendar) => {
        userOccupationPlanCalendar.showCompletedTasks = false;
        userOccupationPlanCalendar.expanded = true;

        return userOccupationPlanCalendar;
      });

      event.forEach(x => this.child.userOccupationPlanCalendars.push(x));


    } else {
      event.showCompletedTasks = false;
      event.expanded = true;
      this.child.userOccupationPlanCalendars.push(event);
    }

  }

  onAutoCalendarDatesCreated(event){

    if (Array.isArray(event)) {

      //create initial filter option for dates with tasks
      //create initial option for expanding list to true
      event.map((userOccupationPlanCalendar) => {
        userOccupationPlanCalendar.showCompletedTasks = false;
        userOccupationPlanCalendar.expanded = true;

        return userOccupationPlanCalendar;
      });

      event.forEach(x => this.child.userOccupationPlanCalendars.push(x));

    } else {
      event.showCompletedTasks = false;
      event.expanded = true;
      this.child.userOccupationPlanCalendars.push(event);
    }

  }

  onAutoTasksCreated(event) {

    if (Array.isArray(event)) {
      event.forEach(x => this.child.userOccupationPlanCalendarTasks.push(x));
    } else {
      this.child.userOccupationPlanCalendarTasks.push(event);
    }

    this.child.setUnassignedTasks();

  }

  onAutoTasksDelete(event) {

    if (Array.isArray(event)) {
      event.forEach((x: UserOccupationPlanCalendarTask) => {
        let removeIndex = this.child.userOccupationPlanCalendarTasks.findIndex(y => y.id == x.id);
        this.child.userOccupationPlanCalendarTasks.splice(removeIndex, 1);
      });

    } else {
      let removeIndex = this.child.userOccupationPlanCalendarTasks.findIndex(y => y.id == event.id);
      this.child.userOccupationPlanCalendarTasks.splice(removeIndex, 1);
    }
    this.child.setUnassignedTasks();

  }

  onAutoCalendarsDelete(event){

    if(event.autoCalendars){
      if (Array.isArray(event.autoCalendars)) {
        event.autoCalendars.forEach((x: UserOccupationPlanCalendar) => {
          let removeIndex = this.child.userOccupationPlanCalendars.findIndex(y => y.id == x.id);
          this.child.userOccupationPlanCalendars.splice(removeIndex, 1);
        });
  
      } else {
        let removeIndex = this.child.userOccupationPlanCalendars.findIndex(y => y.id == event.id);
        this.child.userOccupationPlanCalendars.splice(removeIndex, 1);
      }
      this.child.setUnassignedTasks();
    }

  }

}