import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighSchoolPlanComponent } from './high-school-plan.component';

describe('HighSchoolPlanComponent', () => {
  let component: HighSchoolPlanComponent;
  let fixture: ComponentFixture<HighSchoolPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighSchoolPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighSchoolPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
