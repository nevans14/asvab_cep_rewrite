import { Component, Input, OnInit } from '@angular/core';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserSchoolPlan } from 'app/core/models/userSchoolPlan.model';
import { SchoolPlanService } from 'app/services/school-plan.service';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-high-school-plan',
  templateUrl: './high-school-plan.component.html',
  styleUrls: ['./high-school-plan.component.scss']
})
export class HighSchoolPlanComponent implements OnInit {

  @Input() userOccupationPlan: UserOccupationPlan;

  public schoolPlanForm: FormGroup;

  public courses: FormArray;
  public activities: FormArray;

  private isLoading: boolean;
  private isSaving: boolean;
  public wasSaved: boolean;

  constructor(private _schoolPlanService: SchoolPlanService,
    private _formBuilder: FormBuilder,
    private _matDialog: MatDialog,
    private _utilityService: UtilityService) {

    this.isLoading = true;
    this.isSaving = false;
    this.schoolPlanForm = this._formBuilder.group({
      graduationDate: [''],
      courses: this._formBuilder.array([]),
      activities: this._formBuilder.array([])
    });
    this.wasSaved = false;

    this.courses = this.schoolPlanForm.get('courses') as FormArray;
    this.activities = this.schoolPlanForm.get('activities') as FormArray;

  }

  ngOnInit() {
    this._schoolPlanService.get(this.userOccupationPlan.id).then((userSchoolPlan: UserSchoolPlan) => {

      if (userSchoolPlan.graduationDate) {
        userSchoolPlan.graduationDate = new Date(this._utilityService.convertServerUTCToUniversalUTC(userSchoolPlan.graduationDate));
      }
      this.updateSchoolPlanView(userSchoolPlan);
      this.isLoading = false;
    }).catch(error => {
      this.isLoading = false;
      console.error("Error:", error);
    })

  }

  updateSchoolPlanView(userSchoolPlan: UserSchoolPlan) {

    userSchoolPlan.activities.forEach(t => {

      var activity = this._formBuilder.group({
        activity: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })

      this.activities.push(activity);

    });

    userSchoolPlan.courses.forEach(t => {

      var course = this._formBuilder.group({
        course: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })

      this.courses.push(course);

    });

    if (this.courses.length <= 0) {
      var course = this._formBuilder.group({
        course: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })

      this.courses.push(course);
    }

    if (this.activities.length <= 0) {
      var activity = this._formBuilder.group({
        activity: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })

      this.activities.push(activity);
    }

    this.schoolPlanForm.patchValue(userSchoolPlan);

  }

  addCourse() {
    this.courses.push(
      this._formBuilder.group({
        course: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })
    );
  }

  addActivity() {
    this.activities.push(
      this._formBuilder.group({
        activity: ['', Validators.maxLength(500)],
        location: ['', Validators.maxLength(500)],
        benefit: ['', Validators.maxLength(1000)],
        id: ['']
      })
    );
  }

  removeCourse(index: number, id: any) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Course, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          if (id) {
            this._schoolPlanService.deleteSchoolCourse(this.userOccupationPlan.id, id).then((rowsAffected: number) => {
              if (rowsAffected > 0) {
                this.courses.removeAt(index);
                // add empty course if there are no courses.
                if (this.courses.length <= 0) {
                  this.addCourse();
                }
              } else {
                console.error("No rows removed for: id", id);
              }
            }).catch(error => {
              console.error("Error:", error);
            })
          } else {
            this.courses.removeAt(index);
            //add empty course if there are no courses.
            if (this.courses.length <= 0) {
              this.addCourse();
            }
          }
        }
      }
    });
  }

  removeActivity(index: number, id: any) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Activity, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          if (id) {
            this._schoolPlanService.deleteSchoolActivity(this.userOccupationPlan.id, id).then((rowsAffected: number) => {
              if (rowsAffected > 0) {
                this.activities.removeAt(index);
                // add empty activity if there are no activities.
                if (this.activities.length <= 0) {
                  this.addActivity();
                }
              } else {
                console.error("No rows removed for: id", id);
              }
            }).catch(error => {
              console.error("Error:", error);
            })
          } else {
            this.activities.removeAt(index);
            // add empty activity if there are no activities.
            if (this.activities.length <= 0) {
              this.addActivity();
            }
          }
        }
      }
    });
  }

  updateSchoolPlan() {

    this.isSaving = true;

    // format formdata to send to server
    const formData: UserSchoolPlan = {
      planId: this.userOccupationPlan.id,
      graduationDate: (this.schoolPlanForm.value.graduationDate) ? this.schoolPlanForm.value.graduationDate : null,
      courses: this.schoolPlanForm.value.courses,
      activities: this.schoolPlanForm.value.activities
    };

    this._schoolPlanService.update(this.userOccupationPlan.id, formData).then((updatedUserSchoolPlan: UserSchoolPlan) => {

      this.schoolPlanForm.reset();

      //clear all courses
      const controlCourses = <FormArray>this.schoolPlanForm.controls['courses'];
      for (let i = controlCourses.length - 1; i >= 0; i--) {
        controlCourses.removeAt(i)
      }

      //clear all activities
      const controlActivities = <FormArray>this.schoolPlanForm.controls['activities'];
      for (let i = controlActivities.length - 1; i >= 0; i--) {
        controlActivities.removeAt(i)
      }

      //refresh the view with values from the server
      if (updatedUserSchoolPlan.graduationDate) {
        updatedUserSchoolPlan.graduationDate = new Date(this._utilityService.convertServerUTCToUniversalUTC(updatedUserSchoolPlan.graduationDate));
      }

      this.updateSchoolPlanView(updatedUserSchoolPlan);
      this.isSaving = false;
      this.startTimer();

    }).catch(error => {
      console.error("Error:", error);
      this.isSaving = false;
    });
  }

  startTimer() {
    this.wasSaved = true;
    let self = this;
    setTimeout(function () {
      self.wasSaved = false;
    }, 2000);
  }
}
