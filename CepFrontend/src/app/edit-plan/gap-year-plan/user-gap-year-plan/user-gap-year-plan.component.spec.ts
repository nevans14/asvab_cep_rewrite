import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGapYearPlanComponent } from './user-gap-year-plan.component';

describe('UserGapYearPlanComponent', () => {
  let component: UserGapYearPlanComponent;
  let fixture: ComponentFixture<UserGapYearPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGapYearPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGapYearPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
