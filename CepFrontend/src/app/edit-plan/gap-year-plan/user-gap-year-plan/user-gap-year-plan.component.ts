import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { DateDialogComponent } from 'app/core/dialogs/date-dialog/date-dialog.component';
import { SampleGapProgramsDialogComponent } from 'app/core/dialogs/sample-gap-programs-dialog/sample-gap-programs-dialog.component';
import { TaskDialogComponent } from 'app/core/dialogs/task-dialog/task-dialog.component';
import { RefGapYearPlan } from 'app/core/models/refGapYearPlan.model';
import { RefGapYearType } from 'app/core/models/refGapYearType.model';
import { UserGapYearPlan } from 'app/core/models/userGapYearPlan.model';
import { UserGapYearPlanType } from 'app/core/models/userGapYearPlanType.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { GapYearPlanService } from 'app/services/gap-year-plan.service';

@Component({
  selector: 'app-user-gap-year-plan',
  templateUrl: './user-gap-year-plan.component.html',
  styleUrls: ['./user-gap-year-plan.component.scss']
})
export class UserGapYearPlanComponent implements OnInit {

  @Input() userGapYearPlan: UserGapYearPlan;
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() gapYearTypes: RefGapYearType[];
  @Input() gapYearPlans: RefGapYearPlan[];

  @Output() onRemoval: EventEmitter<number>;
  @Output() onUpdate: EventEmitter<UserGapYearPlan>;
  @Output() onDateAdd: EventEmitter<UserOccupationPlanCalendar>;
  @Output() onTaskAdd: EventEmitter<UserOccupationPlanCalendarTask>;

  public userGapYearPlanForm: FormGroup;
  public updateUserGapYearPlanStepOneForm: FormGroup;
  public isSaving: boolean;
  public editStepOne: boolean;
  public types: FormArray;
  public wasSaved: boolean;

  constructor(private _matDialog: MatDialog,
    private _gapYearPlanService: GapYearPlanService,
    private _formBuilder: FormBuilder,
    private _dialog: MatDialog) {

    this.onRemoval = new EventEmitter();
    this.onUpdate = new EventEmitter();
    this.onDateAdd = new EventEmitter();
    this.onTaskAdd = new EventEmitter();
    this.wasSaved = false;

    this.userGapYearPlanForm = this._formBuilder.group({
      id: [''],
      planId: [''],
      gapYearId: [''],
      goals: [''],
      planName: [''],
      programCost: [''],
      costOfLiving: [''],
      fundingSource: [''],
      estimatedAmount: [''],
      totalAnticipatedWages: [''],
      isVisaRequired: [''],
      isVaccinationsRequired: [''],
      gapYearOption: [''],
      title: [''],
      url: [''],
      types: [''],
      isAbroad: [''],
      showWages: ['']
    });

    this.updateUserGapYearPlanStepOneForm = this._formBuilder.group({
      id: [''],
      planId: [''],
      goals: [''],
      refGapYearId: [''],
      planName: ['', [Validators.required, Validators.maxLength(500)]],
      types: this._formBuilder.array([]),
    });

    this.types = this.updateUserGapYearPlanStepOneForm.get('types') as FormArray;

    this.isSaving = false;
    this.editStepOne = false;

  }

  ngOnInit() {
    this.updateView(this.userGapYearPlan);
  }

  removeUserGapYearPlan(planId, userGapYearPlanId) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Gap Year Plan, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          this._gapYearPlanService.delete(planId, userGapYearPlanId).then((rowsAffected: number) => {
            //remove record from ui
            this.onRemoval.emit(userGapYearPlanId);
          }).catch(error => {
            console.error("Error:", error);
          });
        }
      }
    });

  }

  getGapYearPlan(gapYearId: number): RefGapYearPlan {
    return this.gapYearPlans.find(x => x.id == gapYearId);
  }

  getGapYearPlanByName(planName: string): RefGapYearPlan {
    return this.gapYearPlans.find(x => x.title.toUpperCase() == planName.toUpperCase());
  }

  updateView(userGapYearPlan: UserGapYearPlan) {

    let refGapYearPlan: RefGapYearPlan = (userGapYearPlan.gapYearId) ? this.getGapYearPlan(userGapYearPlan.gapYearId) : null;

    this.userGapYearPlanForm.patchValue({
      id: userGapYearPlan.id,
      planId: userGapYearPlan.planId,
      gapYearId: userGapYearPlan.gapYearId,
      goals: userGapYearPlan.goals,
      planName: userGapYearPlan.planName,
      programCost: userGapYearPlan.programCost,
      costOfLiving: userGapYearPlan.costOfLiving,
      fundingSource: userGapYearPlan.fundingSource,
      totalAnticipatedWages: userGapYearPlan.totalAnticipatedWages,
      title: userGapYearPlan.planName,
      estimatedAmount: userGapYearPlan.estimatedAmount,
      url: (refGapYearPlan) ? refGapYearPlan.url : '',
      types: this.formatTypes(userGapYearPlan.types),
      isAbroad: this.checkTypesForAbroad(userGapYearPlan.types),
      showWages: this.checkTypesForWages(userGapYearPlan.types),
      isVisaRequired: (userGapYearPlan.isVisaRequired == null)
        ? "" : (String(userGapYearPlan.isVisaRequired) == "") ? "" : String(userGapYearPlan.isVisaRequired),
      isVaccinationsRequired: (userGapYearPlan.isVaccinationsRequired == null)
        ? "" : (String(userGapYearPlan.isVaccinationsRequired) == "") ? "" : String(userGapYearPlan.isVaccinationsRequired)
    });

  }

  formatTypes(types: UserGapYearPlanType[]): string {

    if (!types || types.length == 0) {
      return "";
    }

    let names: string[] = [];

    for (let x of types) {
      if (x.isSelected) {
        names.push(x.name);
      }
    }

    return names.join(", ");

  }

  checkTypesForAbroad(types: UserGapYearPlanType[]): boolean {
    if (!types || types.length == 0) {
      return false;
    }

    // 1	Study Abroad
    // 2	Work Abroad
    // 3	College Sponsored
    // 4	Intership/Work
    // 5	Volunteer
    // 6	Travel
    for (let x of types) {
      if (x.isSelected) {
        if (x.id == 1 || x.id == 2 || x.id == 5) {
          return true;
        }
      }
    }

    return false;
  }


  checkTypesForWages(types: UserGapYearPlanType[]): boolean {
    if (!types || types.length == 0) {
      return false;
    }

    // 1	Study Abroad
    // 2	Work Abroad
    // 3	College Sponsored
    // 4	Intership/Work
    // 5	Volunteer
    // 6	Travel
    for (let x of types) {
      if (x.isSelected) {
        if (x.id == 2 || x.id == 4) {
          return true;
        }
      }
    }

    return false;
  }

  saveUserGapYearPlanForm() {
    this.isSaving = true;

    let formData: UserGapYearPlan = {
      id: this.userGapYearPlanForm.value.id,
      planId: this.userOccupationPlan.id,
      gapYearId: this.userGapYearPlanForm.value.gapYearId,
      goals: this.userGapYearPlan.goals, //no changes on this page
      planName: this.userGapYearPlanForm.value.planName,
      fundingSource: this.userGapYearPlanForm.value.fundingSource,
      totalAnticipatedWages: this.userGapYearPlanForm.value.totalAnticipatedWages,
      programCost: this.userGapYearPlanForm.value.programCost,
      costOfLiving: this.userGapYearPlanForm.value.costOfLiving,
      estimatedAmount: this.userGapYearPlanForm.value.estimatedAmount,
      isVisaRequired: this.userGapYearPlanForm.value.isVisaRequired,
      isVaccinationsRequired: this.userGapYearPlanForm.value.isVaccinationsRequired,
      types: this.userGapYearPlan.types  //no changes on this page
    }

    this._gapYearPlanService.update(this.userOccupationPlan.id, this.userGapYearPlanForm.value.id, formData).then((rowsAffected: number) => {
      if (rowsAffected > 0) {

        this.userGapYearPlanForm.markAsPristine();
        this.isSaving = false;

        this.startTimer();

      } else {
        window.alert("ERROR saving.  Please try again");
      }
    }).catch(error => {
      console.error("ERROR:", error);
    })
  }

  startTimer() {
    this.wasSaved = true;
    let self = this;
    setTimeout(function () {
      self.wasSaved = false;
    }, 2000);
  }

  editGoalsTypesStepOne() {

    //load data into the form
    this.gapYearTypes.forEach(t => {
      let isSelected: boolean = false;

      if (this.userGapYearPlan.types.find(x => (x.id == t.id) && x.isSelected)) {
        isSelected = true;
      }
      var gapYearType = this._formBuilder.group({
        id: [t.id],
        name: [t.name],
        isSelected: [isSelected]
      })
      this.types.push(gapYearType);
    });

    this.updateUserGapYearPlanStepOneForm.patchValue({
      goals: this.userGapYearPlan.goals,
      refGapYearId: this.userGapYearPlan.gapYearId,
      planName: this.userGapYearPlan.planName,
      id: this.userGapYearPlan.id
    });

    this.editStepOne = true;
  }

  showProgramList() {
    this._dialog.open(SampleGapProgramsDialogComponent, {
      maxWidth: '600px'
    }).afterClosed().subscribe(response => {
      if (response) {
        this.updateUserGapYearPlanStepOneForm.markAsDirty();
        this.updateUserGapYearPlanStepOneForm.patchValue({ planName: response.title, refGapYearId: response.id });
      }
    });
  }

  hasTypes(): boolean {

    const controlActivities = <FormArray>this.updateUserGapYearPlanStepOneForm.controls['types'];
    for (let i = controlActivities.length - 1; i >= 0; i--) {
      if (controlActivities.value[i].isSelected) {
        return true;
      }
    }

    return false;
  }

  getSelectedTypes(): UserGapYearPlanType[] {

    let returnValue: UserGapYearPlanType[] = [];

    const controlActivities = <FormArray>this.updateUserGapYearPlanStepOneForm.controls['types'];
    for (let i = controlActivities.length - 1; i >= 0; i--) {
      let gapYearPlanType: UserGapYearPlanType = {
        id: controlActivities.value[i].id,
        name: controlActivities.value[i].name,
        isSelected: controlActivities.value[i].isSelected
      };
      returnValue.push(gapYearPlanType);

    }

    return returnValue;
  }

  updateUserGapYearPlanStepOne() {

    this.isSaving = true;

    let refGapYearPlan: RefGapYearPlan = this.getGapYearPlanByName(this.updateUserGapYearPlanStepOneForm.value.planName);

    let formData: UserGapYearPlan = {
      id: this.userGapYearPlan.id,
      planId: this.userGapYearPlan.planId,
      gapYearId: (refGapYearPlan) ? refGapYearPlan.id : null,
      goals: this.updateUserGapYearPlanStepOneForm.value.goals,
      planName: this.updateUserGapYearPlanStepOneForm.value.planName,
      fundingSource: this.userGapYearPlan.fundingSource,
      programCost: this.userGapYearPlan.programCost,
      costOfLiving: this.userGapYearPlan.costOfLiving,
      estimatedAmount: this.userGapYearPlan.estimatedAmount,
      totalAnticipatedWages: this.userGapYearPlan.totalAnticipatedWages,
      isVisaRequired: this.userGapYearPlan.isVisaRequired,
      isVaccinationsRequired: this.userGapYearPlan.isVaccinationsRequired,
      types: this.getSelectedTypes()
    }

    this._gapYearPlanService.update(this.userGapYearPlan.planId, this.userGapYearPlan.id, formData).then((rowsAffected: number) => {
      if (rowsAffected > 0) {
        this.isSaving = false;
        this.userGapYearPlan = formData;
        this.editStepOne = false;
        this.updateUserGapYearPlanStepOneForm.reset();
        while (this.types.length !== 0) {
          this.types.removeAt(0)
        }
        this.onUpdate.emit(formData);
        this.updateView(this.userGapYearPlan);

      } else {
        this.isSaving = false;
        console.error("ERROR updating gap year plan");
      }

    }).catch(error => {
      this.isSaving = false;
      console.error("ERROR", error);
    })

  }

  addDate() {
    let data = {
      planId: this.userOccupationPlan.id,
      canEditPlan: false
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        let userOccupationPlanCalendar: any = result;

        this.onDateAdd.emit(userOccupationPlanCalendar);

      }

    });

  }

  addTask() {

    let data = {
      planId: this.userOccupationPlan.id,
      canEditPlan: false
    }

    const dialogRef = this._dialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        this.onTaskAdd.emit(userOccupationPlanCalendarTask);

      }
    });
  }


  delay = t => new Promise(resolve => setTimeout(resolve, t));

}
