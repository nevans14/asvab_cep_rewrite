import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GapYearPlanComponent } from './gap-year-plan.component';

describe('GapYearPlanComponent', () => {
  let component: GapYearPlanComponent;
  let fixture: ComponentFixture<GapYearPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GapYearPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GapYearPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
