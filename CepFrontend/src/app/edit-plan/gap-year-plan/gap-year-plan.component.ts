import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { SampleGapProgramsDialogComponent } from 'app/core/dialogs/sample-gap-programs-dialog/sample-gap-programs-dialog.component';
import { RefGapYearPlan } from 'app/core/models/refGapYearPlan.model';
import { RefGapYearType } from 'app/core/models/refGapYearType.model';
import { UserGapYearPlan } from 'app/core/models/userGapYearPlan.model';
import { UserGapYearPlanType } from 'app/core/models/userGapYearPlanType.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { GapYearPlanService } from 'app/services/gap-year-plan.service';

@Component({
  selector: 'app-gap-year-plan',
  templateUrl: './gap-year-plan.component.html',
  styleUrls: ['./gap-year-plan.component.scss']
})
export class GapYearPlanComponent implements OnInit {
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() occupationDescription: string;

  @Output() onDateAdd: EventEmitter<UserOccupationPlanCalendar>;
  @Output() onTaskAdd: EventEmitter<UserOccupationPlanCalendarTask>;

  public userGapYearPlans: any[];
  public isSaving: boolean;
  public addAnotherUserGapYearPlan: boolean;
  public gapYearOptionFilters: string[];
  public gapYearTypes: RefGapYearType[];
  public gapYearPlans: RefGapYearPlan[];

  public userGapYearPlanForm: FormGroup;
  public types: FormArray;
  public isSavingStepOne: boolean;

  constructor(private _gapYearPlanService: GapYearPlanService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _dialog: MatDialog) {

    this.userGapYearPlans = [];
    this.gapYearTypes = [];
    this.gapYearPlans = [];

    this.onDateAdd = new EventEmitter();
    this.onTaskAdd = new EventEmitter();

    this.isSaving = false;
    this.isSavingStepOne = false;
    this.addAnotherUserGapYearPlan = true;
    this.gapYearOptionFilters = [];
    this.userGapYearPlanForm = this._formBuilder.group({
      id: [''],
      planId: [''],
      goals: [''],
      refGapYearId: [''],
      planName: ['', [Validators.required, Validators.maxLength(500)]],
      types: this._formBuilder.array([]),
    });

    this.types = this.userGapYearPlanForm.get('types') as FormArray;

  }

  ngOnInit() {

    //set's up the form
    this.setupView();

  }

  async getAllGapYearTypes() {

    try {
      const gapYearTypes = await this._gapYearPlanService.getAllGapYearTypes();

      if (gapYearTypes) {
        for (let gapYearType of gapYearTypes) {
          this.gapYearTypes.push(gapYearType);
        };
      }

    } catch (error) {
      console.error("ERROR:", error);
    }

  }

  async getAllGapYearPlans() {

    try {
      const gapYearOptions: any = await this._gapYearPlanService.getAllGapYearOptions();

      if (gapYearOptions) {
        for (let gapYearOption of gapYearOptions) {
          this.gapYearPlans.push(gapYearOption);
        };

      }

    } catch (error) {
      console.error("ERROR:", error);
    }

  }

  async getAllUserGapYearPlans() {

    try {
      const gapYearPlans: any = await this._gapYearPlanService.getAll(this.userOccupationPlan.id);

      if (gapYearPlans) {
        for (let gapYearPlan of gapYearPlans) {
          this.userGapYearPlans.push(gapYearPlan);
        };

        if (gapYearPlans.length > 0) {
          this.addAnotherUserGapYearPlan = false;
        }

      }

    } catch (error) {
      console.error("ERROR:", error);
    }

  }

  async setupView() {

    //get all gap year types
    let x = await this.getAllGapYearTypes();
    let y = await this.getAllGapYearPlans();
    let z = await this.getAllUserGapYearPlans();

    this.gapYearTypes.forEach(t => {

      var gapYearType = this._formBuilder.group({
        id: [t.id],
        name: [t.name],
        isSelected: ['']
      })

      this.types.push(gapYearType);

    });

  }

  saveUserGapYearPlan() {

    this.isSavingStepOne = true;

    let formData: UserGapYearPlan = {
      id: null,
      planId: this.userOccupationPlan.id,
      gapYearId: (this.userGapYearPlanForm.value.refGapYearId) ? this.userGapYearPlanForm.value.refGapYearId : null,   //if program was selected
      goals: this.userGapYearPlanForm.value.goals,
      planName: this.userGapYearPlanForm.value.planName,
      fundingSource: null,
      programCost: null,
      costOfLiving: null,
      estimatedAmount: null,
      totalAnticipatedWages: null,
      isVisaRequired: null,
      isVaccinationsRequired: null,
      types: this.getSelectedTypes()
    }

    this._gapYearPlanService.save(this.userOccupationPlan.id, formData).then((response: UserGapYearPlan) => {
      this.userGapYearPlans.push(response);
      this.addAnotherUserGapYearPlan = false;
      this.isSavingStepOne = false;
    }).catch(error => {
      this.isSavingStepOne = false;
      console.error("ERROR", error);
    })

  }

  removeUserGapYearPlan(userGapYearPlanId) {

    this.userGapYearPlans.forEach((value: UserGapYearPlan, index) => {
      if (value.id == userGapYearPlanId) this.userGapYearPlans.splice(index, 1);

      if (this.userGapYearPlans.length == 0) {
        this.addAnotherUserGapYearPlan = true;
      }

    });

  }

  updateUserGapYearPlan(updatedUserGapYearPlan) {
    let index = this.userGapYearPlans.findIndex(x => x.id == updatedUserGapYearPlan.id);
    if (index > -1) this.userGapYearPlans[index] = updatedUserGapYearPlan;
  }

  showProgramList() {
    this._dialog.open(SampleGapProgramsDialogComponent, {
      maxWidth: '600px'
    }).afterClosed().subscribe(response => {
      if (response) {
        this.userGapYearPlanForm.patchValue({ planName: response.title, refGapYearId: response.id });
      }
    });
  }

  /**
   * 
   * @returns true if at least one item was checked in the check box list
   */
  hasTypes(): boolean {

    const controlActivities = <FormArray>this.userGapYearPlanForm.controls['types'];
    for (let i = controlActivities.length - 1; i >= 0; i--) {
      if (controlActivities.value[i].isSelected) {
        return true;
      }
    }

    return false;
  }

  getSelectedTypes(): UserGapYearPlanType[] {

    let returnValue: UserGapYearPlanType[] = [];

    const controlActivities = <FormArray>this.userGapYearPlanForm.controls['types'];
    for (let i = controlActivities.length - 1; i >= 0; i--) {
      let gapYearPlanType: UserGapYearPlanType = {
        id: controlActivities.value[i].id,
        name: controlActivities.value[i].name,
        isSelected: controlActivities.value[i].isSelected

      };
      returnValue.push(gapYearPlanType);

    }

    return returnValue;
  }

  updateOnTaskAdd(event){
    this.onTaskAdd.emit(event);
  }

  updateOnDateAdd(event){
    this.onDateAdd.emit(event);
  }

}