import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilitaryPlanComponent } from './military-plan.component';

describe('MilitaryPlanComponent', () => {
  let component: MilitaryPlanComponent;
  let fixture: ComponentFixture<MilitaryPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilitaryPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilitaryPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
