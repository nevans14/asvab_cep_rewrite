import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMilitaryPlanComponent } from './user-military-plan.component';

describe('UserMilitaryPlanComponent', () => {
  let component: UserMilitaryPlanComponent;
  let fixture: ComponentFixture<UserMilitaryPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMilitaryPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMilitaryPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
