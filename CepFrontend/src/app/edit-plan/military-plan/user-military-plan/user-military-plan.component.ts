import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { MilitaryAcademyAddDialogComponent } from 'app/core/dialogs/military-academy-add-dialog/military-academy-add-dialog.component';
import { SearchSchoolsDialogComponent } from 'app/core/dialogs/search-schools-dialog/search-schools-dialog.component';
import { FavoriteSchool } from 'app/core/models/favoriteSchool.model';
import { SchoolProfile } from 'app/core/models/schoolProfile.model';
import { ServiceCollege } from 'app/core/models/serviceCollege.model';
import { UserCollegePlan } from 'app/core/models/userCollegePlan.model';
import { UserMilitaryPlan } from 'app/core/models/userMilitaryPlan.model';
import { UserMilitaryPlanRotc } from 'app/core/models/userMilitaryPlanRotc.model';
import { UserMilitaryPlanServiceCollege } from 'app/core/models/userMilitaryPlanServiceCollege.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { UserSchoolPlan } from 'app/core/models/userSchoolPlan.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { CollegePlanObservableService } from 'app/services/college-plan-observable.service';
import { CollegePlanService } from 'app/services/college-plan.service';
import { ConfigService } from 'app/services/config.service';
import { MilitaryPlanService } from 'app/services/military-plan.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { SchoolPlanService } from 'app/services/school-plan.service';
import { UtilityService } from 'app/services/utility.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-military-plan',
  templateUrl: './user-military-plan.component.html',
  styleUrls: ['./user-military-plan.component.scss']
})
export class UserMilitaryPlanComponent implements OnInit, OnDestroy {

  @Input() userMilitaryPlan: UserMilitaryPlan;
  @Input() occuMilitaryCareerTitles: any[];
  @Input() availableJobTitles: any[];
  @Input() favoriteCollegesForROTC: SchoolProfile[];
  @Input() serviceColleges: ServiceCollege[];
  @Input() occupationDescription: string;
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() selectedPathways: any;
  @Input() favoriteCollegesForOccu: FavoriteSchool[];

  @Output() onUserMilitaryPlanRemove: EventEmitter<{ id: number, autoTasksToRemove: UserOccupationPlanCalendarTask[] }>;
  @Output() onAutoTasksCreated: EventEmitter<UserOccupationPlanCalendarTask[]>;
  @Output() onAutoCalendarsCreated: EventEmitter<UserOccupationPlanCalendar[]>;

  private selectedServices: string[];
  public userMilitaryPlanForm: FormGroup;
  public documentLocation: string;
  public selectedCareers: FormArray;
  public selectedRotcAcademies: FormArray;
  public selectedRotcColleges: FormArray;
  public selectAllFilters: string[];
  public isSaving: boolean;
  public userCollegePlans: UserCollegePlan[];
  public filteredOccuMilitaryCareerTitles: any[];
  public favoriteCollegesToSelect: any[];
  public favoriteSchoolsSelected: any[];
  public wasSaved: boolean;

  public filteredServiceColleges: ServiceCollege[];
  private readonly onDestroy = new Subject<void>();

  constructor(private _matDialog: MatDialog,
    private _militaryPlanService: MilitaryPlanService,
    private _formBuilder: FormBuilder,
    private _configService: ConfigService,
    private _collegePlanService: CollegePlanService,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _collegePlanObservableService: CollegePlanObservableService,
    private _planCalendarService: PlanCalendarService,
    private _schoolPlanService: SchoolPlanService,
    private _utilityService: UtilityService) {

    this.onUserMilitaryPlanRemove = new EventEmitter();
    this.onAutoTasksCreated = new EventEmitter();
    this.onAutoCalendarsCreated = new EventEmitter();
    this.wasSaved = false;

    this.selectedServices = [];
    this.userMilitaryPlanForm = this._formBuilder.group({
      applyMilitaryAcademy: [''],
      isEnlisted: [''],
      isInterestedInActiveDuty: [''],
      isInterestedInReserves: [''],
      isInterestedInNationalGuard: [''],
      isNotSure: [''],
      selectedCareers: this._formBuilder.array([]),
      selectedRotcAcademies: this._formBuilder.array([]),
      selectedRotcColleges: this._formBuilder.array([])
    });

    this.documentLocation = this._configService.getImageUrl() + this._configService.getDocumentFolder();
    this.selectedCareers = this.userMilitaryPlanForm.get('selectedCareers') as FormArray;
    this.selectedRotcAcademies = this.userMilitaryPlanForm.get('selectedRotcAcademies') as FormArray;
    this.selectedRotcColleges = this.userMilitaryPlanForm.get('selectedRotcColleges') as FormArray;

    this.selectAllFilters = [];
    this.isSaving = false;
    this.filteredServiceColleges = [];
    this.favoriteCollegesForOccu = [];
    this.favoriteCollegesToSelect = [];
    this.favoriteSchoolsSelected = [];

    //subscribe to user college plan observable
    this._collegePlanObservableService.collegePlanChanged$.pipe(takeUntil(this.onDestroy)).subscribe((userCollegePlans: UserCollegePlan[]) => {
      this.userCollegePlans = userCollegePlans;

      if (this.userMilitaryPlan) {
        this.updateUserCollegePlanView(this.userMilitaryPlan);
      }

    });

  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {

    let unitsForOccupation = this.favoriteCollegesForOccu.map(y => { return y.unitId });

    if (unitsForOccupation) {
      this.favoriteCollegesToSelect = this.favoriteCollegesForROTC.filter(x => unitsForOccupation.includes(x.unitId));
    }

    //filter available military titles based on enlisted vs officer selection
    if (this.userMilitaryPlan.didSelectOfficer) {
      this.filteredOccuMilitaryCareerTitles = this.occuMilitaryCareerTitles.filter(x => x.officer == 'T');
    } else {
      this.filteredOccuMilitaryCareerTitles = this.occuMilitaryCareerTitles.filter(x => x.enlisted == 'T');
    }

    //set available filters based off the services seletected in step 1 for the plan
    this.setSelectedServices(this.userMilitaryPlan);

    //do not show occu military career title if no job titles exist
    for (var i = this.filteredOccuMilitaryCareerTitles.length - 1; i >= 0; i--) {
      let jobTitles = this.availableJobTitles
        .filter(x => x.mcId == this.filteredOccuMilitaryCareerTitles[i].mcId && this.selectAllFilters.find(y => y == x.service));

      if (jobTitles.length <= 0) {
        this.filteredOccuMilitaryCareerTitles.splice(i, 1); // Remove even numbers
      }
    }

    this.updateView(this.userMilitaryPlan);

  }

  removeUserMilitaryPlan(planId, userMilitaryPlanId) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Military Plan, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).afterClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          this._militaryPlanService.delete(planId, userMilitaryPlanId).then((rowsAffected: number) => {

            //remove associated tasks if any
            this._planCalendarService.deleteAutoTasks('UserMilitaryPlan', planId, userMilitaryPlanId).then((response: UserOccupationPlanCalendarTask[]) => {

              //update ui
              this.onUserMilitaryPlanRemove.emit({ id: userMilitaryPlanId, autoTasksToRemove: response });

            });

          }).catch(error => {
            console.error("Error:", error);
          });
        }
      }
    });

  }

  removeSelectedRotcAcademy(index, id, unitId) {
    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Military Academy, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '400px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {
          if (id == "") {
            //only remove from gui
            this.selectedRotcAcademies.removeAt(index);
          } else {
            //remove from server and then refresh gui
            this._militaryPlanService.deleteMilitaryPlanServiceCollege(this.userMilitaryPlan.id, id).then((rowsAffected: number) => {
              //remove record from ui
              this.selectedRotcAcademies.removeAt(index);

              //remove associated auto tasks if any
              if (unitId == 197036) {
                this._planCalendarService.getAutoTasksForPlan('UserMilitaryPlan', this.userOccupationPlan.id, this.userMilitaryPlan.id).then(
                  (autoTasks: UserOccupationPlanCalendarTask[]) => {
                    if (autoTasks) {

                      //some records exist;
                      autoTasks.forEach((task: UserOccupationPlanCalendarTask) => {
                        let parseTaskName = JSON.parse(task.taskName);

                        if (parseTaskName.title == "West Point Check List") {
                          this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                        }

                        if (this.selectedRotcAcademies.length == 0) {
                          //remove Service Academy Application Timeline task if no academies are selected
                          if (parseTaskName.title == "Service Academy Application Timeline") {
                            this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                          }
                        }

                      });

                    }
                  });
              }

              if (unitId == 164155) {
                this._planCalendarService.getAutoTasksForPlan('UserMilitaryPlan', this.userOccupationPlan.id, this.userMilitaryPlan.id).then(
                  (autoTasks: UserOccupationPlanCalendarTask[]) => {
                    if (autoTasks) {

                      //some records exist;
                      autoTasks.forEach((task: UserOccupationPlanCalendarTask) => {
                        let parseTaskName = JSON.parse(task.taskName);

                        if (parseTaskName.title == "The United States Naval Academy Check List") {
                          this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                        }

                        if (this.selectedRotcAcademies.length == 0) {
                          //remove Service Academy Application Timeline task if no academies are selected
                          if (parseTaskName.title == "Service Academy Application Timeline") {
                            this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                          }
                        }

                      });

                    }
                  });
              }

              if (unitId == 128328) {
                this._planCalendarService.getAutoTasksForPlan('UserMilitaryPlan', this.userOccupationPlan.id, this.userMilitaryPlan.id).then(
                  (autoTasks: UserOccupationPlanCalendarTask[]) => {
                    if (autoTasks) {

                      //some records exist;
                      autoTasks.forEach((task: UserOccupationPlanCalendarTask) => {
                        let parseTaskName = JSON.parse(task.taskName);

                        if (parseTaskName.title == "The United States Air Force Academy Check List") {
                          this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                        }

                        if (this.selectedRotcAcademies.length == 0) {
                          //remove Service Academy Application Timeline task if no academies are selected
                          if (parseTaskName.title == "Service Academy Application Timeline") {
                            this._planCalendarService.deleteTask(task.id).then(x => this.onUserMilitaryPlanRemove.emit({ id: null, autoTasksToRemove: response }));
                          }
                        }

                      });

                    }
                  });
              }



            }).catch(error => {
              console.error("Error:", error);
            });
          }
        }
      }
    });
  }

  /**
   * Loops through the boolean fields of which service they plan on selecting and adds it to the array
   * Will also add available service colleges to the filtered service college array
   * @param userMilitaryPlan 
   */
  setSelectedServices(userMilitaryPlan: UserMilitaryPlan) {

    if (userMilitaryPlan.didSelectAirForce) {
      this.selectedServices.push("Air Force");
      this.selectAllFilters.push("F");
    }

    if (userMilitaryPlan.didSelectArmy) {
      this.selectedServices.push("Army");
      this.selectAllFilters.push("A");
    }

    if (userMilitaryPlan.didSelectCoastGuard) {
      this.selectedServices.push("Coast Guard");
      this.selectAllFilters.push("C");
    }

    if (userMilitaryPlan.didSelectMarineCorps) {
      this.selectedServices.push("Marine Corps");
      this.selectAllFilters.push("M");
    }

    if (userMilitaryPlan.didSelectNavy) {
      this.selectedServices.push("Navy");
      this.selectAllFilters.push("N");
    }

    if (userMilitaryPlan.didSelectSpaceForce) {
      this.selectedServices.push("Space Force");
      this.selectAllFilters.push("S");
    }

    for (var serviceCollege of this.serviceColleges) {
      for (var service of serviceCollege.services) {
        if (this.selectAllFilters.indexOf(service) !== -1) {
          this.filteredServiceColleges.push(serviceCollege);
          break;
        }
      }
    };

  }

  onSelectAllChange(value: any, index: number) {
    const selectedCareer = (<FormArray>this.selectedCareers.at(index));

    if (value.target.checked) {
      //select all filters if select all was checked
      selectedCareer.get('filterAirForce').setValue(true);
      selectedCareer.get('filterArmy').setValue(true);
      selectedCareer.get('filterNavy').setValue(true);
      selectedCareer.get('filterCoastGuard').setValue(true);
      selectedCareer.get('filterMarineCorps').setValue(true);
      selectedCareer.get('filterSpaceForce').setValue(true);
    } else {
      //unselect all filters if select was unchecked
      selectedCareer.get('filterAirForce').setValue(false);
      selectedCareer.get('filterArmy').setValue(false);
      selectedCareer.get('filterNavy').setValue(false);
      selectedCareer.get('filterCoastGuard').setValue(false);
      selectedCareer.get('filterMarineCorps').setValue(false);
      selectedCareer.get('filterSpaceForce').setValue(false);
    }
  }

  onFilterChange(value: any, index: number) {
    const selectedCareer = (<FormArray>this.selectedCareers.at(index));

    if (value.target.checked) {

      if (selectedCareer.get('filterAirForce').value &&
        selectedCareer.get('filterArmy').value &&
        selectedCareer.get('filterNavy').value &&
        selectedCareer.get('filterCoastGuard').value &&
        selectedCareer.get('filterMarineCorps').value &&
        selectedCareer.get('filterSpaceForce').value) {

        selectedCareer.get('filterSelectAll').setValue(true);
      }
    } else {
      //unselect filter all 
      selectedCareer.get('filterSelectAll').setValue(false);
    }
  }

  onCareerChange(value: any, index: number) {
    const selectedCareer = (<FormArray>this.selectedCareers.at(index));

    //reset filtering
    selectedCareer.get('filterSelectAll').setValue(true);
    selectedCareer.get('filterAirForce').setValue(true);
    selectedCareer.get('filterArmy').setValue(true);
    selectedCareer.get('filterNavy').setValue(true);
    selectedCareer.get('filterCoastGuard').setValue(true);
    selectedCareer.get('filterMarineCorps').setValue(true);
    selectedCareer.get('filterSpaceForce').setValue(true);


    //initially select 'select all' filter if a new record 
    if (value == '') {
      //unselect all filters if no career selected
      let showFiltersForCareer: {} = {
        army: false,
        marines: false,
        navy: false,
        airForce: false,
        spaceForce: false,
        coastGuard: false,
        show: false,
        numFilters: 0
      }
      selectedCareer.get('showFilters').setValue(showFiltersForCareer);

    } else {
      //only show filters based on the offerings of the selected career
      let career = this.filteredOccuMilitaryCareerTitles.find(y => y.mcId == value);
      let numFilters = 0;
      if (career.isArmy == "T" && this.userMilitaryPlan.didSelectArmy) numFilters++;
      if (career.isMarines == "T" && this.userMilitaryPlan.didSelectMarineCorps) numFilters++;
      if (career.isNavy == "T" && this.userMilitaryPlan.didSelectNavy) numFilters++;
      if (career.isAirForce == "T" && this.userMilitaryPlan.didSelectAirForce) numFilters++;
      if (career.isSpaceForce == "T" && this.userMilitaryPlan.didSelectSpaceForce) numFilters++;
      if (career.isCoastGuard == "T" && this.userMilitaryPlan.didSelectCoastGuard) numFilters++;

      let showFiltersForCareer: {} = {
        army: (career.isArmy == "T" && this.userMilitaryPlan.didSelectArmy),
        marines: (career.isMarines == "T" && this.userMilitaryPlan.didSelectMarineCorps),
        navy: (career.isNavy == "T" && this.userMilitaryPlan.didSelectNavy),
        airForce: (career.isAirForce == "T" && this.userMilitaryPlan.didSelectAirForce),
        spaceForce: (career.isSpaceForce == "T" && this.userMilitaryPlan.didSelectSpaceForce),
        coastGuard: (career.isCoastGuard == "T" && this.userMilitaryPlan.didSelectCoastGuard),
        show: (numFilters > 1),
        numFilters: numFilters
      }
      selectedCareer.get('showFilters').setValue(showFiltersForCareer);
    }
    selectedCareer.get('mcId').setValue(value);
  }

  updateView(userMilitaryPlan: UserMilitaryPlan) {

    let formattedSelectedCareers = [];  //used for patch value
    let formattedSelectedRotcAcademies = [];  //used for patch value
    let formattedSelectedRotcColleges = [];  //used for patch value
    let applyMilitaryAcademy: string = '';

    if (!userMilitaryPlan.selectedCareers) {
      userMilitaryPlan.selectedCareers = [];
    }

    //setup empty array records for each selected career (if any) for form patch value
    userMilitaryPlan.selectedCareers.forEach(t => {

      this.addCareer();

      //only show filters based on the offerings of the selected career
      let career = this.filteredOccuMilitaryCareerTitles.find(y => y.mcId == t.mcId);
      let numFilters = 0;
      if (career.isArmy == "T" && this.userMilitaryPlan.didSelectArmy) numFilters++;
      if (career.isMarines == "T" && this.userMilitaryPlan.didSelectMarineCorps) numFilters++;
      if (career.isNavy == "T" && this.userMilitaryPlan.didSelectNavy) numFilters++;
      if (career.isAirForce == "T" && this.userMilitaryPlan.didSelectAirForce) numFilters++;
      if (career.isSpaceForce == "T" && this.userMilitaryPlan.didSelectSpaceForce) numFilters++;
      if (career.isCoastGuard == "T" && this.userMilitaryPlan.didSelectCoastGuard) numFilters++;

      let showFiltersForCareer: {} = {
        army: (career.isArmy == "T" && this.userMilitaryPlan.didSelectArmy),
        marines: (career.isMarines == "T" && this.userMilitaryPlan.didSelectMarineCorps),
        navy: (career.isNavy == "T" && this.userMilitaryPlan.didSelectNavy),
        airForce: (career.isAirForce == "T" && this.userMilitaryPlan.didSelectAirForce),
        spaceForce: (career.isSpaceForce == "T" && this.userMilitaryPlan.didSelectSpaceForce),
        coastGuard: (career.isCoastGuard == "T" && this.userMilitaryPlan.didSelectCoastGuard),
        show: (numFilters > 1),
        numFilters: numFilters
      }

      formattedSelectedCareers.push({
        militaryPlanId: t.militaryPlanId,
        mcId: t.mcId,
        moc: {
          svc: t.svc,
          mpc: userMilitaryPlan.didSelectEnlisted ? 'E' : 'O',
          moc: t.moc
        },
        id: t.id,
        filterSelectAll: false,
        filterArmy: (t.svc == "A"),
        filterMarineCorps: (t.svc == "M"),
        filterNavy: (t.svc == "N"),
        filterAirForce: (t.svc == "F"),
        filterSpaceForce: (t.svc == "S"),
        filterCoastGuard: (t.svc == "C"),
        showFilters: showFiltersForCareer
      });
    });

    //if no careers selected show a default empty career
    if (this.selectedCareers.length <= 0) {
      this.addCareer();
      formattedSelectedCareers.push({
        militaryPlanId: 0,
        mcId: '',
        moc: {
          svc: '',
          mpc: '',
          moc: ''
        },
        id: 0
      });
    }

    //setup empty array records for each selected academy (if any) for form patch value
    if (!userMilitaryPlan.serviceColleges) {
      userMilitaryPlan.serviceColleges = [];
    }

    userMilitaryPlan.serviceColleges.forEach(p => {
      this.addRotcAcademy();
      formattedSelectedRotcAcademies.push({
        militaryPlanId: p.militaryPlanId,
        unitId: p.unitId,
        id: p.id,
        collegeTypeId: p.collegeTypeId
      });
    });

    //loop through each college plan (if any)
    //create a blank rotc record for each one
    //then match up with the rotcPrograms if applicable 
    if (!this.userCollegePlans) {
      this.userCollegePlans = [];
    }

    this.userCollegePlans.forEach(p => {

      //add record to array i.e. "space" to store during patch value
      this.addRotcCollege();

      let rotcProgram = (userMilitaryPlan.rotcPrograms) ? userMilitaryPlan.rotcPrograms.find(x => x.unitId == p.unitId) : null;

      //rotcProgram record exists for military plan for this college plan;
      if (rotcProgram) {
        formattedSelectedRotcColleges.push({
          militaryPlanId: rotcProgram.militaryPlanId,
          unitId: rotcProgram.unitId,
          id: rotcProgram.id,
          collegePlanId: rotcProgram.collegePlanId,
          wantsToApply: (rotcProgram.wantsToApply == 1) ? "yes" : (rotcProgram.wantsToApply == 0) ? "no" : "maybe",
          institutionName: p.schoolProfile.institutionName,
          rotcType: this.getCollegeROTCInfo(p.schoolProfile),
          url: p.schoolProfile.url
        });
      } else {
        //no linkage exists; create default record
        formattedSelectedRotcColleges.push({
          militaryPlanId: this.userMilitaryPlan.id,
          unitId: p.unitId,
          id: 0,
          collegePlanId: p.id,
          wantsToApply: '',
          institutionName: p.schoolProfile.institutionName,
          rotcType: this.getCollegeROTCInfo(p.schoolProfile),
          url: p.schoolProfile.url
        });
      }

    });

    //set the initial state of the radio button for whether they wish to apply 
    if ((userMilitaryPlan.rotcPrograms && userMilitaryPlan.rotcPrograms.length > 0)) {
      applyMilitaryAcademy = 'no';
    }

    if (userMilitaryPlan.serviceColleges && userMilitaryPlan.serviceColleges.length > 0) {
      applyMilitaryAcademy = 'yes';
    }

    this.userMilitaryPlanForm.patchValue({
      isEnlisted: !!userMilitaryPlan.didSelectEnlisted ? userMilitaryPlan.didSelectEnlisted : '',
      isInterestedInActiveDuty: !!userMilitaryPlan.isInterestedInActiveDuty ? userMilitaryPlan.isInterestedInActiveDuty : '',
      isInterestedInReserves: !!userMilitaryPlan.isInterestedInReserves ? userMilitaryPlan.isInterestedInReserves : '',
      isInterestedInNationalGuard: !!userMilitaryPlan.isInterestedInNationalGuard ? userMilitaryPlan.isInterestedInNationalGuard : '',
      isNotSure: !!userMilitaryPlan.isNotSure ? userMilitaryPlan.isNotSure : '',
      selectedCareers: !!userMilitaryPlan.selectedCareers ? formattedSelectedCareers : '',
      selectedRotcAcademies: formattedSelectedRotcAcademies.length > 0 ? formattedSelectedRotcAcademies : [],
      selectedRotcColleges: formattedSelectedRotcColleges.length > 0 ? formattedSelectedRotcColleges : [],
      applyMilitaryAcademy: applyMilitaryAcademy
    });

  }

  updateUserCollegePlanView(userMilitaryPlan: UserMilitaryPlan) {
    let formattedSelectedRotcColleges = [];  //used for patch value

    //remove previous values if any
    this.selectedRotcColleges = this.userMilitaryPlanForm.get('selectedRotcColleges') as FormArray;
    const controlColleges = <FormArray>this.userMilitaryPlanForm.controls['selectedRotcColleges'];
    for (let i = controlColleges.length - 1; i >= 0; i--) {
      controlColleges.removeAt(i)
    }

    this.userCollegePlans.forEach(p => {

      //add record to array i.e. "space" to store during patch value
      this.addRotcCollege();

      let rotcProgram = (userMilitaryPlan.rotcPrograms) ? userMilitaryPlan.rotcPrograms.find(x => x.unitId == p.unitId) : null;

      //rotcProgram record exists for military plan for this college plan;
      if (rotcProgram) {
        formattedSelectedRotcColleges.push({
          militaryPlanId: rotcProgram.militaryPlanId,
          unitId: rotcProgram.unitId,
          id: rotcProgram.id,
          collegePlanId: rotcProgram.collegePlanId,
          wantsToApply: (rotcProgram.wantsToApply == 1) ? "yes" : (rotcProgram.wantsToApply == 0) ? "no" : "maybe",
          institutionName: p.schoolProfile.institutionName,
          rotcType: this.getCollegeROTCInfo(p.schoolProfile),
          url: p.schoolProfile.url
        });
      } else {
        //no linkage exists; create default record
        formattedSelectedRotcColleges.push({
          militaryPlanId: this.userMilitaryPlan.id,
          unitId: p.unitId,
          id: '',
          collegePlanId: p.id,
          wantsToApply: '',
          institutionName: p.schoolProfile.institutionName,
          rotcType: this.getCollegeROTCInfo(p.schoolProfile),
          url: p.schoolProfile.url
        });
      }

    });

    this.selectedRotcColleges.patchValue(formattedSelectedRotcColleges);
  }

  addCareer() {
    this.selectedCareers.push(
      this._formBuilder.group({
        militaryPlanId: ['', Validators.maxLength(500)],
        mcId: ['', Validators.maxLength(7)],
        moc: [{
          svc: '',
          mpc: '',
          moc: ''
        }],
        id: [''],
        filterSelectAll: [''],
        filterAirForce: [''],
        filterArmy: [''],
        filterCoastGuard: [''],
        filterMarineCorps: [''],
        filterNavy: [''],
        filterSpaceForce: [''],
        showFilters: ['']
      })
    );
  }

  addRotcAcademy() {
    this.selectedRotcAcademies.push(
      this._formBuilder.group({
        militaryPlanId: [''],
        unitId: [''],
        id: [''],
        collegeTypeId: ['']
      })
    );
  }

  addRotcCollege() {
    this.selectedRotcColleges.push(
      this._formBuilder.group({
        militaryPlanId: [''],
        collegePlanId: [''],
        unitId: [''],
        id: [''],
        wantsToApply: [''],
        institutionName: [''],
        rotcType: [''],
        url: ['']
      })
    );
  }

  removeUserMilitaryCareer(index: number, id: any) {
    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Military Career, are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          if (id) {
            this._militaryPlanService.deleteUserMilitaryPlanCareer(this.userMilitaryPlan.id, id).then((rowsAffected: number) => {
              if (rowsAffected > 0) {
                this.selectedCareers.removeAt(index);
                // add empty career if there are no careers.
                if (this.selectedCareers.length <= 0) {
                  this.addCareer();
                }
              } else {
                console.error("No rows removed for: id", id);
              }
            }).catch(error => {
              console.error("Error:", error);
            })
          } else {
            this.selectedCareers.removeAt(index);
            // add empty career if there are no careers.
            if (this.selectedCareers.length <= 0) {
              this.addCareer();
            }
          }
        }
      }
    });
  }

  getCollegePlanByUnitId(unitId: number) {

    //find college plan based on unit id
    return this.userCollegePlans.find(x => x.unitId == unitId);

  }

  saveUserMilitaryPlan() {
    this.isSaving = true;
    let formattedSelectedCareers: any[] = [];
    let userMilitaryPlanRotcs: UserMilitaryPlanRotc[] = [];
    let userMilitaryPlanServiceColleges: UserMilitaryPlanServiceCollege[] = [];

    for (var i = 0; i < this.selectedCareers.length; i++) {

      if (this.selectedCareers.value[i].mcId != "") {
        formattedSelectedCareers.push({
          militaryPlanId: this.userMilitaryPlan.id,
          mcId: this.selectedCareers.value[i].mcId,
          moc: this.selectedCareers.value[i].moc.moc,
          svc: this.selectedCareers.value[i].moc.svc,
          mpc: this.userMilitaryPlan.didSelectEnlisted ? 'E' : 'O',
          id: this.selectedCareers.value[i].id,
        });
      }
    }

    for (var i = 0; i < this.selectedRotcAcademies.length; i++) {
      userMilitaryPlanServiceColleges.push({
        militaryPlanId: this.userMilitaryPlan.id,
        id: (this.selectedRotcAcademies.value[i].id) == "" ? 0 : this.selectedRotcAcademies.value[i].id,
        unitId: this.selectedRotcAcademies.value[i].unitId,
        collegeTypeId: this.selectedRotcAcademies.value[i].collegeTypeId,
      });

    }

    for (var i = 0; i < this.selectedRotcColleges.length; i++) {
      var wantsToApply = 2;
      if (this.selectedRotcColleges.value[i].wantsToApply == "yes") {
        wantsToApply = 1;
      }
      if (this.selectedRotcColleges.value[i].wantsToApply == "no") {
        wantsToApply = 0;
      }
      userMilitaryPlanRotcs.push({
        militaryPlanId: this.userMilitaryPlan.id,
        id: (this.selectedRotcColleges.value[i].id) == "" ? 0 : this.selectedRotcColleges.value[i].id,
        unitId: this.selectedRotcColleges.value[i].unitId,
        collegePlanId: this.selectedRotcColleges.value[i].collegePlanId,
        wantsToApply: wantsToApply,
      });
    }

    let formData: UserMilitaryPlan = {
      id: this.userMilitaryPlan.id,
      planId: this.userMilitaryPlan.planId,
      didSelectEnlisted: this.userMilitaryPlan.didSelectEnlisted,
      didSelectOfficer: this.userMilitaryPlan.didSelectOfficer,
      didSelectArmy: this.userMilitaryPlan.didSelectArmy,
      didSelectNavy: this.userMilitaryPlan.didSelectNavy,
      didSelectAirForce: this.userMilitaryPlan.didSelectAirForce,
      didSelectMarineCorps: this.userMilitaryPlan.didSelectMarineCorps,
      didSelectCoastGuard: this.userMilitaryPlan.didSelectCoastGuard,
      didSelectSpaceForce: this.userMilitaryPlan.didSelectSpaceForce,
      isInterestedInActiveDuty: this.userMilitaryPlanForm.value.isInterestedInActiveDuty,
      isInterestedInReserves: this.userMilitaryPlanForm.value.isInterestedInReserves,
      isInterestedInNationalGuard: this.userMilitaryPlanForm.value.isInterestedInNationalGuard,
      selectedCareers: formattedSelectedCareers,
      isNotSure: this.userMilitaryPlanForm.value.isNotSure,
      rotcPrograms: userMilitaryPlanRotcs,
      serviceColleges: userMilitaryPlanServiceColleges,
    }

    this._militaryPlanService.update(formData.planId, formData.id, formData).then((updatedUserMilitaryPlan: UserMilitaryPlan) => {
      if (updatedUserMilitaryPlan) {

        updatedUserMilitaryPlan.rotcPrograms = updatedUserMilitaryPlan.rotcPrograms;
        updatedUserMilitaryPlan.serviceColleges = updatedUserMilitaryPlan.serviceColleges;
        updatedUserMilitaryPlan.selectedCareers = updatedUserMilitaryPlan.selectedCareers;

        //create auto tasks?
        if (updatedUserMilitaryPlan.serviceColleges.length > 0) {

          let westPointSelected: boolean = false;
          let navySelected: boolean = false;
          let afSelected: boolean = false;
          let academySelected: boolean = false;

          updatedUserMilitaryPlan.serviceColleges.forEach((serviceCollege: UserMilitaryPlanServiceCollege) => {
            if (serviceCollege.unitId == 197036) {
              westPointSelected = true;
            }
            if (serviceCollege.unitId == 164155) {
              navySelected = true;
            }
            if (serviceCollege.unitId == 128328) {
              afSelected = true;
            }

            academySelected = true;
          })

          this._planCalendarService.getAutoTasksForPlan('UserMilitaryPlan', updatedUserMilitaryPlan.planId, updatedUserMilitaryPlan.id).then(
            (autoTasks: UserOccupationPlanCalendarTask[]) => {
              let typeOfTasksToCreate = [];

              if (autoTasks) {

                //some records exist;
                autoTasks.forEach((task: UserOccupationPlanCalendarTask) => {
                  let parseTaskName = JSON.parse(task.taskName);

                  if (parseTaskName.title == "West Point Check List") {
                    westPointSelected = false;
                  }
                  if (parseTaskName.title == "The United States Naval Academy Check List") {
                    navySelected = false;
                  }
                  if (parseTaskName.title == "The United States Air Force Academy Check List") {
                    afSelected = false;
                  }
                  if (parseTaskName.title == "Service Academy Application Timeline") {
                    academySelected = false;
                  }
                });

              }

              //at least one academy was selected
              if (academySelected) typeOfTasksToCreate.push({
                url: 'https://www.asvabprogram.com/pdf/Service_Academy_Application_Timeline.pdf',
                title: 'Service Academy Application Timeline'
              });

              if (westPointSelected) typeOfTasksToCreate.push({
                url: 'https://prod-media.asvabprogram.com/CEP_PDF_Contents/Military_academy-West_Point.pdf',
                title: 'West Point Check List'
              });

              if (navySelected) typeOfTasksToCreate.push({
                url: 'https://prod-media.asvabprogram.com/CEP_PDF_Contents/Military_academy-USNaval_Academy.pdf',
                title: 'The United States Naval Academy Check List'
              });

              if (afSelected) typeOfTasksToCreate.push({
                url: 'https://prod-media.asvabprogram.com/CEP_PDF_Contents/Military_academy-US_Air_Force_Academy.pdf',
                title: 'The United States Air Force Academy Check List'
              });

              Promise.all(typeOfTasksToCreate.map((x: any) => {
                return this._planCalendarService.saveAutoDateTask('UserMilitaryPlan', updatedUserMilitaryPlan.planId, updatedUserMilitaryPlan.id, x.url, x.title)
                  .then((success: any) => {
                    return success;
                  });
              })).then((done: any) => {

                this.onAutoTasksCreated.emit(done);

                this.userMilitaryPlanForm.markAsPristine();

                //refresh view with saved data;
                const controlCareers = <FormArray>this.userMilitaryPlanForm.controls['selectedCareers'];
                for (let i = controlCareers.length - 1; i >= 0; i--) {
                  controlCareers.removeAt(i)
                }

                const controlAcademies = <FormArray>this.userMilitaryPlanForm.controls['selectedRotcAcademies'];
                for (let i = controlAcademies.length - 1; i >= 0; i--) {
                  controlAcademies.removeAt(i)
                }

                const controlColleges = <FormArray>this.userMilitaryPlanForm.controls['selectedRotcColleges'];
                for (let i = controlColleges.length - 1; i >= 0; i--) {
                  controlColleges.removeAt(i)
                }

                this.isSaving = false;

                this.startTimer();

                this.updateView(updatedUserMilitaryPlan);

              });

            }
          );

        } else {

          this.userMilitaryPlanForm.markAsPristine();

          //refresh view with saved data;
          const controlCareers = <FormArray>this.userMilitaryPlanForm.controls['selectedCareers'];
          for (let i = controlCareers.length - 1; i >= 0; i--) {
            controlCareers.removeAt(i)
          }

          const controlAcademies = <FormArray>this.userMilitaryPlanForm.controls['selectedRotcAcademies'];
          for (let i = controlAcademies.length - 1; i >= 0; i--) {
            controlAcademies.removeAt(i)
          }

          const controlColleges = <FormArray>this.userMilitaryPlanForm.controls['selectedRotcColleges'];
          for (let i = controlColleges.length - 1; i >= 0; i--) {
            controlColleges.removeAt(i)
          }

          this.isSaving = false;

          this.startTimer();

          this.updateView(updatedUserMilitaryPlan);
        }

      }
    }).catch(error => {
      console.error("Error:", error);
      this.isSaving = false;
    })

  }

  showHelpMessageForROTC() {

    var data = {
      title: 'Reserve Officers Training Corps (ROTC)',
      message: "Reserve Officer Training Corps (ROTC) programs provide officer training for students during college in exchange for scholarship money. " +
        " Research the ROTC options and requirements at your preferred post-secondary institution. <br /> <a href='https://www.careersinthemilitary.com/options-becoming-an-officer/rotc' target='_blank'>Learn more about ROTC</a>. "
    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '300px'
    });

  }

  showHelpMessageForMilitaryCareer() {

    var data = {
      title: 'Military Career',
      message: "Your favorite occupation could have another name in the Military. Select a military career related to your favorite occupation in your desired Service. " +
        " You can learn more about each job at <a href='https://www.careersinthemilitary.com' target='_blank'>careersinthemilitary.com.</a>"
    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '300px'
    });

  }

  showHelpMessageForMilitaryInterest() {

    var data = {
      title: 'Military career interest',
      message: "There are a variety of options when you consider a Military Service commitment. You can join the " +
        " military as full-time (Active Duty) or part-time (Reserve, National Guard). Learn more about these options on " +
        " <a target='_blank' href='https://www.careersinthemilitary.com'>careersinthemilitary.com</a> "
    };

    const dialogRef1 = this._matDialog.open(MessageDialogComponent, {
      data: data,
      maxWidth: '300px'
    });

  }

  compareJobTitle(val1, val2) {
    return val1.moc == val2.moc;
  }

  // checkJobTitles() {

  //   for (var i = 0; i < this.selectedCareers.length; i++) {

  //     if (this.selectedCareers.value[i].moc) {
  //       if (this.selectedCareers.value[i].moc.moc) {
  //         return true;
  //       }
  //     }
  //   }

  //   return false;
  // }

  showMilitaryAcademyAddDialog() {

    let dialogData = {
      selectedSchools: this.selectedRotcAcademies.value,
      allMilitaryAcademies: this.filteredServiceColleges,
      occupationDescription: this.occupationDescription,
    };

    this._matDialog.open(MilitaryAcademyAddDialogComponent, {
      data: dialogData,
      maxWidth: '800px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response != 'cancel') {
          this.saveMilitaryAcademy(response);
        }
      }
    });
  }

  addSchoolSelected(school, event) {
    if (event.target.checked) {
      this.favoriteSchoolsSelected.push(school);
    } else {
      this.favoriteSchoolsSelected.splice(this.favoriteSchoolsSelected.findIndex(e => e.unitId === school.unitId), 1);
    }

  }

  showSchoolAddDialog() {
    let dialogData = {
      userOccupationPlan: this.userOccupationPlan,
      selectedSchools: [],
      occupationDescription: this.occupationDescription,
      selectedPathways: this.selectedPathways
    };

    this._matDialog.open(SearchSchoolsDialogComponent, {
      data: dialogData,
      maxWidth: '800px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response != 'cancel') {
          this.saveUserCollegePlan(response);
        }
      }
    });
  }

  saveFavoriteColleges() {

    for (let school of this.favoriteSchoolsSelected) {
      this.saveUserCollegePlan(school.unitId);
    }

    this.favoriteSchoolsSelected = [];  //reset list

  }

  /**
   * 
   * @param unitId 
   */
  saveUserCollegePlan(unitId) {

    let formData: UserCollegePlan = {
      id: null,
      planId: this.userOccupationPlan.id,
      unitId: unitId,
      isSeekingInStateTuition: null,
      degree: null,
      major: null,
      familyContribution: null,
      scholarshipContribution: null,
      savings: null,
      loan: null,
      other: null,
      schoolProfile: null
    };

    //save to database
    this._collegePlanService.save(this.userOccupationPlan.id, formData).then((newRecord: any) => {
      this._occufindRestFactoryService.getSchoolProfile(newRecord.unitId).toPromise()
        .then((schoolProfile: SchoolProfile) => {
          //combine objects to display to user and allow entry
          newRecord.schoolProfile = schoolProfile;
          // newRecord.
          this._collegePlanObservableService.add(newRecord);


          //do automatic tasks already exist?
          //assumption that both FAFSA dates are always created
          //therefore only need to check for one task title 
          this._planCalendarService.checkAutoTaskExistsForPlan('UserCollegePlan', this.userOccupationPlan.id, formData.id, schoolProfile.institutionName + ': FAFSA open date').then(
            (value: boolean) => {

              if (!value) {
                //auto tasks do not exist for the college plan;
                //create only if graduation date exists

                //get graduation date
                this._schoolPlanService.get(this.userOccupationPlan.id).then((userSchoolPlan: UserSchoolPlan) => {

                  //create tasks only if graduation date exists;
                  if (userSchoolPlan.graduationDate) {
                    //create a FAFSA tasks assigned to a calendar date
                    let graduationYear = new Date(this._utilityService.convertServerUTCToUniversalUTC(userSchoolPlan.graduationDate)).getFullYear();

                    // Oct 1 , same year as graduation
                    let x = this._planCalendarService.saveAutoDateForCollegePlan(formData.planId,
                      schoolProfile.institutionName + ': FAFSA open date', new Date(graduationYear, 9, 1));

                    // June 30, same year as graduation
                    let y = this._planCalendarService.saveAutoDateForCollegePlan(formData.planId,
                      schoolProfile.institutionName + ': FAFSA close date', new Date(graduationYear, 5, 30));

                    Promise.all([x, y]).then((done: any) => {

                      let emitTasks = [];
                      emitTasks.push(done[0]);
                      emitTasks.push(done[1]);
                      this.onAutoCalendarsCreated.emit(emitTasks);

                    });
                  }
                });
              }

            }
          )

        });
    }).catch(error => {
      console.error("Error: ", error);
    });

  }

  /**
   * Saves the selected military academy to the UI list
   * @param school 
   */
  saveMilitaryAcademy(school: any) {

    //update ui
    this.selectedRotcAcademies.push(
      this._formBuilder.group({
        militaryPlanId: this.userMilitaryPlan.id,
        unitId: school.unitId,
        id: [''],
        collegeTypeId: school.collegeTypeId
      })
    );

    this.userMilitaryPlanForm.markAsDirty();

  }

  getAcademyName(unitId: number, collegeTypeId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId && x.collegeTypeId == collegeTypeId);
    if (academy) {
      return academy.schoolProfile.institutionName;
    } else {
      return unitId;
    }
  }

  getAcademyApplicationTimelinePdfKey(unitId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId);
    if (academy) {
      return academy.applicationTimelinePdfKey;
    } else {
      return null;
    }
  }

  getAcademyChecklistPdfKey(unitId: number) {
    let academy = this.serviceColleges.find(x => x.unitId == unitId);
    if (academy) {
      return academy.checklistPdfKey;
    } else {
      return null;
    }
  }

  getCollegeROTCInfo(schoolProfile: SchoolProfile): any[] {

    let rotcList: any[] = [];

    if (schoolProfile.rotcAirForce) {
      rotcList.push({ image: 'assets/images/air-force.png', alt: 'Air Force logo', caption: "Air Force", href: "https://www.airforce.com/" });
    }

    if (schoolProfile.rotcNavy) {
      rotcList.push({ image: 'assets/images/Navy_new.svg', alt: 'Navy logo', caption: "Navy", href: "https://www.navy.com/" });
    }

    if (schoolProfile.rotcArmy) {
      rotcList.push({ image: 'assets/images/Army_new.svg', alt: 'Army logo', caption: "Army", href: "https://www.goarmy.com/" });
    }

    return rotcList;
  }

  getServiceTitle(svc: string) {

    if (svc == 'F') return "Air Force";

    if (svc == 'A') return "Army";

    if (svc == 'C') return "Coast Guard";

    if (svc == 'M') return "Marine Corps";

    if (svc == 'N') return "Navy";

    if (svc == 'S') return "Space Force";

  }

  setFilterColumnClass(userMilitaryPlan: UserMilitaryPlan, showFilters: any, service: string) {

    let totalServices = showFilters.numFilters;

    //army is always the first filter
    if (showFilters.army && service == 'army') {
      return "column medium-3 medium-offset-2 end";
    }

    //marine corps filter 
    if (showFilters.marines && service == 'marinecorps') {
      if (showFilters.army) {
        return "column medium-3 end";
      } else {
        return "column medium-3 medium-offset-2 end"
      }
    }

    //navy filter
    if (showFilters.navy && service == 'navy') {
      if ((showFilters.army || showFilters.marines) || (showFilters.army && showFilters.marines)) {
        return "column medium-3 end";
      } else {
        return "column medium-3 medium-offset-2 end";
      }
    }

    //air force filter
    if (showFilters.airForce && service == 'airforce') {

      //determine whether we need to indent
      if (totalServices > 3) {
        if (showFilters.army && showFilters.marines && showFilters.navy) {

          if (totalServices == 4) {
            return "column medium-3 medium-offset-2 end";  //on 2nd row but the last service
          } else {
            return "column medium-3 medium-offset-2 end";  //on 2nd row 
          }
        } else {
          return "column medium-3 end";  //on first row
        }
      }

      // 3 or less selected
      if (showFilters.army || showFilters.marines || showFilters.navy) {
        return "column medium-3 end";  //on first row and not the first record
      } else {
        return "column medium-3 medium-offset-2 end";  //first record on first row
      }
    }

    // space force filter
    if (showFilters.spaceForce && service == 'spaceforce') {

      //determine whether we need to indent
      // if(totalServices > 3){
      var totalBefore = 0;
      if (showFilters.army) totalBefore++;
      if (showFilters.marines) totalBefore++;
      if (showFilters.navy) totalBefore++;
      if (showFilters.airForce) totalBefore++;

      if (totalBefore > 2) {
        //2nd row
        if (totalBefore == 4) {
          //if coast guard was not selected then assume this is the last service filter
          //2nd record on the 2nd row
          if (showFilters.coastGuard) {
            return "column medium-4 end";
          } else {
            return "column medium-4 end";
          }

        } else {
          if (showFilters.coastGuard) {
            return "column medium-4 medium-offset-2 end";
          } else {
            return "column medium-4 medium-offset-2 end";
          }
        }

      } else {
        //first row 

        //last record in row
        if (totalBefore == 2) {
          return "column medium-3 end";
        }
        //middle record
        if (totalBefore == 1) {
          if (showFilters.coastGuard) {
            return "column medium-3 end";
          } else {
            return "column medium-3 end";
          }
        }

        //first record
        if (showFilters.coastGuard) {
          return "column medium-3 medium-offset-2 end";
        } else {
          return "column medium-3 medium-offset-2 end";
        }
      }

    }

    // coast guard filter
    if (showFilters.coastGuard && service == 'coastguard') {

      if (totalServices > 4) {
        return "column medium-3 end";
      }

      var totalBefore = 0;
      if (showFilters.army) totalBefore++;
      if (showFilters.marines) totalBefore++;
      if (showFilters.navy) totalBefore++;
      if (showFilters.airForce) totalBefore++;
      if (showFilters.spaceForce) totalBefore++;

      if (totalBefore > 2) {
        //on 2nd row

        if (totalBefore == 3) {
          return "column medium-4 medium-offset-2 end";
        } else {
          return "column medium-4 end";
        }

      } else {
        //on first row
        return "column medium-4 end"
      }

    }

    return "";
  }

  startTimer() {
    let self = this;
    self.wasSaved = true;
    setTimeout(function () {
      self.wasSaved = false;
    }, 2000);
  }
}
