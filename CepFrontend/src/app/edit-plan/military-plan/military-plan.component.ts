import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FavoriteSchool } from 'app/core/models/favoriteSchool.model';
import { SchoolProfile } from 'app/core/models/schoolProfile.model';
import { ServiceCollege } from 'app/core/models/serviceCollege.model';
import { UserMilitaryPlan } from 'app/core/models/userMilitaryPlan.model';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { OccufindRestFactoryService } from 'app/occufind/occupation-details/services/occufind-rest-factory.service';
import { PortfolioRestFactoryService } from 'app/portfolio/services/portfolio-rest-factory.service';
import { FavoritesRestFactoryService } from 'app/services/favorites-rest-factory.service';
import { IpedsService } from 'app/services/ipeds.service';
import { MilitaryPlanService } from 'app/services/military-plan.service';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-military-plan',
  templateUrl: './military-plan.component.html',
  styleUrls: ['./military-plan.component.scss']
})
export class MilitaryPlanComponent implements OnInit {
  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() occupationDescription: string;
  @Input() selectedPathways: any;

  @Output() onTaskAdd: EventEmitter<UserOccupationPlanCalendarTask>;
  @Output() onTasksAdd: EventEmitter<UserOccupationPlanCalendarTask[]>;
  @Output() onTasksRemoval: EventEmitter<UserOccupationPlanCalendarTask[]>;
  @Output() onCalendarsAdd: EventEmitter<UserOccupationPlanCalendar[]>;


  public userMilitaryPlans: UserMilitaryPlan[]
  public newUserMilitaryPlanForm: FormGroup;
  private serviceCounter: number;
  public addAnotherMilitaryPlan: boolean;
  public occuMilitaryCareerTitles: any[];
  public availableJobTitles: any[];
  public favoriteCollegesForROTC: SchoolProfile[];
  public favoriteColleges: FavoriteSchool[];
  public serviceColleges: ServiceCollege[];
  public serviceOptions: {
    isAirForce: boolean,
    isArmy: boolean,
    isCoastGuard: boolean,
    isMarines: boolean,
    isNavy: boolean,
    isSpaceForce: boolean
  };
  public enlistOptions: {
    enlisted: boolean,
    officer: boolean
  }
  public isLoading: boolean;
  public favorites: any;

  constructor(private _militaryPlanService: MilitaryPlanService,
    private _formBuilder: FormBuilder,
    private _occufindRestFactoryService: OccufindRestFactoryService,
    private _userService: UserService,
    private _ipedsService: IpedsService,
    private _planCalendarService: PlanCalendarService,
    private _portfolioRestFactory: PortfolioRestFactoryService,
    private _favoritesRestFactory: FavoritesRestFactoryService) {

    this.addAnotherMilitaryPlan = true;
    this.availableJobTitles = [];

    this.onTaskAdd = new EventEmitter();
    this.onTasksAdd = new EventEmitter();
    this.onTasksRemoval = new EventEmitter();
    this.onCalendarsAdd = new EventEmitter();

    this.serviceCounter = 0;
    this.newUserMilitaryPlanForm = this._formBuilder.group({
      isEnlisted: ['', Validators.required],
      selectAll: [''],
      army: [''],
      marinecorps: [''],
      navy: [''],
      airforce: [''],
      spaceforce: [''],
      coastguard: ['']
    });
    this.occuMilitaryCareerTitles = [];
    this.favoriteCollegesForROTC = [];
    this.serviceColleges = [];
    this.serviceOptions = {
      isAirForce: false,
      isArmy: false,
      isCoastGuard: false,
      isMarines: false,
      isNavy: false,
      isSpaceForce: false
    };
    this.enlistOptions = {
      enlisted: false,
      officer: false
    }
    this.isLoading = true;

  }

  ngOnInit() {

    //get existing military plans if any
    let militaryPlans = this.getMilitaryPlans(this.userOccupationPlan.id);

    //get available military job titles for the military careers for the occupation
    let availableMilitaryJobTitles = this.getAvailableMilitaryCareersForOccu(this.userOccupationPlan.socId);

    let favoriteColleges = this.getFavoriteColleges();

    let serviceColleges = this.getServiceColleges();

    let citmFavoriteOccupations = this.getCitmFavoriteOccupations();

    let favoriteCollegesForOccu = this.getFavoriteCollegesForSocID();

    Promise.all([militaryPlans, availableMilitaryJobTitles, favoriteColleges, serviceColleges, citmFavoriteOccupations, favoriteCollegesForOccu]).then((done: any) => {
      this.userMilitaryPlans = done[0];
      this.availableJobTitles = done[1];
      this.favoriteCollegesForROTC = done[2];
      let serviceColleges = done[3];
      this.favorites = done[4];
      this.favoriteColleges = done[5];

      //clean job titles array; some titles have empty spaces
      let cleanAvailableJobTitles = this.availableJobTitles.map((x) => {x.title = x.title.trim(); return x;});

      //remove duplicate job titles
      this.availableJobTitles = cleanAvailableJobTitles.filter((v,i,a)=>a.findIndex(t=>(t.mcId === v.mcId && t.title===v.title && t.service===v.service))===i);

      //set favorites
      this.occuMilitaryCareerTitles.map((careerTitle) => {

        if (this.favorites) {
          let favoriteExists = this.favorites.find(x => x.mcId == careerTitle.mcId);

          if (favoriteExists) {
            careerTitle.isFavorite = true;
          } else {
            careerTitle.isFavorite = false;
          }
        } else {
          careerTitle.isFavorite = false;
        }
        return careerTitle;
      });

      //if no military plans show step 1
      if (this.userMilitaryPlans.length > 0) {
        this.addAnotherMilitaryPlan = false;
      }

      //show alternative names if present for each service college
      for (let serviceCollege of serviceColleges) {
        if (serviceCollege.alternativeName) {
          serviceCollege.schoolProfile.institutionName = serviceCollege.alternativeName;
        }
        this.serviceColleges.push(serviceCollege);
      };

      this.isLoading = false;
    });

  }

  /**
   * Adjusts whether a user has the option to select enlist/officer or services based on the occupation jobs
   * @param occuMilitaryCareerTitle 
   */
  adjustServiceOptionsAndEnlistOptions(occuMilitaryCareerTitle: any) {

    if (occuMilitaryCareerTitle.isAirForce == 'T') {
      this.serviceOptions.isAirForce = true;
    }

    if (occuMilitaryCareerTitle.isArmy == 'T') {
      this.serviceOptions.isArmy = true;
    }

    if (occuMilitaryCareerTitle.isCoastGuard == 'T') {
      this.serviceOptions.isCoastGuard = true;
    }

    if (occuMilitaryCareerTitle.isMarines == 'T') {
      this.serviceOptions.isMarines = true;
    }

    if (occuMilitaryCareerTitle.isNavy == 'T') {
      this.serviceOptions.isNavy = true;
    }

    if (occuMilitaryCareerTitle.isSpaceForce == 'T') {
      this.serviceOptions.isSpaceForce = true;
    }

    if (occuMilitaryCareerTitle.enlisted == 'T') {
      this.enlistOptions.enlisted = true;
    }

    if (occuMilitaryCareerTitle.officer == 'T') {
      this.enlistOptions.officer = true;
    }

  }

  async getMilitaryPlans(planId: number) {
    try {
      let response = await this._militaryPlanService.get(planId);
      return await response;
    } catch (error) {
      console.error("ERROR", error);
    }
  }

  async getAvailableMilitaryCareersForOccu(socId) {

    try {

      let militaryMoreDetails = await this._occufindRestFactoryService.getMilitaryMoreDetails(socId).toPromise();

      return [].concat.apply([], await Promise.all(militaryMoreDetails.map(async (military: any) => {

        this.occuMilitaryCareerTitles.push(military);
        this.adjustServiceOptionsAndEnlistOptions(military);

        let militaryServiceOffering = await this._militaryPlanService.getServiceOffering(military.mcId);

        let uniqueAvailableJobTitles: any = await militaryServiceOffering;

        let returnAvailableJobTitles: any[] = [];

        for (var g = 0; g < uniqueAvailableJobTitles.length; g++) {


          returnAvailableJobTitles.push({
            mcId: uniqueAvailableJobTitles[g].mcId,
            moc: {
              moc: uniqueAvailableJobTitles[g].mocId,
              svc: uniqueAvailableJobTitles[g].svc,
              mpc: uniqueAvailableJobTitles[g].mpc
            },
            title: uniqueAvailableJobTitles[g].mocTitle,
            service: uniqueAvailableJobTitles[g].svc
          });

        }

        return await returnAvailableJobTitles;

      })));

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  setSelectAll(checked) {
    if (checked) {
      this.newUserMilitaryPlanForm.get('army').patchValue(true);
      this.newUserMilitaryPlanForm.get('marinecorps').patchValue(true);
      this.newUserMilitaryPlanForm.get('navy').patchValue(true);
      this.newUserMilitaryPlanForm.get('airforce').patchValue(true);
      this.newUserMilitaryPlanForm.get('spaceforce').patchValue(true);
      this.newUserMilitaryPlanForm.get('coastguard').patchValue(true);
      this.serviceCounter = 6;
    } else {
      this.newUserMilitaryPlanForm.get('army').patchValue(false);
      this.newUserMilitaryPlanForm.get('marinecorps').patchValue(false);
      this.newUserMilitaryPlanForm.get('navy').patchValue(false);
      this.newUserMilitaryPlanForm.get('airforce').patchValue(false);
      this.newUserMilitaryPlanForm.get('spaceforce').patchValue(false);
      this.newUserMilitaryPlanForm.get('coastguard').patchValue(false);
      this.serviceCounter = 0;
    }

  }

  cannotSubmitNewUserMilitaryPlanForm(): boolean {

    if (!this.newUserMilitaryPlanForm.dirty || !this.newUserMilitaryPlanForm.valid) {
      return true;
    }

    if (this.serviceCounter <= 0) {
      return true;
    }

    return false;
  }

  saveNewUserMilitaryPlan() {
    let formData: UserMilitaryPlan = {
      id: undefined,
      planId: this.userOccupationPlan.id,
      didSelectEnlisted: (this.newUserMilitaryPlanForm.value.isEnlisted == 'true') ? true : false,
      didSelectOfficer: (this.newUserMilitaryPlanForm.value.isEnlisted == 'false') ? true : false,
      didSelectArmy: (this.serviceOptions.isArmy) ? (this.newUserMilitaryPlanForm.value.army) ? true : false : false,
      didSelectNavy: (this.serviceOptions.isNavy) ? (this.newUserMilitaryPlanForm.value.navy) ? true : false : false,
      didSelectAirForce: (this.serviceOptions.isAirForce) ? (this.newUserMilitaryPlanForm.value.airforce) ? true : false : false,
      didSelectMarineCorps: (this.serviceOptions.isMarines) ? (this.newUserMilitaryPlanForm.value.marinecorps) ? true : false : false,
      didSelectCoastGuard: (this.serviceOptions.isCoastGuard) ? (this.newUserMilitaryPlanForm.value.coastguard) ? true : false : false,
      didSelectSpaceForce: (this.serviceOptions.isSpaceForce) ? (this.newUserMilitaryPlanForm.value.spaceforce) ? true : false : false,
      isInterestedInActiveDuty: undefined,
      isInterestedInReserves: undefined,
      isInterestedInNationalGuard: undefined,
      selectedCareers: undefined,
      rotcPrograms: undefined,
      isNotSure: undefined,
      serviceColleges: undefined,
    }

    this._militaryPlanService.save(this.userOccupationPlan.id, formData).then((newUserMilitaryPlan: UserMilitaryPlan) => {

      if (formData.didSelectEnlisted) {
        //if they selected enlisted
        //create auto tasks only if they do not already exist
        this._planCalendarService.saveAutoDateTask('UserMilitaryPlan', formData.planId, newUserMilitaryPlan.id,
          'https://prod-media.asvabprogram.com/CEP_PDF_Contents/Military_Check_list.pdf', 'Military Enlistment Check List').then((response: any) => {

            this.onTaskAdd.emit(response);

            this.newUserMilitaryPlanForm.reset();
            this.serviceCounter = 0;
            this.userMilitaryPlans.push(newUserMilitaryPlan);
            this.addAnotherMilitaryPlan = false;

          });
      } else {
        this.newUserMilitaryPlanForm.reset();
        this.serviceCounter = 0;
        this.userMilitaryPlans.push(newUserMilitaryPlan);
        this.addAnotherMilitaryPlan = false;
      }



    }).catch(error => {
      console.error("Error:", error);
    })

  }



  async getFavoriteColleges() {

    let favoriteSchools: any = await this._userService.getSchoolFavoritesList();

    return await Promise.all(favoriteSchools.map(async (favoriteSchool: any) => {
      let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(favoriteSchool.unitId).toPromise();
      return await schoolProfile;
    }));

  }

  async getFavoriteCollegesForSocID() {

    try {
      let response = await new Promise((resolve, reject) => {
        this._favoritesRestFactory.getFavoriteSchoolBySocId(this.userOccupationPlan.socId)
          .then((obj) => {
            resolve(obj);
          }).catch(error => {
            reject(error);
          });
      });

      return await response;
    } catch (error) {
      console.error("ERROR", error);
    }


    // return await Promise.all(favoriteSchools.map(async (favoriteSchool: any) => {
    //   let schoolProfile = await this._occufindRestFactoryService.getSchoolProfile(favoriteSchool.unitId).toPromise();
    //   return await schoolProfile;
    // }));


    // let self = this;
    // this._favoritesRestFactory.getFavoriteSchoolBySocId(this.userOccupationPlan.socId).then(async (favoriteSchools: FavoriteSchool[]) => {


  }


  async getCitmFavoriteOccupations() {
    try {
      let response = await this._portfolioRestFactory.getCitmFavoriteOccupationsNF();
      return await response;
    } catch (error) {
      console.error("ERROR", error);
    }

  }

  autoTasksCreated(event) {
    this.onTasksAdd.emit(event);
  }

  async getServiceColleges() {
    let services: string[] = ['A', 'F', 'C', 'M', 'N', 'S']
    //retrieve all service colleges
    //filter out within the specific user plan
    try {
      let serviceColleges: any = await this._ipedsService.getServiceColleges(services);
      return serviceColleges;
    } catch (error) {
      console.error("ERROR:", error);
    }
  }

  autoCalendarsCreated(event) {
    this.onCalendarsAdd.emit(event);
  }

  userMilitaryPlanRemoval(obj: any) {

    if (obj.id) {
      this.userMilitaryPlans.forEach((value: UserMilitaryPlan, index) => {
        if (value.id == obj.id) this.userMilitaryPlans.splice(index, 1);

        if (this.userMilitaryPlans.length == 0) {
          this.addAnotherMilitaryPlan = true;
        }

      });
    }

    if (obj.autoTasksToRemove) {
      this.onTasksRemoval.emit(obj.autoTasksToRemove);
    }

  }

}
