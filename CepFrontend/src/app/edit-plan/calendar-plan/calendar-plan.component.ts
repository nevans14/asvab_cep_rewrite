import { formatDate } from '@angular/common';
import { Inject, LOCALE_ID } from '@angular/core';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { DateDialogComponent } from 'app/core/dialogs/date-dialog/date-dialog.component';
import { TaskDialogComponent } from 'app/core/dialogs/task-dialog/task-dialog.component';
import { UserOccupationPlan } from 'app/core/models/userOccupationPlan.model';
import { UserOccupationPlanCalendar } from 'app/core/models/userOccupationPlanCalendar.model';
import { UserOccupationPlanCalendarTask } from 'app/core/models/userOccupationPlanCalendarTask.model';
import { PlanCalendarService } from 'app/services/plan-calendar.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-calendar-plan',
  templateUrl: './calendar-plan.component.html',
  styleUrls: ['./calendar-plan.component.scss']
})
export class CalendarPlanComponent implements OnInit {

  leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs

  public userOccupationPlanCalendarTasks: UserOccupationPlanCalendarTask[];
  public userOccupationPlanCalendars: any[];
  public tasksUnassignedToDate: UserOccupationPlanCalendarTask[];
  public userOccupationPlans: any[];
  public showCompletedTasks: boolean;
  public unassignedProgress: any;
  public isLoading: boolean;

  @Input() userOccupationPlan: UserOccupationPlan;
  @Input() occupationDescription: string;

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _planCalendarService: PlanCalendarService,
    private _utilityService: UtilityService,
    @Inject(LOCALE_ID) private locale: string
  ) {

    this.userOccupationPlanCalendarTasks = [];
    this.userOccupationPlanCalendars = [];
    this.userOccupationPlans = [];

    this.isLoading = true;

  }

  ngOnInit() {

    this.userOccupationPlans.push(this.userOccupationPlan);

    this.tasksUnassignedToDate = [];

    let userOccupationPlanCalendarTasks = this.getUserOccupationPlanCalendarTasks(this.userOccupationPlan.id);
    let userOccupationPlanCalendars = this.getUserOccupationPlanCalendars(this.userOccupationPlan.id);


    Promise.all([userOccupationPlanCalendarTasks, userOccupationPlanCalendars]).then((done: any) => {

      this.userOccupationPlanCalendarTasks = done[0];
      this.userOccupationPlanCalendars = done[1];

      this.showCompletedTasks = false;  //for unassigned tasks to dates

      //create initial filter option for dates with tasks
      //create initial option for expanding list to true
      this.userOccupationPlanCalendars.map((userOccupationPlanCalendar) => {
        userOccupationPlanCalendar.showCompletedTasks = false;
        userOccupationPlanCalendar.expanded = true;

        return userOccupationPlanCalendar;
      });

      this.setUnassignedTasks();

      this.isLoading = false;
    });

  }

  async getUserOccupationPlanCalendarTasks(taskId) {

    try {

      let userOccupationPlanCalendarTasks = await this._planCalendarService.getPlanTasks(taskId);

      return await userOccupationPlanCalendarTasks;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  async getUserOccupationPlanCalendars(taskId) {

    try {

      let userOccupationPlanCalendars = await this._planCalendarService.getPlanCalendars(taskId);

      return await userOccupationPlanCalendars;

    } catch (error) {
      console.error("ERROR", error);
    }

  }

  onLeftNavCollapsed(e) {
    this.leftNavCollapsed = true;
  }

  addDate() {
    let data = {
      planId: this.userOccupationPlan.id,
      canEditPlan: false
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        let userOccupationPlanCalendar: any = result;

        if (userOccupationPlanCalendar.tasks) {
          if (userOccupationPlanCalendar.tasks.length > 0) {
            userOccupationPlanCalendar.showCompletedTasks = false;
            userOccupationPlanCalendar.expanded = true;
          }
        }
        this.userOccupationPlanCalendars.push(userOccupationPlanCalendar);

      }
    });

  }

  addTask() {

    let data = {
      planId: this.userOccupationPlan.id
    }

    const dialogRef = this._dialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let userOccupationPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        if (userOccupationPlanCalendarTask.calendarId) {

          this.userOccupationPlanCalendars.find(x => x.id == userOccupationPlanCalendarTask.calendarId).tasks.push(userOccupationPlanCalendarTask);

        } else {
          this.userOccupationPlanCalendarTasks.push(userOccupationPlanCalendarTask);
        }

        this.setUnassignedTasks();

      }
    });
  }

  setUnassignedTasks() {

    //set tasks
    this.tasksUnassignedToDate = this.userOccupationPlanCalendarTasks.filter(x => (!x.calendarId && (x.completed == this.showCompletedTasks)));

    this.unassignedProgress = this.getProgressBarValue(this.userOccupationPlanCalendarTasks.length, this.tasksUnassignedToDate.length);

  }

  getProgressBarValue(max: number, current: number) {
    return (max-current) / max * 100;
  }

  getProgressBarValueForCalendar(calendar: UserOccupationPlanCalendar) {

    //set tasks
    if (calendar.tasks) {

      let notCompletedTasks = calendar.tasks.filter(x => x.completed == false);

      return (this.getProgressBarValue(calendar.tasks.length, notCompletedTasks.length));

    }

    return 0;


  }

  getRemainingTasksForCalendar(calendar: UserOccupationPlanCalendar, showCompleted: boolean) {

    //get tasks
    if (calendar.tasks) {
      let notCompletedTasks = calendar.tasks.filter(x => x.completed == showCompleted);
      if (notCompletedTasks) {
        return notCompletedTasks.length;
      }
    }

    return 0;
  }

  switchTaskViewByCompleted() {

    this.showCompletedTasks = !this.showCompletedTasks;

    this.tasksUnassignedToDate = this.userOccupationPlanCalendarTasks.filter(x => (!x.calendarId && (x.completed == this.showCompletedTasks)));

  }

  editTask(task: UserOccupationPlanCalendarTask, index, calendarIndex = null) {

    let data = {
      planId: task.planId,
      task: task,
      canEditPlan: false
    }

    const dialogRef = this._dialog.open(TaskDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let updatedPlanCalendarTask: UserOccupationPlanCalendarTask = result;

        //check to see if updated record has a calendar id
        if (updatedPlanCalendarTask.calendarId) {

          //remove from previous calendar only if id's do not match up

          //check if accessed from calendar tasks
          if (calendarIndex == null) {

            //edit task was from unassigned
            //assign to new calendar
            //add record to ui
            this.userOccupationPlanCalendars.find(x => x.id == updatedPlanCalendarTask.calendarId).tasks.push(updatedPlanCalendarTask);

            //remove from unassigned tasks in ui
            this.userOccupationPlanCalendarTasks.splice(this.userOccupationPlanCalendarTasks.findIndex(x => x.id == updatedPlanCalendarTask.id), 1);
            this.setUnassignedTasks();

          } else {
            let prevUserOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

            //remove from previous calendar only if previous record had a calendar id and it doesn't match the new one
            if (prevUserOccupationPlanCalendar.calendarId != updatedPlanCalendarTask.calendarId) {

              let removeIndex = prevUserOccupationPlanCalendar.tasks.findIndex(x => x.id == updatedPlanCalendarTask.id);
              this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);

              //add record to ui
              this.userOccupationPlanCalendars.find(x => x.id == updatedPlanCalendarTask.calendarId).tasks.push(updatedPlanCalendarTask);

            }

          }

        } else {

          //no calendar date was assigned to modified task

          //check if accessed from calendar tasks i.e. had a previously assigned calendar date
          //if so remove it from ui
          if (calendarIndex == null) {
            //unassigned task to a date simply update based on response
            this.tasksUnassignedToDate[index] = updatedPlanCalendarTask;

          } else {
            let userOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

            let removeIndex = userOccupationPlanCalendar.tasks.findIndex(x => x.id == updatedPlanCalendarTask.id);
            this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);

            //add record to ui
            this.userOccupationPlanCalendarTasks.push(updatedPlanCalendarTask);
            this.setUnassignedTasks();
          }

        }

      }
    });

  }

  removeTask(taskId, index, calendarIndex = null) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Task, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._planCalendarService.deleteTask(taskId).then((rowsAffected: number) => {

            if (rowsAffected > 0) {

              if (calendarIndex == null) {
                //update ui for unassigned tasks
                this.userOccupationPlanCalendarTasks.splice(this.userOccupationPlanCalendarTasks.findIndex(x => x.id == taskId), 1);
                this.setUnassignedTasks();
              } else {
                let userOccupationPlanCalendar = this.userOccupationPlanCalendars[calendarIndex];

                let removeIndex = userOccupationPlanCalendar.tasks.findIndex(x => x.id == taskId);
                this.userOccupationPlanCalendars[calendarIndex].tasks.splice(removeIndex, 1);
              }


            } else {
              console.error("ERROR, record was not deleted. Please try again.");
            }

          }).catch(error => console.error("ERROR", error));

        }
      }
    });
  }

  setTaskComplete(task: UserOccupationPlanCalendarTask, taskIndex, event, calendarIndex = null) {

    const verbiage: string = (event.target.checked) ? "Complete" : "Reset";

    let dialogData = {
      title: 'Confirm Completion',
      message: verbiage + ' task <strong>' + this.convertTaskName(task.taskName) + '</strong>, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          task.completed = event.target.checked;
          //updating an existing record
          this._planCalendarService.updateTask(task, task.id).then((rowsAffected: number) => {

            if (task.calendarId) {
              this.userOccupationPlanCalendars.find(x => x.id == task.calendarId).tasks.find(y => y.id == task.id).completed = event.target.checked;
            } else {
              this.userOccupationPlanCalendarTasks[this.userOccupationPlanCalendarTasks.findIndex(x => x.id == task.id)].completed = event.target.checked;
            }

            this.setUnassignedTasks(); //refresh view

          }).catch((error: any) => {
            console.error("ERROR", error);
          });

        } else {
          event.target.checked = !event.target.checked;
        }
      } else {
        event.target.checked = !event.target.checked;
      }
    });

  }

    /**
   * 
   * @param date 
   * @returns returns an adjusted date due to the server storing a date as a midnight; with utc conversion it was removing a day from 
   * timestamp.
   */
     adjustDate(date) {
      let returnDate: Date = new Date(this._utilityService.convertServerUTCToUniversalUTC(date));
      returnDate.setDate(returnDate.getDate() + 1);
      return returnDate;
    }

  editDate(date: UserOccupationPlanCalendar) {

    date.date = this.adjustDate(date.date).toLocaleDateString();

    let data = {
      userOccupationPlanCalendar: date,
      planId: this.userOccupationPlan.id,
      canEditPlan: false
    }

    const dialogRef = this._dialog.open(DateDialogComponent, { data: data, maxWidth: '500px', autoFocus: false });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        let calendarDateToUpdate: UserOccupationPlanCalendar = result;

        let existingIndex = this.userOccupationPlanCalendars.findIndex(x => x.id == calendarDateToUpdate.id);

        //update tasks
        if (calendarDateToUpdate.tasks) {
          calendarDateToUpdate.tasks.forEach((task: UserOccupationPlanCalendarTask) => {

            if(this.userOccupationPlanCalendars[existingIndex].tasks){
              let index = this.userOccupationPlanCalendars[existingIndex].tasks.findIndex(x => x.id == task.id);

              if (index > -1) {
                //record was found; update
                this.userOccupationPlanCalendars[existingIndex].tasks[index] = task;
              } else {
                this.userOccupationPlanCalendars[existingIndex].tasks.push(task);
              }
            }else{
              this.userOccupationPlanCalendars[existingIndex].tasks = [];
              this.userOccupationPlanCalendars[existingIndex].tasks.push(task);
            }
            
          });
        }

        this.userOccupationPlanCalendars[existingIndex].planId = calendarDateToUpdate.planId;
        this.userOccupationPlanCalendars[existingIndex].title = calendarDateToUpdate.title;
        this.userOccupationPlanCalendars[existingIndex].date = 
          formatDate(new Date(calendarDateToUpdate.date).toLocaleDateString(),'MM/dd/yyyy', this.locale);
      }
    });

  }

  removeDate(calendarId) {

    let dialogData = {
      title: 'Confirm Deletion',
      message: 'Remove Date, are you sure?'
    };

    this._dialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._planCalendarService.deleteCalendar(calendarId).then((rowsAffected: number) => {

            if (rowsAffected > 0) {
              //remove record from ui
              let removeIndex = this.userOccupationPlanCalendars.findIndex(x => x.id == calendarId);
              this.userOccupationPlanCalendars.splice(removeIndex, 1);
            } else {
              console.error("ERROR, record was not deleted. Please try again.");
            }

          }).catch(error => console.error("ERROR", error));

        }
      }
    });
  }

  convertTaskName(taskName) {

    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).title;
    }

    return taskName;
  }

  hasUrl(taskName): boolean{
    if (this._utilityService.isJson(taskName)) {
      return true;
    }

    return false;
  }

  getUrl(taskName){
    if (this._utilityService.isJson(taskName)) {
      return JSON.parse(taskName).url;
    }

    return taskName;
  }

}
