import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ContentManagementService } from '../services/content-management.service';
import { UtilityService } from 'app/services/utility.service';
import { UserService } from 'app/services/user.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { ConfigService } from 'app/services/config.service';
import { ViewDetailDialogComponent } from 'app/core/dialogs/view-detail-dialog/view-detail-dialog.component';
import { StrategyTakingAsvabDialogComponent } from 'app/core/dialogs/strategy-taking-asvab-dialog/strategy-taking-asvab-dialog.component';

import $ from 'jquery'
declare var $: $
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  // Current page from the DB
  pageHtml: any = undefined;
  studentCep: boolean = true;
  studentExplore: boolean = false;
  studentLearn: boolean = false;
  studentPlan: boolean = false;
  path;
  page;
  showStudent: boolean = false;
  showStudentLearn: boolean = false;
  showStudentExplore: boolean = false;
  showStudentPlan: boolean = false;
  resource: string;
  documents: string;

  constructor(
    private _contentManagementService: ContentManagementService,
    private _router: Router,
    private _utility: UtilityService,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _dialog: MatDialog,
    private _user: UserService,
    private _config: ConfigService,
  ) {
    this.resource = this._config.getImageUrl();
    this.documents = this.resource + 'CEP_PDF_Contents/';
  }

  ngOnInit() {
    this.path = this._router.url;
    console.debug('url:', this.path);

    this.setSelectedTab(this.path);

    $(document).ready(function () {
      $(document).on('click', '.interest-nav-tabs .interest-lists li a, .interest_triangle li a', function (e) {
        $('.interest-nav-tabs .interest-lists li, .interest_triangle li').removeClass('active');
        var tagid = $(this).data('toggle');
        $('.interest-lists li a[data-toggle=' + tagid + '], .interest_triangle li a[data-toggle=' + tagid + ']')
          .parent().addClass('active');
        $('.tab-pane').removeClass('active');
        $('#' + tagid).addClass('active');
      });

      $(document).on('click', '.cep_tabs a', function (e) {
        var $_cep_tabs = $(this) .closest('.cep_tabs')
        $_cep_tabs.find('.nav-tabs li').removeClass('active');
        var tagid = $(this).data('toggle');
        $_cep_tabs.find('.nav-tabs a[data-toggle=' + tagid + ']').parent().addClass('active');
        $_cep_tabs.find('.tab-pane').removeClass('active');
        $_cep_tabs.find('#' + tagid).addClass('active');
      });
    });

    switch (this.path) {
      case '/student':
        this.showStudent = true;
        break;
      case '/student-learn':
        this.showStudentLearn = true;
        break;
      case '/student-explore':
        this.showStudentExplore = true;
        break;
      case '/student-plan':
        this.showStudentPlan = true;
        break;
    }

    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  redirect(path) {
    this.setSelectedTab(path);
    this._router.navigate([path]);
  }

  setSelectedTab(path) {
    if (path.indexOf('explore') > -1) {
      this.studentCep = false;
      this.studentLearn = false;
      this.studentExplore = true;
      this.studentPlan = false;
    } else if (path.indexOf('learn') > -1) {
      this.studentCep = false;
      this.studentLearn = true;
      this.studentExplore = false;
      this.studentPlan = false;
    } else if (path.indexOf('plan') > -1) {
      this.studentCep = false;
      this.studentLearn = false;
      this.studentExplore = false;
      this.studentPlan = true;
    } else if (this.path.indexOf('student') > -1) {
      this.studentCep = true;
      this.studentLearn = false;
      this.studentExplore = false;
      this.studentPlan = false;
    }
  }

  showViewDetailModal() {
    if (this._user.isLoggedIn()) {
      window.location.href = '/occufind-occupation-search';
    } else {
      this._dialog.open(ViewDetailDialogComponent , {
        hasBackdrop: true,
        panelClass: 'new-modal',
        // height: '95%',
        width: '300px',
        autoFocus: true,
        disableClose: true
      });
    }
  }

  showStrategyTakingAsvabModal(){

    this._dialog.open(StrategyTakingAsvabDialogComponent , {
      minWidth: '80vw',
      autoFocus: false,
      disableClose: true,
      // data: modalOptions
    });
  }

}
