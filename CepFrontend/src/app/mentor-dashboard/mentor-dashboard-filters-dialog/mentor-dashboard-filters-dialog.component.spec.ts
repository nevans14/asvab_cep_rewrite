import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorDashboardFiltersDialogComponent } from './mentor-dashboard-filters-dialog.component';

describe('MentorDashboardFiltersDialogComponent', () => {
  let component: MentorDashboardFiltersDialogComponent;
  let fixture: ComponentFixture<MentorDashboardFiltersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorDashboardFiltersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorDashboardFiltersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
