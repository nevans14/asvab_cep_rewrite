import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'app/services/user.service';

interface AvailableFilters {
  groups: any[],
  graduationDates: any[],
  studentName: any,
  educationLevels: any[],
  gender: any[],
  asvabAdminMonth: any,
  asvabAdminYear: any
}

@Component({
  selector: 'app-mentor-dashboard-filters-dialog',
  templateUrl: './mentor-dashboard-filters-dialog.component.html',
  styleUrls: ['./mentor-dashboard-filters-dialog.component.scss']
})
export class MentorDashboardFiltersDialogComponent implements OnInit {

  public isSaving: boolean;
  public isLoading: boolean;
  public students: any[];
  public studentGroups: any[];
  public availableMonths: any[];
  public availableYears: any[];
  public availableFilters: AvailableFilters;
  public selectedFilters: any;
  public selectedGroups: any[];
  public selectedGraduationDates: any[];
  public selectedEducationLevels: any[];

  constructor(public dialogRef: MatDialogRef<MentorDashboardFiltersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService,
  ) {

    this.isSaving = false;
    this.isLoading = true;
    this.studentGroups = [];
    this.students = [];
    this.availableYears = [];
    this.availableMonths = [];
    this.selectedFilters = {};
    this.selectedGroups = [];
    this.selectedGraduationDates = [];
    this.selectedEducationLevels = [];
  }

  ngOnInit() {

    let currentFullYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.availableYears.push(currentFullYear - i);
    }

    this.availableMonths = [
      { text: '1 - January', value: 1 },
      { text: '2 - February', value: 2 },
      { text: '3 - March', value: 3 },
      { text: '4 - April', value: 4 },
      { text: '5 - May', value: 5 },
      { text: '6 - June', value: 6 },
      { text: '7 - July', value: 7 },
      { text: '8 - August', value: 8 },
      { text: '9 - September', value: 9 },
      { text: '10 - October', value: 10 },
      { text: '11 - November', value: 11 },
      { text: '12 - December', value: 12 }];

    //set groups and graduation dates
    let availableGroups = [];
    let availableGraduationDates = [];
    let availableEducationLevels = [];

    if (this.data.students && this.data.students.length > 0) {

      this.data.students.forEach((student: any) => {

        if (student.group) {
          availableGroups.push(student.group.groupName);
        }
        if (student.grade) {
          if (student.grade.toString().indexOf('12') > -1) {
            availableGraduationDates.push(currentFullYear + ' - ' + (currentFullYear + 1));
            availableEducationLevels.push('12');
          }
          if (student.grade.toString().indexOf('11') > -1) {
            availableGraduationDates.push((currentFullYear + 1) + ' - ' + (currentFullYear + 2));
            availableEducationLevels.push('11');
          }
          if (student.grade.toString().indexOf('10') > -1) {
            availableGraduationDates.push((currentFullYear + 2) + ' - ' + (currentFullYear + 3));
            availableEducationLevels.push('10');
          }
        }

      })

    }

    this.availableFilters = {
      groups: (availableGroups) ? Array.from(new Set(availableGroups)) : [],
      graduationDates: (availableGraduationDates) ? Array.from(new Set(availableGraduationDates)) : [],
      studentName: '',
      educationLevels: (availableEducationLevels) ? Array.from(new Set(availableEducationLevels)) : [],
      gender: ['male', 'female'],
      asvabAdminMonth: this.availableMonths,
      asvabAdminYear: this.availableYears
    }

    this.selectedFilters = (this.data.selectedFilters) ? this.data.selectedFilters : {};

    if (this.selectedFilters && this.selectedFilters.groups && this.selectedFilters.groups.length > 0) {
      this.selectedGroups = this.selectedFilters.groups;
    }

    if (this.selectedFilters && this.selectedFilters.graduationDates && this.selectedFilters.graduationDates.length > 0) {
      this.selectedGraduationDates = this.selectedFilters.graduationDates;
    }

    if (this.selectedFilters && this.selectedFilters.educationLevels && this.selectedFilters.educationLevels.length > 0) {
      this.selectedEducationLevels = this.selectedFilters.educationLevels;
    }

    this.isLoading = false;

  }

  close() {
    this.dialogRef.close();
  }

  applyFilters() {

    let selectedFilters = {
      groups: this.selectedGroups, //get groups
      graduationDates: this.selectedGraduationDates,  //get grad dates
      educationLevels: this.selectedEducationLevels,
      genderFilter: this.selectedFilters.genderFilter,
      studentName: this.selectedFilters.studentName,
      asvabAdminMonth: this.selectedFilters.asvabAdminMonth,
      asvabAdminYear: this.selectedFilters.asvabAdminYear
    }

    this.dialogRef.close(selectedFilters);

  }

  updateSelectedGroupFilter(groupName, checked) {
    if (checked) {
      this.selectedGroups.push(groupName);
    } else {
      let index = this.selectedGroups.findIndex(x => x == groupName);
      if (index > -1) {
        this.selectedGroups.splice(index, 1);
      }
    }
  }

  updateSelectedGraduationDates(graduationDate, checked) {
    if (checked) {
      this.selectedGraduationDates.push(graduationDate);
    } else {
      let index = this.selectedGraduationDates.findIndex(x => x == graduationDate);
      if (index > -1) {
        this.selectedGraduationDates.splice(index, 1);
      }
    }
  }

  updateSelectedEducationLevels(educationLevel, checked) {
    if (checked) {
      this.selectedEducationLevels.push(educationLevel);
    } else {
      let index = this.selectedEducationLevels.findIndex(x => x == educationLevel);
      if (index > -1) {
        this.selectedEducationLevels.splice(index, 1);
      }
    }
  }

  getSelectedGroupFilter(groupName) {
    let index = this.selectedGroups.findIndex(x => x == groupName);
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  getSelectedGraduationDateFilter(graduationDate) {
    let index = this.selectedGraduationDates.findIndex(x => x == graduationDate);
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  getSelectedEducationLevelFilter(educationLevel){
    let index = this.selectedEducationLevels.findIndex(x => x == educationLevel);
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }

  hasFilter(){

    if(this.selectedGroups && this.selectedGroups.length > 0){ return true}
    if(this.selectedGraduationDates && this.selectedGraduationDates.length > 0){ return true}
    if(this.selectedEducationLevels && this.selectedEducationLevels.length > 0){ return true}
    if(this.selectedFilters && this.selectedFilters.genderFilter ){ return true}
    if(this.selectedFilters && this.selectedFilters.studentName ){ return true}
    if(this.selectedFilters && this.selectedFilters.asvabAdminMonth ){ return true}
    if(this.selectedFilters && this.selectedFilters.asvabAdminYear ){ return true}

    return false;
  }

}
