import { Component, OnInit, LOCALE_ID, Inject } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { UtilityService } from 'app/services/utility.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { RecentMessagesDialogComponent } from 'app/core/dialogs/recent-messages-dialog/recent-messages-dialog.component';
import { formatDate } from '@angular/common';
import { DirectMessageDialogComponent } from 'app/core/dialogs/direct-message-dialog/direct-message-dialog.component';
import { MentorDateDialogComponent } from 'app/core/dialogs/mentor-date-dialog/mentor-date-dialog.component';
import { MentorTaskDialogComponent } from 'app/core/dialogs/mentor-task-dialog/mentor-task-dialog.component';
import { MentorService } from 'app/services/mentor.service';
import { Subscription } from 'rxjs';
import { MentorGroupDialogComponent } from 'app/core/dialogs/mentor-group-dialog/mentor-group-dialog.component';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { MentorDashboardFiltersDialogComponent } from './mentor-dashboard-filters-dialog/mentor-dashboard-filters-dialog.component';
import { LinkedSchoolsDialogComponent } from 'app/core/dialogs/linked-schools-dialog/linked-schools-dialog.component';
import { SelectedSchool } from 'app/core/models/selectedSchool';
import { BringAsvabToYourSchoolComponent } from 'app/core/dialogs/bring-asvab-to-your-school/bring-asvab-to-your-school.component';
import { MentorStudentReportDialogComponent } from 'app/core/dialogs/mentor-student-report-dialog/mentor-student-report-dialog.component';
import { StatusLegendComponent } from 'app/core/dialogs/status-legend/status-legend.component';

enum SelectionType {
	ADD_CALENDAR_DATE = 1,
	ADD_TASK = 2,
	SEND_DM = 3,
	CREATE_GROUP = 4,
	REMOVE_STUDENT = 5,
	REMOVE_STUDENT_GROUP = 6,
	RUN_REPORT = 7
}

@Component({
	selector: 'app-mentor-dashboard',
	templateUrl: './mentor-dashboard.component.html',
	styleUrls: ['./mentor-dashboard.component.scss']
})
export class MentorDashboardComponent implements OnInit {

	leftNavCollapsed = true; //show content for desktop and tablet, mobile don't show content until user click on 1 of 3 tabs

	public studentList: any[];
	public studentGroups: any[];
	public studentPendingList: any[];
	public eventList: any[];
	public userName: any;
	public closeHint_MENTOR_DASHBOARD_WELCOME: boolean;
	public closeHint_MENTOR_DASHBOARD_TEST_DRIVE: boolean;
	public unreadMsgCounterTotal: number;
	public selectedStudents: string[];
	public actionSelection: SelectionType;
	subscription: Subscription;
	public selectedFilters: any;
	public isParent: boolean;
	public selectedSchools: SelectedSchool[];

	userCompletion;

	constructor(private _user: UserService,
		private _dialog: MatDialog,
		@Inject(LOCALE_ID) private locale: string,
		private _utilityService: UtilityService,
		private _activatedRoute: ActivatedRoute,
		private _mentorService: MentorService,
		private _router: Router) {

		this.studentList = [];
		this.studentGroups = [];
		this.eventList = [];
		this.userName = {};
		this.closeHint_MENTOR_DASHBOARD_WELCOME = false;
		this.unreadMsgCounterTotal = 0;
		this.selectedStudents = [];
		this.actionSelection = 0;
		this.selectedFilters = null;
		this.isParent = false;
		this.selectedSchools = [];
	}

	ngOnInit() {

		if (window.innerWidth < 640) {
			this.leftNavCollapsed = this._user.getLeftNavCollapsed();
		}

		this.isParent = (this._user.getUser().currentUser.role.toLowerCase() == 'd') ? true : false;
		let userInfo = this._activatedRoute.snapshot.data.mentorDashboardResolver[0];

		if (userInfo) {
			if (userInfo.firstName && userInfo.lastName) {
				this.userName = this._utilityService.capitalizeFirstLetter2(userInfo.firstName) + " " + this._utilityService.capitalizeFirstLetter2(userInfo.lastName);
			} else {
				this.userName = userInfo.username;
			}
		} else {
			this.userName = '';
		}

		let studentList = this._activatedRoute.snapshot.data.mentorDashboardResolver[1];
		this.studentGroups = this._activatedRoute.snapshot.data.mentorDashboardResolver[2];
		this.studentPendingList = this._activatedRoute.snapshot.data.mentorDashboardResolver[3];
		this.selectedSchools = this._activatedRoute.snapshot.data.mentorDashboardResolver[4];

		this.setStudentList(studentList);

		//subscribe and set the visibility of hints for the page.
		this.setHintsSubscription();

		this.setStudentsSubscription();
		this._user.sendUserUpdate("update");
	}

	ngOnDestroy() {

		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	getDateInvited(pendingStudent) {
		if (pendingStudent && pendingStudent.dateInvited) {
			const dateDb = new Date(this._utilityService.convertServerUTCToUniversalUTC(pendingStudent.dateInvited));
			const dateLocal = new Date(Date.UTC(
				dateDb.getFullYear(),
				dateDb.getMonth(),
				dateDb.getDate(),
				dateDb.getHours(),
				dateDb.getMinutes(),
				dateDb.getSeconds(),
			))
			return formatDate(dateLocal, 'MM/dd/yyyy', this.locale);
		}
	}

	onLeftNavCollapsed(e) {
		this.leftNavCollapsed = true;
	}

	updateStudentLists() {
		this._mentorService.getStudents().then((students: any) => {
			this.studentList = students;
		});
		this._mentorService.getPendingStudents().then((studentsPending: any) => {
			this.studentPendingList = studentsPending;
		});
	}

	pendingAction(action, mentorLinkId) {
		if (action.toLowerCase() === "accept") {
			this._mentorService.acceptStudentInvite(mentorLinkId).then(success => {
				this.updateStudentLists();
			});
		} else if (action.toLowerCase() === 'deny') {
			this._mentorService.denyStudentInvite(mentorLinkId).then(success => {
				this.updateStudentLists();
			});
		}
	}

	openRecentMessagesDialog(student: any = null) {

		if (!student) {
			const dialogRef = this._dialog.open(RecentMessagesDialogComponent, {
				disableClose: true,
				data: { students: this.studentList }
			});
		} else {
			const dialogRef = this._dialog.open(RecentMessagesDialogComponent, {
				disableClose: true,
				data: { students: [student], studentName: student.firstName + ' ' + student.lastName }
			});
		}

	}

	openStatusLegend() {
		const dialogRef = this._dialog.open(StatusLegendComponent);
	}

	setStudentList(studentList: any[]) {

		if (studentList && studentList.length > 0) {
			this.unreadMsgCounterTotal = 0;

			studentList.forEach((student: any) => {
				//set the unread message count for the mentor
				if (student.unreadLogs && student.unreadLogs.length > 0) {
					this.unreadMsgCounterTotal += student.unreadLogs.length;
				}
				if (student.unreadMessages && student.unreadMessages.length > 0) {
					this.unreadMsgCounterTotal += student.unreadMessages.length;
				}
			})

			//set up student groups
			if (this.studentGroups && this.studentGroups.length > 0) {
				this.studentGroups.forEach((group: any) => {
					let studentsInGroup = studentList.filter(student => (student.group && student.group.groupId == group.groupId));
					if (studentsInGroup) {
						group.students = studentsInGroup;
					}
				})
			}
		}

		this.studentList = studentList;

	}

	//set the visibility of hints for the page.
	setHintsSubscription() {
		this._user.getCompletion().subscribe(data => {
			this.userCompletion = data;

			if (this._user.checkUserCompletion(this.userCompletion, UserService.MENTOR_DASHBOARD_WELCOME)) {
				this.closeHint_MENTOR_DASHBOARD_WELCOME = true;
			}

			if (this._user.checkUserCompletion(this.userCompletion, UserService.MENTOR_DASHBOARD_TEST_DRIVE)) {
				this.closeHint_MENTOR_DASHBOARD_TEST_DRIVE = true;
			}

		});

	}

	setStudentsSubscription() {
		this.subscription = this._mentorService.getStudentUpdates().subscribe(async hasUpdate => {

			let students = this._mentorService.getStudents();
			let studentGroups = this._mentorService.getGroups();

			await Promise.all([students, studentGroups]).then((done: any) => {

				this.studentGroups = done[1];

				this.setStudentList(done[0]);

				this.selectedStudents = [];

			})

		})
	}

	closeHint(hint) {

		this._user.setCompletion(hint, 'true').then((response: any) => {
			if (hint == "MENTOR_DASHBOARD_WELCOME") {
				this.closeHint_MENTOR_DASHBOARD_WELCOME = true;
			}
			if (hint == "MENTOR_DASHBOARD_TEST_DRIVE") {
				this.closeHint_MENTOR_DASHBOARD_TEST_DRIVE = true;
			}

		}).catch(error => console.error("ERROR", error));
	}

	getPortfolioStatusCssClass(portfolio) {

		if (!portfolio) {
			return "not_started";
		}

		if (portfolio.statusId == 5) {
			if (portfolio.mentors) {
				return "checkmark";
			} else {
				return "not_started";
			}
		};

		if (portfolio.statusId == 2) {
			if (portfolio.mentors) {
				return "ready_for_review";
			} else {
				return "not_started";
			}
		};

		return "in_progress";
	}

	getFYIStatusCssClass(isFyiCompleted) {

		if (!isFyiCompleted) return "not_started";

		return "checkmark";

	}

	getOccupationPlanStatusCssClass(occupationPlan, mentorLinkId) {
		return this._utilityService.getUserOccupationStatusCssClass(occupationPlan, mentorLinkId);
	}

	getActivityStatusCssClass(activity) {
		return this._utilityService.getActivityStatusCssClass(activity);
	}

	getStudentLastUpdateDate(student) {

		// console.log("student", student);

		let lastUpdateDate: any = null;

		//get last activity last update date
		if (student.activitySubmissions && student.activitySubmissions.length > 0) {

			student.activitySubmissions.forEach((activitySubmission: any) => {

				let currentLastUpdateDate: any = null;


				//order submissions for the activity
				let sortedSubmission = activitySubmission.submissions.sort((a, b) => {
					let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
						db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
					return da - db;
				})

				let lastSortedSubmission = sortedSubmission[sortedSubmission.length - 1];


				currentLastUpdateDate = lastSortedSubmission.dateUpdated ? lastSortedSubmission.dateUpdated : lastSortedSubmission.dateCreated;

				if (currentLastUpdateDate != null) {
					if (lastUpdateDate != null) {
						if (lastUpdateDate < currentLastUpdateDate) {
							lastUpdateDate = currentLastUpdateDate;
						}
					} else {
						lastUpdateDate = currentLastUpdateDate;
					}
				}

			})

		}

		//get last update date for occu plans
		if (student.occupationPlans && student.occupationPlans.length > 0) {
			let sortedPlans = student.occupationPlans.sort((a, b) => {
				let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
					db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
				return da - db;
			});

			let lastSortedPlan = sortedPlans[sortedPlans.length - 1];
			let lastUpdateDateForPlan = lastSortedPlan.dateUpdated ? lastSortedPlan.dateUpdated : lastSortedPlan.dateCreated;

			if (lastUpdateDate != null) {
				if (lastUpdateDate < lastUpdateDateForPlan) {
					lastUpdateDate = lastUpdateDateForPlan;
				}
			} else {
				lastUpdateDate = lastUpdateDateForPlan;
			}

		}

		//get last update date for portfolio
		if (student.portfolio) {
			let studentPortfolio = student.portfolio;
			let studentLastUpdateDate = (studentPortfolio.dateUpdated) ? studentPortfolio.dateUpdated : (studentPortfolio.dateCreated)
				? studentPortfolio.dateCreated : null;
			if (studentLastUpdateDate) {
				if (lastUpdateDate != null) {
					if (lastUpdateDate < studentLastUpdateDate) {
						lastUpdateDate = studentLastUpdateDate;
					}
				} else {
					lastUpdateDate = studentLastUpdateDate;
				}
			}
		}

		//get last update date for FYI
		if (student.fyiProfile && student.fyiProfile.dateUpdated) {
			if (lastUpdateDate != null) {
				if (lastUpdateDate < student.fyiProfile.dateUpdated) {
					lastUpdateDate = student.fyiProfile.dateUpdated;
				}
			} else {
				lastUpdateDate = student.fyiProfile.dateUpdated;
			}
		}

		if (student.workValueProfile && student.workValueProfile.dateUpdated) {
			if (lastUpdateDate != null) {
				if (lastUpdateDate < student.workValueProfile.dateUpdated) {
					lastUpdateDate = student.workValueProfile.dateUpdated;
				}
			} else {
				lastUpdateDate = student.workValueProfile.dateUpdated;
			}
		}

		if (student.moreAboutMe && student.moreAboutMe.length > 0) {
			let sortedMoreAboutMeList = student.moreAboutMe.sort((a, b) => {
				let da: any = (a.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(a.dateCreated)),
					db: any = (b.dateUpdated) ? new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateUpdated)) : new Date(this._utilityService.convertServerUTCToUniversalUTC(b.dateCreated));
				return da - db;
			});

			let lastMoreAboutMeRecord = sortedMoreAboutMeList[sortedMoreAboutMeList.length - 1];
			if (lastMoreAboutMeRecord && lastMoreAboutMeRecord.dateUpdated) {
				if (lastUpdateDate != null) {
					if (lastUpdateDate < lastMoreAboutMeRecord.dateUpdated) {
						lastUpdateDate = lastMoreAboutMeRecord.dateUpdated;
					}
				} else {
					lastUpdateDate = lastMoreAboutMeRecord.dateUpdated;
				}
			}

		}

		if (lastUpdateDate != null) {
			return formatDate(new Date(this._utilityService.convertServerUTCToUniversalUTC(lastUpdateDate)), 'MM/dd/yyyy', this.locale);
		}

		return "";

	}

	updateSelectedStudentList(mentorLinkId, checked) {

		if (checked) {
			this.selectedStudents.push(mentorLinkId);
		} else {
			let index = this.selectedStudents.findIndex(x => x == mentorLinkId);
			if (index > -1) {
				this.selectedStudents.splice(index, 1);
			}
		}

	}

	updateSelectedStudentListByGroup(groupId, checked) {
		if (checked) {
			let selectedStudents = this.studentList.filter((x: any) => (x.group && x.group.groupId == groupId));

			if (selectedStudents && selectedStudents.length > 0) {
				selectedStudents.forEach((x: any) => this.selectedStudents.push(x.mentorLinkId));
			}
		} else {
			let selectedStudents = this.studentList.filter((x: any) => (x.group && x.group.groupId == groupId));
			if (selectedStudents && selectedStudents.length > 0) {
				selectedStudents.forEach((x: any) => {
					let mentorLinkId = x.mentorLinkId;
					let index = this.selectedStudents.findIndex(x => x == String(mentorLinkId));
					if (index > -1) {
						this.selectedStudents.splice(index, 1);
					}
				})
			}
		}
	}

	getChecked(mentorLinkId) {
		let index = this.selectedStudents.findIndex(x => x == mentorLinkId);
		if (index > -1) {
			return true;
		}

		return false;
	}

	startActionSelection(selectionValue: SelectionType) {

		let students = this.selectedStudents;

		if (selectionValue == SelectionType.ADD_CALENDAR_DATE) {
			let data: any = {
				mentorLinkIds: students
			}

			this._dialog.open(MentorDateDialogComponent,
				{
					data: data,
					maxWidth: '450px',
					autoFocus: false
				});
		}

		if (selectionValue == SelectionType.ADD_TASK) {
			let data: any = {
				mentorLinkIds: students
			}

			this._dialog.open(MentorTaskDialogComponent,
				{
					data: data,
					maxWidth: '450px',
					autoFocus: false
				});
		}

		if (selectionValue == SelectionType.SEND_DM) {
			let data: any = {
				isStudent: false,
				mentorLinkIds: students
			}

			this._dialog.open(DirectMessageDialogComponent, {
				data: data,
				maxWidth: '450px',
				autoFocus: false
			});
		}

		if (selectionValue == SelectionType.CREATE_GROUP) {
			let data: any = {
				mentorLinkIds: students,
				groups: this.studentGroups
			}

			this._dialog.open(MentorGroupDialogComponent, {
				data: data,
				maxWidth: '450px',
				autoFocus: false
			});
		}

		if (selectionValue == SelectionType.REMOVE_STUDENT) {

			let dialogData = {
				title: 'Confirm Student Removal',
				message: 'Remove mentor relationship with selected student(s), Are you sure?'
			};

			this._dialog.open(ConfirmDialogComponent, {
				data: dialogData,
				maxWidth: '400px'
			}).beforeClosed().subscribe(response => {
				if (response) {
					if (response === 'ok') {
						this.removeStudents(students);
					}
				}
			});
		}

		if (selectionValue == SelectionType.REMOVE_STUDENT_GROUP) {
			let dialogData = {
				title: 'Confirm Group Removal',
				message: 'Remove selected student(s) from group, Are you sure?'
			};

			this._dialog.open(ConfirmDialogComponent, {
				data: dialogData,
				maxWidth: '400px'
			}).beforeClosed().subscribe(response => {
				if (response) {
					if (response === 'ok') {
						this.removeStudentsFromGroup(students);
					}
				}
			});
		}

		if (selectionValue == SelectionType.RUN_REPORT) {

			let data: any = {
				students: students
			}

			this._dialog.open(MentorStudentReportDialogComponent, { data: data, autoFocus: false })
				.beforeClosed().subscribe(response => {
					if (response) {
						if (response != 'cancel') {
						}
					}
				})
		}

		return;
	}

	async removeStudents(students: string[]) {

		await Promise.all(students.map(async (mentorLinkId: string) => {

			let removal = await this._mentorService.removeStudent(mentorLinkId);

			return await removal;

		})).then((done: any) => {
			this._mentorService.sendUpdate(true);
		}).catch((error: any) => {
			console.error("ERROR", error);
		});

	}

	async removeStudentsFromGroup(students: string[]) {

		//group students by group id
		//if selected student is not apart of a group just ignore
		if (students && students.length > 0) {

			let studentGroups: any = [];

			//groupId
			//students: []
			students.forEach((mentorLinkId: string) => {
				let studentDetails = this.studentList.find(x => (x.mentorLinkId == mentorLinkId));
				if (studentDetails && studentDetails.group) {
					if (studentGroups && studentGroups.length > 0) {
						let group = studentGroups.find(x => x.groupId == studentDetails.group.groupId);
						if (group) {
							group.students.push(studentDetails.mentorLinkId)
						} else {
							group.push({ groupId: studentDetails.group.groupId, students: [studentDetails.mentorLinkId] })
						}
					} else {
						studentGroups.push({ groupId: studentDetails.group.groupId, students: [studentDetails.mentorLinkId] })
					}
				}
			})

			if (studentGroups && studentGroups.length > 0) {

				let updateStudentGroups = await Promise.all(studentGroups.map(async (groupWithStudents: any) => {
					let formData = groupWithStudents.students;
					let removal = await this._mentorService.removeStudentsFromGroup(groupWithStudents.groupId, formData);

					return await removal;

				})).then((done: any) => {
					//updates the dashboard with new student data
					this._mentorService.sendUpdate(true);
				}).catch((error: any) => {
					console.error("ERROR", error);
				});

			}
		}

	}

	showAvailableFiltersDialog() {
		const dialogRef = this._dialog.open(MentorDashboardFiltersDialogComponent, {
			disableClose: true,
			autoFocus: false,
			maxWidth: '600px',
			data: { students: this.studentList, groups: this.studentGroups, selectedFilters: this.selectedFilters }
		}).beforeClosed().subscribe((selectedFilters: any) => {

			if (selectedFilters) {

				this.selectedFilters = selectedFilters;

			}
		});
	}

	clearSelectedFilters() {
		this.selectedFilters = null;
	}

	removeSelectedFilter(filter: string) {

		switch (filter) {
			case 'genderFilter': {
				this.selectedFilters.genderFilter = null;
				break;
			}
			case 'studentName': {
				this.selectedFilters.studentName = null;
				break;
			}
			case 'asvabAdminMonth': {
				this.selectedFilters.asvabAdminMonth = null;
				break;
			}
			case 'asvabAdminYear': {
				this.selectedFilters.asvabAdminYear = null;
				break;
			}
			default: {
				this.selectedFilters.studentName = null;
				break;
			}
		}

		if (this.wasLastSelectedFilter()) this.clearSelectedFilters();

	}

	removeGraduationDateFilter(gradDate) {

		if (this.selectedFilters.graduationDates.length == 1) {
			this.selectedFilters.graduationDates = null;
			if (this.wasLastSelectedFilter()) this.clearSelectedFilters();
		} else {
			let filterIndex = this.selectedFilters.graduationDates.findIndex(x => x == gradDate);
			if (filterIndex > -1) { this.selectedFilters.graduationDates.splice(filterIndex, 1) };
		}
	}

	removeEducationLevelFilter(educationLevel) {
		if (this.selectedFilters.educationLevels.length == 1) {
			this.selectedFilters.educationLevels = null;
			if (this.wasLastSelectedFilter()) this.clearSelectedFilters();
		} else {
			let filterIndex = this.selectedFilters.educationLevels.findIndex(x => x == educationLevel);
			if (filterIndex > -1) { this.selectedFilters.educationLevels.splice(filterIndex, 1) };
		}
	}

	removeGroupFilter(group) {
		if (this.selectedFilters.groups.length == 1) {
			this.selectedFilters.groups = null;
			if (this.wasLastSelectedFilter()) this.clearSelectedFilters();
		} else {
			let filterIndex = this.selectedFilters.groups.findIndex(x => x == group);
			if (filterIndex > -1) { this.selectedFilters.groups.splice(filterIndex, 1) };
		}
	}

	wasLastSelectedFilter() {

		if (this.selectedFilters) {
			if (this.selectedFilters.groups && this.selectedFilters.groups.length > 0) return false;
			if (this.selectedFilters.graduationDates && this.selectedFilters.graduationDates.length > 0) return false;
			if (this.selectedFilters.educationLevels && this.selectedFilters.educationLevels.length > 0) return false;

			if (this.selectedFilters.genderFilter && this.selectedFilters.genderFilter != null) return false;
			if (this.selectedFilters.studentName && this.selectedFilters.studentName != null) return false;
			if (this.selectedFilters.asvabAdminMonth && this.selectedFilters.asvabAdminMonth != null) return false;
			if (this.selectedFilters.asvabAdminYear && this.selectedFilters.asvabAdminYear != null) return false;




			// 	groups: this.selectedGroups, //get groups
			// 	graduationDates: this.selectedGraduationDates,  //get grad dates
			// 	educationLevels: this.selectedEducationLevels,

			// 	genderFilter: this.selectedFilters.genderFilter,
			// 	studentName: this.selectedFilters.studentName,
			// 	asvabAdminMonth: this.selectedFilters.asvabAdminMonth,
			// 	asvabAdminYear: this.selectedFilters.asvabAdminYear
		}

		return true;
	}

	showLinkedSchools() {

		let dataOptions = {
			selectedSchools: this.selectedSchools
		}

		this._dialog.open(LinkedSchoolsDialogComponent, {
			data: dataOptions
		}).beforeClosed().subscribe((response: any) => {
			if (response) {
				let selectedSchools: SelectedSchool[] = response;

				this.selectedSchools.push(...selectedSchools);
			}
		});

	}

	bringToSchool() {
		const dialogRef2 = this._dialog.open(BringAsvabToYourSchoolComponent, {
			hasBackdrop: true,
			panelClass: 'new-modal',
			// maxHeight: '90vh',
			autoFocus: false,
			disableClose: true
		});
	}
}
