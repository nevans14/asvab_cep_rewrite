import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogComponent } from 'app/core/dialogs/confirm-dialog/confirm-dialog.component';
import { EditAccountDialogComponent } from 'app/core/dialogs/edit-account-dialog/edit-account-dialog.component';
import { LinkedSchoolsDialogComponent } from 'app/core/dialogs/linked-schools-dialog/linked-schools-dialog.component';
import { UserEmailSettings } from 'app/core/models/emailSettings';
import { SelectedSchool } from 'app/core/models/selectedSchool';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-mentor-setting',
  templateUrl: './mentor-setting.component.html',
  styleUrls: ['./mentor-setting.component.scss']
})
export class MentorSettingComponent implements OnInit {

  public user: any;
  public isMentorSettingsDisabled: boolean;
  public isSaving: boolean;
  public wouldLikeToReceiveNotifications: any;
  public wouldLikeToReceiveMarketingComm: any;
  public sendDesignatedStudentSummaries: any;
  public sendDesignatedStudentSummariesOption: any;
  public wouldLikeToReceiveMonthlyNewsletter: any;
  public emailSettings: UserEmailSettings;
  public selectedSchools: SelectedSchool[];
  public isDirty: boolean;

  constructor(private _matDialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService) {
    this.user = {};
    this.isMentorSettingsDisabled = true;
    this.isSaving = false;
    this.isDirty = false;
    this.selectedSchools = [];
  }

  ngOnInit() {

    this.user = this._activatedRoute.snapshot.data.mentorSettingResolver[0];
    this.emailSettings = this._activatedRoute.snapshot.data.mentorSettingResolver[1];
    this.selectedSchools = this._activatedRoute.snapshot.data.mentorSettingResolver[2];

    if (this.emailSettings) {
      this.wouldLikeToReceiveNotifications = this.emailSettings.wouldLikeToReceiveNotifications;
      this.wouldLikeToReceiveMarketingComm = this.emailSettings.wouldLikeToReceiveMarketingComm;
      this.sendDesignatedStudentSummaries = this.emailSettings.sendDesignatedStudentSummaries;
      this.wouldLikeToReceiveMonthlyNewsletter = this.emailSettings.wouldLikeToReceiveMonthlyNewsletter;
      this.sendDesignatedStudentSummariesOption = null;
      if (this.emailSettings.sendDesignatedStudentSummariesMonthly) {
        this.sendDesignatedStudentSummariesOption = "monthly";
      }

      if (this.emailSettings.sendDesignatedStudentSummariesWeekly) {
        this.sendDesignatedStudentSummariesOption = "weekly";
      }

    }

  }

  editAccountModal() {
    let dialogRef = this._matDialog.open(EditAccountDialogComponent, {
      data: this.user.username,
      maxWidth: '340px',
    });

    dialogRef.afterClosed().subscribe(res => {

      if(res){
        this.user.username = res.username;
      }
    })
  }

  updateSettings() {
    this.isSaving = true;
    let sendDesignatedStudentSummariesWeekly: boolean = null;
    let sendDesignatedStudentSummariesMonthly: boolean = null;

    if (this.sendDesignatedStudentSummaries) {
      if (this.sendDesignatedStudentSummariesOption && this.sendDesignatedStudentSummariesOption == "weekly") {
        sendDesignatedStudentSummariesWeekly = true;
      }
      if (this.sendDesignatedStudentSummariesOption && this.sendDesignatedStudentSummariesOption == "monthly") {
        sendDesignatedStudentSummariesMonthly = true;
      }
    }

    let formData: UserEmailSettings = {
      sendDesignatedStudentSummaries: this.sendDesignatedStudentSummaries,
      sendDesignatedStudentSummariesWeekly: sendDesignatedStudentSummariesWeekly,
      sendDesignatedStudentSummariesMonthly: sendDesignatedStudentSummariesMonthly,
      wouldLikeToReceiveNotifications: this.wouldLikeToReceiveNotifications,
      wouldLikeToReceiveMarketingComm: this.wouldLikeToReceiveMarketingComm,
      wouldLikeToReceiveMonthlyNewsletter: this.wouldLikeToReceiveMonthlyNewsletter
    }

    this._userService.updateEmailSettings(formData).then((results: any) => {

      //show success message?
      this.isSaving = false;
      this.isDirty = false;

    }).catch((error: any) => {
      //show error message?
      this.isSaving = false;
      console.error("ERROR", error);
    })

  }

  removeSelectedSchool(school: SelectedSchool) {

    let dialogData = {
      title: 'Confirm School Removal',
      message: 'Remove ' + school.schoolName + ', are you sure?'
    };

    this._matDialog.open(ConfirmDialogComponent, {
      data: dialogData,
      maxWidth: '300px'
    }).beforeClosed().subscribe(response => {
      if (response) {
        if (response === 'ok') {

          this._userService.deleteSelectedSchool(school.schoolCode).then((rowsAffected: number) => {

            if (rowsAffected > 0) {
              let selectedSchoolIndex = this.selectedSchools.findIndex(x => x.schoolCode == school.schoolCode);
              if (selectedSchoolIndex > -1) {
                this.selectedSchools.splice(selectedSchoolIndex, 1);
              }

            }
          })
        }
      }
    });
  }

  showLinkedSchools() {

    let dataOptions = {
      selectedSchools : this.selectedSchools
    }

    this._matDialog.open(LinkedSchoolsDialogComponent, {
      data: dataOptions
    }).beforeClosed().subscribe((response:any) => 
    {
			if (response) {
        let selectedSchools: SelectedSchool[] = response;

        this.selectedSchools.push(...selectedSchools);
			}
		});
    
  }

}
