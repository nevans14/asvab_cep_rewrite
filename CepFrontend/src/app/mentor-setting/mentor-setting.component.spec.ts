import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorSettingComponent } from './mentor-setting.component';

describe('MentorSettingComponent', () => {
  let component: MentorSettingComponent;
  let fixture: ComponentFixture<MentorSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
