import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';

import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';

@Component({
  selector: 'app-employment-details',
  templateUrl: './employment-details.component.html',
  styleUrls: ['./employment-details.component.scss']
})
export class EmploymentDetailsComponent implements OnInit {

  stateSalaryYear;
  occupationTitle;
  employementMoreDetails;
  favorites;
  currentUser;
  occuFindUserLoggedIn;
  socId;

  constructor(
    private _user: UserService,
    private route: ActivatedRoute,
    private _utility: UtilityService,
    private _dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.stateSalaryYear = this.route.snapshot.data.employmentDetails[0];
    this.occupationTitle = this.route.snapshot.data.employmentDetails[1].title;
    this.employementMoreDetails = this.route.snapshot.data.employmentDetails[2];
    this.socId = this.route.snapshot.paramMap.get( 'socId' );
    this.favorites = undefined;

    if (this._user.isLoggedIn()) {
      this.occuFindUserLoggedIn = true;
      this.favorites = this.route.snapshot.data.employmentDetails[3];
    } else {
      this.occuFindUserLoggedIn = false;
    }
  }

  showOccufindNotes() {
    this._dialog.open(NotesDialogComponent, {
      data: {
        title: this.occupationTitle,
        socId: this.socId,
      },
      maxWidth: '900px',
    });
  }
}
