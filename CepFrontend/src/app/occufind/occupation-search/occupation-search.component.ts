import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Title, Meta } from '@angular/platform-browser';

import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { MediaCenterService } from 'app/services/media-center.service';
import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';

import $ from 'jquery'
declare var $: $

enum workValueType{
	Achievement = 1,
	Independence,
	Recognition,
	Relationships,
	Support,
	WorkingConditions
}
@Component({
  selector: 'app-occupation-search',
  templateUrl: './occupation-search.component.html',
  styleUrls: ['./occupation-search.component.scss']
})
export class OccupationSearchComponent implements OnInit {

  mediaCenterList;

  stemOccupationFlag;
  brightOccupationFlag;
  greenOccupationFlag;
  hotOccupationFlag;

  currentUser;
  occuFindUserLoggedIn;

  seoTitle = 'Career Search | OCCU-Find | ASVAB Career Exploration Program';
	metaDescription = 'Conduct a career search through the OCCU-Find. The OCCU-Find catalogs 1000+ career options that are available to you to search!';

  // INTEREST 
  keyword = '';
  checkboxInterest = [{
    name: 'Realistic',
    code: 'R'
  }, {
    name: 'Investigative',
    code: 'I'
  }, {
    name: 'Artistic',
    code: 'A'
  }, {
    name: 'Social',
    code: 'S'
  }, {
    name: 'Enterprising',
    code: 'E'
  }, {
    name: 'Conventional',
    code: 'C'
  }];

  workValuesExpanded: boolean;
	categoryExpanded: boolean;

  filterItems = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };
  interestLimit = 2;
  interestOneChecked = 0;
  interestTwoChecked = 0;
  interestThreeChecked = 0;
  selectedInterestCodeOne;
  selectedInterestCodeTwo;
  selectedInterestCodeThree;

  filterItemsTwo = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };

  filterItemsThree = {
    'A': false,
    'C': false,
    'E': false,
    'I': false,
    'R': false,
    'S': false
  };

  countInterestOneChecked(item) {
    if (this.filterItems[item.code]) {
      this.selectedInterestCodeOne = item.code;
      this.interestOneChecked++;
    }
    else {
      this.selectedInterestCodeOne = undefined;
      this.interestOneChecked--;
    }
  }

  countInterestTwoChecked(item) {
    if (this.filterItemsTwo[item.code]) {
      this.selectedInterestCodeTwo = item.code;
      this.interestTwoChecked++;
    }
    else {
      this.selectedInterestCodeTwo = undefined;
      this.interestTwoChecked--;
    }
  }

  countInterestThreeChecked(item) {
    if (this.filterItemsThree[item.code]) {
      this.selectedInterestCodeThree = item.code;
      this.interestThreeChecked++;
    }
    else {
      this.selectedInterestCodeThree = undefined;
      this.interestThreeChecked--;
    }
  }

  // SKILL
  skillImportance = '';
  skillView = 'hi'; // by default, high importance will be shown
  currentPage = 1; // by default, pagination will be on the first page
  skillLimit = 1;
  skillChecked = 0;
  itemsPerPage = 20;
  tab = { index: 0 };

  checkboxSkill = [{
    name: 'Verbal',
    code: 'V'
  }, {
    name: 'Math',
    code: 'M'
  }, {
    name: 'Science/Technical',
    code: 'S'
  }];

  skillItems = {
    'V': false,
    'M': false,
    'S': false
  };

  limitOne(item) {
    if (this.skillItems[item.code])
      this.skillChecked++;
    else
      this.skillChecked--;
  };

  onSkillChange(item) {
    this.skillImportance = (this.skillItems[item.code] ? item.code : '');
    // if (this.skillImportance == '') {
    //   this.resetTabs();
    // }
    this.saveSearch();
  };

  // CATEGORY
  checkboxCategory = [{
    name: 'Bright Outlook',
    code: 'B',
    iconClass: 'icon hot',
    rel: 'BRIGHT'
  },{
    name: 'STEM Careers',
    code: 'S',
    iconClass: 'icon stem',
    rel: 'STEM'
  }, {
    name: 'Hot Military Careers',
    code: 'H',
    iconClass: 'icon-hotjob',
    rel: 'HOT'
  }, {
    name: 'Available in the Military',
    code: 'M'
  }];

  categoryItems = {
    'B': false,
    'G': false,
    'S': false,
    'H': false,
    'M': false
  };

  // WORK VALUES
  checkboxWorkValues = [{
    name: 'Achievement',
    code: 'A',
    iconClass: 'iconWV achievement',
    rel: 'Achievement'
  }, {
    name: 'Independence',
    code: 'I',
    iconClass: 'iconWV independence',
    rel: 'Independence'
  }, {
    name: 'Recognition',
    code: 'R',
    iconClass: 'iconWV recognition',
    rel: 'Recognition'
  }, {
    name: 'Relationships',
    code: 'E',
    iconClass: 'iconWV relationships',
    rel: 'Relationships'
  }, {
    name: 'Support',
    iconClass: 'iconWV support',
    code: 'S',
    rel: 'Support'
  }, {
    name: 'Working Conditions',
    iconClass: 'iconWV workingConditions',
    code: 'W',
    rel: 'Working Conditions'
  }];

  workValueItems = {
    'A': false,
    'I': false,
    'R': false,
    'E': false,
    'S': false,
    'W': false
  };

  constructor(
    private mediaCenterService: MediaCenterService,
    private _user: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private _utility: UtilityService,
    private dialog: MatDialog,
    private _meta: Meta,
    private _titleTag: Title,
  ) {
    // Initialize occufind search params from session
    const currentSearchSelections = JSON.parse(window.sessionStorage.getItem('currentSearchSelections'));
    this.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : this.skillImportance;
    this.skillView = currentSearchSelections ? currentSearchSelections.skillView : this.skillView;
    this.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : this.skillItems;
    this.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : this.filterItems;
    this.filterItemsTwo = currentSearchSelections ? currentSearchSelections.filterItemsTwo : this.filterItemsTwo;
    this.filterItemsThree = currentSearchSelections ? currentSearchSelections.filterItemsThree : this.filterItemsThree;
    this.selectedInterestCodeOne = currentSearchSelections ? currentSearchSelections.selectedInterestCodeOne : this.selectedInterestCodeOne;
    this.selectedInterestCodeTwo = currentSearchSelections ? currentSearchSelections.selectedInterestCodeTwo : this.selectedInterestCodeTwo;
    this.selectedInterestCodeThree = currentSearchSelections ? currentSearchSelections.selectedInterestCodeThree : this.selectedInterestCodeThree;
    this.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : this.categoryItems;
    this.workValueItems = currentSearchSelections ? currentSearchSelections.workValueItems : this.workValueItems;
    this.keyword = currentSearchSelections ? currentSearchSelections.keyword : this.keyword;
    // initialize how many items have been checked
    this.interestOneChecked = this.filter(this.filterItems, this.checkboxInterest);
    this.interestTwoChecked = this.filter(this.filterItemsTwo, this.checkboxInterest);
    this.interestThreeChecked = this.filter(this.filterItemsThree, this.checkboxInterest);
    this.skillChecked = this.filter(this.skillItems, this.checkboxSkill);


  }

  ngOnInit() {
    this.workValuesExpanded = false;
		this.categoryExpanded = false; 
    this._utility.scrollToTop();

    this.currentUser = this._user.getUser();
    if (typeof this.currentUser === 'undefined') {
      this.occuFindUserLoggedIn = false;
    } else {
      this.occuFindUserLoggedIn = true;
    }

    this._titleTag.setTitle(this.seoTitle);
    this._meta.updateTag({name: 'description', content: this.metaDescription});

    this._meta.updateTag({property: 'og:title', content: this.seoTitle});
    this._meta.updateTag({property: 'og:description', content: this.metaDescription});

    this._meta.updateTag({ name: 'twitter:title', content:  this.seoTitle});
    this._meta.updateTag({ name: 'twitter:description', content: this.metaDescription});
  }

  filter(items, checkbox) {
    var size = 0;
    for (var i = 0; i < checkbox.length; i++) {
      if (items[checkbox[i].code]) {
        size++;
      }
    }
    return size;
  }

  saveSearch() {
    var currentSearchSelections = {
      filterItems: this.filterItems,
      filterItemsTwo: this.filterItemsTwo,
      filterItemsThree: this.filterItemsThree,
      selectedInterestCodeOne: this.selectedInterestCodeOne,
      selectedInterestCodeTwo: this.selectedInterestCodeTwo,
      selectedInterestCodeThree: this.selectedInterestCodeThree,
      categoryItems: this.categoryItems,
      workValueItems: this.workValueItems,
      skillItems: this.skillItems,
      keyword: this.keyword,
      currentPage: this.currentPage,
      skillImportance: this.skillImportance,
      skillView: this.skillView
    };
    window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
  };

  occufindSearch() {
    if (Object.values(this.categoryItems).some(i => i)
      || Object.values(this.workValueItems).some(i => i)
      || Object.values(this.filterItems).some(i => i)
      || Object.values(this.filterItemsTwo).some(i => i)
      || Object.values(this.filterItemsThree).some(i => i)
      || Object.values(this.skillItems).some(i => i)
      || this.keyword.length > 0
    ) {
      this._user.setCompletion(UserService.REFINED_OCCUFIND_SEARCH, 'true');
    }

    this.router.navigate(['/occufind-occupation-search-results/']);
  }

  showSortByStrength() {
    var data = {
      title: 'Career Exploration Scores',
      message: '<p>You can explore occupations that fit your ASVAB strengths. Choose the skill you want to investigate. Consider if it is one of your strengths.</p>' +
        '<p>You\'ll see the occupations organized in three levels of importance: Most Important, Moderately Important, and Less Important. This has to do with how often the skills are used on the job. Explore the importance level that relates to your relative strength in that skill.</p>' +
        '<p>(For example, is math your top ASVAB strength? Sort by math and explore occupations in the Most Important category. If verbal skills are your lowest strength, sort by verbal and explore the occupations in the less important category).</p>'
    };

    const dialogRef1 = this.dialog.open(MessageDialogComponent, {
      data: data
      //maxWidth: '400px'
    });
  }

  careerDefinitionsNoCareerCluster() {
    var data = {
      title: 'Categories',
      message: "<p><b>Available in the Military</b> indicates occupations that are offered in one of more of the Military Services, each with its own unique requirements.</p>" +
      "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
      "<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
      "<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
      "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" +
      "<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>",
    };

    const dialogRef1 = this.dialog.open(MessageDialogComponent, {
      data: data
      //maxWidth: '400px'
    });
  }
  // ngAfterViewInit() {
  //   $(document).on('click', '.select_with_checkbox .custom_select_dropdown', function (e) {
  //     e.preventDefault();
  //     $(this).siblings('table').toggleClass('active');
  //   });
  // }


  getFilterCount(type): number {
    let totalCount = 0;

    const currentSearchSelections = JSON.parse(window.sessionStorage.getItem('currentSearchSelections'));

    if (!currentSearchSelections) {
      return totalCount;
    }

    if (type == 'category') {
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.B == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.G == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.S == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.H == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.categoryItems) ? (currentSearchSelections.categoryItems.M == true) ? totalCount + 1 : totalCount : totalCount;
      return totalCount;
    }

    if (type == 'workValues') {
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.A == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.I == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.R == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.E == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.S == true) ? totalCount + 1 : totalCount : totalCount;
      totalCount = (currentSearchSelections.workValueItems) ? (currentSearchSelections.workValueItems.W == true) ? totalCount + 1 : totalCount : totalCount;
      return totalCount;
    }

    return totalCount;
  }

  
	getWorkValueHelp() {
		var data = {
			title: 'WORK VALUES',
			message: "<p>Consider your top work values when exploring occupations and creating your action plan for the future. You can take the work values assessment <a href='work-values-test' title='Work Values test'>here</a>.</p>"
		};

		const dialogRef1 = this.dialog.open(MessageDialogComponent, {
			data: data,
			maxWidth: '400px',
      autoFocus: false
		});
	}

	expandCategory(value) {
		this.categoryExpanded = value;

		//close the other drop down if its expanded
		if (this.workValuesExpanded) {
			this.workValuesExpanded = false;
		}
	}

	expandWorkValues(value) {
		this.workValuesExpanded = value;

		//close the other drop down if its expanded
		if (this.categoryExpanded) {
			this.categoryExpanded = false;
		}
	}

	@ViewChild('workValues') workValues: ElementRef;
	@ViewChild('categories') categories: ElementRef;

	@HostListener('document:mousedown', ['$event'])
	onGlobalClick(event): void {
		if (!this.workValues.nativeElement.contains(event.target)) {
			// clicked outside => close dropdown list
			if(this.workValuesExpanded){
				this.workValuesExpanded = false;
			}
		}

		if (!this.categories.nativeElement.contains(event.target)) {
			// clicked outside => close dropdown list
			if(this.categoryExpanded){
				this.categoryExpanded = false;
			}
		}
	}
}
