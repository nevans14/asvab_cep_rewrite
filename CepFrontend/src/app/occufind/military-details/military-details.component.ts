import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { ConfigService } from 'app/services/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';

@Component({
  selector: 'app-military-details',
  templateUrl: './military-details.component.html',
  styleUrls: ['./military-details.component.scss']
})
export class MilitaryDetailsComponent implements OnInit {
  ssoUrl;
  socId;
  occupationTitle;
  militaryMoreDetails;
  militaryResources;
  occuFindUserLoggedIn;
  ctmImageUrl;
  favorites;

  constructor(
    private _ga: GoogleAnalyticsService,
    private _config: ConfigService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _dialog: MatDialog,
  ) {
    this.ssoUrl = this._config.getSsoUrl();
    this.ctmImageUrl = this._config.getCtmImageUrl();
  }

  ngOnInit() {
    this.socId = this._activatedRoute.snapshot.params.socId;
	  this.occupationTitle = this._activatedRoute.snapshot.data.occupationTitleDescription.title;
    this.militaryMoreDetails = this._activatedRoute.snapshot.data.militaryMoreDetails;
	  this.militaryResources = this._activatedRoute.snapshot.data.militaryResources;
	
    if (this._userService.isLoggedIn()) {
      this.occuFindUserLoggedIn = true;
      this.favorites = this._activatedRoute.snapshot.data.favoritesOccupation;
    } else {
      this.occuFindUserLoggedIn = false;
    }
  }

  showOccufindNotes(title, socId) {
    this._dialog.open(NotesDialogComponent, {
      data: {
        title: this.occupationTitle,
        socId: this.socId,
      },
      maxWidth: '900px',
    });
  }

  setCompletionToCTM() {
    if ( this._userService.isLoggedIn() ) {
      this._userService.setCompletion(UserService.TO_CTM_FROM_OCCU_DETAIL, 'true');
    }
  }
}
