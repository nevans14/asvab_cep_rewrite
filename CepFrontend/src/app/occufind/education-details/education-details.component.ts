import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UtilityService } from 'app/services/utility.service';
import { UserService } from 'app/services/user.service';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
@Component({
  selector: 'app-education-details',
  templateUrl: './education-details.component.html',
  styleUrls: ['./education-details.component.scss']
})


export class EducationDetailsComponent implements OnInit {
  public socId;
  public unitId;
  public userId;
  public occuFindUserLoggedIn: boolean;
  public showMore: boolean;
  public filtered;
  public ipedsDisclaimer;
  public isFavorited: boolean;

  // resolver properties
  public occupationTitle;
  public schoolDetails;
  public favorites = undefined;

  public schoolMajors = [];

  // filter
  stateFilter: any;
  sort: any;

  // pagination
  public totalItems: number;
  public currentPage: number;
  public maxSize: number;
  public itemsPerPage: number;

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    // private _dialog: MatDialog,
    private _utilityService: UtilityService,
    private _userService: UserService,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {

    //set favorites and user id
    if (this._userService.getUser()) {
      this.userId = this._userService.getUser().userId;
    }

    if (this._userService.isLoggedIn()) {
      this.occuFindUserLoggedIn = true;
      this.favorites = this._activatedRoute.snapshot.data.favoritesOccupation;
    } else {
      this.occuFindUserLoggedIn = false;
    }

    // Initialize variables
    this.socId = this._activatedRoute.snapshot.params.socId;
    this.unitId = this._activatedRoute.snapshot.params.unitId;

    //init values passed down by route resolve
    this.occupationTitle = this._activatedRoute.snapshot.data.occupationTitleDescription.title;
    this.schoolDetails = this._activatedRoute.snapshot.data.occuSchoolDetails;
    this.schoolMajors = this._activatedRoute.snapshot.data.occuSchoolMajors;
    this.ipedsDisclaimer = this._activatedRoute.snapshot.data.dbInfoResolveService;

    // Pagination Initialization
    this.totalItems = this.schoolDetails.length;
    this.filtered = this.schoolDetails;
    this.currentPage = 1;
    this.maxSize = 4;
    this.itemsPerPage = 20;
    this.stateFilter = "";
    this.sort = "schoolName";
  }

  showNotes() {
    this._dialog.open(NotesDialogComponent, {
        data: {
            title: this.occupationTitle,
            socId: this.socId,
        },
        maxWidth: '900px',
    });
  }

  showMoreText() {
    if (!this.showMore) {
      return 'More';
    } else {
      return 'Less';
    }
  }

  hasMore() {
    if (!this.schoolMajors)
      return false;

    if (this.schoolMajors.length <= 3) {
      return false;
    } else {
      return true;
    }
  }

  onStateFilterSelect(event) {
    const value = event.target.value;
    this.stateFilter = value;

    this.filtered = this.applyStateFilter(this.schoolDetails, value);
    this.totalItems = this.filtered.length;
  }

  onSortChange(event) {
    const value = event.target.value;
    this.sort = value;

    this.filtered = this.applySort(this.filtered, value);
  }

  applyStateFilter(schoolDetails, value): any[] {
    if ((!value) || (value === '')) {
      return schoolDetails;
    } else {
      return schoolDetails.filter(school => school.state === value);
    }
  }

  applySort(schoolDetails, value): any[] {

    if (value.charAt(0) === '-') {
      schoolDetails.sort(this.compareValues(value.slice(1), 'desc'));
    } else {
      schoolDetails.sort(this.compareValues(value));
    }

    return schoolDetails;
  }

  showOccufindNotes() {

    //TODO update logic
    //need to show notes modal
  }

  /**
   * returns a sorted array based on the key and order passed
   * @param key 
   * @param order can be 'asc' or 'desc'; 'asc' is by default
   */
  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) return 0;  //property doesn't exist

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if ((typeof varA === 'string') && (typeof varB === 'string')) {
        comparison = varA.localeCompare(varB);
      } else {
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
      }

      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

}
