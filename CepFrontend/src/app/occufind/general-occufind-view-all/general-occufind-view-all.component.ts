import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { ConfigService } from 'app/services/config.service';
@Component({
  selector: 'app-general-occufind-view-all',
  templateUrl: './general-occufind-view-all.component.html',
  styleUrls: ['./general-occufind-view-all.component.scss']
})
export class GeneralOccufindViewAllComponent implements OnInit {


  occupationPagination = {};
  occupationList = [];  

  constructor(
    private _dialog: MatDialog,
    private _http: HttpClient,
    private _config: ConfigService,
  ) { }

  ngOnInit() {
    this.occupationPagination['TotalItems'] = 0;
    this.occupationPagination['CurrentPage'] = 1;
    this.occupationPagination['MaxSize'] = 4;
    this.occupationPagination['ItemsPerPage'] = 10;

    this._http.get(this._config.getAwsFullUrl('/api/occufind/ViewAllOccupations/')).subscribe((result: any) => {
      this.occupationList = result;
      this.occupationPagination['TotalItems'] = this.occupationList.length;
    })
  }

  showLoginModal() {
    this._dialog.open(LoginDialogComponent, {
    });
  }
}
