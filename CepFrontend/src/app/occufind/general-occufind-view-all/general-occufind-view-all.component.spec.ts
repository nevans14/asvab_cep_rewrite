import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralOccufindViewAllComponent } from './general-occufind-view-all.component';

describe('GeneralOccufindViewAllComponent', () => {
  let component: GeneralOccufindViewAllComponent;
  let fixture: ComponentFixture<GeneralOccufindViewAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralOccufindViewAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralOccufindViewAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
