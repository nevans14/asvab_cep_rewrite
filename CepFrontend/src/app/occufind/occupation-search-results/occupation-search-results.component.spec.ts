import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationSearchResultsComponent } from './occupation-search-results.component';

describe('OccupationSearchResultsComponent', () => {
  let component: OccupationSearchResultsComponent;
  let fixture: ComponentFixture<OccupationSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
