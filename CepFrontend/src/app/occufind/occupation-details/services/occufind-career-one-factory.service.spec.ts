import { TestBed } from '@angular/core/testing';

import { OccufindCareerOneFactoryService } from './occufind-career-one-factory.service';

describe('OccufindCareerOneFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccufindCareerOneFactoryService = TestBed.get(OccufindCareerOneFactoryService);
    expect(service).toBeTruthy();
  });
});
