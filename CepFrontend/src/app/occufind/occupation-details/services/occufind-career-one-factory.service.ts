import { Injectable } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable( {
    providedIn: 'root'
} )
export class OccufindCareerOneFactoryService {

    restURL: any;

    constructor( private _http: HttpClient,
        private _config: ConfigService,
        private _user: UserService,
        private _httpHelper: HttpHelperService ) {
        this.restURL = _config.getRestUrl() + '/';
    }

    getOccupationCertificates = function( onetSoc ) {
        var onetOccupationCode = onetSoc.replace( /-/g, '' ).replace( /\./g, '' ).trim();
        return this._http.get( this.restURL + 'CareerOneStop/cert/' + onetOccupationCode );

        // Commented out since getting timedout. Jason suggest it is not AWS but Career One Stop.
        // return this._http.get(this._config.getAwsFullUrl(`/api/CareerOneStop/cert/${onetOccupationCode}`));
    }

    getOccupationCertificateDetails = function( certId ) {
        return this._http.get(this._config.getAwsFullUrl(`/api/CareerOneStop/certDetail/${certId.trim()}`));
    }

    getOccupationLicenses = function( onetSoc, state ) {
        var onetOccupationCode = onetSoc.replace( /-/g, '' ).replace( /\./g, '' ).trim();
        return this._http.get(this._config.getAwsFullUrl(`/api/CareerOneStop/license/${onetOccupationCode}/${state.trim()}/`));
    }

    getOccupationLicensesDetail = function( onetSoc, licenseId, state ) {
        var onetOccupationCode = onetSoc.replace( /-/g, '' ).replace( /\./g, '' ).trim();
        return this._http.get(this._config.getAwsFullUrl(`/api/CareerOneStop/licenseDetail/${onetOccupationCode}/${licenseId.trim()}/${state.trim()}`));
    }

    getOccupationApprenticeship = function( onetSoc, state ) {
        var onetOccupationCode = onetSoc.replace( /-/g, '' ).replace( /\./g, '' ).trim();
        return this._http.get( this.restURL + 'CareerOneStop/apprenticeship/' + onetOccupationCode + '/' + state.trim() );
    }

    getOccupationSocCrosswalk(socId){
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/2019SocCrosswalk/${socId}/`)).toPromise();
    }

}
