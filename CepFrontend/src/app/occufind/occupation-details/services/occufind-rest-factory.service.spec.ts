import { TestBed } from '@angular/core/testing';

import { OccufindRestFactoryService } from './occufind-rest-factory.service';

describe('OccufindRestFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OccufindRestFactoryService = TestBed.get(OccufindRestFactoryService);
    expect(service).toBeTruthy();
  });
});
