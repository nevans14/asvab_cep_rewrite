import { Injectable } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpHelperService } from 'app/services/http-helper.service';

@Injectable({
    providedIn: 'root'
})
export class OccufindRestFactoryService {

    restURL: any;

    constructor(
        private _http: HttpClient,
        private _config: ConfigService,
        private _user: UserService,
        private _httpHelper: HttpHelperService) {
        this.restURL = _config.getRestUrl() + '/';
    }

    getOccupationResults = function (occufindSearchJSON) {
        const fullUrl = this._config.getAwsFullUrl(`/api/occufind/getOccufindSearch/`);
        return this._httpHelper.httpHelper('get', fullUrl, null, occufindSearchJSON, true);
    }

    getOccupationTitleDescription = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/occupationTitleDescription/${onetSoc}/`));
    }

    getOccupationTitleDescriptionNF(onetSoc) {
        const endPoint = this._config.getAwsFullUrl(`/api/occufind/occupationTitleDescription/${onetSoc}/`);

        return new Promise((resolve, reject) => {
          this._httpHelper.httpHelper('get', endPoint, null, null, true, 'aws')
            .then((obj) => {
              resolve(obj);
            }).catch(error => {
              reject(error);
            });
        });

    }

    getOccupationInterestCodes = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/OccupationInterestCodes/${onetSoc}/`));
    }

    getHotGreenStemFlags = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/HotGreenStemFlags/${onetSoc}/`));
    }

    getServiceOfferingCareer = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/ServiceOfferingCareer/${onetSoc}/`));
    }

    getRelatedCareerCluster = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/RelatedCareerCluster/${onetSoc}/`));
    }

    getSchoolMoreDetails = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/SchoolMoreDetails/${onetSoc}/`));
    }

    getSchoolMajors = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/SchoolMajors/${onetSoc}/`));
    }

    getSchoolMajorsForCP(onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/SchoolMajors/${onetSoc}/`)).toPromise();
    }

    getStateSalaryYear = function () {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/StateSalaryYear/`));
    }

    getEmploymentMoreDetails = function (trimmedOnetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/EmploymentMoreDetails/${trimmedOnetSoc}/`));
    }

    getMilitaryMoreDetails = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/MilitaryMoreDetails/${onetSoc}/`));
    }

    getMilitaryHotJobs = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/MilitaryHotJobs/${onetSoc}/`));
    }

    getNationalSalary = function (trimmedOnetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/NationalSalary/${trimmedOnetSoc}/`));
    }

    getNationalEntrySalary = function (trimmedOnetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/NationalEntrySalary/${trimmedOnetSoc}/`));
    }

    getBLSTitle = function (trimmedOnetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/BLSTitle/${trimmedOnetSoc}/`));
    }

    getMoreResources = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/OccufindMoreResources/${onetSoc}/`));
    }

    getAlternateView = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/OccufindAlternativeView/${onetSoc}/`));
    }

    getImageAltText = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/ImageAltText/${onetSoc}/`));
    }

    getJobZone = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/job-zone/${onetSoc}/`));
    }

    getCertificateExplanation = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/certificateExplanation/${onetSoc}/`));
    }

    getAlternateTitles = function (onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/alternateTitles/${onetSoc}/`));
    }

    getSchoolProfile = function (unitId) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/SchoolProfile/${unitId}/`));
    }

    getOccupationWorkValues = function(onetSoc) {
        return this._http.get(this._config.getAwsFullUrl(`/api/occufind/work-values/${onetSoc}/`));
    }
}
