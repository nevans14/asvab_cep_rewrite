import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Title, Meta } from '@angular/platform-browser';
import { UtilityService} from 'app/services/utility.service';
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { MessageDialogComponent } from '../../core/dialogs/message-dialog/message-dialog.component';
import { ApprenticeshipListComponent } from '../../core/dialogs/career-one-stop-dialogs/apprenticeship-list/apprenticeship-list.component';
import { CertificationListComponent } from '../../core/dialogs/career-one-stop-dialogs/certification-list/certification-list.component';
import { LicenseListComponent } from '../../core/dialogs/career-one-stop-dialogs/license-list/license-list.component';
import { LoginDialogComponent } from '../../core/dialogs/login-dialog/login-dialog.component';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';

import $ from 'jquery'
declare var $: $

declare let raterater: any;
@Component( {
    selector: 'app-occupation-details',
    templateUrl: './occupation-details.component.html',
    styleUrls: ['./occupation-details.component.scss']
} )
export class OccupationDetailsComponent implements OnInit, OnDestroy {


    public socId;
    public imageUrl;
    public absUrl;
    public domainUrl = 'https://asvabprogram.com'
    public userId;
    public occupationLimit: number = 5;
    public limitFlag: boolean = true;
    public occupationGreen;
    public occupationStem;
    public occupationBright;
    public occupationInterestOne;
    public occupationInterestTwo;
    public occupationInterestThree;
    public occuFindUserLoggedIn;
    public chart; //salary map chart
    public entryLevelSalaryChart; // entry level salary map chart
    public chartMobile //mobile salary map chart
    public entryLevelSalaryChartMobile; //mobile entry level salary map chart
    public barList = []; // bar chart;
    public shrinkVal: number = 5;
    public shrinkCredentialsValLimit: number = 160;
    public shrinkCredentialsVal: number;

    //resolver properties
    public occupationCertificates;
    public occupationTitle;
    public occupationDescription;
    public stateSalary;
    public stateEntrySalary;
    public nationalSalary;
    public nationalEntrySalary;
    public blsTitle;
    public skillImportance;
    public tasks;
    public occupationInterestCodes;
    public educationLevel;
    public relatedOccupations;
    public hotGreenStemFlags;
    public servicesOffering;
    public relatedCareerCluster;
    public moreResources;
    public schoolDetails;
    public alternateView;
    public imageAltText;
    public jobZone;
    public alternateTitles;
    public certificateExplanation;
    public favorites = undefined;
    public navigationSubscription;
    public stateSalaryYear;
    public userCompletion;
    public workValues: any[];

    public workValueDisplayObjArr = [{
		name: 'Achievement',
		code: 'A',
		iconClass: 'iconWV large-icon achievement',
		rel: 'Achievement'
	  }, {
		name: 'Independence',
		code: 'I',
		iconClass: 'iconWV large-icon independence',
		rel: 'Independence'
	  }, {
		name: 'Recognition',
		code: 'R',
		iconClass: 'iconWV large-icon recognition',
		rel: 'Recognition'
	  }, {
		name: 'Relationships',
		code: 'E',
		iconClass: 'iconWV large-icon relationships',
		rel: 'Relationships'
	  }, {
		name: 'Support',
		iconClass: 'iconWV large-icon support',
		code: 'S',
		rel: 'Support'
	  }, {
		name: 'Working Conditions',
		iconClass: 'iconWV large-icon workingConditions',
		code: 'W',
		rel: 'Working Conditions'
	  }];

    // Guided Tour
    public guidedTipConfig;
    public showRelatedCareerCCTip;
    public showCompareJobAttributesTip;
    public visitedHowToGetThereDetailLink;
    public visitedHowToGetThereDetailLinkClosedHint;
    public visitedMilitaryInfo;
    public visitedMilitaryInfoClosedHint;

    constructor( private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dialog: MatDialog,
        private _utilityService: UtilityService,
        private _userService: UserService,
        private _config: ConfigService,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _meta: Meta,
        private _titleTag: Title,
    ) {
        this.shrinkCredentialsVal = this.shrinkCredentialsValLimit;

        this.navigationSubscription = this._router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
              this.ngOnInit();
            }
          });

        this.showRelatedCareerCCTip = false;
        this.showCompareJobAttributesTip = false;
        this.visitedHowToGetThereDetailLink = false;
        this.visitedHowToGetThereDetailLinkClosedHint = false;
        this.visitedMilitaryInfo = false;
        this.visitedMilitaryInfoClosedHint = false;
    }

    ngOnInit() {
        this.socId = this._activatedRoute.snapshot.params.socId;
        let baseImageURL = this._config.getImageUrl();
        this.imageUrl = baseImageURL + 'OCCUFIND_IMGS/' + this.socId + '.jpg';
        this.absUrl = this._utilityService.getCanonicalUrl();
        const fullUrl = location.href;

        if (this._userService.isLoggedIn()) {
            this._userService.getCompletion().subscribe(data => {
                // if (!this.userCompletion) {
                    this.userCompletion = data;
                    if (!this._userService.checkUserCompletion(data, UserService.VIEWED_CAREER_DETAIL)) {
                        this._userService.setCompletion(UserService.VIEWED_CAREER_DETAIL, 'true');
                    }

                    if (this._userService.checkUserCompletion(this.userCompletion, UserService.VISITED_OCCUPATION_DETAIL_LINK)) {
                        this.visitedHowToGetThereDetailLink = true;
                    }

                    if (this._userService.checkUserCompletion(this.userCompletion, UserService.VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT)) {
                        this.visitedHowToGetThereDetailLinkClosedHint = true;
                    }

                    if (this._userService.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_INFO)) {
                        this.visitedMilitaryInfo = true;
                    }

                    if (this._userService.checkUserCompletion(this.userCompletion, UserService.VISITED_MILITARY_INFO_CLOSED_HINT)) {
                        this.visitedMilitaryInfoClosedHint = true;
                    }

                    const guidedTipConfigs = this._userService.getCompletionByType(this.userCompletion, UserService.GUIDED_TIP_CONFIG);
                    this.guidedTipConfig = JSON.parse(guidedTipConfigs.value);

                    this.showRelatedCareerCCTip = this.guidedTipConfig.exploreCareer
                        && (!this._userService.checkUserCompletion(this.userCompletion, UserService.VIEWED_RELATED_CAREERS)
                        || !this._userService.checkUserCompletion(this.userCompletion, UserService.VIEWED_RELATED_CC))
                        && !this._userService.checkUserCompletion(this.userCompletion, UserService.RELATED_CAREERS_CC_CLOSED_HINT)
                        && this._userService.checkUserCompletion(this.userCompletion, UserService.COMPARE_JOB_ATTRIBUTES_CLOSED_HINT);

                    this.showCompareJobAttributesTip = this.guidedTipConfig.helpLearn
                        && !this._userService.checkUserCompletion(this.userCompletion, UserService.COMPARE_JOB_ATTRIBUTES_CLOSED_HINT);
                // }
            })
        }

        // Portfolio data resolver
        let occupationDetailsResolveArray = this._activatedRoute.snapshot.data.occupationDetails;

        this.occupationCertificates = occupationDetailsResolveArray[0];
        this.occupationTitle = occupationDetailsResolveArray[1].title;
        this.occupationDescription = occupationDetailsResolveArray[1].description;
        this.stateSalary = occupationDetailsResolveArray[2];
        this.stateEntrySalary = occupationDetailsResolveArray[3];
        this.nationalSalary = occupationDetailsResolveArray[4];
        this.nationalEntrySalary = occupationDetailsResolveArray[5];
        this.blsTitle = occupationDetailsResolveArray[6];
        this.skillImportance = occupationDetailsResolveArray[7];
        this.tasks = occupationDetailsResolveArray[8];
        this.occupationInterestCodes = occupationDetailsResolveArray[9];
        this.educationLevel = occupationDetailsResolveArray[10];
        this.relatedOccupations = occupationDetailsResolveArray[11];
        this.hotGreenStemFlags = occupationDetailsResolveArray[12];
        this.servicesOffering = occupationDetailsResolveArray[13];
        this.relatedCareerCluster = occupationDetailsResolveArray[14];
        this.moreResources = occupationDetailsResolveArray[15];
        this.schoolDetails = occupationDetailsResolveArray[16];
        this.alternateView = occupationDetailsResolveArray[17];
        this.imageAltText = occupationDetailsResolveArray[18];
        this.jobZone = occupationDetailsResolveArray[19];
        this.certificateExplanation = occupationDetailsResolveArray[20] ? occupationDetailsResolveArray[20].licensing + occupationDetailsResolveArray[20].training: null;
        this.alternateTitles = occupationDetailsResolveArray[21];

        if ( this._userService.getUser() ) {
            this.favorites = occupationDetailsResolveArray[22];
            this.userId = this._userService.getUser().userId;
        }

        this.stateSalaryYear = occupationDetailsResolveArray[23];
        this.workValues = occupationDetailsResolveArray[24];

        this.workValues.map((workValue: any) => {

            //for each work value return set up its icon class 
            //and title for 508
            let workValueToDisplay = this.workValueDisplayObjArr.find(x => x.name.toLowerCase() == workValue.name.toLowerCase());

            if(workValueToDisplay){
                workValue.iconClass = workValueToDisplay.iconClass;
                workValue.title = workValueToDisplay.rel;
            }else{
                workValue.iconClass = "";
                workValue.title = "";
            }
            
    
            return workValue;
          });

        this.occupationGreen = this.hotGreenStemFlags.isGreenOccupation == "1" ? true : false;
        this.occupationStem = this.hotGreenStemFlags.isStemOccupation == "1" ? true : false;
        this.occupationBright = this.hotGreenStemFlags.isBrightOccupation == "1" ? true : false;

        if ( typeof this.occupationInterestCodes[0] != 'undefined' ) {
            this.occupationInterestOne = this.occupationInterestCodes[0].interestCd;
            this.occupationInterestTwo = this.occupationInterestCodes[1].interestCd;
            this.occupationInterestThree = this.occupationInterestCodes[2].interestCd;
        }

        if ( this.IsJsonString( this.occupationCertificates.payload ) ) {
            this.occupationCertificates = JSON.parse( this.occupationCertificates.payload );
        } else {
            this.occupationCertificates = this.occupationCertificates.payload;
        }

        if ( this.occupationCertificates ) {
            if ( this.IsJsonString( this.occupationCertificates ) ) {
                this.occupationCertificates.RecordCount = 0;
            }
        }
        if ( this._userService.isLoggedIn() ) {
            this.occuFindUserLoggedIn = true;
        } else {
            this.occuFindUserLoggedIn = false;
        }

        //******Salary Map******//
        var outerArray = [];
        var innerArray = ['State', 'AverageSalary', {
            type: 'string',
            role: 'tooltip'
        }];
        outerArray[0] = innerArray;
        for ( var i = 0; i < this.stateSalary.length; i++ ) {
            if ( this.stateSalary[i].avgSalary != -1 ) {
                innerArray = [this.stateSalary[i].state, this.stateSalary[i].avgSalary, 'Average Salary: $' + this.stateSalary[i].avgSalary.toFixed( 2 ).replace( /(\d)(?=(\d{3})+\.)/g, '$1,' )];
            } else {
                innerArray = [this.stateSalary[i].state, 187200, 'Average Salary: $' + '187,200 or more'];
            }

            outerArray[i + 1] = innerArray;
        }

        var chart1 = { chartType: undefined, dataTable: undefined, options: undefined };
        chart1.chartType = "GeoChart";
        chart1.dataTable = outerArray;
        chart1.options = {
            width: 480,
            /*
             * width : 406, height : 297,
             */
            region: "US",
            resolution: "provinces",
            colors: ['#ffffff', '#02767b']
        };
        this.chart = chart1;

        var entryOuterArray = [];
        var entryInnerArray = ['State', 'EntrySalary', {
            type: 'string',
            role: 'tooltip'
        }];
        entryOuterArray[0] = entryInnerArray;
        for ( var i = 0; i < this.stateEntrySalary.length; i++ ) {
            if ( this.stateEntrySalary[i].entrySalary == undefined ) {
                entryInnerArray = [this.stateEntrySalary[i].state, 0, 'Entry Salary: N/A'];
            } else if ( this.stateEntrySalary[i].entrySalary != -1 ) {
                entryInnerArray = [this.stateEntrySalary[i].state, this.stateEntrySalary[i].entrySalary, 'Entry Salary: $' + this.stateEntrySalary[i].entrySalary.toFixed( 2 ).replace( /(\d)(?=(\d{3})+\.)/g, '$1,' )];
            } else {
                entryInnerArray = [this.stateEntrySalary[i].state, 208000, 'Entry Salary: $208000 or more'];
            }
            entryOuterArray[i + 1] = entryInnerArray;
        }

        // entry-level state salary
        var entryLevelSalaryChart = { chartType: undefined, dataTable: undefined, options: undefined };
        entryLevelSalaryChart.chartType = "GeoChart";
        entryLevelSalaryChart.dataTable = entryOuterArray;
        entryLevelSalaryChart.options = {
            width: 480,
            region: "US",
            resolution: "provinces",
            colors: ['#fff', '#02737b']
        }
        this.entryLevelSalaryChart = entryLevelSalaryChart;

        // mobile version
        var chart2 = { chartType: undefined, dataTable: undefined, options: undefined };
        chart2.chartType = "GeoChart";
        chart2.dataTable = outerArray;
        chart2.options = {
            width: 300,
            /*
             * width : 406, height : 297,
             */
            region: "US",
            resolution: "provinces",
            colors: ['#ffffff', '#02767b']
        };
        this.chartMobile = chart2;

        var entryLevelSalaryChartMobile = { chartType: undefined, dataTable: undefined, options: undefined };
        entryLevelSalaryChartMobile.chartType = "GeoChart";
        entryLevelSalaryChartMobile.dataTable = entryOuterArray;
        entryLevelSalaryChartMobile.options = {
            width: 300,
            region: "US",
            resolution: "provinces",
            colors: ['#fff', '#02767b']
        }
        this.entryLevelSalaryChartMobile = entryLevelSalaryChartMobile;



        /******Education Bar Chart******/
        var barColor = ['blue1', 'blue2', 'blue3', 'blue4', 'purple1', 'purple2', 'purple3'];

        function shortenEducationName( education ) {

            // Delete “or the equivalent” after High School Diploma. 
            if ( education.indexOf( "High School Diploma or the equivalent" ) != -1 ) {
                return "High School Diploma";
            }
            // Change "Less than a high school diploma" to "Some High School"
            if ( education.indexOf( "Less than a High School" ) != -1 ) {
                return "Some High School";
            }
            // Change “Some College Education” to “Some College"
            if ( education.indexOf( "Some College Courses" ) != -1 ) {
                return "Some College";
            }
            // Change "Post Baccalaureate Certificate" to "Post Baccalaureate"
            if ( education.indexOf( "Post Baccalaureate Certificate" ) != -1 ) {
                return "Post Baccalaureate";
            }
            // Change “Associate's Degree (or other 2-year degree)” to "Associate's Degree"
            if ( education.indexOf( "Associate's Degree (or other 2-year degree)" ) != -1 ) {
                return "Associate's Degree";
            }
            return education;
        }


        if (!this.barList.length) {
            for ( var i = 0; i < this.educationLevel.length; i++ ) {
                var educationValue = Math.round( this.educationLevel[i].educationLvlVal );
                if ( educationValue === 0 ) {
                    continue;
                }

                var data = {};
                var education = shortenEducationName( this.educationLevel[i].educationDesc );

                data['school'] = education;
                data['value'] = educationValue;
                data['color'] = barColor.shift();


                // Just in case there are more than 7 education, re-use color
                barColor.push( data['color'] );

                this.barList.push( data );
            }
        }

        if (this.occupationTitle && this.occupationDescription) {
            this._titleTag.setTitle('With ASVAB CEP, I explored: ' + this.occupationTitle);
            this._meta.updateTag({name: 'description', content: this.occupationDescription});
      
            this._meta.updateTag({property: 'og:title', content: 'With ASVAB CEP, I explored: ' + this.occupationTitle});
            this._meta.updateTag({property: 'og:description', content: this.occupationDescription});
            this._meta.updateTag({property: 'og:image', content: this.imageUrl});
            this._meta.updateTag({property: 'og:image:alt', content: this.occupationTitle});
            this._meta.updateTag({property: 'og:url', content: fullUrl});
      
            this._meta.updateTag({ name: 'twitter:site', content: '@CareersintheMil'});
            this._meta.updateTag({ name: 'twitter:creator', content: '@CareersintheMil'});
            this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
            this._meta.updateTag({ name: 'twitter:title', content:  'With ASVAB CEP, I explored: ' + this.occupationTitle});
            this._meta.updateTag({ name: 'twitter:image', content: this.imageUrl});
            this._meta.updateTag({ name: 'twitter:description', content: this.occupationDescription});
        }
    }

    ngOnDestroy() {
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    closeRelatedCareerCCTip() {
        this.showRelatedCareerCCTip = false;
        this._userService.setCompletion(UserService.RELATED_CAREERS_CC_CLOSED_HINT, 'true');
    }

    closeCompareJobAttributesTip() {
        this.showCompareJobAttributesTip = false;
        this._userService.setCompletion(UserService.COMPARE_JOB_ATTRIBUTES_CLOSED_HINT, 'true');
    }
    
    closeVisitedHowToGetThereDetailLinkTip() {
        this.visitedHowToGetThereDetailLinkClosedHint = true;
        this._userService.setCompletion(UserService.VISITED_OCCUPATION_DETAIL_LINK_CLOSED_HINT, 'true');
    }

    closeVisitedMilitaryInfoTip() {
        this.visitedMilitaryInfoClosedHint = true;
        this._userService.setCompletion(UserService.VISITED_MILITARY_INFO_CLOSED_HINT, 'true');
    }

    showNotes() {
        this._dialog.open(NotesDialogComponent, {
            data: {
                title: this.occupationTitle,
                socId: this.socId,
            },
            maxWidth: '900px',
        });
    }

    showModifiedMsg() {
        var socId = this.socId;
        if ( socId != undefined ) {
            var firstThreePositions = socId.substring( 0, 3 );
            if ( firstThreePositions == '55-' ) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    showBLSStatement() {
        var socId = this.socId;
        if ( socId != undefined ) {
            var lastTwoPositions = socId.slice( -2 );
            if ( lastTwoPositions == '00' ) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    ShrinkDescription( description, size ) {
        if ( description.length <= size ) {
            return description;
        } else {
            return description.substring( 0, size ) + "...";
        }
    };


    /**
     * Skill rating
     */
    ngAfterViewInit() {
        $( '.customrating' ).each( function() {
            var rating = $( this ).data( 'customrating' );
            var width = $( this ).width() / 5 * rating;
            if ( rating.toString().indexOf( '.' ) !== -1 ) {
                width += 1;
            }
            $( this ).width( width );
        } );


        /**
         * New star rating.
         */
        $( function() {
            $( '.ratebox' ).raterater( {

                // allow the user to change their mind after they have submitted a rating
                allowChange: false,

                // width of the stars in pixels
                starWidth: 12,

                // spacing between stars in pixels
                spaceWidth: 0,

                numStars: 5,
                isStatic: true,
                step: false,
            } );

        } );
    }


    IsJsonString( str ) {
        try {
            JSON.parse( str );
        } catch ( e ) {
            return false;
        }
        return true;
    }



    logout = function() {
        //TODO:
        /*AuthenticationService.ClearCredentials();*/
    };

    showLoginModal() {
        this._dialog.open(LoginDialogComponent , {
            hasBackdrop: true,
            panelClass: 'new-modal',
            // height: '95%',
            width: '300px',
            autoFocus: true,
            disableClose: true
          });
    }


    



    /**
     * Pie chart Implementation
     */


    //    $scope.chartObject = {};
    //    var col = [ {
    //        id : "e",
    //        label : "Education",
    //        type : "string"
    //    }, {
    //        id : "v",
    //        label : "Value",
    //        type : "number"
    //    } ];
    //    var row = [];
    //
    //    for (var i = 0; i < $scope.educationLevel.length; i++) {
    //        var c = {
    //            c : [ {
    //                v : $scope.educationLevel[i].educationDesc
    //            }, {
    //                v : $scope.educationLevel[i].educationLvlVal
    //            } ]
    //        };
    //        row.push(c);
    //    }
    //
    //    $scope.chartObject.data = {
    //        "cols" : col,
    //        "rows" : row
    //    };
    //
    //    $scope.chartObject.type = "PieChart";
    //    $scope.chartObject.options = {
    //        colors : [ '#02767b', '#29afb5', '#8bd6d9', '#cee3e7', '#9593b8', '#2f2a6a', '#5c5792', '#8c5050', '#885a4c', '#d69785' ]
    //    };
    //
    //    $scope.chartPie = $scope.chartObject;


    relatedCareers( onetSoc ) {
        if (this._userService.isLoggedIn()) {
            this._userService.setCompletion(UserService.VIEWED_RELATED_CAREERS, 'true');
        }
        this._router.navigate( ['/occufind-occupation-details/' + onetSoc] );
    }

    relatedCC(ccId) {
        if (this._userService.isLoggedIn()) {
            this._userService.setCompletion(UserService.VIEWED_RELATED_CC, 'true');
        }
        this._router.navigate( ['/career-cluster-pathway-' + ccId] );
    }

    // direct to military more details page
    militaryMoreDetails() {
        this._router.navigate( ['/occufind-military-details/' + this.socId] );
    }

    // direct to school more details page
    schoolMoreDetails() {
        this._router.navigate( ['/occufind-education-details/' + this.socId] );
    }

    // direct to school more details page
    employmentMoreDetails() {
        this._router.navigate( ['/occufind-employment-details/' + this.socId] );
    }

    findCertifications() {
        const data = {
            certs: this.occupationCertificates,
            occupationTitle: this.occupationTitle
        };
        const dialogRef1 = this._dialog.open( CertificationListComponent, {
            data: data,
            autoFocus: false,
            maxHeight: 'calc(100vh - 100px)',
            height: 'auto',
        } );
    }

    findLicenses() {
        const data = {
            onetOccupationCode: this.socId,
            occupationTitle: this.occupationTitle
        };
        const dialogRef1 = this._dialog.open( LicenseListComponent, {
            data: data,
        } );

    }

    findApprenticeship() {
        const data = {
            onetOccupationCode: this.socId,
            occupationTitle: this.occupationTitle
        };
        const dialogRef1 = this._dialog.open( ApprenticeshipListComponent, {
            data: data,
        } );
    }


    orderByService( item ) {
        if ( item.serviceCode == 'A' ) {
            return 1;
        } else if ( item.serviceCode == 'M' ) {
            return 2;
        } else if ( item.serviceCode == 'N' ) {
            return 3;
        } else if ( item.serviceCode == 'F' ) {
            return 4;
        } else if ( item.serviceCode == 'C' ) {
            return 5;
        } else if ( item.serviceCode == 'G' ) {
            return 6;
        } else {
            return 7;
        }
    }


    /**
     * Opens modal popup for description for each section.
     */
    openDescription( section ) {

        var headerTitle, bodyText;
        if ( section == 'interest-codes' ) {
            headerTitle = "Interest Codes";
            bodyText = "RIASEC codes represent the interest codes most closely related to each occupation. Evaluate how well the interest codes for this occupation match your interests."
        } else if ( section == 'skill-importance' ) {
            headerTitle = 'Skill Importance Ratings';
            bodyText = 'These ratings show the relative importance of Verbal, Math, and Science/Technical skills for performing the job. The Skill Importance Ratings are not the same as Career Exploration Scores, but can be used to evaluate suitability for the job.';
        } else if ( section == 'what-they-do' ) {
            headerTitle = 'What They Do';
            bodyText = 'This is a list of tasks performed on the job.';
        } else if ( section == 'how-to-get-there' ) {
            headerTitle = 'How to Get There';
            bodyText = 'There may be different ways to get started in the career you want.  Learn about the education, credentials, licenses, apprenticeships, and military opportunities below.';
        } else if ( section == 'education' ) {
            headerTitle = 'Education';
            bodyText = ' This section describes the levels of education seen as necessary to get this job as reported by people who work in the job. ' +
                '  You can find a list of institutions that offer degrees related to this occupation under College Info.  ' +
                ' These totals are rounded to the nearest percent. The actual descriptions of levels of education included are as follows:<br/><br/>  ' +
                ' <ul> ' +
                ' <li>Less than a High School Diploma </li> ' +
                ' <li>High School Diploma or the equivalent (for example GED)</li> ' +
                ' <li>Post-Secondary Certificate</li> ' +
                ' <li>Some College Courses</li> ' +
                ' <li>Associate\'s Degree (or other 2-year degree)</li> ' +
                ' <li>Bachelor\'s Degree</li> ' +
                ' <li>Post-Baccalaureate Certificate</li> ' +
                ' <li>Master\'s Degree</li> ' +
                ' <li>Post-Master\'s Certificate</li> ' +
                ' <li>First Professional Degree</li> ' +
                ' <li>Doctoral Degree</li> ' +
                ' <li>Post-Doctoral Training</li> ' +
                ' </ul>';
        } else if ( section == 'credentials' ) {
            headerTitle = 'Credentials';
            bodyText = 'Certification, licensure, and/or apprenticeships are possible paths to this career. Select Find Certification, Find Licenses, and Find Apprenticeships to explore these options.';
        } else if ( section == 'mil-offering' ) {
            headerTitle = 'Military Services Offering this Occupation';
            bodyText = 'This job is available in the Military.  Select Military Info to learn more about which branches offer related opportunities.';
        } else if ( section == 'emp-stats' ) {
            headerTitle = 'Employment Statistics';
            bodyText = 'This section illustrates the average salary for this occupation by state. Select More Details to learn more about the national employment outlook.';
        } else if ( section == 'poverty-level' ) {
            headerTitle = 'Poverty Level';
            bodyText = 'This number is issued each year by the Department of Health and Human Services (HHS). It represents the poverty guidelines for a single individual living in the 48 contiguous states. The number varies for citizens of Alaska and Hawaii, and increases with each additional member of the household.';
        } else if ( section == 'work-values' ) {
            headerTitle = 'Work Values';
			bodyText = 'The work values listed represent those most closely related to this occupation. Evaluate how well the work values for this occupation match your own.';
        }else if(section == 'Achievement'){
			headerTitle = 'ACHIEVEMENT';
			bodyText = 'Those who score high on Achievement are results-oriented. These workers often pursue jobs where employees are able to apply their strengths and abilities. This gives the employee a sense of accomplishment.';
		}else if(section == 'Independence'){
			headerTitle = 'INDEPENDENCE';
			bodyText = 'Those who score high on Independence value the ability to approach work activities with creativity. These workers want to make their own decisions and plan their work with little supervision from a manager.';
		} else if(section == 'Recognition'){
			headerTitle = 'RECOGNITION';
			bodyText = 'Those who score high on Recognition pursue jobs with opportunities for advancement and leadership responsibilities that allow them to give direction and instruction to others. These workers are often considered prestigious by their peers and others in their organization and receive recognition for the work they contribute.';
		} else if(section == 'Relationships'){
			headerTitle = 'RELATIONSHIPS';
			bodyText = 'Those who score high on Relationships prefer jobs that provide services to others and work with others in a friendly, non-competitive environment. Workers in these jobs value getting along well with others and do not like to be pressured to do things that go against their morals or sense of what is right and wrong.';
		} else if(section == 'Support'){
			headerTitle = 'SUPPORT';
			bodyText = 'Those who score high on Support appreciate when their company\'s leadership stands behind and supports their employees. People in these types of jobs like to feel like they are being treated fairly by the company and have supervisors who spend time and effort training their workers to perform well.';
		} else if(section == 'Working Conditions'){
			headerTitle = 'WORKING CONDITIONS';
			bodyText = 'Those who score high on Working Conditions value job security and pleasant working conditions. These workers enjoy being busy and want to be paid well for the work they do. They enjoy developing ways of doing things with little or no supervision and depend on themselves to get the work done. These workers pursue steady employment that offers something different to do on a daily basis.';
		}else {
            return 0;
        }

        var modalOptions = {
            headerText: headerTitle,
            bodyText: bodyText
        };

        var screenSize = 'sm';
        if ( section == 'education' ) {
            screenSize = 'md';
        }

        const data = {
            'title': modalOptions.headerText,
            'message': modalOptions.bodyText
        };
        const dialogRef1 = this._dialog.open( MessageDialogComponent, {
            data: data,
            //hasBackdrop: true,
            //disableClose: true,
            maxWidth: '600px'
        } );
    }

    getShareUrl() {
        return window.location.href;
    }

    setCompletionOfMoreResource() {
        this._userService.setCompletion(UserService.VISITED_RESOURCE, 'true');
    }

    setCompletionVisitedOccupationDetailLink() {
        this._userService.setCompletion(UserService.VISITED_OCCUPATION_DETAIL_LINK, 'true');
    }

    setCompletionVisitedMilitaryInfo() {
        this._userService.setCompletion(UserService.VISITED_MILITARY_INFO, 'true');
    }
}
