import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-occufind-occupation-details-not-logged-in',
  templateUrl: './occufind-occupation-details-not-logged-in.component.html',
  styleUrls: ['./occufind-occupation-details-not-logged-in.component.scss']
})
export class OccufindOccupationDetailsNotLoggedInComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
