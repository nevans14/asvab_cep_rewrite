import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccufindOccupationDetailsNotLoggedInComponent } from './occufind-occupation-details-not-logged-in.component';

describe('OccufindOccupationDetailsNotLoggedInComponent', () => {
  let component: OccufindOccupationDetailsNotLoggedInComponent;
  let fixture: ComponentFixture<OccufindOccupationDetailsNotLoggedInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccufindOccupationDetailsNotLoggedInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccufindOccupationDetailsNotLoggedInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
