import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';

import { UserService } from 'app/services/user.service';
import { UtilityService } from 'app/services/utility.service';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';

@Component({
  selector: 'app-school-details',
  templateUrl: './school-details.component.html',
  styleUrls: ['./school-details.component.scss']
})
export class SchoolDetailsComponent implements OnInit {

  occupationTitle;
  schoolDetails;
  schoolTitle;
  socId;
  unitId;
  favoriteSchools;
  favorites;
  percentageAdmitted;
  currentUser;
  occuFindUserLoggedIn;
  satCritica25Percent;
  satCritica75Percent;
  satMath25Percent;
  satMath75Percent;
  actCritical25Percent;
  actCritical75Percent;
  objectKeys = Object.keys;
  myChartObject: GoogleChartInterface;
  favoritedCollegeClosedHint = false;
  userCompletion;
  guidedTipConfig;

  constructor(
    private _user: UserService,
    private route: ActivatedRoute,
    private _utility: UtilityService,
    private _dialog: MatDialog,
    ) { }

  ngOnInit() {
    this.occupationTitle = this.route.snapshot.data.schoolDetails[0] 
      ? this.route.snapshot.data.schoolDetails[0].title
      : null;
    this.schoolDetails = this.route.snapshot.data.schoolDetails[1];
    this.favoriteSchools = this.route.snapshot.data.schoolDetails[2];
    this.favorites = this.route.snapshot.data.schoolDetails[3];

    this.schoolTitle = this.schoolDetails.institutionName;
    this.socId = this.route.snapshot.paramMap.get( 'socId' );
    this.unitId = this.route.snapshot.paramMap.get( 'unitId' );
	  this.favoriteSchools = this.favoriteSchools ? this.favoriteSchools : undefined;
	  this.percentageAdmitted = this.schoolDetails.totalAdmitted / this.schoolDetails.totalApplicants * 100;
    
    this.currentUser = this._user.getUser();
    if ( typeof this.currentUser === 'undefined' ) {
      this.occuFindUserLoggedIn = false ; 
    } else {
      this.occuFindUserLoggedIn = true ;
    }

      // Pie chart
    this.myChartObject = {
      chartType: 'PieChart',
      dataTable: {
        "cols" : [
          {
            id : "t",
            label : "Topping",
            type : "string"
          },
          {
            id : "s",
            label : "Slices",
            type : "number"
          }
        ],
        "rows" : [
          {
            c : [
              {
                v : "2015"
              },
              {
                v : this.schoolDetails.retentionRate
              },
            ]
          },
          {
            c : [
              {
                v : "2016"
              },
              {
                v : 100 - this.schoolDetails.retentionRate
              },
            ]
          }
        ]
      },
      options: {
        'legend' : 'none',
        tooltip : {
          trigger : 'none'
        },
        slices : {
          0 : {
            color : '00acad'
          },
          1 : {
            color : 'transparent'
          }
        }
      }
    };

    //  Test score graph
    this.satCritica25Percent = 268*this.schoolDetails.satCritica25Percent/800;
    this.satCritica75Percent = 268*this.schoolDetails.satCritica75Percent/800;
    
    this.satMath25Percent = 268*this.schoolDetails.satMath25Percent/800;
    this.satMath75Percent = 268*this.schoolDetails.satMath75Percent/800;

    this.actCritical25Percent = 268*this.schoolDetails.actCritical25Percent/31;
    this.actCritical75Percent = 268*this.schoolDetails.actCritical75Percent/31;

    this._user.getCompletion().subscribe(data => {
      this.userCompletion = data;

      if (this._user.checkUserCompletion(this.userCompletion, UserService.FAV_COLLEGE_CLOSED_HINT)) {
        this.favoritedCollegeClosedHint = true;
      }

      const guidedTipConfigs = this._user.getCompletionByType(this.userCompletion, UserService.GUIDED_TIP_CONFIG);
      this.guidedTipConfig = JSON.parse(guidedTipConfigs.value);

      // this.checkPlusItemToShow();
    });
  }

  closedFavCollegeHint() {
    this.favoritedCollegeClosedHint = true;
    this._user.setCompletion(UserService.FAV_COLLEGE_CLOSED_HINT, 'true');
  }

  showNotes() {
    this._dialog.open(NotesDialogComponent, {
        data: {
            schoolName: this.schoolTitle,
            unitId: this.unitId,
        },
        maxWidth: '900px',
    });
  }

  openDescription(section) {
    var title, message;
    if(section == 'rotc'){
      title = 'Reserve Officers Training Corps (ROTC)';
      message = 'Reserve Officers Training Corps (ROTC) is a college scholarship program that prepares students to become military officers. Offered at more than'+
        ' 1,000 colleges and universities, it can help cover the costs of tuition and expenses.  '+
        ' <a href="https://www.careersinthemilitary.com/options-becoming-an-officer" target="_blank">Learn more about ROTC</a>.';
    } else {
      return 0;
    }

    this._utility.modal(title, message);
  }

  isFavorited(unitId) {
    if (this.currentUser) {
      var school = this.getSchoolInFavorites(unitId);
      return (school ? true : false);
    }
    return false;
  }

  getSchoolInFavorites(unitId) {
    var school = undefined;
    for (var i = 0; i < this.favoriteSchools.length; i++) {
      if (this.favoriteSchools[i].unitId === parseInt(unitId)) {
        return school = this.favoriteSchools[i];
      }
    }
    return school;
  }

  addFavorite(unitId) {
    if(this.favoriteSchools.length >= 10) {
      this._utility.modal('FAVORITE SCHOOLS', 'Favorite Schools list is full. Please remove a school before adding this one.')
      return;
    }

    var requestObj = {
      unitId
    };

    let self = this;
    this._user.insertFavoriteSchool(requestObj).then((success: any) => {
      self.favoriteSchools.push({
        id: success.id,
        unitId: success.unitId,
        schoolName: self.schoolTitle,
      });

      self.favoriteSchools = self.favoriteSchools.filter(s => !!s.schoolName);
      
      // update session storage
      this._user.setSchoolFavorites(self.favoriteSchools);

      this._utility.modal('FAVORITE SCHOOLS', this.schoolTitle.bold() + ' is added to your favorites.')
    }, err => {
      console.error(err)
    })
  }

  removeFromFavorites(unitId) {
    let self = this;
    let favoriteSchoolObj = this.getSchoolInFavorites(unitId);
    if (favoriteSchoolObj) {
      this._user.deleteFavoriteSchool(favoriteSchoolObj.id).then((success: any) => {
        let index = self.getIndexOfSchoolInFavorites(success.unitId);
        self.favoriteSchools.splice(index, 1);
        
        // update session storage
        this._user.setSchoolFavorites(self.favoriteSchools);

        this._utility.modal('FAVORITE SCHOOLS', this.schoolTitle.bold() + ' was removed from your favorites.')
      })
    }
  }

  getIndexOfSchoolInFavorites(unitId) {
    let index = -1;
    for (let i = 0; i < this.favoriteSchools.length; i++) {
      if (this.favoriteSchools[i].unitId === parseInt(unitId)) {
        return index = i;
      }
    }
    return index;
  }
}
