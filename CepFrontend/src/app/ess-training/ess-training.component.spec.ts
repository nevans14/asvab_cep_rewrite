import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssTrainingComponent } from './ess-training.component';

describe('EssTrainingComponent', () => {
  let component: EssTrainingComponent;
  let fixture: ComponentFixture<EssTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EssTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
