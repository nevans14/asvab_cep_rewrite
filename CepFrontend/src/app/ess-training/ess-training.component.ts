import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { EssTrainingService } from 'app/services/ess-training.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { MatDialog, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MessageDialogComponent } from '../core/dialogs/message-dialog/message-dialog.component';
import { ConfigService } from 'app/services/config.service';
import { AlternateTrainingDialogComponent } from '../core/dialogs/alternate-training-dialog/alternate-training-dialog.component';
import { MatDateFormats, MAT_NATIVE_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

import * as _moment from 'moment';

const moment = _moment;
@Component({
  selector: 'app-ess-training',
  templateUrl: './ess-training.component.html',
  styleUrls: ['./ess-training.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class EssTrainingComponent implements OnInit {

  isVirtual: boolean;
  isVirtualPTI: boolean;
  isESS: boolean;
  isTroopsToTeachers: boolean;

  training: any;
  alternateTraining: any;
  alternateTrainingSelected: any;
  virtualTrainingAccessCodeLookupPath: any;
  virtualTrainingBarPath: any;

  validation;
  todayDate : any;
  regId : any;
  regFirstName : any;
  regLastName : any;
  regEmailAddress : any;
  regAddress : any;
  regOrganization : any;
  regRole : any;
  regComment: any;
  showDietaryRestrictions : any;
  regDietaryRestrictions : any;
  showMobilityRestrictions : any;
  regMobilityRestrictions : any;
  locationName: any;
  addressOne: any;
  addressTwo: any;
  attn: any;
  city: any;
  formDisabled: any; 
  regState: any;
  regZip: any;


  //set non-virtual form controls default values and assign to form
  nonVirtualForm = new FormGroup({
    regFirstName: new FormControl('', Validators.required),
    regLastName: new FormControl('', Validators.required),
    regEmailAddress: new FormControl(''),
    regOrganization: new FormControl(''),
    regRole: new FormControl(''),
    regComment: new FormControl(''),
    regDietaryRestrictions: new FormControl(''),
    regMobilityRestrictions: new FormControl('')
  });

  //set virtual form controls default values and assign to form
  virtualForm = new FormGroup({
    regRole: new FormControl('', Validators.required),
    regFirstName: new FormControl('', Validators.required),
    regLastName: new FormControl('', Validators.required),
    regEmailAddress: new FormControl('', Validators.required),
    hasResultsSheet: new FormControl(''),
    regSchoolName: new FormControl(''),
    regSchoolCity: new FormControl(''),
    regSchoolCounty: new FormControl(''),
    regCountry: new FormControl(''),
    regState: new FormControl({ value: '', disabled: true }),
    dt: new FormControl(moment()),
    regGradeLevel: new FormControl('')
  });

  //set troops form controls default values and assign to form
  troopsToTeachersForm = new FormGroup({
    regFirstName: new FormControl('', Validators.required),
    regLastName: new FormControl('', Validators.required),
    regEmailAddress: new FormControl('', Validators.required),
    regAddress: new FormControl('', Validators.required),
    regSchoolName: new FormControl('', Validators.required),
    regSchoolCity: new FormControl('', Validators.required),
    regCountry: new FormControl('', Validators.required),
    regState: new FormControl({ value: '', disabled: true }),
    regZip: new FormControl('', Validators.required)
  });

  constructor(
    private _essTrainingService: EssTrainingService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _config: ConfigService,
  ) { }

  ngOnInit() {

    //init vars
    this.validation;
    this.todayDate = new Date();
    this.regId = undefined;
    this.regFirstName = undefined;
    this.regLastName = undefined;
    this.regEmailAddress = undefined;
    this.regAddress = undefined;
    this.regOrganization = undefined;
    this.regRole = undefined;
    this.regComment = undefined;
    this.showDietaryRestrictions = undefined;
    this.regDietaryRestrictions = undefined;
    this.showMobilityRestrictions = undefined;
    this.regMobilityRestrictions = undefined;
    this.locationName = undefined;
    this.addressOne = undefined;
    this.addressTwo = undefined;
    this.attn = undefined;
    this.city = undefined;
    this.regState= '';
    this.regZip= undefined;

    //retrieve training info from resolver
    this.training = this._activatedRoute.snapshot.data["essTrainingResolveService"][0];
    this.alternateTraining = this._activatedRoute.snapshot.data["essTrainingResolveService"][1].filter
      (x => x.essTrainingId !== this.training.essTrainingId);
    this.alternateTrainingSelected = undefined;

    //set type of form
    this.isVirtual = false;
    this.isVirtualPTI = false;

    this.isESS = false;
    this.isTroopsToTeachers = false;
    if (this.training.typeId === 1) {
      this.isESS = true;
    } else if (this.training.typeId === 4) {
      this.isTroopsToTeachers = true;
    } else if (this.training.typeId === 5) {
      this.isVirtualPTI = true;
    }
    else {
      this.isVirtual = true;
    }

    this.virtualTrainingAccessCodeLookupPath = this._config.getImageUrl() + 'TRAINING/AccessCodeLookup.png';
    this.virtualTrainingBarPath = this._config.getImageUrl() + 'TRAINING/Testimonial.png';

    //if virtual form set observables for drop downs
    //this sets required fields; enables/disables inputsw
    if (this.isVirtual) {
      this.setVirtualFormOnChangesObservables();
    }

    if (this.isTroopsToTeachers) {
      this.setTroopsToTeachersFormOnChangesObservables();
    }

  }

  ngAfterViewInit() {

    if ((this.isEmpty() || this.isPassedCutOffDate() || this.isFull()) && this.alternateTraining.length > 0) {
      this.selectAlternateTraining();
    }

  }

  submitNonVirtualForm() {

    //map formData
    let postFormData = {
      essTrainingId: this._activatedRoute.snapshot.params.id,
      essTrainingRegistrationId: undefined,
      registrationFirstName: this.nonVirtualForm.value.regFirstName,
      registrationLastName: this.nonVirtualForm.value.regLastName,
      registrationEmailAddress: this.nonVirtualForm.value.regEmailAddress,
      registrationOrganization: this.nonVirtualForm.value.regOrganization,
      registrationRole: this.nonVirtualForm.value.regRole,
      registrationComment: this.nonVirtualForm.value.regComment,
      registrationDietaryRestrictions: (this.nonVirtualForm.value.regDietaryRestrictions) == undefined ? "" : this.nonVirtualForm.value.regDietaryRestrictions,
      registrationMobilityRestrictions: (this.nonVirtualForm.value.regMobilityRestrictions) == undefined ? "" : this.nonVirtualForm.value.regMobilityRestrictions
    };

    this._essTrainingService.addTrainingRegistration(postFormData).then(data => {
      this.nonVirtualForm.reset();
      //decrement
      this.training.numberOfParticipants++;
      
      var modalDataSuccess = {
        title: 'Training Session Registration',
        message: 'Thank you for registering for the training session! A confirmation email has been sent. Please check for you inbox (or possibly junk mail) for more information.'
      }

      this.showMessageModal(modalDataSuccess);

    }, error => {

      console.debug('error:', error);
      if (error === undefined) {
        const modalOptions = {
          title: 'Training Session Registration Error',
          message: 'An unexpected error has occurred. Please try again.'
        }
        this.showMessageModal(modalOptions);
      } else {
        var modalDataError = {
          title: 'Training Session Registration Error',
          message: error + ". Please try again."
        }
        this.showMessageModal(modalDataError);
      };

    });

  }

  submitVirtualForm() {
    let postFormData = {
      essTrainingId: this.training.essTrainingId,
      essTrainingRegistrationId: undefined,
      registrationFirstName: this.virtualForm.value.regFirstName,
      registrationLastName: this.virtualForm.value.regLastName,
      registrationEmailAddress: this.virtualForm.value.regEmailAddress,
      registrationRole: this.virtualForm.value.regRole,
      doesKnowAccessCode: this.virtualForm.value.hasResultsSheet,
      registrationTypeId: this.training.typeId,
      testScoreRequest: {
        schoolName: this.virtualForm.value.regSchoolName,
        schoolCity: this.virtualForm.value.regSchoolCity,
        schoolCounty: this.virtualForm.value.regSchoolCounty,
        schoolState: this.virtualForm.value.regState,
        schoolCountry: this.virtualForm.value.regCountry,
        testMonth: !this.virtualForm.value.dt ? undefined : this.virtualForm.value.dt.month() + 1,
        testYear: !this.virtualForm.value.dt ? undefined : this.virtualForm.value.dt.year(),
        gradeLevel: !this.virtualForm.value.regGradeLevel ? undefined : parseInt(this.virtualForm.value.regGradeLevel),
      }
    };

    this.submitPostData(postFormData)
  }

  submitTroopsToTeachersForm() {

    let postFormData = {
      essTrainingId: this.training.essTrainingId,
      essTrainingRegistrationId: undefined,
      registrationFirstName: this.troopsToTeachersForm.value.regFirstName,
      registrationLastName: this.troopsToTeachersForm.value.regLastName,
      registrationEmailAddress: this.troopsToTeachersForm.value.regEmailAddress,
      registrationRole: '',
      registrationTypeId: this.training.typeId,
      troopSchool: {
        city: this.troopsToTeachersForm.value.regSchoolCity,
        state: this.troopsToTeachersForm.value.regState,
        zipCode: this.troopsToTeachersForm.value.regZip,
        name: this.troopsToTeachersForm.value.regSchoolName,
        country: this.troopsToTeachersForm.value.regCountry,
        address: this.troopsToTeachersForm.value.regAddress
      }
    };

    this.submitPostData(postFormData)
  }

  /**
   * submits form data to server called from troops to teacher and virtual
   * @param postFormData data to submit to the server
   */
  submitPostData(postFormData) {

    let self = this;

    this._essTrainingService.addTrainingRegistration(postFormData).then((success: any) => {

      //reset forms
      if (this.isVirtual) {
        this.virtualForm.reset();
        //decrement
        this.training.numberOfParticipants++;
      }

      if (this.isTroopsToTeachers) {
        this.troopsToTeachersForm.reset();
        //decrement
        this.training.numberOfParticipants++;
      }

      if(this.isVirtualPTI){
        this.regFirstName = undefined;
        this.regLastName = undefined;
        this.regEmailAddress = undefined;
        this.regOrganization = undefined;
        this.regRole = undefined;
        this.locationName = undefined;
        this.addressOne = undefined;
        this.addressTwo = undefined;
        this.attn = undefined;
        this.city = undefined;
        this.regState = undefined;
        this.regZip = undefined;
        //decrement
        this.training.numberOfParticipants++;
      }

      //set body text
      let bodyText = '';
      switch (success.resultCode) {
        case 1:
        case 2:
          if (success.resultMessage) {
            bodyText = success.data.resultMessage;
            break;
          }
        default:
          bodyText = 'Thank you for registering for this session! A confirmation email has been sent. Please check for you inbox (or possibly junk mail) for more information.';
          break;
      }

      let modalDataSuccess = {
        title: 'Registration Confirmation',
        message: bodyText
      }

      this.showMessageModal(modalDataSuccess);

    }, (error: any) => {
      // console.log(error);
      if (error === undefined) {
        const modalOptions = {
          title: 'Training Session Registration Error',
          message: 'An unexpected error has occurred. Please try again.'
        }
        this.showMessageModal(modalOptions);
      } else {

        // Check if has open spot
        if (error.resultCode === 410) {
          this._essTrainingService.getAlternateTrainingByTrainingId(this.training.essTrainingId).then(function (success) {

            if (!success.data) {
              window.location.reload();
            }
            self.alternateTraining = success.data;

            if (self.alternateTraining.length > 0) {

              let modalOptions = {
                title: 'SELECT ANOTHER SESSION',
                message: "Registration for this event is unavailable at this time because the session is full or the date has passed. Please select from the options below. Submit inquiries to <a href=\"mailto:asvabcep@gmail.com\">asvabcep@gmail.com</a>.",
                alternateTraining: this.alternateTraining
              }

              self._dialog.open(AlternateTrainingDialogComponent, { data: modalOptions }).beforeClosed().subscribe(result => {

                if (result) {

                  if (result !== 'cancel') {
                    self.alternateTrainingSelected = result.modalSelection;

                    if (!isNaN(self.alternateTrainingSelected)) {
                      let currentTrainingId = self.training.essTrainingId;
                      self.training.essTrainingId = parseInt(self.alternateTrainingSelected);
                      //resubmit forms based on new alternate training id
                      if (self.isVirtual) {
                        self.submitVirtualForm();
                      } else if (self.isTroopsToTeachers) {
                        self.submitTroopsToTeachersForm();
                      }else{
                        self.submitNonVirtualForm();
                      }
                      self.training.essTrainingId = currentTrainingId;
                      self.disableForms();
                    } else {
                      self.training = undefined;
                      self.disableForms();
                    }

                  } else {
                    //cancel was selected
                    self.training = undefined;
                    self.disableForms();
                  }

                }

              });

            } else {
              window.location.reload();
            }
          }).catch(function (error) {
            self.errorHandlingModal(error);
          })
        } else {
          this.errorHandlingModal(error);
        }

      };

    });

  }



  /**
   * Displays message dialog component with title and message
   * @param msgData accepts object with title, and message
   */
  showMessageModal(msgData) {
    const dialogRef = this._dialog.open(MessageDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      autoFocus: true,
      data: msgData,
      maxWidth:'300px'
    });
  }

  errorHandlingModal(error) {
    let errorMsg = undefined;
    if (error.resultMessage) {
      errorMsg = error && error.resultMessage ? error.resultMessage : 'Error occured during registration.'
    } else {
      errorMsg = "It's taking longer than expected to find what you're looking for. Please try again.";
    }

    let modalOptions = {
      title: 'Training Session Registration',
      message: errorMsg
    }
    this.showMessageModal(modalOptions);

  }

  AccessCodeLookupModal() {

    let modalOptions = {
      title: 'Access Code Lookup',
      message: '<img src=' + this.virtualTrainingAccessCodeLookupPath + '></img>'
    }

    this.showMessageModal(modalOptions);

  }

  isClosed(): Boolean {
    return (this.isEmpty() || this.isFull() || this.isPassedCutOffDate());
  }

  isEmpty(): Boolean {
    return (this.training === undefined || this.training === "");
  }

  isFull(): Boolean {
    return (!this.isEmpty() && (this.training.essMaxNumberOfParticipants <= this.training.numberOfParticipants));
  }

  isPassedCutOffDate(): Boolean {
    return (!this.isEmpty() && (new Date() > this.convertUTCDateToLocalDate(new Date(this.training.essCutOffDate))));
  }

  convertUTCDateToLocalDate(date): any {
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    return new Date(Date.UTC(year, month, day, hour, minute, second));        
  }

  /**
   * set observables for regRole, hasResultsSheet, regCountry
   */
  setVirtualFormOnChangesObservables() {

    const hasResultsSheetControl = this.virtualForm.get('hasResultsSheet');
    const regCountryControl = this.virtualForm.get('regCountry');
    const regSchoolNameControl = this.virtualForm.get('regSchoolName');
    const regSchoolCityControl = this.virtualForm.get('regSchoolCity');
    const regSchoolCountyControl = this.virtualForm.get('regSchoolCounty');
    const regdtControl = this.virtualForm.get('dt');
    const regState = this.virtualForm.get('regState');
    const regGradeLevel = this.virtualForm.get('regGradeLevel');
    const regRoleControl = this.virtualForm.get('regRole');

    //if role changes reset required validation on controls
    regRoleControl.valueChanges.subscribe(selection => {

      if (selection === 'Student') {
        //set the hasResultsSheet required
        hasResultsSheetControl.setValidators([Validators.required]);
        hasResultsSheetControl.updateValueAndValidity();
      } else {
        //clear all validators
        hasResultsSheetControl.setValue(null);
        hasResultsSheetControl.clearValidators();
        regSchoolNameControl.clearValidators();
        regSchoolCityControl.clearValidators();
        regSchoolCountyControl.clearValidators();
        regCountryControl.setValue('');
        regCountryControl.clearValidators();
        regdtControl.clearValidators();
        regGradeLevel.clearValidators();
        regState.clearValidators();
        regState.disable();

        hasResultsSheetControl.updateValueAndValidity();
        regSchoolNameControl.updateValueAndValidity();
        regSchoolCityControl.updateValueAndValidity();
        regSchoolCountyControl.updateValueAndValidity();
        regCountryControl.updateValueAndValidity();
        regdtControl.updateValueAndValidity();
        regGradeLevel.updateValueAndValidity();
        regState.updateValueAndValidity();
      }
    });

    hasResultsSheetControl.valueChanges.subscribe(selection => {

      //if student does not have a datasheet; require all fields
      if (selection === "false") {

        regSchoolNameControl.setValidators([Validators.required]);
        regSchoolCityControl.setValidators([Validators.required]);
        regSchoolCountyControl.setValidators([Validators.required]);
        regCountryControl.setValidators([Validators.required]);
        regdtControl.setValidators([Validators.required]);
        regGradeLevel.setValidators([Validators.required]);

        //update controls with new validation
        regSchoolNameControl.updateValueAndValidity();
        regSchoolCityControl.updateValueAndValidity();
        regSchoolCountyControl.updateValueAndValidity();
        regCountryControl.updateValueAndValidity();
        regdtControl.updateValueAndValidity();
        regGradeLevel.updateValueAndValidity();

      } else {
        //clear all validators
        regSchoolNameControl.clearValidators();
        regSchoolCityControl.clearValidators();
        regSchoolCountyControl.clearValidators();
        regCountryControl.setValue('');
        regCountryControl.clearValidators();
        regdtControl.clearValidators();
        regGradeLevel.clearValidators();
        regState.disable();
        regState.clearValidators();

        //update controls with new validation
        regSchoolNameControl.updateValueAndValidity();
        regSchoolCityControl.updateValueAndValidity();
        regSchoolCountyControl.updateValueAndValidity();
        regCountryControl.updateValueAndValidity();
        regdtControl.updateValueAndValidity();
        regGradeLevel.updateValueAndValidity();
        regState.updateValueAndValidity();
      }

    });

    //if a student and no datasheets; if US is selected require state selection and enable it
    regCountryControl.valueChanges.subscribe(selection => {

      if (selection === 'US') {
        regState.enable();
        regState.setValidators([Validators.required]);
        regState.setValue('');

        //update controls with new validation
        regState.updateValueAndValidity();

      } else {
        regState.disable();
        regState.clearValidators();

        //update controls with new validation
        regState.updateValueAndValidity();
      }
    });

  }

  /**
   * set observable for regCountry
   */
  setTroopsToTeachersFormOnChangesObservables() {

    const regCountryControl = this.troopsToTeachersForm.get('regCountry');
    const regState = this.troopsToTeachersForm.get('regState');

    //if a student and no datasheets; if US is selected require state selection and enable it
    regCountryControl.valueChanges.subscribe(selection => {

      if (selection === 'US') {
        regState.enable();
        regState.setValidators([Validators.required]);
        regState.setValue('');

        //update controls with new validation
        regState.updateValueAndValidity();

      } else {
        regState.clearValidators();
        regState.disable();
        
        //update controls with new validation
        regState.updateValueAndValidity();
      }
    });

  }

  selectAlternateTraining() {
    let modalOptions = {
      title: 'SELECT ANOTHER SESSION',
      message: "Registration for this event is unavailable at this time because the session is full or the date has passed. Please select from the options below. Submit inquiries to <a href=\"mailto:asvabcep@gmail.com\">asvabcep@gmail.com</a>.",
      alternateTraining: this.alternateTraining
    }

    // had to open in setTimeout due to https://github.com/angular/components/issues/5268
    setTimeout(() => this._dialog.open(AlternateTrainingDialogComponent, { data: modalOptions }).beforeClosed().subscribe(result => {

      if (result) {

        if (result !== 'cancel') {
          this.alternateTrainingSelected = result;

          if (!isNaN(this.alternateTrainingSelected)) {
            //route to alternateTrainingSelected
            this.reloadComponent(this.alternateTrainingSelected);
          } else {
            this.alternateTraining = [];
            this.disableForms();
          }

        } else {
          //cancel was selected
          this.alternateTraining = [];
          this.disableForms();
        }

      }

    }));

  }

  reloadComponent(trainingId) {
    this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    this._router.onSameUrlNavigation = 'reload';
    this._router.navigate(['/training-registration-form/' + trainingId]);
  }

  disableForms() {
    this.virtualForm.disable();
    this.nonVirtualForm.disable();
    this.troopsToTeachersForm.disable();
  }

  getFormValidationErrors() {
    Object.keys(this.troopsToTeachersForm.controls).forEach(key => {
  
    const controlErrors: ValidationErrors = this.troopsToTeachersForm.get(key).errors;
    if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            console.error('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          });
        }
      });
    }

    submitVirtualPTIRegistration(){
      this.validation = undefined;
      this.formDisabled = true;
      
      var essTrainingRegistration = {
        essTrainingId : this.training.essTrainingId,
        registrationTypeId : this.training.typeId,
        registrationFirstName : this.regFirstName,
        registrationLastName : this.regLastName,
        registrationEmailAddress : this.regEmailAddress,
        registrationOrganization : this.regOrganization,
        registrationRole : this.regRole,
        mailingAddress : {
          locationName : this.locationName,
          addressOne : this.addressOne,
          addressTwo : this.addressTwo,
          attn : this.attn,
          city : this.city,
          state : this.regState,
          zip : this.regZip
         }
      };
      
      if (this.regFirstName == undefined ||
        this.regLastName == undefined ||
        this.regEmailAddress == undefined ||
        this.regOrganization == undefined ||
        this.regRole == undefined ||
        this.locationName == undefined ||
        this.addressOne == undefined ||
        this.attn == undefined ||
        this.city == undefined ||
        this.regState == '' ||
        this.regZip == undefined
        ) {
        this.validation = 'Fill out all required fields.';
        this.formDisabled = false;
        return false;
      }
      
      this.submitPostData(essTrainingRegistration);

    }
}
