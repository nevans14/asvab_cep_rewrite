import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentManagementService } from '../services/content-management.service';

@Component({
  selector: 'app-general-link',
  templateUrl: './general-link.component.html',
  styleUrls: ['./general-link.component.scss']
})
export class GeneralLinkComponent implements OnInit {

  pageHtml = [];
  path;

  constructor(
    private _contentManagementService: ContentManagementService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  redirect(path) {
    this._router.navigate([path]);
  }
}
