import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralPrivacySecurityComponent } from './general-privacy-security.component';

describe('GeneralPrivacySecurityComponent', () => {
  let component: GeneralPrivacySecurityComponent;
  let fixture: ComponentFixture<GeneralPrivacySecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralPrivacySecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralPrivacySecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
