import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ContentManagementService } from '../services/content-management.service';
import { MediaCenterService } from 'app/services/media-center.service';
import { ConfigService } from 'app/services/config.service';
import { UtilityService } from 'app/services/utility.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { RegisterDialogComponent } from 'app/core/dialogs/register-dialog/register-dialog.component';
import { UserService } from 'app/services/user.service';
@Component({
  selector: 'app-educators',
  templateUrl: './educators.component.html',
  styleUrls: ['./educators.component.scss']
})
export class EducatorsComponent implements OnInit {
  // Current page from the DB
  pageHtml = [];
  educators = true;
  educatorsTest = false;
  educatorsPostTest = false;
  federallyFundedSources = false;
  teachers = false;
  path;
  mediaCenterList: any;
  showEducators = false;
  showEducatorsTest = false;
  showEducatorsPostTest = false;
  showFederallyFundedSources = false;
  showTeachers = false;
  resource = undefined;
  resources = undefined;
  documents = undefined;

  constructor(
    private _contentManagementService: ContentManagementService,
    private _router: Router,
    private _mediaCenterService: MediaCenterService,
    private _config: ConfigService,
    private _utility: UtilityService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _dialog: MatDialog,
    private _userService: UserService,
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    console.debug('url:', this.path);
  
    this.setSelectedTab(this.path);
    
    this._mediaCenterService.getMediaCenterList().then(
      data => {
          this.mediaCenterList = data;
          console.debug( 'mediaCenterList:', this.mediaCenterList );
      });

      switch (this.path) {
        case '/educators':
          this.showEducators = true;
          break;
        case '/educators-test':
          this.showEducatorsTest = true;
          break;
        case '/educators-post-test':
          this.showEducatorsPostTest = true;
          break;
        case '/federally-funded-sources':
          this.showFederallyFundedSources = true;
          break;
        case '/teachers':
          this.showTeachers = true;
          break;
      }

    this.resource = this._config.getImageUrl();
    this.resources = this.resource + 'static/';
    this.documents = this.resource + 'CEP_PDF_Contents/';

    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }

  redirect(path) {
    this.setSelectedTab(path);
    this._router.navigate([path]);
  }

  setSelectedTab(path) {
    if (path.indexOf('educators-test') > -1) {
      this.educators = false;
      this.educatorsTest = true;
      this.educatorsPostTest = false;
      this.federallyFundedSources = false;
      this.teachers = false;
    } else if (path.indexOf('educators-post-test') > -1) {
      this.educators = false;
      this.educatorsTest = false;
      this.educatorsPostTest = true;
      this.federallyFundedSources = false;
      this.teachers = false;
    } else if (path.indexOf('federally-funded-sources') > -1) {
      this.educators = false;
      this.educatorsTest = false;
      this.educatorsPostTest = false;
      this.federallyFundedSources = true;
      this.teachers = false;
    } else if (path.indexOf('educators') > -1) {
      this.educators = true;
      this.educatorsTest = false;
      this.educatorsPostTest = false;
      this.federallyFundedSources = false;
      this.teachers = false;
    } else if (path.indexOf('teachers') > -1) {
      this.educators = false;
      this.educatorsTest = false;
      this.educatorsPostTest = false;
      this.federallyFundedSources = false;
      this.teachers = true;
    }
  }

  openRegistrtionModal() {
    this._dialog.open(RegisterDialogComponent, {
      hasBackdrop: true,
      panelClass: 'custom-dialog-container',
      height: '95%',
      autoFocus: true,
      disableClose: true
    });
  }
}
