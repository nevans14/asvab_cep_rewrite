import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'app/services/config.service';
import { ContentManagementService } from 'app/services/content-management.service';
@Component({
  selector: 'app-continuing-education',
  templateUrl: './continuing-education.component.html',
  styleUrls: ['./continuing-education.component.scss']
})
export class ContinuingEducationComponent implements OnInit {

  imageBaseUrl: any = undefined;
  path: string = undefined;
  pageHtml: any = [];

  constructor(
    private _configService: ConfigService,
    private _router: Router,
    private _contentManagementService: ContentManagementService,
  ) { }

  ngOnInit() {
    this.imageBaseUrl = this._configService.getImageUrl();
    this.path = this._router.url;
    this.pageHtml = this._contentManagementService.getPageByName(this.path);
  }
}
